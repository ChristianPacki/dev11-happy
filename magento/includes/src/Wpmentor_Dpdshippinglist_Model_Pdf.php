<?php

class Wpmentor_Dpdshippinglist_Model_Pdf
{
    public function generate_pdf($event) {
        //get the admin session
        Mage::getSingleton('core/session', array('name'=>'adminhtml'));

        //verify if the user is logged in to the backend
        if(Mage::getSingleton('admin/session')->isLoggedIn()){
            if(isset($_POST['generate_dpdshippinglist_pdf'])){
                require_once(Mage::getBaseDir('lib').'/MPDF57/mpdf.php');
                $mpdf=new mPDF('c','A4','','' , 0 , 0); 
                $mpdf->SetDisplayMode('fullpage');
                $mpdf->list_indent_first_level = 0;  // 1 or 0 - whether to indent the first level of a list
                $mpdf->AddPage();
                $mpdf->defaultfooterfontsize=10;
                $mpdf->defaultfooterfontstyle='BI';
                $mpdf->defaultfooterline=0;
                $footer = '
                    <div style="width:92%;margin-left:auto;margin-right:auto;font-family: Arial, "Helvetica Neue", Helvetica, sans-serif;">
                        <div style="float:left;width:50%;">
                            Unterschrift    _______________
                        </div>
                        <div style="float:right;width:50%;text-align:right;">
                            Seite {PAGENO} von {nbpg}
                        </div>
                    </div>
                ';
                $mpdf->SetHTMLFooter($footer);
                
                $html = '
	
		<style>
			.container {
				width:92%;
				margin-left:auto;
				margin-right:auto;
			}
			.top-header {
			overflow:hidden;
			font-size:13px;
			font-family: Arial, "Helvetica Neue", Helvetica, sans-serif;
			}
			.header-column {
			width:30%;
			display:inline-block;
			float:left;
			}
			.header-column img {
			float:right;
			}
			.column-down {
			margin-top:38px;
			}
			.content {
			margin-top:100px;
			width:100%;
			}
			.content table, .content table tr, .content table thead, .content table tbody {
			width:100%;
			}
			
			.norm_td {
			
	
			}
			
			td {
			font-family: Arial, "Helvetica Neue", Helvetica, sans-serif;
			font-size:11px;
			}
			
			table th {
			font-family: Arial, "Helvetica Neue", Helvetica, sans-serif;
			text-align:left;
			border-bottom:2px solid black;
            font-size: 10px !important;
            padding-bottom: 7px;
			}
			.address {
			/*width:33%;*/
			}
            table {
            border-collapse: separate;
            border-spacing: 7px 0;
            }

            td {
            padding: 5px 0;
            font-size: 10px !important;
            }
		</style>
		

		<div class="container">
			<div class="inn-con">
				<header class="top-header"> 
					<div class="first-column header-column">
						<span>
							<div style="font-size: 20px; front-weight: bold;">DPD Shipping List</div>
							
						</span>
					</div>
					<div class="third-column header-column" style="text-align:right;padding-right:20px;">
						<span>
							<div>Von: '.$_POST["starting_date"].'</div>
                            <div>Nach: '.$_POST["ending_date"].'</div>
						</span>
					</div>
					<div class="forth-column header-column">
						<img src="'.Mage::getBaseDir("lib").'/MPDF57/logo.png'.'" width="100%" style="margin-left:40px;float:right;clear:both;"/>
					</div>
					
				</header>
				
				<div class="content">
					<table style="width:100%;">
							<tr>
                                <th>Nr.</th>
                                <th>Bestellnummer</th>
                                <th>Paket-Nr</th>
                                <th>Kundenname</th>
                                <th>Kunden-eMail</th>
                                <th>Kundenadresse</th>
                                <th>Tracking-URL</th>
							</tr>
						<tbody>
						';
                        
        $resource = Mage::getSingleton('core/resource');
        
        $readConnection = $resource->getConnection('core_read');

        $salesShipmentTable = $resource->getTableName('sales_flat_shipment');
        $customerTable = $resource->getTableName('customer_entity');
        $salesOrderAddressTable = $resource->getTableName('sales_flat_order_address');
        
        $startingDateWhere = '';
        if(isset($_POST['starting_date']) && $_POST['starting_date'] != ''){
            $startingDate = $_POST['starting_date'];
            $startingDateWhere = " AND DATE(t1.created_at) >= '$startingDate'";
        }

        $endingDateWhere = '';
        if(isset($_POST['ending_date']) && $_POST['ending_date'] != ''){
            $endingDate = $_POST['ending_date'];
            $endingDateWhere = " AND DATE(t1.created_at) <= '$endingDate.'";
        }

        $query = "SELECT t1.entity_id, t1.dpd_label_path, t1.dpd_tracking_url, t1.customer_id FROM $salesShipmentTable t1 WHERE t1.dpd_label_exported IS NOT NULL ".$startingDateWhere.$endingDateWhere;
        $results = $readConnection->fetchAll($query);

			if($results){
                $i = 1;
                foreach($results as $result){
                    $dpdLabelPath = $result['dpd_label_path'];
                    $dpdLabelPathArray = explode("-", $dpdLabelPath);
                    $orderNumber = $dpdLabelPathArray[0];
                    $dpdLabelPathFileName = $dpdLabelPathArray[1];
                    $trackingNumberArray = explode(".", $dpdLabelPathFileName);
                    $trackingNumber = $trackingNumberArray[0];
                    $order_id = $result['entity_id'];
                    $customer_id = 0;
                    if($result['customer_id']){
                        $customer_id = $result['customer_id'];
                    }
                    $query = "SELECT t3.firstname, t3.lastname, t3.email, t3.telephone, t3.street, t3.city  FROM $salesOrderAddressTable t3  WHERE t3.customer_id = $customer_id AND t3.address_type = 'shipping' LIMIT 1";
                    $customerDetails = $readConnection->fetchAll($query);
                    $html .= '<tr>
                        <td>'.$i.'</td>
                        <td>'.$orderNumber.'</td>
                        <td>'.$trackingNumber.'</td>';
                        
                        if($customerDetails){
                            $html .= '<td>'.$customerDetails[0]["firstname"].' '.$customerDetails[0]["lastname"].'</td>
                            <td>'.$customerDetails[0]["email"].'</td>
                            <td>'.$customerDetails[0]["street"].' '. $customerDetails[0]["city"].'</td>';
                        }
                        else{
                            $html .= '
                            <td></td>
                            <td></td>
                            <td></td>
                            ';
                        }
                        $html .= '
                        <td>'.$result['dpd_tracking_url'].'</td>
                    </tr>
                    ';
                    $i++;
                }
            }
						
						$html .= '</tbody>
						</table>
						</div>	
						</div>
		</div>
	';

                $html = mb_convert_encoding($html, 'UTF-8', 'UTF-8');
                $mpdf->WriteHTML($html);
                
                $mpdf->Output('Shipping_list.pdf','I');
                exit;
            }
        }
    }
}