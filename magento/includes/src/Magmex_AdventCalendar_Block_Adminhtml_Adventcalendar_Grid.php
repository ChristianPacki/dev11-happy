<?php
/**
 * Magmex Adventcalendar grid block for showing the Advent Calendars already created
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.magmex.com/MAGMEX_SOFTWARE_LICENSE.txt
 *
 * @copyright Copyright (c) 2015 magmex, Michael Stork, (http://www.magmex.com)
 * @license http://www.magmex.com/MAGMEX_SOFTWARE_LICENSE.txt
 * @category Magmex
 * @package Magmex_AdventCalendar
 * @author Michael Stork <m.stork@magmex.com>
 */
class Magmex_AdventCalendar_Block_Adminhtml_Adventcalendar_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    /**
     * Construct and set default options for the main index block
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        
        $this->setId('magmexAdventcalendarGrid');
        $this->setDefaultSort('year');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
    }
    
    /**
     * Loads the data for the advent calendar default grid
     *
     * @return Magmex_AdventCalendar_Block_Adminhtml_Adventcalendar_Grid chaining
     */
    protected function _prepareCollection()
    {
        /** @var $collection Magmex_AdventCalendar_Model_Mysql4_Adventcalendar_Collection */
        $collection = Mage::getModel('magmex_adventcalendar/adventcalendar')->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }
    
    /**
     *
     *
     */
    protected function _prepareColumns()
    {
        $helperData = Mage::helper('magmex_adventcalendar');
        
        $this->addColumn('advent_calendar_id',
            array(
                    'header' => $helperData->__('ID'),
                    'align' => 'right',
                    'width' => '50px',
                    'index' => 'advent_calendar_id',
            )
        );
    
        $this->addColumn('year',
            array(
                    'header' => $helperData->__('Year'),
                    'align' => 'left',
                    'index' => 'year',
            )
        );
        
        if (!Mage::app()->isSingleStoreMode()) {
            $this->addColumn('store_id', array(
                'header' => $helperData->__('Store View'),
                'index' => 'store_id',
                'type' => 'store',
                'store_all' => true,
                'store_view' => true,
                'sortable' => false,
                'filter_condition_callback' => array($this, '_filterStoreCondition'),
            ));
        }
        
        $this->addColumn('edit_advent_calendar_days',
            array(
                'header' => $helperData->__('Edit Advent Calendars or Advent Calendar days'),
                'width' => '151',
                'type' => 'action',
                'getter' => 'getId',
                'actions' => array(
                    array(
                        'caption' => $helperData->__('Edit Advent Calendar days'),
                        'url' => array( 'base' => '*/adventcalendar_day/' ),
                        'field' => 'id'
                    ),
                    array(
                            'caption' => Mage::helper('magmex_adventcalendar')->__('Edit Advent Calendar'),
                            'url' => array( 'base' => '*/*/edit' ),
                            'field' => 'id'
                    )
                ),
                'filter' => false,
                'sortable' => false,
                'index' => 'stores',
                'is_system' => true,
            )
        );
        
        return parent::_prepareColumns();
    }
    
    /**
     * Callback for store filter in grid
     *
     * @param Magmex_AdventCalendar_Model_Mysql4_Adventcalendar_Collection $collection advent calendar collection used in grid
     * @param Mage_Adminhtml_Block_Widget_Grid_Column $column grid column of store selector
     */
    protected function _filterStoreCondition($collection, $column)
    {
        if (!$value = $column->getFilter()->getValue()) {
            return;
        }
        
        $this->getCollection()->addFieldToFilter('store_id', $value);
    }
}