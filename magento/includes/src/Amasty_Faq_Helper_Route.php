<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_Faq
 */

class Amasty_Faq_Helper_Route extends Mage_Core_Helper_Abstract
{
    public function restrictUndispatched()
    {
        if (!Mage::registry('amfaq_route_dispatched'))
        {
            $response = Mage::app()->getResponse();
            $action = Mage::app()->getFrontController()->getAction();

            $response
                ->setHeader('HTTP/1.1','404 Not Found')
                ->setHeader('Status','404 File not found');

            $pageId = Mage::getStoreConfig('web/default/cms_no_route');
            Mage::helper('cms/page')->renderPage($action, $pageId);

            $response->sendResponse();
            Mage::helper('ambase/utils')->_exit();
        }
    }
}
