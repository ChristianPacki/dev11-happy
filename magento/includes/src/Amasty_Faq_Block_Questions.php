<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_Faq
 */

class Amasty_Faq_Block_Questions extends Mage_Core_Block_Template
{
    protected function _construct()
    {
        parent::_construct();
        $this->setTemplate('amasty/amfaq/questions.phtml');
    }

    public function getQuestions()
    {
        // Load topics

        $topicCollection = Mage::getResourceModel('amfaq/topic_collection');

        $orderTopics = Mage::getStoreConfig('amfaq/faq_page/sort_topics');
        if (!Mage::app()->isSingleStoreMode()) {
            $topicCollection->addStoreFilter(Mage::app()->getStore());
        }
        $topicCollection->setOrder($orderTopics, 'ASC');

        $topicCollection->load();

        $noneTopic = Mage::getModel('amfaq/topic')
            ->setId(0)
            ->setTitle(Mage::getStoreConfig('amfaq/topic_page/none_title'))
        ;

        $topics = array(array(
            'questions' => array(),
            'id' =>0,
            'object' => $noneTopic
        )); //empty topic

        $i = 0;
        foreach ($topicCollection as $topic)
            $topics[++$i] = array(
                'title' => $topic->title,
                'questions' => array(),
                'id' => $topic->getId(),
                'object' => $topic
            );

        // Load questions
        $totalQuestions = 0;
        $questionIds = array();
        
        foreach ($topics as &$topic)
        {
            $questionCollection = Mage::getResourceSingleton('amfaq/question_collection')
                ->loadByTopic(
                    $topic['id'],
                    Mage::getStoreConfig('amfaq/faq_page/limit'),
                    $this->getSearch()
                );

            $totalQuestions += sizeof($questionCollection);

            foreach($questionCollection as $question) {
                $questionIds[] = $question->getId();
                $topic['questions'][] = $question;
            }

            $topic['more'] = $questionCollection->getSize() - sizeof($questionCollection);
        }unset($topic);

        // Break by columns

        $columnCount = Mage::getStoreConfig('amfaq/faq_page/columns');
        $columns = array();

        $totalItems = 0;
        foreach ($topics as $topic)
        {
            $questions = sizeof($topic['questions']);
            if ($questions > 0)
            {
                $totalItems += $questions;
                if (isset($topic['title']))
                    $totalItems += 2;
            }
        }

        $inColumn = (int)ceil($totalItems / $columnCount);
        $column = 0;
        $totalItems = 0;
        for ($i = 0; $i < sizeof($topics); $i++)
        {
            $topic = $topics[$i];

            $questionsSize = sizeof($topic['questions']);
            if ($questionsSize > 0)
            {
                $headSize = isset($topic['title']) ? 2 : 0;

                $groupSize = $questionsSize + $headSize;

                $remains = ($column + 1) * $inColumn - $totalItems;
                if ($remains < $headSize + 1 && $column < $columnCount - 1)
                {
                    $column++;
                    $remains = ($column + 1) * $inColumn - $totalItems;
                }

                if ($remains < $groupSize && $column < $columnCount - 1) // break group
                {
                    $breakAt = $remains - $headSize;

                    $thisCol = array(
                        'questions' => array_slice($topic['questions'], 0, $breakAt),
                        'more' => 0,
                        'object' => $topic['object']
                    );
                    if (isset($topic['title']))
                        $thisCol['title'] = $topic['title'];
                    $columns[$column][] = $thisCol;
                    $topics[$i] = array(
                        'questions' => array_slice($topic['questions'], $breakAt),
                        'more' => $topic['more'],
                        'object' => $topic['object']
                    );
                    $i--;
                    $totalItems += $breakAt + $headSize;
                }
                else // insert entire group
                {
                    $columns[$column][] = $topic;
                    $totalItems += $groupSize;
                }
            }
        };

        return array(
            'total' => $totalQuestions,
            'columns' => $columns,
            'question_ids' => $questionIds,
        );
    }

    public function getAllTags($questionIds = array())
    {
        if ($questionIds) {
            $tbl = Mage::getSingleton('core/resource')->getTableName('amfaq/question_tag');
            $db  = Mage::getSingleton('core/resource')->getConnection('amfaq_read');
            $tags = $db->select()->from(array('t2' => $tbl), array('tag_id'))
                ->where('question_id IN (?)', $questionIds);
            $tags = array_unique($db->fetchCol($tags));
            return Mage::getResourceModel('amfaq/tag_collection')->addFieldToFilter('tag_id', array('in' => $tags));
        } else {
            return Mage::getResourceModel('amfaq/tag_collection');
        }
    }
}
