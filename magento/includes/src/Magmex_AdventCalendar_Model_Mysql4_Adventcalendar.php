<?php
/**
 * Magmex MySQL model for main Advent Calendar table
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.magmex.com/MAGMEX_SOFTWARE_LICENSE.txt
 *
 * @copyright Copyright (c) 2015 magmex, Michael Stork, (http://www.magmex.com)
 * @license http://www.magmex.com/MAGMEX_SOFTWARE_LICENSE.txt
 * @category Magmex
 * @package Magmex_AdventCalendar
 * @author Michael Stork <m.stork@magmex.com>
 */
class Magmex_AdventCalendar_Model_Mysql4_Adventcalendar extends Mage_Core_Model_Mysql4_Abstract
{
    /**
     * Construct
     * Inits the main advent calendar table
     *
     * @see Mage_Core_Model_Resource_Abstract::_construct()
     * @return void
     */
    public function _construct()
    {
        $this->_init('magmex_adventcalendar/adventcalendar', 'advent_calendar_id');
    }
    
    /**
     * Loads an advent calendar model for a given year
     *
     * @param Magmex_AdventCalendar_Model_Adventcalendar $adventCalendar advent calendar model
     * @param integer $year year to load the advent calendar
     * @return Magmex_AdventCalendar_Model_Mysql4_Adventcalendar chaining
     */
    public function loadAdventCalendarByYear(Magmex_AdventCalendar_Model_Adventcalendar $adventCalendar, $year)
    {
        $readAdapter = $this->_getReadAdapter();
        $select = $readAdapter->select()
            ->from($this->getMainTable())
            ->reset(Zend_Db_Select::COLUMNS)
            ->columns('advent_calendar_id')
            ->where('store_id = ?', Mage::app()->getStore()->getId())
            ->where('year = ?', $year);
        
        $data = $readAdapter->fetchRow($select);
        
        if (!empty($data)) {
            $adventCalendar->load($data['advent_calendar_id']);
        }
        
        return $this;
    }
}