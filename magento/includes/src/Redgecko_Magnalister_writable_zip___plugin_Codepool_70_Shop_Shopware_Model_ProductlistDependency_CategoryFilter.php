<?php
/**
 * 888888ba                 dP  .88888.                    dP                
 * 88    `8b                88 d8'   `88                   88                
 * 88aaaa8P' .d8888b. .d888b88 88        .d8888b. .d8888b. 88  .dP  .d8888b. 
 * 88   `8b. 88ooood8 88'  `88 88   YP88 88ooood8 88'  `"" 88888"   88'  `88 
 * 88     88 88.  ... 88.  .88 Y8.   .88 88.  ... 88.  ... 88  `8b. 88.  .88 
 * dP     dP `88888P' `88888P8  `88888'  `88888P' `88888P' dP   `YP `88888P' 
 *
 *                          m a g n a l i s t e r
 *                                      boost your Online-Shop
 *
 * -----------------------------------------------------------------------------
 * $Id$
 *
 * (c) 2010 - 2014 RedGecko GmbH -- http://www.redgecko.de
 *     Released under the MIT License (Expat)
 * -----------------------------------------------------------------------------
 */
MLFilesystem::gi()->loadClass('Shop_Model_ProductListDependency_CategoryFilter_Abstract');
class ML_Shopware_Model_ProductListDependency_CategoryFilter extends ML_Shop_Model_ProductListDependency_CategoryFilter_Abstract {
    
    /**
     * if count of categories more then $iTreeMaxCount, display just part of cat-tree
     * @var int $iTreeMaxCount
     */
	protected $iTreeMaxCount = 200;
    
    /**
     * @var null not initalised
     * @var array array(catId=>count childs) displays only cats, count childs is needed to display arrow in select>option
     */
	protected $aCatsFilter = null;
    
    /**
     * key=>value for filtering (eg. validation and form-select)
     * @var array|null
     */
    protected $aFilterValues = null;    
    
    /**
     * all categories
     * @var array|null
     */
    protected $aCategories = null;

    /**
     * @param ML_Database_Model_Query_Select $mQuery
     * @return void
     */
    public function manipulateQuery ($mQuery) {
        $sFilterValue = (int)$this->getFilterValue();
        if (
            !empty($sFilterValue) 
            && $sFilterValue !== 1 //root-category
            && array_key_exists($sFilterValue, $this->getFilterValues())
        ) {
            $mQuery
                ->join(array('s_categories', 'c', "c.id = $sFilterValue"), ML_Database_Model_Query_Select::JOIN_TYPE_LEFT)
                ->join(array(MLDatabase::getDbInstance()->tableExists('s_articles_categories_ro') ? 's_articles_categories_ro' : 's_articles_categories', 'pc', 'pc.articleID  = p.id AND pc.categoryID = c.id'), ML_Database_Model_Query_Select::JOIN_TYPE_INNER)
            ;
        }
    }
    
    /**
     * parent checks if value is possible - we dont know in this moment because perhaps not all categories are loaded
     * @param string $sValue
     * @return \ML_Shopware_Model_ProductListDependency_CategoryFilter
     */
    public function setFilterValue($sValue) {
        $this->sFilterValue = $sValue;
        return $this;
    }
    
    /**
     * key=>value for categories
     * @return array
     */
    protected function getFilterValues () {
        if ($this->aFilterValues === null) {
            if (
                $this->getFilterValue() !== null 
                && Shopware()->Db()->fetchOne(
                    'SELECT count(*) FROM '.Shopware()->Models()->getClassMetadata('Shopware\Models\Category\Category')->getTableName()
                ) > $this->iTreeMaxCount
            ) {
                $this->aCatsFilter = array(
                    1, trim($this->getFilterValue()),
                );
                $oBuilder = Shopware()->Models()->createQueryBuilder();
                $oBuilder->from('Shopware\Models\Category\Category', 'category')
                    ->select(array(
                        'category',
                    ))
                    ->andWhere('category.id = :id')
                    ->setParameter('id', (int)$this->getFilterValue())
                    ->addOrderBy('category.position')
                ;
                foreach ($oBuilder->getQuery()->getArrayresult() as $aCurrent) {
                    foreach(explode('|',$aCurrent['path']) as $iCurrent) {
                        $iCurrent = trim($iCurrent);
                        if (!empty($iCurrent) && !in_array($iCurrent, $this->aCatsFilter)) {
                            $this->aCatsFilter[] = $iCurrent;
                        }
                    }
                    if (!in_array(trim($aCats['id']), $this->aCatsFilter)) {
                        $this->aCatsFilter[] = trim($aCats['id']);
                    }
                }
            }
            $aCats = array(array(
                'value' => '',
                'label' => sprintf(MLI18n::gi()->get('Productlist_Filter_sEmpty'), MLI18n::gi()->get('Shopware_Productlist_Filter_sCategory')),
            ));
            foreach($this->getShopwareCategories() as $aValue){                
                 $aCats[$aValue['value']] = $aValue;
            }
            $this->aFilterValues = $aCats;
        }
        return $this->aFilterValues;
    }
    
    /**
     * gets all categories
     * @param array|null $aCats nested cats
     * @return array
     */
    protected function getShopwareCategories ($iParentId = null) {
        $aCats = $this->getShopwareCategoryByParentId($iParentId === null ? 1 : $iParentId) ;
        foreach ($aCats as $aCat) {
            $this->aCategories[$aCat['id']] = array(
                'value' => $aCat['id'],
                'label' => str_repeat('&nbsp;', substr_count($aCat['path'], '|') * 2) . $aCat['name'],
            );
            if (
                empty($this->aCatsFilter) 
                || $aCat['parentid'] == $this->getFilterValue()
                || in_array($aCat['id'],$this->aCatsFilter)
            ) {
                $this->getShopwareCategories($aCat['id']);
            } else {
                $this->aCategories[$aCat['id']]['label'] .= 
                    (count($this->getShopwareCategoryByParentId($aCat['id'], 1)) > 0)
                    ? '&nbsp;&#8628;' 
                    : ''
                ;
            }
        }
        if($iParentId === null){
            return $this->aCategories;
        } else {
            return;
        }
    }
    
    protected function getShopwareCategoryByParentId ($iParentId, $iMaxResults = null) {
        $oBuilder = Shopware()->Models()->createQueryBuilder();
        $oBuilder->from('Shopware\Models\Category\Category', 'category')
            ->select(array(
                'category',
            ))
            ->andWhere('category.parent = :parent')
            ->setParameter('parent', $iParentId)
            ->addOrderBy('category.position')
        ;
        if ($iMaxResults !== null) {
            $oBuilder->setMaxResults($iMaxResults);
        }
        return $oBuilder->getQuery()->getArrayresult();
    }
    
}
