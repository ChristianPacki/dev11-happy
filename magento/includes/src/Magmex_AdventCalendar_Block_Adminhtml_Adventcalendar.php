<?php
/**
 * Magmex Adventcalendar block for the showing Advent Calendars
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.magmex.com/MAGMEX_SOFTWARE_LICENSE.txt
 *
 * @copyright Copyright (c) 2015 magmex, Michael Stork, (http://www.magmex.com)
 * @license http://www.magmex.com/MAGMEX_SOFTWARE_LICENSE.txt
 * @category Magmex
 * @package Magmex_AdventCalendar
 * @author Michael Stork <m.stork@magmex.com>
 */
class Magmex_AdventCalendar_Block_Adminhtml_Adventcalendar extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    /**
     * Construct and set default options for the main index block
     *
     * @return void
     */
    public function __construct()
    {
        $this->_controller = 'adminhtml_adventcalendar';
        $this->_blockGroup = 'magmex_adventcalendar';
        $helperData = Mage::helper('magmex_adventcalendar');
        $this->_headerText = $helperData->__('Manage Advent Calendars');
        $this->_addButtonLabel = $helperData->__('Add new Advent Calendar');
        parent::__construct();
    }
}