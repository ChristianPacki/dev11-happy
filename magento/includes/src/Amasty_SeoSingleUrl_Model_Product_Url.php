<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_SeoSingleUrl
 */

class Amasty_SeoSingleUrl_Model_Product_Url extends Mage_Catalog_Model_Product_Url
{
    public function getUrl(Mage_Catalog_Model_Product $product, $params = array())
    {
        $url = $this->getSeoUrl($product);
        if (!$url) {
            return parent::getUrl($product, $params);
        }

        $params['_direct'] = $url;

        return rtrim(Mage::getUrl('', $params), '/');
    }

    /**
     * @param Mage_Catalog_Model_Product $product
     * @return null|string
     */
    protected function getSeoUrl(Mage_Catalog_Model_Product $product)
    {
        $url = null;
        if (!Mage::helper('amseourl')->useDefaultProductUrlRules()) {
            $url = Mage::helper('amseourl/product_url_rewrite')->getProductPath($product);
        }

        return $url;
    }
}
