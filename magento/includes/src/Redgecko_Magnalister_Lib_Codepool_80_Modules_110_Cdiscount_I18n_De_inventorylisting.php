<?php

/*
 * location of getStatus function -->
 * 110_Cdiscount/controler/Cdiscount/Listing/010_Inventory.php
 *
 * Statuses that Cdiscount API have
 * STATUS_NEW = 'New';
 * STATUS_UPDATE = 'Update';
 * STATUS_WAITING = 'Waiting';
 * STATUS_WAITING_UPDATE = 'WaitingUpdate';
 * STATUS_ACTIVE = 'Active';
*/

MLI18n::gi()->cdiscount_inventory_listing_quantity = 'Bestand<br />Shop / Cdiscount';
MLI18n::gi()->cdiscount_inventory_listing_price = 'Cdiscount Preis';
MLI18n::gi()->cdiscount_inventory_listing_status = 'Status';
MLI18n::gi()->cdiscount_inventory_listing_status_new = 'New';
MLI18n::gi()->cdiscount_inventory_listing_status_update = 'Cdiscount is updating item';
MLI18n::gi()->cdiscount_inventory_listing_status_waiting = 'Submission to Cdiscount in progress';
MLI18n::gi()->cdiscount_inventory_listing_status_waiting_update = 'Cdiscount is processing item';
MLI18n::gi()->cdiscount_inventory_listing_status_active = 'Active';