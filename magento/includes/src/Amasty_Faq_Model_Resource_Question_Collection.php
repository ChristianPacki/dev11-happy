<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_Faq
 */

class Amasty_Faq_Model_Resource_Question_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    protected function _construct()
    {
        $this->_init('amfaq/question');
        $this->_map['fields']['store'] = 'store_table.store_id';
    }

    public function applyDefaultFilters()
    {
        $select = $this->getSelect();

        $select
            ->where('status != ?', 'pending')
            ->group('main_table.question_id');

        if (!Mage::app()->isSingleStoreMode()) {
            $store = array(0, Mage::app()->getStore()->getId());

            $this->addFilter('store_table.store_id', array('in' => $store), 'public');
            $select->join(
                    array('store_table' => $this->getTable('amfaq/question_store')),
                    'main_table.question_id = store_table.question_id'
                );
        }

        $visibilities = array('public');
        if (Mage::helper('customer')->isLoggedIn())
            $visibilities[] = 'auth';
        $this->addFilter('visibility', array('in' => $visibilities), 'public');
    }

    public function applySearchFilter($query)
    {
        $adapter = $this->getSelect()->getAdapter();

        $this->getSelect()->where('main_table.title LIKE ? OR answer LIKE ?', "%$query%");
        $titleMatch = $adapter->quoteInto('main_table.title LIKE ?', "%$query%");
        $this->setOrder($titleMatch);
    }

    public function applyTagFilter($id)
    {
        $this->getSelect()
            ->join(
                array('qt' => $this->getTable('amfaq/question_tag')),
                'qt.question_id = main_table.question_id',
                array()
            )
            ->where('qt.tag_id = ?', $id)
        ;
    }

    public function applyDefaultOrder()
    {
        $orderQuestions = Mage::getStoreConfig('amfaq/faq_page/sort_questions');
        $this->setOrder($orderQuestions, 'ASC');
    }

    public function loadByTopic($topicId, $limit = 0, $search = null)
    {
        $questionCollection = Mage::getModel('amfaq/question')->getCollection();

        $questionCollection
            ->getSelect()
            ->limit($limit)
        ;

        $questionCollection->applyDefaultFilters();

        if ($search)
            $questionCollection->applySearchFilter($search);

        $questionCollection->applyDefaultOrder();

        $select = $questionCollection->getSelect();

        $select->joinLeft(
            array('question_topic' => $this->getTable('amfaq/question_topic')),
            'question_topic.question_id = main_table.question_id',
            array()
        );
        if ($topicId > 0)
            $select->where('topic_id = ?', $topicId);
        else
            $select->where('topic_id IS NULL');

        $questionCollection->load();
        $first = $questionCollection->getFirstItem();
        $first->afterLoad();

        return $questionCollection;
    }

    public function addStoreFilter($store, $withAdmin = true)
    {
        if ($store instanceof Mage_Core_Model_Store) {
            $store = array($store->getId());
        }

        if (!is_array($store)) {
            $store = array($store);
        }

        if ($withAdmin) {
            $store[] = Mage_Core_Model_App::ADMIN_STORE_ID;
        }

        $this->addFilter('store', array('in' => $store), 'public');

        return $this;
    }

    protected function _renderFiltersBefore()
    {
        if ($this->getFilter('store')) {
            $this->getSelect()->join(
                array('store_table' => $this->getTable('amfaq/question_store')),
                'main_table.question_id = store_table.question_id',
                array()
            )->group('main_table.question_id');

            /*
             * Allow analytic functions usage because of one field grouping
             */
            $this->_useAnalyticFunction = true;
        }
        return parent::_renderFiltersBefore();
    }

    public function getSelectCountSql()
    {
        $countSelect = parent::getSelectCountSql();

        $countSelect->reset(Zend_Db_Select::GROUP);

        return $countSelect;
    }

}