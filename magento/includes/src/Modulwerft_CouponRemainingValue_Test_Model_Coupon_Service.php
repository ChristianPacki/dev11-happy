<?php

/**
 * @category Modulwerft
 * @package Modulwerft_CouponRemainingValue
 * @author Modulwerft GmbH <info@modulwerft.com>
 * @author Philipp Zabel <p.zabel@shopwerft.com>
 * @copyright 2014 Shopwerft GmbH (http://www.shopwerft.com)
 * @since 1.2.0
 */
class Modulwerft_CouponRemainingValue_Test_Model_Coupon_Service
    extends EcomDev_PHPUnit_Test_Case
{

    /**
     * @param int $orderId An order with a remaining value coupon
     *
     * @loadFixture
     * @loadFixture new_orders
     * @loadFixture rules
     * @dataProvider dataProvider
     *
     * @singleton modulwerft_couponremainingvalue/coupon_service
     */
    public function testUseCouponValues($orderId)
    {
        /** @var Mage_Sales_Model_Order $order */
        $order = Mage::getModel('sales/order')
            ->load($orderId);

        $couponCode = $order->getCouponCode();

        /** @var Modulwerft_CouponRemainingValue_Model_Coupon $coupon */
        $coupon = Mage::getModel('modulwerft_couponremainingvalue/coupon')
            ->loadByCode($couponCode);
        $rule = $coupon->getRule();
        $oldValue = $coupon->getRemainingValue();

        /** @var Modulwerft_CouponRemainingValue_Model_Coupon_Service $service */
        $service = Mage::getSingleton('modulwerft_couponremainingvalue/coupon_service');
        $service->useCouponValues($order);

        // event must be dispatched
        if ($rule->getUseRemainingValue()) {
            $this->assertEventDispatched('modulwerft_couponremainingvalue_coupon_service_used_coupon_value');
        } else {
            $this->assertEventNotDispatched('modulwerft_couponremainingvalue_coupon_service_used_coupon_value');
        }

        /** @var Modulwerft_CouponRemainingValue_Model_Resource_History_Collection $historyCollection */
        $historyCollection = Mage::getResourceModel('modulwerft_couponremainingvalue/history_collection');
        $historyCollection->addOrderToFilter($order);

        // check if history entry was created
        $this->assertEquals(
            ($rule->getUseRemainingValue() ? 1 : 0),
            $historyCollection->count(),
            'Missing history entry for order ' . $order->getIncrementId()
        );

        if (!$rule->getUseRemainingValue()) {
            return;
        }

        /** @var Modulwerft_CouponRemainingValue_Model_History $history */
        $history = $historyCollection->getFirstItem();

        // check values of history entry
        $this->assertEquals(
            $oldValue - abs($order->getDiscountAmount()),
            $history->getLeftValue(),
            'History left value is wrong'
        );
        $this->assertEquals(
            abs($order->getDiscountAmount()),
            $history->getUsedValue(),
            'History used value is wrong'
        );

        // reload coupon because values should have been changed
        $coupon->load($coupon->getId());

        // check if value in coupon was reduced
        $this->assertEquals(
            $history->getLeftValue(),
            $coupon->getRemainingValue(),
            'Remaining value of coupon "' . $couponCode . '" was reduced incorrect'
        );
    }

    /**
     * @param int $orderId An order with canceled items
     * @param int|null $creditmemoId A creditmemo for the given order
     *
     * @loadFixture
     * @loadFixture canceled_orders
     * @loadFixture rules
     * @dataProvider dataProvider
     *
     * @singleton modulwerft_couponremainingvalue/coupon_service
     */
    public function testResetCouponValuesWithEmails($orderId, $creditmemoId = null)
    {
        /** @var Mage_Sales_Model_Order $order */
        $order = Mage::getModel('sales/order')
            ->load($orderId);

        $creditmemo = null;
        if (!is_null($creditmemoId)) {
            /** @var Mage_Sales_Model_Order_Creditmemo $creditmemo */
            $creditmemo = Mage::getModel('sales/order_creditmemo')
                ->load($creditmemoId);
        }

        /** @var Modulwerft_CouponRemainingValue_Helper_Config $configHelper */
        $configHelper = Mage::helper('modulwerft_couponremainingvalue/config');

        $this->_testResetCouponValues(
            $order,
            $creditmemo,
            $configHelper->isEmailResetCouponValueActive($order->getStoreId())
        );
    }

    /**
     * @param int $orderId An order with canceled items
     * @param int|null $creditmemoId A creditmemo for the given order
     *
     * @loadFixture
     * @loadFixture canceled_orders
     * @loadFixture rules
     * @dataProvider dataProvider
     *
     * @singleton modulwerft_couponremainingvalue/coupon_service
     */
    public function testResetCouponValuesWithoutEmails($orderId, $creditmemoId = null)
    {
        /** @var Mage_Sales_Model_Order $order */
        $order = Mage::getModel('sales/order')
            ->load($orderId);

        $creditmemo = null;
        if (!is_null($creditmemoId)) {
            /** @var Mage_Sales_Model_Order_Creditmemo $creditmemo */
            $creditmemo = Mage::getModel('sales/order_creditmemo')
                ->load($creditmemoId);
        }

        /** @var Modulwerft_CouponRemainingValue_Helper_Config $configHelper */
        $configHelper = Mage::helper('modulwerft_couponremainingvalue/config');

        $this->_testResetCouponValues(
            $order,
            $creditmemo,
            $configHelper->isEmailResetCouponValueActive($order->getStoreId())
        );
    }

    /**
     * @param Mage_Sales_Model_Order $order
     * @param Mage_Sales_Model_Order_Creditmemo|null $creditmemo
     * @param bool $checkEmails
     * @return $this
     */
    protected function _testResetCouponValues($order, $creditmemo = null, $checkEmails)
    {
        $couponCode = $order->getCouponCode();

        /** @var Modulwerft_CouponRemainingValue_Model_Coupon $coupon */
        $coupon = Mage::getModel('modulwerft_couponremainingvalue/coupon')
            ->loadByCode($couponCode);

        /** @var Modulwerft_CouponRemainingValue_Model_Coupon_Service $service */
        $service = Mage::getSingleton('modulwerft_couponremainingvalue/coupon_service');

        $service->useCouponValues($order);

        // reload coupon because values should have been changed
        $coupon->load($coupon->getId());
        $oldValue = $coupon->getRemainingValue();

        // create mock object for email template to check whether the emails were sent
        $email = $this->getModelMock('core/email_template', array('send'));

        // set expected value
        $email->expects($this->exactly(intval($checkEmails)))
            ->method('send')
            ->will($this->returnValue(true));

        // use mock model for email template
        $this->replaceByMock('model', 'core/email_template', $email);

        // count history items for this order
        /** @var Modulwerft_CouponRemainingValue_Model_Resource_History_Collection $historyCollection */
        $historyCollection = Mage::getResourceModel('modulwerft_couponremainingvalue/history_collection');
        $historyCount = $historyCollection->addOrderToFilter($order)->count();

        if (!is_null($creditmemo)) {
            $service->resetCouponValues($order, $creditmemo);
        } else {
            $service->resetCouponValues($order);
        }

        // event must be dispatched
        $this->assertEventDispatched('modulwerft_couponremainingvalue_coupon_service_reset_coupon_value');

        // count history items for this order again and check, if a new one appeared
        $historyCountNew = $historyCollection->clear()->load()->count();

        // check if history entry was created
        $this->assertEquals(
            $historyCount + 1,
            $historyCountNew,
            'Missing history entry for order ' . $order->getIncrementId()
        );

        /** @var Modulwerft_CouponRemainingValue_Model_Resource_History_Collection $historyCollection */
        $historyCollection = Mage::getResourceModel('modulwerft_couponremainingvalue/history_collection');
        $historyCollection->addOrderToFilter($order)
            ->addFieldToFilter('used_value', array('lt' => 0));

        /** @var Modulwerft_CouponRemainingValue_Model_History $history */
        $history = $historyCollection->getFirstItem();

        $refundValue = $this->expected('order-%s', $order->getId())->getRefundValue();

        // check values of history entry
        $this->assertEquals(
            $oldValue + $refundValue,
            $history->getLeftValue(),
            'History left value is wrong'
        );
        $this->assertEquals(
            -1.0 * $refundValue,
            $history->getUsedValue(),
            'History used value is wrong'
        );

        // reload coupon because values should have been changed
        $coupon->load($coupon->getId());

        // check if value in coupon was reduced
        $this->assertEquals(
            $history->getLeftValue(),
            $coupon->getRemainingValue(),
            'Remaining value of coupon "' . $couponCode . '" was reduced incorrectly'
        );

        return $this;
    }

    /**
     * Bugfix test for #5434 (Gutschrift ohne Steuerbetrag der Versandkosten)
     *
     * @loadFixture
     * @dataProvider dataProvider
     */
    public function testResetCouponValue($orderID, $couponId)
    {
        /** @var Modulwerft_CouponRemainingValue_Model_Coupon $coupon */
        $coupon = Mage::getModel('modulwerft_couponremainingvalue/coupon')->load($couponId);

        $this->assertNotEquals(
            $coupon->getCouponValue(),
            $coupon->getRemainingValue(),
            'Origin remaining value of coupon is not different to remaining value before resetting coupon.'
        );

        /** @var Mage_Sales_Model_Order $order */
        $order = Mage::getModel('sales/order')->load($orderID);
        $order->cancel();

        // reload coupon from database
        $coupon->load($couponId);

        $this->assertEquals(
            $coupon->getCouponValue(),
            $coupon->getRemainingValue(),
            'Origin remaining value of coupon is not equal to remaining value after resetting coupon.'
        );
    }

}
