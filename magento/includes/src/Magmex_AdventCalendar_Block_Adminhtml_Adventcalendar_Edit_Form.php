<?php
/**
 * magmex: Form for adding and editing an Advent Calendar
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.magmex.com/MAGMEX_SOFTWARE_LICENSE.txt
 *
 * @copyright Copyright (c) 2015 magmex, Michael Stork, (http://www.magmex.com)
 * @license http://www.magmex.com/MAGMEX_SOFTWARE_LICENSE.txt
 * @category Magmex
 * @package Magmex_AdventCalendar
 * @author Michael Stork <m.stork@magmex.com>
 */
class Magmex_AdventCalendar_Block_Adminhtml_Adventcalendar_Edit_Form extends Mage_Adminhtml_Block_Widget_Form
{
    /**
     * Prepares the form inclusive all form fields, the form action an and the form id
     *
     * @return Magmex_AdventCalendar_Block_Adminhtml_Adventcalendar_Edit_Form chaining
     */
    protected function _prepareForm()
    {
        $adventCalendarId = $this->getRequest()->getParam('id');
        /** @var Magmex_AdventCalendar_Helper_Data $helperData */
        $helperData = Mage::helper('magmex_adventcalendar');;
        /** @var Magmex_AdventCalendar_Helper_Form $helperForm */
        $helperForm = Mage::helper('magmex_adventcalendar/form');
        
        
        $form = new Varien_Data_Form(array(
                'id' => 'edit_form',
                'action' => $this->getUrl('*/*/save', array('id' => $adventCalendarId)),
                'method' => 'post',
                'enctype' => 'multipart/form-data'
            )
        );
        
        $fieldset = $form->addFieldset('magmex_adventcalendar_form_edit_adventcalendar',
            array(
                'legend' => $this->__('Edit Advent Calendar')
            )
        );
        
        $modelAdventCalendar = Mage::getModel('magmex_adventcalendar/adventcalendar');
        $yearToEdit = $modelAdventCalendar->load($adventCalendarId)->getYear();
        
        $fieldset->addField('year', 'select',
            array(
                'name'  => 'year',
                'label' => $this->__('Year'),
                'values' => $helperForm->getAvailabeYearSelectOptions(),
                'required' => true,
                'note' => $helperData->__('Only years that don\'t already have an Advent Calender can be added. '
                        . 'If you want to create a new Advent Calendar for an existing year, delete this calendar first and create a new one.')
            )
        );
        
        $fieldset->addField('title', 'text',
            array(
                'name'  => 'title',
                'label' => $this->__('Title'),
                'required' => false,
                'title' => $helperData->__('The title will be shown on the background image above the doors dependent on the chosen layout')
            )
        );
        
        $noteStandardBackgroundImage = $helperData->__('Please choose a standard background image. If a customized background image was selected, this option is ignored');
        foreach ($helperForm->getAdventCalendarStandarBackgroundImageTypeLinks() as $textAndLink) {
            $noteStandardBackgroundImage .= '<br />' . $helperData->__($textAndLink['text']) . ': ' . $textAndLink['link'];
        }
        
        $fieldset->addField('standard_background_image', 'select',
            array(
                'name'  => 'standard_background_image',
                'label' => $this->__('Standard background image'),
                'values' => $helperForm->getAdventCalendarStandardBackgroundImageTypesOptionArray(),
                'required' => true,
                'note' => $noteStandardBackgroundImage
            )
        );
        
        $fieldset->addField('customized_background_image', 'image',
            array(
                'name'  => 'customized_background_image',
                'label' => $this->__('Customized background image'),
                'required' => false,
                'note' => $helperData->__('Please choose a customized background image in the size of 1100px x 800px with the format JPG or PNG')
            )
        );

        /**
         * Check if it is a single store mode
         */
        if (!Mage::app()->isSingleStoreMode()) {
            $field =$fieldset->addField('store_id', 'select',
                array(
                    'name'      => 'store_id',
                    'label'     => $this->__('Store View'),
                    'title'     => $this->__('Store View'),
                    'required'  => true,
                    'values'    => Mage::getSingleton('adminhtml/system_store')->getStoreValuesForForm(false, false),
                )
            );

            if (version_compare(Mage::getVersion(), '1.5.0.0', '>=')) {
                $renderer = $this->getLayout()->createBlock('adminhtml/store_switcher_form_renderer_fieldset_element');
                $field->setRenderer($renderer);
            }
        }
        else {
            $fieldset->addField('store_id', 'hidden',
                array(
                    'name'      => 'store_id',
                    'value'     => Mage::app()->getStore(true)->getId()
                )
            );
        }
        
        $fieldset->addField('layout_type', 'select',
            array(
                'name'  => 'layout_type',
                'label' => $this->__('Layout type'),
                'values' => $helperForm->getAdventCalendarLayoutTypesOptionArray(Mage::getModel('magmex_adventcalendar/adventcalendar')),
                'required' => true,
                'note' => $helperData->__('Please choose a layout type for the Advent Calendar.<br />'
                        . '\'randomSizeDoors\': days/doors have 4 different sizes.<br />'
                        . '\'sameSizeDoors\': days/doors all have the same size.<br />'
                        . 'With both layout types the day numbers get ordered randomely when generating the Advent Calendar.')
            )
        );
        
        if ($adventCalendarId != 0) {
            $fieldset->addField('randomize_door_positions_and_colors_and_background_images', 'checkbox',
                array(
                    'name'  => 'randomize_door_positions_and_colors_and_background_images',
                    'label' => $this->__('Randomize door day positions, door colors (if selected) and background images (if selected).'),
                    'required' => false,
                    'checked' => false,
                    'onclick'    => 'this.value = this.checked ? 1 : 0;',
                    'note' => '<strong><font style="color: red">' . $helperData->__('If selected, the door day positions, the door colors and the door background images will be randomized '
                                            . 'again after saving. Existing day actions will remain') . '</font></strong>'
                )
            );
        }
        
        $fieldset->addField('show_door_background_images', 'checkbox',
            array(
                'name'  => 'show_door_background_images',
                'label' => $this->__('Show door background images'),
                'required' => false,
                'onclick'    => 'this.value = this.checked ? 1 : 0;',
                'note' => $helperData->__('If selected, the doors will have small Christmassy background images')
            )
        );
        
        $fieldset->addField('main_color', 'text',
            array(
                'name'  => 'main_color',
                'label' => $this->__('Main color'),
                'required' => true,
                'note' => $helperData->__('Used in the open door day action layer. If no color is selected in the input fields below, this color will also be used for all doors. Format e.g. #000000, #CCCCCC')
            )
        );
        
        $fieldset->addField('note', 'note',
            array(
                'text' => $helperData->__('If you change one color, the colors will be ranomized, but the day numbers stay at the same position')
            )
        );
        
        $fieldset->addField('door_color_one', 'text',
            array(
                'name'  => 'door_color_one',
                'label' => $this->__('Door Color 1'),
                'required' => false
            )
        );
        
        $fieldset->addField('door_color_two', 'text',
            array(
                'name'  => 'door_color_two',
                'label' => $this->__('Door Color 2'),
                'required' => false
            )
        );
        
        $fieldset->addField('door_color_three', 'text',
            array(
                'name'  => 'door_color_three',
                'label' => $this->__('Door Color 3'),
                'required' => false
            )
        );
        
        $fieldset->addField('door_color_four', 'text',
            array(
                'name'  => 'door_color_four',
                'label' => $this->__('Door Color 4'),
                'required' => false
            )
        );
        
        $fieldset->addField('door_color_five', 'text',
            array(
                'name'  => 'door_color_five',
                'label' => $this->__('Door Color 5'),
                'required' => false
            )
        );
        
        //set existant values in form
        $adventCalendarData = Mage::registry('magmex_adventcalendar_data');
        $adventCalendarDataAllData = $adventCalendarData->getData();
        //Only set data, if data array is not empty (means, a new advent calendar gets created).
        //Otherwise the store_id hidden field would be overwritten with an empty value
        //in single store Magento configurations
        if (empty($adventCalendarDataAllData) === false) {
            $form->setValues($adventCalendarDataAllData);
        }

        $form->getElement('show_door_background_images')->setIsChecked(!empty($adventCalendarData['show_door_background_images']));
        $form->setUseContainer(true);
        $this->setForm($form);
        return parent::_prepareForm();
    }
}