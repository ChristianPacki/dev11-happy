<?php
class Wpmentor_Dpdshippinglist_Adminhtml_DpdshippinglistbackendController extends Mage_Adminhtml_Controller_Action
{

	protected function _isAllowed()
	{
		//return Mage::getSingleton('admin/session')->isAllowed('dpdshippinglist/dpdshippinglistbackend');
		return true;
	}

	public function indexAction()
    {
       $this->loadLayout();
	   $this->_title($this->__("DPD Shipping List"));
	   $this->renderLayout();
    }
}