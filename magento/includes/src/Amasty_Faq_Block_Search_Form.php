<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_Faq
 */

class Amasty_Faq_Block_Search_Form extends Mage_Core_Block_Template
{
    protected function _construct()
    {
        parent::_construct();
        $this->setTemplate('amasty/amfaq/search/form.phtml');
    }

    public function getSearchText()
    {
        return Mage::helper('core')->escapeHtml($this->getData('search_text'));
    }
}
