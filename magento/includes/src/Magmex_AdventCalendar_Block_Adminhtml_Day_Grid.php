<?php
/**
 * Magmex Adventcalendar day grid block for showing the Advent Calendar days for one Advent Calendar
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.magmex.com/MAGMEX_SOFTWARE_LICENSE.txt
 *
 * @copyright Copyright (c) 2015 magmex, Michael Stork, (http://www.magmex.com)
 * @license http://www.magmex.com/MAGMEX_SOFTWARE_LICENSE.txt
 * @category Magmex
 * @package Magmex_AdventCalendar
 * @author Michael Stork <m.stork@magmex.com>
 */
class Magmex_AdventCalendar_Block_Adminhtml_Day_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    /**
     * Construct and set default options for the main index block
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        
        $this->setId('magmexAdventCalendarDayGrid');
        $this->setDefaultSort('date');
        $this->setDefaultDir('ASC');
        $this->setDefaultLimit(24);
        $this->setSaveParametersInSession(true);
    }
    
    /**
     * Loads the data for the advent calendar days default grid for the given advent_calendar_id
     *
     * @return Magmex_AdventCalendar_Block_Adminhtml_Adventcalendar_Grid chaining
     */
    protected function _prepareCollection()
    {
        $collection = Mage::getModel('magmex_adventcalendar/day')
            ->getCollection()
            ->addFieldToFilter('advent_calendar_id', $this->getParam('id'));
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }
    
    /**
     * Prepares the columns shown in the grid
     *
     * @return Magmex_AdventCalendar_Block_Adminhtml_Day_Grid chaining
     */
    protected function _prepareColumns()
    {
        $helperData = Mage::helper('magmex_adventcalendar');
        
        $this->addColumn('advent_calendar_day_id',
            array(
                    'header' => $helperData->__('ID'),
                    'align' => 'right',
                    'width' => '20px',
                    'index' => 'advent_calendar_day_id',
            )
        );
    
        $this->addColumn('date',
            array(
                    'header' => $helperData->__('Date'),
                    'align' => 'left',
                    'type' => 'date',
                    'index' => 'date'
            )
        );
        
        $this->addColumn('action_type',
                array(
                        'header' => $helperData->__('Action type'),
                        'align' => 'left',
                        'index' => 'action_type'
                )
        );
        
        $this->addColumn('edit_day',
            array(
                'header' => $helperData->__('Edit day'),
                'width' => '100',
                'align' => 'center',
                'type' => 'action',
                'getter' => 'getId',
                'actions' => array(
                    array(
                            'caption' => $helperData->__('Edit day'),
                            'url' => array( 'base' => '*/*/edit' ),
                            'field' => 'id'
                    )
                ),
                'filter' => false,
                'sortable' => false,
                'index' => 'stores',
                'is_system' => true,
            )
        );
    
        return parent::_prepareColumns();
    }
    
    /**
     * Return the url for clicking on a row action
     *
     * @param object row instance
     * @return string url string
     */
    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }
}