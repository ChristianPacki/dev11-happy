<?php

/**
 * @category Modulwerft
 * @package Modulwerft_CouponRemainingValue
 * @author Modulwerft GmbH <info@modulwerft.com>
 * @author Benjamin Wunderlich <b.wunderlich@shopwerft.com>
 * @copyright 2014 Shopwerft GmbH (http://www.shopwerft.com)
 */
class Modulwerft_CouponRemainingValue_Model_Resource_History
    extends Mage_Core_Model_Mysql4_Abstract
{

    /**
     * Constructor
     */
    protected function _construct()
    {
        $this->_init('modulwerft_couponremainingvalue/history', 'use_id');
    }

    /**
     * @param Mage_Core_Model_Abstract $object
     * @return $this
     */
    protected function _beforeSave(Mage_Core_Model_Abstract $object)
    {
        parent::_beforeSave($object);

        if ($object->isObjectNew()) {
            /** @var Mage_Core_Model_Date $dateModel */
            $dateModel = Mage::getModel('core/date');
            $object->setData('used_at', $this->formatDate($dateModel->gmtDate()));
        }

        return $this;
    }

}
