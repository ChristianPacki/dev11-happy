<?php

/**
 * @category Modulwerft
 * @package Modulwerft_CouponRemainingValue
 * @author Modulwerft GmbH <info@modulwerft.com>
 * @author Benjamin Wunderlich <b.wunderlich@shopwerft.com>
 * @copyright 2014 Shopwerft GmbH (http://www.shopwerft.com)
 */

/**
 * @method Mage_Sales_Model_Order getOrder()
 * @method Modulwerft_CouponRemainingValue_Model_Notification setOrder(Mage_Sales_Model_Order $order)
 * @method Mage_SalesRule_Model_Rule getRule()
 * @method Modulwerft_CouponRemainingValue_Model_Notification setRule(Mage_SalesRule_Model_Rule $rule)
 * @method Modulwerft_CouponRemainingValue_Model_History getCouponHistory()
 * @method Modulwerft_CouponRemainingValue_Model_Notification setCouponHistory(Modulwerft_CouponRemainingValue_Model_History $history)
 * @method Mage_Sales_Model_Order_Creditmemo getCreditmemo()
 * @method Modulwerft_CouponRemainingValue_Model_Notification setCreditmemo(Mage_Sales_Model_Order_Creditmemo $creditmemo)
 */
class Modulwerft_CouponRemainingValue_Model_Notification
    extends Mage_Core_Model_Abstract
{

    /** @var string */
    protected $_eventPrefix = 'modulwerft_couponremainingvalue_notification';

    /** @var string */
    protected $_eventObject = 'notification';

    /**
     * Send a notification to the customer to inform about the coupon remaining value
     *
     * @return $this
     * @throws Exception
     */
    public function send()
    {
        $order = $this->getOrder();
        if (!$order instanceof Mage_Sales_Model_Order) {
            Mage::throwException('Sending notification for remaining value to customer failed: order not set');
        }

        $rule = $this->getRule();
        if (!$rule instanceof Mage_SalesRule_Model_Rule) {
            Mage::throwException('Sending notification for remaining value to customer failed: sales rule not set');
        }

        $history = $this->getCouponHistory();
        if (!$history instanceof Modulwerft_CouponRemainingValue_Model_History) {
            Mage::throwException('Sending notification for remaining value to customer failed: coupon history not set');
        }

        /** @var Modulwerft_CouponRemainingValue_Helper_Config $helperConfig */
        $helperConfig = Mage::helper('modulwerft_couponremainingvalue/config');

        /** @var Modulwerft_CouponRemainingValue_Helper_Log $helperLog */
        $helperLog = Mage::helper('modulwerft_couponremainingvalue/log');

        /** @var $store Mage_Core_Model_Store */
        $store = $order->getStore();
        $storeId = ($store instanceof Mage_Core_Model_Store) ? $store->getId() : null;

        // decide which email template to use depending on used and left value
        if ($history->getUsedValue() >= 0) {
            if ($history->getLeftValue() > 0) {
                if (!$helperConfig->isCustomerNotificationActive($storeId)) {
                    $helperLog->log('Email notification for used coupon value is deactivated');
                    return $this;
                }
                $emailTemplate = $helperConfig->getCustomerNotificationEmailTemplate($storeId);
                $emailBcc = $helperConfig->getCustomerNotificationEmailBcc($storeId);
            } else {
                if (!$helperConfig->isEmailEmptyCouponActive($storeId)) {
                    $helperLog->log('Email notification for empty coupon is deactivated');
                    return $this;
                }
                $emailTemplate = $helperConfig->getEmailTemplateEmptyCoupon($storeId);
                $emailBcc = $helperConfig->getEmailBccEmptyCoupon($storeId);
            }
        } else {
            if (!$helperConfig->isEmailResetCouponValueActive($storeId)) {
                $helperLog->log('Email notification for reset coupon value is deactivated');
                return $this;
            }
            $emailTemplate = $helperConfig->getEmailTemplateResetCouponValue($storeId);
            $emailBcc = $helperConfig->getEmailBccResetCouponValue($storeId);
        }

        // check whether an email template was set
        if (is_null($emailTemplate)) {
            $helperLog->log('No email template is configured for notification of coupon remaining value');
            return $this;
        }

        /** @var $email Mage_Core_Model_Email_Template */
        $email = Mage::getModel('core/email_template');

        // add bcc
        if (count($emailBcc) > 0) {
            $email->addBcc($emailBcc);
        }

        // set design config to frontend area
        $email->setDesignConfig(
            array(
                'area' => 'frontend',
                'store' => $storeId
            )
        );

	    // start store emulation process
	    if (version_compare(Mage::getVersion(), '1.5.0.0', '>=')) {
		    $appEmulation = Mage::getSingleton('core/app_emulation');
		    $initialEnvironmentInfo = $appEmulation->startEnvironmentEmulation($storeId);
	    }

        // prepare event params and dispatch event
        $params = array(
            'order'          => $order,
            'rule'           => $rule,
            'history'        => $history, /** @deprecated */
            'coupon_history' => $history,
            'mail'           => $email,
            'creditmemo'     => $this->getCreditmemo(),
        );
        Mage::dispatchEvent($this->_eventPrefix . '_send_before', $params);
        Mage::dispatchEvent($this->_eventPrefix . '_notification_send_before', $params); /** @deprecated */

        if ($order->getCustomerIsGuest()) {
            $customerName = $order->getBillingAddress()->getName();
        } else {
            $customerName = $order->getCustomerName();
        }

	    $createdAtStoreDate = $order->getCreatedAtStoreDate();

        // send transactional email
        $email->sendTransactional(
            $emailTemplate,
            'general',
            $order->getCustomerEmail(),
            $customerName,
            array(
                'order'                  => $order,
                'customer_name'          => $customerName,
                'rule'                   => $rule,
                'history'                => $history,
                'store'                  => $store,
                'coupon_value'           => $order->formatPrice($rule->getData('coupon_value')),
                'coupon_used_value'      => $order->formatPrice($history->getUsedValue()),
                'coupon_remaining_value' => $order->formatPrice($history->getLeftValue()),
                'couponcode'             => $order->getCouponCode(),
                'order_date'             => $createdAtStoreDate->toString(Zend_Date::DATES),
                'order_time'             => $createdAtStoreDate->toString(Zend_Date::TIMES),
            ),
	        $storeId
        );

        // check whether sending was successful
        if (!$email->getSentSuccess()) {
            $errMsg = 'Could not send transactional mail for customer notification';

            $helperLog->log($errMsg, Zend_Log::WARN);
            Mage::throwException($errMsg);
        }

        // dispatch event
        $params = array(
            'order'          => $order,
            'rule'           => $rule,
            'history'        => $history, /** @deprecated */
            'coupon_history' => $history,
            'mail'           => $email,
            'creditmemo'     => $this->getCreditmemo(),
        );
        Mage::dispatchEvent($this->_eventPrefix . '_send_after', $params);
        Mage::dispatchEvent($this->_eventPrefix . '_notification_send_after', $params); /** @deprecated */

	    // stop emulation
	    if (version_compare(Mage::getVersion(), '1.5.0.0', '>=')) {
	        $appEmulation->stopEnvironmentEmulation($initialEnvironmentInfo);
	    }

        return $this;
    }

}
