<?php

/**
 * @category	Mage
 * @package	Ricordo_Anrede
 * @developer	Cajetan Oberhaus (cajetan.oberhaus@gmail.com)
 * @version	0.2
 * @license	http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

class Ricordo_Anrede_Model_Sales_Order extends Mage_Sales_Model_Order
{
    public function getCustomerNameFormal()
    {
	$customerNameFormal = '';
	if ($this->getCustomerIsGuest()) {
	    
	    if ($this->getBillingAddress()->getPrefix()) {
		if ($this->getBillingAddress()->getPrefix()=='Frau') {
		    $customerNameFormal .= 'liebe '. $this->getBillingAddress()->getPrefix() . ' ';
		}
		if ($this->getBillingAddress()->getPrefix()=='Herr') {
		    $customerNameFormal .= 'lieber '. $this->getBillingAddress()->getPrefix() . ' ';
		}
		if ($this->getBillingAddress()->getPrefix()=='Familie') {
		    $customerNameFormal .= 'liebe '. $this->getBillingAddress()->getPrefix() . ' ';
		}
	    $customerNameFormal .=  ' ' . $this->getBillingAddress()->getLastname();
		if ($this->getBillingAddress()->getSuffix()) {
		    $customerNameFormal .= ' ' . $this->getBillingAddress()->getSuffix();
		}
	    }
	} else {
	    if ($this->getCustomerPrefix()) {
		if ($this->getCustomerPrefix()=='Frau') {
		    $customerNameFormal .= 'liebe '. $this->getCustomerPrefix() . ' ';
		}
		if ($this->getCustomerPrefix()=='Herr') {
		    $customerNameFormal .= 'lieber '. $this->getCustomerPrefix() . ' ';
		}
		if ($this->getCustomerPrefix()=='Familie') {
		    $customerNameFormal .= 'liebe '. $this->getCustomerPrefix() . ' ';
		}
	    $customerNameFormal .=  ' ' . $this->getCustomerLastname();
		if ($this->getCustomerSuffix()) {
		    $customerNameFormal .= ' ' . $this->getCustomerSuffix();
		}
	    }
	}
	return $customerNameFormal;
    }

}