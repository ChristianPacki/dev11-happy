<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_Faq
 */

class Amasty_Faq_Model_Config_Source_Sort
{
    public function toOptionArray()
    {
        return array(
            array('value' => 'title', 'label' => Mage::helper('amfaq')->__('Name')),
            array('value' => 'position', 'label' => Mage::helper('amfaq')->__('Position')),
        );
    }
}
