<?php
/**
 * Magmex: Edit form for editing the day action cms block
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.magmex.com/MAGMEX_SOFTWARE_LICENSE.txt
 *
 * @copyright Copyright (c) 2015 magmex, Michael Stork, (http://www.magmex.com)
 * @license http://www.magmex.com/MAGMEX_SOFTWARE_LICENSE.txt
 * @category Magmex
 * @package Magmex_AdventCalendar
 * @author Michael Stork <m.stork@magmex.com>
 */
class Magmex_AdventCalendar_Block_Adminhtml_Day_Action_Cmsblock_Edit_Form extends Magmex_AdventCalendar_Block_Adminhtml_Day_Action_EditFormAbstract
{
    /**
     * Prepares the form inclusive all form fields, the form action and the form id
     *
     * @return Magmex_AdventCalendar_Block_Adminhtml_Adventcalendar_Edit_Form chaining
     */
    protected function _prepareForm()
    {
        $adventCalenderDayId = $this->getRequest()->getParam('id');
        $helperData = Mage::helper('magmex_adventcalendar');
        $modelDayAction = Mage::registry('magmex_adventcalendar_day_action_data');
        
        $form = new Varien_Data_Form(array(
                'id' => 'edit_form',
                'action' => $this->getUrl('*/*/save', array('id' => $adventCalenderDayId)),
                'method' => 'post',
                'enctype' => 'multipart/form-data'
            )
        );
        
        $fieldset = $form->addFieldset(self::FIELDSET_ID,
            array(
                'legend' => $this->__('Edit Advent Calendar day with action CMS Block')
            )
        );
        
        /** @var Magmex_AdventCalendar_Model_Adventcalendar $adventCalendar*/
        $adventCalendar = Mage::getModel('magmex_adventcalendar/adventcalendar')->loadAdventCalendarByDayId($adventCalenderDayId);
        $storeName = Mage::app()->getStore($adventCalendar->getStoreId())->getName();
        
        $fieldset->addField('cms_block_id', 'select',
            array(
                'name'  => 'cms_block_id',
                'label' => $this->__('CMS Block'),
                'values'=> Mage::helper('magmex_adventcalendar/form')->getCmsblockOptions($adventCalendar->getStoreId()),
                'required' => true,
                'note' => Mage::helper('magmex_adventcalendar')->__('You can just choose store specific CMS Blocks for the defined store view in the Advent Calendar Configuration')
                        . ' (' . $storeName . ')'
            )
        );
        
        //set existant values in form
        $form->setValues($modelDayAction->getData());
        $form->setUseContainer(true);
        $this->setForm($form);
        return parent::_prepareForm();
    }
}