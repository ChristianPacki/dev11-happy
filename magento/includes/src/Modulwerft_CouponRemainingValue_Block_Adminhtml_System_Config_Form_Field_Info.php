<?php

/**
 * @category Modulwerft
 * @package Modulwerft_CouponRemainingValue
 * @author Modulwerft <info@modulwerft.com>
 * @author Benjamin Wunderlich <b.wunderlich@shopwerft.com>
 * @copyright 2014 Shopwerft GmbH (http://www.shopwerft.com)
 */
class Modulwerft_CouponRemainingValue_Block_Adminhtml_System_Config_Form_Field_Info
    extends Mage_Adminhtml_Block_Abstract
    implements Varien_Data_Form_Element_Renderer_Interface
{

    const URL = 'http://www.modulwerft.com/';

    /**
     * @param Varien_Data_Form_Element_Abstract $element
     * @return string
     */
    public function render(Varien_Data_Form_Element_Abstract $element)
    {
        $imagesPath = str_replace('_', '/', mb_strtolower($this->getModuleName()));
        $logoSrc = $this->getSkinUrl('images/' . $imagesPath . '/modulwerft_logo.png');

        $html = '
<tr id="row_%s">
    <td colspan="2">
        <div class="box">
            <p>
                <a href="' . self::URL . '" target="_blank" title="' . $this->__('Go to Modulwerft Website') . '">
                    <img src="' . $logoSrc . '" alt="' . $this->__('Modulwerft') . '" />
                </a>
            </p>
            <p>%s</p>
            <p><strong>%s %s</strong></p>
            <ul>%s</ul>
        </div>
    </td>
</tr>
';

        $linksHtml = '';
        $fieldConfig = $element->getFieldConfig();

        /** @var $links Mage_Core_Model_Config_Element */
        $links = $fieldConfig->links;
        if ($links) {
            foreach ($links->children() as $_link) {
                $_linkLabel = $this->__((string)$_link->label);
                $linksHtml .= sprintf('<li><a href="%s" target="_blank">%s</a>', $_link->url, $_linkLabel) . '</li>';
            }
        }

        /** @var Modulwerft_CouponRemainingValue_Helper_Common_Meta $metaHelper */
        $metaHelper = Mage::helper('modulwerft_couponremainingvalue/common_meta');

        $version = $metaHelper->getModuleVersion();
        $versionText = $this->__('Installed Version: %s', $version);

        $locale = Mage::app()->getLocale()->getLocaleCode();
        $updateUrl = $metaHelper->getCheckUpdateUrl($locale);

        return sprintf($html, $element->getHtmlId(), $element->getComment(), $versionText, $updateUrl, $linksHtml);
    }

}
