<?php

class Valkanis_Datevproduct_Block_Config_Adminhtml_Form_Field_TextArea extends Mage_Core_Block_Abstract {

    /**
     * Set element's HTML ID
     *
     * @param string $id ID
     * @return Mage_Core_Block_Html_Select
     */
    public function setId($id) {

        $this->setData('id', $id);
        return $this;
    }

    /**
     * Set element's CSS class
     *
     * @param string $class Class
     * @return Mage_Core_Block_Html_Select
     */
    public function setClass($class) {
        $this->setData('class', $class);
        return $this;
    }

    /**
     * Set element's HTML title
     *
     * @param string $title Title
     * @return Mage_Core_Block_Html_Select
     */
    public function setTitle($title) {
        $this->setData('title', $title);
        return $this;
    }

    /**
     * HTML ID of the element
     *
     * @return string
     */
    public function getId() {
        return $this->getData('id');
    }

    /**
     * CSS class of the element
     *
     * @return string
     */
    public function getClass() {
        return $this->getData('class');
    }

    /**
     * Returns HTML title of the element
     *
     * @return string
     */
    public function getTitle() {
        return $this->getData('title');
    }

    /**
     * Render HTML
     *
     * @return string
     */
    protected function _toHtml() {
        if (!$this->_beforeToHtml()) {
            return '';
        }

        $html = '<textarea name="' . $this->getName() . '" id="' . $this->getId() . '" class="'
                . $this->getClass() . '" title="' . $this->getTitle() . '" ' . $this->getExtraParams() . '>' . $this->getValue() . '</textarea>';

//        var_dump($html);
//        var_dump($this->getOptions());
//        die();

        return $html;
    }

    /**
     * Alias for toHtml()
     *
     * @return string
     */
    public function getHtml() {

        return $this->toHtml();
    }

    public function setInputName($value) {
        return $this->setName($value);
    }

}
