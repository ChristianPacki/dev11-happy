<?php 
/**
 * 888888ba                 dP  .88888.                    dP                
 * 88    `8b                88 d8'   `88                   88                
 * 88aaaa8P' .d8888b. .d888b88 88        .d8888b. .d8888b. 88  .dP  .d8888b. 
 * 88   `8b. 88ooood8 88'  `88 88   YP88 88ooood8 88'  `"" 88888"   88'  `88 
 * 88     88 88.  ... 88.  .88 Y8.   .88 88.  ... 88.  ... 88  `8b. 88.  .88 
 * dP     dP `88888P' `88888P8  `88888'  `88888P' `88888P' dP   `YP `88888P' 
 *
 *                          m a g n a l i s t e r
 *                                      boost your Online-Shop
 *
 * -----------------------------------------------------------------------------
 * $Id$
 *
 * (c) 2010 - 2014 RedGecko GmbH -- http://www.redgecko.de
 *     Released under the MIT License (Expat)
 * -----------------------------------------------------------------------------
 */
class_exists('ML', false) or die();
?>
<?php if (
    isset($aField['hiddenifdisabled']) && $aField['hiddenifdisabled']
    && isset($aField['disabled']) && $aField['disabled']
) { 
    $aField['type'] = 'hidden';
    $this->includeType($aField);   
}
if (isset($aField['realname']) && in_array($aField['realname'], array('prepareaction', 'saveaction'))) {
    $sCssClassAdd = ' action';
} else {
    $sCssClassAdd = '';
}
?>
<button type="submit" value="1" id="<?php echo $aField['id'] ?>" class="mlbtn<?php echo $sCssClassAdd; ?>" 
        name="<?php echo MLHttp::gi()->parseFormFieldName($aField['name'])?>"
        <?php echo ((isset($aField['disabled']) && $aField['disabled']) ? ' disabled="disabled"' : '') ?>>
    <?php echo $aField['i18n']['label']?>
</button>

<?php if ($aField['id'] === 'cdiscount_prepare_variations_field_saveaction' || $aField['id'] === 'cdiscount_prepare_apply_form_field_prepareaction') :?>
<div id="infodiagmandatory" class="ml-modal dialog2" title="Hinweis"></div>
<span id="mandatoryfieldsinfo" style="display: none"><?php echo MLI18n::gi()->get('cdiscount_prepare_variations_mandatory_fields_popup') ?></span>
<?php
    if ($aField['id'] === 'cdiscount_prepare_variations_field_saveaction') {
        $formId = 'cdiscount_prepare_variations';
        $buttonId = 'cdiscount_prepare_variations_field_saveaction[value="1"]';
        $action = 'saveaction';
    } else {
        $formId = 'cdiscount_prepare_apply_form';
        $buttonId = 'cdiscount_prepare_apply_form_field_prepareaction[value="1"]';
        $action = 'prepareaction';
    }
?>
<input type="hidden" name="ml[action][<?php echo $action?>]" disabled="disabled">
<script type="text/javascript">
    (function($) {
        $(document).ready(function() {
            var isSafari = /^((?!chrome).)*safari/i.test(navigator.userAgent);
            $('#<?php echo $buttonId?>').unbind('click');
            $('#<?php echo $buttonId?>')[0].onclick = function (e) {
                var form = $('#<?php echo $formId?>');
                var d = $('#mandatoryfieldsinfo').html();
                $('#infodiagmandatory').html(d).jDialog({
                    width: (d.length > 1000) ? '700px' : '500px',
                    buttons: {
                        'ABBRECHEN': function() {
                            $(this).dialog('close');
                            return false;
                        },
                        'OK': function() {
                            $(this).dialog('close');
                            $('input[name="ml[action][<?php echo $action?>]"]').removeAttr('disabled');
                            $('input[name="ml[action][<?php echo $action?>]"]').val('1');
                            var button = $('#<?php echo $buttonId?>');
                            if (isSafari) {
                                e.preventDefault();
                                var tehForm = $(button).parents('form'),
                                    btnName = $(button).attr('name') || '';
                                // Pass the information which button has been pressed. For some forms it is important
                                if (btnName != '') {
                                    tehForm.append($('<input>').attr({
                                        'type': 'hidden',
                                        'name': btnName,
                                        'value': $(button).attr('value') || ''
                                    }));
                                }
                                $.blockUI(jQuery.extend(blockUILoading, {
                                    onBlock: function () {
                                        //console.log('Submit');
                                        tehForm.submit();
                                    }
                                }));
                                return false;
                            } else {
                                setTimeout(function() { $.blockUI(blockUILoading); }, 1000);
                                form.submit();
                                return true;
                            }
                        }
                    }
                });

                return false;
            };
        });
    })(jqml);
</script>
<?php endif; ?>