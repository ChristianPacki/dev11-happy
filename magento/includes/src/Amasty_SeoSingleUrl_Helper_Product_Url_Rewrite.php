<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_SeoSingleUrl
 */


class Amasty_SeoSingleUrl_Helper_Product_Url_Rewrite
    extends Amasty_SeoSingleUrl_Helper_Product_Url_Rewrite_Pure
{
    protected $_productUrlCache = array();

    protected $_enabledCategory = array();

    /**
     * Adapter instance
     *
     * @var Varien_Db_Adapter_Interface
     */
    protected $_connection;

    /**
     * Resource instance
     *
     * @var Mage_Core_Model_Resource
     */
    protected $_resource;

    /**
     * @var bool
     */
    protected $_useDefaultUrlSettings;

    /**
     * @var array
     */
    protected $_notActiveCategories = null;

    /**
     * Initialize resource and connection instances
     *
     * @param array $args
     */
    public function __construct(array $args = array())
    {
        $this->_resource = Mage::getSingleton('core/resource');
        $this->_connection = !empty($args['connection']) ? $args['connection'] : $this->_resource
            ->getConnection(Mage_Core_Model_Resource::DEFAULT_READ_RESOURCE);

        $this->_useDefaultUrlSettings = Mage::helper('amseourl')->useDefaultProductUrlRules();

    }

    protected function generateNotActiveCategories()
    {
        if ($this->_notActiveCategories === null) {
            $this->_notActiveCategories = array();
            $collection = Mage::getModel('catalog/category')->getCollection()
                ->addAttributeToSelect('url_key')
                ->addAttributeToFilter('is_active', 0);

            foreach ($collection as $category) {
                $this->_notActiveCategories[] = $category->getUrlKey();
            }
        }
    }

    /**
     * Prepare and return select
     *
     * @param array $productIds
     * @param int $categoryId
     * @param int $storeId
     * @param array $items
     *
     * @return Varien_Db_Select|Varien_Db_Select|null
     */
    public function getTableSelect(
        array $productIds,
        $categoryId,
        $storeId,
        $items = array('product_id', 'request_path')
    ) {
        if ($this->_useDefaultUrlSettings) {
            if (method_exists(get_parent_class($this), 'getTableSelect')) {
                return parent::getTableSelect($productIds, $categoryId, $storeId);
            } else {
                //code from magento 1.7.0.2
                return  $select = $this->_connection->select()
                    ->from($this->_resource->getTableName('core/url_rewrite'), $items)
                    ->where('store_id = ?', Mage::app()->getStore()->getId())
                    ->where('is_system = ?', 1)
                    ->where('category_id = ? OR category_id IS NULL', $categoryId)
                    ->where('product_id IN(?)', $productIds)
                    ->order('category_id ' . 'DESC');
            }
        }

        $storeId = Mage::registry('amseourl_store_id') ?: $storeId;
        /*$nestedQuery = $this->_getNestedQuery($storeId, $this->_resource->getTableName('core/url_rewrite'), 'product_id');*/

        $select = $this->_connection->select()
            ->from($this->_resource->getTableName('core/url_rewrite'), $items)
            ->where('store_id = ?', (int) $storeId)
            ->where('is_system = ?', 1)
            ->where('category_id IS NOT NULL')
            ->where('product_id IN(?)', $productIds)
            ->order('LENGTH(request_path) ' . $this->_getSortOrder());

        Mage::unregister('amseourl_store_id');

        return $select;
    }

    /**
     * @param Mage_Catalog_Model_Product $product
     *
     * @return string
     */
    public function getProductPath(Mage_Catalog_Model_Product $product)
    {
        if ($this->_useDefaultUrlSettings) {
            return null;
        }

        if (! isset($this->_productUrlCache[$product->getId()])) {
            $resultUrl = '';

            $select = $this->getTableSelect(
                array($product->getId()),
                null,
                Mage::app()->getStore()->getId(),
                array('request_path')
            );

            $collection = $this->_connection->fetchAll($select);
            if (count($collection) > 1) {
                $this->generateNotActiveCategories();
                $resultUrl = $collection[0]['request_path'];

                foreach ($collection as $item) {
                    $categoriesUrlKey = explode('/', $item['request_path'], -1);

                    //check if url don't contain disabled categories
                    if (count(array_uintersect($categoriesUrlKey, $this->_notActiveCategories, "strcasecmp")) == 0) {
                        $resultUrl = $item['request_path'];
                        break;
                    }
                }
            }

            $this->_productUrlCache[$product->getId()] = $resultUrl;
        }

        return $this->_productUrlCache[$product->getId()];
    }

    public function flushProductUrlCache()
    {
        $this->_productUrlCache = array();
    }

    /**
     * Prepare url rewrite left join statement for given select instance and store_id parameter.
     *
     * @param Varien_Db_Select $select
     * @param int $storeId
     * @return Mage_Catalog_Helper_Product_Url_Rewrite_Interface
     */
    public function joinTableToSelect(Varien_Db_Select $select, $storeId)
    {
        if ($this->_useDefaultUrlSettings) {
            return parent::joinTableToSelect($select, $storeId);
        }

        $rewriteIds = $this->_connection->fetchCol($this->_getNestedQuery($storeId));

        $select->joinLeft(
            array('url_rewrite' => $this->_resource->getTableName('core/url_rewrite')),
            'url_rewrite.product_id = main_table.entity_id AND ' .
            $this->_connection->quoteInto('url_rewrite.url_rewrite_id IN (?)', $rewriteIds),
            array('request_path' => 'url_rewrite.request_path'));
        return $this;
    }

    /**
     * @param $storeId
     * @param array $productIds
     * @return Varien_Db_Select
     */
    protected function _getNestedQuery($storeId, $productIds = array())
    {
        $nestedQuery = $this->_connection->select()
            ->from(array('nested' => $this->_resource->getTableName('core/url_rewrite')), array('url_rewrite_id', 'product_id'))
            ->where('nested.store_id = ?', (int) $storeId)
            ->where('nested.is_system = ?', 1)
            ->where('nested.category_id IS NOT NULL')
            ->where('nested.product_id IS NOT NULL')
            /*->where('att.value = 1 OR cce.entity_id IS NULL')*/
            ->order('LENGTH(nested.request_path) ' . $this->_getSortOrder());

        if (! empty($productIds)) {
            $nestedQuery->where('nested.product_id IN(?)', $productIds);
        }

        $mainQuery = $this->_connection->select()
            ->from(new Zend_Db_Expr('(' . $nestedQuery . ')'), 'url_rewrite_id')
            ->group('product_id');

        return $mainQuery;
    }

    protected function _getSortOrder()
    {
        $urlFormat = Mage::helper('amseourl')->getProductUrlType();
        return $urlFormat == Amasty_SeoSingleUrl_Helper_Data::PRODUCT_URL_PATH_SHORTEST
            ? Varien_Data_Collection::SORT_ORDER_ASC
            : Varien_Data_Collection::SORT_ORDER_DESC;
    }
}
