<?php
/**
 * Magmex: Edit the Advent Calendars days
 * 24 days per year are possible
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.magmex.com/MAGMEX_SOFTWARE_LICENSE.txt
 *
 * @copyright Copyright (c) 2015 magmex, Michael Stork, (http://www.magmex.com)
 * @license http://www.magmex.com/MAGMEX_SOFTWARE_LICENSE.txt
 * @category Magmex
 * @package Magmex_AdventCalendar
 * @author Michael Stork <m.stork@magmex.com>
 */
class Magmex_AdventCalendar_Block_Adminhtml_Day_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    /**
     * Construct
     * Define all preferences of the Edit Block plus the form child block
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
                 
        $this->_objectId = 'advent_calendar_day_id';
        $this->_blockGroup = 'magmex_adventcalendar';
        $this->_controller = 'adminhtml_day';
        
        $helperData = Mage::helper('magmex_adventcalendar');
        
        $this->_updateButton('save', 'label', $helperData->__('Save Advent Calendar day and configure day action'));
        $this->removeButton('delete');
	
        $this->_formScripts[] = "
            function toggleEditor() {
                if (tinyMCE.getInstanceById('form_content') == null) {
                    tinyMCE.execCommand('mceAddControl', false, 'edit_form');
                } else {
                    tinyMCE.execCommand('mceRemoveControl', false, 'edit_form');
                }
            }
        ";
    }
    
    /**
     * Sets header text according to selected action (new or edit)
     *
     * @see Mage_Adminhtml_Block_Widget_Container::getHeaderText()
     * @return string header text
     */
    public function getHeaderText()
    {
        return Mage::helper('magmex_adventcalendar')->__('Edit Advent Calendar day');
    }
}