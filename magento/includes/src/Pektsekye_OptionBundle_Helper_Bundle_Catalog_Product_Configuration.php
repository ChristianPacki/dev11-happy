<?php

class Pektsekye_OptionBundle_Helper_Bundle_Catalog_Product_Configuration extends Mage_Bundle_Helper_Catalog_Product_Configuration
    implements Mage_Catalog_Helper_Product_Configuration_Interface
{

    /**
     * Get bundled selections (slections-products collection)
     *
     * Returns array of options objects.
     * Each option object will contain array of selections objects
     *
     * @return array
     */
    public function getBundleOptions(Mage_Catalog_Model_Product_Configuration_Item_Interface $item)
    {
        $options = array();
        $product = $item->getProduct();

        /**
         * @var Mage_Bundle_Model_Product_Type
         */
        $typeInstance = $product->getTypeInstance(true);

        // get bundle options
        $optionsQuoteItemOption = $item->getOptionByCode('bundle_option_ids');
        $bundleOptionsIds = $optionsQuoteItemOption ? unserialize($optionsQuoteItemOption->getValue()) : array();
        if ($bundleOptionsIds) {
            /**
            * @var Mage_Bundle_Model_Mysql4_Option_Collection
            */
            $optionsCollection = $typeInstance->getOptionsByIds($bundleOptionsIds, $product);

            // get and add bundle selections collection
            $selectionsQuoteItemOption = $item->getOptionByCode('bundle_selection_ids');

            $selectionsCollection = $typeInstance->getSelectionsByIds(
                unserialize($selectionsQuoteItemOption->getValue()),
                $product
            );
            
          $canDisplayImages = $this->canDisplayOptionImages();
          $image = '';
          $obOptions = Mage::getModel('optionbundle/boption')->getBoptions($product->getId(), 0);			                                 
          $obValues  = Mage::getModel('optionbundle/bvalue')->getValues($product->getId(), 0);          

            $bundleOptions = $optionsCollection->appendSelections($selectionsCollection, true);
            foreach ($bundleOptions as $bundleOption) {
            
                if (!$bundleOption->getSelections()) {
                    continue;
                }
                
                $option = array(
                    'label' => $bundleOption->getTitle(),
                    'value' => array()
                );
                
                $id = (int) $bundleOption->getId();                
                $useProductImage = isset($obOptions[$id]['use_product_image']) ? $obOptions[$id]['use_product_image'] : 1;
                
                $bundleSelections = $bundleOption->getSelections();

                foreach ($bundleSelections as $bundleSelection) {
                    $qty = $this->getSelectionQty($product, $bundleSelection->getSelectionId()) * 1;
                    
                    $valueId = (int) $bundleSelection->getSelectionId();
                    if ($canDisplayImages){
                                        
                      if ($useProductImage == 1){
                        $productModel = Mage::getModel('catalog/product')->load($bundleSelection->getProductId());                      
                        $image = $productModel->getImage() != 'no_selection' ? (string) $productModel->getImage() : '';                                            
                      } else {                                      
                        $image = isset($obValues[$valueId]['image']) ? $obValues[$valueId]['image'] : '';
                      } 
                      if (!empty($image))		
                        $image = $this->makeImage($image);                         
                    }  
                      
                    if ($qty) {
                        $option['value'][] = $qty . ' x ' . $this->escapeHtml($bundleSelection->getName())
                            . ' ' . Mage::helper('core')->currency(
                                $this->getSelectionFinalPrice($item, $bundleSelection)
                            ) . $image;
                    }
                }

                if ($option['value']) {
                    $options[] = $option;
                }
            
            }
        }
        
        return $options;
    }
 

    public function makeImage($image)
    {    						
			$url = Mage::helper('catalog/image')->init(Mage::getModel('catalog/product'), 'thumbnail', $image)->resize(45,45)->__toString();
			return  '<img src="'.$url.'" style="vertical-align:middle;margin:5px;display:inline;">';
    }
    

     public function canDisplayOptionImages()
    {	
      $request = Mage::app()->getRequest();
      $path = $request->getModuleName() .'_'. $request->getControllerName() .'_'. $request->getActionName(); 
         	 
      return Mage::getStoreConfig('checkout/cart/custom_option_images') == 1 && $path == 'checkout_cart_index';
    }
    
    

//to apply sort order only for bulndle product type on the front-end shopping cart page, onepage checkout page and wishlist page
    public function getOptions(Mage_Catalog_Model_Product_Configuration_Item_Interface $item)
    {
      $options = parent::getOptions($item);
      
      $options = Mage::helper('optionbundle')->applySortOrder($item->getProductId(), $options);

      return $options;
    }    	 

}
