<?php
class Pektsekye_OptionBundle_Model_Convert_Parser_Relation extends Mage_Dataflow_Model_Convert_Parser_Csv
{

   public function unparse()
  {
    $io = $this->getBatchModel()->getIoAdapter();
    $io->open();
  
    $optionIds = Mage::getModel('optionbundle/relation')->getUsedOptionIds();      
    $io->write($this->getCsvString(array('option_ids', Zend_Json::encode($optionIds))));
    
    $productSkus = Mage::getResourceModel('optionbundle/relation')->getUsedPoductSkus();      
    foreach($productSkus as $productId => $productSku){               
      $data = array( 
        'boptions'   => Mage::getModel('optionbundle/boption')->getBoptionsAllStores($productId),
        'bvalues'    => Mage::getModel('optionbundle/bvalue')->getBvaluesAllStores($productId),        
        'options'    => Mage::getModel('optionbundle/option')->getOptionsAllStores($productId),
        'values'     => Mage::getModel('optionbundle/value')->getValuesAllStores($productId),                             
        'relations'  => Mage::getModel('optionbundle/relation')->getRelations($productId) 
      );             
      $io->write($this->getCsvString(array($productSku, Zend_Json::encode($data))));	      
    }

    $io->close();
    
    return $this;
  }

}
