<?php
class Redgecko_Magnalister_Adminhtml_MagnalisterController extends Mage_Adminhtml_Controller_Action {
    
    public function indexAction(){
        $this->loadLayout()->_setActiveMenu('magnalister');
        $this->renderLayout();
    }
    
    protected function _isAllowed() {
        return Mage::getSingleton('admin/session')->isAllowed('admin/magnalister/magnalister');
    }
    
}