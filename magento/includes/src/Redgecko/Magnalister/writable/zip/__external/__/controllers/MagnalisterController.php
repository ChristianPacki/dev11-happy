<?php
class Redgecko_Magnalister_MagnalisterController extends Mage_Core_Controller_Front_Action{
    protected $sMagnaAction='';
    public function magnaAction() {
        include Mage::getModuleDir('','Redgecko_Magnalister').DIRECTORY_SEPARATOR.'Lib'.DIRECTORY_SEPARATOR.'Core'.DIRECTORY_SEPARATOR.'ML.php';
        ML::gi()->runFrontend($this->sMagnaAction);
    }
    public function getActionMethodName($action){
        $this->sMagnaAction=$action;
        return 'magnaAction';
    }
}