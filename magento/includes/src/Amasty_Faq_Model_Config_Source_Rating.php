<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_Faq
 */


class Amasty_Faq_Model_Config_Source_Rating
{
    public function toOptionArray()
    {
        return array(
            array(
                'value' => Amasty_Faq_Model_Question_Rating::TYPE_STARS,
                'label' => Mage::helper('amfaq')->__('Stars')
            ),
            array(
                'value' => Amasty_Faq_Model_Question_Rating::TYPE_YESNO,
                'label' => Mage::helper('amfaq')->__('Yes/No')
            ),
        );
    }
}
