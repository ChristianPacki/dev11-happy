<?php
/**
 * Magmex MySQL model for Advent Calendar day table
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.magmex.com/MAGMEX_SOFTWARE_LICENSE.txt
 *
 * @copyright Copyright (c) 2015 magmex, Michael Stork, (http://www.magmex.com)
 * @license http://www.magmex.com/MAGMEX_SOFTWARE_LICENSE.txt
 * @category Magmex
 * @package Magmex_AdventCalendar
 * @author Michael Stork <m.stork@magmex.com>
 */

class Magmex_AdventCalendar_Model_Mysql4_Day extends Mage_Core_Model_Mysql4_Abstract
{
    /**
     * Construct
     * Inits the advent calendar day table
     *
     * @see Mage_Core_Model_Resource_Abstract::_construct()
     * @return void
     */
    public function _construct()
    {
        $this->_init('magmex_adventcalendar/day', 'advent_calendar_day_id');
    }
    
    /**
     * Adds the day action data to the advent calendar model - needed e.g. for the frontend output,
     * to have all stuff in one data array - data is only added, if a action is existant
     *
     * @see Mage_Core_Model_Resource_Db_Abstract::_afterLoad()
     * @param Magmex_AdventCalendar_Model_Day $adventCalendarDay advent calendar day instance
     * @return Magmex_AdventCalendar_Model_Mysql4_Day chaining
     */
    public function addDayActionData(Magmex_AdventCalendar_Model_Day $adventCalendarDay)
    {
        $dayActionType = $adventCalendarDay->getActionType();
    
        if (!empty($dayActionType)) {
            $select = $this->_getReadAdapter()->select()
                ->from($this->getTable('magmex_adventcalendar/day_action_' . $dayActionType))
                ->where('advent_calendar_day_id = ?', $adventCalendarDay->getId());
            $data = $this->_getReadAdapter()->fetchAll($select);
            if (!empty($data[0])) {
                $data = $data[0];
                foreach ($data as $valueName => $value) {
                    //don't overwrite existing data like the id (advent_calendar_day_id is also used in foreign day action tables)
                    $existingDataValue = $adventCalendarDay->getData($valueName);
                    if (empty($existingDataValue)) {
                        $adventCalendarDay->setData($valueName, $value);
                    }
                }
            }
        }
        
        return $this;
    }
    
    /**
     * Loads an advent calendar model for a given advent calendar id and the day number (the combination is unique)
     *
     * @param Magmex_AdventCalendar_Model_Day $adventCalendarDay advent calendar day instance that should get the loaded data
     * @param integer $adventCalendarId advent calendar id
     * @param integer $adventCalendarDate advent calendar day date
     * @return Magmex_AdventCalendar_Model_Mysql4_Adventcalendar_Day chaining
     */
    public function loadAdventCalendarDayByAdventCalendarIdAndDate($adventCalendarDay, $adventCalendarId, $adventCalendarDate)
    {
        $readAdapter = $this->_getReadAdapter();
        $select = $readAdapter->select()
            ->from($this->getMainTable())
            ->reset(Zend_Db_Select::COLUMNS)
            ->columns('advent_calendar_day_id')
            ->where('advent_calendar_id = ?', $adventCalendarId)
            ->where('date = ?', $adventCalendarDate);
        
        $data = $readAdapter->fetchRow($select);
    
        if (!empty($data)) {
            $adventCalendarDay->load($data['advent_calendar_day_id']);
        }
    
        return $this;
    }
}