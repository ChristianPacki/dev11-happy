<?php
/**
 * Mages: Contains all helper methods needed for the different form types in the AdventCalender module.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.magmex.com/MAGMEX_SOFTWARE_LICENSE.txt
 *
 * @copyright Copyright (c) 2015 magmex, Michael Stork, (http://www.magmex.com)
 * @license http://www.magmex.com/MAGMEX_SOFTWARE_LICENSE.txt
 * @category Magmex
 * @package Magmex_AdventCalendar
 * @author Michael Stork <m.stork@magmex.com>
 */
class Magmex_AdventCalendar_Helper_Form extends Mage_Core_Helper_Abstract
{
    /**
     * Returns the select options for the actual year until 2030
     *
     * @return array select options array
     */
    public function getAvailabeYearSelectOptions()
    {
        $optionArray = array();

        $this->_addPleaseChooseOptionToOptionArray($optionArray, Mage::helper('magmex_adventcalendar')->__('Please select a year...'));

        $currentTimestamp = Mage::getModel('core/date')->timestamp(time());
        $actualYear = date('Y', $currentTimestamp);

        $yearTodayUntilFutureArray = range($actualYear, $actualYear + (2030 - $actualYear));
        foreach ($yearTodayUntilFutureArray as $year) {
            $this->_addOptionToOptionArray($optionArray, $year, $year);
        }
        return $optionArray;
    }

    /**
     * Returns the day action types for a fieldset drop down/select
     *
     * @param boolean $addChooseOptionText (optional) default: true, defines, if the "please select an option" text should be added to the selectable options
     * @return array options array for a fieldset drop down/select
     */
    public function getDayActionTypeOptions($addChooseOptionText = true)
    {
        $helperData = Mage::helper('magmex_adventcalendar');
        $optionArray = array();
        if ($addChooseOptionText === true) {
            $this->_addPleaseChooseOptionToOptionArray($optionArray, Mage::helper('magmex_adventcalendar')->__('Please select an advent calendar day action...'));
        }

        $this->_addOptionToOptionArray($optionArray, $helperData->__('CMS Block'), Magmex_AdventCalendar_Model_Day_Action_Cmsblock::ACTION_TYPE)
            ->_addOptionToOptionArray($optionArray, $helperData->__('Coupon'), Magmex_AdventCalendar_Model_Day_Action_Coupon::ACTION_TYPE)
            ->_addOptionToOptionArray($optionArray, $helperData->__('Product Discount'), Magmex_AdventCalendar_Model_Day_Action_Productdiscount::ACTION_TYPE)
            ->_addOptionToOptionArray($optionArray, $helperData->__('Teaser Link'), Magmex_AdventCalendar_Model_Day_Action_Teaserlink::ACTION_TYPE);

        return $optionArray;
    }

    /**
     * Returns an option array with all available cms blocks for a specific store_id
     *
     * @param integer $storeId store id - adds a filter to the collection, the just return the store specific block ids
     * @return array options array for a fieldset drop down/select
     */
    public function getCmsBlockOptions($storeId)
    {
        $optionArray = Mage::getResourceModel('cms/block_collection')
            ->addStoreFilter($storeId)
            ->load()
            ->toOptionArray();
        $this->_addPleaseChooseOptionToOptionArray($optionArray, Mage::helper('magmex_adventcalendar')->__('Please select a static block...'));
        return $optionArray;
    }

    /**
     * Returns the JavaScript code necessary to deactivate form validation to make the changing
     * of day action types possible in action type forms
     *
     * @param string $submitUrl submit url posted after changing the day action type
     * @return string JavaScript code string
     */
    public function getActionTypeOnChangeJavaScript($submitUrl)
    {
        $javaScript = "
            $('edit_form').select('.required-entry').each(function(element) {
                element.removeClassName('required-entry');
            });
            editForm.submit('" . $submitUrl . "');
        ";
        return $javaScript;
    }

    /**
     * Returns an option array for the available advent calendar standard background images
     *
     * @return array options array for select fields
     */
    public function getAdventCalendarStandardBackgroundImageTypesOptionArray()
    {
        $optionArray = array();
        $modelAdventCalendar = Mage::getModel('magmex_adventcalendar/adventcalendar');
        $standardBackgroundImageTypeArray = $modelAdventCalendar->getStandardBackgroundImageTypes();
        $counter = 1;
        foreach ($standardBackgroundImageTypeArray as $standardBackgroundImageName) {
            $this->_addOptionToOptionArray($optionArray, 'Standard background image ' . $counter, Magmex_AdventCalendar_Model_Adventcalendar::SKIN_STANDARD_BACKGROUND_IMAGE_PATH . DS . $standardBackgroundImageName . '.jpg');
            $counter++;
        }
        return $optionArray;
    }

    /**
     * Retruns an array which contains a text and a link of an standard background image type.
     * Used for e.g. to show a not for a form field.
     *
     * @return array an array in the format array('text' => 'lorem', 'link' => '<a href="ipsum">loremlink</a>')
     */
    public function getAdventCalendarStandarBackgroundImageTypeLinks()
    {
        $textArray = array();
        $modelAdventCalendar = Mage::getModel('magmex_adventcalendar/adventcalendar');
        $standardBackgroundImageTypeArray = $modelAdventCalendar->getStandardBackgroundImageTypes();

        $counter = 1;
        foreach ($standardBackgroundImageTypeArray as $standardBackgroundImageName) {
            $path = DS . Magmex_AdventCalendar_Model_Adventcalendar::SKIN_STANDARD_BACKGROUND_IMAGE_PATH . DS . $standardBackgroundImageName . '.jpg';
            $textArray[$counter]['text'] = 'Standard background image ' . $counter;
            $textArray[$counter]['link'] = '<a href="' . $path . '" target="_blank">' . Mage::helper('magmex_adventcalendar')->__('Click here and have a look') . '</a>';
            $counter++;
        }
        return $textArray;
    }

    /**
     * Returns an option array for the available layout types
     *
     * @param Magmex_AdventCalendar_Model_Adventcalendar $modelAdventCalendar
     * @return array options array for select fields
     */
    public function getAdventCalendarLayoutTypesOptionArray($modelAdventCalendar)
    {
        $optionArray = array();
        $layoutTypeArray = $modelAdventCalendar->getLayoutTypes();
        foreach ($layoutTypeArray as $layoutType) {
            $this->_addOptionToOptionArray($optionArray, $layoutType, $layoutType);
        }
        return $optionArray;
    }

    /**
     * Returns an option array containing all available coupon codes with the coupon
     * id as value
     *
     * @return array option array with all coupon codes available
     */
    public function getCouponCodeOptionArray()
    {
        $optionArray = array();
        $this->_addPleaseChooseOptionToOptionArray($optionArray, Mage::helper('magmex_adventcalendar')->__('Please select a coupon code...'));

        if (version_compare(Mage::getVersion(), '1.5.0.0', '<')) {
            $salesRuleCollection = Mage::getModel('salesrule/rule')
                ->getResourceCollection();

            foreach ($salesRuleCollection as $salesRule) {
                $this->_addOptionToOptionArray($optionArray, $salesRule->getCouponCode(), $salesRule->getId());
            }
        } else {
            /** @var $collection Mage_SalesRule_Model_Mysql4_Coupon_Collection */
            $couponCollection = Mage::getModel('salesrule/coupon')
                ->getResourceCollection();

            foreach ($couponCollection as $coupon) {
                $this->_addOptionToOptionArray($optionArray, $coupon->getCode(), $coupon->getId());
            }
        }

        return $optionArray;
    }

    /**
     * Uploads an image with the allowed image extensions.
     * Can be used by several forms
     *
     * @param string $imageFieldName the image field name in $_FILES
     * @param string $imageSubPath the sub path the to constant magmex image file root directory
     * @param array $modelData reference to the model data, that gets edited by the image upload
     *                         (set image, delete it or do nothing, if no image is given in the post)
     * @return void
     */
    public function uploadImage($imageFieldName, $imageSubPath, &$modelData)
    {
        if (isset($_FILES[$imageFieldName]['name']) && (file_exists($_FILES[$imageFieldName]['tmp_name']))) {
            try {
                $uploader = new Varien_File_Uploader($imageFieldName);
                $uploader->setAllowedExtensions(array('jpg','jpeg','png'));
                $uploader->setAllowRenameFiles(false);
                $uploader->setFilesDispersion(false);
                $imagePathRelativeToMediaFolder = Magmex_AdventCalendar_Model_Adventcalendar::MEDIA_SUB_IMAGE_PATH . DS . $imageSubPath . DS;
                $localImagePath = Mage::getBaseDir('media') . DS . $imagePathRelativeToMediaFolder;
                $imageFileName = $_FILES[$imageFieldName]['name'];
                $uploader->save($localImagePath, $imageFileName);
                $modelData[$imageFieldName] = $imagePathRelativeToMediaFolder . $uploader->getUploadedFileName();
            } catch(Exception $e) {

            }
        } else {
            if (isset($modelData[$imageFieldName]['delete']) && $modelData[$imageFieldName]['delete'] == 1) {
                $modelData[$imageFieldName] = '';
            } else {
                unset($modelData[$imageFieldName]);
            }
        }
    }

    /**
     * Adds a "please choose" option to the option array, which has no value and therefore is not selectable
     *
     * @param array &$optionArray option array reference
     * @param label $label label of the new option
     *
     * @return Magmex_AdventCalendar_Helper_Form chaining
     */
    protected function _addPleaseChooseOptionToOptionArray(&$optionArray, $label)
    {
        array_unshift($optionArray,
            array(
                'value' => '',
                'label' => $label
            )
        );
        return $this;
    }

    /**
     * Adds a new option to a given option array
     *
     * @param array &$optionArray option array reference
     * @param label $label label of the new option
     * @param string $value value to add to the new option
     *
     * @return Magmex_AdventCalendar_Helper_Form chaining
     */
    protected function _addOptionToOptionArray(&$optionArray, $label, $value)
    {
        $optionArray[] = array(
            'value' => $value,
            'label' => $label
        );
        return $this;
    }
}