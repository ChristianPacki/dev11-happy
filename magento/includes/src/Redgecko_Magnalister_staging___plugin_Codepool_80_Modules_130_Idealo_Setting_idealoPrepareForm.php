<?php

MLSetting::gi()->idealo_prepare_form = array(
    'details' => array(
        'fields' => array(
            'Title' => array(
                'name' => 'Title',
                'singleproduct' => true,
            ),
            'Description' => array(
                'name' => 'Description',
                'singleproduct' => true,
            ),
            'Image' => array(
                'name' => 'Image',
                'singleproduct' => true,
            ),
        ),
    ),
    'shipping' => array(
        'fields' => array(
                'Shippingcountry' => array(
                    'name' => 'Shippingcountry',
                    'type' => 'select',
                ),
                'shippingmethodandcost' => array(
                    'name' => 'shippingmethodandcost',
                    'type' => 'selectwithtextoption',
                    'subfields' => array(
                        'select' => array('name' => 'shippingcostmethod', 'type' => 'select'),
                        'string' => array('name' => 'shippingcost', 'type' => 'string'),
                    )
                )
        ),
    ),
);

MLSetting::gi()->amazon_prepare_apply_form = array(
    'details' => array(
        'fields' => array(
            'ItemTitle' => array(
                'name' => 'ItemTitle',
                'singleproduct' => true
            ),
            'Manufacturer' => array(
                'name' => 'Manufacturer',
                'singleproduct' => true
            ),
            'Brand' => array(
                'name' => 'Brand',
                'singleproduct' => true
            ),
            'ManufacturerPartNumber' => array(
                'name' => 'ManufacturerPartNumber',
                'singleproduct' => true
            ),
            'EAN' => array(
                'name' => 'Ean',
                'singleproduct' => true
            ),
        ),
    ),
    'moredetails' => array(
        'fields' => array(
            'Images' => array(
                'name' => 'Images',
                'singleproduct' => true
            ),
            'BulletPoints' => array(
                'name' => 'BulletPoints',
                'singleproduct' => true
            ),
            'Description' => array(
                'name' => 'Description',
                'singleproduct' => true
            ),
            'Keywords' => array(
                'name' => 'Keywords',
                'singleproduct' => true
            ),
        ),
    ),
    'common' => array(
        'fields' => array(
            'ShippingTime' => array(
                'name' => 'ShippingTime',
            ),
            'ConditionType' => array(
                'name' => 'ConditionType',
            ),
            'ConditionNote' => array(
                'name' => 'ConditionNote',
            ),
        ),
    ),
);