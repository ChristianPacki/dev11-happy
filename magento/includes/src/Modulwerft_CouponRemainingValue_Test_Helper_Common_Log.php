<?php
/**
 * @category Modulwerft
 * @package Modulwerft_CouponRemainingValue
 * @author Shopwerft GmbH <werft@shopwerft.com>
 * @author Benjamin Wunderlich <b.wunderlich@shopwerft.com>
 * @copyright 2014 Shopwerft GmbH (http://www.shopwerft.com)
 * @since 1.2.2
 */

class Modulwerft_CouponRemainingValue_Test_Helper_Common_Log
    extends EcomDev_PHPUnit_Test_Case

{
    const LOGFILE_NAME = 'Modulwerft_CouponRemainingValue.log';

    public static function setUpBeforeClass()
    {
        parent::setUpBeforeClass();

        // empty log file
        $logfile = self::_getLogfileName();
        if (is_writable($logfile)) {
            file_put_contents($logfile, '');
        }
    }

    /**
     * @return string
     */
    protected static function _getLogfileName()
    {
        return Mage::getBaseDir('var') . DS . 'log' . DS . self::LOGFILE_NAME;
    }

    /**
     * @return string
     */
    private function _getUniqueLogText()
    {
        return '@@@@' . microtime(true) . '@@@@';
    }

    private function _checkLogfileContainsString($string)
    {
        $fileContent = file_get_contents($this->_getLogfileName());
        return (bool) strstr($fileContent, $string);
    }

    /**
     * Assert that the modules logfile contains a given string
     *
     * @param $string
     * @param string $message
     */
    public function assertLogfileContainsString($string, $message = '')
    {
        $this->assertTrue($this->_checkLogfileContainsString($string), $message);
    }

    /**
     * Assert that the modules logfile does not contain a given string
     *
     * @param $string
     * @param string $message
     */
    public function assertLogfileNotContainsString($string, $message = '')
    {
        $this->assertFalse($this->_checkLogfileContainsString($string), $message);
    }

    /**
     * Test if logging works
     *
     * @loadFixture
     */
    public function testLogging()
    {
        /** @var Modulwerft_CouponRemainingValue_Helper_Log $logHelper */
        $logHelper = Mage::helper('modulwerft_couponremainingvalue/log');

        $this->assertTrue($logHelper->isLoggingActive());
        $this->assertFalse($logHelper->isDebugLoggingActive());

        $logText = $this->_getUniqueLogText();
        $logHelper->log($logText, Zend_Log::WARN);
        $this->assertLogfileContainsString($logText, "Logfile does not contain test-text after logging.");

        $logText = $this->_getUniqueLogText();
        $logHelper->log($logText, Zend_Log::DEBUG);
        $this->assertLogfileNotContainsString($logText, "Logfile does contain test-text after logging, but mustn't.");
    }

    /**
     * Test if debug logging works
     *
     * @loadFixture
     */
    public function testDebugLogging()
    {
        /** @var Modulwerft_CouponRemainingValue_Helper_Log $logHelper */
        $logHelper = Mage::helper('modulwerft_couponremainingvalue/log');

        $this->assertTrue($logHelper->isLoggingActive());
        $this->assertTrue($logHelper->isDebugLoggingActive());

        $logText = $this->_getUniqueLogText();
        $logHelper->log($logText, Zend_Log::WARN);
        $this->assertLogfileContainsString($logText, "Logfile does not contain test-text (WARN) after logging.");

        $logText = $this->_getUniqueLogText();
        $logHelper->log($logText, Zend_Log::DEBUG);
        $this->assertLogfileContainsString($logText, "Logfile does not contain test-text (DEBUG) after logging.");
    }

    /**
     * Test if exception logging works
     *
     * @loadFixture
     */
    public function testExceptionLogging()
    {
        /** @var Modulwerft_CouponRemainingValue_Helper_Log $logHelper */
        $logHelper = Mage::helper('modulwerft_couponremainingvalue/log');

        $this->assertTrue($logHelper->isLoggingActive());

        $logText = $this->_getUniqueLogText();
        $logHelper->logException(new Exception($logText));
        $this->assertLogfileContainsString($logText, "Logfile does not contain test-text (Exception) after logging.");
    }


}