<?php


MLFilesystem::gi()->loadClass('Magento_Model_ProductList_Abstract');

class ML_MagentoMeinpaket_Model_ProductList_Meinpaket_Prepare_Prepare extends ML_Magento_Model_ProductList_Abstract {

    public function getSelectionName() {
        return 'match';
    }

    protected function executeFilter() {
        $this->oFilter
            ->registerDependency('magentonovariantsfilter')
            ->registerDependency('searchfilter')
            ->limit()
            ->registerDependency('categoryfilter')
            ->registerDependency('preparestatusfilter')
            ->registerDependency('magentoattributesetfilter')
            ->registerDependency('productstatusfilter')
            ->registerDependency('manufacturerfilter')
            ->registerDependency('magentoproducttypefilter')
            ->registerDependency('magentosaleablefilter')
        ;
        return $this;
    }


}
