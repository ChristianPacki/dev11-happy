<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_Faq
 */

class Amasty_Faq_Block_Adminhtml_Topic_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        $this->_blockGroup = 'amfaq';
        $this->_objectId = 'id';
        $this->_controller = 'adminhtml_topic';

        parent::__construct();
    }

    public function getHeaderText()
    {
        if (Mage::registry('current_topic')->getId()) {
            return $this->htmlEscape(Mage::registry('current_topic')->getTitle());
        }
        else {
            return Mage::helper('amfaq')->__('New Topic');
        }
    }

    public function getSaveUrl()
    {
        return $this->getUrl('*/*/save', array('_current'=>true, 'back'=>null));
    }
}