<?php

/**
 * @category Modulwerft
 * @package Modulwerft_CouponRemainingValue
 * @author Modulwerft <info@modulwerft.com>
 * @author Benjamin Wunderlich <b.wunderlich@shopwerft.com>
 * @copyright 2014 Shopwerft GmbH (http://www.shopwerft.com)
 */

abstract class Modulwerft_CouponRemainingValue_Helper_Common_Config
    extends Mage_Core_Helper_Abstract
{
    const CONFIG_MODULE_ACTIVE = 'module_active';
    const CONFIG_LOGGING = 'logging';
    const CONFIG_DEBUG_LOGGING = 'debug_logging';

    /**
     * Retrieve the prefix for the configuration paths.
     *
     * @return string
     */
    abstract protected function _getConfigPathPrefix();

    /**
     * Build config path containing config prefix and given paramName
     *
     * @param string $paramName
     * @param null|string $groupName
     * @return string
     */
    protected function _configPath($paramName, $groupName = null)
    {
        $configPath = $this->_getConfigPathPrefix() . $paramName;

        // replace group name
        if (isset($groupName) && is_string($groupName) && strlen($groupName) > 0) {
            $parts = explode('/', $configPath);
            $parts[1] = $groupName;
            $configPath = implode('/', $parts);
        }

        return $configPath;
    }

    /**
     * Retrieve a store config value by given value (will be build containing prefix) and store
     *
     * @param string $paramName
     * @param null|int|Mage_Core_Model_Store $store
     * @param null|string $groupName
     * @return bool
     */
    protected function _getStoreConfig($paramName, $store = null, $groupName = null)
    {
        $configPath = $this->_configPath($paramName, $groupName);

        return Mage::getStoreConfig($configPath, $store);
    }

    /**
     * Retrieve a store config flag by given value (will be build containing prefix) and store
     *
     * @param string $paramName
     * @param null|int|Mage_Core_Model_Store $store
     * @param null $groupName
     * @return bool
     */
    protected function _getStoreConfigFlag($paramName, $store = null, $groupName = null)
    {
        $configPath = $this->_configPath($paramName, $groupName);

        return Mage::getStoreConfigFlag($configPath, $store);
    }

    /**
     * Check if module is active in a specific store.
     *
     * @param null|int|Mage_Core_Model_Store $store
     * @return bool
     */
    public function isModuleActive($store = null)
    {
        return $this->_getStoreConfigFlag(self::CONFIG_MODULE_ACTIVE, $store);
    }

    /**
     * Retrieve all stores in which this module is activated.
     *
     * @return array
     */
    public function getActiveStoreIds()
    {
        return array_keys(Mage::getConfig()->getStoresConfigByPath($this->_configPath(self::CONFIG_MODULE_ACTIVE), array('1')));
    }

    /**
     * Returns whether logging is generally activated.
     *
     * @param null|int|Mage_Core_Model_Store $store
     * @return bool
     */
    public function isLoggingActive($store = null)
    {
        return $this->_getStoreConfigFlag(self::CONFIG_LOGGING, $store);
    }

    /**
     * Returns whether debug logging is activated.
     *
     * @param null|int|Mage_Core_Model_Store $store
     * @return bool
     */
    public function isDebugLoggingActive($store = null)
    {
        return $this->_getStoreConfigFlag(self::CONFIG_DEBUG_LOGGING, $store);
    }

    /**
     * Converts bcc from comma separated string to array.
     *
     * Only valid e-mail address will be returned.
     *
     * @param string $bcc
     * @return array
     */
    protected function _getEmailBcc($bcc)
    {
        // is field filled?
        if (empty($bcc)) {
            return array();
        }

        // split email addresses
        $addresses = $bcc;
        if (is_string($bcc)) {
            $addresses = preg_split('# *, *#', $bcc);
        }

        // validate email addresses
        $validAddresses = array();
        foreach ($addresses as $_address) {
            if (Zend_Validate::is($_address, 'EmailAddress')) {
                $validAddresses[] = $_address;
            }
        }

        return $validAddresses;
    }


}