<?php

MLSetting::gi()->add('idealo_config_account', array(    
    'tabident' => array(
        'legend' => array(
            'classes' => array('mlhidden'),
        ),
        'fields' => array(
            array(
                'name' => 'tabident',
                'type' => 'string',
            ),
        ),
    ),
    'account' => array(
        'fields' => array(
            array(
                'name' => 'access.inventorypath',
                'type' => 'information',
            ),
        ),
    )
), false);


MLSetting::gi()->add('idealo_config_prepare', array(
    'prepare' => array(
        'fields' => array(
            array(
                'name' => 'checkout.status',
                'type' => 'bool',
            ),
            array(
                'name' => 'shipping.methods',
                'type' => 'select',
            ),
            array(
                'name' => 'payment.methods',
                'type' => 'select',
            ),
        ),
    ),
    'upload' => array(
        'fields' => array(
            array(
                'name' => 'checkin.status',
                'type' => 'bool',
            ),
            array(
                'name' => 'lang',
                'type' => 'select',
            ),
            array(
                'name' => 'productfield.shippingtime',
                'type' => 'select',
                'expert' => true
            ),
        )
    ),
    'shipping' =>array(        
            'fields' => array(
                array(
                    'name' => 'shippingcountry',
                    'type' => 'select',
                ),
                array(
                    'name' => 'shippingmethodandcost',
                    'type' => 'selectwithtextoption',
                    'subfields' => array(
                        'select' => array('name' => 'shippingmethod', 'type' => 'select'),
                        'string' => array('name' => 'shippingcost', 'type' => 'string', 'default' => '0.00'),
                    )
                )
            )
        )
    ), false);

MLSetting::gi()->add('idealo_config_price', array(
    'price' => array(
        'fields' => array(
            array(
                'name' => 'price',
                'type' => 'subFieldsContainer',
                'subfields' => array(
                    'addkind' => array('name' => 'price.addkind', 'type' => 'select'),
                    'factor' => array('name' => 'price.factor', 'type' => 'string'),
                    'signal' => array('name' => 'price.signal', 'type' => 'string')
                )
            ),
            array(
                'name' => 'priceoptions',
                'type' => 'subFieldsContainer',
                'subfields' => array(
                    'group' => array('name' => 'price.group', 'type' => 'select'),
                    'usespecialoffer' => array('name' => 'price.usespecialoffer', 'type' => 'bool'),
                ),
            ),
            array(
                'name' => 'exchangerate_update',
                'type' => 'bool',
            ),
        )
    ),
), false);

MLSetting::gi()->add('idealo_config_sync', array(
    'sync' => array(
        'fields' => array(
            array(
                'name' => 'inventorysync.price',
                'type' => 'select',
            )
        )
    )
), false);

MLSetting::gi()->add('idealo_config_orderimport', array(
    'importactive' => array(
        'fields' => array(
            array(
                'name' => 'importactive',
                'type' => 'subFieldsContainer',
                'subfields' => array(
                    'import' => array('name' => 'import', 'type' => 'radio', ),
                    'preimport.start' => array('name' => 'preimport.start', 'type' => 'datepicker'),
                ),
            ),
            array(
                'name' => 'customergroup',
                'type' => 'select',
            ),
            array(
                'name' => 'orderimport.shop',
                'type' => 'select',
            ),
            array(
                'name' => 'orderstatus.open',
                'type' => 'select',
            ),
            array (
                'name' => 'checkout.token',
                'type' => 'string',
            ),
            'orderimport.shippingmethod' => array(
                'name' => 'orderimport.shippingmethod',
                'type' => 'string',
                'default' => 'Idealo',
                'expert' => true,
            ),
        ),
    ),
    'mwst' => array(
        'fields' => array(
            array(
                'name' => 'mwst.fallback',
                'type' => 'string',
            ),
        ),
    ),
    'orderstatus' => array(
        'fields' => array(
            array(
                'name' => 'orderstatus.sync',
                'type' => 'select',
            ),
            array(
                'name' => 'orderstatus.shipped',
                'type' => 'select'
            ),
            array(
                'name' => 'orderstatus.carrier.default',
                'type' => 'string',
                'expert' => true
            ),
            array(
                'name' => 'orderstatus.canceled',
                'type' => 'select'
            ),
             array (
                'name' => 'orderstatus.cancelreason',
                'type' => 'select',
            ),
             array (
                'name' => 'orderstatus.cancelcomment',
                'type' => 'string',
            ),
        ),
    )
), false);

MLSetting::gi()->add('idealo_config_emailtemplate', array(
    'mail' => array(
        'fields' => array(
            array(
                'name' => 'mail.send',
                'type' => 'radio',
                'default' => false,
            ),
            array(
                'name' => 'mail.originator.name',
                'type' => 'string',
                'default' => '{#i18n:idealo_config_account_emailtemplate_sender#}',
            ),
            array(
                'name' => 'mail.originator.adress',
                'type' => 'string',
                'default' => '{#i18n:idealo_config_account_emailtemplate_sender_email#}',
            ),
            array(
                'name' => 'mail.subject',
                'type' => 'string',
                'default' => '{#i18n:idealo_config_account_emailtemplate_subject#}',
            ),
            array(
                'name' => 'mail.content',
                'type' => 'configMailContentContainer',
                'default' => '{#i18n:idealo_config_account_emailtemplate_content#}',
            ),
            array(
                'name' => 'mail.copy',
                'type' => 'radio',
                'default' => true,
            ),
        ),
    ),
), false);