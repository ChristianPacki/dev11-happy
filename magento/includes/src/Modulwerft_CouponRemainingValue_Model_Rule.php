<?php

/**
 * @category Modulwerft
 * @package Modulwerft_CouponRemainingValue
 * @author Modulwerft GmbH <info@modulwerft.com>
 * @author Philipp Zabel <p.zabel@shopwerft.com>
 * @copyright 2014 Shopwerft GmbH (http://www.shopwerft.com)
 * @since 1.2.0
 */
class Modulwerft_CouponRemainingValue_Model_Rule
    extends Mage_Core_Model_Abstract
{

    const RULE_USE_REMAINING_VALUE = 'use_remaining_value';

    /** @var string */
    protected $_eventPrefix = 'modulwerft_couponremainingvalue_rule';

    /** @var string */
    protected $_eventObject = 'rule';

    protected function _construct()
    {
        $this->_init('modulwerft_couponremainingvalue/rule');
    }

    /**
     * @param bool $useRemainingValue
     * @return $this
     */
    public function setUseRemainingValue($useRemainingValue)
    {
        return $this->setData(self::RULE_USE_REMAINING_VALUE, intval($useRemainingValue));
    }

    /**
     * @return bool
     */
    public function getUseRemainingValue()
    {
        return (bool)$this->_getData(self::RULE_USE_REMAINING_VALUE);
    }

}
