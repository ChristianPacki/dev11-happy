<?php

class Pektsekye_OptionBundle_Helper_Data extends Mage_Core_Helper_Abstract
{


/*
The following function is called:
on line 43 in the file:
app/design/frontend/default/default/template/optionbundle/bundle/sales/order/items/renderer.phtml
for customer front-end order view page

on line 44 in the file:
app/design/frontend/default/default/template/optionbundle/bundle/email/order/items/order/default.phtml
for order email

on line 51 in the file:
app/design/adminhtml/default/default/template/optionbundle/bundle/sales/order/view/items/renderer.phtml
for back-end order view page

*/
  public function sortTemplateItems($parentItem, $childrenItems, $customOptions)
  {
    
    if ($parentItem instanceof Mage_Sales_Model_Order_Item) {
        $product = $parentItem->getProduct();
    } else {
        $product = $parentItem->getOrderItem()->getProduct();
    }
            
            
    $hasOrder = false; 
               
    if ($product->getTypeId() == Mage_Catalog_Model_Product_Type::TYPE_BUNDLE){ 

      $bundleOrder = array();

      foreach ($this->getBoptions($product) as $_option) {         
        if (!$_option->getSelections()) {
            continue;
        }          
      
        $bundleOrder[$_option->getId()] = (int) $_option->getPosition();
        $hasOrder |= $_option->getPosition() > 0;            
      }  
    }

    foreach ($childrenItems as $_item){
      $attributes = $this->getSelectionAttributes($_item);
      $order = isset($bundleOrder[$attributes['option_id']]) ? $bundleOrder[$attributes['option_id']] : 0;
      $_item->setSortOrder($order);
    }                  
   
             
    $productOptions = $product->getOptions();         
    
    foreach ($customOptions as $k => $option) {
      if (isset($option['option_id'])){
        $id = (int) $option['option_id'];                 
        $customOptions[$k]['sort_order'] = isset($productOptions[$id]) ? (int) $productOptions[$id]->getSortOrder() : 0;              
      }
    }


    $options = array_merge($childrenItems, $customOptions);
      
     if ($hasOrder) 
        usort($options, array($this, "sortItems"));
        
     array_unshift($options, $parentItem);
    
     return $options;
  }


  public function getSelectionAttributes($item) {

      if ($item instanceof Mage_Sales_Model_Order_Item) {
          $options = $item->getProductOptions();
      } else {
          $options = $item->getOrderItem()->getProductOptions();
      }
      if (isset($options['bundle_selection_attributes'])) {
          return unserialize($options['bundle_selection_attributes']);
      }
      return null;
  }


   public function getSingleSelectionOptions($product)
  {
    $optionIds = array();
    foreach ($this->getBoptions($product, false) as $option) {
      if (count($option->getSelections()) == 1 && $option->getRequired()){
        $optionIds[$option->getId()] = 1;
      }
    }
    
    return $optionIds;   
  }
  

  public function applySortOrder($productId, $options)
  {
      
      $product = Mage::getModel('catalog/product')->load($productId);
      
      if (!$product->getId())
        return $options;
        
      $hasOrder = false; 
                 
      if ($product->getTypeId() == Mage_Catalog_Model_Product_Type::TYPE_BUNDLE){ 
 
        $bundleOrder = array();

        foreach ($this->getBoptions($product) as $_option) {         
          if (!$_option->getSelections()) {
              continue;
          }          
        
          $bundleOrder[$_option->getTitle()] = (int) $_option->getPosition();
          $hasOrder |= $_option->getPosition() > 0;            
        }  
      }
             
      $customOptions = $product->getOptions();         
      
      foreach ($options as $k => $option) {
        if (isset($option['option_id'])){
          $id = (int) $option['option_id'];                 
          $options[$k]['sort_order'] = isset($customOptions[$id]) ? (int) $customOptions[$id]->getSortOrder() : 0;              
        } else {
          $options[$k]['sort_order'] = isset($bundleOrder[$option['label']]) ? $bundleOrder[$option['label']] : 0;
        }
      }
      
     if ($hasOrder) 
        usort($options, array($this, "sortOptions"));

    return $options;   
  }  

  
  
  public function getHiddenOptions($product, $requestOptions)
  {

    $rData = Mage::getModel('optionbundle/relation')->getRelationData($product); 
	
    $hiddenOIds	 = array('b' => array(), 'o' => array());
    $visibleOIds = array('b' => array(), 'o' => array());      

    foreach (array('o','b') as $t){ 
  
      foreach ($requestOptions[$t] as $v){
        $vIds = is_array($v) ? $v : array($v);
        foreach ($vIds as $vId){
          if (isset($rData['cOIdsByVId'][$t][$vId]['o'])){
            foreach ($rData['cOIdsByVId'][$t][$vId]['o'] as $oId)         
                $visibleOIds['o'][$oId] = 1;
          }
          if (isset($rData['cOIdsByVId'][$t][$vId]['b'])){
            foreach ($rData['cOIdsByVId'][$t][$vId]['b'] as $oId)         
                $visibleOIds['b'][$oId] = 1;
          }
          if (isset($rData['cVIdsByVId'][$t][$vId]['o'])){
            foreach ($rData['cVIdsByVId'][$t][$vId]['o'] as $id){
              $oId = $rData['oIdByVId']['o'][$id];
              $visibleOIds['o'][$oId] = 1;
            }
          } 
          if (isset($rData['cVIdsByVId'][$t][$vId]['b'])){
            foreach ($rData['cVIdsByVId'][$t][$vId]['b'] as $id){
              $oId = $rData['oIdByVId']['b'][$id];
              $visibleOIds['b'][$oId] = 1;
            }
          }                                               
        }                 	  
      }          
    }
    
 
    
    if ($product->getTypeId() == Mage_Catalog_Model_Product_Type::TYPE_BUNDLE){
      $t = 'b';	                    	               
      foreach ($this->getBoptions($product) as $_option) {         
          if (!$_option->getSelections()) {
              continue;
          }          
        
          $id = (int) $_option->getId();
          
          if (isset($rData['pOIdByOId'][$t][$id]) && !isset($visibleOIds[$t][$id]))                    
            $hiddenOIds[$t][$id] = 1;          
      }        
    }    
    
    
    $t = 'o';             
    foreach ($product->getOptions() as $option){
      $oId = $option->getId();
      if (isset($rData['pOIdByOId'][$t][$oId]) && !isset($visibleOIds[$t][$oId])){
        $hiddenOIds[$t][$oId]	= 1;
      }		  
    }	

    return $hiddenOIds;            
                            
  }  

  
  
    
   public function getBoptions($product, $skipStockCheck = true)
  {

      $typeInstance = $product->getTypeInstance(true);
      $typeInstance->setStoreFilter($product->getStoreId(), $product);

      $optionCollection = $typeInstance->getOptionsCollection($product);

      $selectionCollection = $typeInstance->getSelectionsCollection(
          $typeInstance->getOptionsIds($product),
          $product
      );

      return $optionCollection->appendSelections($selectionCollection, true, $skipStockCheck);
  }  
  
  
  

  
  
  public function sortOptions($o1, $o2)
  {
    $a = (int) $o1['sort_order'];
    $b = (int) $o2['sort_order'];	
    if ($a == $b)
        return 0;
    return ($a < $b) ? -1 : 1;
  }

 
  public function sortItems($o1, $o2)
  {
    $a = is_array($o1) ? $o1['sort_order'] : $o1->getSortOrder();
    $b = is_array($o2) ? $o2['sort_order'] : $o2->getSortOrder();	
    if ($a == $b)
        return 0;
    return ($a < $b) ? -1 : 1;
  } 
  
  
    
}
