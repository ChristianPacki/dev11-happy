<?php
class Redgecko_Magnalister_Model_Order_Observer{
    protected $sStatus='';
    public function save_after($oObserver){
        $oOrder=$oObserver->getOrder();
        if($this->sStatus!=$oOrder->getStatus()){
            $this->orderStatusChanged($oOrder);
        }
        return $this;
    }
    public function save_before($oObserver){
        $this->sStatus='';
        $oOrder=$oObserver->getOrder();
        $this->sStatus=$oOrder->getStatus();
        return $this;
    }
    protected function orderStatusChanged($oOrder){
    }
}