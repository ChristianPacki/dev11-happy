<?php

/**
 * @category Modulwerft
 * @package Modulwerft_CouponRemainingValue
 * @author Modulwerft GmbH <info@modulwerft.com>
 * @author Benjamin Wunderlich <b.wunderlich@shopwerft.com>
 * @copyright 2014 Shopwerft GmbH (http://www.shopwerft.com)
 */
class Modulwerft_CouponRemainingValue_Model_Resource_History_Collection
    extends Mage_Core_Model_Mysql4_Collection_Abstract
{

    /** @var string */
    protected $_eventPrefix = 'modulwerft_couponremainingvalue_history_collection';

    /** @var string */
    protected $_eventObject = 'history_collection';

    /**
     * Constructor
     *
     */
    protected function _construct()
    {
        parent::_construct();
        $this->_init('modulwerft_couponremainingvalue/history');
    }

    /**
     * Add rule to filter
     *
     * @param Mage_SalesRule_Model_Rule|int $rule
     * @return $this
     */
    public function addRuleToFilter($rule)
    {
        if ($rule instanceof Mage_SalesRule_Model_Rule) {
            $ruleId = $rule->getId();
        } else {
            $ruleId = intval($rule);
        }

        $this->addFieldToFilter('main_table.rule_id', $ruleId);

        return $this;
    }

    /**
     * @param string $couponCode
     * @return $this
     */
    public function addCouponCodeFilter($couponCode)
    {
        $this->getSelect()
            ->join(
                array('c' => $this->getResource()->getTable('salesrule/coupon')),
                'main_table.coupon_id = c.coupon_id',
                ''
            )
            ->where('c.code = ?', $couponCode);

        return $this;
    }

    /**
     * Add order to filter
     *
     * @param Mage_Sales_Model_Order|int $order
     * @return $this
     */
    public function addOrderToFilter($order)
    {
        if ($order instanceof Mage_Sales_Model_Order) {
            $orderId = $order->getId();
        } else {
            $orderId = intval($order);
        }

        $this->addFieldToFilter('main_table.order_id', $orderId);

        return $this;
    }

}
