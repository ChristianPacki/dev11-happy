<?php

/**
 * @category Modulwerft
 * @package Modulwerft_CouponRemainingValue
 * @author Modulwerft GmbH <info@modulwerft.com>
 * @author Benjamin Wunderlich <b.wunderlich@shopwerft.com>
 * @copyright 2014 Shopwerft GmbH (http://www.shopwerft.com)
 */
class Modulwerft_CouponRemainingValue_Helper_Data
    extends Mage_Core_Helper_Abstract
{

    /** @since 1.2.0 */
    const DIFF_DELTA = 0.001;

    /**
     * The sales rule types that are applicable for remaining values of coupons.
     *
     * @var array
     */
    protected $_applicableRuleActions = array();

    /**
     * Do some initial stuff to initialize the object.
     */
    public function __construct()
    {
        if (defined('Mage_SalesRule_Model_Rule::CART_FIXED_ACTION')) {
            // Magento >= 1.4.2.0
            $this->_applicableRuleActions[] = Mage_SalesRule_Model_Rule::CART_FIXED_ACTION;
        } else {
            // Magento < 1.4.2.0
            $this->_applicableRuleActions[] = 'cart_fixed';
        }

        return $this;
    }

    /**
     * Check if given rule is applicable for remaining value calculation (check action type)
     *
     * @param $rule Mage_SalesRule_Model_Rule
     * @return bool
     */
    public function isRuleActionApplicable(Mage_SalesRule_Model_Rule $rule)
    {
        return in_array($rule->getSimpleAction(), $this->_applicableRuleActions);
    }

    /**
     * @return array
     */
    public function getApplicableRuleActions()
    {
        return $this->_applicableRuleActions;
    }

    /**
     * @param Mage_Sales_Model_Quote $quote
     * @return array
     * @since 1.2.0
     */
    public function getAppliedRulesOfQuote(Mage_Sales_Model_Quote $quote)
    {
        return $this->_getAppliedRuleIds($quote->getAppliedRuleIds());
    }

    /**
     * @param Mage_Sales_Model_Order $order
     * @return array
     * @since 1.1.0
     */
    public function getAppliedRulesOfOrder(Mage_Sales_Model_Order $order)
    {
        return $this->_getAppliedRuleIds($order->getAppliedRuleIds());
    }

    /**
     * Converts applied rules from string to array.
     *
     * @param string $appliedRuleIds
     * @return array
     */
    protected function _getAppliedRuleIds($appliedRuleIds)
    {
        if (empty($appliedRuleIds)) {
            return array();
        }

        $ruleIds = explode(',', $appliedRuleIds);
        $ruleIds = array_unique($ruleIds);

        return $ruleIds;
    }

    /**
     * @param Mage_SalesRule_Model_Rule $rule
     * @return bool
     * @since 1.2.0
     */
    public function usesRuleRemainingValue(Mage_SalesRule_Model_Rule $rule)
    {
        return (bool)intval($rule->getData(Modulwerft_CouponRemainingValue_Model_Rule::RULE_USE_REMAINING_VALUE));
    }

    /**
     * @param Mage_SalesRule_Model_Mysql4_Rule_Collection $rules
     * @return $this
     * @since 1.2.0
     */
    public function addRemainingValueFlagToRules($rules)
    {
        $rules->getSelect()
            ->joinLeft(
                array('crvrule' => $rules->getResource()->getTable('modulwerft_couponremainingvalue/rule')),
                'main_table.rule_id = crvrule.rule_id',
                'use_remaining_value'
            );

        return $this;
    }

}
