<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_Faq
 */

class Amasty_Faq_TopicController extends Mage_Core_Controller_Front_Action
{
    public function viewAction()
    {
        Mage::helper('amfaq/route')->restrictUndispatched();

        $this->loadLayout();
        $this->_removeDefaultTitle = true;

        $id = $this->getRequest()->getParam('id');
        if ($id == '0')
        {
            $topic = Mage::getModel('amfaq/topic')
                ->setTitle(Mage::getStoreConfig('amfaq/topic_page/none_title'))
            ;
        }
        else
        {
            $topic = Mage::getModel('amfaq/topic')->load($id);
            if (!$topic->getId())
            {
                $this->getResponse()->setHeader('HTTP/1.1','404 Not Found');
                $this->getResponse()->setHeader('Status','404 File not found');
                $pageId = Mage::getStoreConfig('web/default/cms_no_route');
                if (!Mage::helper('cms/page')->renderPage($this, $pageId)) {
                    $this->_forward('defaultNoRoute');
                }
                return;
            }
        }

        if ($topic->getMetaTitle())
        {
            $this->_title($topic->getMetaTitle());
        }
        else
        {
            $titleFormat = Mage::getStoreConfig('amfaq/topic_page/title');

            $title = str_replace('{topic}', $topic->getTitle(), $titleFormat);
            $this->_title($title);
        }

        if ($topic->getMetaDescription())
        {
            $description = $topic->getMetaDescription();
        }
        else
        {
            $descriptionFormat = Mage::getStoreConfig('amfaq/topic_page/description');
            $description = str_replace('{topic}', $topic->getTitle(), $descriptionFormat);
        }

        if ($canonical = $topic->getCanonicalUrl()) {
            $headBlock = $this->getLayout()->getBlock('head');
            $headBlock->addLinkRel(
                'canonical',
                Mage::helper('core')->quoteEscape($canonical)
            );
        }

        $this->getLayout()->getBlock('head')->setDescription($description);

        $layout = Mage::getStoreConfig('amfaq/topic_page/layout');
        $layouts = Mage::getSingleton('page/config')->getPageLayouts();
        $this->getLayout()->getBlock('root')->setTemplate($layouts[$layout]->getTemplate());

        $questions = Mage::getResourceSingleton('amfaq/question_collection')->loadByTopic($id);

        $buttons = $this->getLayout()->createBlock('amfaq/buttons', 'buttons');

        $block = $this->getLayout()->createBlock('core/template')
            ->setTemplate('amasty/amfaq/topic.phtml')
            ->setTopic($topic)
            ->setQuestions($questions)
            ->append($buttons)
        ;

        if (Mage::getStoreConfig('amfaq/faq_page/search'))
        {
            $searchForm = $this->getLayout()->createBlock('amfaq/search_form', 'search_form');
            $block->append($searchForm);
        }

        $this->getLayout()->getBlock('content')->append($block);
        
        $breadCrumbs = $this->getLayout()->getBlock('breadcrumbs');
        if ($breadCrumbs) {
            $breadCrumbs->addCrumb('home', array(
                    'label'=>Mage::helper('catalog')->__('Home'),
                    'title'=>Mage::helper('catalog')->__('Go to Home Page'),
                    'link'=>Mage::getBaseUrl()
                ))
                ->addCrumb('faq', array(
                    'label' => $this->__(Mage::getStoreConfig('amfaq/general/kb_title')),
                    'title' => $this->__(Mage::getStoreConfig('amfaq/general/kb_title')),
                    'link'  => Mage::helper('amfaq')->getFaqUrl(),
                ))
                ->addCrumb('question', array(
                    'label' => $topic->getTitle(),
                ))
            ;
        }

        $this->renderLayout();
    }
}
