<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_Faq
 */

class Amasty_Faq_Adminhtml_Amfaq_QuestionController extends Mage_Adminhtml_Controller_Action
{

    public function indexAction()
    {
        $this->_title($this->__('CMS'))
            ->_title($this->__(Mage::getStoreConfig('amfaq/general/kb_title')))
            ->_title($this->__('Manage questions'));

        $this->loadLayout()
            ->_setActiveMenu('cms/amfaq/questions')
            ->_addContent($this->getLayout()->createBlock('amfaq/adminhtml_question'))
            ->renderLayout();
    }

    protected function _initQuestion($idFieldName = 'id')
    {
        $this->_title($this->__('Questions'))->_title($this->__('Manage Questions'));

        $questionId = (int) $this->getRequest()->getParam($idFieldName);
        $question = Mage::getModel('amfaq/question');

        if ($questionId) {
            $question->load($questionId);
        }

        Mage::register('current_question', $question);
        return $this;
    }


    public function newAction()
    {
        $this->_forward('edit');
    }

    public function editAction()
    {
        $this->_initQuestion();
        $this->loadLayout(array('default', 'editor', 'amfaq_uploads', 'amfaq_edit_form'));

        $question = Mage::registry('current_question');

        $this->_title($question->getId() ? $question->getTitle() : $this->__('New Question'));

        $this->_setActiveMenu('cms/amfaq/question');

        $this->_addContent($this->getLayout()->createBlock('amfaq/adminhtml_question_edit'));
        $this->_addLeft($this->getLayout()->createBlock('amfaq/adminhtml_question_edit_tabs'));

        $this->renderLayout();
    }

    protected function _saveFiles($question)
    {
        foreach ($question->files as $i => $data) {
            $file = Mage::getModel('amfaq/file');

            if (isset($data['file_id'])) {
                $file->load($data['file_id']);
                if ($file->getId() && $data['delete']) {
                    $file->delete();
                    continue;
                }
            }

            $file->setData($data);

            if (isset($_FILES['files']['name'][$i]['file'])
                && ($_FILES['files']['name'][$i]['file'] != "")) {
                $file->setUploadName($_FILES['files']['name'][$i]['file']);
                $file->setTmpName($_FILES['files']['tmp_name'][$i]['file']);
                if (!$file->getName()) {
                    $file->setName($_FILES['files']['name'][$i]['file']);
                }
            }
            if ($file->getId() || $file->getTmpName()) {
                $file->setQuestionId($question->getId());
                $file->save();
            }
        }
    }

    public function saveAction()
    {
        if ($data = $this->getRequest()->getPost()) {
            $id = intVal($this->getRequest()->getParam('question_id'));
            $model = Mage::getModel('amfaq/question')->load($id);
            if (!$model->getId() && $id) {
                Mage::getSingleton('adminhtml/session')->addError($this->__('This question no longer exists.'));
                $this->_redirect('*/*/');
                return;
            }

            if ($data['old_rating'] == $data['rating'])
                unset($data['rating']);

            $model->setData($data);

            $links = $this->getRequest()->getPost('product_ids');
            $productIds = Mage::helper('adminhtml/js')->decodeGridSerializedInput($links);
            if ($links)
                $model->setProductIds($productIds);

            $related = $this->getRequest()->getPost('question_ids');
            if ($related)
                $model->setQuestionIds(Mage::helper('adminhtml/js')->decodeGridSerializedInput($related));

            try {
                $model->save();
                
                $this->_saveFiles($model);
                
                if ($this->getRequest()->getParam('notify')) {
                    $helper = Mage::helper('amfaq');
                    $helper->notifyUser($model->getId());
                }

                Mage::getSingleton('adminhtml/session')->addSuccess($this->__('The question has been saved.'));

                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', array('id' => $model->getId(), '_current'=>true));
                    return;
                }
                $this->_redirect('*/*/');
                return;

            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setFormData($data);
                $this->_redirect('*/*/edit', array('question_id' => $this->getRequest()->getParam('question_id')));
                return;
            }
        }
        $this->_redirect('*/*/');
    }

    protected function _deleteQuestion($id)
    {
        $model = Mage::getModel('amfaq/question')->load($id);
        if ($model->getId())
        {
            $model->delete();
        }
    }

    public function deleteAction()
    {
        $id = Mage::app()->getRequest()->getParam('id');
        try {
            $this->_deleteQuestion($id);
            Mage::getSingleton('adminhtml/session')->addSuccess($this->__('Question deleted.'));
        } catch (Mage_Core_Exception $e) {
            $this->_getSession()->addError($e->getMessage());
        } catch (Exception $e) {
            Mage::logException($e);
            $this->_getSession()->addError($e->getMessage());
        }

        $this->_redirect('*/*/');
    }

    public function massDeleteAction()
    {
        $ids = Mage::app()->getRequest()->getParam('question');
        foreach ($ids as $id)
        {
            try {
                $this->_deleteQuestion($id);
            } catch (Mage_Core_Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            } catch (Exception $e) {
                Mage::logException($e);
                $this->_getSession()->addError($e->getMessage());
            }
        }
        Mage::getSingleton('adminhtml/session')->addSuccess($this->__('Questions deleted.'));
        $this->_redirect('*/*/');
    }

    public function relatedGridAction()
    {
        $this->_initQuestion();

        $grid = $this->getLayout()->createBlock('amfaq/adminhtml_question_edit_tab_products')
            ->setProductIds($this->getRequest()->getPost('product_ids', null));

        $this->getResponse()->setBody($grid->toHtml());
    }

    public function relatedQuestionsGridAction()
    {
        $this->_initQuestion();

        $grid = $this->getLayout()->createBlock('amfaq/adminhtml_question_edit_tab_related')
            ->setQuestionIds($this->getRequest()->getPost('question_ids', null));

        $this->getResponse()->setBody($grid->toHtml());
    }

    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('cms/amfaq/questions');
    }
}
