<?php
/**
 * 888888ba                 dP  .88888.                    dP                
 * 88    `8b                88 d8'   `88                   88                
 * 88aaaa8P' .d8888b. .d888b88 88        .d8888b. .d8888b. 88  .dP  .d8888b. 
 * 88   `8b. 88ooood8 88'  `88 88   YP88 88ooood8 88'  `"" 88888"   88'  `88 
 * 88     88 88.  ... 88.  .88 Y8.   .88 88.  ... 88.  ... 88  `8b. 88.  .88 
 * dP     dP `88888P' `88888P8  `88888'  `88888P' `88888P' dP   `YP `88888P' 
 *
 *                          m a g n a l i s t e r
 *                                      boost your Online-Shop
 *
 * -----------------------------------------------------------------------------
 * $Id$
 *
 * (c) 2010 - 2015 RedGecko GmbH -- http://www.redgecko.de
 *     Released under the MIT License (Expat)
 * -----------------------------------------------------------------------------
 */

class Redgecko_Magnalister_Block_Adminhtml_Widget_Grid_Column_Renderer_MarketplaceImage extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract{
    
    public function render(Varien_Object $oRow){
        require_once Mage::getModuleDir('','Redgecko_Magnalister').DIRECTORY_SEPARATOR.'Lib'.DIRECTORY_SEPARATOR.'Core'.DIRECTORY_SEPARATOR.'ML.php';
        ML::setFastLoad(true);
        $sLogo = MLOrder::factory()->set('current_orders_id', $oRow->getData($this->getColumn('magnalister')->getData('mlOrderId')))->getLogo();
        return $sLogo === null ? '' : '<div style="text-align:center;margin-top:3px;"><img src="'.$sLogo.'"></div>';
    }
    
}
