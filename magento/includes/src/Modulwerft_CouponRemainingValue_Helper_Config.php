<?php

/**
 * @category Modulwerft
 * @package Modulwerft_CouponRemainingValue
 * @author Modulwerft GmbH <info@modulwerft.com>
 * @author Benjamin Wunderlich <b.wunderlich@shopwerft.com>
 * @copyright 2014 Shopwerft GmbH (http://www.shopwerft.com)
 */
class Modulwerft_CouponRemainingValue_Helper_Config
    extends Modulwerft_CouponRemainingValue_Helper_Common_Config
{

    /**
     * Prefix for the config path for all config params
     */
    const CONFIG_PATH_PREFIX = 'checkout/coupon/';

    const CONFIG_EMAIL_ACTIVE = 'email_active';

    const CONFIG_CUSTOMER_NOTIFICATION_EMAIL_TEMPLATE = 'email_customer_notification_email_template';
    const CONFIG_CUSTOMER_NOTIFICATION_EMAIL_BCC      = 'email_customer_notification_email_bcc';

    const CONFIG_EMAIL_ACTIVE_EMPTY_COUPON   = 'email_active_empty_coupon';
    const CONFIG_EMAIL_TEMPLATE_EMPTY_COUPON = 'email_template_empty_coupon';
    const CONFIG_EMAIL_BCC_EMPTY_COUPON      = 'email_bcc_empty_coupon';

    const CONFIG_EMAIL_ACTIVE_RESET_COUPON_VALUE   = 'email_active_reset_coupon_value';
    const CONFIG_EMAIL_TEMPLATE_RESET_COUPON_VALUE = 'email_template_reset_coupon_value';
    const CONFIG_EMAIL_BCC_RESET_COUPON_VALUE      = 'email_bcc_reset_coupon_value';

    /**
     * Retrieve the prefix for the configuration paths.
     *
     * @return string
     */
    protected function _getConfigPathPrefix()
    {
        return self::CONFIG_PATH_PREFIX;
    }


    /**
     * Check if customer notification can be send due to module configuration in store.
     *
     * @param null $store
     * @return bool
     */
    public function isCustomerNotificationActive($store = null)
    {
        return $this->_getStoreConfigFlag(self::CONFIG_EMAIL_ACTIVE, $store);
    }

    /**
     * Retrieve the Id of the email template for customer notification.
     *
     * @param null|int $store
     * @return int|mixed
     */
    public function getCustomerNotificationEmailTemplate($store = null)
    {
        $template = $this->_getStoreConfig(self::CONFIG_CUSTOMER_NOTIFICATION_EMAIL_TEMPLATE, $store);
        if (is_numeric($template)) {
            return intval($template);
        }
        return $template;
    }

    /**
     * Retrieve the BCC email addresses for customer notification
     *
     * @param null|int $store
     * @return array The bcc email addresses
     */
    public function getCustomerNotificationEmailBcc($store = null)
    {
        $_bcc = $this->_getStoreConfig(self::CONFIG_CUSTOMER_NOTIFICATION_EMAIL_BCC, $store);
        return $this->_getEmailBcc($_bcc);
    }

    /**
     * Check if customer notification can be send due to module configuration in store.
     *
     * @param null $store
     * @return bool
     * @since 1.1.0
     */
    public function isEmailEmptyCouponActive($store = null)
    {
        return $this->_getStoreConfigFlag(self::CONFIG_EMAIL_ACTIVE_EMPTY_COUPON, $store);
    }

    /**
     * Retrieve the Id of the email template for customer notification.
     *
     * @param null|int $store
     * @return int|mixed
     * @since 1.1.0
     */
    public function getEmailTemplateEmptyCoupon($store = null)
    {
        $template = $this->_getStoreConfig(self::CONFIG_EMAIL_TEMPLATE_EMPTY_COUPON, $store);
        if (is_numeric($template)) {
            return intval($template);
        }
        return $template;
    }

    /**
     * Retrieve the BCC email addresses for customer notification
     *
     * @param null|int $store
     * @return array The bcc email addresses
     * @since 1.1.0
     */
    public function getEmailBccEmptyCoupon($store = null)
    {
        $_bcc = $this->_getStoreConfig(self::CONFIG_EMAIL_BCC_EMPTY_COUPON, $store);
        return $this->_getEmailBcc($_bcc);
    }

    /**
     * Check if customer notification can be send due to module configuration in store.
     *
     * @param null $store
     * @return bool
     * @since 1.1.0
     */
    public function isEmailResetCouponValueActive($store = null)
    {
        return $this->_getStoreConfigFlag(self::CONFIG_EMAIL_ACTIVE_RESET_COUPON_VALUE, $store);
    }

    /**
     * Retrieve the Id of the email template for customer notification.
     *
     * @param null|int $store
     * @return int|mixed
     * @since 1.1.0
     */
    public function getEmailTemplateResetCouponValue($store = null)
    {
        $template = $this->_getStoreConfig(self::CONFIG_EMAIL_TEMPLATE_RESET_COUPON_VALUE, $store);
        if (is_numeric($template)) {
            return intval($template);
        }
        return $template;
    }

    /**
     * Retrieve the BCC email addresses for customer notification
     *
     * @param null|int $store
     * @return array The bcc email addresses
     * @since 1.1.0
     */
    public function getEmailBccResetCouponValue($store = null)
    {
        $_bcc = $this->_getStoreConfig(self::CONFIG_EMAIL_BCC_RESET_COUPON_VALUE, $store);
        return $this->_getEmailBcc($_bcc);
    }


}
