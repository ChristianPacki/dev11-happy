<?php
/**
 * Magmex Advent Calendar image controller for enabling responsive design
 * and cache the images
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.magmex.com/MAGMEX_SOFTWARE_LICENSE.txt
 *
 * @copyright Copyright (c) 2015 magmex, Michael Stork, (http://www.magmex.com)
 * @license http://www.magmex.com/MAGMEX_SOFTWARE_LICENSE.txt
 * @category Magmex
 * @package Magmex_AdventCalendar
 * @author Michael Stork <m.stork@magmex.com>
 */
class Magmex_AdventCalendar_ImageController extends Mage_Core_Controller_Front_Action
{
    /**
     * Sets a JSON object/string containing the response of the request
     *
     * @return void
     */
    public function indexAction()
    {
        try {
            $absoluteImagePath = $this->getRequest()->getParam('absoluteImagePath');
            $imageWidth = $this->getRequest()->getParam('width');
            $imageHeight = $this->getRequest()->getParam('height');
            Mage::getModel('magmex_adventcalendar/image')->displayImage($absoluteImagePath, $imageWidth, $imageHeight);
        } catch (Exception $exception) {
            
        }
    }
}