<?php
/**
 * 888888ba                 dP  .88888.                    dP                
 * 88    `8b                88 d8'   `88                   88                
 * 88aaaa8P' .d8888b. .d888b88 88        .d8888b. .d8888b. 88  .dP  .d8888b. 
 * 88   `8b. 88ooood8 88'  `88 88   YP88 88ooood8 88'  `"" 88888"   88'  `88 
 * 88     88 88.  ... 88.  .88 Y8.   .88 88.  ... 88.  ... 88  `8b. 88.  .88 
 * dP     dP `88888P' `88888P8  `88888'  `88888P' `88888P' dP   `YP `88888P' 
 *
 *                          m a g n a l i s t e r
 *                                      boost your Online-Shop
 *
 * -----------------------------------------------------------------------------
 * $Id$
 *
 * (c) 2010 - 2015 RedGecko GmbH -- http://www.redgecko.de
 *     Released under the MIT License (Expat)
 * -----------------------------------------------------------------------------
 */

MLI18n::gi()->add('hitmeister_prepare_apply_form', array(
    'legend' => array(
        'details' => 'Produktdetails',
        'categories' => 'Kategorie',
        'variationmatching' => array('real.de Erforderliche Attribute', 'Attribut- und Attributswert-Matching'),
        'variationmatchingoptional' => array('real.de Optionale Attribute', 'Attribut- und Attributswert-Matching'),
        'unit' => 'Allgemeine Einstellungen',
    ),
    'field' => array(
        'variationgroups' => array(
            'label' => 'real.de Kategorien',
        ),
        'variationgroups.value' => array(
            'label' => '1. Marktplatz-Kategorie:',
        ),
        'webshopattribute' => array(
            'label' => 'Web-Shop Attribut',
        ),
        'attributematching' => array(
            'matching' => array(
                'titlesrc' => 'Shop-Wert',
                'titledst' => 'real.de-Wert',
            ),
        ),
        'title' => array(
            'label' => 'Titel',
        ),
        'subtitle' => array(
            'label' => 'Untertitel',
        ),
        'description' => array(
            'label' => 'Beschreibung',
        ),
        'images' => array(
            'label' => 'Produktbilder',
        ),
        'price' => array(
            'label' => 'Preis',
        ),
        'itemcondition' => array(
            'label' => 'Zustand',
        ),
        'shippingtime' => array(
            'label' => 'Lieferzeit',
        ),
        'itemcountry' => array(
            'label' => 'Artikel wird versandt aus',
        ),
        'comment' => array(
            'label' => 'Hinweise zu Ihrem Artikel',
        ),
    ),
),false);

MLI18n::gi()->add('hitmeister_prepare_variations', array(
    'legend' => array(
        'variations' => 'Variantengruppe von real.de ausw&auml;hlen',
        'attributes' => 'Attributsnamen von real.de ausw&auml;hlen',
        'variationmatching' => array('real.de Erforderliche Attribute', 'Attribut- und Attributswert-Matching'),
        'variationmatchingoptional' => array('real.de Optionale Attribute', 'Attribut- und Attributswert-Matching'),
        'action' => '{#i18n:form_action_default_legend#}',
    ),
    'field' => array(
        'variationgroups' => array(
            'label' => 'Variantengruppe',
        ),
        'variationgroups.value' => array(
            'label' => '1. Marktplatz-Kategorie:',
        ),
        'deleteaction' => array(
            'label' => '{#i18n:ML_BUTTON_LABEL_DELETE#}',
        ),
        'groupschanged' => array(
            'label' => '',
        ),
        'attributename' => array(
            'label' => 'Attributsnamen',
        ),
        'attributenameajax' => array(
            'label' => '',
        ),
        'customidentifier' => array(
            'label' => 'Bezeichner',
        ),
        'webshopattribute' => array(
            'label' => 'Web-Shop Attribut',
        ),
        'saveaction' => array(
            'label' => 'SPEICHERN UND SCHLIESSEN',
        ),
        'resetaction' => array(
            'label' => '{#i18n:hitmeister_varmatch_reset_matching#}',
            'confirmtext' => '{#i18n:hitmeister_prepare_variations_reset_info#}',
        ),
        'attributematching' => array(
            'matching' => array(
                'titlesrc' => 'Shop-Wert',
                'titledst' => 'real.de-Wert',
            ),
        ),
    ),
), false);

MLI18n::gi()->hitmeister_prepareform_max_length_part1 = 'Max length of';
MLI18n::gi()->hitmeister_prepareform_max_length_part2 = 'attribute is';
MLI18n::gi()->hitmeister_prepareform_category = 'Category attribute is mandatory.';
MLI18n::gi()->hitmeister_prepareform_title = 'Bitte geben Sie einen Titel an.';
MLI18n::gi()->hitmeister_prepareform_description = 'Bitte geben Sie eine Artikelbeschreibung an.';
MLI18n::gi()->hitmeister_prepareform_category_attribute = ' (Kategorie Attribute) ist erforderlich und kann nicht leer sein.';
MLI18n::gi()->hitmeister_category_no_attributes= 'Es sind keine Attribute f&uuml;r diese Kategorie vorhanden.';
MLI18n::gi()->hitmeister_prepare_variations_title = 'Attributes Matching';
MLI18n::gi()->hitmeister_prepare_variations_groups = 'real.de Gruppen';
MLI18n::gi()->hitmeister_prepare_variations_groups_custom = 'Eigene Gruppen';
MLI18n::gi()->hitmeister_prepare_variations_groups_new = 'Eigene Gruppe anlegen';
MLI18n::gi()->hitmeister_prepare_match_variations_no_selection = 'Bitte w&auml;hlen Sie eine Variantengruppe aus.';
MLI18n::gi()->hitmeister_prepare_match_variations_custom_ident_missing = 'Bitte w&auml;hlen Sie Bezeichner.';
MLI18n::gi()->hitmeister_prepare_match_variations_attribute_missing = 'Bitte w&auml;hlen Sie Attributsnamen.';
MLI18n::gi()->hitmeister_prepare_match_variations_category_missing = 'Bitte w&auml;hlen Sie Variantengruppe.';
MLI18n::gi()->hitmeister_prepare_match_variations_not_all_matched = 'Bitte weisen Sie allen real.de Attributen ein Shop-Attribut zu.';
MLI18n::gi()->hitmeister_prepare_match_notice_not_all_auto_matched = 'Es konnten nicht alle ausgewählten Werte gematcht werden. Nicht-gematchte Werte werden weiterhin in den DropDown-Feldern angezeigt. Bereits gematchte Werte werden in der Produktvorbereitung berücksichtigt.';
MLI18n::gi()->hitmeister_prepare_match_variations_saved = 'Erfolgreich gespeichert.';
MLI18n::gi()->hitmeister_prepare_variations_saved = 'Es wurden alle ausgewählten Werte erfolgreich gematcht. Sie können nun Produkte mit den gematchten Attributen vorbereiten, oder mit dem Matching hier fortfahren.';
MLI18n::gi()->hitmeister_prepare_variations_reset_success = 'Das Matching wurde aufgehoben.';
MLI18n::gi()->hitmeister_prepare_match_variations_delete = 'Wollen Sie die eigene Gruppe wirklich l&ouml;schen? Alle zugeh&ouml;rigen Variantenmatchings werden dann ebenfalls gel&ouml;scht.';
MLI18n::gi()->hitmeister_error_checkin_variation_config_empty = 'Variationen sind nicht konfiguriert.';
MLI18n::gi()->hitmeister_error_checkin_variation_config_cannot_calc_variations = 'Es konnten keine Variationen errechnet werden.';
MLI18n::gi()->hitmeister_error_checkin_variation_config_missing_nameid = 'Es konnte keine Zuordnung f&uuml;r das Shop Attribut "{#Attribute#}" bei der gew&auml;hlten Ayn24 Variantengruppe "{#MpIdentifier#}" f&uuml;r den Varianten Artikel mit der SKU "{#SKU#}" gefunden werden.';
MLI18n::gi()->hitmeister_prepare_variations_free_text = 'Eigene Angaben machen';
MLI18n::gi()->hitmeister_prepare_variations_additional_category = 'Zusätzliche Kategorie';
MLI18n::gi()->hitmeister_prepare_variations_error_text = 'Das Attribute {#attribute_name#} ist ein Pflichtfeld. Bitte ordnen Sie alle Werte zu.';
MLI18n::gi()->hitmeister_prepare_variations_error_missing_value = 'Attribute {#attribute_name#} is mandatory, and your product does not have value for chosen matched attribute from shop.';
MLI18n::gi()->hitmeister_prepare_variations_error_free_text = ': Das Freitext Feld darf nicht leer sein.';
MLI18n::gi()->hitmeister_prepare_variations_matching_table = 'Gematchte';
MLI18n::gi()->hitmeister_prepare_variations_manualy_matched = ' - (manuel zugeordnet)';
MLI18n::gi()->hitmeister_prepare_variations_auto_matched = ' - (automatisch zugeordnet)';
MLI18n::gi()->hitmeister_prepare_variations_free_text_add = ' - (eigene angaben)';
MLI18n::gi()->hitmeister_prepare_variations_reset_info = 'Wollen Sie das Matching wirklich aufheben?';
MLI18n::gi()->hitmeister_prepare_variations_change_attribute_info = 'Bevor Sie Attribute &auml;ndern k&ouml;nnen, heben Sie bitte alle Matchings zuvor auf.';
MLI18n::gi()->hitmeister_prepare_variations_additional_attribute_label = 'Eigene Attribute';
MLI18n::gi()->hitmeister_prepare_variations_separator_line_label = '-------------------------------------------------------------';
MLI18n::gi()->hitmeister_prepare_variations_mandatory_fields_info = '<b>Hinweis:</b> Die mit <span class="bull">&bull;</span> markierten Felder sind Pflichtfelder und m&uuml;ssen ausgef&uuml;llt werden.';
MLI18n::gi()->hitmeister_prepare_variations_already_matched = '(bereits gematcht)';
MLI18n::gi()->hitmeister_prepare_variations_category_without_attributes_info = 'Für die ausgewählte Kategorie unterstützt real.de keine Attribute.';

MLI18n::gi()->hitmeister_prepare_variations_choose_mp_value = 'Verwende real.de Attributswert';
MLI18n::gi()->hitmeister_prepare_variations_notice = 'Bitte beachten Sie, dass Sie einige Artikel der gew&auml;hlten Kategorie "{#category_name#}" abweichend unter „Produkte vorbereiten“ gematcht haben. Es werden die dort gespeicherten Werte zum Marktplatz &uuml;bermittelt.';
MLI18n::gi()->hitmeister_varmatch_attribute_changed_on_mp = 'Achtung: real.de hat einige bereits gematchte Attributswerte nachträglich entfernt oder geändert. 
Bitte überprüfen Sie Ihre Zuordnungen und matchen die betroffenen Werte bei Bedarf erneut.';
MLI18n::gi()->hitmeister_varmatch_attribute_different_on_product = 'Hinweis: Sie haben diesen Wert abweichend zum globalen Attributs-Matching gespeichert. 
Es werden die hier abweichend gespeicherten Werte zum Marktplatz übertragen.';
MLI18n::gi()->hitmeister_varmatch_attribute_deleted_from_mp = 'Dieses Attribut wurde von real.de gelöscht oder geändert. Matchings dazu wurden daher aufgehoben. 
Bitte matchen Sie bei Bedarf erneut auf ein geeignetes real.de Attribut.';
MLI18n::gi()->hitmeister_varmatch_attribute_value_deleted_from_mp = 'Dieser Attributswert wurde von real.de gelöscht oder geändert. Matchings dazu wurden daher aufgehoben. 
Bitte matchen Sie bei Bedarf erneut auf einen geeigneten real.de Attributswert.';

MLI18n::gi()->hitmeister_varmatch_define_name = 'Bitte geben Sie einen Bezeichner ein.';
MLI18n::gi()->hitmeister_varmatch_ajax_error = 'Ein Fehler ist aufgetreten.';
MLI18n::gi()->hitmeister_varmatch_all_select = 'Alle';
MLI18n::gi()->hitmeister_varmatch_please_select = 'Bitte w&auml;hlen...';
MLI18n::gi()->hitmeister_varmatch_auto_matchen = 'Auto-matchen';
MLI18n::gi()->hitmeister_varmatch_reset_matching = 'Matchen aufheben';
MLI18n::gi()->hitmeister_varmatch_delete_custom_title = 'Varianten-Matching-Gruppe l&ouml;schen';
MLI18n::gi()->hitmeister_varmatch_delete_custom_content = 'Wollen Sie die eigene Gruppe wirklich l&ouml;schen?<br />Alle zugeh&ouml;rigen Variantenmatchings werden dann ebenfalls gel&ouml;scht.';

