<?php
/**
 * 888888ba                 dP  .88888.                    dP                
 * 88    `8b                88 d8'   `88                   88                
 * 88aaaa8P' .d8888b. .d888b88 88        .d8888b. .d8888b. 88  .dP  .d8888b. 
 * 88   `8b. 88ooood8 88'  `88 88   YP88 88ooood8 88'  `"" 88888"   88'  `88 
 * 88     88 88.  ... 88.  .88 Y8.   .88 88.  ... 88.  ... 88  `8b. 88.  .88 
 * dP     dP `88888P' `88888P8  `88888'  `88888P' `88888P' dP   `YP `88888P' 
 *
 *                          m a g n a l i s t e r
 *                                      boost your Online-Shop
 *
 * -----------------------------------------------------------------------------
 * $Id$
 *
 * (c) 2010 - 2014 RedGecko GmbH -- http://www.redgecko.de
 *     Released under the MIT License (Expat)
 * -----------------------------------------------------------------------------
 */
MLFilesystem::gi()->loadClass('Form_Controller_Widget_Form_PrepareAbstract');
class ML_Idealo_Controller_Idealo_Prepare_Form extends ML_Form_Controller_Widget_Form_PrepareAbstract {
    
    protected $aParameters = array('controller');
    
    protected function getSelectionNameValue() {
        return 'match';
    }
    
    /**
     * @todo verify addItems
     * @return boolean
     */
    protected function triggerBeforeFinalizePrepareAction() {
        $this->oPrepareList->set('verified', 'OK');
        return true;
    }
    
    public function render() {
        $this->getFormWidget();
        return $this;
    }
    protected function titleField(&$aField) {
        $aField['type'] = 'string';
    }
    
    protected function descriptionField(&$aField) {
        $aField['type'] = 'wysiwyg';
    }
    protected function imageField(&$aField) { 
            $aField['type'] = 'imagemultipleselect';
    }
    
    protected function shippingTimeField (&$aField) {
        $aField['values'] = ML::gi()->instance('controller_idealo_config_prepare')->getField('shippingtime', 'values');
    }
    
    protected function shippingCountryField (&$aField) {
        $aField['values'] = array();
        try{
            $aData = MagnaConnector::gi()->submitRequestCached(array(
               'ACTION' => 'GetCountries', 
               'SUBSYSTEM' => 'Core', 
               'DATA' => array(
                   'Language' => MLModul::gi()->getConfig('marketplace.lang')
               )
            ), 60 * 60 * 24 * 30);
            if($aData['STATUS'] == 'SUCCESS' && isset($aData['DATA'])){
                $aField['values'] = $aData['DATA'];
            }
        }  catch (Exception $oEx){}
         
    }
    
    public function shippingCostMethodField(&$aField) { 
       
        $aField['values'] = array(
            '__ml_lump' =>  array(
                'title' => MLI18n::gi()->ML_COMPARISON_SHOPPING_LABEL_LUMP,
                'textoption' => true
            )
	);
        $aField['values']['__ml_weight'] = array(
                'title' => MLI18n::gi()->idealo_config_shippingcosts_eq_articleweight,
                'textoption' => false
        );
    }
    
    
}