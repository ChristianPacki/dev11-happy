<?php

# HappyFabric
# Original NOT DECODED
# 13/12/2017 Updated for Group Discounts
# 28/02/2018 When there is no coupon make discount on same row
# 16/03/2018 Updated
# 11/04/2018 getPos8 4127 if VATID and 4121 if not EU, getPos40 returns VATID if exists
# 23/04/2018 Shipping if VATID, europe: 4833 else 4832

class Valkanis_Datevproduct_Adminhtml_DatevproductController extends Mage_Adminhtml_Controller_Action {

    private $csvDelimeter = ';';

    public function indexAction() {
        $this->loadLayout()->renderLayout();
    }

    protected function _isAllowed() {
        return Mage::getSingleton('admin/session')->isAllowed('sales/datevproduct_adminform');
    }

    public function postAction() {
        // $invoices = Mage::getModel('sales/order_invoice_item')->getCollection()->addAttributeToFilter('created_at', array('from' => '2014-11-18 20:33:53', 'to' => '2014-11-19 16:07:39', 'date' => true,));
        // // ->addAttributeToFilter('status', array('eq' => Mage_Sales_Model_Order::STATE_COMPLETE));
        // //$this->fromDate = date('Y-m-d H:i:s', strtotime($this->fromDate));
        // '2014-11-18 20:33:53'
        # Table sales_flat_invoice_item
        $dayFrom = $this->getRequest()->getPost('date_from', date('01/m/Y'));
        $dayTo = $this->getRequest()->getPost('date_to', date('d/m/Y'));
        $dayTo = $this->getRequest()->getPost('date_to', date('d/m/Y'));
        $type = $this->getRequest()->getPost('type', 'invoice');
        switch ($type) {
            case 'credit':
                $datev = new DatevCredit;
                break;
            default :
                $datev = new DatevInvoice;
                break;
        }
        $datev->setFromDate($dayFrom);
        $datev->setToDate($dayTo);
        $orders_invoice = $datev->getDocs();
        $export = array();
        foreach ($orders_invoice as $val) {
            $datev->setInvoice($val);
            $export = array_merge($export, $datev->getCSVRows());
        }

        setlocale(LC_ALL, 'de_DE.iso88591');
        $fileName = $datev->getCsvFilename();
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header('Content-Description: File Transfer');
        header('Content-Encoding: ISO-8859-1');
        header("Content-type: text/csv;charset=ISO-8859-1");
        header("Content-Disposition: attachment; filename={$fileName}");
        header("Expires: 0");
        header("Pragma: public");
        $fh = @fopen('php://output', 'w');
        stream_filter_register('crlf', 'crlf_filter');
        stream_filter_append($fh, 'crlf');
//        fprintf($fh, chr(239) . chr(187) . chr(191));

        fputcsv($fh, array_map("utf8_decode", $datev->getCsvHeader()), $this->csvDelimeter, ' ');
        fputcsv($fh, array_map("utf8_decode", $datev->getUserCsvHeader()), $this->csvDelimeter, ' ');
        foreach ($export as $data) {
            fputcsv($fh, array_map("utf8_decode", $data), $this->csvDelimeter, ' ');
        }
        fclose($fh);
        die();
        $post = $this->getRequest()->getPost();

        try {
            if (empty($post)) {
                Mage::throwException($this->__('Invalid form data.'));
            }

            /* here's your form processing */

            $message = $this->__('Your form has been submitted successfully.');
            Mage::getSingleton('adminhtml/session')->addSuccess($message);
        } catch (Exception $e) {
            Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
        }
        $this->_redirect('*/*');
    }

    function encodeCSV(&$value, $key) {
        $value = iconv('UTF-8', 'Windows-1252', $value);
    }

}

class DatevInvoice {

    protected $kostenStelle = [];
    protected $columnCount = 90;
    protected $productCount = 0;
    protected $invoice;
    protected $order;
    protected $fromDate;
    protected $toDate;
    protected $paymethodCode = '';
    protected $billingAddress;
    protected $shippingAddress;
    protected $thousandSeperator = '';
    protected $decSeperator = ',';
    # New
    protected $paymentExtOrderId = '';
    protected $paymentTransactionId = '';

    public function getPos2() {
        return '"S"';
    }

    public function getPos3() {
        return '"' . $this->invoice->getBaseCurrencyCode() . '"';
    }

    public function getPos7() {
        switch ($this->paymethodCode) {
            case 'amazon';
                $return = '31967';
                break;
            case 'm2epropayment';
                $array = unserialize($this->order->getPayment()->getAdditionalData());
                if ($array['component_mode'] === 'amazon') {
                    $return = '31967';
                } elseif ($array['payment_method'] === 'PayPal') {
                    $return = '31965';
                } else {
                    $return = '31965';
                }
                break;
            case 'paypal_express':
                $return = '31965';
                break;
            case 'payone_advance_payment';
            case 'payone_cash_on_delivery';
            case 'payone_debit_payment';
            case 'payone_invoice';
            case 'payone_safe_invoice';
            case 'payone_online_bank_transfer';
            case 'payone_barzahlen';
            case 'payone_ratepay';
            case 'payone_payolution';
            default :
                $return = '31965';
                break;
        }
        return '"' . $return . '"';
    }

    public function getPos8() {
        #Mage_Sales_Model_Order_Address
        if ($this->billingAddress->getVatId()) {
            return '4127';
        }
        if ($this->shippingAddress) {
            $country = $this->shippingAddress->getCountry();
        } elseif ($this->billingAddress) {
            $country = $this->billingAddress->getCountry();
        } else {
            return '';
        }
        if ($country === 'DE') {
            return '4401';
        } elseif (!Mage::helper('core')->isCountryInEU($country)) {
            return '4121';
        } else {
            return '4401';
        }
    }

    public function getPos10() {
        $date = DateTime::createFromFormat('Y-m-d H:i:s', $this->invoice->getCreatedAt());
        return $date->format('dm');
    }

    public function getPos11() {

        return '"' . $this->invoice->getIncrementId() . '"';
    }

    public function getPos14() {
        return '"' . $this->billingAddress->getName() . '"';
    }

    public function getPos21() {
        return '"Bestell Nr."';
    }

    public function getPos22() {
        return '"' . $this->order->getIncrementId() . '"';
    }

    public function getPos23() {
        return '"Abrechnung Nr."';
    }

    public function getPos24() {
        switch ($this->paymethodCode) {
            case 'payone_advance_payment';
            case 'payone_cash_on_delivery';
            case 'payone_creditcard';
            case 'payone_creditcard_iframe';
            case 'payone_debit_payment';
            case 'payone_invoice';
            case 'payone_safe_invoice';
            case 'payone_online_bank_transfer';
            case 'payone_barzahlen';
            case 'payone_ratepay';
            case 'payone_payolution';
                $lastTransactionId = $this->order->getPayment()->getLastTransId();
                $return = !empty($lastTransactionId) ? $lastTransactionId : $this->order->getIncrementId();
            case 'amazon';
                $return = $this->paymentExtOrderId;
            case 'm2epropayment';
                $array = unserialize($this->order->getPayment()->getAdditionalData());
                if ($array['component_mode'] === 'amazon') {
                    $return = $array['channel_order_id'];
                } elseif ($array['payment_method'] === 'PayPal') {
                    $newTranDate = new DateTime('1900-01-01');
                    $transactionId = NULL;
                    foreach ($array['transactions'] as $transaction) {
                        $tranDate = new DateTime($transaction['transaction_date']);
                        if ($tranDate >= $newTranDate) {
                            $newTranDate = $tranDate;
                            $transactionId = $transaction['transaction_id'];
                        }
                    }
                    $return = $transactionId;
                } else {
                    $return = $this->order->getIncrementId();
                }
            case 'paypal_express':
                $return = $this->paymentTransactionId;
            default :
                $return = $this->order->getIncrementId();
        }
        return '"' . $return . '"';
    }

    public function getPos40() {
        if ($this->billingAddress->getVatId()) {
            return '"' . $this->billingAddress->getVatId() . '"';
        }
        if ($this->shippingAddress) {
            $country = $this->shippingAddress->getCountry();
        } elseif ($this->billingAddress) {
            $country = $this->billingAddress->getCountry();
        } else {
            return '""';
        }
        if (Mage::helper('core')->isCountryInEU($country) && $country !== 'DE') {
            return '"' . $country . '"';
        }
        return '""';
    }

    // $data = $this->order->getPayment()->getMethodInstance()->getData();
    // $this->invoiceData = $this->invoice->getData();
    // $this->orderData = $this->order->getData();
    /**
     *  
     * Getters / Setters
     * 
     */
    public function getDocs() {
        return Mage::getModel("sales/order_invoice")->getCollection()
                        ->addAttributeToSort('created_at', 'desc')
                        ->addAttributeToFilter('created_at', array(
                            'from' => $this->fromDate->format('Y-m-d 00:00:00'),
                            'to' => $this->toDate->format('Y-m-d 23:59:59'),
                            'date' => true,))
        // ->setPageSize(50)
        ;
    }

    public function setInvoice($invoice) {
        $this->invoice = $invoice;
        $this->order = Mage::getModel('sales/order')->load($this->invoice["order_id"]);
        try {
            $this->paymethodCode = $this->order->getPayment()->getMethodInstance()->getCode();

            switch ($this->paymethodCode) {
                case 'paypal_express':
                    $this->paymentTransactionId = $this->invoice->getTransactionId();
                    break;
                case 'magnalister':
                    $this->paymentExtOrderId = $this->order->getExtOrderId();
                    # Getting data from DB
                    $resource = Mage::getSingleton('core/resource');
                    $readConnection = $resource->getConnection('core_read');
                    $mgn = $readConnection->fetchAll('SELECT * FROM magnalister_orders WHERE special = "' . $this->paymentExtOrderId . '" LIMIT 1');
                    if ($mgn) {
                        if ($mgn[0]['platform'] === 'ebay') {
                            if (($arr = json_decode($mgn[0]['orderData'], true))) {
                                foreach ($arr['Totals'] as $ttl) {
                                    if ($ttl['Type'] === 'Payment' && $ttl['Code'] === 'PayPal') {
                                        $this->paymethodCode = 'paypal_express';
                                        if (array_key_exists('ExternalTransactionID', $ttl)) {
                                            $this->paymentTransactionId = $ttl['ExternalTransactionID'];
                                        }
                                    }
                                }
                            }
                        } elseif ($mgn[0]['platform'] === 'amazon') {
                            $this->paymethodCode = 'amazon';
                        }
                    }
                    break;
            }
        } catch (Exception $e) {
            // var_dump($e->getCode());
        }
        $this->billingAddress = $this->order->getBillingAddress();
        $this->shippingAddress = $this->order->getShippingAddress();
    }

    public function setFromDate($date, $format = 'd/m/Y') {
        $this->fromDate = DateTime::createFromFormat($format, $date);
    }

    public function setToDate($date, $format = 'd/m/Y') {
        $this->toDate = DateTime::createFromFormat($format, $date);
    }

    public function getFromDate() {
        return $this->fromDate;
    }

    public function getToDate() {
        return $this->toDate;
    }

    public function getCsvHeader() {
        $fromMonth = intval($this->fromDate->format('m'));
        if ($fromMonth <= 6) {
            $out = (intval($this->fromDate->format('Y')) - 1) . '0701';
        } else {
            $out = $this->fromDate->format('Y') . '0701';
        }
        return array(
            '"EXTF"', // 1
            '510', // 2
            '21', // 3
            '"Buchungsstapel"', //4
            '7', // 5
            $this->fromDate->format('YmdHis000') . '000', // milisecs
            '', // 7 (Empty)
            '"SV"', // 8
            '"Datev"', // 9
            '""', // 10 (Empty)
            '220793', // 11 (?)
            '11422', // 12 (?)
            $out, // 13 
            '4', //14
            // Extra
            $this->fromDate->format('Ymd'), // 15
            $this->toDate->format('Ymd'), // 16
            '"' . utf8_encode(strftime('%B %Y', $this->fromDate->format('U'))) . '"', // 17
            '"EL"', //18
            '1', //19
            '0', //20
            '1', // 21 (Empty)
            '"EUR"', // 22 (Currency)
            '', // 23 (Empty)
            '"MP"', // 24 (Empty)
            '', // 25 (Empty)
            '192755', // 26 (Empty)
            '"04"', // 27 (Empty)
            '', // 28 (Empty)
            '', // 29 (Empty)
            '""', // 30 (Empty)
            '""', // 31 (Empty)
        );
    }

    public function getCsvFilename() {
        return 'EXTF_Datev_Rechnungen_' . $this->fromDate->format('Ymd') . '_' . $this->toDate->format('Ymd') . '.csv';
    }

    public function getUserCsvHeader() {
        $names = array();
        $userHeader = $this->getUserCsvHeaderArray();
        for ($i = 1; $i < $this->columnCount; $i++) {
            $names[] = isset($userHeader[$i]) ? $userHeader[$i] : '';
        }

        return $names;
    }

    protected function getUserCsvHeaderArray() {
        return array(
            1 => 'Umsatz',
            2 => 'Soll/Haben-Kennzeichen',
            3 => 'WKZ Umsatz',
            4 => 'Kurs',
            5 => 'Basis-Umsatz',
            6 => 'WKZ Basis-Umsatz',
            7 => 'Konto',
            8 => 'Gegenkonto (ohne BU-Schlüssel)',
            9 => 'BU-Schlüssel',
            10 => 'Belegdatum',
            11 => 'Belegfeld 1',
            12 => 'Belegfeld 2',
            13 => 'Skonto',
            14 => 'Buchungstext',
            15 => 'Postensperre',
            16 => 'Diverse Adressnummer',
            17 => 'Geschäftspartnerbank',
            18 => 'Sachverhalt',
            19 => 'Zinssperre',
            20 => 'Beleglink',
            21 => 'Beleginfo - Art 1',
            22 => 'Beleginfo - Inhalt 1',
            23 => 'Beleginfo - Art 2',
            24 => 'Beleginfo - Inhalt 2',
            25 => 'Beleginfo - Art 3',
            26 => 'Beleginfo - Inhalt 3',
            27 => 'Beleginfo - Art 4',
            28 => 'Beleginfo - Inhalt 4',
            29 => 'Beleginfo - Art 5',
            30 => 'Beleginfo - Inhalt 5',
            31 => 'Beleginfo - Art 6',
            32 => 'Beleginfo - Inhalt 6',
            33 => 'Beleginfo - Art 7',
            34 => 'Beleginfo - Inhalt 7',
            35 => 'Beleginfo - Art 8',
            36 => 'Beleginfo - Inhalt 8',
            37 => 'KOST1 - Kostenstelle',
            38 => 'KOST2 - Kostenstelle',
            39 => 'Kost-Menge',
            40 => 'EU-Land u. UStID',
        );
    }

//        return count($this->invoice->getAllItems());
//        $items = $this->invoice->getAllItems();
//
//        foreach ($items as $_item):
//            echo $_item->getRowTotalInclTax();
//            echo $_item->getPrice();
//        endforeach;
//        return $this->productCount;

    /**
     * 
     * Returns a generic row based on position getters
     * 
     */
    protected function getGenericRow() {
        $row = array();
        for ($i = 1; $i < $this->columnCount; $i++) {
            $methodName = 'getPos' . $i;
            $val = '';
            if (method_exists($this, $methodName)) {
                $val = utf8_decode($this->$methodName());
            }
            $row[$i] = empty($val) ? '""' : $val;
        }
        return $row;
    }

    public function getCSVRows() {
        $storeId = 1;
        $config = Mage::getModel('eav/config');
        $attribute = $config->getAttribute(Mage_Catalog_Model_Product::ENTITY, 'datev_kostenstelle');
        $values = $attribute->setStoreId($storeId)->getSource()->getAllOptions();
        foreach ($values as $val) {
            $this->kostenStelle[$val['value']] = '"' . $val['label'] . '"';
        }
        $csvRow = array();
        $items = $this->invoice->getAllItems();
        # Getting Product Row
        $hasCoupon = $this->order->getCouponCode() ? true : false;
        foreach ($items as $_item) {
            if (empty(floatval($_item->getRowTotalInclTax()))) {
                continue;
            }
            $csvRow[] = $this->getCSVInvoiceRow($_item, $hasCoupon);
        }
        $shipping = $this->getCSVShippingRow();
        if ($shipping) {
            $csvRow[] = $shipping;
        }
        if (($discount = abs($this->invoice->getDiscountAmount())) && $hasCoupon) {
            $csvRow[] = $this->generateDiscountRow($discount, $this->order->getCouponCode());
        }

        return $csvRow;
    }

    /**
     * 
     * Returns the shipping row for CSV
     * 
     */
    protected function getCSVShippingRow() {
        $val = floatval($this->invoice->getShippingAmount()) + floatval($this->invoice->getShippingTaxAmount());
        if (!$val) {
            return false;
        }
        $row = $this->getGenericRow();
        $row[1] = number_format($val, 2, $this->decSeperator, $this->thousandSeperator);
        $konto = 4831;
        if ($this->billingAddress->getVatId()) {
            if ($this->shippingAddress) {
                $country = $this->shippingAddress->getCountry();
            } elseif ($this->billingAddress) {
                $country = $this->billingAddress->getCountry();
            } else {
                $country = '';
            }
            if ($country !== 'DE') {
                $konto = Mage::helper('core')->isCountryInEU($country) ? 4833 : 4832;
            }
        }
        $row[8] = $konto;
        $row[9] = '3'; // Tax
        $row[37] = 500;
        return $row;
    }

    /**
     * 
     * Returns the shipping row for CSV
     * 
     */
    protected function generateDiscountRow($amount, $discountCode = false) {
        $row = $this->getGenericRow();
        $row[1] = number_format(abs($amount), 2, $this->decSeperator, $this->thousandSeperator);
        $row[7] = '99999';
        $row[8] = '31965';
        if ($discountCode) {
            $row[12] = '"' . $discountCode . '"';
        }
        return $row;
    }

    /**
     * 
     * Returns the product row for CSV
     * 
     */
    protected function getCSVInvoiceRow($_item, $hasCoupon) {
        $row = $this->getGenericRow();
        $rowAmount = floatval($_item->getRowTotalInclTax());
        if (!$hasCoupon) {
            $rowAmount -= floatval($_item->getDiscountAmount());
        }

        $row[1] = number_format($rowAmount, 2, $this->decSeperator, $this->thousandSeperator);
        /**
         * get attribute collection
         */
//            $storeId = 1;
//            $config = Mage::getModel('eav/config');
//            $attribute = $config->getAttribute(Mage_Catalog_Model_Product::ENTITY, 'datev_kostenstelle');
//            $values = $attribute->setStoreId($storeId)->getSource()->getAllOptions();
//            print_r($values);
        //here is another method
//            $options = Mage::getResourceModel('eav/entity_attribute_option_collection');
//            $values = $options->setAttributeFilter($attribute->getId())->setStoreFilter($storeId)->toOptionArray();
//            print_r($values);
//
//            die();
        $selected = Mage::getResourceModel('catalog/product')->getAttributeRawValue($_item->getProductId(), 'datev_kostenstelle', 0);
        if (isset($this->kostenStelle[$selected])) {
            $row[37] = $this->kostenStelle[$selected];
        }
        return $row;
    }

}

class DatevCredit extends DatevInvoice {

    public function getPos7() {
        return parent::getPos8();
    }

    public function getPos8() {
        return '31965';
    }

    public function getDocs() {
        return Mage::getModel("sales/order_creditmemo")->getCollection()
                        ->addAttributeToSort('

            created_at', '

            desc')
                        ->addAttributeToFilter('created_at', array(
                            'from' => $this->getFromDate()->format('Y-m-d 00:00:00'),
                            'to' => $this->getToDate()->format('Y-m-d 23:59:59'),
                            'date' => true,))
        // ->setPageSize(50)
        ;
    }

    public function getCsvFilename() {
        return 'EXTF_Datev_Gutschriften_' . $this->fromDate->format('Ymd') . '_' . $this->toDate->format('Ymd') . '.csv';
    }

}

class crlf_filter extends php_user_filter {

    function filter($in, $out, &$consumed, $closing) {
        while ($bucket = stream_bucket_make_writeable($in)) {
            // make sure the line endings aren't already CRLF
            $bucket->data = preg_replace("/(?<!\r)\n/", "\r\n", $bucket->data);
            $consumed += $bucket->datalen;
            stream_bucket_append($out, $bucket);
        }
        return PSFS_PASS_ON;
    }

}
