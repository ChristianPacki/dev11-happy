<?php
/**
 * Mirasvit
 *
 * This source file is subject to the Mirasvit Software License, which is available at https://mirasvit.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Mirasvit
 * @package   mirasvit/extension_rewards
 * @version   1.1.25
 * @copyright Copyright (C) 2017 Mirasvit (https://mirasvit.com/)
 */



class Mirasvit_RewardsSocial_TwitterController extends Mage_Core_Controller_Front_Action
{
    public function _getCustomer()
    {
        return Mage::getSingleton('customer/session')->getCustomer();
    }

    public function tweetAction()
    {
        $url = $this->getRequest()->getParam('url');
        $result = Mage::helper('rewards/behavior')->addToQueueRule(
            Mirasvit_Rewards_Model_Config::BEHAVIOR_TRIGGER_TWITTER_TWEET, $this->_getCustomer(), false, $url
        );

        if ($result) {
            echo Mage::helper('rewardssocial')->__(
                "You'll get your%s for Tweet shortly!",
                Mage::helper('rewards')->formatPoints('')
            );
            die;
        }
    }
}
