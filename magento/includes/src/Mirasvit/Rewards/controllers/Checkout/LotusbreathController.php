<?php
/**
 * Mirasvit
 *
 * This source file is subject to the Mirasvit Software License, which is available at https://mirasvit.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Mirasvit
 * @package   mirasvit/extension_rewards
 * @version   1.1.25
 * @copyright Copyright (C) 2017 Mirasvit (https://mirasvit.com/)
 */



require_once Mage::getBaseDir('code').'/local/Lotusbreath/OneStepCheckout/controllers/IndexController.php';

class Mirasvit_Rewards_Checkout_LotusbreathController extends Lotusbreath_OneStepCheckout_IndexController
{
    //this code is from Lotusbreath
    public function applyPointsAction()
    {
        $response = Mage::helper('rewards/checkout')->processRequest();

        $saveCouponResult = array();
        if ($response['success']) {
            $saveCouponResult['success'] = true;
            $saveCouponResult['message'] = $response['message'];
        } elseif ($response['message']) {
            $saveCouponResult['success'] = false;
            $saveCouponResult['message'] = $response['message'];
        }

        $this->_savePayment();
        $this->_saveShippingMethod();

        $quote = $this->getOnepage()->getQuote();

        try {
            $quote->getShippingAddress()->setCollectShippingRates(true);
            //$quote->setCouponCode(strlen($couponCode) ? $couponCode : '');

            $quote
                ->setTotalsCollectedFlag(false)
                ->collectTotals()
                ->save();
            //$this->_requireUpdateQuote();
        } catch (Mage_Core_Exception $e) {
            //$this->_getSession()->addError($e->getMessage());
            $saveCouponResult['success'] = false;
            $saveCouponResult['message'] = $e->getMessage();
        } catch (Exception $e) {
            $saveCouponResult['success'] = false;
            $saveCouponResult['message'] = Mage::helper('checkout/cart')->__('Cannot apply the points.');
            Mage::logException($e);
        }

        $return = array(
            'results' => $saveCouponResult,
            //'update_items' => array('shipping_partial', 'payment_partial', 'review_partial' ),
            'update_items' => array('review_partial', 'payment_partial', 'shipping_partial'),
            'htmlUpdates' => array(
                'review_partial' => $this->_getReviewHtml(),
                'shipping_partial' => $this->_getShippingMehodHtml(),
                'payment_partial' => $this->_getPaymentHtml(),
            ),
        );
        $this->getResponse()
            ->clearHeaders()
            ->setHeader('Content-Type', 'application/json')
            ->setBody(Mage::helper('core')->jsonEncode($return));
    }

//    //this code is from firecheckout
//    public function applyPointsAction()
//    {
//        if (!$this->getRequest()->isPost()) {
//            return;
//        }
//        $session = Mage::getSingleton('core/session');
//        $response = Mage::helper('rewards/checkout')->processRequest();
//        if ($response['success']) {
//            $session->addSuccess($response['message']);
//        } elseif ($response['message']) {
//            $session->addError($response['message']);
//        }
//        $result = array();
//
//        $quote    = $this->getCheckout()->getQuote();
//        $oldTotal = $quote->getBaseGrandTotal();
//        $sections = array();
//
//        $quote->collectTotals();
//        $sections[] = 'review';
//        if (Mage::getStoreConfig('firecheckout/ajax_update/shipping_method_on_total')) {
//            $sections[] = 'shipping-method';
//            $quote->getShippingAddress()->setCollectShippingRates(true)->collectShippingRates();
//            $quote->setTotalsCollectedFlag(false)->collectTotals();
//        }
//
//        if (Mage::getStoreConfig('firecheckout/ajax_update/payment_method_on_total')
//            || $quote->getBaseGrandTotal() <= 0 || $oldTotal <= 0) {
//
//            $sections[] = 'payment-method';
//            $this->getOnepage()->applyPaymentMethod();
//
//            if (Mage::getStoreConfig('firecheckout/ajax_update/total_on_payment_method')) {
//                $quote->getShippingAddress()->setDiscountDescriptionArray(array())->isObjectNew(true);
//                $quote->setTotalsCollectedFlag(false)->collectTotals();
//            }
//        }
//
//        $quote->save();
//        $result['update_section'] = $this->_renderSections($sections);
//        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
//    }
}
