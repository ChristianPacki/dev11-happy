<?php
/**
 * Magmex MySQL model for Advent Calendar day action product discount table
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.magmex.com/MAGMEX_SOFTWARE_LICENSE.txt
 *
 * @copyright Copyright (c) 2015 magmex, Michael Stork, (http://www.magmex.com)
 * @license http://www.magmex.com/MAGMEX_SOFTWARE_LICENSE.txt
 * @category Magmex
 * @package Magmex_AdventCalendar
 * @author Michael Stork <m.stork@magmex.com>
 */

class Magmex_AdventCalendar_Model_Mysql4_Day_Action_Productdiscount extends Mage_Core_Model_Mysql4_Abstract
{
    /**
     * Construct
     * Inits the advent calendar day action product discount table
     *
     * @see Mage_Core_Model_Resource_Abstract::_construct()
     * @return void
     */
    public function _construct()
    {
        $this->_init('magmex_adventcalendar/day_action_product_discount', 'advent_calendar_day_action_product_discount_id');
    }
}