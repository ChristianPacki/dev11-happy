<?php
/**
 * Magmex Adventcalendar block for the showing Advent Calendars
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.magmex.com/MAGMEX_SOFTWARE_LICENSE.txt
 *
 * @copyright Copyright (c) 2015 magmex, Michael Stork, (http://www.magmex.com)
 * @license http://www.magmex.com/MAGMEX_SOFTWARE_LICENSE.txt
 * @category Magmex
 * @package Magmex_AdventCalendar
 * @author Michael Stork <m.stork@magmex.com>
 */
class Magmex_AdventCalendar_Block_Adventcalendar extends Mage_Core_Block_Template
{
    /**
     * Advent Calendar model instance
     *
     * @var Magmex_AdventCalendar_Model_Adventcalendar
     */
    protected $_modelAdventCalendar;
    
    /**
     * Construct
     * Set template for outputting an Advent Calendar
     *
     * @return void
     */
    public function __construct()
    {
        $this->_init();
        parent::__construct();
    }
    
    /**
     * Initalizes all necessary layout things
     *
     * @return Magmex_AdventCalendar_Block_Adventcalendar chaining
     */
    protected function _init()
    {
        $this->setTemplate('magmex/adventcalendar/adventcalendar.phtml');
        $this->_modelAdventCalendar = Mage::getModel('magmex_adventcalendar/adventcalendar')->loadAdventCalendarForActualYear();
    }
    
    /**
     * Prepares the layout of the Advent Calendar and adds the CSS
     *
     * @see Mage_Adminhtml_Block_Widget_Grid_Container::_prepareLayout()
     * @return Mage_Core_Block_Abstract chaining
     */
    protected function _prepareLayout()
    {
        $this->getLayout()->getBlock('head')->addCss('css/magmex/adventcalendar/adventcalendar.css');
        return parent::_prepareLayout();
    }

    /**
     * Returns the magmex script tag
     *
     * @return string script tags html string
     */
    public function getScriptTags()
    {
        $scriptBody = '<script type="text/javascript" src="' . Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_JS) . 'Magmex/AdventCalendar/%s"></script>';
        $scriptTagsHtml = sprintf($scriptBody, 'JQueryNoConflict.js');
        $scriptTagsHtml .= sprintf($scriptBody, 'jquery-1.8.2.min.js');
        $scriptTagsHtml .= sprintf($scriptBody, 'jquery.blockUI.js');
        $scriptTagsHtml .= sprintf($scriptBody, 'AdventCalendar.js');
        return $scriptTagsHtml;
    }
    
    /**
     * Returns the loaded advent calendar model for usage in the template
     *
     * @return Magmex_AdventCalendar_Model_Adventcalendar advent calendar instance
     */
    public function getAdventCalendar()
    {
        return $this->_modelAdventCalendar;
    }
    
    /**
     * Returns the CSS class name the title container of the shown advent calendar - necessary,
     * because dependent on the chosen background image, the title has to be positioned differently and gets
     * different fonts
     *
     * @return string CSS class name
     */
    public function getTitleCssClass()
    {
        $standardBackgroundImageFilename = basename($this->_modelAdventCalendar->getStandardBackgroundImage());
        $newNameBase = substr($standardBackgroundImageFilename, 0, strpos($standardBackgroundImageFilename, '.'));
        $splittedName = explode('_', $newNameBase);
        $cssClassName = '';
        
        //lcfirst is not usable because it's availabe > PHP version 5.3.0 - therefore we have to use the foreach $counter stuff
        $counter = 0;
        foreach ($splittedName as $namePart) {
            if ($counter === 0) {
                $cssClassName .= $namePart;
            } else {
                $cssClassName .= ucfirst($namePart);
            }
            $counter++;
        }
        
        return $cssClassName;
    }
    
    /**
     * Returns the day string for the given advent calendar day
     *
     * @param Magmex_AdventCalendar_Model_Day $adventCalendarDay advent calendar day instance
     * @return string day string
     */
    public function getDayForAdventCalendarDate(Magmex_AdventCalendar_Model_Day $adventCalendarDay)
    {
        return Mage::getModel('core/date')->date('j', $adventCalendarDay->getDate());
    }
    
    /**
     * Returns the AJAX url to get the contents for a day action
     *
     * @return string AJAX url
     */
    public function getDayActionAjaxUrl()
    {
        $url = 'magmex_adventcalendar/dayactionajax/getday/';
        $urlParameters = array(
            '_query' => array('dayId' => '')
        );

        if (Mage::app()->getStore()->isCurrentlySecure() === true) {
            $urlParameters['_secure'] = true;
        }

        return $this->getUrl($url, $urlParameters);
    }
    
    /**
     * Returns the query param needed to build image resize urls
     *
     * @param string $absoluteImagePath absolute image path of image to resize
     * @return array query param array in the format $this->getUrl() needs
     */
    protected function _getImageResizeQueryParamArray($absoluteImagePath)
    {
        $urlQueryArray = array(
            '_query' => array(
                'absoluteImagePath' => $absoluteImagePath,
                'width' => ''
            )
        );
        return $urlQueryArray;
    }
    
    
    /**
     * Returns the url of the Advent Calendar background image depending on the configuration
     *
     * @return string background image url
     */
    public function getMainBackgroundImageUrl()
    {
        $absoluteImagePath = '/';
        if ($this->_modelAdventCalendar->getCustomizedBackgroundImage() !== null) {
            $absoluteImagePath .= $this->_modelAdventCalendar->getCustomizedBackgroundImage();
        } else {
            $absoluteImagePath .= $this->_modelAdventCalendar->getStandardBackgroundImage();
        }
        
        return $this->getUrl(Magmex_AdventCalendar_Model_Image::BASE_IMAGE_RESIZE_URL, $this->_getImageResizeQueryParamArray($absoluteImagePath));
    }
    
    /**
     * Returns the url of the advent calendar door background image sprite
     *
     * @return string door background image sprite url
     */
    public function getDoorBackgroundImageSpriteUrl()
    {
        $absoluteImagePath = DS. Magmex_AdventCalendar_Helper_Data::SKIN_DOOR_BACKGROUND_IMAGE_SPRITE_PATH . DS . 'door_background_image_sprite.png';
        return $this->getUrl(Magmex_AdventCalendar_Model_Image::BASE_IMAGE_RESIZE_URL, $this->_getImageResizeQueryParamArray($absoluteImagePath));
    }
    
    /**
     * Returns the background image sprite position for the given advent calendar day
     *
     * @param Magmex_AdventCalendar_Model_Day $adventCalendarDay advent calendar day instance
     * @return integer element width
     */
    public function getDoorBackgroundImageSpritePosition(Magmex_AdventCalendar_Model_Day $adventCalendarDay)
    {
        $spritePosition = -(($adventCalendarDay->getBackgroundImagePosition() - 1) * 125);
        return $spritePosition;
    }

    /**
     * Triggers the parent::stripTags method in Magento version >= 1.5.0.0.
     * Otherwise strip_tags is used.
     *
     * @param string $string string to be stripped
     * @param null $allowableTags allowed tags
     * @param bool $allowHtmlEntities are html entities allowed?
     * @return string the new and cleaned string
     */
    public function stripTags($string, $allowableTags = null, $allowHtmlEntities = false)
    {
        if (version_compare(Mage::getVersion(), '1.5.0.0', '>=')) {
            return parent::stripTags($string);
        } else {
            return strip_tags($string);
        }
    }
}