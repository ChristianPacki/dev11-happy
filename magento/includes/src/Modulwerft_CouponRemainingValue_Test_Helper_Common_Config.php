<?php
/**
 * @category Modulwerft
 * @package Modulwerft_CouponRemainingValue
 * @author Shopwerft GmbH <werft@shopwerft.com>
 * @author Benjamin Wunderlich <b.wunderlich@shopwerft.com>
 * @copyright 2014 Shopwerft GmbH (http://www.shopwerft.com)
 * @since 1.2.2
 */

class Modulwerft_CouponRemainingValue_Test_Helper_Common_Config
    extends EcomDev_PHPUnit_Test_Case_Config

{

    /**
     * Check helper classes
     */
    public function testHelperClasses()
    {
        $this->assertHelperAlias(
            'modulwerft_couponremainingvalue',
            'Modulwerft_CouponRemainingValue_Helper_Data',
            'Data helper has wrong class name'
        );
        $this->assertHelperAlias(
            'modulwerft_couponremainingvalue/config',
            'Modulwerft_CouponRemainingValue_Helper_Config',
            'Config helper has wrong class name'
        );
        $this->assertHelperAlias(
            'modulwerft_couponremainingvalue/log',
            'Modulwerft_CouponRemainingValue_Helper_Log',
            'Log helper has wrong class name'
        );
        $this->assertHelperAlias(
            'modulwerft_couponremainingvalue/common_meta',
            'Modulwerft_CouponRemainingValue_Helper_Common_Meta',
            'Meta helper has wrong class name'
        );
    }

    /**
     * Check if config helper returns correct config value for module_active
     *
     * @loadFixture
     */
    public function testModuleActive()
    {
        /** @var Modulwerft_CouponRemainingValue_Helper_Config $helper */
        $helper = Mage::helper('modulwerft_couponremainingvalue/config');

        $this->assertTrue($helper->isModuleActive(), 'Module is not active, but should be');
    }

    /**
     * Check if config helper returns correct config value module_active
     *
     * @loadFixture
     */
    public function testModuleInactive()
    {
        /** @var Modulwerft_CouponRemainingValue_Helper_Config $helper */
        $helper = Mage::helper('modulwerft_couponremainingvalue/config');

        $this->assertFalse($helper->isModuleActive(), 'Module is active, but shouldn\'t be');
    }

}