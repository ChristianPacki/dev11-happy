<?php

/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_Faq
 */
class Amasty_Faq_Model_Resource_File extends Mage_Core_Model_Resource_Db_Abstract {

    public function _construct() 
    {
        $this->_init('amfaq/file', 'file_id');
    }
}