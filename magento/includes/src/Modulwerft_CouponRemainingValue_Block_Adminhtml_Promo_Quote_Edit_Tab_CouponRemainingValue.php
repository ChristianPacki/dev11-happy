<?php

/**
 * @category Modulwerft
 * @package Modulwerft_CouponRemainingValue
 * @author Modulwerft GmbH <info@modulwerft.com>
 * @author Benjamin Wunderlich <b.wunderlich@shopwerft.com>
 * @copyright 2014 Shopwerft GmbH (http://www.shopwerft.com)
 */
class Modulwerft_CouponRemainingValue_Block_Adminhtml_Promo_Quote_Edit_Tab_CouponRemainingValue
    extends Mage_Adminhtml_Block_Text_List
    implements Mage_Adminhtml_Block_Widget_Tab_Interface
{

    /**
     * Prepare content for tab
     *
     * @return string
     */
    public function getTabLabel()
    {
        return $this->__('Coupon Value History');
    }

    /**
     * Prepare title for tab
     *
     * @return string
     */
    public function getTabTitle()
    {
        return $this->__('Coupon Value History');
    }

    /**
     * Can show tab in tabs
     *
     * @return boolean
     */
    public function canShowTab()
    {
        /** @var Modulwerft_CouponRemainingValue_Helper_Config $configHelper */
        $configHelper = Mage::helper('modulwerft_couponremainingvalue/config');
        return $this->_isEditing() && $configHelper->isModuleActive() && $this->_isCrvRule();
    }

    /**
     * Tab is hidden
     *
     * @return boolean
     */
    public function isHidden()
    {
        /** @var Modulwerft_CouponRemainingValue_Helper_Config $configHelper */
        $configHelper = Mage::helper('modulwerft_couponremainingvalue/config');
        return !$this->_isEditing() || !$configHelper->isModuleActive() || !$this->_isCrvRule();
    }

    /**
     * Check whether we edit existing rule or adding new one
     *
     * @return bool
     */
    protected function _isEditing()
    {
        $priceRule = Mage::registry('current_promo_quote_rule');
        return !is_null($priceRule->getRuleId());
    }

    /**
     * @return bool
     * @since 1.2.1
     */
    protected function _isCrvRule()
    {
        $priceRule = Mage::registry('current_promo_quote_rule');

        /** @var Modulwerft_CouponRemainingValue_Helper_Data $helper */
        $helper = Mage::helper('modulwerft_couponremainingvalue');

        return $helper->usesRuleRemainingValue($priceRule);
    }

}
