<?php
class Redgecko_Magnalister_Block_Adminhtml_Magnalister extends Mage_Adminhtml_Block_Template{
    protected $sRenderedHtml='';
    public function _construct(){
        /**
         * include magna-plugin
         */
        require_once(Mage::getModuleDir('','Redgecko_Magnalister').DS.'Lib'.DS.'Core'.DS.'ML.php');
        return parent::_construct();
    }
    public function render(){
        echo $this->sRenderedHtml;
    }
    protected function _prepareLayout() {
        parent::_prepareLayout();
        /**
         * first run magna-plugin to get all js and css. these can be added in views.
         * also starts correct bootstrap
         */
        $this->sRenderedHtml=ML::gi()->run();
        /**
         *todo page-title 
         */
        /* @var Mage_Adminhtml_Block_Page_Head $oHead */
        $oHead=$this->getLayout()->getBlock('head');
        /* @var $sClientVersion string will be added to url as parameter to avoid browser-cache */
        $sClientVersion=MLSetting::gi()->get('sClientBuild');
        //add css to front-controller
        foreach(MLSetting::gi()->get('aCss') as $sFile){
            $oHead->addLinkRel('stylesheet', MLHttp::gi()->getResourceUrl('css/'.sprintf($sFile, $sClientVersion)));
        }
        //add js to front-controller
        foreach(MLSetting::gi()->get('aJs') as $sFile){
            if (preg_match('/tiny_mce\/tiny_mce.js/', $sFile)) {
                /**
                 * @see app/deign/adminhtml/default/default/template/page/head.phtml
                 *      <?php if($this->getCanLoadTinyMce()): // TinyMCE is broken when loaded through index.php ?>
                 * tiny_mce can't load plugins, languages etc. if js-url have multiple entries (,-separated - only when js-files are minimezed, cached etc)
                 * as a workaround add tiny_mce.js in body instead head
                 */
                $this->sRenderedHtml = '<script type="text/javascript" src="'.MLHttp::gi()->getResourceUrl('js/'.sprintf($sFile, $sClientVersion)).'"></script>'.$this->sRenderedHtml;
            } else {
                $oHead->addJs(MLHttp::gi()->getResourceUrl('js/'.sprintf($sFile, $sClientVersion), false));
            }
        }
        //add magna css-class to body-tag
        $oPage=$this->getLayout()->getBlock('root');
        foreach(MLSetting::gi()->get('aBodyClasses') as $sClass){
            $oPage->addBodyClass($sClass);
        }
    }
}
