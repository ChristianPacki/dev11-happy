<?php
/**
 * 888888ba                 dP  .88888.                    dP                
 * 88    `8b                88 d8'   `88                   88                
 * 88aaaa8P' .d8888b. .d888b88 88        .d8888b. .d8888b. 88  .dP  .d8888b. 
 * 88   `8b. 88ooood8 88'  `88 88   YP88 88ooood8 88'  `"" 88888"   88'  `88 
 * 88     88 88.  ... 88.  .88 Y8.   .88 88.  ... 88.  ... 88  `8b. 88.  .88 
 * dP     dP `88888P' `88888P8  `88888'  `88888P' `88888P' dP   `YP `88888P' 
 *
 *                          m a g n a l i s t e r
 *                                      boost your Online-Shop
 *
 * -----------------------------------------------------------------------------
 * $Id$
 *
 * (c) 2010 - 2015 RedGecko GmbH -- http://www.redgecko.de
 *     Released under the MIT License (Expat)
 * -----------------------------------------------------------------------------
 */

MLI18n::gi()->add('amazon_prepare_apply_form', array(
    'legend'=>array(
        'category' => 'Kategorie',
        'details' => 'Details',
        'variations' => 'Variantengruppe von Amazon ausw&auml;hlen',
        'attributes' => 'Attributsnamen von Amazon ausw&auml;hlen',
        'variationmatching' => array('Amazon Erforderliche Attribute', 'Attribut- und Attributswert-Matching'),
        'variationmatchingoptional' => array('Amazon Optionale Attribute', 'Attribut- und Attributswert-Matching'),
        'moredetails' => 'Weitere Details (Empfohlen)',
        'common' => 'Allgemeine Einstellungen',
        'b2b' => 'Amazon Business (B2B)',
    ),
    'field' => array(
        'variationgroups.value' => array(
            'label' => 'Hauptkategorie <span class="bull">•</span>',
        ),
        'producttype' => array(
            'label' => 'Unterkategorie <span class="bull">•</span>',
            'hint' => '(Produkttyp)',
        ),
        'browsenodes' => array(
            'label' => 'Browsenodes <span class="bull">•</span>',
            'hint' => '',
        ),
        'webshopattribute' => array(
            'label' => 'Web-Shop Attribut',
        ),
        'attributematching' => array(
            'matching' => array(
                'titlesrc' => 'Shop-Wert',
                'titledst' => 'Amazon-Wert',
            ),
        ),
        'itemtitle'=>array(
            'label' => 'Produktname <span class="bull">•</span>',
            'hint' => '',
            'optional' => array(
                'checkbox' => array(
                    'labelNegativ' => 'Artikelbeschreibung immer aktuell aus Web-Shop verwenden',
                ),
            ),
        ),
        'manufacturer' => array(
            'label' => 'Artikelhersteller <span class="bull">•</span>',
            'hint' => 'Hersteller des Produktes',
            'optional' => array(
                'checkbox' => array(
                    'labelNegativ' => 'Artikelhersteller immer aktuell aus Web-Shop verwenden',
                ),
            ),
        ),
        'brand' => array(
            'label' => 'Marke <span class="bull">•</span>',
            'hint' => 'Marke oder Hersteller des Produktes',
            'optional' => array(
                'checkbox' => array(
                    'labelNegativ' => 'Marke oder Hersteller immer aktuell aus Web-Shop verwenden',
                ),
            ),
        ),
        'manufacturerpartnumber' => array(
            'label' => 'Modellnummer <span class="bull">•</span>',
            'hint' => 'Geben Sie die Modellnummer des Herstellers für das Produkt an.',
            'optional' => array(
                'checkbox' => array(
                    'labelNegativ' => 'Modellnummer immer aktuell aus Web-Shop verwenden',
                ),
            ),
        ),
        'ean' => array(
            'hint' => 'Nicht relevant, wenn an den Varianten {#Type#} hinterlegt ist',
            'optional' => array(
                'checkbox' => array(
                    'labelNegativ' => '{#Type#} immer aktuell aus Web-Shop verwenden',
                ),
            ),
        ),
        'images' => array(
            'label' => 'Produktbilder',
            'hint' => 'Maximal 9 Produktbilder',
            'optional' => array(
                'checkbox' => array(
                    'labelNegativ' => 'Bilder immer aktuell aus Web-Shop verwenden',
                ),
            ),
        ),
        'bulletpoints' => array(
            'label' => 'Bulletpoints',
            'hint' => 'Key-Features des Artikels (z. B. "Vergoldete Armaturen", "Extrem edles Design")<br /><br />Diese Daten werden aus Meta-Description gezogen und müssen dort mit Kommas getrennt sein.<br />Maximal je 500 Zeichen.',
            'optional' => array(
                'checkbox' => array(
                    'labelNegativ' => 'Bulletpoints immer aktuell aus Web-Shop verwenden (Metadescription)',
                ),
            ),
        ),
        'description' => array(
            'label' => 'Produktbeschreibung',
            'hint' => 'Maximal 2000 Zeichen. Einige HTML-Tags und deren Attribute sind erlaubt. Diese Zählen zu den 2000 Zeichen dazu.',
            'optional' => array(
                'checkbox' => array(
                    'labelNegativ' => 'Produktbeschreibung immer aktuell aus Web-Shop verwenden',
                ),
            ),
        ),
        'keywords' => array(
            'label' => 'Allgemeine Schlüsselwörter',
            'hint' => 'Werden bei Suche verwendet (z. B. "vergoldet", "edel", "kitschig")<br /><br />Diese Daten werden aus Meta-Keywords gezogen und müssen dort mit Kommas getrennt sein.<br />Je nach Kategorie maximal zwischen 50 und 1000 Zeichen..',
            'optional' => array(
                'checkbox' => array(
                    'labelNegativ' => 'Keywords immer aktuell aus Web-Shop verwenden (Metakeywords)',
                ),
            ),
        ),
        'shippingtime' => array(
            'label' => 'Versandzeit',
            'hint' => '',
            'optional' => array(
                'checkbox' => array(
                    'labelNegativ' => 'Immer Wert aus Konfiguration nehmen',
                ),
            ),
        ),
        'shippingtemplate' => array(
            'label' => 'Verk&auml;uferversandgruppe',
            'hint' => 'Unter &quot;Konfiguration -&gt; Artikelvorbereitung&quot; k&ouml;nnen Sie die verschiedenen Versandgruppen anlegen'
        ),
        'b2bactive' => array(
            'label' => 'Amazon Business aktivieren',
            'help' => 'Wenn aktiviert, k&ouml;nnen Artikel f&uuml;r den Business-to-Business Verkauf an Amazon &uuml;bermittelt werden, wie unten konfiguriert. <b>Bitte stellen Sie sicher, dass Ihr Amazon Konto f&uuml;r Amazon Business freigeschaltet ist.</b> Andernfalls wird das Hochladen von B2B-Artikeln zu Fehlern f&uuml;hren.',
            'notification' => 'Um Amazon Business zu nutzen, brauchen Sie eine Aktivierung in Ihrem Amazon-Konto.  <b>Bitte stellen Sie sicher, dass Ihr Amazon Konto f&uuml;r Amazon Business freigeschaltet ist.</b> Andernfalls wird das Hochladen von B2B-Artikeln zu Fehlern f&uuml;hren.<br>Um Ihr Konto f&uuml;r Amazon Business freizuschalten, folgen Sie bitte der Anleitung unter <a href="https://sellercentral.amazon.de/business/b2bregistration" target="_blank">diesem Link</a>.',
            'disabledNotification' => 'Um Amazon Business zu nutzen, aktivieren Sie es bitte zuerst in der Konfiguration.',
            'values' => array(
                'true' => 'Ja',
                'false' => 'Nein',
            ),
        ),
        'b2bsellto' => array(
            'label' => 'Verkaufen an',
            'help' => 'Wenn <i>B2B Only</i> ausgew&auml;hlt, werden hochgeladene Produkte nur f&uuml;r Gesch&auml;ftskunden sichtbar sein. Andernfalls sowohl f&uuml;r Gesch&auml;fts- als auch Privatkunden.',
            'values' => array(
                'b2b_b2c' => 'B2B und B2C',
                'b2b_only' => 'B2B Only',
            ),
        ),
        'b2bdiscounttype' => array(
            'label' => 'Staffelpreis-Berechnung',
            'help' => '<b>Staffelpreise</b><br>
                      Staffelpreise sind erm&auml;&szlig;igte Preise, die f&uuml;r Gesch&auml;ftskunden beim Kauf
                      gr&ouml;&szlig;erer St&uuml;ckzahlen verf&uuml;gbar sind. Verk&auml;ufer, die am Amazon 
                      Business Seller Program teilnehmen, k&ouml;nnen entsprechende Mindestmengen
                      und Preisabschl&auml;ge definieren.<br><br>
                      <b>Beispiel</b>:
                      F&uuml;r ein Produkt, das 100 &euro; kostet, k&ouml;nnten folgende
                      Prozent-Abschl&auml;ge (f&uuml;r Gesch&auml;ftskunden) definiert werden:
                      <table><tr>
                          <th style="background-color: #ddd;">Mindestmenge</th>
                          <th style="background-color: #ddd;">Abschlag</th>
                          <th style="background-color: #ddd;">Endpreis pro St&uuml;ck</th>
                      <tr><td>5 (oder mehr)</td><td style="text-align: right;">10</td><td style="text-align: right;">$90</td></tr>
                      <tr><td>8 (oder mehr)</td><td style="text-align: right;">12</td><td style="text-align: right;">$88</td></tr>
                      <tr><td>12 (oder mehr)</td><td style="text-align: right;">15</td><td style="text-align: right;">$85</td></tr>
                      <tr><td>20 (oder mehr)</td><td style="text-align: right;">20</td><td style="text-align: right;">$80</td></tr>
                      </table>',
            'values' => array(
                '' => 'Nicht verwenden',
                'percent' => 'Prozent',
                'fixed' => 'Fixed',
            ),
        ),
        'b2bdiscounttier1' => array(
            'label' => 'Staffelpreis Ebene 1',
        ),
        'b2bdiscounttier2' => array(
            'label' => 'Staffelpreis Ebene 2',
        ),
        'b2bdiscounttier3' => array(
            'label' => 'Staffelpreis Ebene 3',
        ),
        'b2bdiscounttier4' => array(
            'label' => 'Staffelpreis Ebene 4',
        ),
        'b2bdiscounttier5' => array(
            'label' => 'Staffelpreis Ebene 5',
        ),
        'b2bdiscounttier1quantity' => array(
            'label' => 'St&uuml;ckzahl',
        ),
        'b2bdiscounttier2quantity' => array(
            'label' => 'St&uuml;ckzahl',
        ),
        'b2bdiscounttier3quantity' => array(
            'label' => 'St&uuml;ckzahl',
        ),
        'b2bdiscounttier4quantity' => array(
            'label' => 'St&uuml;ckzahl',
        ),
        'b2bdiscounttier5quantity' => array(
            'label' => 'St&uuml;ckzahl',
        ),
        'b2bdiscounttier1discount' => array(
            'label' => 'Rabatt',
        ),
        'b2bdiscounttier2discount' => array(
            'label' => 'Rabatt',
        ),
        'b2bdiscounttier3discount' => array(
            'label' => 'Rabatt',
        ),
        'b2bdiscounttier4discount' => array(
            'label' => 'Rabatt',
        ),
        'b2bdiscounttier5discount' => array(
            'label' => 'Rabatt',
        ),
    ),
), false);

MLI18n::gi()->add('amazon_prepare_variations', array(
    'legend' => array(
        'variations' => 'Variantengruppe von Amazon ausw&auml;hlen',
        'attributes' => 'Attributsnamen von Amazon ausw&auml;hlen',
        'variationmatching' => array('Amazon Erforderliche Attribute', 'Attribut- und Attributswert-Matching'),
        'variationmatchingoptional' => array('Amazon Optionale Attribute', 'Attribut- und Attributswert-Matching'),
        'action' => '{#i18n:form_action_default_legend#}',
    ),
    'field' => array(
        'variationgroups.value' => array(
            'label' => 'Variantengruppe',
        ),
        'customidentifier' => array(
            'label' => 'Unterkategorie',
        ),
        'deleteaction' => array(
            'label' => '{#i18n:ML_BUTTON_LABEL_DELETE#}',
        ),
        'groupschanged' => array(
            'label' => '',
        ),
        'attributename' => array(
            'label' => 'Attributsnamen',
        ),
        'attributenameajax' => array(
            'label' => '',
        ),
        'webshopattribute' => array(
            'label' => 'Web-Shop Attribut',
        ),
        'saveaction' => array(
            'label' => 'SPEICHERN UND SCHLIESSEN',
        ),
        'resetaction' => array(
            'label' => '{#i18n:amazon_varmatch_reset_matching#}',
            'confirmtext' => '{#i18n:amazon_prepare_variations_reset_info#}',
        ),
        'attributematching' => array(
            'matching' => array(
                'titlesrc' => 'Shop-Wert',
                'titledst' => 'Amazon-Wert',
            ),
        ),
    ),
), false);

MLI18n::gi()->amazon_varmatch_define_name = 'Bitte geben Sie einen Bezeichner ein.';
MLI18n::gi()->amazon_varmatch_ajax_error = 'Ein Fehler ist aufgetreten.';
MLI18n::gi()->amazon_varmatch_all_select = 'Alle';
MLI18n::gi()->amazon_varmatch_please_select = 'Bitte w&auml;hlen...';
MLI18n::gi()->amazon_varmatch_auto_matchen = 'Auto-matchen';
MLI18n::gi()->amazon_varmatch_reset_matching = 'Matchen aufheben';
MLI18n::gi()->amazon_varmatch_delete_custom_title = 'Varianten-Matching-Gruppe l&ouml;schen';
MLI18n::gi()->amazon_varmatch_delete_custom_content = 'Wollen Sie die eigene Gruppe wirklich l&ouml;schen?<br />Alle zugeh&ouml;rigen Variantenmatchings werden dann ebenfalls gel&ouml;scht.';
MLI18n::gi()->amazon_varmatch_attribute_changed_on_mp = 'Achtung: Amazon hat einige bereits gematchte Attributswerte nachträglich entfernt oder geändert. 
Bitte überprüfen Sie Ihre Zuordnungen und matchen die betroffenen Werte bei Bedarf erneut.';
MLI18n::gi()->amazon_varmatch_attribute_different_on_product = 'Hinweis: Sie haben diesen Wert abweichend zum globalen Attributs-Matching gespeichert. 
Es werden die hier abweichend gespeicherten Werte zum Marktplatz übertragen.';
MLI18n::gi()->amazon_varmatch_attribute_deleted_from_mp = 'Dieses Attribut wurde von Amazon gelöscht oder geändert. Matchings dazu wurden daher aufgehoben. 
Bitte matchen Sie bei Bedarf erneut auf ein geeignetes Amazon Attribut.';
MLI18n::gi()->amazon_varmatch_attribute_value_deleted_from_mp = 'Dieser Attributswert wurde von Amazon gelöscht oder geändert. Matchings dazu wurden daher aufgehoben. 
Bitte matchen Sie bei Bedarf erneut auf einen geeigneten Amazon Attributswert.';
MLI18n::gi()->amazon_varmatch_already_matched = '(bereits gematcht)';

MLI18n::gi()->amazon_category_attribut = 'Amazon Attribut';
MLI18n::gi()->amazon_shop_attribut = 'Attribut- und Attributswert-Matching';
MLI18n::gi()->amazon_web_shop_attribut = 'Web-Shop Attribut';
MLI18n::gi()->amazon_shop_select = 'Shop-Wert';
MLI18n::gi()->amazon_marketplace_select = 'Amazon-Wert';
MLI18n::gi()->amazon_prepareform_max_length_part1 = 'Max length of';
MLI18n::gi()->amazon_prepareform_max_length_part2 = 'attribute is';
MLI18n::gi()->amazon_prepareform_category = 'Category attribute is mandatory.';
MLI18n::gi()->amazon_prepareform_title = 'Bitte geben Sie einen Titel an.';
MLI18n::gi()->amazon_prepareform_description = 'Bitte geben Sie eine Artikelbeschreibung an.';
MLI18n::gi()->amazon_prepareform_category_attribute = ' (Kategorie Attribut) ist erforderlich und kann nicht leer sein.';
MLI18n::gi()->amazon_category_no_attributes= 'Es sind keine Attribute f&uuml;r diese Kategorie vorhanden.';
MLI18n::gi()->amazon_prepare_variations_title = 'Attributes Matching';
MLI18n::gi()->amazon_prepare_variations_groups = 'Amazon Gruppen';
MLI18n::gi()->amazon_prepare_variations_groups_custom = 'Eigene Gruppen';
MLI18n::gi()->amazon_prepare_variations_groups_new = 'Eigene Gruppe anlegen';
MLI18n::gi()->amazon_prepare_match_variations_no_selection = 'Bitte w&auml;hlen Sie eine Variantengruppe aus.';
MLI18n::gi()->amazon_prepare_match_variations_custom_ident_missing = 'Bitte w&auml;hlen Sie Bezeichner.';
MLI18n::gi()->amazon_prepare_match_variations_attribute_missing = 'Bitte w&auml;hlen Sie Attributsnamen.';
MLI18n::gi()->amazon_prepare_match_variations_category_missing = 'Bitte w&auml;hlen Sie Variantengruppe.';
MLI18n::gi()->amazon_prepare_match_variations_not_all_matched = 'Bitte weisen Sie allen amazon Attributen ein Shop-Attribut zu.';
MLI18n::gi()->amazon_prepare_match_notice_not_all_auto_matched = 'Es konnten nicht alle ausgewählten Werte gematcht werden. Nicht-gematchte Werte werden weiterhin in den DropDown-Feldern angezeigt. Bereits gematchte Werte werden in der Produktvorbereitung berücksichtigt.';
MLI18n::gi()->amazon_prepare_match_variations_saved = 'Erfolgreich gespeichert.';
MLI18n::gi()->amazon_prepare_variations_saved = 'Es wurden alle ausgewählten Werte erfolgreich gematcht. Sie können nun Produkte mit den gematchten Attributen vorbereiten, oder mit dem Matching hier fortfahren.';
MLI18n::gi()->amazon_prepare_variations_reset_success = 'Das Matching wurde aufgehoben.';
MLI18n::gi()->amazon_prepare_match_variations_delete = 'Wollen Sie die eigene Gruppe wirklich l&ouml;schen? Alle zugeh&ouml;rigen Variantenmatchings werden dann ebenfalls gel&ouml;scht.';
MLI18n::gi()->amazon_error_checkin_variation_config_empty = 'Variationen sind nicht konfiguriert.';
MLI18n::gi()->amazon_error_checkin_variation_config_cannot_calc_variations = 'Es konnten keine Variationen errechnet werden.';
MLI18n::gi()->amazon_error_checkin_variation_config_missing_nameid = 'Es konnte keine Zuordnung f&uuml;r das Shop Attribut "{#Attribute#}" bei der gew&auml;hlten Ayn24 Variantengruppe "{#MpIdentifier#}" f&uuml;r den Varianten Artikel mit der SKU "{#SKU#}" gefunden werden.';
MLI18n::gi()->amazon_prepare_variations_free_text = 'Eigene Angaben machen';
MLI18n::gi()->amazon_prepare_variations_additional_category = 'Zusätzliche Kategorie';
MLI18n::gi()->amazon_prepare_variations_error_text = 'Das Attribut "{#attribute_name#}" ist ein Pflichtfeld. Bitte ordnen Sie alle Werte zu.';
MLI18n::gi()->amazon_prepare_variations_error_missing_value = 'Attribute "{#attribute_name#}" is mandatory, and your product does not have value for chosen matched attribute from shop.';
MLI18n::gi()->amazon_prepare_variations_error_free_text = ': Das Freitext Feld darf nicht leer sein.';
MLI18n::gi()->amazon_prepare_variations_matching_table = 'Gematchte';
MLI18n::gi()->amazon_prepare_variations_manualy_matched = ' - (manuel zugeordnet)';
MLI18n::gi()->amazon_prepare_variations_auto_matched = ' - (automatisch zugeordnet)';
MLI18n::gi()->amazon_prepare_variations_free_text_add = ' - (eigene angaben)';
MLI18n::gi()->amazon_prepare_variations_reset_info = 'Wollen Sie das Matching wirklich aufheben?';
MLI18n::gi()->amazon_prepare_variations_change_attribute_info = 'Bevor Sie Attribute &auml;ndern k&ouml;nnen, heben Sie bitte alle Matchings zuvor auf.';
MLI18n::gi()->amazon_prepare_variations_additional_attribute_label = 'Eigene Attribute';
MLI18n::gi()->amazon_prepare_variations_separator_line_label = '-------------------------------------------------------------';
MLI18n::gi()->amazon_prepare_variations_mandatory_fields_info = '<b>Hinweis:</b> Die mit <span class="bull">&bull;</span> markierten Felder sind Pflichtfelder und m&uuml;ssen ausgef&uuml;llt werden.';
MLI18n::gi()->amazon_prepare_variations_category_without_attributes_info = 'Für die ausgewählte Kategorie unterstützt Amazon keine Attribute.';
MLI18n::gi()->amazon_prepare_variations_choose_mp_value = 'Verwende Amazon Attributswert';
MLI18n::gi()->amazon_prepare_variations_notice = 'Bitte beachten Sie, dass Sie einige Artikel der gew&auml;hlten Kategorie "{#category_name#}" abweichend unter „Produkte vorbereiten“ gematcht haben. Es werden die dort gespeicherten Werte zum Marktplatz &uuml;bermittelt.';
MLI18n::gi()->amazon_prepare_variations_already_matched = '(bereits gematcht)';
