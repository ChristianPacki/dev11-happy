<?php
/**
 * Magmex Adventcalendar day model
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.magmex.com/MAGMEX_SOFTWARE_LICENSE.txt
 *
 * @copyright Copyright (c) 2015 magmex, Michael Stork, (http://www.magmex.com)
 * @license http://www.magmex.com/MAGMEX_SOFTWARE_LICENSE.txt
 * @category Magmex
 * @package Magmex_AdventCalendar
 * @author Michael Stork <m.stork@magmex.com>
 */

class Magmex_AdventCalendar_Model_Day extends Mage_Core_Model_Abstract
{
    /**
     * The day action to edit, change or save with the day model if existant
     *
     * @var Magmex_AdventCalendar_Model_Day_Action
     */
    protected $_modelDayAction;
    
    /**
     * Construct
     * Inits the resource model
     *
     * @see Varien_Object::_construct()
     */
    protected function _construct()
    {
        parent::_construct();
        $this->_init('magmex_adventcalendar/day');
    }
    
    /**
     * Deletes an existing day action type entry before saving a day - searches for day action types with the
     * original day action type of the day model and compares them.
     * The day action is only deleted, when it was changed
     *
     * @see Mage_Core_Model_Abstract::_beforeSave()
     * return Mage_Core_Model_Abstract chaining
     */
    protected function _beforeSave()
    {
        $originalActionType = $this->getOrigData('action_type');
        
        if (!empty($originalActionType) && $originalActionType !== $this->getActionType()) {
            $modelDayAction = Mage::helper('magmex_adventcalendar')->getDayActionModel($originalActionType, $this->getId());
            if (!empty($modelDayAction)) {
                $modelDayAction->delete();
            }
        }
        parent::_beforeSave();
    }
    
    /**
     * Checks, if a day is active
     *
     * @param Magmex_AdventCalendar_Model_Adventcalendar $adventCalendar
     * @return boolean true, when day is active (today or past days)
     */
    public function isDayActive()
    {
        $currentTimestamp = Mage::getModel('core/date')->timestamp(time());
        $actualDate = date('Y-m-d', $currentTimestamp);
        $active = (Mage::getModel('core/date')->timestamp($actualDate) >= Mage::getModel('core/date')->timestamp($this->getDate()));
        
        $requestParamUnlockAll = Mage::app()->getRequest()->getParam('unlockAllDoors');
        if (!empty($requestParamUnlockAll)) {
            return true;
        }
        
        return $active;
    }
    
    /**
     * Adds the day action data (if existant) to the advent calendar day model data
     *
     * @return Magmex_AdventCalendar_Model_Day chaining
     */
    public function addDayActionData()
    {
        $this->_getResource()->addDayActionData($this);
        return $this;
    }
    
    /**
     * Loads the advent calendar day by an advent calendar
     *
     * @param integer $adventCalendarId advent calendar id
     * @param integer $adventCalendarDate advent calendar day date
     * @return Magmex_AdventCalendar_Model_Day advent calendar day model instance
     */
    public function loadByAdventCalendarIdAndDate($adventCalendarId, $adventCalendarDate)
    {
        $this->_getResource()->loadAdventCalendarDayByAdventCalendarIdAndDate($this, $adventCalendarId, $adventCalendarDate);
        return $this;
    }
}