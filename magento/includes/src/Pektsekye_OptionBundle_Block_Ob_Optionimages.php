<?php
class Pektsekye_OptionBundle_Block_Ob_Optionimages extends Mage_Adminhtml_Block_Widget
    implements Mage_Adminhtml_Block_Widget_Tab_Interface
{

    protected $_options;
    protected $_boptions;     

    protected $_uploaderId = 'ob_uploader';
    
    
    public function __construct()
    {
      parent::__construct();
      $this->setId('optionbundle_optionimages');
      $this->setSkipGenerateContent(true);        
      $this->setTemplate('optionbundle/ob/optionimages.phtml');
    }


    public function getProduct()
    {
      return Mage::registry('current_product');
    }  
   
 
 
     public function getBoptions()
    {
      if (!$this->_boptions) {
        $product = $this->getProduct();
        $typeInstance = $product->getTypeInstance(true);
        $typeInstance->setStoreFilter($product->getStoreId(), $product);

        $optionCollection = $typeInstance->getOptionsCollection($product);

        $selectionCollection = $typeInstance->getSelectionsCollection(
            $typeInstance->getOptionsIds($product),
            $product
        );

        $this->_boptions = $optionCollection->appendSelections($selectionCollection);
      }

      return $this->_boptions;
    }



    public function getOptions()
    {
      if (!isset($this->_options)){
        $options = array();       
        $hasOrder = false;            
        
        if ($this->getProduct()->getTypeId() == Mage_Catalog_Model_Product_Type::TYPE_BUNDLE){
          $t = 'b';
          $obOptions = Mage::getModel('optionbundle/boption')->getBoptions($this->getProduct()->getId(), (int)$this->getProduct()->getStoreId());         
          foreach ($this->getBoptions() as $_option) {
          
              if (!$_option->getSelections()) {
                  continue;
              }
                            
              $id = (int) $_option->getId();
              
              $optionType = $_option->getType();
              if ($optionType == 'multi'){
                $optionType = 'multiple';
              } elseif ($optionType == 'select'){
                $optionType = 'drop_down';                
              }  
              
              $option = array(
                  'type' => $t,
                  'id' => $id,
                  'title' => $this->htmlEscape($_option->getTitle()),
                  'sort_order' => $_option->getPosition(),
                  'required' => isset($obOptions[$id]['required']) ? (int) $obOptions[$id]['required'] : 0,                   
                  'selection_type' =>  in_array($_option->getType(), array('multi', 'checkbox')) ? 'multiple' : 'single',                     
                  'note' => isset($obOptions[$id]['note']) ? $obOptions[$id]['note'] : '',  
                  'store_note' => isset($obOptions[$id]['store_note']) ? $obOptions[$id]['store_note'] : null,                      
                  'layout' => isset($obOptions[$id]['layout']) ? $obOptions[$id]['layout'] : '',  
                  'popup' => isset($obOptions[$id]['popup']) ? $obOptions[$id]['popup'] : 0,
                  'use_product_image' => isset($obOptions[$id]['use_product_image']) ? $obOptions[$id]['use_product_image'] : 1,                   
                  'use_product_description' => isset($obOptions[$id]['use_product_description']) ? $obOptions[$id]['use_product_description'] : 1, 
                  'option_type' => $optionType,                                                                                                                             
                  'values' => array()
              );

              $obValues = Mage::getModel('optionbundle/bvalue')->getValues($this->getProduct()->getId(), (int)$this->getProduct()->getStoreId());
              foreach ($_option->getSelections() as $selection) {
               $valueId = (int) $selection->getSelectionId(); 

                $image = isset($obValues[$valueId]['image']) ? $obValues[$valueId]['image'] : '';                                              
                $option['values'][] = array(
                    'id' => $valueId,
                    'title' => $this->htmlEscape($selection->getName()),
                    'price' => $this->getPriceValue($selection->getSelectionPriceValue(), $selection->getSelectionPriceType()),
                    'image' => $image,
                    'image_json' => $this->getImageJson($image),                        
                    'description' => isset($obValues[$valueId]['description']) ? $obValues[$valueId]['description'] : '',
                    'store_description' => isset($obValues[$valueId]['store_description']) ? $obValues[$valueId]['store_description'] : null                                                                                       
                    );  
              }
            
             $options[] = $option;
             $hasOrder |= $_option->getPosition() > 0;             
          }        
        }

        $t = 'o';
        $obOptions = Mage::getModel('optionbundle/option')->getOptions($this->getProduct()->getId(), (int)$this->getProduct()->getStoreId());          
        foreach ($this->getProduct()->getOptions() as $_option) {
          $id = (int) $_option->getOptionId();
          $option = array(
            'type' => $t,            
            'id' => $id,
            'title' => $this->htmlEscape($_option->getTitle()),         
            'sort_order' => $_option->getSortOrder(), 
            'required' => $_option->getIsRequire(),             
            'default' => isset($obOptions[$id]['default']) ? $obOptions[$id]['default'] : '', 
            'note' => isset($obOptions[$id]['note']) ? $obOptions[$id]['note'] : '',
            'store_note' => isset($obOptions[$id]['store_note']) ? $obOptions[$id]['store_note'] : null,                  
            'layout' => isset($obOptions[$id]['layout']) ? $obOptions[$id]['layout'] : '',  
            'popup' => isset($obOptions[$id]['popup']) ? $obOptions[$id]['popup'] : 0,
            'option_type' => $_option->getType(),                
            'values' => array()
          );        


          if ($_option->getGroupByType() == Mage_Catalog_Model_Product_Option::OPTION_GROUP_SELECT) {
          
            $option['selection_type'] = $_option->getType() == Mage_Catalog_Model_Product_Option::OPTION_TYPE_DROP_DOWN || $_option->getType() == Mage_Catalog_Model_Product_Option::OPTION_TYPE_RADIO ? 'single' : 'multiple';
            
            $obValues = Mage::getModel('optionbundle/value')->getValues($this->getProduct()->getId(), (int)$this->getProduct()->getStoreId());              
            foreach ($_option->getValues() as $value) {

              $valueId = (int) $value->getOptionTypeId();
              $image = isset($obValues[$valueId]['image']) ? $obValues[$valueId]['image'] : '';
              $option['values'][] = array(
                  'id' => $valueId,
                  'title' => $this->htmlEscape($value->getTitle()),
                  'price' => $this->getPriceValue($value->getPrice(), $value->getPriceType() == 'percent'),
                  'image' => $image,
                  'image_json' => $this->getImageJson($image), 
                  'description' => isset($obValues[$valueId]['description']) ? $obValues[$valueId]['description'] : '',
                  'store_description' => isset($obValues[$valueId]['store_description']) ? $obValues[$valueId]['store_description'] : null                                                                        
                  );
            }
            
           }
           
           $options[] = $option;
        }       
       
        if ($hasOrder){   
          usort($options, array(Mage::helper('optionbundle'), "sortOptions"));
        }
         
        $this->_options = $options; 
     }
                        
     return $this->_options;                         
    }                            


    
    public function getImageJson($value)
    {
      $js = '';   
      if ($value != '') {
        $image['url'] = $this->helper('catalog/image')->init(Mage::getModel('catalog/product'), 'thumbnail', $value)->keepFrame(true)->resize(100,100)->__toString();
        $js = Zend_Json::encode($image);
      }
      return $js;
    }

    
    public function getConfigJson()
    {
      
      $uploaderConfig = Mage::getModel('uploader/config_uploader')
        ->setSingleFile(true)
        ->setFileParameterName('image')
        ->setTarget(
            Mage::getModel('adminhtml/url')
                ->addSessionParam()
                ->getUrl('*/catalog_product_gallery/upload', array('_secure' => true))
        );  
        
      $elementIds = array(
            'container'    => $this->getUploaderId() . '-new',
            'templateFile' => $this->getUploaderId() . '-template',
            'browse'       => array($this->getUploaderId() . '-browse'),
            'delete'       => $this->getUploaderId() . '-delete'
      );
        
      $browseConfig = Mage::getModel('uploader/config_browsebutton')
        ->setSingleFile(true);
      
      $miscConfig = Mage::getModel('uploader/config_misc')
        ->setReplaceBrowseWithRemove(true);
                        
      return $this->helper('core')->jsonEncode(array(
          'uploaderConfig'    => $uploaderConfig->getData(),
          'elementIds'        => $elementIds,
          'browseConfig'      => $browseConfig->getData(),
          'miscConfig'        => $miscConfig->getData()
      ));      
    }


     public function getUploaderId()
    {
      return $this->_uploaderId;
    }
    
    
     public function getLayoutSelect($option)
    {
      $options = array();
      
      switch($option['option_type']){
        case 'radio' :
          $options = array(
            array('value' =>'above' , 'label'=>$this->__('Above Option')),        
            array('value' =>'before', 'label'=>$this->__('Before Option')),
            array('value' =>'below' , 'label'=>$this->__('Below Option')),
            array('value' =>'swap'  , 'label'=>$this->__('Main Image')),
            array('value' =>'grid'  , 'label'=>$this->__('Grid')),    
            array('value' =>'list'  , 'label'=>$this->__('List'))                
          );
        break;
        case 'checkbox' :
          $options = array(
            array('value' =>'above', 'label'=>$this->__('Above Option')),         
            array('value' =>'below', 'label'=>$this->__('Below Option')),
            array('value' =>'grid' , 'label'=>$this->__('Grid')),    
            array('value' =>'list' , 'label'=>$this->__('List'))     
          );
        break;
        case 'drop_down' :
          $options = array(
            array('value' =>'above'     , 'label'=>$this->__('Above Option')),         
            array('value' =>'before'    , 'label'=>$this->__('Before Option')),
            array('value' =>'below'     , 'label'=>$this->__('Below Option')),
            array('value' =>'swap'      , 'label'=>$this->__('Main Image')),
            array('value' =>'picker'    , 'label'=>$this->__('Color Picker')), 
            array('value' =>'pickerswap', 'label'=>$this->__('Picker & Main'))                   
          );
        break;       
        case 'multiple' :
          $options = array(
            array('value' =>'above', 'label'=>$this->__('Above Option')),         
            array('value' =>'below', 'label'=>$this->__('Below Option'))          
          );
        break;            
      }
      
      $select = $this->getLayout()->createBlock('adminhtml/html_select')
          ->setData(array(
              'id' => 'optionbundle_'.$option['type'].'_'.$option['id'].'_layout',
              'class' => 'select optionbundle-layout-select',
              'extra_params' => 'onchange="optionBundleImage.changePopup(\''.$option['type'].'\','.$option['id'].')"'                               
          ))
          ->setName('product[optionbundle_'.($option['type'] == 'b' ? 'boption' : 'option').']['.$option['id'].'][layout]')
          ->setValue($option['layout'])             
          ->setOptions($options);

      return $select->getHtml();
    }    
    
    
    public function hasCustomOptions()
    {     
      return count($this->getProduct()->getOptions()) > 0;                         
    } 
    
    
           
    public function isBundle()
    {     
      return $this->getProduct()->getTypeId() == Mage_Catalog_Model_Product_Type::TYPE_BUNDLE;                         
    }     
    
    
    
    public function hasBoptions()
    {     
      return $this->isBundle() && count($this->getBoptions()) > 0;                         
    }     
    
         	  
	  
    public function getPriceValue($value, $isPercent)
    {
      if ((int)$value == 0)
        return '';        
      return $isPercent ? (float) $value : number_format($value, 2, null, '');
    }	  
	  
    public function getNoteDisabled($option)
    {
      return is_null($option['store_note']) && $this->getProduct()->getStoreId() > 0 ? 'disabled="disabled"' : '';
    }

    public function getDescriptionDisabled($value)
    {
      return is_null($value['store_description']) && $this->getProduct()->getStoreId() > 0 ? 'disabled="disabled"' : '';
    }

    public function getNoteShowHidden($option)
    {
      return is_null($option['store_note']) && $this->getProduct()->getStoreId() > 0 ? 'style="display:none"' : '';
    }

    public function getDescriptionShowHidden($value)
    {
      return is_null($value['store_description']) && $this->getProduct()->getStoreId() > 0 ? 'style="display:none"' : '';
    }
    	  
    public function getNoteScopeHtml($option)
    {
    
      if ($this->getProduct()->getStoreId() == 0)
        return '';
 
      $checked   = is_null($option['store_note']);
      $inputId   = "optionbundle_{$option['type']}_{$option['id']}_note";
      $type      = $option['type'] == 'b' ? 'boption' : 'option';
      $inputName = "product[optionbundle_{$type}][{$option['id']}][scope][note]";             
      
      $checkbox  = '<br/><input type="checkbox" id="'. $inputId .'_use_default" class="product-option-scope-checkbox" name="'. $inputName .'" value="1" '. ($checked ? 'checked="checked"' : '') .'  onclick="optionBundleImage.setScope(this, \''. $inputId .'\')"/>'.
                   '<label class="normal" for="'. $inputId .'_use_default"> '. Mage::helper('adminhtml')->__('Use Default Value') .'</label>';
                    
      return $checkbox;
    }	  
	  

    public function getDescriptionScopeHtml($option, $value)
    {
    
      if ($this->getProduct()->getStoreId() == 0)
        return '';
 
      $checked    = is_null($value['store_description']);
      $inputId    = "optionbundle_{$option['type']}_{$value['id']}_description";
      $type       = $option['type'] == 'b' ? 'bvalue' : 'value';
      $inputName  = "product[optionbundle_{$type}][{$value['id']}][scope][description]"; 
      
      $checkbox   = '<br/><input type="checkbox" id="'. $inputId .'_use_default" class="product-option-scope-checkbox" name="'. $inputName .'" value="1" '. ($checked ? 'checked="checked"' : '') .'  onclick="optionBundleImage.setScope(this, \''. $inputId .'\')"/>'.
                    '<label class="normal" for="'. $inputId .'_use_default"> '. Mage::helper('adminhtml')->__('Use Default Value') .'</label>';
                    
      return $checkbox;
    }
	  
	               
    public function getShowTextArea($option, $value = null)
    {
      if ($this->getIsWysiwygEnabled()){
        $wysiwygUrl = Mage::helper('adminhtml')->getUrl('adminhtml/catalog_product/wysiwyg');
        if (!is_null($value))
          $fieldId = 'optionbundle_'.$option['type'].'_'.$value['id'].'_description';
        else  
          $fieldId = 'optionbundle_'.$option['type'].'_'.$option['id'].'_note';      
        $extra = 'onclick="catalogWysiwygEditor.open(\''. $wysiwygUrl .'\', \''. $fieldId .'\')"';   
      } else {
        $extra = 'onclick="optionBundleImage.showTextArea(this)"';     
      } 

      return $extra;
    }	  
	  
	  
    public function getClickToEditText()
    {
        return $this->getIsWysiwygEnabled() ? Mage::helper('catalog')->__('WYSIWYG Editor') : $this->__('Click to edit');   
    }  
    
    public function getIsWysiwygEnabled()
    {
        $version = Mage::getVersion();
        return version_compare($version, '1.4.0.0') >= 0 && Mage::getSingleton('cms/wysiwyg_config')->isEnabled();
    } 
        	  
    public function getTabUrl()
    {
        return $this->getUrl('adminhtml/ob_options/optionimages', array('_current'=>true));
    }

    public function getTabClass()
    {
        return 'ajax';
    }
        
    public function getTabLabel()
    {
        return $this->__('Option Images');
    }
        
    public function getTabTitle()
    {
        return $this->__('Option Images');
    }
        
    public function canShowTab()
    {
        return $this->getProduct()->getId() != null;
    }    
    
    public function isHidden()
    {
        return false;
    }

}
