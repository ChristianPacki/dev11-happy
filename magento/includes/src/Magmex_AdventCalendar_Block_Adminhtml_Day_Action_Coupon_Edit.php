<?php
/**
 * Magmex: Edit the Advent Calendar action Coupon for an Advent Calendar day
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.magmex.com/MAGMEX_SOFTWARE_LICENSE.txt
 *
 * @copyright Copyright (c) 2015 magmex, Michael Stork, (http://www.magmex.com)
 * @license http://www.magmex.com/MAGMEX_SOFTWARE_LICENSE.txt
 * @category Magmex
 * @package Magmex_AdventCalendar
 * @author Michael Stork <m.stork@magmex.com>
 */
class Magmex_AdventCalendar_Block_Adminhtml_Day_Action_Coupon_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    /**
     * Construct
     * Define all preferences of the Edit Block plus the form child block
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->_objectId = 'advent_calendar_day_action_coupon_id';
        $this->_blockGroup = 'magmex_adventcalendar';
        $this->_controller = 'adminhtml_day_action';
        $this->_mode = 'coupon_edit';
        
        $helperData = Mage::helper('magmex_adventcalendar');
        
        $this->_updateButton('save', 'label', $helperData->__('Save Advent Calendar day with action Coupon'));
        $this->removeButton('delete');
        
        $this->_addButton('save_and_continue', array(
                'label'     => $helperData->__('Save And Continue Edit'),
                'onclick'   => 'saveAndContinueEdit()',
                'class'     => 'save',
        ), -100);

        $this->_formScripts[] = "
            function toggleEditor() {
                if (tinyMCE.getInstanceById('form_content') == null) {
                    tinyMCE.execCommand('mceAddControl', false, 'edit_form');
                } else {
                    tinyMCE.execCommand('mceRemoveControl', false, 'edit_form');
                }
            }

            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";
    }
    
    /**
     * Sets header text according to selected edit action
     *
     * @see Mage_Adminhtml_Block_Widget_Container::getHeaderText()
     * @return string header text
     */
    public function getHeaderText()
    {
        return Mage::helper('magmex_adventcalendar')->__('Edit Advent Calendar day with action Coupon');
    }
    
    /**
     * Returns the back url
     *
     * @see Mage_Adminhtml_Block_Widget_Form_Container::getBackUrl()
     * @return string back url to the advent calendar day grid
     */
    public function getBackUrl()
    {
        return $this->getUrl('*/adventcalendar_day/');
    }
}