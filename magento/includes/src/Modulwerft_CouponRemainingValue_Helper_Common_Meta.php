<?php

/**
 * @category Modulwerft
 * @package Modulwerft_CouponRemainingValue
 * @author Modulwerft <info@modulwerft.com>
 * @author Benjamin Wunderlich <b.wunderlich@shopwerft.com>
 * @copyright 2014 Shopwerft GmbH (http://www.shopwerft.com)
 */

class Modulwerft_CouponRemainingValue_Helper_Common_Meta
    extends Mage_Core_Helper_Abstract
{

    const CHECK_UPDATE_URL_DE = 'http://www.modulwerft.com/extension/version/check/version/%s/extension/%s';
    const CHECK_UPDATE_URL_EN = 'http://en.modulwerft.com/extension/version/check/version/%s/extension/%s';

    /**
     * Retrieve module config node.
     *
     * @return Varien_Simplexml_Object
     */
    protected function _getModuleConfig()
    {
        return Mage::getConfig()->getModuleConfig($this->_getModuleName());
    }

    /**
     * Retrieve the current version of the module.
     *
     * @return string
     */
    public function getModuleVersion()
    {
        return (string) $this->_getModuleConfig()->version;
    }

    /**
     * Retrieve the modulwerft sku of the module.
     *
     * @return string
     */
    public function getModuleSku()
    {
        return (string) $this->_getModuleConfig()->modulwerft_sku;
    }

    /**
     * Retrieve the URL to check if module updates are available.
     *
     * @param string $locale
     * @return string
     */
    public function getCheckUpdateUrl($locale)
    {
        if (!is_string($locale) || !preg_match('#\A[a-z]{2}_[A-Z]{2}\z#', $locale)) {
            return '';
        }

        if ('de' == substr($locale, 0, 2)) {
            $updateUrl = self::CHECK_UPDATE_URL_DE;
        } else {
            $updateUrl = self::CHECK_UPDATE_URL_EN;
        }
        $updateUrl = sprintf($updateUrl, $this->getModuleVersion(), $this->getModuleSku());

        return sprintf('(<a href="%s" target="_blank">' . $this->__('check for updates') . '</a>)', $updateUrl);
    }

}