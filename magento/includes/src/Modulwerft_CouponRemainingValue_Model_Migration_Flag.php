<?php

/**
 * @category Modulwerft
 * @package Modulwerft_CouponRemainingValue
 * @author Modulwerft GmbH <info@modulwerft.com>
 * @author Philipp Zabel <p.zabel@shopwerft.com>
 * @copyright 2014 Shopwerft GmbH (http://www.shopwerft.com)
 * @since 1.2.0
 */
class Modulwerft_CouponRemainingValue_Model_Migration_Flag
    extends Mage_Core_Model_Flag
{

    const MIGRATION_DONE = '1';

    /** @var string */
    protected $_flagCode = 'modulwerft_couponremainingvalue_migration';

    /**
     * @return bool
     */
    public function isPending()
    {
        return self::MIGRATION_DONE != $this->getState();
    }

    /**
     * @return $this
     */
    public function setDone()
    {
        $this->setState(self::MIGRATION_DONE);
        return $this;
    }

}
