<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_Faq
 */

class Amasty_Faq_Model_Resource_Question extends Amasty_Faq_Model_Resource_Relational
{
    protected function _construct()
    {
        $this->_init('amfaq/question', 'question_id');
    }

    public function updateVotes($object, $dropVotes = false)
    {
        $table = $this->getTable('amfaq/question_rating');

        $ratingType = Mage::getStoreConfig('amfaq/rating/type');

        if ($dropVotes)
        {
            $this->_getWriteAdapter()->update(
                $table,
                array('used' => 0),
                array(
                    'question_id=?' => $object->getId(),
                    'rating_type' => $ratingType
                )
            );
        }

        $adapter = $this->_getReadAdapter();

        switch ($ratingType) {
            case Amasty_Faq_Model_Question_Rating::TYPE_STARS:
                $select = $this->_getCountSelect($table, 'COUNT(*) as `count`, AVG(rate) as rate', $object->getId(), 1, $ratingType);
                $votes = $adapter->fetchRow($select);

                $select = $this->_getCountSelect($table, 'COUNT(*)', $object->getId(), 0, $ratingType);
                $ignored = +$adapter->fetchOne($select);

                $used = +$votes['count'];
                $avg = +$votes['rate'];
                $rating = +$object->getRating();
                $total = $used + $ignored;

                $result = $rating;

                if (!$dropVotes && $used > 0) {
                    if ($rating) {
                        $result = $ignored / $total * $rating + $used / $total * $avg;
                    } else {
                        $result = $avg;
                    }
                }

                $object->setCalculatedRating($result);
                break;
            case Amasty_Faq_Model_Question_Rating::TYPE_YESNO:
                $select = $this->_getCountSelect($table, 'COUNT(*)', $object->getId(), 1, $ratingType, 2);
                $votes = $adapter->fetchRow($select);

                $select = $this->_getCountSelect($table, 'COUNT(*)', $object->getId(), 0, $ratingType, 2);
                $ignored = $adapter->fetchRow($select);

                $result = $votes['COUNT(*)'] + $ignored['COUNT(*)'];

                $object->setYesnoRating($result);
                break;
        }
    }

    protected function _getCountSelect($table, $cols, $questionId, $used, $ratingType, $rate = null)
    {
        $adapter = $this->_getReadAdapter();
        $select = $adapter
            ->select()
            ->from($table, $cols)
            ->where("question_id = ?", $questionId)
            ->where("used = ?", $used)
            ->where('rating_type = ?', $ratingType)
        ;
        if (!is_null($rate)) {
            $select->where('rate = ?', $rate);
        }
        return $select;
    }

    protected function _afterLoad(Mage_Core_Model_Abstract $object)
    {
        $this->loadExternalIds($object, 'topic');
        $this->loadExternalIds($object, 'product');
        $this->loadExternalIds($object, 'store');

        $this->loadTags($object);

        return parent::_afterLoad($object);
    }

    protected function _afterSave(Mage_Core_Model_Abstract $object)
    {
        $this->saveExternalIds($object, 'topic');
        $this->saveExternalIds($object, 'product');
        $this->saveExternalIds($object, 'store');

        $this->saveRelated($object);
        $this->saveTags($object);

        return parent::_afterSave($object);
    }

    protected function _afterDelete(Mage_Core_Model_Abstract $object)
    {
        $this->cleanupTags();

        return parent::_afterDelete($object);
    }

    protected function loadTags($object)
    {
        $tags = Mage::getResourceModel('amfaq/tag_collection');
        $tags->getSelect()
            ->join(
                array('qt' => $this->getTable('amfaq/question_tag')),
                'qt.tag_id = main_table.tag_id',
                array()
            )
            ->where('qt.question_id = ?', $object->getId())
        ;
        $object->setTags(implode(', ', $tags->getColumnValues('title')));
        $object->setTagsCollection($tags);
    }

    protected function saveTags($object)
    {
        $this->loadExternalIds($object, 'tag');

        $tags = preg_split('/\s*,\s*/', trim($object->getData('tags')), -1, PREG_SPLIT_NO_EMPTY);

        $oldTags = Mage::getResourceModel('amfaq/tag_collection');

        $oldTags->getSelect()
            ->join(
                array('qt' => $this->getTable('amfaq/question_tag')),
                'qt.tag_id = main_table.tag_id',
                array()
            )
            ->where('qt.question_id = ?', $object->getId())
        ;

//        $oldTagsIds = $oldTags->getAllIds();

        $existingTags = Mage::getResourceModel('amfaq/tag_collection')
            ->addFieldToFilter('title', array('in' => $tags))
//            ->getColumnValues('title')
        ;

        $delete = array();
        $insert = array_fill_keys($tags, 0);

        foreach ($oldTags as $tag)
        {
            if (!in_array($tag->getTitle(), $tags))
                $delete []= $tag->getId();

            if (isset($insert[$tag->getTitle()]))
                unset($insert[$tag->getTitle()]);
        }

        foreach ($existingTags as $tag)
        {
            if (isset($insert[$tag->getTitle()]))
                $insert[$tag->getTitle()] = $tag->getId();
        }

        if ($delete) {
            $where = array(
                "question_id = ?" => $object->getId(),
                "tag_id IN (?)" => $delete
            );

            $this->_getWriteAdapter()->delete($this->getTable('amfaq/question_tag'), $where);
        }

        if ($insert) {
            foreach ($insert as $tag => $id)
            {
                if (!$id)
                {
                    $id = Mage::getModel('amfaq/tag')
                        ->setTitle($tag)
                        ->generateAlias()
                        ->save()
                        ->getId()
                    ;
                }

                $data = array(
                    "question_id"  => (int) $object->getId(),
                    "tag_id" => (int) $id
                );

                $this->_getWriteAdapter()->insertMultiple($this->getTable('amfaq/question_tag'), $data);
            }
        }

        $this->cleanupTags();
    }

    protected function cleanupTags()
    {
        $tags = Mage::getResourceModel('amfaq/tag_collection');

        $tags
            ->getSelect()
            ->joinLeft(
                array('qt' => $this->getTable('amfaq/question_tag')),
                'qt.tag_id = main_table.tag_id',
                array('uses' => 'COUNT(qt.question_id)')
            )
            ->having('uses = 0')
            ->group('tag_id')
        ;

        foreach ($tags as $tag)
            $tag->delete();
    }

    protected function saveRelated($object)
    {
        $old = $object->getRelatedQuestions()->getAllIds();
        $new = $object->getQuestionIds();

        if (!is_array($new))
            $new = array();

        $resource = Mage::getSingleton('core/resource');

        $table = $resource->getTableName('amfaq/question_relation');

        $insert = array_diff($new, $old);
        $delete = array_diff($old, $new);

        if ($delete) {
            $where = array(
                'a = ?'     => (int) $object->getId(),
                'b IN (?)' => $delete
            );

            $this->_getWriteAdapter()->delete($table, $where);

            if (Mage::getStoreConfig('amfaq/answer_page/two_way_relations'))
            {
                $where = array(
                    'b = ?'     => (int) $object->getId(),
                    'a IN (?)' => $delete
                );

                $this->_getWriteAdapter()->delete($table, $where);
            }
        }

        if ($insert) {
            $data = array();

            foreach ($insert as $id) {
                $data[] = array(
                    'a'  => (int) $object->getId(),
                    'b' => (int) $id
                );
            }

            $this->_getWriteAdapter()->insertMultiple($table, $data);
        }
    }
}
