<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_Faq
 */

class Amasty_Faq_Model_Config_Source_Blocks
{
    public function toOptionArray()
    {
        $model = Mage::getModel('catalog/category_attribute_source_page');

        return $model->getAllOptions();
    }
}
