<?php

/**
 * @category Modulwerft
 * @package Modulwerft_CouponRemainingValue
 * @author Modulwerft GmbH <info@modulwerft.com>
 * @author Philipp Zabel <p.zabel@shopwerft.com>
 * @copyright 2014 Shopwerft GmbH (http://www.shopwerft.com)
 * @since 1.2.0
 */
class Modulwerft_CouponRemainingValue_Adminhtml_Promo_Quote_CouponController
    extends Mage_Adminhtml_Controller_Action
{

    protected function _initRule()
    {
        Mage::register('current_promo_quote_rule', Mage::getModel('salesrule/rule'));
        $id = (int)$this->getRequest()->getParam('id');

        if (!$id && $this->getRequest()->getParam('rule_id')) {
            $id = (int)$this->getRequest()->getParam('rule_id');
        }

        if ($id) {
            Mage::registry('current_promo_quote_rule')->load($id);
        }
    }

    public function historyAction()
    {
        $this->_initRule();

        $gridBlock = $this->getLayout()
            ->createBlock('modulwerft_couponremainingvalue/adminhtml_promo_quote_edit_tab_couponRemainingValue_grid');

        $this->getResponse()->setBody($gridBlock->toHtml());
    }

    /**
     * Export coupon codes remaining value as CSV file
     *
     * @return void
     */
    public function exportCouponsRemainingValueCsvAction()
    {
        $this->_initRule();

        $fileName = 'coupons_remaining_value.csv';
        $content = $this->getLayout()
            ->createBlock('modulwerft_couponremainingvalue/adminhtml_promo_quote_edit_tab_couponRemainingValue_grid')
            ->getCsvFile();
        $this->_prepareDownloadResponse($fileName, $content);
    }

    /**
     * Export coupon codes remaining value as excel xml file
     *
     * @return void
     */
    public function exportCouponsRemainingValueXmlAction()
    {
        $this->_initRule();

        $fileName = 'coupons_remaining_value.xml';
        $content = $this->getLayout()
            ->createBlock('modulwerft_couponremainingvalue/adminhtml_promo_quote_edit_tab_couponRemainingValue_grid')
            ->getExcelFile();
        $this->_prepareDownloadResponse($fileName, $content);
    }

    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('promo/quote');
    }

}
