<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_Faq
 */

abstract class Amasty_Faq_Model_Resource_Relational extends Mage_Core_Model_Resource_Db_Abstract
{
    protected function loadExternalIds($object, $type)
    {
        $resource = Mage::getSingleton('core/resource');

        $table = $resource->getTableName("{$this->_resourcePrefix}/{$this->_mainTable}_{$type}");
        $field = "{$type}_id";
        $data = "{$type}_ids";

        $adapter = $this->_getReadAdapter();

        $select = $adapter
            ->select()
            ->from($table, $field)
            ->where("{$this->_mainTable}_id = ?", $object->getId());

        $ids = $adapter->fetchCol($select);

        $object->setData($data, $ids);
    }

    protected function saveExternalIds($object, $type)
    {
        $resource = Mage::getSingleton('core/resource');

        $table = $resource->getTableName("{$this->_resourcePrefix}/{$this->_mainTable}_{$type}");
        $data = "{$type}_ids";

        $new = $object->getData($data);
        $old = $object->getOrigData("{$type}_ids");

        if (!is_array($old))
            $old = array();

        if (!is_array($new))
            $new = array();
        else
        {
            foreach ($new as $k => $v)
                if (!is_numeric($v))
                    unset($new[$k]);
        }


        $insert = array_diff($new, $old);
        $delete = array_diff($old, $new);

        if ($delete) {
            $where = array(
                "{$this->_mainTable}_id = ?"     => (int) $object->getId(),
                "{$type}_id IN (?)" => $delete
            );

            $this->_getWriteAdapter()->delete($table, $where);
        }

        if ($insert) {
            $data = array();

            foreach ($insert as $id) {
                $data[] = array(
                    "{$this->_mainTable}_id"  => (int) $object->getId(),
                    "{$type}_id" => (int) $id
                );
            }

            $this->_getWriteAdapter()->insertMultiple($table, $data);
        }
    }
}