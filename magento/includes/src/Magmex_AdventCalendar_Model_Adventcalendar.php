<?php
/**
 * Magmex Adventcalendar main model
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.magmex.com/MAGMEX_SOFTWARE_LICENSE.txt
 *
 * @copyright Copyright (c) 2015 magmex, Michael Stork, (http://www.magmex.com)
 * @license http://www.magmex.com/MAGMEX_SOFTWARE_LICENSE.txt
 * @category Magmex
 * @package Magmex_AdventCalendar
 * @author Michael Stork <m.stork@magmex.com>
 */

class Magmex_AdventCalendar_Model_Adventcalendar extends Mage_Core_Model_Abstract
{
    /**
     * Defines the Magmex image path
     *
     * @var string
     */
    const MEDIA_SUB_IMAGE_PATH = 'magmex/adventcalendar';
    
    /**
     * Defines the the background image path for standard background images
     *
     * @var unknown_type
     */
    const SKIN_STANDARD_BACKGROUND_IMAGE_PATH = 'skin/frontend/base/default/images/magmex/adventcalendar/standard_background_images';
    
    /**
     * Contains the advent calendar day collection if
     *
     * @var Magmex_AdventCalendar_Model_Mysql4_Day_Collection
     */
    protected $_adventCalendarDayCollection;
    
    
    /**
     * Contains the possible background image types
     *
     * @var array
     */
    protected $_standardBackgroundImageTypes = array(
        'standard_background_one',
        'standard_background_two',
        'standard_background_three',
        'standard_background_four'
    );
    
    /**
     * Contains the possible values for layout_type
     *
     * @var array
     */
    protected $_layoutTypes = array(
        'randomSizeDoors',
        'sameSizeDoors'
    );
    
    /**
     * Construct
     * Inits the resource model
     *
     * @see Varien_Object::_construct()
     */
    protected function _construct()
    {
        parent::_construct();
        $this->_init('magmex_adventcalendar/adventcalendar');
    }
    
    /**
     * Initalizes
     *
     * @see Mage_Core_Model_Abstract::_afterLoad()
     * @return Mage_Core_Model_Abstract chaining
     */
    protected function _afterLoad()
    {
        $this->_addAdventCalendarDayCollection();
    }
    
    /**
     * Inits the advent calendar day collection for the actual advent calendar model
     * sorted by the randomly generated sort order
     *
     * @return Magmex_AdventCalendar_Model_Adventcalendar chaining
     */
    protected function _addAdventCalendarDayCollection()
    {
        $this->_adventCalendarDayCollection = Mage::getModel('magmex_adventcalendar/day')->getCollection()
            ->addFieldToFilter('advent_calendar_id', $this->getId())
            ->setOrder('sort_order', Magmex_AdventCalendar_Model_Mysql4_Day_Collection::SORT_ORDER_ASC);
        return $this;
    }
    
    /**
     * Adds or edits new advent calendar days for a year.
     * Does nothing, if no new days have to be added or nothing changed
     *
     * @return Magmex_AdventCalendar_Model_Adventcalendar chaining
     */
    protected function _generateAndSaveAdventCalendarDays($createRandomDoors = true)
    {
        //If original data is set, we have to check, if day positions, door colors and door background images
        //should be randomized - normally they should not change except the user selected this option in the
        //advent calendar form.
        //If only one color has changed, then just change the colors randomly.
        $originalData = $this->getOrigData();
        
        $randomizeDoorPositionsAndColorsAndBackgroundImages = $this->getRandomizeDoorPositionsAndColorsAndBackgroundImages();
        
        if (empty($originalData) || $randomizeDoorPositionsAndColorsAndBackgroundImages == '1') {
            $this->_saveAdventCalendarDays(true, false);
        } else if ($this->_areOldAndNewColorsEqual() === false) {
            $this->_saveAdventCalendarDays(false, true);
        }

        //Update the advent calendard day dates, if the year has changed.
        $this->_updateAdventCalendarDayDates();
    
        return $this;
    }
    
    /**
     * Saves an advent calendar day.
     * If $randomizeAll is set true, then all data (door day positions, door colors, door background images) get randomized, but
     * if the advent calendar day was already existant, the date, the id and the adventCalendarId remain the same - if it
     * is a new advent calendar day, all data is saved
     *
     * If $randomizeAll is set to true, the option $randomizeColor is ignored.
     *
     * If $randomizeColor is set to true and $randomizeAll is set to false, all advent calendar day data (id, adventCalendarId,
     * day door position, door background images) remain the same and just the door colors get randomized again
     *
     * @param boolean $randomizeAll if true, all data gets randomized (dependened on already existant advent calendar days)
     * @param boolean $randomizeColor (only colors get randomized - only possible for already existant advent calendar days)
     * @throws Exception if a non allowed combination of $randomizeAll and $randomize color was given (both false
     * @return void
     */
    protected function _saveAdventCalendarDays($randomizeAll, $randomizeColor)
    {
        if ($randomizeAll === true) {
            $dayRangeArray = $this->_getChristmasRangeArray();
            $randomOrderArray = $this->_getRandomChristmasRangeArray();
            $randomBackgroundImagePositionArray = $this->_getRandomChristmasRangeArray();
            $randomColorArray = $this->_getRandomColorArray();
            
            foreach ($dayRangeArray as $key => $day) {
                $adventCalendarDate = $this->getYear() . '-12-' . $day;
                
                /** @var Magmex_AdventCalendar_Model_Day $modelAdventCalendarDay */
                $modelAdventCalendarDay = Mage::getModel('magmex_adventcalendar/day')->loadByAdventCalendarIdAndDate($this->getId(), $adventCalendarDate);
                $adventCalendarDayId = $modelAdventCalendarDay->getId();
                
                $modelAdventCalendarDay->setSortOrder($randomOrderArray[$key])
                    ->setBackgroundColor($randomColorArray[$key])
                    ->setBackgroundImagePosition($randomBackgroundImagePositionArray[$key]);
                
                if (empty($adventCalendarDayId)) {
                    $modelAdventCalendarDay->setAdventCalendarId($this->getId())
                        ->setDate($adventCalendarDate);
                }
                
                $modelAdventCalendarDay->save();
            }
        } else if ($randomizeAll === false && $randomizeColor === true) {
            $randomColorArray = $this->_getRandomColorArray();
            $adventCalendarDayCollection = Mage::getModel('magmex_adventcalendar/day')->getCollection()
                ->addFieldToFilter('advent_calendar_id', $this->getId());
            
            $counter = 0;
            foreach ($adventCalendarDayCollection as $modelAdventCalendarDay) {
                $modelAdventCalendarDay->setBackgroundColor($randomColorArray[$counter]);
                $modelAdventCalendarDay->save();
                $counter++;
            }
        } else {
            throw new Exception('combination of randomizeAll=false and randomizeColor=false not allowed. '
                              . 'Use randomizeAll=true and randomizeColor=false, to save a new advent calendar day');
        }
    }

    /**
     * Updates the advent calendar days to the selected year if the year of the advent calendar has changed.
     *
     * @return Magmex_AdventCalendar_Model_Adventcalendar chaining
     */
    protected function _updateAdventCalendarDayDates()
    {
        //Do nothing, if the year has not changed.
        if ($this->getOrigData('year') === $this->getYear()) {
            return $this;
        }

        $adventCalendarDayCollection = Mage::getModel('magmex_adventcalendar/day')->getCollection()
            ->addFieldToFilter('advent_calendar_id', $this->getId());

        foreach ($adventCalendarDayCollection as $modelAdventCalendarDay) {
            $adventCalendarDayNumber = date('d', strtotime($modelAdventCalendarDay->getDate()));
            $newAdventCalendarDayDate = $this->getYear() . '-12-' . $adventCalendarDayNumber;
            $modelAdventCalendarDay->setDate($newAdventCalendarDayDate)
                ->save();
        }

        return $this;
    }
    
    /**
     * Returns an array with the range of the 24 christmas days
     *
     * @return array from 1 to 24
     */
    protected function _getChristmasRangeArray()
    {
        return range(1, 24);
    }
    
    /**
     * Returns a a random array with the range of the 24 christmas days
     *
     * @return array random from 1 to 24
     */
    protected function _getRandomChristmasRangeArray()
    {
        $christmasRangeArray = $this->_getChristmasRangeArray();
        shuffle($christmasRangeArray);
        return $christmasRangeArray;
    }
    
    /**
     * Checks, wether old and new colors set in the model are equal
     *
     * @return boolean true, if originalData colors are equal to actual color data in the model
     */
    protected function _areOldAndNewColorsEqual()
    {
        $originalData = $this->getOrigData();
        if (empty($originalData)) {
            return false;
        } else {
            $colorArray = array();
            $colorOriginalArray = array();
            foreach ($this->_getColorFieldNameArray() as $colorFieldName) {
                $color = $this->getData($colorFieldName);
                if (!empty($color)) {
                    $colorArray[$colorFieldName] = $color;
                }
                
                $colorOriginal = $this->getOrigData($colorFieldName);
                if (!empty($colorOriginal)) {
                    $colorOriginalArray[$colorFieldName] = $colorOriginal;
                }
            }
            
            //if no colors where set, we have just the main color and have to check, if the main color has changed,
            //otherwise the colorArray is compared to the original data colors
            if ($colorArray !== $colorOriginalArray) {
                return false;
            } else {
                if (empty($colorArray) && $this->getMainColor() !== $this->getOrigData('main_color')) {
                    return false;
                }
            }
        }
        return true;
    }
    
    /**
     * Returns an array with all available color data field names
     *
     * @return array color field names
     */
    protected function _getColorFieldNameArray()
    {
        $colorGetterMethodArray = array(
            'door_color_one',
            'door_color_two',
            'door_color_three',
            'door_color_four',
            'door_color_five'
        );
        
        return $colorGetterMethodArray;
    }
    
    
    /**
     * Returns an array with 24 random background colors based on the maximum 5 defined
     * colors for the days/doors
     *
     * @return array empty array, if no color is defined, otherwise an array with
     */
    protected function _getRandomColorArray()
    {
        $randomColorArray = array();
        $colorFieldNameArray = $this->_getColorFieldNameArray();
        
        $colorArray = array();
        foreach ($colorFieldNameArray as $colorFieldName) {
            $color = $this->getData($colorFieldName);
            if (!empty($color)) {
                $colorArray[] = $color;
            }
        }
        
        $colorCount = count($colorArray);
        
        if ($colorCount > 0) {
            $breakPoint = 24 / $colorCount;
            $colorKeyCounter = 1;
            for($counter = 1; $counter <= 24; $counter++) {
                $randomColorArray[] = $colorArray[$colorKeyCounter - 1];
                if ($counter >= $breakPoint * $colorKeyCounter) {
                    $colorKeyCounter++;
                }
            }
            shuffle($randomColorArray);
        } else {
            //if no color was selected, all doors will become the advent calendar main color
            $randomColorArray = array_fill(0, 24, $this->getMainColor());
        }
        
        return $randomColorArray;
    }
    
    /**
     * Returns the advent calendar day collection and lazy loads it, if not done already
     *
     * @return Magmex_AdventCalendar_Model_Mysql4_Day_Collection advent calendar day collection
     */
    public function getAdventCalendarDayCollection()
    {
        if (empty($this->_adventCalendarDayCollection)) {
            $this->_addAdventCalendarDayCollection();
        }
        return $this->_adventCalendarDayCollection;
    }
    
    /**
     * Returns the possible standard background image types
     *
     * @return array background image types array
     */
    public function getStandardBackgroundImageTypes()
    {
        return $this->_standardBackgroundImageTypes;
    }
    
    /**
     * Returns the possible layout types
     *
     * @return array layout types array
     */
    public function getLayoutTypes()
    {
        return $this->_layoutTypes;
    }
    
    /**
     * Saves a new advent calendar and generates all advent calendar days
     *
     * @return Magmex_AdventCalendar_Model_Adventcalendar chaining
     */
    public function saveAdventCalendar()
    {
        $year = $this->getYear();
        if (!empty($year)) {
            try {
                $this->_getResource()->beginTransaction();
                $this->save();
                $this->_generateAndSaveAdventCalendarDays();
                $this->_getResource()->commit();
            } catch (Exception $exception) {
                $this->_getResource()->rollBack();
                throw $exception;
            }
        } else {
            throw new Exception(get_class() . ': new advent calender could not be saved, because no year was set; set year first with model->setYear()');
        }
    }
    
    /**
     * Loads the advent calendar model for the actual year
     *
     * @return Magmex_AdventCalendar_Model_Adventcalendar chaining
     */
    public function loadAdventCalendarForActualYear()
    {
        $currentTimestamp = Mage::getModel('core/date')->timestamp(time());
        $actualYear = date('Y', $currentTimestamp);
        
        $requestParamYear = Mage::app()->getRequest()->getParam('adventCalendarYear');
        if (!empty($requestParamYear)) {
            $actualYear = $requestParamYear;
        }
        
        $this->_getResource()
            ->loadAdventCalendarByYear($this, $actualYear);
        return $this;
    }
    
    /**
     * Loads an Advent Calendar by the given advent Calendar Day
     *
     * @param integer $adventCalendarDayId advent calendar day id
     * @return Magmex_AdventCalendar_Model_Adventcalendar loaded advent calendar instance
     */
    public function loadAdventCalendarByDayId($adventCalendarDayId)
    {
        $adventCalendarDay = Mage::getModel('magmex_adventcalendar/day')->load($adventCalendarDayId);
        $this->load($adventCalendarDay->getAdventCalendarId());
        return $this;
    }
}