<?php
class ML_Shopware_Model_Language extends ML_Shop_Model_Language_Abstract {
    public function getCurrentIsoCode() {
        if (MLSetting::gi()->blTranslateInline && MLSetting::gi()->sTranslationLanguage) {
            return MLSetting::gi()->sTranslationLanguage;
        }

        return Shopware()->Locale()->getLanguage();
    }

    public function getCurrentCharset() {
        return 'UTF-8';
    }
    
}