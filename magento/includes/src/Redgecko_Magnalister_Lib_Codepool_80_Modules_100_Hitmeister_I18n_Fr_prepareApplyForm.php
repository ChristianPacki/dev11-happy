<?php
/* Autogenerated file. Do not change! */

MLI18n::gi()->{'hitmeister_prepare_apply_form__legend__details'} = 'Fiche Produit';
MLI18n::gi()->{'hitmeister_prepare_apply_form__legend__categories'} = 'Catégorie';
MLI18n::gi()->{'hitmeister_prepare_apply_form__legend__variationmatching__0'} = 'Attribut de Hitmeister';
MLI18n::gi()->{'hitmeister_prepare_apply_form__legend__variationmatching__1'} = 'Attribut de boutique en ligne.';
MLI18n::gi()->{'hitmeister_prepare_apply_form__legend__variationmatchingoptional__0'} = 'hitmeister_prepare_apply_form__legend__variationmatchingoptional__0';
MLI18n::gi()->{'hitmeister_prepare_apply_form__legend__variationmatchingoptional__1'} = 'hitmeister_prepare_apply_form__legend__variationmatchingoptional__1';
MLI18n::gi()->{'hitmeister_prepare_apply_form__legend__unit'} = 'Attributs d\'articles';
MLI18n::gi()->{'hitmeister_prepare_apply_form__field__variationgroups__label'} = 'Catégorie Hitmeister';
MLI18n::gi()->{'hitmeister_prepare_apply_form__field__variationgroups.value__label'} = '1. Catégorie de la place de marché';
MLI18n::gi()->{'hitmeister_prepare_apply_form__field__webshopattribute__label'} = 'Attribut de la boutique en ligne';
MLI18n::gi()->{'hitmeister_prepare_apply_form__field__attributematching__matching__titlesrc'} = 'Valeur de la boutique';
MLI18n::gi()->{'hitmeister_prepare_apply_form__field__attributematching__matching__titledst'} = 'Valeur Hitmeister';
MLI18n::gi()->{'hitmeister_prepare_apply_form__field__title__label'} = 'Titre';
MLI18n::gi()->{'hitmeister_prepare_apply_form__field__subtitle__label'} = 'Sous-titre';
MLI18n::gi()->{'hitmeister_prepare_apply_form__field__description__label'} = 'Description';
MLI18n::gi()->{'hitmeister_prepare_apply_form__field__images__label'} = 'Images de l\'article';
MLI18n::gi()->{'hitmeister_prepare_apply_form__field__price__label'} = 'Prix';
MLI18n::gi()->{'hitmeister_prepare_apply_form__field__itemcondition__label'} = 'État de l\'article';
MLI18n::gi()->{'hitmeister_prepare_apply_form__field__shippingtime__label'} = 'Délais de livraison';
MLI18n::gi()->{'hitmeister_prepare_apply_form__field__itemcountry__label'} = 'L\'article est expédié depuis';
MLI18n::gi()->{'hitmeister_prepare_apply_form__field__comment__label'} = 'Informations sur l\'article';
MLI18n::gi()->{'hitmeister_prepare_variations__legend__variations'} = 'Veuillez sélectionner un groupe d\'attributs de Hitmeister.';
MLI18n::gi()->{'hitmeister_prepare_variations__legend__attributes'} = 'Veuillez sélectionner un nom d\'attribut de Hitmeister.';
MLI18n::gi()->{'hitmeister_prepare_variations__legend__variationmatching__0'} = 'Attribut Hitmeister';
MLI18n::gi()->{'hitmeister_prepare_variations__legend__variationmatching__1'} = 'Attribut de votre boutique';
MLI18n::gi()->{'hitmeister_prepare_variations__legend__variationmatchingoptional__0'} = 'hitmeister_prepare_variations__legend__variationmatchingoptional__0';
MLI18n::gi()->{'hitmeister_prepare_variations__legend__variationmatchingoptional__1'} = 'hitmeister_prepare_variations__legend__variationmatchingoptional__1';
MLI18n::gi()->{'hitmeister_prepare_variations__legend__action'} = '{#i18n:form_action_default_legend#}';
MLI18n::gi()->{'hitmeister_prepare_variations__field__variationgroups__label'} = 'Groupe d\'attributs';
MLI18n::gi()->{'hitmeister_prepare_variations__field__variationgroups.value__label'} = '1. Catégorie de la place de marché';
MLI18n::gi()->{'hitmeister_prepare_variations__field__deleteaction__label'} = '{#i18n:ML_BUTTON_LABEL_DELETE#}';
MLI18n::gi()->{'hitmeister_prepare_variations__field__groupschanged__label'} = '';
MLI18n::gi()->{'hitmeister_prepare_variations__field__attributename__label'} = 'Nom de l\'attribut';
MLI18n::gi()->{'hitmeister_prepare_variations__field__attributenameajax__label'} = '';
MLI18n::gi()->{'hitmeister_prepare_variations__field__customidentifier__label'} = 'Identificateur
';
MLI18n::gi()->{'hitmeister_prepare_variations__field__webshopattribute__label'} = 'Attribut de la boutique';
MLI18n::gi()->{'hitmeister_prepare_variations__field__saveaction__label'} = '{#i18n:ML_BUTTON_LABEL_SAVE_DATA#}';
MLI18n::gi()->{'hitmeister_prepare_variations__field__resetaction__label'} = '{#i18n:hitmeister_varmatch_reset_matching#}';
MLI18n::gi()->{'hitmeister_prepare_variations__field__resetaction__confirmtext'} = '{#i18n:hitmeister_prepare_variations_reset_info#}';
MLI18n::gi()->{'hitmeister_prepare_variations__field__attributematching__matching__titlesrc'} = 'Valeur de la boutique';
MLI18n::gi()->{'hitmeister_prepare_variations__field__attributematching__matching__titledst'} = 'Valeur Hitmeister';
MLI18n::gi()->{'hitmeister_prepareform_max_length_part1'} = 'Longueur maximale de';
MLI18n::gi()->{'hitmeister_prepareform_max_length_part2'} = 'L\'attribut est';
MLI18n::gi()->{'hitmeister_prepareform_category'} = 'Les attributs sont obligatoire pour cette catégorie.';
MLI18n::gi()->{'hitmeister_prepareform_title'} = 'Veuillez saisir un titre.';
MLI18n::gi()->{'hitmeister_prepareform_description'} = 'Veuillez saisir une description de l\'article.';
MLI18n::gi()->{'hitmeister_prepareform_category_attribute'} = 'Les attributs sont obligatoire pour cette catégorie.';
MLI18n::gi()->{'hitmeister_category_no_attributes'} = 'Il n\'y a pas d\'attributs pour cette catégorie.';
MLI18n::gi()->{'hitmeister_prepare_variations_title'} = 'Classement (matching) de déclinaisons';
MLI18n::gi()->{'hitmeister_prepare_variations_groups'} = 'Groupes Hitmeister';
MLI18n::gi()->{'hitmeister_prepare_variations_groups_custom'} = 'Groupes crées';
MLI18n::gi()->{'hitmeister_prepare_variations_groups_new'} = 'Créer un propre groupe';
MLI18n::gi()->{'hitmeister_prepare_match_variations_no_selection'} = 'Veuillez choisir un groupe de déclinaisons.';
MLI18n::gi()->{'hitmeister_prepare_match_variations_custom_ident_missing'} = 'Choisissez un identificateur.';
MLI18n::gi()->{'hitmeister_prepare_match_variations_attribute_missing'} = 'Veuillez choisir une désignation pour l\'attribut.';
MLI18n::gi()->{'hitmeister_prepare_match_variations_category_missing'} = 'hitmeister_prepare_match_variations_category_missing';
MLI18n::gi()->{'hitmeister_prepare_match_variations_not_all_matched'} = 'Veuillez attribuer un attribut de votre boutique à chaque attribut Hitmeister.';
MLI18n::gi()->{'hitmeister_prepare_match_notice_not_all_auto_matched'} = 'hitmeister_prepare_match_notice_not_all_auto_matched';
MLI18n::gi()->{'hitmeister_prepare_match_variations_saved'} = 'Sauvegardé avec succès.';
MLI18n::gi()->{'hitmeister_prepare_variations_saved'} = 'hitmeister_prepare_variations_saved';
MLI18n::gi()->{'hitmeister_prepare_variations_reset_success'} = 'L\'appariement à été suprimé.';
MLI18n::gi()->{'hitmeister_prepare_match_variations_delete'} = 'Voulez vous vraiment supprimer ce groupe? Tous les attributs classés correspondant seront également supprimés.';
MLI18n::gi()->{'hitmeister_error_checkin_variation_config_empty'} = 'Les déclinaisons ne sont pas configurées.';
MLI18n::gi()->{'hitmeister_error_checkin_variation_config_cannot_calc_variations'} = 'Aucune déclinaison d\'article n\'a été trouvée';
MLI18n::gi()->{'hitmeister_error_checkin_variation_config_missing_nameid'} = 'Aucune concordance n\'a été trouvé entre l\'attribut "{#Attribute#}" de votre boutique et le groupe de déclinaison Hitmeister "{#MpIdentifier#}" choisit pour l\'article à déclinaison ayant pour SKU "{#SKU#}".';
MLI18n::gi()->{'hitmeister_prepare_variations_free_text'} = 'Texte Libre';
MLI18n::gi()->{'hitmeister_prepare_variations_additional_category'} = 'Catégorie supplémentaire';
MLI18n::gi()->{'hitmeister_prepare_variations_error_text'} = 'L\'attribut est obligatoire.';
MLI18n::gi()->{'hitmeister_prepare_variations_error_missing_value'} = 'L\'attribut est obligatoire. Vous n\'avez pas indiqué de valeur pour l\'attribut choisit et classé depuis votre boutique.';
MLI18n::gi()->{'hitmeister_prepare_variations_error_free_text'} = ': Le champs de texte libre ne peut pas rester vide.';
MLI18n::gi()->{'hitmeister_prepare_variations_matching_table'} = 'Classés';
MLI18n::gi()->{'hitmeister_prepare_variations_manualy_matched'} = '- (Classé manuellement)';
MLI18n::gi()->{'hitmeister_prepare_variations_auto_matched'} = '- (Classé automatiquement)';
MLI18n::gi()->{'hitmeister_prepare_variations_free_text_add'} = '- (champs de texte libre)';
MLI18n::gi()->{'hitmeister_prepare_variations_reset_info'} = 'Voulez-vous vraiment annuler le classement?';
MLI18n::gi()->{'hitmeister_prepare_variations_change_attribute_info'} = 'Avant de pouvoir modifier les attributs vous devez annuler les classements précédents  ';
MLI18n::gi()->{'hitmeister_prepare_variations_additional_attribute_label'} = 'Attributs personnalisés';
MLI18n::gi()->{'hitmeister_prepare_variations_separator_line_label'} = 'hitmeister_prepare_variations_separator_line_label';
MLI18n::gi()->{'hitmeister_prepare_variations_mandatory_fields_info'} = '<b>Remarque :</b> <span class="bull">&bull;</span> Saisie obligatoire des champs marqués d\'une astérisque rouge.';
MLI18n::gi()->{'hitmeister_prepare_variations_already_matched'} = 'hitmeister_prepare_variations_already_matched';
MLI18n::gi()->{'hitmeister_prepare_variations_category_without_attributes_info'} = 'hitmeister_prepare_variations_category_without_attributes_info';
MLI18n::gi()->{'hitmeister_prepare_variations_choose_mp_value'} = 'hitmeister_prepare_variations_choose_mp_value';
MLI18n::gi()->{'hitmeister_prepare_variations_notice'} = 'hitmeister_prepare_variations_notice';
MLI18n::gi()->{'hitmeister_varmatch_attribute_changed_on_mp'} = 'hitmeister_varmatch_attribute_changed_on_mp';
MLI18n::gi()->{'hitmeister_varmatch_attribute_different_on_product'} = 'hitmeister_varmatch_attribute_different_on_product';
MLI18n::gi()->{'hitmeister_varmatch_attribute_deleted_from_mp'} = 'hitmeister_varmatch_attribute_deleted_from_mp';
MLI18n::gi()->{'hitmeister_varmatch_attribute_value_deleted_from_mp'} = 'hitmeister_varmatch_attribute_value_deleted_from_mp';
MLI18n::gi()->{'hitmeister_varmatch_define_name'} = 'hitmeister_varmatch_define_name';
MLI18n::gi()->{'hitmeister_varmatch_ajax_error'} = 'hitmeister_varmatch_ajax_error';
MLI18n::gi()->{'hitmeister_varmatch_all_select'} = 'hitmeister_varmatch_all_select';
MLI18n::gi()->{'hitmeister_varmatch_please_select'} = 'hitmeister_varmatch_please_select';
MLI18n::gi()->{'hitmeister_varmatch_auto_matchen'} = 'hitmeister_varmatch_auto_matchen';
MLI18n::gi()->{'hitmeister_varmatch_reset_matching'} = 'hitmeister_varmatch_reset_matching';
MLI18n::gi()->{'hitmeister_varmatch_delete_custom_title'} = 'hitmeister_varmatch_delete_custom_title';
MLI18n::gi()->{'hitmeister_varmatch_delete_custom_content'} = 'hitmeister_varmatch_delete_custom_content';
