
SoftwareChecker wurde erfolgreich gestartet... bitte kurz warten....


###########################################################
#
#                     SoftwareChecker
#
#
# Beschreibung:
# Der SoftwareChecker kann feststellen ob eine installierte Software veraltet ist.
# Derzeit koennen 143 Software-Module/Pakete geprueft werden.
# Software, welche verschluesselt ist oder die Versionsnummer in einer Datenbank speichert, kann nicht geprueft werden.
# Bei Fragen wenden Sie sich bitte an unsere Technik-Abteilung.
#
#
#
# Signatur:            116
# Such-Pfad:           /kunden/375433_12161/webseiten/happyfabric_08_2014/magento/
# Funde:               4
# Such-Module:         alle Module
# Module gesamt:       143 (Software-Produkte)
# Start-Zeit:          30.05.2017 10:07:00 +0200
#
#
# Alle Ergebnisse OHNE GEWAEHR!!!
#
###########################################################



FUND:         /kunden/375433_12161/webseiten/happyfabric_08_2014/magento/
Modul:        magento
AKTUELL:      1.9.2.4
FUND/ALT:     1.9.3.1 WARNING!!! OLD is higher than NEW!!!


FUND:         /kunden/375433_12161/webseiten/happyfabric_08_2014/magento/wordpress Sicherung vor update/
Modul:        wordpress
AKTUELL:      4.7.5
FUND/ALT:     3.9.5


FUND:         /kunden/375433_12161/webseiten/happyfabric_08_2014/magento/wordpress Sicherung vor update/wp-content/plugins/akismet/akismet.php
Modul:        wp-akismet
AKTUELL:      3.3.2
FUND/ALT:     3.0.1


FUND:         /kunden/375433_12161/webseiten/happyfabric_08_2014/magento/wordpress/wp-content/plugins/akismet/akismet.php
Modul:        wp-akismet
AKTUELL:      3.3.2
FUND/ALT:     3.3



###########################################################
##                   100%  FERTIG                        ##
###########################################################
