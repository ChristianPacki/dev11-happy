<?php
/*******************************************************************************************
*                                                                                          									*
*  definitions fuer websites for magento shop										*
*  Copyright (C) 2010 DoubleM-GmbH.de											*
*                                                                                          									*
*******************************************************************************************/
defined( 'VALID_DMC' ) or die( 'Direct Access to this location is not allowed.' );

// Websites-Preistabellen 
//define('WEBSITE_PRICE0','1');											// Website fuer Hauptpreis
//define('STORE_PRICE0','0');												// Store fuer Hauptpreis
//define('GROUP_PRICE0','all');											// Kundengruppe fuer diesen preis - 'all' fuer alle
define('WEBSITE_PRICE0',1);												// Website fuer Preis 1 - EINE STORE_VIEW, die zu einer Website gehoert 
// ZB zu Website 1 gehoeren views 1,5,7 dann zB 5 angeben, 1 und 7 werden automatisch mit gesetzt.
define('STORE_PRICE0',1);												// Store fuer Preis 1
define('GROUP_PRICE0','all');											// Kundengruppe fuer diesen preis - 'all' fuer alle
define('WEBSITE_PRICE1','10');											// Website fuer Preis 1
define('STORE_PRICE1',1);												// Store fuer Preis 1
define('GROUP_PRICE1','all');											// Kundengruppe fuer diesen preis - 'all' fuer alle
define('WEBSITE_PRICE2','11');
define('STORE_PRICE2',2);												// Store fuer Preis 2
define('GROUP_PRICE2','all');											// Kundengruppe fuer diesen preis - 'all' fuer alle
define('WEBSITE_PRICE3','9');
define('STORE_PRICE3',3);												// Store fuer Preis 3
define('GROUP_PRICE3','all');											// Kundengruppe fuer diesen preis - 'all' fuer alle
define('WEBSITE_PRICE4','7');
define('STORE_PRICE4',4);												// Store fuer Preis 4
define('GROUP_PRICE4','all');											// Kundengruppe fuer diesen preis - 'all' fuer alle
define('WEBSITE_PRICE5','');
define('GROUP_PRICE5','all');											// Kundengruppe fuer diesen preis - 'all' fuer alle

?>