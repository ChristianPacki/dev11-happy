<?php
/********************************************************************************************
*                                                                           				*
*  dmConnector Export Konfiguration Magento 												*
*  Copyright (C) 2014 DoubleM-GmbH.de														*
*  VERSION AB 08.2014																		*
*                                                                                          	*
*******************************************************************************************/
defined( 'VALID_DMC' ) or die( 'Direct Access to this location is not allowed.' );

// WICHTIGE EINSTELLUNGEN
// Bitte geben Sie hier den Order Status von den abzurufenden Bestellungen aus  Magento Stores ein
define('UPDATE_ORDER_STATUS',true);			// Order Status wahrend Bestellabruf �ndern ? true/false	
define('ORDER_STATUS_GET','processing');			// Abzurufende Orderstatus - mehrere durch @ getrennt, '' f�r alle	
define('ORDER_STATUS_SET','pending');		// Neuer Orderstatus nach Abruf		
define('NOTIFY_CUSTOMER',false);				// Kunde �ber ge�nderten Order Status nach Bestellabruf informieren ? true/false	


// Weitere Einstellungen BEI BEDARF
define('ORDER_SHOP_IDS','');					// Abzurufende Magento Store - mehrere durch @ getrennt	, '' f�r alle	
define('USE_ORDER_ID',false);					// Datei order_id.txt mit aktuellem Abrufdatum beschreiben?
define('FIRST_ORDER_ID','');					// Erste Order ID bei Bedarf
// Versand etc
define('SHIPPING_AS_PRODUCT',true);				// Versandkosten als Artikel? 
define('SHIPPING_DISCOUNTED',false);			// Rabatte auch auf Versandkosten  
define('WAWI_NAME','NAV');						// Export zu Warenwirtschaftssystem / 'europa3000' oder andere
// Export Rechnungen an Stelle von Bestellungen
define('EXPORT_INVOICES',false);				// Rechnungen an Stelle von Bestellungen exportieren? true/false	
// Unterstuetzung von Oderstatus nach Eintrag in ERP - in der Regel NICHT verwenden
define('UPDATE_ORDER_STATUS_ERP',false);		// Order Status nach Bestellabruf �ndern ? true/false	
define('NEW_ORDER_STATUS_ERP','processing');	//  Erfolgreich on der WaWi eingelesen
define('NEW_ORDER_STATUS_FAILED','pending');	// Nicht oder fehlerhaft  on der WaWi eingelesen
// Copie der Bestellungen per eMail als Status
define('ORDER_COPY_EMAIL',false);   
define('ORDER_COPY_EMAIL_FROM','info@mobilize.de');   
define('ORDER_COPY_EMAIL_TO','info@mobilize.de');   
define('ORDER_BACKUP_FILE','./order_backup/bestellung_');   
// SONDERFUNKTIONEN - In der Regel �berfl�ssig
define('COPY_ORDER_IN_DATEBASE',false);
define('DB_SERVER2','');
define('DATABASE2','');
define('DB_USER2','root');
define('DB_PWD2','');

// Export LOG Datei
define('LOG_FILE_EXPORT','./logs/dmc_log_export.txt'); 

// Produktexport und Kategorieexport
// Maximale Kategorieebenen zum Export
define('MAX_CATEGORY_LEVEL',4);
// ANZAHL maximal zu exportierender Kategorien
define('MAX_CATEGORIES_EXPORT',1000);
// ANZAHL maximal zu exportierender Produkte
define('MAX_PRODUCTS_EXPORT',1000);
define('FIRST_PRODUCTS_EXPORT',1000);	// Erstes anzurufendes Produkt
define('STD_WAWI_CAT_ID',999);	//Standard Kategorie der Warenwirtschaft, wenn Artikel in Magento nicht zugeordnet
// WaWi Bezeichnung Kategorie oberste Ebene , z.B. 0 oder EMPTY fuer Office Line
define('MAIN_ERP_CATEGORY','EMPTY');
// Kategoriebeschreibungen exportieren
define('EXPORT_CATEGORY_DESC',false);

?>