<?php
/*******************************************************************************************
*                                                                                          	*
*  dmConnector Artikeltext generator für WaWi												*
*  generate_wawi_textr.php																	*
*  Copyright (C) 2014 DoubleM-GmbH.de														*
*                                                                                          	*
*******************************************************************************************/
  
    define('VALID_DMC',true);	
	include ('./conf/definitions.inc.php');
	include ('dmc_db_functions.php');
	if (is_file('userfunctions/products/dmc_art_functions.php')) include ('functions/products/dmc_art_functions.php');
	else include ('functions/products/dmc_art_functions.php');
	
    // Open DB
	$link=dmc_db_connect();
	$dateiname="./logs/do_selectline_sql.sql";	
	$dateihandle = fopen($dateiname,"w");
	  
	$store_view = 2; // 1 english, 2 deutsch, 4 - french
	$artikel_art="configurable";
	// Produkte Ohne Bilder ermitteln
	// SELECT * FROM `catalog_product_flat_1` where type_id='configurable' AND  (`small_image` is null or `small_image` like 'no%') ORDER BY `sku` ASC
	$query	= "SELECT sku, name, description, short_description FROM `catalog_product_flat_".$store_view."` where type_id='".$artikel_art."' ORDER BY `sku` ASC";	
	
	$result = mysql_query($query) or die(mysql_error());

	// Alle ermittelten Artikelnummern durchlaufen
	while($row = mysql_fetch_array($result)){
		$Artikel_Artikelnr = str_replace("'","´",$row['sku']);
		$Artikel_Bezeichnung= str_replace("'","´",$row['name']);
		$Artikel_Kurztext= str_replace("'","´",$row['short_description']);
		// $Artikel_Kurztext= str_replace("\r\n","",$row['short_description']);	// doppelte Zeilenumbrueche entefernen
		$Artikel_Text= str_replace("'","´",$row['description']);
		//$Artikel_Text= str_replace("\r\n","´",$row['description']); // doppelte Zeilenumbrueche entefernen
		
		 
 		//  evtl delete first, weitere Spalten [ARTBEZ_ID] ... ,[TS],[HTMLLangtext],[HTMLBestelltext]
		$query	=	"INSERT INTO [SL_MWAWI].[dbo].[ARTBEZ] ";
		$query	.=	"([Artikelnummer],[Sprache],[Bezeichnung],[Zusatz],[Langtext],[Bestelltext]) ";
		$query	.=	"VALUES ('".$Artikel_Artikelnr."', 'shop', '".$Artikel_Bezeichnung."', '', '".$Artikel_Text."', '".$Artikel_Kurztext."');";
		$query	.= "\n";
 
		fwrite($dateihandle, $query);
		echo  $query;
			
	} // end while

	echo "Vielen Dank fuer die Geduld";
	// close db
	dmc_db_disconnect($link);		


?>