#ATTENTION!
#
#DO NOT MODIFY THIS FILE BECAUSE IT WAS GENERATED AUTOMATICALLY,
#SO ALL YOUR CHANGES WILL BE LOST THE NEXT TIME THE FILE IS GENERATED.
// $Id: named.conf,v 1.1.1.1 2001/10/15 07:44:36 kap Exp $
//
// Refer to the named(8) man page for details.  If you are ever going
// to setup a primary server, make sure you've understood the hairy
// details of how DNS is working.  Even with simple mistakes, you can
// break connectivity for affected parties, or cause huge amount of
// useless Internet traffic.

options {
	allow-recursion {
		localnets;
	};
listen-on-v6 { any; };
	version "none";
	directory "/var";
	auth-nxdomain no;
	pid-file "/var/run/named/named.pid";

// In addition to the "forwarders" clause, you can force your name
// server to never initiate queries of its own, but always ask its
// forwarders only, by enabling the following line:
//
//      forward only;

// If you've got a DNS server around at your upstream provider, enter
// its IP address here, and enable the line below.  This will make you
// benefit from its cache, thus reduce overall DNS traffic in the Internet.
/*
	forwarders {
		127.0.0.1;
	};
*/
	/*
	 * If there is a firewall between you and nameservers you want
	 * to talk to, you might need to uncomment the query-source
	 * directive below.  Previous versions of BIND always asked
	 * questions using port 53, but BIND 8.1 uses an unprivileged
	 * port by default.
	 */
	// query-source address * port 53;

	/*
	 * If running in a sandbox, you may have to specify a different
	 * location for the dumpfile.
	 */
	// dump-file "s/named_dump.db";
};

//Use with the following in named.conf, adjusting the allow list as needed:

key "rndc-key" {
	algorithm hmac-md5;
	secret "CeMgS23y0oWE20nyv0x40Q==";
};

controls {
	inet 127.0.0.1 port 953
	allow { 127.0.0.1; } keys { "rndc-key"; };
};

// Note: the following will be supported in a future release.
/*
host { any; } {
	topology {
		127.0.0.0/8;
	};
};
*/

// Setting up secondaries is way easier and the rough picture for this
// is explained below.
//
// If you enable a local name server, don't forget to enter 127.0.0.1
// into your /etc/resolv.conf so this server will be queried first.
// Also, make sure to enable it in /etc/rc.conf.

zone "." {
	type hint;
	file "named.root";
};

zone "0.0.127.IN-ADDR.ARPA" {
	type master;
	file "localhost.rev";
};

// NB: Do not use the IP addresses below, they are faked, and only
// serve demonstration/documentation purposes!
//
// Example secondary config entries.  It can be convenient to become
// a secondary at least for the zone where your own domain is in.  Ask
// your network administrator for the IP address of the responsible
// primary.
//
// Never forget to include the reverse lookup (IN-ADDR.ARPA) zone!
// (This is the first bytes of the respective IP address, in reverse
// order, with ".IN-ADDR.ARPA" appended.)
//
// Before starting to setup a primary zone, better make sure you fully
// understand how DNS and BIND works, however.  There are sometimes
// unobvious pitfalls.  Setting up a secondary is comparably simpler.
//
// NB: Don't blindly enable the examples below. :-)  Use actual names
// and addresses instead.
//
// NOTE!!! FreeBSD runs bind in a sandbox (see named_flags in rc.conf).
// The directory containing the secondary zones must be write accessible 
// to bind.  The following sequence is suggested:
//
//	mkdir /etc/namedb/s
//	chown bind.bind /etc/namedb/s
//	chmod 750 /etc/namedb/s

/*
zone "domain.com" {
	type slave;
	file "s/domain.com.bak";
	masters {
		192.168.1.1;
	};
};

zone "0.168.192.in-addr.arpa" {
	type slave;
	file "s/0.168.192.in-addr.arpa.bak";
	masters {
		192.168.1.1;
	};
};
*/

zone "admin.mobilize.de" {
	type master;
	file "admin.mobilize.de";
	allow-transfer {
		85.214.106.99;
		common-allow-transfer;
	};
};
zone "gipsbinden.com" {
	type master;
	file "gipsbinden.com";
	allow-transfer {
		85.214.106.99;
		common-allow-transfer;
	};
};
zone "haustechnikhandel24.de" {
	type master;
	file "haustechnikhandel24.de";
	allow-transfer {
		85.214.106.99;
		common-allow-transfer;
	};
};
zone "dmconnector.de" {
	type master;
	file "dmconnector.de";
	allow-transfer {
		85.214.106.99;
		common-allow-transfer;
	};
};
zone "dmconnector.com" {
	type master;
	file "dmconnector.com";
	allow-transfer {
		85.214.106.99;
		common-allow-transfer;
	};
};
zone "fantastisch.info" {
	type master;
	file "fantastisch.info";
	allow-transfer {
		85.214.106.99;
		common-allow-transfer;
	};
};
zone "106.214.85.in-addr.arpa" {
	type master;
	file "106.214.85.in-addr.arpa";
	allow-transfer {
		common-allow-transfer;
	};
};
acl common-allow-transfer {
	none;
};
