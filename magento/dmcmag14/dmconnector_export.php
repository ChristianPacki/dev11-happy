﻿<?php
/*******************************************************************************************
*                                                      										*
*  dm.connector export magento shop	in andere Datenbank										*
*  Copyright (C) 2013 DoubleM-GmbH.de														*
*                                                                                          	*
*	12.12.13 - Abfrage orderlist von API auf DB umgestellt									*
*	06.10.14 - Von API auf DB umgestellt													*
*******************************************************************************************/

	ini_set("display_errors", 1);

	#error_reporting(E_ERROR);
	error_reporting(E_ALL);
	
	if (is_file('../app/Mage.php')) require_once ('../app/Mage.php');
	else if (is_file('../../app/Mage.php')) require_once ('../../app/Mage.php');
	else if (is_file('../../../app/Mage.php')) require_once ('../../../app/Mage.php');
	else { echo "Mage.php nicht gefunden, Abbruch"; exit; }

	umask(0);
	Mage::app();
		  
	define('VALID_DMC',true);									// zugriff auf includes
	
	if (!defined(INVOICED_ORDERS))
				define('INVOICED_ORDERS',false);				// Nur in Rechnung gestellte Bestellungen abrufen
		
	if (!defined(SPECIAL_VERSION))
		$SPECIAL_VERSION='';
	else 
		$SPECIAL_VERSION=SPECIAL_VERSION;							// Special Version zb 'loewe';
	
	
	
	include ('./conf/definitions.inc.php');
	include ('definitions_export.inc.php');

	// include needed functions
	include('functions/products/dmc_art_functions.php');     
	include('dmc_db_functions.php');     
	include('dmc_functions.php');     
	
	if (DEBUGGER>=1)
			{
				date_default_timezone_set('Europe/Berlin');
				$daten = "\n***********************************************************************\n";
				$daten .= "******************* dmconnector export ".date("YmdHis")." ****\n";
				$daten .= "************************************************************************\n";
				if (LOG_ROTATION=='size' && is_numeric(LOG_ROTATION_VALUE))
					if ((filesize(str_replace(".txt", "_export.txt", LOG_FILE))/1048576)>LOG_ROTATION_VALUE) 
						$dateihandle = fopen(str_replace(".txt", "_export.txt", LOG_FILE),"w"); // LOG File erstellen
					else
						$dateihandle = fopen(str_replace(".txt", "_export.txt", LOG_FILE),"a");
				else
						$dateihandle = fopen(str_replace(".txt", "_export.txt", LOG_FILE),"a");
				fwrite($dateihandle, $daten);				
			}
						

	// check permissions for XML-Access
	// $user=$_GET['user'];
	//$password=$_GET['password'];
	//$ExportModus = $_POST['ExportModus'];
	//$Artikel_ID = (integer)($_POST['Artikel_ID']);
	// user authentification 
	$action = isset($_POST['action']) ? $_POST['action'] : $_GET['action'];
	$user = isset($_POST['user']) ?  $_POST['user'] : $_GET['user'];
	$password = isset($_POST['password']) ?  $_POST['password'] : $_GET['password'];
	// Kein Update des Bestelldatums
	if (isset($_POST['noupdate_order_date'])) {
		$noupdate_order_date = $_POST['noupdate_order_date'];
	} else if (isset($_GET['noupdate_order_date'])) { 
		$noupdate_order_date = $_GET['noupdate_order_date'];
	} else {
		$noupdate_order_date = -1;
	}
	
	// Überprüfen, ob nur bestimmte Bestellungen abzurufen sind
	if (isset($_POST['orders_from'])) {
		$orders_from = $_POST['orders_from'];
	} else if (isset($_GET['orders_from'])) { 
		$orders_from = $_GET['orders_from'];
	} else {
		$orders_from = -1;
	}
	if (isset($_POST['orders_to'])) {
		$orders_to = $_POST['orders_to'];
	} else if (isset($_GET['orders_to'])) { 
		$orders_to = $_GET['orders_to'];
	} else {
		$orders_to = -1;
	}
	
	
	$noupdate=false;
	
	if (isset($orders_from) && $orders_from!=-1) {
		// kein Zeitliches Update der abgerufenen Bestellungen durchführen
		$noupdate_order_date = 1;
		$noupdate=true;
	}
	// Überprüfen, ob nur eine bestimmte Anzahl von Bestellungen zu importieren ist,
	if (isset($_POST['noOfOrder'])) {
		$noOfOrder = $_POST['noOfOrder'];
	} else if (isset($_GET['noOfOrder'])) { 
		$noOfOrder = $_GET['noOfOrder'];
	} else {
		$noOfOrder = 0;
	}
	
	if (substr($password,0,2)=='%%') {
	 $password=md5(substr($password,2,40));
	}
	if ($user!='' and $password!='') {
	}
	if ($password=='dmconnector123') $password="";
	// soap authentification
	$zugriff=true;
    try {		 
		// Get Soap Connection
	//	    $client = new SoapClient(SOAP_CLIENT);
		    	//  api authentification, ->  get session token   
	//		$session = $client->login($user, $password);	
		$client="";
		$session=0;
		if (DEBUGGER>=1) fwrite($dateihandle,"api authentification, ->  get session token :".$session);			
	} catch (SoapFault $e) {
			// Fehlerabfangroutine, wenn Session zugeteilt aber Access Denied
			// if ($debugger==1) fwrite($dateihandle,"Access denied");
			$sessionID=dmc_get_session_id();
			if ($sessionID<>0) {
				if (DEBUGGER>=1) fwrite($dateihandle,"api authentification failed ->  get session token over dmc_get_session_id\n");
				$zugriff=true;
			} else {
				$session=0;	
				$zugriff=false;
				if (DEBUGGER>=1) fwrite($dateihandle, "user authentification Access denied for ".$user."/".$password." Error=:\n ".$e." \n");
			}
	}
	
	
	if (DEBUGGER>=1) fwrite($dateihandle,"action: ".$action." zugriff ".$zugriff.".\n");
	
	// Überprüfen, welcher Export Modus und ggfls andere Datei aufrufen
	
	if ($action == 'products_export' && $zugriff) {
	    // Artikel exportieren
		include('dmc_export_products.php');     
		dmc_export_products($client, $session);
		//		echo "NewId=".$NewId."\n";
	} elseif ($action == 'categories_export' && $zugriff) {
	    // Kategorien exportieren
		include('dmc_export_categories.php');     
		dmc_export_categories($client, $session);
		//		echo "NewId=".$NewId."\n";
	} elseif ($action == 'orders_export' && $zugriff) {
		if (DEBUGGER>=1) fwrite($dateihandle,"orders_export\n");
		// Letzte Abgerufene Bestellung ermitteln
		$dateihandleOrderID = fopen("./order_id.txt","r");
		$last_order = fread($dateihandleOrderID, 20);
		fclose($dateihandleOrderID);
		
		// Ermitteln der Anzahl der vorliegenden Bestellungen
		// $BestellAnzahl = checkOrders($session, $client);	
		$BestellAnzahl = checkOrdersDB();	
		if (DEBUGGER>=1) fwrite($dateihandle,"Anzahl Bestellungen: ".$BestellAnzahl." und abzurufende Bestellungen laut Anfrage:".$noOfOrder.".\n");
		
		// XML Schema 
	    $schema = '<?xml version="1.0" encoding="UTF-8"?>' . "\n" ;
	      //          '<ORDER_LIST>' . "\n";
		
		// $schema .= 	'<ORDER_ANZAHL>' . sizeof($order_list).'</ORDER_ANZAHL>';
		$schema .= 	'<ORDER_ANZAHL>' . $BestellAnzahl.'</ORDER_ANZAHL>';
		
		// Abbrechen, wenn keine Bestellungen vorhanden.
		if ($BestellAnzahl==0) {
		//	$schema .= 	'</ORDER_LIST>';
			echo $schema;
			exit;	
		}
		
		$from_incr_id = 0;
		$last_order_abgerufen = $last_order;
		// Schrittfolge ermitteln
		// Weniger als 10 Bestellungen
		if ($BestellAnzahl <= 10) {
			$AnzahlAbruf = 1;
		} else {
			// Abgerundeter Wert+1
			$AnzahlAbruf = floor($BestellAnzahl/10) + 1;
		}
		$durchlaufendeBestellungen = 0;
		
		// BEI DB Abfragen in einem Rutsch
		$AnzahlAbruf=1;
		//  Abschnitt Bestellungen in 10er Schritten 
		for ($rcm=0;$rcm<$AnzahlAbruf;$rcm++){
		
			$order_list = array();
			$schema2="";
			// Open DB, if not opened
			$link=dmc_db_connect();
			// SQL fuer Abrufen der Orders ermitteln aus dmc_functions.php
			
			// evtl nur in Rechnung gestellte Bestellungen?
			if (INVOICED_ORDERS==false) {
				$query = "SELECT *, o.entity_id AS bestell_id, '' AS invoice_no FROM ".DB_TABLE_PREFIX."sales_flat_order AS o INNER JOIN sales_flat_order_payment AS op ON o.entity_id=op.parent_id WHERE ". getOrdersSQLWhere();		
			} else {
				$query = "SELECT *, o.entity_id AS bestell_id, (SELECT increment_id FROM ".DB_TABLE_PREFIX."sales_flat_invoice WHERE order_id=o.entity_id) AS invoice_no FROM ".DB_TABLE_PREFIX."sales_flat_order AS o INNER JOIN sales_flat_order_payment AS op ON o.entity_id=op.parent_id WHERE o.total_invoiced IS NOT NULL AND ". getOrdersSQLWhere();
			}			
			if (DEBUGGER>=1) fwrite($dateihandle,"ORDER SQL = $query \n");
			$sql_query = mysqli_query($link,$query);		
			$i = 0;		
			while ($wert = mysqli_fetch_assoc($sql_query)) {
				// Open DB, if not opened
			$link=dmc_db_connect();
		
				// Zugehoerige Adressinformatinen ermitteln
				// Billing  
				// entity_id mehrfach vorhanden, daher neu zuweisen.
				$wert['entity_id']=$wert['bestell_id'];
				if (DEBUGGER>=1) fwrite($dateihandle,"ORDER ID = ".$wert['entity_id']." \n");
				$query = "SELECT * FROM  ".DB_TABLE_PREFIX."sales_flat_order_address WHERE parent_id = '".$wert['entity_id']."' AND address_type='billing' LIMIT 1";
				if (DEBUGGER>=1) fwrite($dateihandle, "dmc_sql_select_value-SQL= ".$query." ");
				// Retrieve all the data from the "example" table
				$result=mysqli_query($link,$query);
				$billing_address_wert = mysqli_fetch_assoc($result);
				// Shipping  
				$query = "SELECT * FROM  ".DB_TABLE_PREFIX."sales_flat_order_address WHERE parent_id = '".$wert['entity_id']."' AND address_type='shipping' LIMIT 1";
				if (DEBUGGER>=1) fwrite($dateihandle, "dmc_sql_select_value-SQL= ".$query." ");
				$result=mysqli_query($link,$query);
				$shipping_address_wert = mysqli_fetch_assoc($result);
					
				// Wichtige Werte in Array $order_list
				// entity_id
				$order_list[$i]['increment_id']=$wert['increment_id'];	
				$order_list[$i]['invoice_no']=$wert['invoice_no'];			 // Rechnungsnummer, falls vorhanden
				$order_list[$i]['store_id']=$wert['store_id'];
				$order_list[$i]['customer_group_id']=$wert['customer_group_id'];	 
				$order_list[$i]['created_at']=$wert['created_at'];
				$order_list[$i]['total_paid']=$wert['base_total_paid'];  
				 
				// Benoetigte Variablen
				// Allgemein
				// $order_list[$i][increment_id] =$wert['increment_id'];	
				$order_infos['subtotal']=$wert['subtotal'];	
				$order_infos['tax_amount']=$wert['tax_amount'];	
				$order_infos['discount_amount']=$wert['discount_amount'];	
				$order_infos['base_subtotal_incl_tax'] = $wert['base_subtotal_incl_tax'];	
				$order_infos['base_discount_amount'] =$wert['base_discount_amount'];	 
				$order_infos['order_currency_code'] =$wert['order_currency_code'];	
				$order_infos['weight'] = $wert['weight'];	
				$order_infos['created_at']=$wert['created_at'];	
				$order_infos['order_id']=$wert['entity_id'];			
				$order_infos['remote_ip']=$wert['remote_ip'];	
				$order_infos['updated_at']=$wert['updated_at'];	
				$order_infos['status']="";	
				// zZt nicht verwendet
				$order_infos['quote_id']=$wert['quote_id'];	
				$order_infos['increment_id']=$wert['increment_id'];
				$order_infos['store_name']=$wert['store_name'];	
				$order_infos['subtotal_incl_tax'] = $wert['subtotal_incl_tax'];	
				$order_infos['grand_total'] = $wert['grand_total'];	
				$order_infos['base_grand_total'] = $wert['base_grand_total'];	
				$order_infos['base_subtotal']=$wert['base_subtotal'];	
				$order_infos['base_tax_amount']=$wert['base_tax_amount'];	
				$order_infos['total_item_count']=$wert['total_item_count'];	
				$order_infos['total_qty_ordered']=$wert['total_qty_ordered'];	
				$order_infos['base_shipping_discount_amount']=$wert['base_shipping_discount_amount'];	
								
				// Versand
				$order_infos['shipping_method'] =$wert['shipping_method'];	
				$order_infos['shipping_amount'] = $wert['shipping_amount'];	
				$order_infos['shipping_tax_amount'] = $wert['shipping_tax_amount'];	
				$order_infos['shipping_discount_amount'] = $wert['shipping_discount_amount'];	
				$order_infos['cod_fee']=isset($wert['cod_fee']) ? $wert['cod_fee'] : "";	
				$order_infos['cod_tax_amount']=isset($wert['cod_fee']) ? $wert['cod_tax_amount'] : "";	
				// zZt nicht verwendet
				$order_infos['shipping_description'] =$wert['shipping_description'];
				$order_infos['shipping_incl_tax'] =$wert['shipping_incl_tax'];
				$order_infos['base_shipping_incl_tax'] =$wert['base_shipping_incl_tax'];
				$order_infos['paypal_ipn_customer_notified'] =$wert['paypal_ipn_customer_notified'];
				$order_infos['gift_message_id'] =$wert['gift_message_id'];
								  
				// Zahlung
				$order_infos['payment']['po_number']=$wert['po_number'];
				$order_infos['payment']['cc_number_enc']=$wert['cc_number_enc'];
				$order_infos['payment']['cc_type']=$wert['cc_type'];
				$order_infos['payment']['cc_exp_month']=$wert['cc_exp_month'];
				$order_infos['payment']['cc_exp_year']=$wert['cc_exp_year'];
				$order_infos['payment']['cc_owner']=$wert['cc_owner'];
				$order_infos['payment']['method'] =$wert['method'];
				$order_infos['payment']['last_trans_id']=$wert['last_trans_id'];	
				
				// Discount etc
				$order_infos['coupon_code'] =$wert['coupon_code'];	
				// zZt nicht verwendet
				$order_infos['discount_description'] =$wert['discount_description'];	
				
				// Kommentare nestepcheckout
				$order_infos['onestepcheckout_customercomment'] = "";
				// zZt nicht verwendet
				$order_infos['customer_note'] =	$wert['customer_note'];

				// Rechnung
				$invoice_infos['items'] = array();
													
				// Kunde
				$order_infos['customer_id']=$wert['customer_id'];	
				$order_infos['customer_email'] =$wert['customer_email'];	
				// zZt nicht verwendet
				$order_infos['customer_is_guest'] =$wert['customer_is_guest'];	
				$order_infos['customer_dob'] =$wert['customer_dob'];	
				$order_infos['customer_gender'] =$wert['customer_gender'];	
				
				// Kundenadresse (zur Zeit nicht verwendet)
				$order_infos['customer_address']['address_id'] ="";	
				$order_infos['customer_address']['street'] ="";	
				$order_infos['customer_address']['prefix'] =$wert['customer_prefix'];	
				$order_infos['customer_address']['title'] =$wert['customer_suffix'];	
				$order_infos['customer_address']['gender'] ="";	
				$order_infos['customer_address']['firstname'] =$wert['customer_firstname'];	
				$order_infos['customer_address']['customer_middlename'] =$wert['customer_middlename'];	
				$order_infos['customer_address']['lastname'] =$wert['customer_lastname'];	
				$order_infos['customer_address']['company'] ="";	
				$order_infos['customer_address']['postcode'] ="";	
				$order_infos['customer_address']['city'] ="";	
				$order_infos['customer_address']['country_id'] ="";	
				$order_infos['customer_address']['telephone'] ="";	
				$order_infos['customer_address']['fax'] ="";	
				$order_infos['customer_email'] = $wert['customer_email'];	
				$order_infos['customer_taxvat'] = $wert['customer_taxvat'];
						 
				// Versandadresse ( aus sales_flat_order_address -> auf parent_id=sales_flat_order.entity_id ? und address_type='billing' )
				$order_infos['billing_address']['address_id'] =$wert['billing_address_id'];
				$order_infos['billing_address']['prefix'] =$billing_address_wert['prefix'];	
				$order_infos['billing_address']['title'] =$billing_address_wert['suffix'];	
				$order_infos['billing_address']['gender'] = '';	
				$order_infos['billing_address']['firstname'] =$billing_address_wert['firstname'];	
				$order_infos['billing_address']['lastname'] =$billing_address_wert['lastname'];	
				$order_infos['billing_address']['company'] =$billing_address_wert['company'];	
				$order_infos['billing_address']['street'] =$billing_address_wert['street'];	
				$order_infos['billing_address']['postcode'] =$billing_address_wert['postcode'];	
				$order_infos['billing_address']['city'] =$billing_address_wert['city'];	
				$order_infos['billing_address']['country_id'] =$billing_address_wert['country_id'];	
				$order_infos['billing_address']['telephone'] =$billing_address_wert['telephone'];	
				$order_infos['billing_address']['fax'] =$billing_address_wert['fax'];
				// zZt nicht aktiv
				$order_infos['billing_address']['region'] =$billing_address_wert['region'];
				$order_infos['billing_address']['email'] =$billing_address_wert['email'];
				$order_infos['billing_address']['middlename'] =$billing_address_wert['middlename'];
				$order_infos['billing_address']['vat_id'] =$billing_address_wert['vat_id'];
				$order_infos['billing_address']['vat_is_valid'] =$billing_address_wert['vat_is_valid'];
				
				// Rechnungsadresse ( aus sales_flat_order_address -> auf parent_id=sales_flat_order.entity_id ? und address_type='shipping' )
				$order_infos['shipping_address']['address_id'] =$wert['shipping_address_id'];	
				$order_infos['shipping_address']['prefix'] =$shipping_address_wert['prefix'];	
				$order_infos['shipping_address']['title'] =$shipping_address_wert['suffix'];	
				$order_infos['shipping_address']['gender'] = '';	
				$order_infos['shipping_address']['firstname'] =$shipping_address_wert['firstname'];	
				$order_infos['shipping_address']['lastname'] =$shipping_address_wert['lastname'];	
				$order_infos['shipping_address']['company'] =$shipping_address_wert['company'];	
				$order_infos['shipping_address']['street'] =$shipping_address_wert['street'];	
				$order_infos['shipping_address']['postcode'] =$shipping_address_wert['postcode'];	
				$order_infos['shipping_address']['city'] =$shipping_address_wert['city'];	
				$order_infos['shipping_address']['country_id'] =$shipping_address_wert['country_id'];	
				$order_infos['shipping_address']['telephone'] =$shipping_address_wert['telephone'];	
				$order_infos['shipping_address']['fax'] =$shipping_address_wert['fax'];
				// zZt nicht aktiv
				$order_infos['shipping_address']['region'] =$shipping_address_wert['region'];
				$order_infos['shipping_address']['email'] =$shipping_address_wert['email'];
				$order_infos['shipping_address']['middlename'] =$shipping_address_wert['middlename'];
				$order_infos['shipping_address']['vat_id'] =$shipping_address_wert['vat_id'];
				$order_infos['shipping_address']['vat_is_valid'] =$shipping_address_wert['vat_is_valid'];
				
				// Spezielle Berücksichtigung für Kundenversionen
				if ($SPECIAL_VERSION=='loewe') $order_infos['lager']=$wert['inventory_mode'];
											 
				// Artikel - typ <> configurable, grouped und bundle
				$order_infos['items'] = array();
				$query = "SELECT * FROM  ".DB_TABLE_PREFIX."sales_flat_order_item WHERE order_id = '".$wert['entity_id']."' ";
				$query .= "AND product_type!='configurable' AND product_type!='grouped' AND product_type!='bundle' ";
				if (DEBUGGER>=1) fwrite($dateihandle, "\n Produkte-SQL1 = ".$query." \n");
			//	echo "dmc_sql_select_value-SQL= ".$query;
				$sql_product_query = mysqli_query($link,$query);		
				$product_no = 0;		
				if (DEBUGGER>=1) fwrite($dateihandle,"379\n");
				while ($product_wert = mysqli_fetch_assoc($sql_product_query)) {
					if (DEBUGGER>=1) fwrite($dateihandle,"382\n");
					$order_infos['items'][$product_no]['item_id'] = $product_wert['product_id'];
					$order_infos['items'][$product_no]['product_id'] = $product_wert['product_id'];						
					$order_infos['items'][$product_no]['sku']= $product_wert['sku'];
					$order_infos['items'][$product_no]['name']=$product_wert['name'];
					$order_infos['items'][$product_no]['desciption']=$product_wert['desciption'];
					// Bei Variantenunterprodukt den Preis des Conf für die Variante ermitteln
					
					/*fwrite($dateihandle, "\n parentid=".$product_wert['parent_item_id'].".\n");
					if ($product_wert['parent_item_id']!="")
					fwrite($dateihandle, "\n 1.\n");
					if ($product_wert['parent_item_id']!=null)
					fwrite($dateihandle, "\n 2.\n");
					if (!is_null($product_wert['parent_item_id']))
					fwrite($dateihandle, "\n 3.\n");
					*/
					if ($product_wert['parent_item_id']!=""
						&& $product_wert['parent_item_id']!=null 
						&& !is_null($product_wert['parent_item_id'])) {
						// Variante -> Werte des conf ermitteln
						$query = "SELECT * FROM ".DB_TABLE_PREFIX."sales_flat_order_item WHERE order_id = '".$wert['entity_id']."' AND item_id=".$product_wert['parent_item_id'];
						if (DEBUGGER>=1)  fwrite($dateihandle, "Variantenartikel.\n");
						if (DEBUGGER>=1)  fwrite($dateihandle, "Produkte-SQL2= ".$query." .\n");
						$result = mysql_query ( $query );
						$parent_product_wert = mysqli_fetch_assoc($result);				
						$order_infos['items'][$product_no]['base_price'] = $parent_product_wert['base_price'];
						$order_infos['items'][$product_no]['row_total'] = $parent_product_wert['row_total'];
						$order_infos['items'][$product_no]['base_price_incl_tax']=$parent_product_wert['base_price_incl_tax'];
						$order_infos['items'][$product_no]['row_total_incl_tax']=$parent_product_wert['row_total_incl_tax'];
						$order_infos['items'][$product_no]['tax_amount']=$parent_product_wert['tax_amount'];
						$order_infos['items'][$product_no]['tax_percent']=$parent_product_wert['tax_percent'];
						$order_infos['items'][$product_no]['discount_amount']=$parent_product_wert['discount_amount'];
						$order_infos['items'][$product_no]['discount_percent']=$parent_product_wert['discount_percent'];	
					} else {
						// Standardartikel
						if (DEBUGGER>=1)  fwrite($dateihandle, "Standardartikel.\n");
						$order_infos['items'][$product_no]['base_price'] = $product_wert['base_price'];
						$order_infos['items'][$product_no]['row_total'] = $product_wert['row_total'];
						$order_infos['items'][$product_no]['base_price_incl_tax']=$product_wert['base_price_incl_tax'];
						$order_infos['items'][$product_no]['row_total_incl_tax']=$product_wert['row_total_incl_tax'];
						$order_infos['items'][$product_no]['tax_amount']=$product_wert['tax_amount'];
						$order_infos['items'][$product_no]['tax_percent']=$product_wert['tax_percent'];
						$order_infos['items'][$product_no]['discount_amount']=$product_wert['discount_amount'];
						$order_infos['items'][$product_no]['discount_percent']=$product_wert['discount_percent'];							
					}
					$order_infos['items'][$product_no]['qty_ordered']=$product_wert['qty_ordered'];
					$order_infos['items'][$product_no]['weight']=$product_wert['weight'];
					$order_infos['items'][$product_no]['product_type']=$product_wert['product_type'];
					$order_infos['items'][$product_no]['product_options']=$product_wert['product_options'];
					// zZt nicht verwendet
					$order_infos['items'][$product_no]['order_item_id'] =$product_wert['item_id'];
					$order_infos['items'][$product_no]['is_virtual'] =$product_wert['is_virtual'];
					$order_infos['items'][$product_no]['description'] =$product_wert['description'];
					$order_infos['items'][$product_no]['additional_data'] =$product_wert['additional_data'];
					$order_infos['items'][$product_no]['price'] = $product_wert['price'];
					$order_infos['items'][$product_no]['price_incl_tax'] = $product_wert['price_incl_tax'];
					$order_infos['items'][$product_no]['base_row_total_incl_tax'] = $product_wert['base_row_total_incl_tax'];
					$order_infos['items'][$product_no]['original_price'] = $product_wert['original_price'];
					$order_infos['items'][$product_no]['base_tax_amount'] = $product_wert['base_tax_amount'];
					$order_infos['items'][$product_no]['gift_message_id'] = $product_wert['gift_message_id'];
					$order_infos['items'][$product_no]['gift_message_available'] = $product_wert['gift_message_available'];
					$product_no++;
				}			
				if (DEBUGGER>=1) fwrite($dateihandle,"Durchgang: ".$rcm." mit Produkten: $product_no \n");
			
				$product_no--;
				// Bestellungen in XML exportieren
				include ('./functions/dmc_xml_order.php');			
				$i++;
				fwrite($dateihandle, "...= ".$order_list[0]['increment_id']."-".$order_infos['items'][0]['sku']);
			}
			// close db
			dmc_db_disconnect($link);	
			
			// exit;
			if (DEBUGGER>=1) fwrite($dateihandle,"--------->  Größe OrderList: ".count($order_list)."\n\n");
			
			// Datum der zuletzt abgerufenen Bestellung ermitteln
			if (count($order_list)>0 && $last_order_id !='') $last_order_abgerufen=get_order_date_by_incr_id($last_order_id);
			
			
		} // end for Abschnitt Bestellungen in 10er Schritten 
		
		// $schema .=	'</ORDER_LIST>' . "\n";		
		
		// Print XML
		if (!COPY_ORDER_IN_DATEBASE) echo $schema;
		// Print Zusatzbestellung 
		//if (!COPY_ORDER_IN_DATEBASE) echo $schema2;
		
	/*	if (BACKUP_ORDERS)
			{
				// Bestellung in Text XML Datei
				$dateiname='./backup/bestellungen'.date("YmdHis").".xml";	
				$dateihandle3 = fopen($dateiname,"w");
				fwrite($dateihandle3, $schema);		
				fclose($dateihandle3);				
			}
		if (ORDER_COPY_EMAIL)  {
			// Status eMails senden	
			$sender = ORDER_COPY_EMAIL_FROM;
			$empfaenger = ORDER_COPY_EMAIL_TO;
			$betreff = "Abgerufene Bestellungen vom ".date("YmdHis");
			$mailtext = "Bestellabruf von Bestellnummern".$erste_id." bis ".$last_order_id."\n\nInhalte:".$schema;
			mail($empfaenger, $betreff, $mailtext, "From: $sender "); 
		} // end if ORDER_COPY_EMAIL
				*/
		// Close the session -> dürfte bei DB nicht sein.
		
		if ($session>0)
	     $client->endSession($session);
	// Keine Export Aktion ausgewählt oder Zugriff verweigert
	} else if (!$zugriff){
		// Zugriff verweigert
		echo "Zugriff verweigert \n access denied";
	} else {
		// Keine Export Aktion ausgewählt
		echo "Keine Aktion ausgewaehlt \n Modus not available";
	}
	
?>