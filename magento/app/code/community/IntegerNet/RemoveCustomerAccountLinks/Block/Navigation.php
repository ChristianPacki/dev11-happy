<?php
/**
 * integer_net Magento Module
 *
 * @category    IntegerNet
 * @package     IntegerNet_RemoveCustomerAccountLinks
 * @copyright   Copyright (c) 2013 integer_net GmbH (http://www.integer-net.de/)
 * @license     http://opensource.org/licenses/osl-3.0.php Open Software Licence 3.0 (OSL-3.0)
 * @author      Christian Philipp <cp@integer-net.de>
 * @author      Viktor Franz <vf@integer-net.de>
 */

// BEGIN: Amasty GDPR, conflict resolving by Dzmitry Smolik (AMO-686-37715 - 2018-07-06)
if (Mage::helper('core')->isModuleEnabled('Amasty_Gdpr')) {
    class IntegerNet_RemoveCustomerAccountLinks_Block_Navigation_Pure extends Amasty_Gdpr_Block_Account_Navigation {}
} else {
    class IntegerNet_RemoveCustomerAccountLinks_Block_Navigation_Pure extends Mage_Customer_Block_Account_Navigation {}
}
// END: Amasty GDPR, conflict resolving by Dzmitry Smolik (AMO-686-37715 - 2018-07-06)

/**
 * Enter description here ...
 */
class IntegerNet_RemoveCustomerAccountLinks_Block_Navigation extends IntegerNet_RemoveCustomerAccountLinks_Block_Navigation_Pure
{
    /**
     * @return $this
     */
    public function removeLink()
    {
        foreach (Mage::helper('integernet_removecustomeraccountlinks')->getNavigationLinksToRemove() as $link) {
            unset($this->_links[$link]);
        }

        return $this;
    }

    /**
     * @return mixed
     */
    protected function _toHtml()
    {
        $this->removeLink();
        return parent::_toHtml();
    }
}
