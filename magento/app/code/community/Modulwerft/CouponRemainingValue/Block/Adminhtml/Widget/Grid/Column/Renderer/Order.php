<?php

/**
 * @category Modulwerft
 * @package Modulwerft_CouponRemainingValue
 * @author Modulwerft <info@modulwerft.com>
 * @author Philipp Zabel <p.zabel@shopwerft.com>
 * @copyright 2014 Shopwerft GmbH (http://www.shopwerft.com)
 */
class Modulwerft_CouponRemainingValue_Block_Adminhtml_Widget_Grid_Column_Renderer_Order
    extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{

    /**
     * @param Varien_Object $row
     * @return string
     */
    protected function _getValue(Varien_Object $row)
    {
        $value = parent::_getValue($row);

        if (Mage::getSingleton('admin/session')->isAllowed('sales/order/actions/view')) {
            $value = '<a href="' . $this->getUrl('*/sales_order/view', array('order_id' => $row->getOrderId())) . '">'
                . $value . '</a>';
        }

        return $value;
    }

    public function renderExport(Varien_Object $row)
    {
        // return the raw value for the export
        return parent::_getValue($row);
    }

}
