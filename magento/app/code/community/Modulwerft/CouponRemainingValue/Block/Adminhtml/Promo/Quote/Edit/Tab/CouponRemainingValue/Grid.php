<?php

/**
 * @category Modulwerft
 * @package Modulwerft_CouponRemainingValue
 * @author Modulwerft GmbH <info@modulwerft.com>
 * @author Benjamin Wunderlich <b.wunderlich@shopwerft.com>
 * @copyright 2014 Shopwerft GmbH (http://www.shopwerft.com)
 */
class Modulwerft_CouponRemainingValue_Block_Adminhtml_Promo_Quote_Edit_Tab_CouponRemainingValue_Grid
    extends Mage_Adminhtml_Block_Widget_Grid
{

    /**
     * Constructor
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('couponRemainingValueGrid');
        $this->setDefaultSort('used_at');
        $this->setUseAjax(true);
    }

    /**
     * Prepare collection for grid
     *
     * @return Mage_Adminhtml_Block_Widget_Grid
     */
    protected function _prepareCollection()
    {
        $priceRule = Mage::registry('current_promo_quote_rule');

        /** @var Modulwerft_CouponRemainingValue_Model_Resource_History_Collection $collection */
        $collection = Mage::getResourceModel('modulwerft_couponremainingvalue/history_collection')
            ->addRuleToFilter($priceRule);

        $collection->getSelect()
            ->join(
                array('o' => $collection->getResource()->getTable('sales/order')),
                'main_table.order_id = o.entity_id',
                'increment_id'
            )
            ->join(
                array('c' => $collection->getResource()->getTable('salesrule/coupon')),
                'main_table.coupon_id = c.coupon_id',
                'code'
            );

        $this->setCollection($collection);

        return parent::_prepareCollection();
    }

    /**
     * Define grid columns
     *
     * @return Mage_Adminhtml_Block_Widget_Grid
     */
    protected function _prepareColumns()
    {
        $this->addColumn('crv_used_at', array(
            'header' => $this->__('Used On'),
            'index'  => 'used_at',
            'type'   => 'datetime',
            'align'  => 'center',
            'width'  => '160px',
            'sortable' => false,
            'filter' => false,
        ));

        $this->addColumn('crv_coupon_code', array(
            'header' => $this->__('Coupon Code'),
            'index'  => 'code',
            'filter_index' => 'c.code',
            'type'   => 'text',
            'width'  => '150px',
            'sortable' => false,
        ));

        $this->addColumn('crv_order_id', array(
            'header' => $this->__('Order ID'),
            'index'  => 'increment_id',
            'type'   => 'number',
            'width'  => '150px',
            'renderer' => 'modulwerft_couponremainingvalue/adminhtml_widget_grid_column_renderer_order',
            'sortable' => false,
            'filter' => false,
        ));

        $this->addColumn('crv_action', array(
            'header' => $this->__('Action'),
            'index'  => 'used_value',
            'type'   => 'number',
            'renderer' => 'modulwerft_couponremainingvalue/adminhtml_widget_grid_column_renderer_coupon_action',
            'sortable' => false,
            'filter' => false,
        ));

        $this->addColumn('crv_used_value', array(
            'header' => $this->__('Used Value'),
            'index'  => 'used_value',
            'type'   => 'currency',
            'currency_code' => Mage::getStoreConfig(Mage_Directory_Model_Currency::XML_PATH_CURRENCY_BASE),
            'sortable' => false,
            'filter' => false,
        ));

        $this->addColumn('crv_left_value', array(
            'header' => $this->__('Left Value'),
            'index'  => 'left_value',
            'type'   => 'currency',
            'currency_code' => Mage::getStoreConfig(Mage_Directory_Model_Currency::XML_PATH_CURRENCY_BASE),
            'sortable' => false,
            'filter' => false,
        ));

        $this->addExportType('*/promo_quote_coupon/exportCouponsRemainingValueCsv', Mage::helper('customer')->__('CSV'));
        $this->addExportType('*/promo_quote_coupon/exportCouponsRemainingValueXml', Mage::helper('customer')->__('Excel XML'));

        return parent::_prepareColumns();
    }

    /**
     * @return string
     * @since 1.2.0
     */
    public function getGridUrl()
    {
        return $this->getUrl('*/promo_quote_coupon/history', array('_current' => true));
    }

    /**
     * @param array $params
     * @return string
     * @since 1.2.0
     */
    public function getAbsoluteGridUrl($params = array())
    {
        return $this->getUrl('*/promo_quote_coupon/history', $params);
    }

}
