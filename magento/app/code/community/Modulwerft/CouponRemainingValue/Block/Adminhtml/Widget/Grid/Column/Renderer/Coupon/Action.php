<?php

/**
 * @category Modulwerft
 * @package Modulwerft_CouponRemainingValue
 * @author Modulwerft <info@modulwerft.com>
 * @author Philipp Zabel <p.zabel@shopwerft.com>
 * @copyright 2014 Shopwerft GmbH (http://www.shopwerft.com)
 * @since 1.1.0
 */
class Modulwerft_CouponRemainingValue_Block_Adminhtml_Widget_Grid_Column_Renderer_Coupon_Action
    extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{

    /**
     * @param Varien_Object $row
     * @return string
     */
    protected function _getValue(Varien_Object $row)
    {
        $value = parent::_getValue($row);

        if ($row->isCreditmemo()) {
            $text = $this->__('Canceled Order');
        } else {
            $text = $this->__('Redemption');
        }

        return $text;
    }

}
