<?php

/**
 * @category Modulwerft
 * @package Modulwerft_CouponRemainingValue
 * @author Modulwerft <info@modulwerft.com>
 * @author Philipp Zabel <p.zabel@shopwerft.com>
 * @copyright 2014 Shopwerft GmbH (http://www.shopwerft.com)
 * @since 1.1.0
 */
class Modulwerft_CouponRemainingValue_Block_Email_History
    extends Mage_Core_Block_Template
{

    /**
     * @return Mage_SalesRule_Model_Rule
     */
    public function getRule()
    {
        return $this->_getData('rule');
    }

    /**
     * @return string
     */
    public function getCouponCode()
    {
        return $this->_getData('coupon_code');
    }

    /**
     * @return Modulwerft_CouponRemainingValue_Model_Coupon|null
     */
    public function getCoupon()
    {
        if (!$this->getCouponCode()) {
            return null;
        }

        /** @var Modulwerft_CouponRemainingValue_Model_Coupon $crvCoupon */
        $crvCoupon = Mage::getModel('modulwerft_couponremainingvalue/coupon')
            ->loadByCode($this->getCouponCode());

        return $crvCoupon;
    }

    /**
     * @return Modulwerft_CouponRemainingValue_Model_Resource_History_Collection|null
     */
    public function getHistoryItems()
    {
        if (!$this->getRule() || !$this->getCouponCode()) {
            return null;
        }

        /** @var Modulwerft_CouponRemainingValue_Model_Resource_History_Collection $history */
        $history = Mage::getResourceModel('modulwerft_couponremainingvalue/history_collection');

        $history->addRuleToFilter($this->getRule())
            ->addCouponCodeFilter($this->getCouponCode())
            ->setOrder('used_at', Varien_Data_Collection::SORT_ORDER_ASC);

        return $history;
    }

}
 