<?php

/**
 * @category Modulwerft
 * @package Modulwerft_CouponRemainingValue
 * @author Modulwerft GmbH <info@modulwerft.com>
 * @author Philipp Zabel <p.zabel@shopwerft.com>
 * @copyright 2014 Shopwerft GmbH (http://www.shopwerft.com)
 */

/**
 * This setup script replaces the install script 1.0.0. There were many problems with the "ALTER TABLE"
 * statement for the foreign keys, when multiple users run in the same time through the script. MySQL then throws
 * an errors telling that the foreign key already exists.
 */

/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

// history table
$installer->run("
CREATE TABLE IF NOT EXISTS `{$installer->getTable('modulwerft_couponremainingvalue/history')}` (
  `use_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `order_id` int(10) unsigned NOT NULL,
  `rule_id` int(10) unsigned NOT NULL,
  `used_at` datetime NOT NULL,
  `used_value` decimal(12,4) NOT NULL,
  `left_value` decimal(12,4) NOT NULL,
  PRIMARY KEY (`use_id`),
  KEY `order_id` (`order_id`),
  KEY `rule_id` (`rule_id`),
  CONSTRAINT `{$installer->getTable('modulwerft_couponremainingvalue/history')}_ibfk_1` FOREIGN KEY (`order_id`) REFERENCES `{$installer->getTable('sales/order')}` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `{$installer->getTable('modulwerft_couponremainingvalue/history')}_ibfk_2` FOREIGN KEY (`rule_id`) REFERENCES `{$installer->getTable('salesrule/rule')}` (`rule_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ;
");

// coupon table
$installer->run("
CREATE TABLE IF NOT EXISTS `{$installer->getTable('modulwerft_couponremainingvalue/coupon')}` (
  `coupon_id` int(10) unsigned NOT NULL,
  `coupon_value` decimal(12,4) NOT NULL,
  `use_remaining_value` smallint(6) NOT NULL default 0,
  PRIMARY KEY (`coupon_id`),
  CONSTRAINT `{$installer->getTable('modulwerft_couponremainingvalue/coupon')}_ibfk_1` FOREIGN KEY (`coupon_id`) REFERENCES `{$installer->getTable('salesrule/coupon')}` (`coupon_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ;
");

$installer->endSetup();
