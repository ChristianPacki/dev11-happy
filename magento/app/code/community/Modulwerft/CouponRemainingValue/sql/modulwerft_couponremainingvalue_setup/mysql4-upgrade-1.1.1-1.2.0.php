<?php

/**
 * @category Modulwerft
 * @package Modulwerft_CouponRemainingValue
 * @author Modulwerft GmbH <info@modulwerft.com>
 * @author Philipp Zabel <p.zabel@shopwerft.com>
 * @copyright 2014 Shopwerft GmbH (http://www.shopwerft.com)
 */

/**
 * This upgrade script builds the new table structure and migrates old data.
 */

/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

$connection = $installer->getConnection();

/**
 * First: fit table definitions.
 */

// add column coupon_id to history table
try {
    $installer->run("
ALTER TABLE `{$installer->getTable('modulwerft_couponremainingvalue/history')}` ADD COLUMN `coupon_id` INT(10) UNSIGNED NOT NULL AFTER `order_id`;
ALTER TABLE `{$installer->getTable('modulwerft_couponremainingvalue/history')}` ADD CONSTRAINT `{$installer->getTable('modulwerft_couponremainingvalue/history')}_ibfk_coupon_id` FOREIGN KEY (`coupon_id`) REFERENCES `{$installer->getTable('salesrule/coupon')}` (`coupon_id`) ON DELETE CASCADE ON UPDATE CASCADE;
    ");
} catch (Exception $e) {
    Mage::logException($e);
}

// add column shipping included to history table
try {
    $installer->run("
ALTER TABLE `{$installer->getTable('modulwerft_couponremainingvalue/history')}` ADD COLUMN `shipping_included` TINYINT(1) NOT NULL;
    ");
} catch (Exception $e) {
    Mage::logException($e);
}

// add column rule_id to coupon table
try {
    $installer->run("
ALTER TABLE `{$installer->getTable('modulwerft_couponremainingvalue/coupon')}` ADD COLUMN `rule_id` INT(10) UNSIGNED NOT NULL AFTER `coupon_id`;
ALTER TABLE `{$installer->getTable('modulwerft_couponremainingvalue/coupon')}` ADD CONSTRAINT `{$installer->getTable('modulwerft_couponremainingvalue/coupon')}_ibfk_rule_id` FOREIGN KEY (`rule_id`) REFERENCES `{$installer->getTable('salesrule/rule')}` (`rule_id`) ON DELETE CASCADE ON UPDATE CASCADE;
    ");
} catch (Exception $e) {
    Mage::logException($e);
}

// add column remaining value to coupon table
try {
    $installer->run("
ALTER TABLE `{$installer->getTable('modulwerft_couponremainingvalue/coupon')}` ADD COLUMN `remaining_value` DECIMAL(12,4) NOT NULL AFTER `coupon_value`;
    ");
} catch (Exception $e) {
    Mage::logException($e);
}

// rule table
$installer->run("
CREATE TABLE IF NOT EXISTS `{$installer->getTable('modulwerft_couponremainingvalue/rule')}` (
  `rule_id` int(10) unsigned NOT NULL,
  `use_remaining_value` smallint(6) NOT NULL default 0,
  PRIMARY KEY (`rule_id`),
  CONSTRAINT `{$installer->getTable('modulwerft_couponremainingvalue/rule')}_ibfk_1` FOREIGN KEY (`rule_id`) REFERENCES `{$installer->getTable('salesrule/rule')}` (`rule_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ;
");

/**
 * Second: migrate old data
 */

/** @var Modulwerft_CouponRemainingValue_Model_Migration_Flag $migrationFlag */
$migrationFlag = Mage::getModel('modulwerft_couponremainingvalue/migration_flag')
    ->loadSelf();

if ($migrationFlag->isPending()) {
    /** @var Varien_Db_Select $select */
    $select = $connection->select();
    $select->from(array('crv' => $installer->getTable('modulwerft_couponremainingvalue/coupon')), '')
        ->joinLeft(array('c' => $installer->getTable('salesrule/coupon')), 'crv.coupon_id = c.coupon_id', '')
        ->columns(array('c.rule_id', 'crv.use_remaining_value'))
        ->where('c.is_primary = 1');

    // retrieve rule_id from coupons and copy data from crv coupon to crv rule table
    $installer->run($connection->insertFromSelect($select, $installer->getTable('modulwerft_couponremainingvalue/rule'), array(), Varien_Db_Adapter_Interface::INSERT_IGNORE));

    try {
        // insert coupon ids in the new column
        $installer->run("
UPDATE {$installer->getTable('modulwerft_couponremainingvalue/history')} `h` LEFT JOIN {$installer->getTable('salesrule/coupon')} `c` ON `h`.`rule_id` = `c`.`rule_id` SET `h`.`coupon_id` = `c`.`coupon_id` WHERE `h`.`rule_id` = `c`.`rule_id` AND `c`.`is_primary` = 1;
        ");
    } catch (Exception $e) {
        Mage::logException($e);
    }

    try {
        // insert rule ids in the new column
        $installer->run("
UPDATE {$installer->getTable('modulwerft_couponremainingvalue/coupon')} `crv` LEFT JOIN {$installer->getTable('salesrule/coupon')} `c` ON `crv`.`coupon_id` = `c`.`coupon_id` SET `crv`.`rule_id` = `c`.`rule_id` WHERE `crv`.`coupon_id` = `c`.`coupon_id`;
        ");
    } catch (Exception $e) {
        Mage::logException($e);
    }

    try {
        // move discount amount of rule to remaining value of crv coupon
        $installer->run("
UPDATE {$installer->getTable('modulwerft_couponremainingvalue/coupon')} `crv` LEFT JOIN {$installer->getTable('salesrule/rule')} `r` ON `r`.`rule_id` = `crv`.`rule_id` SET `crv`.`remaining_value` = `r`.`discount_amount` WHERE `r`.`rule_id` = `crv`.`rule_id`;
        ");
    } catch (Exception $e) {
        Mage::logException($e);
    }

    try {
        // set discount amount of rule to coupon value of crv coupon
        $installer->run("
UPDATE {$installer->getTable('salesrule/rule')} `r` LEFT JOIN {$installer->getTable('modulwerft_couponremainingvalue/coupon')} `crv` ON `r`.`rule_id` = `crv`.`rule_id` LEFT JOIN {$installer->getTable('modulwerft_couponremainingvalue/rule')} `crvrule` ON `r`.`rule_id` = `crvrule`.`rule_id` SET `r`.`discount_amount` = `crv`.`coupon_value` WHERE `r`.`rule_id` = `crv`.`rule_id` AND `crvrule`.`use_remaining_value` = 1;
        ");
    } catch (Exception $e) {
        Mage::logException($e);
    }

    try {
        // apply shipping included flag to coupon history table
        $installer->run("
UPDATE {$installer->getTable('modulwerft_couponremainingvalue/history')} `h` LEFT JOIN {$installer->getTable('salesrule/rule')} `r` ON `h`.`rule_id` = `r`.`rule_id` SET `h`.`shipping_included` = `r`.`apply_to_shipping` WHERE `h`.`rule_id` = `r`.`rule_id`;
        ");
    } catch (Exception $e) {
        Mage::logException($e);
    }

    try {
        $migrationFlag->setDone()
            ->save();
    } catch (Exception $e) {
        Mage::logException($e);
    }
}

/**
 * Third: removed unused data
 */

try {
    // remove coupon table, because we don't need it anymore
    $connection->dropColumn($installer->getTable('modulwerft_couponremainingvalue/coupon'), 'use_remaining_value');
} catch (Exception $e) {
    Mage::logException($e);
}

$installer->endSetup();
