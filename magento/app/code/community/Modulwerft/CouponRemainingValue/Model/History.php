<?php

/**
 * @category Modulwerft
 * @package Modulwerft_CouponRemainingValue
 * @author Modulwerft GmbH <info@modulwerft.com>
 * @author Benjamin Wunderlich <b.wunderlich@shopwerft.com>
 * @copyright 2014 Shopwerft GmbH (http://www.shopwerft.com)
 */

/**
 * @method int getRuleId()
 * @method int getCouponId()
 * @method int getOrderId()
 * @method string getUsedAt()
 * @method float getUsedValue()
 * @method float getLeftValue()
 * @method Modulwerft_CouponRemainingValue_Model_History setRuleId($ruleId)
 * @method Modulwerft_CouponRemainingValue_Model_History setCouponId($couponId)
 * @method Modulwerft_CouponRemainingValue_Model_History setOrderId($orderId)
 * @method Modulwerft_CouponRemainingValue_Model_History setUsedAt($usedAt)
 * @method Modulwerft_CouponRemainingValue_Model_History setUsedValue($usedValue)
 * @method Modulwerft_CouponRemainingValue_Model_History setLeftValue($leftValue)
 * @method Modulwerft_CouponRemainingValue_Model_History setShippingIncluded($shippingIncluded)
 */
class Modulwerft_CouponRemainingValue_Model_History
    extends Mage_Core_Model_Abstract
{

    /** @var string */
    protected $_eventPrefix = 'modulwerft_couponremainingvalue_history';

    /** @var string */
    protected $_eventObject = 'history';

    protected function _construct()
    {
        parent::_construct();
        $this->_init('modulwerft_couponremainingvalue/history');
    }

    /**
     * @param Mage_SalesRule_Model_Rule $rule
     * @return $this
     */
    public function setRule(Mage_SalesRule_Model_Rule $rule)
    {
        $this->setRuleId($rule->getId());

        return $this;
    }

    /**
     * @param Mage_Sales_Model_Order $order
     * @return $this
     */
    public function setOrder(Mage_Sales_Model_Order $order)
    {
        $this->setOrderId($order->getId());
        $this->setData('order', $order);
        return $this;
    }

    /**
     * @return Mage_Sales_Model_Order
     * @since 1.1.0
     */
    public function getOrder()
    {
        if (!$this->hasData('order')) {
            /** @var Mage_Sales_Model_Order $order */
            $order = Mage::getModel('sales/order')
                ->load($this->getOrderId());

            $this->setData('order', $order);
        }
        return $this->_getData('order');
    }

    /**
     * @param Mage_SalesRule_Model_Rule $rule
     * @return $this
     */
    public function loadByRule(Mage_SalesRule_Model_Rule $rule)
    {
        $this->load($rule->getId(), 'rule_id');
        return $this;
    }

    /**
     * @return bool
     * @since 1.1.0
     */
    public function isValueLeft()
    {
        return $this->getLeftValue() > 0;
    }

    /**
     * @return bool
     * @since 1.1.0
     */
    public function isCreditmemo()
    {
        return $this->getUsedValue() < 0;
    }

    /**
     * @return bool
     * @since 1.2.0
     */
    public function isShippingIncluded()
    {
        return (bool)$this->_getData('shipping_included');
    }

}
