<?php

/**
 * @category Modulwerft
 * @package Modulwerft_CouponRemainingValue
 * @author Modulwerft GmbH <info@modulwerft.com>
 * @author Benjamin Wunderlich <b.wunderlich@shopwerft.com>
 * @copyright 2014 Shopwerft GmbH (http://www.shopwerft.com)
 */
class Modulwerft_CouponRemainingValue_Model_Observer
{

    /**
     * Handle sales rule after order has been placed.
     *
     * @param Varien_Event_Observer $observer
     */
    public function salesOrderPlaceAfter(Varien_Event_Observer $observer)
    {
        /** @var Mage_Sales_Model_Order $order */
        $order = $observer->getEvent()->getOrder();

        /** @var Modulwerft_CouponRemainingValue_Helper_Config $helperConfig */
        $helperConfig = Mage::helper('modulwerft_couponremainingvalue/config');

        // is module active in orders store?
        if (!$helperConfig->isModuleActive($order->getStoreId())) {
            return;
        }

        /** @var Modulwerft_CouponRemainingValue_Model_Coupon_Service $service */
        $service = Mage::getSingleton('modulwerft_couponremainingvalue/coupon_service');

        try {
            $service->useCouponValues($order);
        } catch (Exception $e) {
            $this->_getLogHelper()->logException($e);
        }
    }

    /**
     * @param Varien_Event_Observer $observer
     */
    public function prepareCouponForm(Varien_Event_Observer $observer)
    {
        /** @var Modulwerft_CouponRemainingValue_Helper_Config $helper */
        $helperConfig = Mage::helper('modulwerft_couponremainingvalue/config');

        if (!$helperConfig->isModuleActive()) {
            return;
        }

        /** @var Varien_Data_Form $form */
        $form = $observer->getEvent()->getForm();

        /** @var Varien_Data_Form_Element_Fieldset $actionFieldset */
        $actionFieldset = $form->getElement('action_fieldset');

        if (!$actionFieldset) {
            return;
        }

        // add field after discount amount
        $actionFieldset->addField(
            Modulwerft_CouponRemainingValue_Model_Rule::RULE_USE_REMAINING_VALUE,
            'select',
            array(
                'label'     => Mage::helper('modulwerft_couponremainingvalue')->__('Use Remaining Value'),
                'title'     => Mage::helper('modulwerft_couponremainingvalue')->__('Use Remaining Value'),
                'name'      => Modulwerft_CouponRemainingValue_Model_Rule::RULE_USE_REMAINING_VALUE,
                'options'    => array(
                    '1' => Mage::helper('adminhtml')->__('Yes'),
                    '0' => Mage::helper('adminhtml')->__('No'),
                ),
            ),
            'discount_amount'
        );
    }

    /**
     * @param Varien_Event_Observer $observer
     * @since 1.2.0
     */
    public function ruleSaveBefore(Varien_Event_Observer $observer)
    {
        /** @var Mage_SalesRule_Model_Rule $rule */
        $rule = $observer->getEvent()->getRule();

        if (!$this->_useRuleCoupons($rule)) {
            return;
        }

        $useRemainingValue = $this->_getHelper()->usesRuleRemainingValue($rule);

        // reset usage limit and multiple coupon codes, when remaining value should be used
        if ($useRemainingValue) {
            $rule->setUsesPerCoupon(null)
                ->setUsesPerCustomer(0);
        }
    }

    /**
     * @param Varien_Event_Observer $observer
     * @since 1.2.0
     */
    public function ruleSaveAfter(Varien_Event_Observer $observer)
    {
        /** @var Mage_SalesRule_Model_Rule $rule */
        $rule = $observer->getEvent()->getRule();

        /** @var Modulwerft_CouponRemainingValue_Helper_Log $logHelper */
        $logHelper = Mage::helper('modulwerft_couponremainingvalue/log');

        if (!$this->_useRuleCoupons($rule)) {
            return;
        }

        $useRemainingValue = $this->_getHelper()->usesRuleRemainingValue($rule);

        /** @var Modulwerft_CouponRemainingValue_Helper_Data $helper */
        $helper = Mage::helper('modulwerft_couponremainingvalue');

        if ($useRemainingValue && !$helper->isRuleActionApplicable($rule)) {
            Mage::throwException('Coupon Remaining Value is not applicable with this rule action');
        }

        /** @var Modulwerft_CouponRemainingValue_Model_Rule $crvRule */
        $crvRule = Mage::getModel('modulwerft_couponremainingvalue/rule')
            ->load($rule->getId());

        // if no remaining value should be used and the rule was not saved yet, don't create one
        if (!$crvRule->getId() && !$useRemainingValue) {
            return;
        }

        // if rule doesn't exist yet, set id from rule
        if (!$crvRule->getId()) {
            $crvRule->setId($rule->getId());
        }

        $crvRule->setData(Modulwerft_CouponRemainingValue_Model_Rule::RULE_USE_REMAINING_VALUE, $useRemainingValue);

        try {
            $crvRule->save();
        } catch (Exception $e) {
            $logHelper->logException($e);
        }
    }

    /**
     * Add the "use_remaining_value" flag to the rule.
     *
     * @param Varien_Event_Observer $observer
     * @since 1.2.0
     */
    public function ruleLoadAfter(Varien_Event_Observer $observer)
    {
        /** @var Mage_SalesRule_Model_Rule $rule */
        $rule = $observer->getEvent()->getRule();

        /** @var Modulwerft_CouponRemainingValue_Model_Rule $crvRule */
        $crvRule = Mage::getModel('modulwerft_couponremainingvalue/rule')
            ->load($rule->getId());

        $useRemainigValue = false;
        if ($crvRule->getId()) {
            $useRemainigValue = $crvRule->getUseRemainingValue();
        }

        $rule->setData(Modulwerft_CouponRemainingValue_Model_Rule::RULE_USE_REMAINING_VALUE, $useRemainigValue);
    }

    /**
     * The event "core_model_save_commit_after" is used here, because otherwise, when creating a new rule, we don't
     * have the flag "use_remaining_value" yet. The flag will be saved in afterSave event of the rule, but the afterSave
     * event of the coupon will be executed before. So the transaction has to be fully committed first.
     *
     * @param Varien_Event_Observer $observer
     */
    public function couponSaveAfter(Varien_Event_Observer $observer)
    {
        /** @var Mage_SalesRule_Model_Coupon $coupon */
        $coupon = $observer->getEvent()->getObject();

        if (!$coupon instanceof Mage_SalesRule_Model_Coupon) {
            return;
        }

        /** @var Mage_SalesRule_Model_Rule $rule */
        $rule = Mage::getModel('salesrule/rule')
            ->load($coupon->getRuleId());

        if (!$rule->getId() || !$this->_getHelper()->usesRuleRemainingValue($rule)) {
            return;
        }

        /** @var Modulwerft_CouponRemainingValue_Model_Coupon $crvCoupon */
        $crvCoupon = Mage::getModel('modulwerft_couponremainingvalue/coupon')
            ->load($coupon->getId());

        // if the coupon already exists, don't do anything
        if ($crvCoupon->getId()) {
            return;
        }

        $crvCoupon->setId($coupon->getId())
            ->setRuleId($rule->getId())
            ->setCouponValue($rule->getDiscountAmount())
            ->setRemainingValue($rule->getDiscountAmount());

        try {
            $crvCoupon->save();
        } catch (Exception $e) {
            $this->_getLogHelper()->logException($e);
        }
    }

    /**
     * @param Varien_Event_Observer $observer
     * @since 1.1.0
     */
    public function orderCancelResetCouponValue(Varien_Event_Observer $observer)
    {
        /** @var Mage_Sales_Model_Order $order */
        $order = $observer->getEvent()->getOrder();

        if (!$this->_isModuleActive($order->getStoreId())) {
            return;
        }

        /** @var Modulwerft_CouponRemainingValue_Model_Coupon_Service $service */
        $service = Mage::getSingleton('modulwerft_couponremainingvalue/coupon_service');

        try {
            $service->resetCouponValues($order);
        } catch (Exception $e) {
            $this->_getLogHelper()->logException($e);
        }
    }

    /**
     * @param Varien_Event_Observer $observer
     * @since 1.1.0
     */
    public function creditmemoRefundResetCouponValue(Varien_Event_Observer $observer)
    {
        /** @var Mage_Sales_Model_Order_Creditmemo $creditmemo */
        $creditmemo = $observer->getEvent()->getCreditmemo();

        /** @var Mage_Sales_Model_Order $order */
        $order = $creditmemo->getOrder();

        if (!$this->_isModuleActive($order->getStoreId())) {
            return;
        }

        /** @var Modulwerft_CouponRemainingValue_Model_Coupon_Service $service */
        $service = Mage::getSingleton('modulwerft_couponremainingvalue/coupon_service');

        try {
            $service->resetCouponValues($order, $creditmemo);
        } catch (Exception $e) {
            $this->_getLogHelper()->logException($e);
        }
    }

    /**
     * @param Varien_Event_Observer $observer
     * @since 1.1.0
     */
    public function couponSendNotificationResetValue(Varien_Event_Observer $observer)
    {
        /** @var Mage_Sales_Model_Order $order */
        $order = $observer->getEvent()->getOrder();

        if (!$this->_isModuleActive($order->getStoreId())) {
            return;
        }

        /** @var Mage_Sales_Model_Order_Creditmemo $creditmemo */
        $creditmemo = $observer->getEvent()->getCreditmemo();

        /** @var Modulwerft_CouponRemainingValue_Model_Coupon_Service $service */
        $service = Mage::getSingleton('modulwerft_couponremainingvalue/coupon_service');

        try {
            $service->sendNotifications($order, $creditmemo);
        } catch (Exception $e) {
            $this->_getLogHelper()->logException($e);
        }
    }

    /**
     * @param Varien_Event_Observer $observer
     * @since 1.1.0
     */
    public function couponSendNotificationUsedValue(Varien_Event_Observer $observer)
    {
        /** @var Mage_Sales_Model_Order $order */
        $order = $observer->getEvent()->getOrder();

        if (!$this->_isModuleActive($order->getStoreId())) {
            return;
        }

        /** @var Modulwerft_CouponRemainingValue_Model_Coupon_Service $service */
        $service = Mage::getSingleton('modulwerft_couponremainingvalue/coupon_service');

        try {
            $service->sendNotifications($order);
        } catch (Exception $e) {
            $this->_getLogHelper()->logException($e);
        }
    }

    /**
     * @param Varien_Event_Observer $observer
     * @since 1.1.0
     */
    public function redeemCoupon(Varien_Event_Observer $observer)
    {
        /** @var Mage_Sales_Model_Order $order */
        $order = $observer->getEvent()->getOrder();

        if (!$this->_isModuleActive($order->getStoreId())) {
            return;
        }

        // we need to check whether state was null, because with payment method "free" the order will be directly set
        // to state "processing" without being in state "new"
        $orderWasNew = Mage_Sales_Model_Order::STATE_NEW == $order->getOrigData('state')
            || is_null($order->getOrigData('state'));

        $orderIsInProcessing = Mage_Sales_Model_Order::STATE_PROCESSING == $order->getState()
            || Mage_Sales_Model_Order::STATE_COMPLETE == $order->getState();

        // check whether order was changed from state "new" to "processing" or to "complete"
        if (!$order->dataHasChangedFor('state') || !$orderWasNew || !$orderIsInProcessing) {
            return;
        }

        /** @var Modulwerft_CouponRemainingValue_Model_Coupon_Service $service */
        $service = Mage::getSingleton('modulwerft_couponremainingvalue/coupon_service');

        try {
            $service->redeemCoupons($order);
        } catch (Exception $e) {
            $this->_getLogHelper()->logException($e);
        }
    }

    /**
     * @param Varien_Event_Observer $observer
     * @since 1.2.0
     */
    public function applyCouponValueToRule(Varien_Event_Observer $observer)
    {
        /** @var Mage_SalesRule_Model_Mysql4_Rule_Collection $collection */
        $collection = $observer->getEvent()->getCollection();

        // event is dispatched for all collection, but we only want to observe the rule collection
        if (!($collection instanceof Mage_SalesRule_Model_Mysql4_Rule_Collection
            || $collection instanceof Mage_SalesRule_Model_Resource_Rule_Collection)
        ) {
            return;
        }

        $this->_getLogHelper()->log('-- Checking rule collection for validation filter');

        if (!$collection->getFlag('validation_filter')) {
            $this->_getLogHelper()->log('Rule collection has no validation filter set');
            return;
        }

        /** @var Mage_SalesRule_Model_Rule $_rule */
        foreach ($collection as $_rule) {
            if (!$this->_getHelper()->usesRuleRemainingValue($_rule)) {
                $this->_getLogHelper()->log('>> Rule #"' . $_rule->getId() . '" does not use remaining value');
                continue;
            }

            $_couponCode = $_rule->getCode();

            /** @var Modulwerft_CouponRemainingValue_Model_Coupon $_crvCoupon */
            $_crvCoupon = Mage::getModel('modulwerft_couponremainingvalue/coupon')
                ->loadByCode($_couponCode);

            if (!$_crvCoupon->getId()) {
                $this->_getLogHelper()->log('>> Did not find coupon with code "' . $_couponCode . '"');
                continue;
            }

            $_rule->setDiscountAmount($_crvCoupon->getRemainingValue());
        }
    }

    /**
     * @param Varien_Event_Observer $observer
     * @since 1.2.0
     */
    public function validateRemainingValueOfCoupon(Varien_Event_Observer $observer)
    {
        /** @var Mage_Sales_Model_Quote $quote */
        $quote = $observer->getEvent()->getQuote();

        $ruleIds = $this->_getHelper()->getAppliedRulesOfQuote($quote);

        /** @var Mage_SalesRule_Model_Mysql4_Rule_Collection $rules */
        $rules = Mage::getResourceModel('salesrule/rule_collection');
        $rules->addFieldToFilter($rules->getResource()->getIdFieldName(), array('in' => $ruleIds));

        // only rules with remaining value
        $rules->getSelect()->where('crvrule.use_remaining_value = ?', 1);

        /** @var Mage_SalesRule_Model_Rule $_rule */
        $rulesHaveChanged = false;
        foreach ($rules as $_rule) {
            $_couponCode = $quote->getCouponCode();

            /** @var Modulwerft_CouponRemainingValue_Model_Coupon $_crvCoupon */
            $_crvCoupon = Mage::getModel('modulwerft_couponremainingvalue/coupon')
                ->loadByCode($_couponCode);

            // check if coupon is already redeemed
            if (!$_crvCoupon->hasValueLeft()) {
                unset($ruleIds[array_search($_rule->getId(), $ruleIds)]);
                $rulesHaveChanged = true;
            }
        }

        // update quote if rules have changed
        if ($rulesHaveChanged) {
            $quote->setAppliedRuleIds(implode(',', $ruleIds));
            $quote->setCouponCode('');
            $quote->collectTotals();
        }
    }

    /**
     * @param Varien_Event_Observer $observer
     * @since 1.2.0
     */
    public function addUseRemainingValueFlag(Varien_Event_Observer $observer)
    {
        /** @var Mage_SalesRule_Model_Mysql4_Rule_Collection $collection */
        $collection = $observer->getEvent()->getCollection();

        // event is dispatched for all collection, but we only want to observe the rule collection
        if (!($collection instanceof Mage_SalesRule_Model_Mysql4_Rule_Collection
            || $collection instanceof Mage_SalesRule_Model_Resource_Rule_Collection)
        ) {
            return;
        }

        $this->_getLogHelper()->log('-- Add use_remaining_value flag to rule collection');

        // join with crv rule table and add use_remaining_value flag
        $this->_getHelper()->addRemainingValueFlagToRules($collection);
    }

    /**
     * @param null|int|Mage_Core_Model_Store $storeId
     * @return bool
     * @since 1.2.0
     */
    protected function _isModuleActive($storeId = null)
    {
        /** @var Modulwerft_CouponRemainingValue_Helper_Config $helperConfig */
        $helperConfig = Mage::helper('modulwerft_couponremainingvalue/config');

        // is module active in orders store?
        return $helperConfig->isModuleActive($storeId);
    }

    /**
     * @param Mage_SalesRule_Model_Rule $rule
     * @return int
     * @since 1.2.0
     */
    protected function _useRuleCoupons(Mage_SalesRule_Model_Rule $rule)
    {
        return Mage_SalesRule_Model_Rule::COUPON_TYPE_NO_COUPON != $rule->getCouponType();
    }

    /**
     * @return Modulwerft_CouponRemainingValue_Helper_Log
     * @since 1.2.0
     */
    protected function _getLogHelper()
    {
        return Mage::helper('modulwerft_couponremainingvalue/log');
    }

    /**
     * @return Modulwerft_CouponRemainingValue_Helper_Data
     * @since 1.2.0
     */
    protected function _getHelper()
    {
        return Mage::helper('modulwerft_couponremainingvalue');
    }

}
