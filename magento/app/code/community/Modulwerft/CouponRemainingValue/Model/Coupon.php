<?php

/**
 * @category Modulwerft
 * @package Modulwerft_CouponRemainingValue
 * @author Modulwerft GmbH <info@modulwerft.com>
 * @author Philipp Zabel <p.zabel@shopwerft.com>
 * @copyright 2014 Shopwerft GmbH (http://www.shopwerft.com)
 */
/**
 * @method int getRuleId()
 * @method Modulwerft_CouponRemainingValue_Model_Coupon setRuleId($ruleId)
 */
class Modulwerft_CouponRemainingValue_Model_Coupon
    extends Mage_Core_Model_Abstract
{

    /**
     * @deprecated since 1.2.0
     * @see Modulwerft_CouponRemainingValue_Model_Rule::RULE_USE_REMAINING_VALUE
     */
    const RULE_USE_REMAINING_VALUE = 'use_remaining_value';

    const RULE_COUPON_VALUE = 'coupon_value';
    const RULE_COUPON_REMAINING_VALUE = 'remaining_value';

    /** @var string */
    protected $_eventPrefix = 'modulwerft_couponremainingvalue_coupon';

    /** @var string */
    protected $_eventObject = 'coupon';

    protected function _construct()
    {
        $this->_init('modulwerft_couponremainingvalue/coupon');
    }

    /**
     * @param bool $useRemainingValue
     * @return $this
     * @deprecated since 1.2.0
     * @see Modulwerft_CouponRemainingValue_Model_Rule::setUseRemainingValue()
     */
    public function setUseRemainingValue($useRemainingValue)
    {
        return $this;
    }

    /**
     * @return bool
     * @deprecated since 1.2.0
     * @see Modulwerft_CouponRemainingValue_Model_Rule::getUseRemainingValue()
     */
    public function getUseRemainingValue()
    {
        /** @var Modulwerft_CouponRemainingValue_Model_Rule $crvRule */
        $crvRule = Mage::getModel('modulwerft_couponremainingvalue/rule')
            ->load($this->getRuleId());

        if (!$crvRule->getId()) {
            return false;
        }

        return $crvRule->getUseRemainingValue();
    }

    /**
     * @param float $couponValue
     * @return $this
     */
    public function setCouponValue($couponValue)
    {
        return $this->setData(self::RULE_COUPON_VALUE, $couponValue);
    }

    /**
     * @return float
     */
    public function getCouponValue()
    {
        return floatval($this->_getData(self::RULE_COUPON_VALUE));
    }

    /**
     * @param float $remainingValue
     * @return $this
     * @since 1.2.0
     */
    public function setRemainingValue($remainingValue)
    {
        return $this->setData(self::RULE_COUPON_REMAINING_VALUE, $remainingValue);
    }

    /**
     * @return float
     * @since 1.2.0
     */
    public function getRemainingValue()
    {
        return floatval($this->_getData(self::RULE_COUPON_REMAINING_VALUE));
    }

    /**
     * @return bool
     * @since 1.2.0
     */
    public function hasValueLeft()
    {
        return abs($this->getRemainingValue()) > Modulwerft_CouponRemainingValue_Helper_Data::DIFF_DELTA;
    }

    /**
     * @param string $couponCode
     * @return $this
     * @since 1.2.0
     */
    public function loadByCode($couponCode)
    {
        /** @var Modulwerft_CouponRemainingValue_Model_Resource_Coupon_Collection $coupons */
        $coupons = Mage::getResourceModel('modulwerft_couponremainingvalue/coupon_collection');

        $coupons->getSelect()
            ->join(
                array('c' => $coupons->getResource()->getTable('salesrule/coupon')),
                'main_table.coupon_id = c.coupon_id',
                ''
            )
            ->where('c.code = ?', $couponCode);

        if ($coupons->getSize() > 0) {
            $coupon = $coupons->getFirstItem();
            $this->setData($coupon->getData());
        }

        return $this;
    }

    /**
     * @return Modulwerft_CouponRemainingValue_Model_Rule|null
     * @since 1.2.0
     */
    public function getRule()
    {
        /** @var Mage_SalesRule_Model_Rule $rule */
        $rule = Mage::getModel('salesrule/rule')
            ->load($this->getRuleId());

        if (!$rule->getId()) {
            return null;
        }

        return $rule;
    }

}
