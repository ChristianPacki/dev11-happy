<?php

/**
 * @category Modulwerft
 * @package Modulwerft_CouponRemainingValue
 * @author Modulwerft GmbH <info@modulwerft.com>
 * @author Philipp Zabel <p.zabel@shopwerft.com>
 * @copyright 2014 Shopwerft GmbH (http://www.shopwerft.com)
 * @since 1.1.0
 */
class Modulwerft_CouponRemainingValue_Model_Coupon_Service
    extends Mage_Core_Model_Abstract
{

    /** @var string */
    protected $_eventPrefix = 'modulwerft_couponremainingvalue_coupon_service';

    /** @var string */
    protected $_eventObject = 'coupon_service';

    /**
     * @param Mage_Sales_Model_Order $order
     * @param Mage_Sales_Model_Order_Creditmemo $creditmemo
     * @return $this
     */
    public function resetCouponValues(Mage_Sales_Model_Order $order,
                                      Mage_Sales_Model_Order_Creditmemo $creditmemo = null)
    {
        // lookup rule ids
        $ruleIds = $this->_getHelper()->getAppliedRulesOfOrder($order);
        if (empty($ruleIds)) {
            $this->_getLogHelper()->log('No rules applied to order #' . $order->getIncrementId(), Zend_Log::DEBUG);
            return $this;
        }

        foreach ($ruleIds as $_ruleId) {
            /** @var Mage_SalesRule_Model_Rule $_rule */
            $_rule = Mage::getModel('salesrule/rule')
                ->load($_ruleId);

            if (!$_rule->getId()) {
                $message = 'Applied rule #' . $_ruleId . ' does not exist anymore'
                    . ' (order #' . $order->getIncrementId() . ')';
                $this->_getLogHelper()->log($message, Zend_Log::DEBUG);
                continue;
            }

            if (!$this->_getHelper()->usesRuleRemainingValue($_rule)) {
                $message = 'Applied rule #' . $_rule->getId() . ' ("' . $_rule->getName() . '") does not use remaining'
                    . ' value for order #' . $order->getIncrementId();
                $this->_getLogHelper()->log($message, Zend_Log::DEBUG);
                continue;
            }

            $this->_resetCouponValue($order, $creditmemo, $_rule);
        }

        return $this;
    }

    /**
     * @param Mage_Sales_Model_Order $order
     * @param Mage_Sales_Model_Order_Creditmemo $creditmemo
     * @param Mage_SalesRule_Model_Rule $rule
     * @return $this
     * @since 1.2.0
     */
    protected function _resetCouponValue(Mage_Sales_Model_Order $order,
                                               Mage_Sales_Model_Order_Creditmemo $creditmemo = null,
                                               Mage_SalesRule_Model_Rule $rule)
    {
        /** @var Modulwerft_CouponRemainingValue_Model_Coupon $crvCoupon */
        $crvCoupon = Mage::getModel('modulwerft_couponremainingvalue/coupon')
            ->loadByCode($order->getCouponCode());

        if (!$crvCoupon->getId()) {
            $message = 'Invalid coupon with code "' . $order->getCouponCode() . '"'
                . ' in order #' . $order->getIncrementId();
            $this->_getLogHelper()->log($message, Zend_Log::WARN);
            return $this;
        }

        /** @var Modulwerft_CouponRemainingValue_Model_Resource_History_Collection $historyOfOrder */
        $historyOfOrder = Mage::getResourceModel('modulwerft_couponremainingvalue/history_collection');
        $historyOfOrder->addOrderToFilter($order)
            ->addFieldToFilter('used_value', array('gteq' => 0));

        // find history entry when coupon was redeemed
        if (0 == $historyOfOrder->count()) {
            $message = 'No history entry found for coupon code "' . $order->getCouponCode() . '"'
                . ' and order #' . $order->getIncrementId();
            $this->_getLogHelper()->log($message, Zend_Log::ERR);
            return $this;
        }

        // there should be only one history entry, because a coupon can be used only once per order
        /** @var Modulwerft_CouponRemainingValue_Model_History $origHistory */
        $origHistory = $historyOfOrder->getFirstItem();
        $shippingIncluded = $origHistory->isShippingIncluded();

        // get refund value from creditmemo (if we have one) else check order items
        if (!is_null($creditmemo)) {
            // from magento 1.8 the discount amount is negative
            $refundValue = abs($creditmemo->getDiscountAmount());

            // check if shipping amount was refunded
            if (!$creditmemo->getShippingAmount()) {
                $shippingIncluded = false;
            }
        } else {
            $refundValue = $this->_getRefundValueOfOrderItems($order);

            // check if shipping amount should be refunded or if it already was invoiced
            /** @var Mage_Sales_Model_Order_Invoice $_invoice */
            foreach ($order->getInvoiceCollection() as $_invoice) {
                if ($_invoice->getShippingAmount() && !$_invoice->isCanceled()) {
                    $shippingIncluded = false;
                    break;
                }
            }

            // refund shipping amount
            if ($shippingIncluded) {
                $refundValue += $order->getShippingInclTax();
            }
        }

        // update remaining value of coupon
        $crvCoupon->setRemainingValue($crvCoupon->getRemainingValue() + $refundValue);

        /** @var Modulwerft_CouponRemainingValue_Model_History $_couponHistory */
        $couponHistoryCreditMemo = Mage::getModel('modulwerft_couponremainingvalue/history');

        $couponHistoryCreditMemo->setOrder($order)
            ->setRule($rule)
            ->setCouponId($crvCoupon->getId())
            ->setUsedValue(-1.0 * $refundValue)
            ->setLeftValue($crvCoupon->getRemainingValue())
            ->setShippingIncluded($shippingIncluded);

        /** @var Mage_Core_Model_Resource_Transaction $transaction */
        $transaction = Mage::getModel('core/resource_transaction');

        $transaction->addObject($crvCoupon)
            ->addObject($couponHistoryCreditMemo);

        try {
            $transaction->save();

            // coupon value was successfully resetted
            $eventParams = array(
                'rule' => $rule,
                'coupon' => $crvCoupon, /** @since 1.2.0 */
                'coupon_history' => $couponHistoryCreditMemo,
                'order' => $order,
                'creditmemo' => $creditmemo,
            );
            Mage::dispatchEvent($this->_eventPrefix . '_reset_coupon_value', $eventParams);
        } catch (Exception $e) {
            Mage::getSingleton('adminhtml/session')
                ->addWarning($this->_getHelper()->__('Could not reset coupon value: %s', $e->getMessage()));
            $this->_getLogHelper()->logException($e);
        }

        return $this;
    }

    /**
     * Collect refund value by iterating through order items, which were canceled. The refund value can't be higher
     * than the amount used with coupon.
     *
     * @param Mage_Sales_Model_Order $order
     * @return float
     * @since 1.2.0
     */
    protected function _getRefundValueOfOrderItems(Mage_Sales_Model_Order $order)
    {
        $refundValue = 0.0;

        /** @var Mage_Sales_Model_Order_Item $_item */
        foreach ($order->getAllVisibleItems() as $_item) {
            if ($_item->getQtyCanceled() > 0) {
                $_refundItemValue = ($_item->getQtyCanceled() * $_item->getPriceInclTax());

                // the refund value can't be higher than the discount amount of the item
                $_itemDiscountAmount = $_item->getDiscountAmount() - $_item->getDiscountInvoiced();
                $_diffDiscountAmountRefundValue = $_itemDiscountAmount - $_refundItemValue;
                if ($_diffDiscountAmountRefundValue < Modulwerft_CouponRemainingValue_Helper_Data::DIFF_DELTA) {
                    $_refundItemValue = $_itemDiscountAmount;
                }

                $refundValue += $_refundItemValue;
            }
        }

        return $refundValue;
    }

    /**
     * @param Mage_Sales_Model_Order $order
     * @return $this
     */
    public function useCouponValues(Mage_Sales_Model_Order $order)
    {
        /** @var Modulwerft_CouponRemainingValue_Helper_Log $logHelper */
        $logHelper = Mage::helper('modulwerft_couponremainingvalue/log');

        /** @var Modulwerft_CouponRemainingValue_Helper_Data $helper */
        $helper = Mage::helper('modulwerft_couponremainingvalue');

        // lookup rule ids
        $ruleIds = $helper->getAppliedRulesOfOrder($order);
        if (empty($ruleIds)) {
            $logHelper->log('No rules applied to order #' . $order->getIncrementId(), Zend_Log::DEBUG);
            return $this;
        }

        // check if order is an edited order
        if (!is_null($order->getRelationParentId()) && !is_null($order->getEditIncrement())) {
            /** @var Mage_Sales_Model_Order $parentOrder */
            $parentOrder = Mage::getModel('sales/order')
                ->load($order->getRelationParentId());

            // check if coupons were already applied in parent order, because we don't have to check them again
            $parentRuleIds = $helper->getAppliedRulesOfOrder($parentOrder);
            if ($parentRuleIds == $ruleIds) {
                $logHelper->log('Order is an edited order', Zend_Log::DEBUG);
                return $this;
            }
        }

        // use each rule (and apply to customer, if applicable)
        foreach ($ruleIds as $_ruleId) {
            /** @var Mage_SalesRule_Model_Rule $_rule */
            $_rule = Mage::getModel('salesrule/rule')
                ->load($_ruleId);

            if (!$_rule->getId()) {
                $message = 'Applied rule #' . $_ruleId . ' does not exist anymore'
                    .' (order #' . $order->getIncrementId() . ')';
                $logHelper->log($message, Zend_Log::DEBUG);
                continue;
            }

            if (!$helper->usesRuleRemainingValue($_rule)) {
                $message = 'Applied rule #' . $_rule->getId() . ' ("' . $_rule->getName() . '") does not use remaining'
                    . ' value for order #' . $order->getIncrementId();
                $logHelper->log($message, Zend_Log::DEBUG);
                continue;
            }

            if (!$helper->isRuleActionApplicable($_rule)) {
                $message = 'Applied rule #' . $_rule->getId() . ' ("' . $_rule->getName() . '") does not ' .
                    'match for remaining value processing for order #' . $order->getIncrementId() . ' - ' .
                    'Times used: ' . $_rule->getTimesUsed();
                $logHelper->log($message, Zend_Log::DEBUG);
                continue;
            }

            $this->_useCouponValue($order, $_rule);
        }

        return $this;
    }

    /**
     * @param Mage_Sales_Model_Order $order
     * @param Mage_SalesRule_Model_Rule $rule
     * @return $this
     * @since 1.2.0
     */
    protected function _useCouponValue(Mage_Sales_Model_Order $order, Mage_SalesRule_Model_Rule $rule)
    {
        /** @var Modulwerft_CouponRemainingValue_Model_Coupon $crvCoupon */
        $crvCoupon = Mage::getModel('modulwerft_couponremainingvalue/coupon')
            ->loadByCode($order->getCouponCode());

        if (!$crvCoupon->getId()) {
            $message = 'Could not find coupon value for Used coupon code "' . $order->getCouponCode() . '"'
                . ' in order #' . $order->getIncrementId();
            $this->_getLogHelper()->log($message, Zend_Log::DEBUG);
            return $this;
        }

        // calculate remaining value
        $remainingValue = $crvCoupon->getRemainingValue() - abs($order->getDiscountAmount());

        // check if remaining value is negative
        if ($remainingValue < -Modulwerft_CouponRemainingValue_Helper_Data::DIFF_DELTA) {
            $this->_getLogHelper()->log('Coupon Remaining Value is negative', Zend_Log::WARN);
        }

        // there is a remaining value
        if ($remainingValue > Modulwerft_CouponRemainingValue_Helper_Data::DIFF_DELTA) {
            $crvCoupon->setRemainingValue($remainingValue);

            $message = 'Rule #' . $rule->getId() . ' ("' . $rule->getName() . '") was applied to' .
                ' order #' . $order->getIncrementId() . ', remaining value has been set to '
                . $remainingValue;
        }
        // there is no remaining value left
        else {
            // avoid rounding errors
            $remainingValue = 0;

            $crvCoupon->setRemainingValue($remainingValue);

            $message = 'Rule #' . $rule->getId() . ' ("' . $rule->getName() . '") was applied to' .
                ' order #' . $order->getIncrementId() . ', no remaining value left';
        }

        /** @var Modulwerft_CouponRemainingValue_Model_History $couponHistory */
        $couponHistory = Mage::getModel('modulwerft_couponremainingvalue/history');

        $couponHistory->setOrder($order)
            ->setCouponId($crvCoupon->getId())
            ->setRule($rule)
            ->setUsedValue(abs($order->getDiscountAmount()))
            ->setLeftValue($remainingValue)
            ->setShippingIncluded($rule->getApplyToShipping());

        /** @var Mage_Core_Model_Resource_Transaction $transaction */
        $transaction = Mage::getModel('core/resource_transaction');

        $transaction->addObject($crvCoupon)
            ->addObject($couponHistory);

        try {
            $transaction->save();

            $this->_getLogHelper()->log($message, Zend_Log::DEBUG);

            $eventParams = array(
                'rule' => $rule,
                'coupon' => $crvCoupon, /** @since 1.2.0 */
                'coupon_history' => $couponHistory,
                'order' => $order,
            );
            Mage::dispatchEvent($this->_eventPrefix . '_used_coupon_value', $eventParams);
        } catch (Exception $e) {
            Mage::getSingleton('adminhtml/session')
                ->addWarning($this->_getHelper()->__('Could not use coupon value: %s', $e->getMessage()));
            $this->_getLogHelper()->logException($e);
        }

        return $this;
    }

    /**
     * @param Mage_Sales_Model_Order $order
     * @param Mage_Sales_Model_Order_Creditmemo $creditmemo
     * @return $this
     */
    public function sendNotifications(Mage_Sales_Model_Order $order,
                                      Mage_Sales_Model_Order_Creditmemo $creditmemo = null)
    {
        /** @var Modulwerft_CouponRemainingValue_Helper_Log $logHelper */
        $logHelper = Mage::helper('modulwerft_couponremainingvalue/log');

        /** @var Modulwerft_CouponRemainingValue_Helper_Data $helper */
        $helper = Mage::helper('modulwerft_couponremainingvalue');

        // lookup rule ids
        $ruleIds = $helper->getAppliedRulesOfOrder($order);
        if (empty($ruleIds)) {
            $logHelper->log('No rules applied to order #' . $order->getIncrementId(), Zend_Log::DEBUG);
            return $this;
        }

        foreach ($ruleIds as $_ruleId) {
            /** @var Mage_SalesRule_Model_Rule $_rule */
            $_rule = Mage::getModel('salesrule/rule')
                ->load($_ruleId);

            if (!$helper->usesRuleRemainingValue($_rule)) {
                $message = 'Applied rule #' . $_rule->getId() . ' ("' . $_rule->getName() . '") does not use remaining'
                    . 'value for order #' . $order->getIncrementId();
                $logHelper->log($message, Zend_Log::DEBUG);
                continue;
            }

            /** @var Modulwerft_CouponRemainingValue_Model_Resource_History_Collection $_couponHistoryCollection */
            $_couponHistoryCollection = Mage::getResourceModel('modulwerft_couponremainingvalue/history_collection');

            $_couponHistoryCollection->addRuleToFilter($_rule)
                ->addCouponCodeFilter($order->getCouponCode())
                ->addOrderToFilter($order)
                ->setOrder('used_at', Varien_Data_Collection::SORT_ORDER_DESC);

            if (0 == $_couponHistoryCollection->getSize()) {
                $message = 'Did not find any history item for rule #' . $_rule->getId();
                $logHelper->log($message, Zend_Log::ERR);
                continue;
            }

            /** @var Modulwerft_CouponRemainingValue_Model_History $_couponHistory */
            $_couponHistory = $_couponHistoryCollection->getFirstItem();

            try {
                /** @var Modulwerft_CouponRemainingValue_Model_Notification $notification */
                $notification = Mage::getModel('modulwerft_couponremainingvalue/notification');

                $notification->setOrder($order)
                    ->setRule($_rule)
                    ->setCouponHistory($_couponHistory)
                    ->setCreditmemo($creditmemo)
                    ->send();
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')
                    ->addWarning($helper->__('Could not send notification for coupon: %s', $e->getMessage()));
                $logHelper->logException($e);
            }
        }

        return $this;
    }

    /**
     * @param Mage_Sales_Model_Order $order
     * @return $this
     */
    public function redeemCoupons(Mage_Sales_Model_Order $order)
    {
        $eventParams = array('order' => $order);
        Mage::dispatchEvent($this->_eventPrefix . '_redeemed_coupon', $eventParams);
        return $this;
    }

    /**
     * @return Modulwerft_CouponRemainingValue_Helper_Data
     * @since 1.2.0
     */
    protected function _getHelper()
    {
        return Mage::helper('modulwerft_couponremainingvalue');
    }

    /**
     * @return Modulwerft_CouponRemainingValue_Helper_Log
     * @since 1.2.0
     */
    protected function _getLogHelper()
    {
        return Mage::helper('modulwerft_couponremainingvalue/log');
    }

}
