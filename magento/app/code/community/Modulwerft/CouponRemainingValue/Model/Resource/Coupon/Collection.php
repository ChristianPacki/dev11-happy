<?php

/**
 * @category Modulwerft
 * @package Modulwerft_CouponRemainingValue
 * @author Modulwerft GmbH <info@modulwerft.com>
 * @author Philipp Zabel <p.zabel@shopwerft.com>
 * @copyright 2014 Shopwerft GmbH (http://www.shopwerft.com)
 */
class Modulwerft_CouponRemainingValue_Model_Resource_Coupon_Collection
    extends Mage_Core_Model_Mysql4_Collection_Abstract
{

    /** @var string */
    protected $_eventPrefix = 'modulwerft_couponremainingvalue_coupon_collection';

    /** @var string */
    protected $_eventObject = 'coupon_collection';

    protected function _construct()
    {
        $this->_init('modulwerft_couponremainingvalue/coupon');
    }

}
