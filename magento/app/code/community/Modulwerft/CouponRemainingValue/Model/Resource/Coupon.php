<?php

/**
 * @category Modulwerft
 * @package Modulwerft_CouponRemainingValue
 * @author Modulwerft GmbH <info@modulwerft.com>
 * @author Philipp Zabel <p.zabel@shopwerft.com>
 * @copyright 2014 Shopwerft GmbH (http://www.shopwerft.com)
 */
class Modulwerft_CouponRemainingValue_Model_Resource_Coupon
    extends Mage_Core_Model_Mysql4_Abstract
{

    /** @var bool */
    protected $_isPkAutoIncrement = false;

    protected function _construct()
    {
        $this->_init('modulwerft_couponremainingvalue/coupon', 'coupon_id');
    }

}
