<?php

/**
 * @category Modulwerft
 * @package Modulwerft_CouponRemainingValue
 * @author Modulwerft GmbH <info@modulwerft.com>
 * @author Philipp Zabel <p.zabel@shopwerft.com>
 * @copyright 2014 Shopwerft GmbH (http://www.shopwerft.com)
 */
class Modulwerft_CouponRemainingValue_Model_Resource_Rule_Collection
    extends Mage_Core_Model_Mysql4_Collection_Abstract
{

    /** @var string */
    protected $_eventPrefix = 'modulwerft_couponremainingvalue_rule_collection';

    /** @var string */
    protected $_eventObject = 'rule_collection';

    protected function _construct()
    {
        $this->_init('modulwerft_couponremainingvalue/rule');
    }

    /**
     * @param Mage_SalesRule_Model_Rule $rule
     * @return $this
     */
    public function addRuleFilter(Mage_SalesRule_Model_Rule $rule)
    {
        if (!$rule->getId()) {
            Mage::throwException('Invalid rule for filter');
        }

        $this->addFieldToFilter('rule_id', $rule->getId());

        return $this;
    }

}
