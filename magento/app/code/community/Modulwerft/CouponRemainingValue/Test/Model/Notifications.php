<?php
/**
 * @category Modulwerft
 * @package Modulwerft_CouponRemainingValue
 * @author Shopwerft GmbH <werft@shopwerft.com>
 * @author Benjamin Wunderlich <b.wunderlich@shopwerft.com>
 * @copyright 2014 Shopwerft GmbH (http://www.shopwerft.com)
 * @since 1.2.2
 */

class Modulwerft_CouponRemainingValue_Test_Model_Notifications
    extends EcomDev_PHPUnit_Test_Case

{

    /**
     * @loadFixture
     */
    public function testCustomerNotificationActive()
    {
        /** @var Modulwerft_CouponRemainingValue_Helper_Config $configHelper */
        $configHelper = Mage::helper('modulwerft_couponremainingvalue/config');

        $this->assertTrue($configHelper->isCustomerNotificationActive());
    }

    /**
     * @loadFixture
     */
    public function testCustomerNotificationEmailTemplate()
    {
        /** @var Modulwerft_CouponRemainingValue_Helper_Config $configHelper */
        $configHelper = Mage::helper('modulwerft_couponremainingvalue/config');

        $this->assertEquals("99", $configHelper->getCustomerNotificationEmailTemplate());
    }

    /**
     * @loadFixture
     */
    public function testCustomerNotificationEmailBcc()
    {
        /** @var Modulwerft_CouponRemainingValue_Helper_Config $configHelper */
        $configHelper = Mage::helper('modulwerft_couponremainingvalue/config');

        $expectations = array('test@modulwerft.com', 'test2@modulwerft.com');
        $this->assertEquals($expectations, $configHelper->getCustomerNotificationEmailBcc());
    }

    /**
     * @loadFixture
     */
    public function testEmailEmptyCouponActive()
    {
        /** @var Modulwerft_CouponRemainingValue_Helper_Config $configHelper */
        $configHelper = Mage::helper('modulwerft_couponremainingvalue/config');

        $this->assertTrue($configHelper->isEmailEmptyCouponActive());
    }

    /**
     * @loadFixture
     */
    public function testEmailTemplateEmptyCoupon()
    {
        /** @var Modulwerft_CouponRemainingValue_Helper_Config $configHelper */
        $configHelper = Mage::helper('modulwerft_couponremainingvalue/config');

        $this->assertEquals("89", $configHelper->getEmailTemplateEmptyCoupon());
    }

    /**
     * @loadFixture
     */
    public function testEmailBccEmptyCoupon()
    {
        /** @var Modulwerft_CouponRemainingValue_Helper_Config $configHelper */
        $configHelper = Mage::helper('modulwerft_couponremainingvalue/config');

        $expectations = array('test@modulwerft.com', 'test2@modulwerft.com');
        $this->assertEquals($expectations, $configHelper->getEmailBccEmptyCoupon());
    }

    /**
     * @loadFixture
     */
    public function testEmailResetCouponValueActive()
    {
        /** @var Modulwerft_CouponRemainingValue_Helper_Config $configHelper */
        $configHelper = Mage::helper('modulwerft_couponremainingvalue/config');

        $this->assertTrue($configHelper->isEmailResetCouponValueActive());
    }

    /**
     * @loadFixture
     */
    public function testEmailTemplateResetCouponValue()
    {
        /** @var Modulwerft_CouponRemainingValue_Helper_Config $configHelper */
        $configHelper = Mage::helper('modulwerft_couponremainingvalue/config');

        $this->assertEquals("79", $configHelper->getEmailTemplateResetCouponValue());
    }

    /**
     * @loadFixture
     */
    public function testEmailBccResetCouponValue()
    {
        /** @var Modulwerft_CouponRemainingValue_Helper_Config $configHelper */
        $configHelper = Mage::helper('modulwerft_couponremainingvalue/config');

        $expectations = array('test@modulwerft.com', 'test2@modulwerft.com');
        $this->assertEquals($expectations, $configHelper->getEmailBccResetCouponValue());
    }

}
