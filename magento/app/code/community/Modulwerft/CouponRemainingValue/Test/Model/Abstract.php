<?php

/**
 * @category Modulwerft
 * @package Modulwerft_CouponRemainingValue
 * @author Modulwerft GmbH <info@modulwerft.com>
 * @author Philipp Zabel <p.zabel@shopwerft.com>
 * @copyright 2014 Shopwerft GmbH (http://www.shopwerft.com)
 * @since 1.2.0
 */
abstract class Modulwerft_CouponRemainingValue_Test_Model_Abstract
    extends EcomDev_PHPUnit_Test_Case
{

    /**
     * Initilialize test context.
     */
    protected function setUp()
    {
        EcomDev_PHPUnit_Model_App::applyTestScope();
        parent::setUp();
    }

    /**
     * Important for resetting values like registry for cleaning e.g. singletons.
     */
    protected function tearDown()
    {
        parent::tearDown();
        EcomDev_PHPUnit_Model_App::discardTestScope();
    }

}
