<?php
/**
 * @copyright  Copyright (c) 2009-2016 AITOC, Inc.
 */

/* @var $this Aitoc_Aitsys_Model_Mysql4_Setup */

$this->startSetup();
$this->setConfigData('aitsys/feed/install_date', time());

$this->endSetup();