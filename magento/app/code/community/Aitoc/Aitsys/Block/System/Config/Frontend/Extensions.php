<?php
/**
 * @copyright  Copyright (c) 2009-2016 AITOC, Inc.
 */

/**
 * Class Aitoc_Aitsys_Block_System_Config_Frontend_Extensions
 */
class Aitoc_Aitsys_Block_System_Config_Frontend_Extensions
    extends Mage_Adminhtml_Block_Abstract
    implements Varien_Data_Form_Element_Renderer_Interface
{
    /**
     * @param Varien_Data_Form_Element_Abstract $element
     * @return string
     */
    public function render(Varien_Data_Form_Element_Abstract $element)
    {
        $html = '<h3 style="margin:15px 0;">'
            . Mage::helper('aitsys')->__('To Enable/Disable AITOC extensions click here')
            . ' <a href="' . Mage::getModel('adminhtml/url')->getUrl('*/aitsys_index')
            . '" >' . Mage::helper('aitsys')->__('Manage Aitoc Modules') . '</a></h3>';

        return $html;
    }
}
