<?php
/**
 * @copyright  Copyright (c) 2009-2016 AITOC, Inc.
 */

/**
 * Class Aitoc_Aitsys_PatchController
 */
class Aitoc_Aitsys_PatchController extends Mage_Adminhtml_Controller_Action
{
    /**
     * Page instruction
     */
    public function instructionAction()
    {
        $this->loadLayout()
            ->_setActiveMenu('system/aitsys')
            ->_title(Mage::helper('aitsys')->__('Aitoc Modules Manager'))
            ->_title(Mage::helper('aitsys')->__('Aitoc Manual Patch Instructions'));
        $this->renderLayout();
    }

    /**
     * Index
     */
    public function indexAction()
    {
        $this->loadLayout()
            ->_setActiveMenu('system/aitsys')
            ->_title(Mage::helper('aitsys')->__('Aitoc Modules Manager'))
            ->_title(Mage::helper('aitsys')->__('Customized Templates'));
        $this->renderLayout();
    }

    /**
     * @return mixed
     */
    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('system/aitsys');
    }
}