<?php
/**
 * @copyright  Copyright (c) 2009-2016 AITOC, Inc.
 */

/**
 * Class Aitoc_Aitsys_IndexController
 */
class Aitoc_Aitsys_IndexController extends Mage_Adminhtml_Controller_Action
{
    /**
     * Page Error
     */
    public function errorAction()
    {
        $this->loadLayout()->_setActiveMenu('system/aitsys');
        $this->renderLayout();
    }

    /**
     * page Index
     */
    public function indexAction()
    {
        $this->loadLayout()
            ->_setActiveMenu('system/aitsys')
            ->_title(Mage::helper('aitsys')->__('Aitoc Modules Manager'));
        $this->renderLayout();
    }

    /**
     * Save
     */
    public function saveAction()
    {
        if ($data = $this->getRequest()->getPost('enable')) {
            if ($aErrorList = Mage::getModel('aitsys/aitsys')->saveData($data)) {
                $aModuleList = Mage::getModel('aitsys/aitsys')->getAitocModuleList();
                
                foreach ($aErrorList as $aError) {
                    $this->_getSession()->addError($aError);
                }
                
                if ($notices = Mage::getModel('aitsys/aitpatch')->getCompatiblityError($aModuleList)) {
                    foreach ($notices as $notice) {
                        $this->_getSession()->addNotice($notice);
                    }
                }
            } else {
                $this->_getSession()->addSuccess(Mage::helper('aitsys')->__('Modules\' settings saved successfully'));
            }
        }
        
        $this->_redirect('*/*');
    }

    /**
     * Permissions
     */
    public function permissionsAction()
    {
        $mode = Mage::app()->getRequest()->getParam('mode');
        
        try {
            Aitoc_Aitsys_Abstract_Service::get()->filesystem()->permissonsChange($mode);
            Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('aitsys')->__('Write permissions were changed successfully'));
        } catch (Exception $e) {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('aitsys')->__('There was an error while changing write permissions. Permissions were not changed.'));        
        }
        
        $this->_redirect('*/index');
    }

    /**
     * @return mixed
     */
    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('system/aitsys');
    }
}
