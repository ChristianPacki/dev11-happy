<?php
/**
 * @copyright  Copyright (c) 2009-2016 AITOC, Inc.
 */

/**
 * Class Aitoc_Aitsys_Helper_License
 */
class Aitoc_Aitsys_Helper_License extends Aitoc_Aitsys_Helper_Data
{
    public function uninstallBefore()
    {
    }

    public function installBefore()
    {
    }
}
