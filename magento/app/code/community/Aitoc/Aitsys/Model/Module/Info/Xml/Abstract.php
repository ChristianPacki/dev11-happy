<?php
/**
 * @copyright  Copyright (c) 2009-2016 AITOC, Inc.
 */

/**
 * Class Aitoc_Aitsys_Model_Module_Info_Xml_Abstract
 */
abstract class Aitoc_Aitsys_Model_Module_Info_Xml_Abstract extends Aitoc_Aitsys_Model_Module_Info_Abstract
{
    /**
     * @var SimpleXMLElement
     */
    protected $_xml;

    /**
     * @var string
     */
    protected $_path;

    /**
     * @var string
     */
    protected $_pathSuffix;

    /**
     * Initialization
     */
    protected function _init()
    {
        $this->_path = BP
            . DS
            . 'app'
            . DS
            . 'code'
            . DS
            . $this->getCodepool()
            . DS
            . join(DS, explode('_', $this->getModule()->getKey()))
            . DS
            . $this->_pathSuffix;
        if (file_exists($this->_path)) {
            try {
                $this->_xml = @simplexml_load_file($this->_path);
            } catch (Exception $e) {
                $e->getMessage();
            }
        }
        if ($this->_xml instanceof SimpleXMLElement) {
            $this->_loaded = true;
        }
    }

    /**
     * @param $var
     * @return null|SimpleXMLElement[]
     */
    public function __get($var)
    {
        return $this->isLoaded() ? $this->_xml->{$var} : null;
    }

    /**
     * @param $method
     * @param $args
     * @return mixed
     */
    public function __call($method, $args)
    {
        if ($this->isLoaded()) {
            try {
                return @call_user_func_array(array($this->_xml, $method), $args);
            } catch (Exception $e) {
                $e->getMessage();
            }
        }
    }
}
