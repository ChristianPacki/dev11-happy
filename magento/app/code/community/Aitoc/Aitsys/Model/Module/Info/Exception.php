<?php
/**
 * @copyright  Copyright (c) 2009-2016 AITOC, Inc.
 */
class Aitoc_Aitsys_Model_Module_Info_Exception extends Exception
{

    /**
     * Aitoc_Aitsys_Model_Module_Info_Exception constructor.
     * @param string $message
     * @param int $code
     * @param Exception $previous
     */
    public function __construct($message, $code, Exception $previous)
    {
        parent::__construct($message, $code, $previous);
    }
}