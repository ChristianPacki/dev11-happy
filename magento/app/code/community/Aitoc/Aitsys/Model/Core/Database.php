<?php
/**
 * @copyright  Copyright (c) 2009-2016 AITOC, Inc.
 */

/**
 * Class Aitoc_Aitsys_Model_Core_Database
 */
class Aitoc_Aitsys_Model_Core_Database extends Aitoc_Aitsys_Abstract_Model
{
    protected $_conn;

    /**
     * @var array()
     */
    protected $_statuses;

    /**
     * @var array()
     */
    protected $_cachedConfig;

    /**
     * @return Varien_Db_Adapter_Pdo_Mysql
     */
    protected function _connection()
    {
        if (is_null($this->_conn)) {
            if (!Mage::registry('_singleton/core/resource')) {
                $config      = $this->_config();
                $this->_conn = new Varien_Db_Adapter_Pdo_Mysql(
                    array(
                        'host' => (string)$config->global->resources->default_setup->connection->host,
                        'username' => (string)$config->global->resources->default_setup->connection->username,
                        'password' => (string)$config->global->resources->default_setup->connection->password,
                        'dbname' => (string)$config->global->resources->default_setup->connection->dbname,
                        'port' => (string)$config->global->resources->default_setup->connection->port,
                        'type' => 'pdo_mysql',
                        'model' => 'mysql4',
                        'active' => 1
                    )
                );
            } else {
                $this->_conn = Mage::getSingleton('core/resource')->getConnection('core_read');
            }
        }

        return $this->_conn;
    }

    /**
     * Get table name using magento tables' prefix
     *
     * @param string $table
     */
    protected function _table($table)
    {
        if (isset($this->_config()->global->resources->db)
            && isset($this->_config()->global->resources->db->table_prefix)) {
            return $this->_config()->global->resources->db->table_prefix . $table;
        }

        return $table;
    }

    /**
     * @return mixed|Zend_Config_Xml
     */
    protected function _config()
    {
        if (is_null($this->_localConfig)) {
            $path = BP . '/app/etc/local.xml';
            if (file_exists($path)) {
                $this->_localConfig = new Zend_Config_Xml($path);
            }
        }

        return $this->_localConfig;
    }

    /**
     * Get value from magento core_config_data table
     *
     * @param string $path
     * @param mixed $defaultValue
     */
    public function getConfigValue($path, $defaultValue = null)
    {
        // if (!$data = $this->_getCachedConfig($path)) { // removed from 2.20
        $conn   = $this->_connection();
        $select = $conn->select()
            ->from($this->_table('core_config_data'))
            ->where('path = ?', $path)
            ->where('scope = ?', 'default');
        $data   = $conn->fetchRow($select);
        if ($data === false || !isset($data['value']) || $data['value'] === '') {
            $data = $defaultValue;
        } else {
            $data = $data['value'];
        }

        $data = $this->tool()->unserialize($data);

        return $data;
    }

    /**
     * Retrieves stored modules' statuses.
     *
     * @param string $key Module key like Aitoc_Aitmodulename
     * @return array|bool
     */
    public function getStatus($key = '')
    {
        if (is_null($this->_statuses)) {
            $this->_statuses = array();
            $conn            = $this->_connection();
            $select          = $conn->select()->from($this->_table('aitsys_status'));
            $data            = $conn->fetchAll($select);

            foreach ($data as $module) {
                $this->_statuses[$module['module']] = $module['status'];
            }
        }

        if ($key) {
            return isset($this->_statuses[$key]) ? (bool)$this->_statuses[$key] : false;
        } else {
            return $this->_statuses;
        }
    }

    /**
     * Return current Aitsys db resource version
     *
     * @return string
     */
    public function dbVersion()
    {
        $conn             = $this->_connection();
        $select           = $conn->select()
            ->from($this->_table('core_resource'), 'version')
            ->where('code =?', 'aitsys_setup');
        $this->_dbVersion = $conn->fetchOne($select);

        return $this->_dbVersion;
    }
}
