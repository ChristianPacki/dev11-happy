<?php
/**
 * @copyright  Copyright (c) 2009-2016 AITOC, Inc.
 */

/**
 * Class Aitoc_Aitsys_Model_Core_Filesystem_Exception
 */
class Aitoc_Aitsys_Model_Core_Filesystem_Exception extends Mage_Core_Exception
{
    /**
     * Aitoc_Aitsys_Model_Core_Filesystem_Exception constructor.
     * @param string $message
     * @param int $code
     * @param Exception $previous
     */
    public function __construct($message, $code, Exception $previous)
    {
        parent::__construct($message, $code, $previous);
    }
}