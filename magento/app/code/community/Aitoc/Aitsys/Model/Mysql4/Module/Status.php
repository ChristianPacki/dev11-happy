<?php
/**
 * @copyright  Copyright (c) 2009-2016 AITOC, Inc.
 */
class Aitoc_Aitsys_Model_Mysql4_Module_Status extends Aitoc_Aitsys_Abstract_Mysql4
{
    /**
     * Construct
     */
    protected function _construct()
    {
        $this->_init('aitsys/status', 'id');
    }
}
