<?php
/**
 * @copyright  Copyright (c) 2009-2016 AITOC, Inc.
 */

/**
 * Class Aitoc_Aitsys_Model_News
 */
class Aitoc_Aitsys_Model_News extends Aitoc_Aitsys_Abstract_Model
{
    /**
     * Construct
     */
    protected function _construct()
    {
        $this->_init('aitsys/news');
    }
    
    /**
     * @return bool
     */
    public function isOld()
    {
        return strtotime($this->getDateAdded()) < time()-86400;
    }
}
