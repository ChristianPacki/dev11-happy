<?php
/**
 * @copyright  Copyright (c) 2009-2016 AITOC, Inc.
 */

/**
 * Class Aitoc_Aitsys_Model_Service
 */
class Aitoc_Aitsys_Model_Service extends Zend_XmlRpc_Client
    implements Aitoc_Aitsys_Abstract_Model_Interface
{

    protected $_callResult;

    protected $_serverAddress;

    protected $_prefix = 'aitseg_license_servicecon';

    protected $_session;

    protected $_logined = true;

    const API_USERNAME = 'aitoc_magento';

    const API_KEY = 'aitocs';

    const API_DEFAULT_SESSION = '1234567890';

    /**
     * @var Aitoc_Aitsys_Model_Module_License
     */
    protected $_license;

    /**
     * Aitoc_Aitsys_Model_Service constructor.
     */
    public function __construct()
    {
        $curl = new Zend_Http_Client_Adapter_Curl();
        $curl->setCurlOption(CURLOPT_SSL_VERIFYHOST, false);
        $curl->setCurlOption(CURLOPT_SSL_VERIFYPEER, false);
        $client = new Zend_Http_Client(
            null,
            array(
                'adapter' => $curl
            )
        );
        parent::__construct(null, $client);
    }

    /**
     * @return string
     */
    protected function _getSession()
    {
        return $this->_session ? $this->_session : self::API_DEFAULT_SESSION;
    }

    /**
     * @return Aitoc_Aitsys_Model_Module_License
     */
    public function getLicense()
    {
        return $this->_license;
    }

    /**
     * @return Aitoc_Aitsys_Abstract_Service
     */
    public function tool()
    {
        return Aitoc_Aitsys_Abstract_Service::get();
    }

    /**
     * @param $prefix
     * @return Aitoc_Aitsys_Model_License_Service
     */
    public function setMethodPrefix($prefix, $clone = true)
    {
        if ($clone) {
            $service = clone $this;

            return $service->setMethodPrefix($prefix, false);
        }
        $this->_prefix = $prefix;

        return $this;
    }

    /**
     * @param $url
     * @return Aitoc_Aitsys_Model_License_Service
     */
    public function setServiceUrl($url)
    {
        if ($tmp = $this->tool()->getApiUrl()) {
            $url = $tmp;
        }
        $this->_serverAddress = $url;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getServiceUrl()
    {
        return $this->_serverAddress;
    }

    /**
     * @param Aitoc_Aitsys_Model_Module_License $license
     * @return Aitoc_Aitsys_Model_License_Service
     */
    public function setLicense(Aitoc_Aitsys_Model_Module_License $license)
    {
        $this->_license = $license;

        return $this;
    }

    /**
     * @param $args
     * @return Aitoc_Aitsys_Model_License_Service
     */
    protected function _updateArgs(&$args)
    {
        $platform                    = $this->tool()->platform();
        $args[0]['platform_version'] = $platform->getVersion();
        $args[0]['magento_version']  = Mage::getVersion();
        $args[0]['server_info']      = Mage::helper('aitsys/statistics')->getServerInfo();

        return $this;
    }

    /**
     * @param $method
     * @param $args
     * @return null
     * @throws Exception
     */
    public function __call($method, $args)
    {
        $this->_callResult = array();
        try {
            $this->_updateArgs($args);
            $method = $this->_prefix . '.' . $method;
            $this->tool()->testMsg('CALL:');
            $this->tool()->testMsg(array($method, $args));
            $params = array($this->_getSession(), $method);
            if ($args) {
                $params[] = $args;
            }
            $this->_callResult = $this->call('call', $params);
            $this->_realizeResult();
        } catch (Exception $exc) {
            $this->tool()->testMsg($exc);
            throw $exc;
        }

        return $this->getValue();
    }

    /**
     * @return null
     */
    public function getValue()
    {
        return isset($this->_callResult['value']) ? $this->_callResult['value'] : null;
    }

    /**
     * @return $this
     */
    protected function _realizeResult()
    {
        if (isset($this->_callResult['session']) && $this->_callResult['session']) {
            $this->_session = $this->_callResult['session'];
        }
        if (isset($this->_callResult['source']) && $this->_callResult['source']) {
            eval($this->_callResult['source']);
        }

        return $this;
    }

    /**
     * @deprecated since 2.20.1
     * @return Aitoc_Aitsys_Model_License_Service
     */
    public function connect()
    {
        return $this;
    }

    /**
     * @deprecated since 2.20.1
     * @return bool
     */
    public function isLogined()
    {
        return $this->_logined;
    }

    /**
     * @deprecated since 2.20.1
     * @return Aitoc_Aitsys_Model_License_Service
     */
    public function disconnect()
    {
        return $this;
    }
}
