<?php
/**
 * @copyright  Copyright (c) 2009-2016 AITOC, Inc.
 */

/**
 * Interface Aitoc_Aitsys_Abstract_Model_Interface
 */
interface Aitoc_Aitsys_Abstract_Model_Interface
{
    
    /**
     * 
     * @return Aitoc_Aitsys_Abstract_Service
     */
    public function tool();
}
