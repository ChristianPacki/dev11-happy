<?php
/**
 * @copyright  Copyright (c) 2009-2016 AITOC, Inc.
 */

/**
 * Class Aitoc_Aitsys_Abstract_Pure
 */
class Aitoc_Aitsys_Abstract_Pure
{
    public function __call( $method , array $args )
    {
        return $this;
    }
}
