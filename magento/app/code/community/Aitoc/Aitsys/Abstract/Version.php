<?php
/**
 * @copyright  Copyright (c) 2009-2016 AITOC, Inc.
 */

/**
 * Class Aitoc_Aitsys_Abstract_Version
 */
class Aitoc_Aitsys_Abstract_Version extends Aitoc_Aitsys_Abstract_Model
{

    protected $_versionCompare = array();

    /**
     * @param $sourceVersion
     * @param $mageVersion
     * @return mixed
     */
    public function isMagentoVersion($sourceVersion, $mageVersion)
    {
        if (!isset($this->_versionCompare[$mageVersion][$sourceVersion])) {
            $version   = $sourceVersion;
            $directive = '=';
            if (!is_numeric(substr($version, 0, 1))) {
                $directive = is_numeric(substr($version, 1, 1)) ? substr($version, 0, 1) : substr($version, 0, 2);
                $version   = substr($version, strlen($directive));
            }
            $this->tool()->testMsg('Total directive: ' . $directive);
            $versionInfo = explode('.', $version);
            $info        = explode('.', $mageVersion);
            if (preg_match('/[+-]/', $version) || '=' == $directive) {
                $this->tool()->testMsg("Use custom");
                $this->_versionCompare[$mageVersion][$sourceVersion] =
                    self::_compareVersion($directive, $versionInfo, $info);
            } else {
                $this->tool()->testMsg("Use default");
                $this->tool()->testMsg(array($version, $mageVersion, $directive));
                $this->_versionCompare[$mageVersion][$sourceVersion] =
                    version_compare($mageVersion, $version, $directive);
                $this->tool()->testMsg(strval($this->_versionCompare[$mageVersion][$sourceVersion]));
            }
        }

        return $this->_versionCompare[$mageVersion][$sourceVersion];
    }

    /**
     * @param $directive
     * @param $version
     * @param $info
     * @return bool
     */
    static private function _compareVersion($directive, $version, $info)
    {
        foreach ($version as $index => $item) {
            $end = $index == (sizeof($version) - 1);
            switch (true) {
                case (false !== strstr($item, '+')):
                    if (!self::_compareDirective($directive, '+', (int)$item, (int)$info[$index], $end)) {
                        Aitoc_Aitsys_Abstract_Service::get()->testMsg('+');

                        return false;
                    }
                    break;
                case (false !== strstr($item, '-')):
                    if (!self::_compareDirective($directive, '-', (int)$item, (int)$info[$index], $end)) {
                        Aitoc_Aitsys_Abstract_Service::get()->testMsg('-');

                        return false;
                    }
                    break;
                default:
                    if (!self::_compareDirective($directive, '.', (int)$item, (int)$info[$index], $end)) {
                        Aitoc_Aitsys_Abstract_Service::get()->testMsg('.');

                        return false;
                    }
                    break;
            }
        }

        return true;
    }

    /**
     * @param $directive
     * @param $case
     * @param $etalon
     * @param $value
     * @param null $end
     * @return bool
     */
    static protected function _compareDirective($directive, $case, $etalon, $value, $end = null)
    {
        if (('=' != $directive) && !$end && self::_compareDirective('=', $case, $etalon, $value)) {
            return true;
        }
        switch ($directive) {
            default:
            case '=':
                self::_compareEqually($case, $etalon, $value);
                break;
            case '<':
                self::_compareLess($case, $etalon, $value);
                break;
            case '>':
                self::_compareMore($case, $etalon, $value);
                break;
            case '>=':
                self::_compareMoreEqually($case, $etalon, $value);
                break;
            case '<=':
                sself::_compareLessEqually($case, $etalon, $value);
                break;
        }
    }

    /**
     * @param $case
     * @param $value
     * @param $etalon
     * @return bool
     */
    static protected function _compareEqually($case, $value, $etalon)
    {
        switch ($case) {
            case '+':
                return $value >= $etalon;
            case '-':
                return $value < $etalon;
            default:
                return $value == $etalon;
        }

        return $value == $etalon;
    }

    /**
     * @param $case
     * @param $value
     * @param $etalon
     * @return bool
     */
    static protected function _compareLess($case, $value, $etalon)
    {
        switch ($case) {
            case '+':
                return $value < $etalon;
            case '-':
                return $value <= $etalon;
            default:
                return $value < $etalon;
        }

        return $value < $etalon;
    }

    /**
     * @param $case
     * @param $value
     * @param $etalon
     * @return bool
     */
    static protected function _compareMore($case, $value, $etalon)
    {
        switch ($case) {
            case '+':
                return $value > $etalon;
            case '-':
                return $value >= $etalon;
            default:
                return $value > $etalon;
        }

        return $value > $etalon;
    }

    /**
     * @param $case
     * @param $value
     * @param $etalon
     * @return bool
     */
    static protected function _compareMoreEqually($case, $value, $etalon)
    {
        switch ($case) {
            case '+':
                return $value >= $etalon + 1;
            case '-':
                return $value > $etalon;
            default:
                return $value >= $etalon;
        }

        return $value >= $etalon;
    }

    /**
     * @param $case
     * @param $value
     * @param $etalon
     * @return bool
     */
    static protected function _compareLessEqually($case, $value, $etalon)
    {
        switch ($case) {
            case '+':
                return $value < $etalon;
            case '-':
                return $value <= $etalon - 1;
            default:
                return $value <= $etalon;
        }

        return $value <= $etalon;
    }
}
