<?php
/**
 * Contains all helper methods needed for the Magmex License module/extension.
 * Needed also for module translations
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.magmex.com/MAGMEX_SOFTWARE_LICENSE.txt
 *
 * @copyright Copyright (c) 2015 magmex, Michael Stork, (http://www.magmex.com)
 * @license http://www.magmex.com/MAGMEX_SOFTWARE_LICENSE.txt
 * @category Magmex
 * @package Magmex_License
 * @author Michael Stork <m.stork@magmex.com>
 */
class Magmex_License_Helper_Data extends Mage_Core_Helper_Abstract
{
    /**
     * Returns the extension Id of the magmex extension to be licensed/used
     *
     * @param string $className the classname, necessary to get the extension id
     * @return string the magmex extension id
     */
    public function getExtensionId($className)
    {
        $nameArray = explode('_', $className);
        $extensionId = strtolower($nameArray[1]);
        return $extensionId;
    }

    /**
     * Returns the license domain.
     *
     * @return bool|string second level domain like e.g. besugre.com or besugre.co.uk
     */
    public function getLicenseDomain()
    {
        $licenseDomain = '';

        $httpHost = Mage::helper('core/http')->getHttpHost();
        if (preg_match('/(?P<domain>[a-z0-9][a-z0-9\-]{1,63}\.[a-z\.]{2,6})$/i', $httpHost, $matches)) {
            $licenseDomain = '*.' . $matches['domain'];
        }

        return $licenseDomain;
    }
}