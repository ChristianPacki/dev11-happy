<?php
/**
 * Magmex License abstract adminhtml controller
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.magmex.com/MAGMEX_SOFTWARE_LICENSE.txt
 *
 * @copyright Copyright (c) 2015 magmex, Michael Stork, (http://www.magmex.com)
 * @license http://www.magmex.com/MAGMEX_SOFTWARE_LICENSE.txt
 * @category Magmex
 * @package Magmex_License
 * @author Michael Stork <m.stork@magmex.com>
 */
abstract class Magmex_License_Controller_AdminhtmlController extends Mage_Adminhtml_Controller_Action
{
    /**
     * License check
     *
     * @see Mage_Adminhtml_Controller_Action::preDispatch()
     * @return
     */
    public function preDispatch()
    {
        return parent::preDispatch();
    }
    
    /**
     * License check
     *
     * @see Mage_Adminhtml_Controller_Action::_isAllowed()
     * return boolean true, if license was successfully checked, otherwise false
     */
    protected function _isAllowed()
    {
        $extensionId = Mage::helper('magmex_license')->getExtensionId(get_class($this));
        
        if (Mage::getModel('magmex_license/license')->checkLicense($extensionId) === true) {
            return parent::_isAllowed();
        } else {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('magmex_license')->__('Your license is invalid. Please contact the magmex support team on http://www.magmex.com or directly per email to service@magmex.com'));
            return false;
        }
        return parent::_isAllowed();
    }
}