<?php
/**
 * Magmex License module database installation
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.magmex.com/MAGMEX_SOFTWARE_LICENSE.txt
 *
 * @copyright Copyright (c) 2015 magmex, Michael Stork, (http://www.magmex.com)
 * @license http://www.magmex.com/MAGMEX_SOFTWARE_LICENSE.txt
 * @category Magmex
 * @package Magmex_License
 * @author Michael Stork <m.stork@magmex.com>
 */

/** @var $installer Mage_Eav_Model_Entity_Setup */
$installer = $this;
$installer->startSetup();

$installer->run("
     DROP TABLE IF EXISTS {$this->getTable('magmex_license')};
     CREATE TABLE {$this->getTable('magmex_license')} (
         `extension_id` VARCHAR(255) NOT NULL,
         `license_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
         `license_key` VARCHAR(255) NOT NULL,
         `domain` VARCHAR(255) NOT NULL,
         `last_check` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
     )
     ENGINE=InnoDB DEFAULT CHARSET=utf8;
");

$installer->endSetup();