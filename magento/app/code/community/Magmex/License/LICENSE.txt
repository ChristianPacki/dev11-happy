magmex License Agreement

This License Agreement is a legal agreement between the customer (LICENSEE) and magmex - Michael Stork (LICENSOR). The Licence Agreement covers the purchase and use of the Magento extensions (EXTENSIONS) developed by magmex same as services which are associated with these EXTENSIONS.
With the purchase of any EXTENSIONS developed by magmex and/or with ordering any service from magmex the following agreements and arrangements are binding.

(1) Copyright
The copyright of any EXTENSION, including all products parts is owned by magmex.

(2) Ownership
All EXTENSIONS are property of magmex. The LICENSEE may not claim any ownership to any of our products. The LICENSEE may not give, sell, distribute, rent, lease or lend any portion of the EXTENSION software to anyone without prior written consent from magmex. Our products are provided "as is" without warranty of any kind, either expressed or implied. It is recommended to always backup your installation prior to usage. We can't guarantee proper work of our EXTENSION with any third party extensions installed. In no event shall our juridical person be liable for any damages including, but not limited to, direct, indirect, special, incidental or consequential damages or other losses arising out of the use of or inability to use our products.

(3) General License Information
Every developed EXTENSION has special license information. These information is shown on the product detail page. 
The following license agreements apply to all EXTENSIONS:
    1. The LICENSEE may not use any part of the EXTENSION code base in whole or part in any other software or product or website.
    2. The LICENSEE may not give, sell, distribute, sub-license, rent, lease or lend any portion of the EXTENSION to anyone. 
    3. The LICENSEE may not place the EXTENSION on a server so that it is accessible via a public network such as the Internet for distribution purposes.
    4. The LICENSEE is authorized to make modifications to the EXTENSION without any agreement from magmex, except all code contained in the module "Magmex_License", which is responsible for requesting and updating the LICENSEE's Commercial License.
    5. If modifications where made to magmex EXTENSIONS, the LICENSEE is not allowed to resell the EXTENSION or parts of the EXTENSION at any time.
    6. In case magmex provides the LICENSEE free upgrades of the product , these updates are available within subscription validity period.  But magmex has no influence on the development of Magento. All EXTENSIONS are designed for the latest released version, but we can not give any guarantee that the EXTENSION will run with future versions of Magento. Please find supported versions on the product detail page.

(4) Commercial License
Each purchased license allows the installation, use and deployment of the EXTENSION on one shop domain (single domain license) without any time limitation. The possible store views within a shop domain are also included in the purchased license, assumed that the second level domains are not different. Furthermore it is allowed to use the EXTENSION on all different test and development environments (e.g. http://localhost.com).
For one single domain license the LICENSEE gets one license key, which will be sent to the LICENSEE after successful purchase via email. This license key will be online updated every three days, automatically and verified by means of https://www.magmex.com.

(5) Shipping
After a successful purchase you will receive an email with a link to your individual EXTENSION download. You can also download the EXTENSION in your personal customer account area on http://www.magmex.com.

(6) Refund
magmex EXTENSIONS are not refundable and no refund is provided for services associated with EXTENSIONS; this includes installation, support and customization services.

(7) Termination
The License Agreement remains effective until terminated. 
If the LICENSEE fails to use the EXTENSION in accordance with the License Agreement, it constitutes a breach of the agreement. In this case the customer's license to use the software will be revoked. Termination due to the violation of the License Agreement does not obligate magmex to return the LICENSEE the amount he spent for purchase of the EXTENSION.
magmex reserves the right to change or modify current terms with no prior notice.