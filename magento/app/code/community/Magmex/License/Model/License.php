<?php
/**
 * Magmex License model
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.magmex.com/MAGMEX_SOFTWARE_LICENSE.txt
 *
 * @copyright Copyright (c) 2015 magmex, Michael Stork, (http://www.magmex.com)
 * @license http://www.magmex.com/MAGMEX_SOFTWARE_LICENSE.txt
 * @category Magmex
 * @package Magmex_License
 * @author Michael Stork <m.stork@magmex.com>
 */

class Magmex_License_Model_License extends Mage_Core_Model_Abstract
{
    /**
     * Contains the magmex admin config area path prefix
     *
     * @var string
     */
    const ADMIN_CONFIG_PATH_PREFIX = 'default/magmex';
    
    /**
     * Contains the magmex store config area path prefix
     *
     * @var string
     */
    const STORE_CONFIG_PATH_PREFIX = 'magmex';
    
    /**
     * Contains the admin config node for the licence_check_url
     *
     * @var string
     */
    const ADMIN_LICENSE_CHECK_URL_CONFIG_NODE = 'default/magmex/license/license_check_url';
    
    /**
     * Contains the store config node for the licence_check_url
     *
     * @var string
     */
    const STORE_LICENSE_CHECK_URL_CONFIG_NODE = 'magmex/license/license_check_url';
    
    /**
     * Defines the license check cyle in days
     *
     * @var integer
     */
    const LICENSE_CHECK_CYCLE_DAYS = 3;
    
    /**
     * Construct
     * Inits the resource model
     *
     * @see Varien_Object::_construct()
     */
    protected function _construct()
    {
        parent::_construct();
        $this->_init('magmex_license/license');
    }
    
    
    /**
     * Checks the license for the given magmex extension
     *
     * @param string $extensionId the magmex extension id
     * @return boolean true, if license is available or license check is not necessary - otherwise false
     */
    public function checkLicense($extensionId)
    {
        if ($this->_getResource()->isLicenseCheckNecessary($this, $extensionId) === true) {
            return $this->_requestAndSaveLicenseKey($extensionId);
        }
        
        return true;
    }
    
    /**
     * Requests a license key and returns true, if successful
     *
     * @param string $extensionId contains the extension id string
     * @return boolean true, if a license key was successfully requested and saved
     */
    protected function _requestAndSaveLicenseKey($extensionId)
    {
        // create a new cURL resource
        $curlSession = @curl_init();

        // set URL and other appropriate options
        //$configValue = Mage::getConfig()->getNode('default/system/magmex/license_check_url');
        $licenseDomain = Mage::helper('magmex_license')->getLicenseDomain();

        if (empty($licenseDomain) === true) {
            return false;
        }
        
        if (Mage::app()->getStore()->isAdmin() !== false) {
            $licenseRequestUrl = Mage::getConfig()->getNode(self::ADMIN_LICENSE_CHECK_URL_CONFIG_NODE);
            $licenseKeyConfig = Mage::getConfig()->getNode(self::ADMIN_CONFIG_PATH_PREFIX . '/' . $extensionId . '/license_key');
        } else {
            $licenseRequestUrl = Mage::getConfig()->getNode(self::STORE_LICENSE_CHECK_URL_CONFIG_NODE);
            $licenseKeyConfig = Mage::getConfig()->getNode(self::STORE_CONFIG_PATH_PREFIX . '/' . $extensionId . '/license_key');
        }
        
        if (empty($licenseKeyConfig) === true) {
            return false;
        }
        
        $licenseRequestUrlComplete = $licenseRequestUrl . '?domain=' . $licenseDomain . '&licenseKey=' . $licenseKeyConfig;
        
        @curl_setopt($curlSession,CURLOPT_TIMEOUT, 30);
        @curl_setopt($curlSession, CURLOPT_URL, $licenseRequestUrlComplete);
        @curl_setopt($curlSession, CURLOPT_RETURNTRANSFER, true);
        @curl_setopt($curlSession, CURLOPT_SSL_VERIFYPEER, false);
        @curl_setopt($curlSession, CURLOPT_FOLLOWLOCATION, true);
        $curlAnswer = @curl_exec($curlSession);
        
        //close cURL resource, and free up system resources
        @curl_close($curlSession);
        
        //if magmex should be unreachable, ignore license key, if a license key was saved before
        if (@curl_getinfo($curlSession, CURLINFO_HTTP_CODE) != '200' && $this->getLicenseKey() === $licenseKeyConfig) {
            return true;
        }
        
        if ($curlAnswer == $licenseKeyConfig) {
            $this->setLicenseKey($licenseKeyConfig)
                ->setExtensionId($extensionId)
                ->setDomain($licenseDomain)
                ->setLastCheck(date('Y-m-d H:i:s', Mage::getModel('core/date')->timestamp(time())))
                ->save();
            return true;
        }
        return false;
    }
}