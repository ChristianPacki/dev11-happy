<?php
/**
 * Magmex license resource model
 * Uses Mysql4 Resource parent for compatibility reasons with Magento 1.5.*
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.magmex.com/MAGMEX_SOFTWARE_LICENSE.txt
 *
 * @copyright Copyright (c) 2015 magmex, Michael Stork, (http://www.magmex.com)
 * @license http://www.magmex.com/MAGMEX_SOFTWARE_LICENSE.txt
 * @category Magmex
 * @package Magmex_License
 * @author Michael Stork <m.stork@magmex.com>
 */

class Magmex_License_Model_Resource_License extends Mage_Core_Model_Mysql4_Abstract
{
    /**
     * Construct
     * Inits the license table
     *
     * @see Mage_Core_Model_Resource_Abstract::_construct()
     * @return void
     */
    public function _construct()
    {
        $this->_init('magmex_license/license', 'license_id');
    }
    
    /**
     * Determines, if a license check is necessary for the actual store and loads
     * the license model, if a license exists already
     *
     * @param Magmex_License_Model_License $modelLicense license model instance
     * @param string $extensionId extension id string
     * @return boolean true, if license has to be checked
     */
    public function isLicenseCheckNecessary(Magmex_License_Model_License $modelLicense, $extensionId)
    {
        
        $readAdapter = $this->_getReadAdapter();
        $select = $readAdapter->select()
            ->from($this->getMainTable())
            ->where('extension_id = ?', $extensionId)
            ->where('domain = ?', Mage::helper('magmex_license')->getLicenseDomain());
        
        $data = $readAdapter->fetchRow($select);
        
        if (!empty($data)) {
            $modelLicense->load($data['license_id']);
            $currentTimestamp = Mage::getModel('core/date')->timestamp(time());
            $lastCheckTimestamp = Mage::getModel('core/date')->timestamp($data['last_check']);
            if (($currentTimestamp - $lastCheckTimestamp) < (Magmex_License_Model_License::LICENSE_CHECK_CYCLE_DAYS * 24 * 60 * 60)) {
                return false;
            }
        }
        
        return true;
    }
}