<?php
/**
 * Magmex: collection model for main license table
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.magmex.com/MAGMEX_SOFTWARE_LICENSE.txt
 *
 * @copyright Copyright (c) 2015 magmex, Michael Stork, (http://www.magmex.com)
 * @license http://www.magmex.com/MAGMEX_SOFTWARE_LICENSE.txt
 * @category Magmex
 * @package Magmex_License
 * @author Michael Stork <m.stork@magmex.com>
 */

class Magmex_License_Model_Resource_License_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    /**
     * Construct
     * Inits the license collection
     *
     * @see Mage_Core_Model_Mysql4_Collection_Abstract::_construct()
     * @return void
     */
    public function _construct()
    {
        parent::_construct();
        $this->_init('magmex_license/license');
    }
}