<?php
/**
 * Magmex Adventcalendar for creating, deleting and editing advent calenders in the admin
 * backend
 *
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.magmex.com/MAGMEX_SOFTWARE_LICENSE.txt
 *
 * @copyright Copyright (c) 2015 magmex, Michael Stork, (http://www.magmex.com)
 * @license http://www.magmex.com/MAGMEX_SOFTWARE_LICENSE.txt
 * @category Magmex
 * @package Magmex_AdventCalendar
 * @author Michael Stork <m.stork@magmex.com>
 */
class Magmex_AdventCalendar_Adminhtml_Adventcalendar_MainController extends Magmex_License_Controller_AdminhtmlController
{

    /**
     * Init action for setting several configuration params
     *
     * @return void
     */
    protected function _initAction()
    {
        $this->loadLayout()
            ->_setActiveMenu('promo/magmex_adventcalendar')
            ->_addBreadcrumb(Mage::helper('magmex_adventcalendar')->__('Advent Calendar configuration'), Mage::helper('magmex_adventcalendar')->__('Advent Calendar configuration'));
        
        $this->_title($this->__('Advent Calendar'))->_title($this->__('Advent Calendar configuration'));
        return $this;
    }
    
    /**
     * Index action
     *
     * @return void
     */
    public function indexAction()
    {
        $this->_initAction();
        $this->_addContent($this->getLayout()->createBlock('magmex_adventcalendar/adminhtml_adventcalendar'));
        $this->renderLayout();
    }
    
    /**
     * Add new Advent Calendar - forwards to the editAction, which renders the form for adding new
     * or editing existing Advent Calenders
     *
     * @return void
     */
    public function newAction()
    {
        $this->_forward('edit');
    }
    
    /**
     * Edit an existing Advent Calendar
     *
     * @return void
     */
    public function editAction()
    {
        $adventCalendarId = $this->getRequest()->getParam('id');
        $modelAdventCalendar = Mage::getModel('magmex_adventcalendar/adventcalendar')->load($adventCalendarId);
        
        if ($modelAdventCalendar->getId() || $adventCalendarId == 0) {
            $data = Mage::getSingleton('adminhtml/session')->getFormData(true);
            if (!empty($data)) {
                $modelAdventCalendar->setData($data);
            }
            
            Mage::register('magmex_adventcalendar_data', $modelAdventCalendar);
            
            $this->loadLayout();
            $this->_setActiveMenu('promo/magmex_adventcalendar');
    
            $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
    
            $this->_addContent($this->getLayout()->createBlock('magmex_adventcalendar/adminhtml_adventcalendar_edit'));
            
            $this->renderLayout();
        } else {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('magmex_adventcalendar')->__('Wrong param was given for adding or editing a new Advent Calendar'));
            $this->_redirect('*/*/');
        }
    }
    
    /**
     * The advent calender save new action saves a new advent calendar generating all
     * necessary advent calendar days
     *
     * @return void
     */
    public function saveAction()
    {
        if ($data = $this->getRequest()->getPost()) {
            Mage::helper('magmex_adventcalendar/form')->uploadImage('customized_background_image', 'customized_background_image', $data);
           
            //load model before setting the data for having the original data later for the comparison
            //of different save types (e.g. randomize existing doors)
            $adventCalendarId = $this->getRequest()->getParam('id');
            $modelAdventCalendar = Mage::getModel('magmex_adventcalendar/adventcalendar')->load($adventCalendarId)
                ->setData($data)
                ->setId($adventCalendarId);
            //necessary for checkbox show_door_background_images
            $modelAdventCalendar->setShowDoorBackgroundImages(!empty($data['show_door_background_images']));
            
            try {
                $modelAdventCalendar->saveAdventCalendar();
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('magmex_adventcalendar')->__('Advent Calendar was successfully saved. You can now edit the Advent Calendar days'));
                Mage::getSingleton('adminhtml/session')->setFormData(false);
                
                //save and continue edit redirect
                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', array('id' => $modelAdventCalendar->getId()));
                    return;
                }
                $this->_redirect('*/adventcalendar_day/', array('id' => $modelAdventCalendar->getId()));
                return;
            } catch (Exception $exception) {
                Mage::getSingleton('adminhtml/session')->addError(Mage::helper('magmex_adventcalendar')->__('You already created an Advent Calendar for this year and this store - please select another year or store'));
                Mage::getSingleton('adminhtml/session')->setFormData($data);
                $this->_redirect('*/*/new');
                return;
            }
            
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('magmex_adventcalendar')->__('Advent Calendar could not be saved'));
            $this->_redirect('*/*/');
        }
    }
    
    /**
     * Deletes an existing advent calendar
     *
     * @return void
     */
    public function deleteAction()
    {
        if ($this->getRequest()->getParam('id') > 0) {
            try {
                $modelAdventCalendar = Mage::getModel('magmex_adventcalendar/adventcalendar');
    
                $modelAdventCalendar->setId($this->getRequest()->getParam('id'))
                    ->delete();
    
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Advent Calendar was successfully deleted'));
                $this->_redirect('*/*/');
            } catch (Exception $exception) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
            }
        }
        $this->_redirect('*/*/');
    }
    //Added by quickfix script. Take note when upgrading this module! Powered by SupportDesk (www.supportdesk.nu)
    function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('promo/magmex_adventcalendar');
    }
}