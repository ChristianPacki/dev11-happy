<?php
/**
 * Magmex Adventcalendar day controller for editing and deleting advent calendar days
 * in the admin backend
 *
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.magmex.com/MAGMEX_SOFTWARE_LICENSE.txt
 *
 * @copyright Copyright (c) 2015 magmex, Michael Stork, (http://www.magmex.com)
 * @license http://www.magmex.com/MAGMEX_SOFTWARE_LICENSE.txt
 * @category Magmex
 * @package Magmex_AdventCalendar
 * @author Michael Stork <m.stork@magmex.com>
 */
class Magmex_AdventCalendar_Adminhtml_Adventcalendar_DayController extends Magmex_License_Controller_AdminhtmlController
{

    /**
     * Init action for setting several configuration params
     *
     * @return void
     */
    protected function _initAction()
    {
        $this->loadLayout()
            ->_setActiveMenu('promo/magmex_adventcalendar')
            ->_addBreadcrumb(Mage::helper('magmex_adventcalendar')->__('Advent Calendar day configuration'), Mage::helper('magmex_adventcalendar')->__('Advent Calendar day configuration'));
        
        $this->_title($this->__('Advent Calendar'))->_title($this->__('Advent Calendar day configuration'));
        //save selected year id in session for later loading after e.g. saving a day action
        $this->_getSession()->setSelectedAdventCalendarId($this->getRequest()->getParam('id'));
        return $this;
    }
    
    /**
     * Index action
     *
     * @return void
     */
    public function indexAction()
    {
        $this->_initAction();
        $this->_addContent($this->getLayout()->createBlock('magmex_adventcalendar/adminhtml_day'));
        $this->renderLayout();
    }
    
    /**
     * Edit an existing Advent Calendar day
     *
     * @return void
     */
    public function editAction()
    {
        $dayId = $this->getRequest()->getParam('id');
        $model  = Mage::getModel('magmex_adventcalendar/day')->load($dayId);
         
        $modelDayId = $model->getId();
        if (!empty($modelDayId)) {
            $data = Mage::getSingleton('adminhtml/session')->getFormData(true);
            if (!empty($data)) {
                $model->setData($data);
            }
            
            Mage::register('magmex_adventcalendar_day_data', $model);
    
            $this->loadLayout();
            $this->_setActiveMenu('promo/magmex_adventcalendar');
    
            $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
            
            //if day action type is already set, redirect to day action controller and show day action type edit form - otherwise show day
            //edit form to select the day action
            $dayActionType = $model->getActionType();
            if (empty($dayActionType)) {
                $this->_addContent($this->getLayout()->createBlock('magmex_adventcalendar/adminhtml_day_edit'));
            } else {
                $this->_redirect('*/adventcalendar_day_action/edit', array('id' => $modelDayId));
                return;
            }
    
            $this->renderLayout();
        } else {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('magmex_adventcalendar')->__('Advent Calendar day does not exist'));
            $this->_redirect('*/*/');
        }
    }
    
    /**
     * Saves the action type to an Advent Calendar day and redirects to the action editing form.
     * The action type is saved so that it is possible to jump to the specific action type edit form
     * later also if the user cancels editing the action in this session.
     * The model has to be loaded before setting the data to compare the original data with the new setted
     * data for deleting possibly existing day action data.
     *
     * @return void
     */
    public function saveAction()
    {
        if ($data = $this->getRequest()->getPost()) {
            $model = Mage::getModel('magmex_adventcalendar/day')->load($this->getRequest()->getParam('id'));
            
            $model->setData($data)
                  ->setId($this->getRequest()->getParam('id'));
            
            try {
                $model->save();
                $this->_redirect('*/adventcalendar_day_action/edit', array('id' => $model->getId()));
                return;
            } catch ( Exception $e ) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setFormData($data);
                $this->_redirect('*/*/edit');
                return;
            }
    
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('magmex_adventcalendar')->__('Advent Calendar day action type could not be saved'));
            $this->_redirect('*/*/');
        }
    }
}