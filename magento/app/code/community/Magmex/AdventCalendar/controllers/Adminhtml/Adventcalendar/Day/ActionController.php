<?php
/**
 * Magmex Adventcalendar day action controller for editing actions for the different
 * Advent Calendar days in the admin backend
 *
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.magmex.com/MAGMEX_SOFTWARE_LICENSE.txt
 *
 * @copyright Copyright (c) 2015 magmex, Michael Stork, (http://www.magmex.com)
 * @license http://www.magmex.com/MAGMEX_SOFTWARE_LICENSE.txt
 * @category Magmex
 * @package Magmex_AdventCalendar
 * @author Michael Stork <m.stork@magmex.com>
 */
class Magmex_AdventCalendar_Adminhtml_Adventcalendar_Day_ActionController extends Magmex_License_Controller_AdminhtmlController
{

    /**
     * Init action for setting several configuration params
     *
     * @return void
     */
    protected function _initAction()
    {
        $this->loadLayout()
            ->_setActiveMenu('promo/magmex_adventcalendar')
            ->_addBreadcrumb(Mage::helper('magmex_adventcalendar')->__('Advent Calendar day action configuration'), Mage::helper('magmex_adventcalendar')->__('Advent Calendar day action configuration'));
        
        $this->_title($this->__('Advent Calendar'))->_title($this->__('Advent Calendar day action configuration'));
        return $this;
    }
    
    /**
     * Edit an Advent Calendar day action.
     * The day action has to be defined and saved in the advent calender model BEFORE editing a day action is possible
     *
     * @return void
     */
    public function editAction()
    {
        $requestDayId = $this->getRequest()->getParam('id');
        $modelDay = Mage::getModel('magmex_adventcalendar/day')->load($requestDayId);
        $dayId = $modelDay->getId();
        if (empty($dayId)) {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('magmex_adventcalendar')->__('Advent Calendar day does not exist'));
            $this->_redirect('*/adventcalendar_day');
            return;
        }
        
        $dayActionType = $modelDay->getActionType();
        
        //day action type has to be given - otherwise editing a day action for a day is not possible
        //and the user gets redirected to the day grid and an error is shown
        if (!empty($dayActionType)) {
            $modelDayAction = Mage::helper('magmex_adventcalendar')->getDayActionModel($dayActionType, $dayId);
            //$model = Mage::getModel('magmex_adventcalendar/day_action_' . $modelString)->load($modelDay->getActionTypeForeignId());
            if (empty($modelDayAction)) {
                Mage::getSingleton('adminhtml/session')->addError(Mage::helper('magmex_adventcalendar')->__('The selected Advent Calendar day action does not exist'));
                $this->_redirect('*/adventcalendar_day');
                return;
            }
            
            $data = Mage::getSingleton('adminhtml/session')->getFormData(true);
            if (!empty($data) && !empty($modelDayAction)) {
                $modelDayAction->setData($data);
            }
            
            Mage::register('magmex_adventcalendar_day_action_data', $modelDayAction);
            
            $this->loadLayout();
            $this->_setActiveMenu('promo/magmex_adventcalendar');
            
            $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
            
            $this->_addContent($this->getLayout()->createBlock('magmex_adventcalendar/adminhtml_day_action_' . Mage::helper('magmex_adventcalendar')->getDayActionNamespaceString($dayActionType) . '_edit'));
            
            $this->renderLayout();
        } else {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('magmex_adventcalendar')->__('Advent Calendar day action was not yet configured for day') . ' ' . $modelDay->getDate());
            $this->_redirect('*/adventcalendar_day');
        }
    }
    
    /**
     * Saves the day actions data for all different day actions.
     * Models use the beforeSave method to do all the complicated stuff they need to do.
     *
     * @return void
     */
    public function saveAction()
    {
        if ($data = $this->getRequest()->getPost()) {
            $dayActionType = $data['action_type'];
            if (empty($dayActionType)) {
                Mage::getSingleton('adminhtml/session')->addError(Mage::helper('magmex_adventcalendar')->__('Advent Calendar day action type was not set by day action form'));
                $this->_redirect('*/adventcalendar_main/');
                return;
            }
            
            $adventCalendarDayId = $data['advent_calendar_day_id'];
            $modelDayAction = Mage::helper('magmex_adventcalendar')->getDayActionModel($dayActionType, $adventCalendarDayId);
            unset($data['action_type']);
            
            if (empty($modelDayAction)) {
                Mage::getSingleton('adminhtml/session')->addError(Mage::helper('magmex_adventcalendar')->__('The selected Advent Calendar day action does not exist'));
                $this->_redirect('*/adventcalendar_day/', array('id' => $this->_getSession()->getSelectedAdventCalendarId()));
                return;
            }
            
            try {
                Mage::helper('magmex_adventcalendar/form')->uploadImage('image', 'day_action_' . $dayActionType, $data);
                //we don't have the id as request param, because only the adventCalendarDayId is contained in the request
                $modelDayActionId = $modelDayAction->getId();
                $modelDayAction->setData($data)
                    ->setId($modelDayActionId);
                
                $modelDayAction->save();
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('magmex_adventcalendar')->__('Advent Calendar day action was successfully saved'));
                Mage::getSingleton('adminhtml/session')->setFormData(false);
                
                //save and continue edit redirect
                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', array('id' => $adventCalendarDayId));
                    return;
                }
                
                $this->_redirect('*/adventcalendar_day/', array('id' => $this->_getSession()->getSelectedAdventCalendarId()));
                return;
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setFormData($data);
                $this->_redirect('*/*/edit', array('id' => $modelDayAction->getAdventCalendarDayId()));
                return;
            }
        
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('magmex_adventcalendar')->__('Advent Calendar day action could not be saved'));
            $this->_redirect('*/*/');
        }
    }
    //Added by quickfix script. Take note when upgrading this module! Powered by SupportDesk (www.supportdesk.nu)
    function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('promo/magmex_adventcalendar');
    }
}