<?php
/**
 * Magmex Advent Calendar day action Ajax controller to load single day actions
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.magmex.com/MAGMEX_SOFTWARE_LICENSE.txt
 *
 * @copyright Copyright (c) 2015 magmex, Michael Stork, (http://www.magmex.com)
 * @license http://www.magmex.com/MAGMEX_SOFTWARE_LICENSE.txt
 * @category Magmex
 * @package Magmex_AdventCalendar
 * @author Michael Stork <m.stork@magmex.com>
 */
class Magmex_AdventCalendar_DayactionajaxController extends Mage_Core_Controller_Front_Action
{
    /**
     * Sets a JSON object/string containing the response of the request
     *
     * @return void
     */
    public function getdayAction()
    {
        $responseArray = array();

        $dayId = $this->getRequest()->getParam('dayId');
        if (empty($dayId)) {
            return;
        }
        /** @var Magmex_AdventCalendar_Model_Day $adventCalendarDay */
        $adventCalendarDay = Mage::getModel('magmex_adventcalendar/day')->load($dayId)
            ->addDayActionData();

        if ($adventCalendarDay->isEmpty()) {
            return;
        }

        //advent calendar day and day action data gets the block data
        $blockData = $adventCalendarDay->getData();

        switch($adventCalendarDay->getActionType()) {
            case Magmex_AdventCalendar_Model_Day_Action_Cmsblock::ACTION_TYPE:
            case Magmex_AdventCalendar_Model_Day_Action_Coupon::ACTION_TYPE:
                //add coupon and rule data to blockData - merge blockData at last, because of possible
                //overwrites because of the same attribute names
                if (version_compare(Mage::getVersion(), '1.5.0.0', '<')) {
                    $modelRule = Mage::getModel('salesrule/rule')->load($adventCalendarDay->getCouponId());
                    //coupon model does not exist in version 1.4, so it is faked right here
                    $modelCouponData = array('code' => $modelRule->getCouponCode());
                } else {
                    $modelCoupon = Mage::getModel('salesrule/coupon')->load($adventCalendarDay->getCouponId());
                    $modelRule = Mage::getModel('salesrule/rule')->load($modelCoupon->getRuleId());
                    $modelCouponData = $modelCoupon->getData();
                }

                $couponCodeColor = array(
                    'coupon_code_color' => Mage::getModel('magmex_adventcalendar/adventcalendar')->loadAdventCalendarByDayId($adventCalendarDay->getId())
                            ->getMainColor()
                );

                $blockData = array_merge($modelCouponData, $modelRule->getData(), $couponCodeColor, $blockData);

            case Magmex_AdventCalendar_Model_Day_Action_Teaserlink::ACTION_TYPE:
                $blockDayAction = $this->getLayout()->createBlock('core/template');
                foreach ($blockData as $name => $value) {
                    $blockDayAction->setData($name, $value);
                }
                $responseArray['html'] = $blockDayAction->setTemplate('magmex/adventcalendar/day/action/' . $adventCalendarDay->getActionType() . '.phtml')
                    ->toHtml();
                break;
            case Magmex_AdventCalendar_Model_Day_Action_Productdiscount::ACTION_TYPE:
                $product = Mage::getModel('catalog/product')->loadByAttribute('sku', $adventCalendarDay->getProductSku());

                //the referer is needed for Magento installations, which have no redirect to cart configured
                //in this case, the ajax request for a day action is set as referer and the cart would redirect to the day action ajax url
                if($this->getRequest()->getServer('HTTP_REFERER') == '' || $this->getRequest()->getServer('HTTP_REFERER') == '/') {
                    $returnUrl = Mage::getBaseUrl();
                } else {
                    $returnUrl = $this->getRequest()->getServer('HTTP_REFERER');
                }

                $addToCartUrlArray = array(
                    'product_discount_add_to_cart_url' => Mage::helper('checkout/cart')->getAddUrl($product, array('_query' => array('return_url' => $returnUrl)))
                );

                $blockData = array_merge(array('product' => $product), $addToCartUrlArray, $blockData);

                $responseArray['html'] = $this->getLayout()->createBlock('catalog/product_list')
                    ->setData($blockData)
                    ->setTemplate('magmex/adventcalendar/day/action/' . $adventCalendarDay->getActionType() . '.phtml')
                    ->toHtml();
                break;
            default:
                //do nothing
                break;
        }

        $this->getResponse()->setBody(Zend_Json::encode($responseArray));
    }
}