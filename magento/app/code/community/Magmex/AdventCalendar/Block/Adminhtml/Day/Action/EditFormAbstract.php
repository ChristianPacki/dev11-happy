<?php
/**
 * Magmex: Abstract day action edit form class for all day action edit forms.
 * Adds the saved advent calender day data to the beginning of every day action form.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.magmex.com/MAGMEX_SOFTWARE_LICENSE.txt
 *
 * @copyright Copyright (c) 2015 magmex, Michael Stork, (http://www.magmex.com)
 * @license http://www.magmex.com/MAGMEX_SOFTWARE_LICENSE.txt
 * @category Magmex
 * @package Magmex_AdventCalendar
 * @author Michael Stork <m.stork@magmex.com>
 */
abstract class Magmex_AdventCalendar_Block_Adminhtml_Day_Action_EditFormAbstract extends Mage_Adminhtml_Block_Widget_Form
{
    /**
     * Defines the fieldset ID for all day action edit form blocks
     *
     * @var string
     */
    const FIELDSET_ID = 'magmex_adventcalendar_form_edit_day_action';
    
    /**
     * Adds the Advent Calendar day data (date and action_type) to the beginning of the defined form in child classes.
     * Does nothing (just returns the parent _prepareForm()), if $this->_form is not instantiated yet.
     *
     * @return Magmex_AdventCalendar_Block_Adminhtml_Adventcalendar_Edit_Form chaining
     */
    protected function _prepareForm()
    {
        if (empty($this->_form)) {
            return parent::_prepareForm();
        }
        $adventCalenderDayId = $this->getRequest()->getParam('id');
        $helperData = Mage::helper('magmex_adventcalendar');
        
        $dateFormat = Mage::app()->getLocale()->getDateFormat(
                Mage_Core_Model_Locale::FORMAT_TYPE_MEDIUM
        );
        
        $fieldset = $this->_form->getElement(self::FIELDSET_ID);
        
        //add advent calendar day id as hidden field for saving the day action later in the ActionController->saveAction
        $fieldset->addField('advent_calendar_day_id', 'hidden',
            array(
                'name' => 'advent_calendar_day_id',
                'value' => $adventCalenderDayId
            )
        );
        
        $fieldset->addField('date', 'date',
            array(
                'name' => 'date',
                'label' => $helperData->__('Date'),
                'title' => $helperData->__('Date advent calendar day'),
                'image' => $this->getSkinUrl('images/grid-cal.gif'),
                'format' => $dateFormat,
                'disabled' => true
            ),
            '^'
        );
        
        $onChangeSubmitUrl = $this->getUrl('*/adventcalendar_day/save', array('id' => $adventCalenderDayId));
        $formDayActionOnChangeJavaScript = Mage::helper('magmex_adventcalendar/form')->getActionTypeOnChangeJavaScript($onChangeSubmitUrl);
        
        $fieldset->addField('action_type', 'select',
            array(
                'name'  => 'action_type',
                'label' => $helperData->__('Action type'),
                'values'=> Mage::helper('magmex_adventcalendar/form')->getDayActionTypeOptions(false),
                'onchange' => $formDayActionOnChangeJavaScript,
                //'onchange' => 'editForm.submitUrl = \'' . $formActionUrlDayActionTypeChange . '\'; editForm.submit()',
                'required' => true,
                'note' => '<font style="color: red">' . $helperData->__('WARNING') . '</font>:' . $helperData->__('If you change the Adventcalendar day action type, all '
                                                                 . 'configured data for the action type will be deleted')
            ),
            'date'
        );
        
        //date value and action type have to be set after setting all values with setValues(), because setValues() sets null for all form element ids
        //that are not contained in the given array
        $modelDay = Mage::getModel('magmex_adventcalendar/day')->load($adventCalenderDayId);
        $this->_form->getElement('date')->setValue($modelDay->getDate());
        $this->_form->getElement('action_type')->setValue($modelDay->getActionType());
        
        return parent::_prepareForm();
    }
}