<?php
/**
 * Magmex: Edit form for editing the day action teaser link
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.magmex.com/MAGMEX_SOFTWARE_LICENSE.txt
 *
 * @copyright Copyright (c) 2015 magmex, Michael Stork, (http://www.magmex.com)
 * @license http://www.magmex.com/MAGMEX_SOFTWARE_LICENSE.txt
 * @category Magmex
 * @package Magmex_AdventCalendar
 * @author Michael Stork <m.stork@magmex.com>
 */
class Magmex_AdventCalendar_Block_Adminhtml_Day_Action_Teaserlink_Edit_Form extends Magmex_AdventCalendar_Block_Adminhtml_Day_Action_EditFormAbstract
{
    /**
     * Prepares the form inclusive all form fields, the form action and the form id
     *
     * @return Magmex_AdventCalendar_Block_Adminhtml_Adventcalendar_Edit_Form chaining
     */
    protected function _prepareForm()
    {
        $adventCalenderDayId = $this->getRequest()->getParam('id');
        $helperData = Mage::helper('magmex_adventcalendar');
        
        
        $form = new Varien_Data_Form(array(
                'id' => 'edit_form',
                'action' => $this->getUrl('*/*/save', array('id' => $adventCalenderDayId)),
                'method' => 'post',
                'enctype' => 'multipart/form-data'
            )
        );
        
        $fieldset = $form->addFieldset(self::FIELDSET_ID,
            array(
                'legend' => $this->__('Edit Advent Calendar day with action Teaser Link')
            )
        );
        
        $fieldset->addField('image', 'image',
            array(
                'name'  => 'image',
                'label' => $this->__('Image'),
                'required' => true,
                'note' => $helperData->__('Please choose an image background image in the size of 410px x 410px with the format JPG or PNG')
            )
        );
        
        $fieldset->addField('link', 'text',
                array(
                        'name'  => 'link',
                        'label' => $this->__('Link'),
                        'required' => true,
                        'note' => Mage::helper('magmex_adventcalendar')->__('Absolute link, e.g. http://www.example.com/some_path')
                        #'afterElementHtml' => Mage::helper('adminhtml/form_wysiwyg')->getAfterElementHtml()
                )
        );
        
        //set existant values in form
        $modelDayAction = Mage::registry('magmex_adventcalendar_day_action_data');
        $form->setValues($modelDayAction->getData());
        
        $form->setUseContainer(true);
        $this->setForm($form);
        return parent::_prepareForm();
    }
}