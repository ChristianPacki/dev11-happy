<?php
/**
 * Magmex: Edit form for editing the day action coupon
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.magmex.com/MAGMEX_SOFTWARE_LICENSE.txt
 *
 * @copyright Copyright (c) 2015 magmex, Michael Stork, (http://www.magmex.com)
 * @license http://www.magmex.com/MAGMEX_SOFTWARE_LICENSE.txt
 * @category Magmex
 * @package Magmex_AdventCalendar
 * @author Michael Stork <m.stork@magmex.com>
 */
class Magmex_AdventCalendar_Block_Adminhtml_Day_Action_Coupon_Edit_Form extends Magmex_AdventCalendar_Block_Adminhtml_Day_Action_EditFormAbstract
{
    /**
     * Prepares the form inclusive all form fields, the form action and the form id
     *
     * @return Magmex_AdventCalendar_Block_Adminhtml_Adventcalendar_Edit_Form chaining
     */
    protected function _prepareForm()
    {
        $adventCalenderDayId = $this->getRequest()->getParam('id');
        $helperData = Mage::helper('magmex_adventcalendar');
        
        
        $form = new Varien_Data_Form(array(
                'id' => 'edit_form',
                'action' => $this->getUrl('*/*/save', array('id' => $adventCalenderDayId)),
                'method' => 'post',
                'enctype' => 'multipart/form-data'
            )
        );
        
        $fieldset = $form->addFieldset(self::FIELDSET_ID,
            array(
                'legend' => $this->__('Edit Advent Calendar day with action Coupon')
            )
        );
        
        $fieldset->addField('headline', 'text',
            array(
                'name'  => 'headline',
                'label' => $this->__('Headline'),
                'required' => true
            )
        );
        
        $fieldset->addField('image', 'image',
            array(
                'name'  => 'image',
                'label' => $this->__('Image'),
                'required' => false,
                'note' => $this->__('Please choose an image background image in the size of 410px x 150px with the format JPG or PNG. '
                                  . 'If you selected an image, the description text will not be shown.')
            )
        );
        
        $fieldset->addField('description', 'textarea',
            array(
                'name'  => 'description',
                'label' => $this->__('Coupon description'),
                'required' => false,
                'note' => $this->__('The description text is only shown if no image was selected')
                    
            )
        );
        
        $fieldset->addField('coupon_id', 'select',
            array(
                'name'  => 'coupon_id',
                'label' => $this->__('Coupon'),
                'values'=> Mage::helper('magmex_adventcalendar/form')->getCouponCodeOptionArray(),
                'required' => true
            )
        );
        
        $fieldset->addField('right_text', 'text',
            array(
                'name'  => 'right_text',
                'label' => $this->__('Rights text (*-text)'),
                'required' => false,
                'note' => $this->__('The *-text is shown at the bottom of the layer to e.g. show limitations of the coupon')
            )
        );
        
        //set existant values in form
        $modelDayAction = Mage::registry('magmex_adventcalendar_day_action_data');
        $form->setValues($modelDayAction->getData());
        
        $form->setUseContainer(true);
        $this->setForm($form);
        return parent::_prepareForm();
    }
}