<?php
/**
 * Magmex: Edit form for editing Advent Calendars days
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.magmex.com/MAGMEX_SOFTWARE_LICENSE.txt
 *
 * @copyright Copyright (c) 2015 magmex, Michael Stork, (http://www.magmex.com)
 * @license http://www.magmex.com/MAGMEX_SOFTWARE_LICENSE.txt
 * @category Magmex
 * @package Magmex_AdventCalendar
 * @author Michael Stork <m.stork@magmex.com>
 */
class Magmex_AdventCalendar_Block_Adminhtml_Day_Edit_Form extends Mage_Adminhtml_Block_Widget_Form
{
    /**
     * Prepares the form inclusive all form fields, the form action and the form id
     *
     * @return Magmex_AdventCalendar_Block_Adminhtml_Adventcalendar_Edit_Form chaining
     */
    protected function _prepareForm()
    {
        $dayId = $this->getRequest()->getParam('id');
        $modelDay = Mage::registry('magmex_adventcalendar_day_data');
        
        $helperData = Mage::helper('magmex_adventcalendar');
        
        $form = new Varien_Data_Form(
            array(
                'id' => 'edit_form',
                'action' => $this->getUrl('*/*/save', array('id' => $dayId)),
                'method' => 'post',
                'enctype' => 'multipart/form-data'
            )
        );
        
        $fieldset = $form->addFieldset('magmex_adventcalendar_form_edit_day',
            array(
                'legend' => $this->__('Edit Advent Calendar day')
            )
        );
        
        $dateFormat = Mage::app()->getLocale()->getDateFormat(
            Mage_Core_Model_Locale::FORMAT_TYPE_MEDIUM
        );
        
        $fieldset->addField('date', 'date',
            array(
                'name' => 'date',
                'label' => $helperData->__('Date'),
                'title' => $helperData->__('Date advent calendar day'),
                'image' => $this->getSkinUrl('images/grid-cal.gif'),
                'format' => $dateFormat,
                'disabled' => true
            )
        );
        
        $fieldset->addField('action_type', 'select',
            array(
                'name'  => 'action_type',
                'label' => $helperData->__('Action'),
                'values' => Mage::helper('magmex_adventcalendar/form')->getDayActionTypeOptions(),
                'required' => true
            )
        );
        
        //set existant values in form
        $form->setValues($modelDay->getData());
        $form->setUseContainer(true);
        $this->setForm($form);
        return parent::_prepareForm();
    }
}