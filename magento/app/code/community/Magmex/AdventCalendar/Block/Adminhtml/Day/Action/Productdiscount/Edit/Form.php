<?php
/**
 * Magmex: Edit form for editing the day action product discount
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.magmex.com/MAGMEX_SOFTWARE_LICENSE.txt
 *
 * @copyright Copyright (c) 2015 magmex, Michael Stork, (http://www.magmex.com)
 * @license http://www.magmex.com/MAGMEX_SOFTWARE_LICENSE.txt
 * @category Magmex
 * @package Magmex_AdventCalendar
 * @author Michael Stork <m.stork@magmex.com>
 */
class Magmex_AdventCalendar_Block_Adminhtml_Day_Action_Productdiscount_Edit_Form extends Magmex_AdventCalendar_Block_Adminhtml_Day_Action_EditFormAbstract
{
    /**
     * Prepares the form inclusive all form fields, the form action and the form id
     *
     * @return Magmex_AdventCalendar_Block_Adminhtml_Adventcalendar_Edit_Form chaining
     */
    protected function _prepareForm()
    {
        $adventCalenderDayId = $this->getRequest()->getParam('id');
        $helperData = Mage::helper('magmex_adventcalendar');
        
        
        $form = new Varien_Data_Form(array(
                'id' => 'edit_form',
                'action' => $this->getUrl('*/*/save', array('id' => $adventCalenderDayId)),
                'method' => 'post',
                'enctype' => 'multipart/form-data'
            )
        );
        
        $fieldset = $form->addFieldset(self::FIELDSET_ID,
            array(
                'legend' => $this->__('Edit Advent Calendar day with action Product Discount')
            )
        );
        
        $fieldset->addField('headline', 'text',
            array(
                'name'  => 'headline',
                'label' => $this->__('Headline'),
                'required' => false
            )
        );
        
        $fieldset->addField('product_sku', 'text',
            array(
                'name'  => 'product_sku',
                'label' => $this->__('Product SKU'),
                'required' => true,
                'note' => $helperData->__('You are just allowed to choose store specific product SKUs for the defined store view in the Advent Calendar Configuration.')
            )
        );
        
        $fieldset->addField('discount_price_or_percent', 'text',
            array(
                'name'  => 'discount_price_or_percent',
                'label' => $this->__('Discount price or percent'),
                'required' => true,
                'class' => 'validate-zero-or-greater',
                'note' => $helperData->__('IMPORTANT: The discount can be a fix amount (price) or a percentage - it depends on the selected product type')
            )
        );
        
        //set existant values in form
        $modelDayAction = Mage::registry('magmex_adventcalendar_day_action_data');
        $form->setValues($modelDayAction->getData());
        
        $form->setUseContainer(true);
        $this->setForm($form);
        return parent::_prepareForm();
    }
}