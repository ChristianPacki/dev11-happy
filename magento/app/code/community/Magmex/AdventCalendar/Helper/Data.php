<?php
/**
 * Contains all helper methods needed for the Magmex Adventcalendar module/extension.
 * Needed also for module translations and admin backend.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.magmex.com/MAGMEX_SOFTWARE_LICENSE.txt
 *
 * @copyright Copyright (c) 2015 magmex, Michael Stork, (http://www.magmex.com)
 * @license http://www.magmex.com/MAGMEX_SOFTWARE_LICENSE.txt
 * @category Magmex
 * @package Magmex_AdventCalendar
 * @author Michael Stork <m.stork@magmex.com>
 */
class Magmex_AdventCalendar_Helper_Data extends Mage_Core_Helper_Abstract
{
    /**
     * Defines the the background image path for standard background images
     *
     * @var unknown_type
     */
    const SKIN_DOOR_BACKGROUND_IMAGE_SPRITE_PATH = 'skin/frontend/base/default/images/magmex/adventcalendar/sprites';
    
    /**
     * Returns a day action namespace string for getting a day action model or block
     *
     * @param string $dayActionType day action type string (normally database value string)
     * @return string cleaned day action type string
     */
    public function getDayActionNamespaceString($dayActionType)
    {
        return strtolower(str_replace('_', '', $dayActionType));
    }
    
    /**
     * Returns a Advent Calendard day action model instance
     *
     * @param string $dayActionType day action type string (normally database value string)
     * @param mixed $dayId boolean|integer Default: false, if an integer value is given, this value is used as model id and the model gets loaded
     * @return mixed Mage_Core_Model_Abstract|boolean model instance, if a correct dayActionType was given/model is available, otherwise false
     */
    public function getDayActionModel($dayActionType, $dayId = false)
    {
        try {
            $dayActionNamespaceString = $this->getDayActionNamespaceString($dayActionType);
            $modelDayAction = Mage::getModel('magmex_adventcalendar/day_action_' . $dayActionNamespaceString);
            
            if ($dayId !== false) {
                $modelDayActionCollection = $modelDayAction->getCollection()
                    ->addFieldToFilter('advent_calendar_day_id', $dayId);
                return $modelDayActionCollection->getFirstItem();
            }
            return $modelDayAction;
        } catch (Exception $exception) {
            return false;
        }
    }
}