<?php
/**
 * Magmex AdventCalendar module database installation
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.magmex.com/MAGMEX_SOFTWARE_LICENSE.txt
 *
 * @copyright Copyright (c) 2015 magmex, Michael Stork, (http://www.magmex.com)
 * @license http://www.magmex.com/MAGMEX_SOFTWARE_LICENSE.txt
 * @category Magmex
 * @package Magmex_AdventCalendar
 * @author Michael Stork <m.stork@magmex.com>
 */

/** @var $installer Mage_Eav_Model_Entity_Setup */
$installer = $this;
$installer->startSetup();

//create the advent calendar main table
$installer->run("
     DROP TABLE IF EXISTS {$this->getTable('magmex_advent_calendar')};
     CREATE TABLE {$this->getTable('magmex_advent_calendar')} (
         `advent_calendar_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
         `year` YEAR(4) UNSIGNED NOT NULL,
         `store_id` INT(11) UNSIGNED NOT NULL,
         `title` VARCHAR(255),
         `standard_background_image` VARCHAR(255) NOT NULL,
         `customized_background_image` VARCHAR(255),
         `layout_type` VARCHAR(255) NOT NULL,
         `show_door_background_images` TINYINT(1) NOT NULL,
         `main_color` VARCHAR(255) NOT NULL,
         `door_color_one` VARCHAR(255) NOT NULL,
         `door_color_two` VARCHAR(255) NOT NULL,
         `door_color_three` VARCHAR(255) NOT NULL,
         `door_color_four` VARCHAR(255) NOT NULL,
         `door_color_five` VARCHAR(255) NOT NULL,
         UNIQUE KEY `year_and_store_id` (`year`, `store_id`)
     )
     ENGINE=InnoDB DEFAULT CHARSET=utf8;
");

//create the advent calendar day table
$installer->run("
    DROP TABLE IF EXISTS {$this->getTable('magmex_advent_calendar_day')};
    CREATE TABLE {$this->getTable('magmex_advent_calendar_day')} (
        `advent_calendar_day_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
        `advent_calendar_id` INT(11) UNSIGNED NOT NULL,
        `date` DATE NOT NULL,
        `action_type` VARCHAR(255),
        `sort_order` TINYINT(4) NOT NULL,
        `background_color` VARCHAR(255) NOT NULL,
        `background_image_position` TINYINT(4),
        CONSTRAINT `FK_advent_calendar_id` FOREIGN KEY (`advent_calendar_id`) REFERENCES `{$this->getTable('magmex_advent_calendar')}` (`advent_calendar_id`)  ON DELETE CASCADE ON UPDATE CASCADE
    )
    ENGINE=InnoDB DEFAULT CHARSET=utf8;
");

$installer->run("
        DROP TABLE IF EXISTS {$this->getTable('magmex_advent_calendar_day_action_product_discount')};
        CREATE TABLE {$this->getTable('magmex_advent_calendar_day_action_product_discount')} (
        `advent_calendar_day_action_product_discount_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
        `advent_calendar_day_id` INT(11) UNSIGNED NOT NULL,
        `headline` VARCHAR(255),
        `product_sku` VARCHAR(64) NOT NULL,
        `discount_price_or_percent` VARCHAR(255) NOT NULL,
        CONSTRAINT `FK_day_action_product_discount_advent_calendar_day_id` FOREIGN KEY (`advent_calendar_day_id`) REFERENCES `{$this->getTable('magmex_advent_calendar_day')}` (`advent_calendar_day_id`)  ON DELETE CASCADE ON UPDATE CASCADE
    )
    ENGINE=InnoDB DEFAULT CHARSET=utf8;
");

//create the advent calendar teaser link table (one possible advent calendar action)
$installer->run("
    DROP TABLE IF EXISTS {$this->getTable('magmex_advent_calendar_day_action_teaser_link')};
    CREATE TABLE {$this->getTable('magmex_advent_calendar_day_action_teaser_link')} (
        `advent_calendar_day_action_teaser_link_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
        `advent_calendar_day_id` INT(11) UNSIGNED NOT NULL,
        `image` VARCHAR(255) NOT NULL,
        `link` VARCHAR(255) NOT NULL,
        CONSTRAINT `FK_day_action_teaser_link_advent_calendar_day_id` FOREIGN KEY (`advent_calendar_day_id`) REFERENCES `{$this->getTable('magmex_advent_calendar_day')}` (`advent_calendar_day_id`)  ON DELETE CASCADE ON UPDATE CASCADE
    )
    ENGINE=InnoDB DEFAULT CHARSET=utf8;
");

//create the advent calendar coupon table (one possible advent calendar action)
$installer->run("
   DROP TABLE IF EXISTS {$this->getTable('magmex_advent_calendar_day_action_coupon')};
   CREATE TABLE {$this->getTable('magmex_advent_calendar_day_action_coupon')} (
        `advent_calendar_day_action_coupon_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
        `advent_calendar_day_id` INT(11) UNSIGNED NOT NULL,
        `headline` VARCHAR(255) NOT NULL,
        `image` VARCHAR(255),
        `description` TEXT,
        `coupon_id` INT(11) NOT NULL,
        `right_text` TEXT,
        CONSTRAINT `FK_day_action_coupon_advent_calendar_day_id` FOREIGN KEY (`advent_calendar_day_id`) REFERENCES `{$this->getTable('magmex_advent_calendar_day')}` (`advent_calendar_day_id`)  ON DELETE CASCADE ON UPDATE CASCADE
    )
    ENGINE=InnoDB DEFAULT CHARSET=utf8;
");

//create the advent calendar cms block table (one possible advent calendar action)
$installer->run("
    DROP TABLE IF EXISTS {$this->getTable('magmex_advent_calendar_day_action_cms_block')};
    CREATE TABLE {$this->getTable('magmex_advent_calendar_day_action_cms_block')} (
        `advent_calendar_day_action_cms_block_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
        `advent_calendar_day_id` INT(11) UNSIGNED NOT NULL,
        `cms_block_id` INT(11) NOT NULL,
        CONSTRAINT `FK_day_action_cms_block_advent_calendar_day_id` FOREIGN KEY (`advent_calendar_day_id`) REFERENCES `{$this->getTable('magmex_advent_calendar_day')}` (`advent_calendar_day_id`)  ON DELETE CASCADE ON UPDATE CASCADE
    )
    ENGINE=InnoDB DEFAULT CHARSET=utf8;
");

$installer->endSetup();