<?php
/**
 * Magmex Adventcalendar image model.
 * Able to resize images, cache them and so on.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.magmex.com/MAGMEX_SOFTWARE_LICENSE.txt
 *
 * @copyright Copyright (c) 2015 magmex, Michael Stork, (http://www.magmex.com)
 * @license http://www.magmex.com/MAGMEX_SOFTWARE_LICENSE.txt
 * @category Magmex
 * @package Magmex_AdventCalendar
 * @author Michael Stork <m.stork@magmex.com>
 */
class Magmex_AdventCalendar_Model_Image
{
    /**
     * Contains the base background image url
     *
     * @var string
     */
    const BASE_IMAGE_RESIZE_URL = 'magmex_adventcalendar/image/index/';

    /**
     * Defines a JPEG image which gets loaded/cached
     *
     * @var integer
     */
    const IMAGE_TYPE_JPEG = 'jpeg';

    /**
     * Defines a PNG image which gets loaded/cached
     *
     * @var integer
     */
    const IMAGE_TYPE_PNG = 'png';

    /**
     * Contains the absoluteImageUrl that should be loaded
     *
     * @var string
     */
    protected $_absoluteImageUrl;

    /**
     * Contains the absoluteImageUrl that should be loaded
     *
     * @var string
     */
    protected $_imageType;

    /**
     * Contains the image instance
     *
     * @var Varien_Image
     */
    protected $_image;

    /**
     * 1. Checks if image is already cached and existant - if yes image is returned and displayed
     * 2. Otherwise the image is opened, cached and displayed
     *
     * @param string $absoluteImageUrl absolute image URL
     * @param integer $width image width (in pixel)
     */
    public function displayImage($absoluteImageUrl, $width)
    {
        $this->_setAbsoluteImageUrl($absoluteImageUrl);
        $cacheFilename = $this->_getCacheFilename($this->_absoluteImageUrl, $width);

        if (file_exists($cacheFilename)) {
            //don't use Varien_Image, because method display() destroys the image quality
            $this->_outputImage($cacheFilename);
        } else {
            $localFilename = $this->_getLocalFilename($this->_absoluteImageUrl);
            $this->_image = new Varien_Image($localFilename);
            $this->_cacheImage($cacheFilename, $width);
            $this->_outputImage($cacheFilename);
        }
    }

    /**
     * Sends the necessary image headers for browser out and outputs the the image.
     * This method is needed, because Varien_Image method display destroys the image quality.
     * Images are cached in the browser for one hour.
     *
     * @param string $cacheFilename path of the image cache file
     * @return void
     */
    protected function _outputImage($cacheFilename)
    {
        $cacheLifetimeInSeconds = 60 * 60;

        header('Content-type: image/' . $this->_imageType);
        header('Cache-Control: max-age=' . $cacheLifetimeInSeconds);
        header('Expires: ' . gmdate('D, d M Y H:i:s', Mage::getModel('core/date')->timestamp(time()) + $cacheLifetimeInSeconds));
        header('Pragma: cache');

        echo file_get_contents($cacheFilename);
    }

    /**
     * Sets the absolute image url for this image instance and sets also the image type
     * dependent on this url
     *
     * @param string $absoluteImageUrl the requestet image url
     * @return void
     */
    protected function _setAbsoluteImageUrl($absoluteImageUrl)
    {
        $this->_absoluteImageUrl = $absoluteImageUrl;
        $this->_setImageType($absoluteImageUrl);
    }

    /**
     * Returns the image type in the actual path
     *
     * @param string $cacheFilename filename
     * @throws Exception when an not allowed image type is contained in the cache filename
     * @return integer Magmex_AdventCalendar_Model_Image::IMAGE_TYPE_JPEG|Magmex_AdventCalendar_Model_Image::IMAGE_TYPE_PNG
     */
    protected function _setImageType($cacheFilename)
    {
        if (strpos($cacheFilename, '.jpeg') !== false || strpos($cacheFilename, '.jpg') !== false) {
            $this->_imageType = self::IMAGE_TYPE_JPEG;
        } else if (strpos($cacheFilename, '.png') !== false) {
            $this->_imageType = self::IMAGE_TYPE_PNG;
        } else {
            throw new Exception('wrong image type was given - only JPEG and PNG are allowed');
        }
    }

    /**
     * Returns the complete local image filename on the filesystem
     *
     * @param string $absoluteImageUrl absolute image URL
     * @return string local filename
     */
    protected function _getLocalFilename($absoluteImageUrl)
    {
        //if a skin url was given, then we have to look in the skin folder
        //otherwise we have normal images in the media folder
        if (strpos($absoluteImageUrl, 'skin/') !== false) {
            $localFilename = Mage::getBaseDir() . $absoluteImageUrl;
        } else {
            $localFilename = Mage::getBaseDir('media') . $absoluteImageUrl;
        }

        return $localFilename;
    }

    /**
     * Returns the cache filename of the image
     *
     * @param string $absoluteImageUrl absolute image URL
     * @param integer $width image width
     * @throws Exception exception, if absoluteImageUrl is incorrect
     * @return string cache filename
     */
    protected function _getCacheFilename($absoluteImageUrl, $width)
    {
        //Replace skin path - cache path is always in the media directory - for standard
        //background images, that lie in the skin path (standard background images and sprites)
        //customized background images lie already in the media folder - so the $absoluteImageUrl is already correct
        $relativeLocalImagePath = $absoluteImageUrl;

        if (strpos($absoluteImageUrl, Magmex_AdventCalendar_Model_Adventcalendar::SKIN_STANDARD_BACKGROUND_IMAGE_PATH) !== false) {
            $relativeLocalImagePath = str_replace(Magmex_AdventCalendar_Model_Adventcalendar::SKIN_STANDARD_BACKGROUND_IMAGE_PATH, Magmex_AdventCalendar_Model_Adventcalendar::MEDIA_SUB_IMAGE_PATH . '/standard_background_image', $absoluteImageUrl);
        } else if (strpos($absoluteImageUrl, Magmex_AdventCalendar_Helper_Data::SKIN_DOOR_BACKGROUND_IMAGE_SPRITE_PATH) !== false) {
            $relativeLocalImagePath = str_replace(Magmex_AdventCalendar_Helper_Data::SKIN_DOOR_BACKGROUND_IMAGE_SPRITE_PATH, Magmex_AdventCalendar_Model_Adventcalendar::MEDIA_SUB_IMAGE_PATH . '/sprites', $absoluteImageUrl);
        }

        $completeImageFilename = Mage::getBaseDir('media') . $relativeLocalImagePath;

        $cacheFilename = dirname($completeImageFilename) . DS . 'cache' . DS . $width . DS . basename($completeImageFilename);
        return $cacheFilename;
    }

    /**
     * Resizes and caches an image
     *
     * @param string $cacheFilename cache filename of the image
     * @param integer $width image width image with
     * @throws Exception if caching is not possible an exception is thrown
     * @return void
     */
    protected function _cacheImage($cacheFilename, $width)
    {
        //GD2 (or maybe the Varien adapter) has big problems, when a high quality number (e.g. 90) is given for PNGs:
        //the number gets interpreted in a false way, so the image filesizes gets up to 5 times bigger even when the
        //image dimensions are much smaller after the resizing
        switch ($this->_imageType) {
            case self::IMAGE_TYPE_JPEG:
                $this->_image->quality(100);
                break;
            case self::IMAGE_TYPE_PNG:
                $this->_image->quality(1);
                $this->_image->keepTransparency(true);
                break;
            default:
                throw new Exception('caching image is not possible, because a not allowed image type was given');
        }

        $this->_image->constrainOnly(true);
        $this->_image->keepAspectRatio(false);
        $this->_image->resize($width);
        $this->_image->save($cacheFilename);
    }
}