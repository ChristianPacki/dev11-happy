<?php
/**
 * Magmex Adventcalendar day action product discount model responsibly for a product discount
 * logic behind an Advent Calendar door
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.magmex.com/MAGMEX_SOFTWARE_LICENSE.txt
 *
 * @copyright Copyright (c) 2015 magmex, Michael Stork, (http://www.magmex.com)
 * @license http://www.magmex.com/MAGMEX_SOFTWARE_LICENSE.txt
 * @category Magmex
 * @package Magmex_AdventCalendar
 * @author Michael Stork <m.stork@magmex.com>
 */

class Magmex_AdventCalendar_Model_Day_Action_Productdiscount extends Mage_Core_Model_Abstract
{
    /**
     * Defines the action type - needed as identification and several validations
     *
     * @var string
     */
    const ACTION_TYPE = 'product_discount';
    
    /**
     * Construct
     * Inits the resource model
     *
     * @see Varien_Object::_construct()
     */
    protected function _construct()
    {
        parent::_construct();
        $this->_init('magmex_adventcalendar/day_action_productdiscount');
        $this->setActionType(self::ACTION_TYPE);
    }
    
    /**
     * Set special price for the selected product SKU before saving the product discount day action
     *
     * @see Mage_Core_Model_Abstract::_beforeSave()
     * return Magmex_AdventCalendar_Model_Day_Action_Productdiscount chaining
     */
    protected function _beforeSave()
    {
        /** @var Mage_Catalog_Model_Product */
        $product = Mage::getModel('catalog/product')->loadByAttribute('sku', $this->getProductSku());
        
        if (empty($product)) {
            throw new Exception('Product SKU is not existant');
        }
        
        $product->setSpecialPrice($this->getDiscountPriceOrPercent());
        
        /** @var Magmex_AdventCalendar_Model_Day */
        $modelAdventCalendarDay = Mage::getModel('magmex_adventcalendar/day')->load($this->getAdventCalendarDayId());
        
        $product->setSpecialFromDate($modelAdventCalendarDay->getDate());
        $product->setSpecialToDate($modelAdventCalendarDay->getDate());
        $product->save();
        
        parent::_beforeSave();
    }
}