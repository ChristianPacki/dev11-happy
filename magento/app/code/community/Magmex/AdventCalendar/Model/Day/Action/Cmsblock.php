<?php
/**
 * Magmex Adventcalendar day action cms block model responsibly for a cms block
 * logic behind an Advent Calendar door
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.magmex.com/MAGMEX_SOFTWARE_LICENSE.txt
 *
 * @copyright Copyright (c) 2015 magmex, Michael Stork, (http://www.magmex.com)
 * @license http://www.magmex.com/MAGMEX_SOFTWARE_LICENSE.txt
 * @category Magmex
 * @package Magmex_AdventCalendar
 * @author Michael Stork <m.stork@magmex.com>
 */

class Magmex_AdventCalendar_Model_Day_Action_Cmsblock extends Mage_Core_Model_Abstract
{
    /**
     * Defines the action type - needed as identification and several validations
     *
     * @var string
     */
    const ACTION_TYPE = 'cms_block';
    
    /**
     * Construct
     * Inits the resource model
     *
     * @see Varien_Object::_construct()
     */
    protected function _construct()
    {
        parent::_construct();
        $this->_init('magmex_adventcalendar/day_action_cmsblock');
        $this->setActionType(self::ACTION_TYPE);
    }
}