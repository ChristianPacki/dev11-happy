<?php
/**
 * Magmex: collection model for Advent Calendar day action product discount
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.magmex.com/MAGMEX_SOFTWARE_LICENSE.txt
 *
 * @copyright Copyright (c) 2015 magmex, Michael Stork, (http://www.magmex.com)
 * @license http://www.magmex.com/MAGMEX_SOFTWARE_LICENSE.txt
 * @category Magmex
 * @package Magmex_AdventCalendar
 * @author Michael Stork <m.stork@magmex.com>
 */

class Magmex_AdventCalendar_Model_Mysql4_Day_Action_Productdiscount_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    /**
     * Construct
     * Inits the Advent Calendar day action product discount collection - uses the day action product discount model
     *
     * @see Mage_Core_Model_Mysql4_Collection_Abstract::_construct()
     * @return void
     */
    public function _construct()
    {
        parent::_construct();
        $this->_init('magmex_adventcalendar/day_action_productdiscount');
    }
}