<?php 
    class_exists('ML', false) or die();
    $aField['value'] = (isset($aField['value']) && is_array($aField['value'])) ? $aField['value'] : array();
    
    $aField['multiple'] = true;
    $aField['type'] = 'select';
    $this->includeType($aField);
?>
