<?php
/* Autogenerated file. Do not change! */

MLI18n::gi()->{'sModuleNameCdiscount'} = 'Cdiscount';
MLI18n::gi()->{'cdiscount_label_title'} = 'Cdiscount Title';
MLI18n::gi()->{'cdiscount_category'} = 'Cdiscount Category';
MLI18n::gi()->{'cdiscount_label_item_id'} = 'Item ID';
MLI18n::gi()->{'cdiscount_search_by_title'} = 'Search by title';
MLI18n::gi()->{'cdiscount_search_by_ean'} = 'Search by ean';
MLI18n::gi()->{'cdiscount_label_not_matched'} = 'don\'t match';
MLI18n::gi()->{'cdiscount_upload_explanation'} = 'Uploaded products will usually be listed within one day on Cdiscount. 
It could take up to two weeks if the product is not already existing on Cdiscount. Articles can be found in your Cdiscount Seller Account after that time.';
