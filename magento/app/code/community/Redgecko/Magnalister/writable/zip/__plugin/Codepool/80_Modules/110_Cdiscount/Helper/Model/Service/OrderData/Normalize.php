<?php
MLFilesystem::gi()->loadClass('Modul_Helper_Model_Service_OrderData_Normalize');

class ML_Cdiscount_Helper_Model_Service_OrderData_Normalize extends ML_Modul_Helper_Model_Service_OrderData_Normalize {

    protected $oModul = null;

    protected function getModul() {
        if($this->oModul === null ){
            $this->oModul = MLModul::gi();
        }

        return $this->oModul;
    }

    protected function getPaymentCode($aTotal) {
        if ('textfield' == $this->getModul()->getConfig('orderimport.paymentmethod')) {
            $sPayment = $this->getModul()->getConfig('orderimport.paymentmethod.name');
            return $sPayment == '' ? MLModul::gi()->getMarketPlaceName() : $sPayment;
        }
        
        return MLModul::gi()->getMarketPlaceName();
    }
    
    protected function getShippingCode($aTotal) {
        $sShippingMethod = $this->getModul()->getConfig('orderimport.shippingmethod');
        if (!empty($sShippingMethod)) {
            if ('textfield' == $sShippingMethod) {
               $sShipping = $this->getModul()->getConfig('orderimport.shippingmethod.name');
               return $sShipping == '' ? MLModul::gi()->getMarketPlaceName() : $sShipping;
            }

            return $sShippingMethod;
        }

        return MLModul::gi()->getMarketPlaceName();
    }

}
