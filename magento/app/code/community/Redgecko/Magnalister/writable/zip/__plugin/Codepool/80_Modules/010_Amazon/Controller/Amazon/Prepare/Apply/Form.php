<?php

/**
 * 888888ba                 dP  .88888.                    dP
 * 88    `8b                88 d8'   `88                   88
 * 88aaaa8P' .d8888b. .d888b88 88        .d8888b. .d8888b. 88  .dP  .d8888b.
 * 88   `8b. 88ooood8 88'  `88 88   YP88 88ooood8 88'  `"" 88888"   88'  `88
 * 88     88 88.  ... 88.  .88 Y8.   .88 88.  ... 88.  ... 88  `8b. 88.  .88
 * dP     dP `88888P' `88888P8  `88888'  `88888P' `88888P' dP   `YP `88888P'
 *
 *                          m a g n a l i s t e r
 *                                      boost your Online-Shop
 *
 * -----------------------------------------------------------------------------
 * $Id$
 *
 * (c) 2010 - 2014 RedGecko GmbH -- http://www.redgecko.de
 *     Released under the MIT License (Expat)
 * -----------------------------------------------------------------------------
 */
MLFilesystem::gi()->loadClass('Form_Controller_Widget_Form_PrepareWithVariationMatchingAbstract');

class ML_Amazon_Controller_Amazon_Prepare_Apply_Form extends ML_Form_Controller_Widget_Form_PrepareWithVariationMatchingAbstract {

    protected $aParameters = array('controller');

    protected function getSelectionNameValue() {
        return 'apply';
    }

    protected function getCustomIdentifier()
    {
        $category = $this->getRequestField('variationgroups.value');
        if (!isset($category)) {
            $category = $this->getField('variationgroups.value','value');
        }

        $sCustomIdentifier = $this->getRequestField('ProductType');
        if (!isset($sCustomIdentifier)) {
            $sCustomIdentifier = $this->getField('ProductType','value');
        }
        return !empty($sCustomIdentifier[$category]) ? $sCustomIdentifier[$category] : '';
    }

    protected function triggerBeforeFinalizePrepareAction() {
        $blReturn = parent::triggerBeforeFinalizePrepareAction();
        $aActions = $this->getRequest($this->sActionPrefix);
        $savePrepare = $aActions['prepareaction'] === '1';

        $sMessage = '';
        $oddEven = true;
        $i = 0;
        foreach ($this->oPrepareList->getList() as $oPrepared) {
            $this->oProduct = MLProduct::factory()->set('id', $oPrepared->get('productsid'));
            $aMissingValues = array();
            foreach(array(
                        MLI18n::gi()->get('ML_LABEL_MAINCATEGORY')              => 'variationgroups.value',
                        MLI18n::gi()->get('ML_AMAZON_LABEL_APPLY_BROWSENODES')  => 'topbrowsenode1',
                        MLI18n::gi()->get('ML_LABEL_PRODUCT_NAME')              => 'itemtitle',
                        MLI18n::gi()->get('ML_GENERIC_MANUFACTURER_NAME')       => 'manufacturer',
                    ) as $sMissingText => $sFieldName) {
                $mValue = $this->getField($sFieldName, 'value');
                if (empty($mValue)) {
                    $blReturn = false;
                    $aMissingValues[] = $sMissingText;
                }
            }

            if (!empty($aMissingValues)) {
                $this->setPreparedStatusFalse();
            }

            if (!empty($aMissingValues) && $i <= 20) {
                $sMessage .= '
                    <tr class="'.(($oddEven = !$oddEven) ? 'odd' : 'even') . '">
                        <td>' . $this->oProduct->get('id') . '</td>
                        <td>' . $this->oProduct->getMarketPlaceSku() . '</td>
                        <td>' . $this->oProduct->getName() . '</td>
                        <td>' . implode(', ', $aMissingValues) . '</td>
                        <td  class="ml-product-edit"><div class="product-link"><a class="gfxbutton edit ml-js-noBlockUi" title="bearbeiten" target="_blank" href="' . $this->oProduct->getEditLink() . '">&nbsp;</a></div></td>
                    </tr>
                ';
            } elseif(!empty($aMissingValues) && $i == 21) {
                $sMessage .= '
                    <tr class="' . (($oddEven = !$oddEven) ? 'odd' : 'even') . '">
                        <td colspan="5" class="textcenter bold">&hellip;</td>
                    </tr>
                ';
            }

            ++$i;
        }

        if ($savePrepare && !empty($sMessage)) {
            $sMessage = '
                <table class="datagrid">
                    <thead>
                        <tr>
                            <th>' . $this->__('ML_LABEL_PRODUCTS_ID') . '</th>
                            <th>' . $this->__('ML_LABEL_ARTICLE_NUMBER') . '</th>
                            <th>' . $this->__('ML_LABEL_PRODUCT_NAME') . '</th>
                            <th>' . $this->__('ML_AMAZON_LABEL_MISSING_FIELDS') . '</th>
                            <th>&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
                        ' . $sMessage . '
                    </tbody>
                </table>
            ';
            MLMessage::gi()->addNotice($this->__('ML_AMAZON_TEXT_APPLY_DATA_INCOMPLETE') . $sMessage);
        }

        if ($savePrepare) {
            $this->oPrepareList->set('applydata', '');
        }

        return $blReturn;
    }

    protected function setPreparedStatusTrue() {
        $this->oPrepareList->set('iscomplete', 'true');
    }

    protected function setPreparedStatusFalse($productIDs = array()) {
        $this->oPrepareList->set('iscomplete', 'false');
    }
    
    protected function variationGroups_valueField(&$aField) {
        $aField['type'] = 'select';
        $aTopTen = $this->getTopTenCategories('topMainCategory', $aField['name']);
        if (count($aTopTen) > 0) {
            $aCategories = array($this->__('ML_TOPTEN_TEXT') => $aTopTen) + MLModul::gi()->getMainCategories();
        } else {
            $aCategories = MLModul::gi()->getMainCategories();
        }

        $aField['values'] = array('' => $this->__('ML_AMAZON_LABEL_APPLY_PLEASE_SELECT')) + $aCategories;
    }

    protected function prepareTypeField(&$aField){
        $aField['value'] = 'apply';
        $aField['type'] = 'hidden';
    }

    protected function productTypeField(&$aField){
        $aField['type']='ajax';
        $mParentValue=$this->getField('variationgroups.value','value');
        $aField['ajax']=array(
            'selector' => '#' . $this->getField('variationgroups.value', 'id'),
            'trigger' => 'change',
        );
        $aField['ajax']['field']['autoTriggerOnLoad'] = 'change';

        if ($mParentValue != '') {
            $aTypesAndAttributes = MLModul::gi()->getProductTypesAndAttributes($mParentValue);
            if (isset($aTypesAndAttributes['ProductTypes']) && !empty($aTypesAndAttributes['ProductTypes'])) {
                $aTopTen = $this->getTopTenCategories('topProductType', array($mParentValue));
                if (count($aTopTen) > 0) {
                    $aCategories = array_merge(
                        array($this->__('ML_TOPTEN_TEXT') => $aTopTen), $aTypesAndAttributes['ProductTypes']
                    );
                } else {
                    $aCategories = $aTypesAndAttributes['ProductTypes'];
                }

                $aField['values'] = $aCategories;

                $aField['ajax']['field']['type'] = 'dependonfield';
                $aField['dependonfield']['field']['type'] = 'select';
            }
        }
    }

    protected function variationMatchingField(&$aField)
    {
        $aField['ajax'] = array(
            'selector' => '#' . $this->getField('ProductType', 'id'),
            'trigger' => 'change',
            'field' => array(
                'type' => 'switch',
            ),
        );
    }

    protected function browseNodesField(&$aField){
        $aField['type']='ajax';
        $aField['ajax']=array(
            'selector' => '#' . $this->getField('variationgroups.value', 'id'),
            'trigger' => 'change',
        );

        $mParentValue=$this->getField('variationgroups.value','value');
        if($mParentValue!=''){
            $aBrowseNodes = MLModul::gi()->getBrowseNodes($mParentValue);
            if(!empty($aBrowseNodes)){
                $aTopTen = $this->getTopTenCategories('topBrowseNode', array($mParentValue));
                if (count($aTopTen) > 0) {
                    $aBrowseNodes = array($this->__('ML_TOPTEN_TEXT') => $aTopTen) + $aBrowseNodes;
                }

                $aField['values'] = array('' => $this->__('ML_AMAZON_LABEL_APPLY_PLEASE_SELECT')) + $aBrowseNodes;
                $aField['ajax']['field']['type'] = 'dependonfield';
                $aField['dependonfield']['field']['type'] = 'amazon_browsenodes';
            }
        }
    }

    protected function EanField(&$aField){
        $sType = $this->getInternationalIdentifier();
        $aField['i18n']['label'] = $sType;
        $aField['i18n']['hint'] = MLI18n::gi()->replace($aField['i18n']['hint'], array('Type' => $sType));
        $aField['i18n']['optional']['checkbox']['labelNegativ'] = MLI18n::gi()->replace($aField['i18n']['optional']['checkbox']['labelNegativ'], array('Type' => $sType));
    }

    protected function b2bselltoField(&$aField) {
        $aField['values'] = $aField['i18n']['values'];
    }

    protected function shippingTimeField(&$aField) {
        $aValues=range(0,30);
        unset($aValues[0]);// value=1 should have key=1
        return array(
            'type'=>'select',
            'values'=> $aValues,
        );
    }

    public function getMPVariationAttributes($sVariationValue) {
        $requestParams = array(
            'ACTION' => 'GetCategoryDetails',
            'CATEGORY' => $sVariationValue
        );

        $productType = $this->getCustomIdentifier();
        if (!empty($productType)) {
            $requestParams['PRODUCTTYPE'] = $productType;
        }

        $aValues = MagnaConnector::gi()->submitRequestCached($requestParams);
        $result = array();
        if ($aValues && is_array($aValues['DATA']['attributes'])) {
            foreach ($aValues['DATA']['attributes'] as $key => $value) {
                $result[$key] = array(
                    'value' => $value['title'],
                    'required' => isset($value['mandatory']) ? $value['mandatory'] : true,
                    'changed' => isset($value['changed']) ? $value['changed'] : null,
                    'desc' => isset($value['desc']) ? $value['desc'] : '',
                    'values' => !empty($value['values']) ? $value['values'] : array(),
                    'dataType' => !empty($value['type']) ? $value['type'] : 'text',
                );
            }
        }

        $aResultFromDB = $this->getAttributesFromDB($sVariationValue, $productType);
        $iFromDb = 0;
        if ($aResultFromDB) {
            $array = $this->arrayFilterKey($aResultFromDB, function ($key) {
                return strpos($key, 'additional_attribute_') === 0;
            });

            $iFromDb = count($array);
        }

        if ($iFromDb <= $this->numberOfMaxAdditionalAttributes) {
            $iFromDb--;
        }

        for ($i = 0; $i <= $iFromDb; $i++) {
            $result['additional_attribute_' . $i] = array(
                'value' => self::getMessage('_prepare_variations_additional_attribute_label'),
                'required' => false,
            );
        }

        $this->detectChanges($result, $sVariationValue);

        return $result;
    }

    protected function getMPAttributeValues($sCategoryId, $sMpAttributeCode, $sAttributeCode = false) {
        $requestParams = array(
            'ACTION' => 'GetCategoryDetails',
            'CATEGORY' => $sCategoryId
        );

        $productType = $this->getCustomIdentifier();
        if (!empty($productType)) {
            $requestParams['PRODUCTTYPE'] = $productType;
        }

        $response = MagnaConnector::gi()->submitRequestCached($requestParams);
        $aValues = array();
        $fromMP = false;
        if ($response && is_array($response['DATA']['attributes'])) {
            foreach ($response['DATA']['attributes'] as $key => $attribute) {
                if ($key === $sMpAttributeCode && !empty($attribute['values'])) {
                    $aValues = $attribute['values'];
                    $fromMP = true;
                    break;
                }
            }
        }

        if (empty($aValues) && $sAttributeCode) {
            $shopValues = $this->getShopAttributeValues($sAttributeCode);
            foreach ($shopValues as $value) {
                $aValues[$value] = $value;
            }
        }

        return array(
            'values' => $aValues,
            'from_mp' => $fromMP
        );
    }

    protected function getAttributeValues($sIdentifier, $sCustomIdentifier, $sAttributeCode = null, $bFreeText = false)
    {
        $oPreparedList = $this->oPrepareHelper->getPrepareList();
        $oPrepareTable = MLDatabase::getPrepareTableInstance();
        $sShopVariationField = $oPrepareTable->getShopVariationFieldName();
        $sPrimaryCategoryValue = current($oPreparedList->get($oPrepareTable->getPrimaryCategoryFieldName(),true));
        
        $aProductType = current($oPreparedList->get('ProductType',true));
        $sProductType = !empty($aProductType[$sPrimaryCategoryValue]) ? $aProductType[$sPrimaryCategoryValue] : '';
        if (!empty($sPrimaryCategoryValue) && $sPrimaryCategoryValue === $sIdentifier && $sProductType == $sCustomIdentifier) {
            $aValue = current($oPreparedList->get($sShopVariationField,true));
        }

        if (!isset($aValue)) {
            $aValue = $this->getVariationDb()
                ->set('Identifier', $sIdentifier, false)
                ->set('CustomIdentifier', $sCustomIdentifier)
                ->get('ShopVariation');
        }

        if ($aValue) {
            if ($sAttributeCode !== null) {
                foreach ($aValue as $sKey => $aMatch) {
                    if ($sKey === $sAttributeCode) {
                        return isset($aMatch['Values']) ? $aMatch['Values'] : ($bFreeText ? '' : array());
                    }
                }
            } else {
                return $aValue;
            }
        }

        if ($bFreeText) {
            return '';
        }

        return array();
    }

    /**
     * Covering situation if client prepared item before new variation matching concept
     *
     * @param $attributes
     * @param $sCategoryId
     * @return array
     */
    private function fixOldAttributes($attributes, $sCategoryId) {
        $response = MagnaConnector::gi()->submitRequest(array('ACTION' => 'GetCategoryDetails', 'CATEGORY' => $sCategoryId));
        $mpAttributes = $response['DATA']['attributes'];

        $attributesFixed = array();
        foreach ($attributes as $attributeKey => $attributeValue) {
            $attributesFixed[$attributeKey] = array(
                'Kind' => 'Matching',
                'Values' => $attributeValue,
                'Required' => isset($mpAttributes[$attributeKey]['mandatory']) ? (bool)$mpAttributes[$attributeKey]['mandatory'] : false,
                'Code' => !empty($mpAttributes[$attributeKey]['values']) ? 'attribute_value' : 'freetext',
                'Error' => false
            );
        }

        return $attributesFixed;
    }
    
    /**
     * @param $sField
     * @param array $aConfig
     * @return array
     */
    private function getTopTenCategories($sField, $aConfig=array()) {
        $mpID = MLModul::gi()->getMarketPlaceId();
        $sParent = isset($aConfig[0]) ? $aConfig[0] : '';
        switch ($sField) {
            case 'topMainCategory':{
                $sWhere = "1 = 1";
                $sUnion = null;
                break;
            }
            case 'topProductType':{
                $sWhere = "topMainCategory = '".$sParent."'";
                $sUnion = null;
                break;
            }
            case 'topBrowseNode':{
                $sField = 'topBrowseNode1';
                $sWhere = "topMainCategory = '".$sParent."'";
                $sUnion = 'topBrowseNode2';
                break;
            }
        }

        if ($sUnion === null) {
            $sSql = "
				select ".$sField." 
				from magnalister_amazon_prepare
				where ".$sWhere."
				and  mpID = '".$mpID."'
				and ".$sField." <> '0'
				group by ".$sField." 
				order by count(*) desc
			";
        } else {
            // if performance problems in this query, get all data and prepare with php
            $sSql="
				select m.".$sField." from
				(
					(
						select f.".$sField."
						from magnalister_amazon_prepare f 
						where ".$sWhere." and mpID = '".$mpID."' and ".$sField." <> '0' 
					)
					UNION ALL
					(
						select u.".$sUnion."
						from magnalister_amazon_prepare u 
						where ".$sWhere." and mpID = '".$mpID."' and ".$sUnion." <> '0'
					)
				) m
				group by m.".$sField."
				order by count(m.".$sField.") desc
			";
        }

        $aTopTen = MLDatabase::getDbInstance()->fetchArray($sSql, true);
        $aOut = array();
        try {
            switch ($sField) {
                case 'topMainCategory':{
                    $aCategories = MLModul::gi()->getMainCategories();
                    break;
                }
                case 'topProductType':{
                    $aCategories = MLModul::gi()->getProductTypesAndAttributes($sParent);
                    $aCategories = $aCategories['ProductTypes'];
                    break;
                }
                case 'topBrowseNode1':{
                    $aCategories = MLModul::gi()->getBrowseNodes($sParent);
                    break;
                }
            }

            foreach($aTopTen as $sCurrent){
                if(array_key_exists($sCurrent, $aCategories)) {
                    $aOut[$sCurrent] = $aCategories[$sCurrent];
                }else{
                    MLDatabase::getDbInstance()->query("UPDATE magnalister_amazon_prepare set ".$sField." = 0 where ".$sField." = '".$sCurrent."'");//no mpid
                    if($sUnion !== null){
                        MLDatabase::getDbInstance()->query("UPDATE magnalister_amazon_prepare set ".$sUnion." = 0 where ".$sUnion." = '".$sCurrent."'");//no mpid
                    }
                }
            }
        } catch (MagnaException $e) {
            echo print_m($e->getErrorArray(), 'Error: '.$e->getMessage(), true);
        }

        asort($aOut);
        return $aOut;
    }

    private function getInternationalIdentifier() {
        $sSite = MLModul::gi()->getConfig('site');
        if ($sSite === 'US') {
            return 'UPC';
        }

        return 'EAN';
    }
    
    protected function shippingTemplateField(&$aField) {
        if(MLModul::gi()->getConfig('shipping.template.active') == '1'){
            $aDefaultTemplate = MLModul::gi()->getConfig('shipping.template');
            $aTemplateName = MLModul::gi()->getConfig('shipping.template.name');
            $aField['type']='select';
            $aField['autooptional'] = false;
            foreach ($aDefaultTemplate as $iKey => $sValue) {
                 $aField['values'][]= $aTemplateName[$iKey];
            }
        }else{
            return array();
        }
    }
}
