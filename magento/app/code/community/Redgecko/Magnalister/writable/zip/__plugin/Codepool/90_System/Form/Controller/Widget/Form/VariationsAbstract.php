<?php

/**
 * 888888ba                 dP  .88888.                    dP
 * 88    `8b                88 d8'   `88                   88
 * 88aaaa8P' .d8888b. .d888b88 88        .d8888b. .d8888b. 88  .dP  .d8888b.
 * 88   `8b. 88ooood8 88'  `88 88   YP88 88ooood8 88'  `"" 88888"   88'  `88
 * 88     88 88.  ... 88.  .88 Y8.   .88 88.  ... 88.  ... 88  `8b. 88.  .88
 * dP     dP `88888P' `88888P8  `88888'  `88888P' `88888P' dP   `YP `88888P'
 *
 *                          m a g n a l i s t e r
 *                                      boost your Online-Shop
 *
 * -----------------------------------------------------------------------------
 * $Id$
 *
 * (c) 2010 - 2014 RedGecko GmbH -- http://www.redgecko.de
 *     Released under the MIT License (Expat)
 * -----------------------------------------------------------------------------
 */
MLFilesystem::gi()->loadClass('Form_Controller_Widget_Form_ConfigAbstract');

abstract class ML_Form_Controller_Widget_Form_VariationsAbstract extends ML_Form_Controller_Widget_Form_ConfigAbstract
{

    protected $aParameters = array('controller');
    protected $shopAttributes;
    protected $numberOfMaxAdditionalAttributes = 0;

    public static function getTabTitle()
    {
        return self::getMessage('_prepare_variations_title');
    }

    public static function getTabActive()
    {
        return MLModul::gi()->isAuthed();
    }

    public function getModificationDate($sIdentifier, $sCustomIdentifier = '')
    {
        return $this->getVariationDb()
            ->set('Identifier', $sIdentifier)
            ->set('CustomIdentifier', $sCustomIdentifier)
            ->get('ModificationDate');
    }

    public function deleteAction($blExecute = true)
    {
        if ($blExecute) {
            $this->getRequestField();
            $sCustomIdentifier = $this->getCustomIdentifier();
            $this->getVariationDb()->deleteCustomVariation($sCustomIdentifier);
        }
    }

    public function resetAction($blExecute = true)
    {
        if ($blExecute) {
            $aActions = $this->getRequest($this->sActionPrefix);
            $reset = $aActions['resetaction'] === '1';
            if ($reset) {
                $aMatching = $this->getRequestField();
                $sIdentifier = $aMatching['variationgroups.value'];
                if (empty($sIdentifier) || $sIdentifier === 'none') {
                    MLMessage::gi()->addError(self::getMessage('_prepare_match_variations_category_missing'));
                    return;
                }

                $sCustomIdentifier = $this->getCustomIdentifier();

                $oVariantMatching = $this->getVariationDb();
                $oVariantMatching->deleteVariation($sIdentifier, $sCustomIdentifier);
                MLRequest::gi()->set('resetForm', true);
                MLMessage::gi()->addSuccess(self::getMessage('_prepare_variations_reset_success'));
            }
        }
    }

    public function saveAction($blExecute = true)
    {
        if ($blExecute) {
            $aActions = $this->getRequest($this->sActionPrefix);
            $savePrepare = $aActions['saveaction'] === '1';
            $aMatching = $this->getRequestField();
            $sIdentifier = $aMatching['variationgroups.value'];
            $sCustomIdentifier = $this->getCustomIdentifier();
            if (isset($aMatching['attributename'])) {
                $sIdentifier = $aMatching['attributename'];
                if ($sIdentifier === 'none') {
                    MLMessage::gi()->addError(self::getMessage('_prepare_match_variations_attribute_missing'));
                    return;
                }
            }

            if (isset($aMatching['variationgroups'])) {
                $aMatching = $aMatching['variationgroups'][$sIdentifier];
                $oVariantMatching = $this->getVariationDb();
                $oVariantMatching->deleteVariation($sIdentifier, $sCustomIdentifier);

                if ($sIdentifier === 'new') {
                    $sIdentifier = $aMatching['variationgroups.code'];
                    unset($aMatching['variationgroups.code']);
                }

                $aErrors = array();
                $addNotAllValuesMatchedNotice = false;
                foreach ($aMatching as $key => &$value) {
                    if (isset($value['Required'])) {
                        $value['Required'] = (bool)$value['Required'];
                    }

                    $value['Error'] = false;
                    $sAttributeName = !empty($value['CustomName']) ? $value['CustomName'] : $value['AttributeName'];

                    if ($value['Code'] == '' || empty($value['Values'])) {
                        if (isset($value['Required']) && $value['Required'] && $savePrepare) {
                            $aErrors[] = self::getMessage('_prepare_variations_error_text', array('attribute_name' => $sAttributeName));
                            $value['Error'] = true;
                        } else {
                            unset($aMatching[$key]);
                        }

                        continue;
                    }

                    if (!is_array($value['Values']) || !isset($value['Values']['FreeText'])) {
                        continue;
                    }

                    $sInfo = self::getMessage('_prepare_variations_manualy_matched');
                    $sFreeText = $value['Values']['FreeText'];
                    unset($value['Values']['FreeText']);

                    if ($value['Values']['0']['Shop']['Key'] === 'noselection' || $value['Values']['0']['Marketplace']['Key'] === 'noselection') {
                        unset($value['Values']['0']);
                        if (empty($value['Values']) && $value['Required'] && $savePrepare) {
                            $aErrors[] = self::getMessage('_prepare_variations_error_text', array('attribute_name' => $sAttributeName));
                            $value['Error'] = true;
                        }

                        foreach ($value['Values'] as $k => &$v) {
                            if (empty($v['Marketplace']['Info']) || $v['Marketplace']['Key'] === 'manual') {
                                $v['Marketplace']['Info'] = $v['Marketplace']['Value'] . self::getMessage('_prepare_variations_free_text_add');
                            }
                        }

                        continue;
                    }

                    if ($value['Values']['0']['Marketplace']['Key'] === 'reset') {
                        unset($aMatching[$key]);
                        continue;
                    }

                    if ($value['Values']['0']['Marketplace']['Key'] === 'manual') {
                        $sInfo = self::getMessage('_prepare_variations_free_text_add');
                        if (empty($sFreeText)) {
                            if ($savePrepare) {
                                $aErrors[] = $sAttributeName . self::getMessage('_prepare_variations_error_free_text');
                                $value['Error'] = true;
                            }

                            unset($value['Values']['0']);
                            continue;
                        }

                        $value['Values']['0']['Marketplace']['Value'] = $sFreeText;
                    }

                    if ($value['Values']['0']['Marketplace']['Key'] === 'auto') {
                        $addNotAllValuesMatchedNotice = !$this->autoMatch($sIdentifier, $key, $value);
                        $value['Values'] = $this->fixAttributeValues($value['Values']);
                        continue;
                    }

                    $this->checkNewMatchedCombination($value['Values']);
                    if ($value['Values']['0']['Shop']['Key'] === 'all') {
                        $newValue = array();
                        $i = 0;
                        foreach ($this->getShopAttributeValues($value['Code']) as $keyAttribute => $valueAttribute) {
                            $newValue[$i]['Shop']['Key'] = $keyAttribute;
                            $newValue[$i]['Shop']['Value'] = $valueAttribute;
                            $newValue[$i]['Marketplace']['Key'] = $value['Values']['0']['Marketplace']['Key'];
                            $newValue[$i]['Marketplace']['Value'] = $value['Values']['0']['Marketplace']['Value'];
                            $newValue[$i]['Marketplace']['Info'] = $value['Values']['0']['Marketplace']['Value'] . $sInfo;
                            $i++;
                        }

                        $value['Values'] = $newValue;
                    } else {
                        foreach ($value['Values'] as $k => &$v) {
                            if (empty($v['Marketplace']['Info'])) {
                                $v['Marketplace']['Info'] = $v['Marketplace']['Value'] . $sInfo;
                            }
                        }
                    }

                    $value['Values'] = $this->fixAttributeValues($value['Values']);
                }

                $oVariantMatching->set('Identifier', $sIdentifier)
                    ->set('CustomIdentifier', $sCustomIdentifier)
                    ->set('ShopVariation', json_encode($aMatching))
                    ->set('ModificationDate', date('Y-m-d H:i:s'))
                    ->save();

                if ($savePrepare) {
                    $showSuccess = empty($aErrors) && !$addNotAllValuesMatchedNotice;
                    if ($showSuccess) {
                        MLRequest::gi()->set('resetForm', true);
                        MLMessage::gi()->addSuccess(self::getMessage('_prepare_variations_saved'));
                    } else {
                        foreach ($aErrors as $sError) {
                            MLMessage::gi()->addError($sError);
                        }

                        if ($addNotAllValuesMatchedNotice) {
                            MLMessage::gi()->addNotice(self::getMessage('_prepare_match_notice_not_all_auto_matched'));
                        }
                    }
                } else if ($addNotAllValuesMatchedNotice) {
                    MLMessage::gi()->addNotice(self::getMessage('_prepare_match_notice_not_all_auto_matched'));
                }
            } else {
                MLMessage::gi()->addError(self::getMessage('_prepare_match_variations_no_selection'));
            }
        }
    }

    private function fixAttributeValues($values) {
        if (isset($values['0'])) {
            $fixedValues = array();
            $i = 1;
            foreach ($values as $value) {
                $fixedValues[$i] = $value;
                $i++;
            }

            return $fixedValues;
        }

        return $values;
    }

    public function getRequestValue(&$aField)
    {
        try {
            if (MLRequest::gi()->data('resetForm')) {
                unset($aField['value']);
                return;
            }
        } catch (MagnaException $ex) {
        }

        parent::getRequestValue($aField);
        $sName = $aField['realname'];
        if ($sName === 'variationgroups.value') {
            return;
        }

        if (MLHttp::gi()->isAjax()) {
            $aRequestTriggerField = MLRequest::gi()->get('ajaxData');
            if ($aRequestTriggerField['method'] === 'variationmatching') {
                unset($aField['value']);
                return;
            }
        }

        if (!isset($aField['value'])) {
            $mValue = null;
            $aRequestFields = $this->getRequestField();
            $aNames = explode('.', $aField['realname']);
            $value = null;
            if (count($aNames) > 1 && isset($aRequestFields[$aNames[0]])) {
                // parent real name is in format "variationgroups.qnvjagzvcm1hda____.rm9ybwf0.code"
                // and name in request is "[variationgroups][Buchformat][Format][Code]"
                $sName = $sKey = $aNames[0];
                $aTmp = $aRequestFields[$aNames[0]];
                for ($i = 1; $i < count($aNames); $i++) {
                    if (is_array($aTmp)) {
                        foreach ($aTmp as $key => $value) {
                            if (strtolower($key) === 'code') {
                                break;
                            } elseif (strtolower($key) == $aNames[$i]) {
                                $sName .= '.' . $key;
                                $sKey = $key;
                                $aTmp = $value;
                                break;
                            }
                        }
                    } else {
                        break;
                    }
                }

                if (isset($sKey) && $sKey !== $aNames[0] && !is_array($value)) {
                    $mValue = array($sKey => $value, 'name' => $sName);
                }
            }

            if ($mValue != null) {
                $aField['value'] = reset($mValue);
                $aField['valuearr'] = $mValue;
            }
        }
    }

    public function getValue(&$aField)
    {
        $sName = $aField['realname'];

        // when top variation groups drop down is changed, its value is updated in getRequestValue
        // otherwise, it should remain empty. 
        // without second condition this function will be executed recursevly because of the second line below.
        if (!isset($aField['value']) && $sName !== 'variationgroups.value') {
            // check whether we're getting value for standard group or for custom variation mathing group
            $sCustomGroupName = $this->getField('variationgroups.value', 'value');
            $aCustomIdentifier = explode(':', $sCustomGroupName);

            if (count($aCustomIdentifier) == 2 && ($sName === 'attributename' || $sName === 'customidentifier')) {
                $aField['value'] = $aCustomIdentifier[$sName === 'attributename' ? 0 : 1];
                return;
            }

            $aNames = explode('.', $sName);
            if (count($aNames) == 4 && strtolower($aNames[3]) === 'code') {
                // real name is in format "variationgroups.qnvjagzvcm1hda____.rm9ybwf0.code"
                $sCustomIdentifier = count($aCustomIdentifier) == 2 ? $aCustomIdentifier[1] : '';
                if (empty($sCustomIdentifier)) {
                    $sCustomIdentifier = $this->getCustomIdentifier();
                }

                $aValue = $this->getVariationDb()
                    ->set('Identifier', $aNames[1])
                    ->set('CustomIdentifier', $sCustomIdentifier)
                    ->get('ShopVariation');
                if ($aValue) {
                    foreach ($aValue as $sKey => $aMatch) {
                        if (strtolower($sKey) === $aNames[2]) {
                            $aField['value'] = $aMatch['Code'];
                            break;
                        }
                    }
                }
            }
        }
    }

    protected function getCustomIdentifier()
    {
        $sCustomIdentifier = $this->getRequestField('customidentifier');
        return !empty($sCustomIdentifier) ? $sCustomIdentifier : '';
    }

    protected function encodeText($sText, $blLower = true)
    {
        return MLHelper::gi('text')->encodeText($sText, $blLower);
    }

    protected function decodeText($sText)
    {
        return MLHelper::gi('text')->decodeText($sText);
    }

    protected function variationGroupsField(&$aField)
    {
        $sMarketplaceName = MLModul::gi()->getMarketPlaceName();
        $aField['subfields']['variationgroups.value']['values'] = array('' => '..') +
            ML::gi()->instance('controller_' . $sMarketplaceName . '_config_prepare')->getField('primarycategory', 'values');

        foreach ($aField['subfields'] as &$aSubField) {
            // adding current cat, if not in top cat
            if (isset($aSubField['value']) && !array_key_exists($aSubField['value'], $aSubField['values'])) {
                $oCat = MLDatabase::factory($sMarketplaceName . '_categories' . $aSubField['cattype']);
                $oCat->init(true)->set('categoryid', $aSubField['value'] ? $aSubField['value'] : 0);
                $sCat = '';
                foreach ($oCat->getCategoryPath() as $oParentCat) {
                    $sCat = $oParentCat->get('categoryname') . ' &gt; ' . $sCat;
                }

                $aSubField['values'][$aSubField['value']] = substr($sCat, 0, -6);
            }
        }
    }

    protected function variationMatchingField(&$aField)
    {
        $aField['ajax'] = array(
            'selector' => '#' . $this->getField('variationgroups.value', 'id'),
            'trigger' => 'change',
            'field' => array(
                'type' => 'switch',
            ),
        );
    }

    protected function deleteActionField(&$aField)
    {
        $sGroupIdentifier = $this->getField('variationgroups.value', 'value');
        $aCustomIdentifier = explode(':', $sGroupIdentifier);
        $sCustomIdentifier = count($aCustomIdentifier) == 2 ? $aCustomIdentifier[1] : '';
        if (empty($sCustomIdentifier)) {
            $sCustomIdentifier = $this->getCustomIdentifier();
        }

        if (!empty($sCustomIdentifier)) {
            $aField['type'] = 'submit';
            $aField['value'] = 'delete';
        } else {
            $aField['type'] = '';
        }
    }

    protected function attributeNameField(&$aField)
    {
        $aField['type'] = 'select';
        $aField['values'] = array_merge(
            array('none' => MLI18n::gi()->get('ML_AMAZON_LABEL_APPLY_PLEASE_SELECT')), $this->getMPVariationGroups(false));
    }

    protected function attributeNameAjaxField(&$aField)
    {
        $aField['type'] = 'ajax';
        $aField['cascading'] = true;
        $aField['breakbefore'] = true;
        $aField['ajax'] = array(
            'selector' => '#' . $this->getField('attributename', 'id'),
            'trigger' => 'change',
            'field' => array(
                'type' => 'variations',
            ),
        );
    }

    protected function loadCategoryAttributesFromMP($sCategoryId)
    {
        return MagnaConnector::gi()->submitRequestCached(array(
                'ACTION' => 'GetCategoryDetails',
                'DATA' => array('CategoryID' => $sCategoryId))
        );
    }

    public function getMPVariationAttributes($sVariationValue)
    {
        $aValues = $this->loadCategoryAttributesFromMP($sVariationValue);
        $result = array();
        if ($aValues) {
            foreach ($aValues['DATA']['attributes'] as $key => $value) {
                $result[$key] = array(
                    'value' => $value['title'],
                    'required' => isset($value['mandatory']) ? $value['mandatory'] : true,
                    'changed' => isset($value['changed']) ? $value['changed'] : null,
                    'desc' => isset($value['desc']) ? $value['desc'] : '',
                    'values' => !empty($value['values']) ? $value['values'] : array(),
                    'dataType' => !empty($value['type']) ? $value['type'] : 'text',
                );
            }
        }

        $this->checkAttributesFromDB($sVariationValue, isset($aValues['DATA']['name']) ? $aValues['DATA']['name'] : '');

        $aResultFromDB = $this->getAttributesFromDB($sVariationValue);
        $iFromDb = 0;
        if ($aResultFromDB) {
            $array = $this->arrayFilterKey($aResultFromDB, function ($key) {
                return strpos($key, 'additional_attribute_') === 0;
            });

            $iFromDb = count($array);
        }

        if ($iFromDb < $this->numberOfMaxAdditionalAttributes || $this->numberOfMaxAdditionalAttributes === -1) {
            $iFromDb++;
        }

        for ($i = 0; $i < $iFromDb; $i++) {
            $result['additional_attribute_' . $i] = array(
                'value' => self::getMessage('_prepare_variations_additional_attribute_label'),
                'customAttributeValue' => isset($aResultFromDB['additional_attribute_' . $i]['CustomName']) ?
                    $aResultFromDB['additional_attribute_' . $i]['CustomName'] : null,
                'custom' => true,
                'required' => false,
            );
        }

        return $result;
    }

    protected function getAttributeValues($sIdentifier, $sCustomIdentifier, $sAttributeCode = null, $bFreeText = false)
    {
        $aReturn = null;
        $aValue = $this->getVariationDb()
            ->set('Identifier', $sIdentifier)
            ->set('CustomIdentifier', $sCustomIdentifier)
            ->get('ShopVariation');
        if ($aValue) {
            if ($sAttributeCode !== null) {
                foreach ($aValue as $sKey => $aMatch) {
                    if ($sKey === $sAttributeCode) {
                        $aReturn = isset($aMatch['Values']) ? $aMatch['Values'] : '';
                        break;
                    }
                }
            } else {
                $aReturn = $aValue;
            }
        }
        if($aReturn === null){
            $aReturn = $bFreeText?'':array();
        }
        return $aReturn;
    }

    protected function getErrorValue($sIdentifier, $sCustomIdentifier, $sAttributeCode)
    {
        $aValue = $this->getVariationDb()
            ->set('Identifier', $sIdentifier)
            ->set('CustomIdentifier', $sCustomIdentifier)
            ->get('ShopVariation');
        if ($aValue) {
            foreach ($aValue as $sKey => $aMatch) {
                if ($sKey === $sAttributeCode) {
                    return $aMatch['Error'];
                }
            }
        }

        return false;
    }

    protected function getMPVariationGroups($blFinal)
    {
        return array();
    }

    protected function getShopAttributes()
    {
        if ($this->shopAttributes == null) {
            $shopAllAttributes = MLFormHelper::getShopInstance()->getPrefixedAttributeList(true);
            $shopVariationAttributes = MLFormHelper::getShopInstance()->getAttributeListWithOptions();

            $shopArticleAttributes = array_diff_key($shopAllAttributes, $shopVariationAttributes);
            $shopVariationFixedAttributes = array_intersect_key($shopAllAttributes, $shopVariationAttributes);

            $shopVariationFixedAttributes['separator_line_1'] = self::getMessage('_prepare_variations_separator_line_label');
            $this->shopAttributes = $shopVariationFixedAttributes + $shopArticleAttributes;
        }

        return $this->shopAttributes;
    }

    protected function getShopAttributeValues($sAttributeCode)
    {
        $shopValues = MLFormHelper::getShopInstance()->getPrefixedAttributeOptions($sAttributeCode);
        if (!isset($shopValues) || empty($shopValues)) {
            $shopValues = MLFormHelper::getShopInstance()->getAttributeOptions($sAttributeCode);
        }

        return $shopValues;
    }

    protected function getMPAttributeValues($sCategoryId, $sMpAttributeCode, $sAttributeCode = false)
    {
        $response = MagnaConnector::gi()->submitRequestCached(array('ACTION' => 'GetCategoryDetails', 'DATA' => array('CategoryID' => $sCategoryId)));
        $fromMP = false;
        foreach ($response['DATA']['attributes'] as $key => $attribute) {
            if ($key === $sMpAttributeCode && !empty($attribute['values'])) {
                $aValues = $attribute['values'];
                $fromMP = true;
                break;
            }
        }

        if (!isset($aValues)) {
            if ($sAttributeCode) {
                $shopValues = $this->getShopAttributeValues($sAttributeCode);
                foreach ($shopValues as $value) {
                    $aValues[$value] = $value;
                }
            } else {
                $aValues = array();
            }
        }

        return array(
            'values' => (isset($aValues) ? $aValues : array()),
            'from_mp' => $fromMP
        );
    }

    /**
     * Checks whether there are some items prepared differently than in Variation Matching tab.
     * If so, adds notice to
     *
     * @param $sIdentifier
     * @param $sIdentifierName
     */
    protected function checkAttributesFromDB($sIdentifier, $sIdentifierName)
    {
        // similar validation exists in ML_Productlist_Model_ProductList_Abstract::isPreparedDifferently
        $aValue = MLDatabase::getVariantMatchingTableInstance()->getMatchedVariations($sIdentifier, $this->getCustomIdentifier());

        $oPrepareTable = MLDatabase::getPrepareTableInstance();
        $sShopVariationField = $oPrepareTable->getShopVariationFieldName();
        $marketplaceID = MLModul::gi()->getMarketPlaceId();

        $aPreparedDataQuery = MLDatabase::getDbInstance()->query("
            SELECT $sShopVariationField
            FROM {$oPrepareTable->getTableName()}
            WHERE mpID = $marketplaceID
                AND {$oPrepareTable->getPrimaryCategoryFieldName()} = '$sIdentifier'
        ");

        $bFoundDifferent = false;
        while (($aPreparedData = MLDatabase::getDbInstance()->fetchNext($aPreparedDataQuery)) && !$bFoundDifferent) {
            if (!empty($aPreparedData)) {
                $aPreparedDataValue = json_decode($aPreparedData[$sShopVariationField], true);
                if ($aPreparedDataValue != $aValue) {
                    MLMessage::gi()->addNotice(self::getMessage('_prepare_variations_notice', array('category_name' => $sIdentifierName)));
                    $bFoundDifferent = true;
                }
            }
        }
    }

    protected function getAttributesFromDB($sIdentifier, $sCustomIdentifier = '')
    {
        $aValue = $this->getVariationDb()
            ->set('Identifier', $sIdentifier, false)
            ->set('CustomIdentifier', $sCustomIdentifier)
            ->get('ShopVariation');

        if ($aValue) {
            return $aValue;
        }

        return array();
    }

    protected function getFromApi($actionName, $aData = array())
    {
        try {
            $aResponse = MagnaConnector::gi()->submitRequestCached(array('ACTION' => $actionName, 'DATA' => $aData));
            if ($aResponse['STATUS'] == 'SUCCESS' && isset($aResponse['DATA']) && is_array($aResponse['DATA'])) {
                return $aResponse['DATA'];
            }
        } catch (MagnaException $e) {

        }

        return array();
    }

    /**
     * @return ML_Database_Model_Table_VariantMatching_Abstract
     */
    protected function getVariationDb()
    {
        return MLDatabase::getVariantMatchingTableInstance();
    }

    protected function autoMatch($categoryId, $sMpAttributeCode, &$aAttributes)
    {
        $aMPAttributeValues = $this->getMPAttributeValues($categoryId, $sMpAttributeCode, $aAttributes['Code']);
        $sInfo = self::getMessage('_prepare_variations_auto_matched');
        $blFound = false;
        $allValuesAreMatched = true;
        if ($aAttributes['Values']['0']['Shop']['Key'] === 'all') {
            $newValue = array();
            $i = 0;
            $shopAttributes = $this->getShopAttributeValues($aAttributes['Code']);
            foreach ($shopAttributes as $keyAttribute => $valueAttribute) {
                foreach ($aMPAttributeValues['values'] as $key => $value) {
                    if (strcasecmp($valueAttribute, $value) == 0) {
                        $newValue[$i]['Shop']['Key'] = $keyAttribute;
                        $newValue[$i]['Shop']['Value'] = $valueAttribute;
                        $newValue[$i]['Marketplace']['Key'] = $key;
                        $newValue[$i]['Marketplace']['Value'] = $value;
                        $newValue[$i]['Marketplace']['Info'] = $value . $sInfo;
                        $blFound = true;
                        $i++;
                        break;
                    }
                }
            }

            $aAttributes['Values'] = $newValue;
            if (count($shopAttributes) !== count($newValue)) {
                $allValuesAreMatched = false;
            }
        } else {
            foreach ($aMPAttributeValues['values'] as $key => $value) {
                if (strcasecmp($aAttributes['Values']['0']['Shop']['Value'], $value) == 0) {
                    $aAttributes['Values']['0']['Marketplace']['Key'] = $key;
                    $aAttributes['Values']['0']['Marketplace']['Value'] = $value;
                    $aAttributes['Values']['0']['Marketplace']['Info'] = $value . $sInfo;
                    $blFound = true;
                    break;
                }
            }

            if (!$blFound) {
                $allValuesAreMatched = false;
            }
        }

        if (!$blFound) {
            unset($aAttributes['Values']['0']);
        }

        $this->checkNewMatchedCombination($aAttributes['Values']);

        return $allValuesAreMatched;
    }

    protected function checkNewMatchedCombination(&$aAttributes)
    {
        foreach ($aAttributes as $key => $value) {
            if ($key === 0) {
                continue;
            }

            if (isset($aAttributes['0']) && $value['Shop']['Key'] === $aAttributes['0']['Shop']['Key']) {
                unset($aAttributes[$key]);
                break;
            }
        }
    }

    protected static function getMessage($sIdentifier, $aReplace = array())
    {
        return MLI18n::gi()->get(MLModul::gi()->getMarketPlaceName() . $sIdentifier, $aReplace);
    }

    protected function arrayFilterKey($input, $callback)
    {
        if (!is_array($input)) {
            trigger_error('array_filter_key() expects parameter 1 to be array, ' . gettype($input) . ' given', E_USER_WARNING);
            return null;
        }

        if (empty($input)) {
            return $input;
        }

        $filteredKeys = array_filter(array_keys($input), $callback);
        if (empty($filteredKeys)) {
            return array();
        }

        $input = array_intersect_key(array_flip($filteredKeys), $input);

        return $input;
    }
}
