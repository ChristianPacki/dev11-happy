<?php
class Redgecko_Magnalister_Model_Product_Observer{
    protected $sStatus='';
    public function save_after($oObserver){
        $oOrder=$oObserver->getProduct();
        return $this;
    }
    public function save_before($oObserver){
        $oProduct=$oObserver->getProduct();
        return $this;
    }
}