<?php
class Redgecko_Magnalister_Model_Carrier_ShippingMethod extends Mage_Shipping_Model_Carrier_Abstract
{
  protected $_code = 'magnalister';
  public function collectRates(Mage_Shipping_Model_Rate_Request $request){
    $result = Mage::getModel('shipping/rate_result');
    $rate = Mage::getModel('shipping/rate_result_method');
    $result->append($rate);
 
    return $result;
  }
}