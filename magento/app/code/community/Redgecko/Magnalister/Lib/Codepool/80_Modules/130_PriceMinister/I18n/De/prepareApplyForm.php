<?php
/**
 * 888888ba                 dP  .88888.                    dP
 * 88    `8b                88 d8'   `88                   88
 * 88aaaa8P' .d8888b. .d888b88 88        .d8888b. .d8888b. 88  .dP  .d8888b.
 * 88   `8b. 88ooood8 88'  `88 88   YP88 88ooood8 88'  `"" 88888"   88'  `88
 * 88     88 88.  ... 88.  .88 Y8.   .88 88.  ... 88.  ... 88  `8b. 88.  .88
 * dP     dP `88888P' `88888P8  `88888'  `88888P' `88888P' dP   `YP `88888P'
 *
 *                          m a g n a l i s t e r
 *                                      boost your Online-Shop
 *
 * -----------------------------------------------------------------------------
 * $Id$
 *
 * (c) 2010 - 2014 RedGecko GmbH -- http://www.redgecko.de
 *     Released under the MIT License (Expat)
 * -----------------------------------------------------------------------------
 */

MLI18n::gi()->add('priceminister_prepare_apply_form', array(
    'legend' => array(
        'details' => 'Produktdetails',
        'categories' => 'Kategorie',
        'variationmatching' => array('PriceMinister Erforderliche Attribute', 'Attribut- und Attributswert-Matching'),
        'variationmatchingoptional' => array('PriceMinister Optionale Attribute', 'Attribut- und Attributswert-Matching'),
        'subcategories' => 'PriceMinister Subcategories',
        'advert' => 'Allgemeine Einstellungen',
    ),
    'field' => array(
        'variationgroups' => array(
            'label' => 'PriceMinister Kategorien',
        ),
        'variationgroups.value' => array(
            'label' => '1. Marktplatz-Kategorie:',
        ),
        'webshopattribute' => array(
            'label' => 'Web-Shop Attribut',
        ),
        'itemtitle' => array(
            'label' => 'Titel',
            'hint' => 'Titel max. 200 Zeichen<br>Erlaubte Platzhalter: <br> #BASEPRICE# - Grundpreis',
        ),
        'description' => array(
            'label' => 'Beschreibung',
            'hint'  => 'Maximal 4000 Zeichen.<br>Liste verf&uuml;gbarer Platzhalter f&uuml;r die Produktbeschreibung:<dl><dt>#TITLE#</dt><dd>Produktname (Titel)</dd><dt>#ARTNR#</dt><dd>Artikelnummer</dd><dt>#PID#</dt><dd>Produkt-ID</dd><dt>#SHORTDESCRIPTION#</dt><dd>Kurzbeschreibung aus dem Shop</dd><dt>#DESCRIPTION#</dt><dd>Beschreibung aus dem Shop</dd><dt>#PICTURE1#</dt><dd>erstes Produktbild</dd><dt>#PICTURE2# etc.</dt><dd>zweites Produktbild, mit #PICTURE3#, #PICTURE4# usw. können weitere Bilder übermittelt werden, so viele wie im Shop vorhanden.</dd></dl>',
        ),
        'images' => array(
            'label' => 'Produktbilder',
            'hint'  => 'Maximum 10 images. Images should be at least 480x640 in resolution.',
        ),
        'price' => array(
            'label' => 'Preis',
        ),
        'itemcondition' => array(
            'label' => 'Zustand',
        ),
        'ean' => array(
            'label' => 'EAN',
        ),
    ),
), false);

MLI18n::gi()->add('priceminister_prepare_variations', array(
    'legend' => array(
        'variations' => 'Kategorie von PriceMinister ausw&auml;hlen',
        'attributes' => 'Attributsnamen von PriceMinister ausw&auml;hlen',
        'variationmatching' => array('PriceMinister Erforderliche Attribute', 'Attribut- und Attributswert-Matching'),
        'variationmatchingoptional' => array('PriceMinister Optionale Attribute', 'Attribut- und Attributswert-Matching'),
        'action' => '{#i18n:form_action_default_legend#}',
    ),
    'field' => array(
        'variationgroups' => array(
            'label' => 'PriceMinister Kategorie',
        ),
        'variationgroups.value' => array(
            'label' => '1. Marktplatz-Kategorie:',
        ),
        'deleteaction' => array(
            'label' => '{#i18n:ML_BUTTON_LABEL_DELETE#}',
        ),
        'groupschanged' => array(
            'label' => '',
        ),
        'attributename' => array(
            'label' => 'Attributsnamen',
        ),
        'attributenameajax' => array(
            'label' => '',
        ),
        'customidentifier' => array(
            'label' => 'Bezeichner',
        ),
        'webshopattribute' => array(
            'label' => 'Web-Shop Attribut',
        ),
        'saveaction' => array(
            'label' => 'SPEICHERN UND SCHLIESSEN',
        ),
        'resetaction' => array(
            'label' => '{#i18n:priceminister_varmatch_reset_matching#}',
            'confirmtext' => '{#i18n:priceminister_prepare_variations_reset_info#}',
        ),
        'attributematching' => array(
            'matching' => array(
                'titlesrc' => 'Shop-Wert',
                'titledst' => 'PriceMinister-Wert',
            ),
        ),
    ),
), false);

MLI18n::gi()->priceminister_prepareform_max_length_part1 = 'Maxpriceminister';
MLI18n::gi()->priceminister_prepareform_max_length_part2 = 'attribute is';
MLI18n::gi()->priceminister_prepareform_category = 'PriceMinister Kategorien is mandatory.';
MLI18n::gi()->priceminister_prepare_form_itemtitle = 'Title attribute is mandatory, and must be between 5 and 64 characters';
MLI18n::gi()->priceminister_prepare_form_description = 'Description attribute is mandatory, and must be between 10 and 4000 characters';
MLI18n::gi()->priceminister_prepareform_category_attribute = ' category attribute is mandatory.';
MLI18n::gi()->priceminister_prepare_variations_title = 'Attributes Matching';
MLI18n::gi()->priceminister_prepare_apply = 'Neue Produkte erstellen';
MLI18n::gi()->priceminister_category_no_attributes = 'There are no attributes for this category.';
MLI18n::gi()->priceminister_prepare_variations_choose_mp_value = 'Verwende PriceMinister Attributswert';
MLI18n::gi()->priceminister_prepare_variations_separator_line_label = '-------------------------------------------------------------';
MLI18n::gi()->priceminister_prepare_variations_free_text = 'Eigene Angaben machen';
MLI18n::gi()->priceminister_prepare_variations_mandatory_fields_info = '<b>Hinweis:</b> Die mit <span class="bull">&bull;</span> markierten Felder sind Pflichtfelder und m&uuml;ssen ausgef&uuml;llt werden.';
MLI18n::gi()->priceminister_prepare_variations_category_without_attributes_info = 'Für die ausgewählte Kategorie unterstützt Priceminister keine Attribute.';
MLI18n::gi()->priceminister_prepare_variations_error_text = 'Das Attribute {#attribute_name#} ist ein Pflichtfeld. Bitte ordnen Sie alle Werte zu.';
MLI18n::gi()->priceminister_prepare_variations_error_missing_value = 'Attribute {#attribute_name#} is mandatory, and your product does not have value for chosen matched attribute from shop.';
MLI18n::gi()->priceminister_prepare_variations_error_free_text = ': Das Freitext Feld darf nicht leer sein.';
MLI18n::gi()->priceminister_prepare_variations_matching_table = 'Gematchte';
MLI18n::gi()->priceminister_prepare_variations_notice = 'Bitte beachten Sie, dass Sie einige Artikel der gew&auml;hlten Kategorie "{#category_name#}" abweichend unter „Produkte vorbereiten“ gematcht haben. Es werden die dort gespeicherten Werte zum Marktplatz &uuml;bermittelt.';
MLI18n::gi()->priceminister_prepare_match_notice_not_all_auto_matched = 'Es konnten nicht alle ausgewählten Werte gematcht werden. Nicht-gematchte Werte werden weiterhin in den DropDown-Feldern angezeigt. Bereits gematchte Werte werden in der Produktvorbereitung berücksichtigt.';
MLI18n::gi()->priceminister_prepare_match_variations_saved = 'Erfolgreich gespeichert.';
MLI18n::gi()->priceminister_prepare_variations_saved = 'Es wurden alle ausgewählten Werte erfolgreich gematcht. Sie können nun Produkte mit den gematchten Attributen vorbereiten, oder mit dem Matching hier fortfahren.';
MLI18n::gi()->priceminister_prepare_variations_reset_success = 'Das Matching wurde aufgehoben.';
MLI18n::gi()->priceminister_prepareform_title = 'Bitte geben Sie einen Titel an.';
MLI18n::gi()->priceminister_prepareform_description = 'Bitte geben Sie eine Artikelbeschreibung an.';
MLI18n::gi()->priceminister_prepare_variations_manualy_matched = ' - (manuel zugeordnet)';
MLI18n::gi()->priceminister_prepare_variations_free_text_add = ' - (eigene angaben)';
MLI18n::gi()->priceminister_prepare_variations_change_attribute_info = 'Bevor Sie Attribute &auml;ndern k&ouml;nnen, heben Sie bitte alle Matchings zuvor auf.';
MLI18n::gi()->priceminister_varmatch_attribute_different_on_product = 'Hinweis: Sie haben diesen Wert abweichend zum globalen Attributs-Matching gespeichert.';
MLI18n::gi()->priceminister_prepare_variations_auto_matched = ' - (automatisch zugeordnet)';
MLI18n::gi()->priceminister_varmatch_attribute_value_deleted_from_mp = 'This attribute value was deleted or changed by PriceMinister. Therefore, matchings were voided. If required, please match again with a proper PriceMinister attribute value.';
MLI18n::gi()->priceminister_varmatch_attribute_changed_on_mp = 'Achtung: PriceMinister hat einige bereits gematchte Attributswerte nachträglich entfernt oder geändert. 
Bitte überprüfen Sie Ihre Zuordnungen und matchen die betroffenen Werte bei Bedarf erneut.';
MLI18n::gi()->priceminister_prepare_variations_already_matched = '(bereits gematcht)';
MLI18n::gi()->priceminister_prepare_variations_reset_info = 'Wollen Sie das Matching wirklich aufheben?';
