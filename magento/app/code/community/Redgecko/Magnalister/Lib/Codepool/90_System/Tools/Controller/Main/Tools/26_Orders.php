<?php

MLFilesystem::gi()->loadClass('Core_Controller_Abstract');

class ML_Tools_Controller_Main_Tools_Orders extends ML_Core_Controller_Abstract {
    protected $aParameters=array('controller');
    protected $aData = null;
    
    protected function getRequestedOrderSpecial () {
        return $this->getRequest('orderspecial');
    }
    
    protected function getOrderData () {
        $oOrder = MLOrder::factory()->getByMagnaOrderId($this->getRequestedOrderSpecial());
        if ($oOrder->exists()) {
            if ($this->aData === null) {
                if ($this->getRequest('action') === 'unacknowledge') {
                    try {
                        $this->aData = array('unAcknowledgeImportedOrder' => $oOrder->unAcknowledgeImportedOrder($oOrder->get('platform'), $oOrder->get('mpid'), $oOrder->get('special'), $oOrder->get('orders_id')));
                    } catch (Exception $oEx) {
                        $this->aData = array('Exception' => $oEx->getMessage());
                    }
                } else {
                    ML::gi()->init(array('mp' => $oOrder->get('mpid')));
                    $this->aData = array(
                        '$oOrder->data()' => $oOrder->data(),
                        'shop' => array(
                            '$oOrder->getShopOrderStatus()' => $oOrder->getShopOrderStatus(),
                            '$oOrder->getShopOrderLastChangedDate()' => $oOrder->getShopOrderLastChangedDate(),
                            '$oOrder->getShippingDateTime()' => $oOrder->getShippingDateTime(),
                            '$oOrder->getShippingCarrier()' => $oOrder->getShippingCarrier(),
                            '$oOrder->getShippingTrackingCode()' => $oOrder->getShippingTrackingCode(),
                        ),
                    );
                    ML::gi()->init(array());
                }
            }
            return $this->aData;
        }
    }
}
