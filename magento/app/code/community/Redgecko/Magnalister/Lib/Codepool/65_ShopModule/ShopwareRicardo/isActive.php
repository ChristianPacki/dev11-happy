<?php

global $magnaConfig;
return (
        isset($magnaConfig['maranon']['Marketplaces'][MLRequest::gi()->data('mp')]) && $magnaConfig['maranon']['Marketplaces'][MLRequest::gi()->data('mp')] == 'ricardo' //ricardo module activation
    && (class_exists('Enlight_Application', false) && Enlight_Application::Instance()->App() === 'Shopware') //shopware shop activation
    );
