<?php

class ML_Shopware_Helper_Model_Product {

    public function getProductSelectQuery() {
        return MLDatabase::factorySelectClass()
                        ->select('DISTINCT p.id')
                        ->from(Shopware()->Models()->getClassMetadata('Shopware\Models\Article\Article')->getTableName(), 'p')
                        ->join(array(Shopware()->Models()->getClassMetadata('Shopware\Models\Article\Detail')->getTableName(), 'details', 'p.id = details.articleid AND p.main_detail_id = details.id '), ML_Database_Model_Query_Select::JOIN_TYPE_INNER)
                        ->join(array(Shopware()->Models()->getClassMetadata('Shopware\Models\Article\Price')->getTableName(), 'pp', 'pp.articledetailsID = details.id'), ML_Database_Model_Query_Select::JOIN_TYPE_INNER);
    }

    /**
     * Get name from a certain article by ordernumber
     * @param string $ordernumber
     * @param bool $returnAll return only name or additional data, too
     * @access public
     * @return string or array
     */
    public function getTranslatedInfo($ordernumber) {
        $checkForArticle = Shopware()->Db()->fetchRow("
            SELECT s_articles.id, s_articles.name AS articleName, description ,description_long,keywords  FROM s_articles WHERE
            id=?
		", array($ordernumber));
        if (!empty($checkForArticle)) {
            return Shopware()->Modules()->Articles()->sGetTranslation($checkForArticle, $checkForArticle["id"], "article");
        } else {
            return false;
        }
    }

    /**
     * use sConfigurator::getDefaultPrices
     * @param type $detailId
     * @return type
     */
    public function getDefaultPrices($detailId, $sUserGroup = null) {
        if ($sUserGroup === null) {
            $sUserGroup = Shopware()->System()->sUSERGROUPDATA['key'];
        }
        $oBbuilder = Shopware()->Models()->createQueryBuilder();
        $aPriceRows = $oBbuilder->select(array('prices'))
                ->from('Shopware\Models\Article\Price', 'prices')
                ->where('prices.articleDetailsId = :detailId')
                ->andWhere('prices.customerGroupKey = :key')
                ->setParameter('detailId', $detailId)
                ->setParameter('key', $sUserGroup)
                ->orderBy('prices.from', 'ASC')
                ->getQuery()
                ->getArrayResult();

        if (count($aPriceRows) <= 0 && $sUserGroup == 'EK') {
            throw new Exception('Error to get Price : there is no Price for this product. Detail id = ' . $detailId);
        }
        $aPrice = count($aPriceRows) > 0 ? array_shift($aPriceRows) : array('price' => $this->getDefaultPrices($detailId, 'EK'));
        return $aPrice['price'];
    }

    /**
     * return all variants related to this product id
     * @param int $iProductId
     * @return array
     */
    public function getProductDetails($iProductId) {
        $oShopwareProduct = Shopware()->Models()->getRepository('Shopware\Models\Article\Article')->find($iProductId);
        /* @var $oShopwareProduct Shopware\Models\Article\Article */
        $oQueryBuilder = Shopware()->Models()->createQueryBuilder();
        $oQueryBuilder->select(array('details', 'attribute', 'prices', 'configuratorOptions', 'configuratorGroup'))->distinct('details.id')
                ->from('Shopware\Models\Article\Detail', 'details')
                ->leftJoin('details.configuratorOptions', 'configuratorOptions')
                ->leftJoin('configuratorOptions.group', 'configuratorGroup')
                ->leftJoin('details.prices', 'prices')
                ->leftJoin('details.attribute', 'attribute')
        ;

        $mConfiguratorSet = $oShopwareProduct->getConfiguratorSet();
        if (empty($mConfiguratorSet)) {
            $oQueryBuilder->where('details.articleId = ?1 AND details.id = ?2 ')
                    ->setParameter(2, $oShopwareProduct->getMainDetail()->getId());
        } else {
            $oQueryBuilder->where('details.articleId = ?1 ');
        }
        $oQueryBuilder->setParameter(1, $iProductId);
        return $oQueryBuilder->getQuery()->getArrayResult();
    }

    public function getAllProperties()
    {
        $aResult = array();

        $oBuilder = Shopware()->Models()->createQueryBuilder()
            ->from('Shopware\Models\Property\Option', 'po')
            ->select(array('PARTIAL po.{id,name}'));

        foreach ($oBuilder->getQuery()->getArrayResult() as $option) {
            $option['name'] = $this->translate($option['id'], 'propertyoption', 'optionName', $option['name']);
            $aResult[$option['id']] = $option;
        }

        return $aResult;
    }

    public function getPropertyValuesFor($iArticleId, $iOptionId)
    {
        $result = array();

        $oBuilder = Shopware()->Models()->createQueryBuilder()
            ->select(array('PARTIAL pv.{id,value}'))
            ->from('Shopware\Models\Property\Value', 'pv')
            ->innerJoin('pv.articles', 'pa', 'with', 'pa.id = :articleId')
            ->innerJoin('pv.option', 'po', 'with', 'po.id = :optionId')
            ->setParameter('articleId', $iArticleId)
            ->setParameter('optionId', $iOptionId);

        $aValues = $oBuilder->getQuery()->getArrayResult();
        foreach ($aValues as $value) {
            $value['value'] = $this->translate($value['id'], 'propertyvalue', 'optionValue', $value['value']);
            $result[$value['id']] = $value['value'];
        }

        return $result;
    }

    public function getProperties($iArticleId, $iPropertyGroupId) {
        $aProperties = array();
        $dbalConnection = Shopware()->Models()->getConnection();
        $aPropertiesData = $dbalConnection->fetchAll('SELECT fo.name, fv.value, fv.optionID, fv.id '
                . 'FROM s_filter_values fv '
                . 'INNER JOIN s_filter_articles fa ON fv.id = fa.valueID '
                . 'INNER JOIN s_articles a ON a.id = fa.articleID AND (a.id = :articleId) '
                . 'INNER JOIN s_filter_options fo ON fv.optionID = fo.id '
                . 'INNER JOIN s_filter_relations fr ON fo.id = fr.optionID '
                . 'INNER JOIN s_filter f ON f.id = fr.groupID AND (f.id = :propertyGroupId) ORDER BY fr.position ASC', array(
            'articleId' => $iArticleId,
            'propertyGroupId' => $iPropertyGroupId)
                )
        ;
        foreach ($aPropertiesData as $aProp) {
            $sName = $this->translate($aProp['optionID'], 'propertyoption', 'optionName', $aProp['name']);
            $sValue = $this->translate($aProp['id'], 'propertyvalue', 'optionValue', $aProp['value']);
            $aProperties[$sName][] = $sValue;
        }
        return $aProperties;
    }

    public function translate($iId, $sType, $sTranlationIndex, $sFallback) {
        $translationWriter = new \Shopware_Components_Translation();
        $iLanguage = Shopware()->Shop()->getId();
        $aTranslate = $translationWriter->read($iLanguage, $sType, $iId);
        $sTranslate = '';
        if (empty($aTranslate) || !isset($aTranslate[$sTranlationIndex])) {
            $sTranslate = $sFallback;
        } else {
            $sTranslate = $aTranslate[$sTranlationIndex];
        }
        return $sTranslate;
    }

    public function getFreeTextFieldValue(\Shopware\Models\Article\Detail $oArticle, $aField) {
        $oAttribute = $oArticle->getAttribute();
        if(!is_object($oAttribute)){
            return array(
                'description' => '',
                'value' => ''
            );
        }
                
        $sValue = '';
        if (method_exists($oAttribute, 'get' . $aField['name'])) {
            $sValue = $oAttribute->{'get' . $aField['name']}();
        } else {
            $sTableName = Shopware()->Models()->getClassMetadata('Shopware\Models\Attribute\Article')->getTableName();
            if (MLDatabase::getDbInstance()->columnExistsInTable($aField['name'], $sTableName)) {
                $sValue = Shopware()->Db()
                        ->fetchOne('select `' . $aField['name'] . '` from ' . $sTableName . ' where id=' . $oAttribute->getId());
            }
        }

        $oTranslationWriter = new \Shopware_Components_Translation();
        if ($oArticle->getKind() != 1) {
            $aTranslated = $oTranslationWriter->read(Shopware()->Shop()->getId(), 'variant', $oArticle->getId());
        } else {
            $aTranslated = $oTranslationWriter->read(Shopware()->Shop()->getId(), 'article', $oArticle->getArticle()->getId());
        }
        $aLabelsTranslated = array();
        $oQueryBuilder = Shopware()->Models()->createQueryBuilder();
        $oQuery = $oQueryBuilder
                ->select('snippet.name,snippet.value')
                ->from('Shopware\Models\Snippet\Snippet', 'snippet')
                ->where("
                snippet.namespace = 'frontend/detail/index' 
                AND snippet.localeId = " . Shopware()->Shop()->getLocale()->getId() . "
                AND snippet.name like 'DetailAttributeField%Label'
            ")
                ->getQuery();
        $data = $oQuery->getArrayResult();
        
        foreach ($data as $aRow) {
            $aLabelsTranslated ['attr' . substr($aRow['name'], strlen('DetailAttributeField'), -strlen('Label'))] = $aRow['value'];
        }
        if (!isset($aField['translatable']) || !isset($aField['label'])) {
            foreach ($this->getAttributeFields() as $aAtrribute) {
                if ($aField['name'] == $aAtrribute['name']) {
                    $aField = $aAtrribute;
                    break;
                }
            }
        }
        if (trim($sValue) != '') {
            if ($aField['translatable']) {
                if (array_key_exists($aField['name'], $aTranslated)) {
                    $sValue = $aTranslated[$aField['name']];
                }else if (array_key_exists('__attribute_'.$aField['name'], $aTranslated)) {//shopwarer 5
                    $sValue = $aTranslated['__attribute_'.$aField['name']];
                }
                if (array_key_exists($aField['name'], $aLabelsTranslated)) {
                    $aField['label'] = $aLabelsTranslated[$aField['name']];
                }
            }
            return array(
                'description' => $aField['label'],
                'value' => $sValue
            );
        }
        return array(
            'description' => '',
            'value' => ''
        );
    }

    static $aAttributeFields = array();

    public function getAttributeFields() {
        if (count(self::$aAttributeFields) == 0) {
            try {//shopware 5.2
                $sTableName = Shopware()->Models()->getClassMetadata('Shopware\Models\Attribute\Article')->getTableName();
                $columns = Shopware()->Container()->get('shopware_attribute.crud_service')->getList($sTableName);
                self::$aAttributeFields = json_decode(json_encode($columns), true);
                foreach (self::$aAttributeFields as &$aField) {
                    $aField['name'] = $aField['columnName'];
                }
            } catch (Exception $oEx) {//shopware < 5.2
                $builder = Shopware()->Models()->createQueryBuilder();
                self::$aAttributeFields = $builder->select(array('elements'))
                        ->from('Shopware\Models\Article\Element', 'elements')
                        ->orderBy('elements.position')
                        ->getQuery()
                        ->getArrayResult();
                foreach (self::$aAttributeFields as &$aField) {
                    $aField['configured'] = true;
                }
            }
        }
        return self::$aAttributeFields;
    }

    /**
     * if product has any variation it i will return sum of quantity of all variation, otherwise it will return quantiy of product
     * @param type $oArticle
     * @return int
     */
    public function getTotalCount($oArticle) {
        $iStock = $oArticle->getMainDetail()->getInStock();
        $mConfiguratorSet = $oArticle->getConfiguratorSet();
        if(!empty($mConfiguratorSet)){
            try{
                $oQueryBuilder = Shopware()->Models()->createQueryBuilder();
                $oQuery = $oQueryBuilder->select('details.inStock','details.id')->distinct('details.id')
                    ->from('Shopware\Models\Article\Detail', 'details')
                    ->leftJoin('details.configuratorOptions', 'configuratorOptions')
                    ->leftJoin('details.prices', 'prices')
                    ->where('details.articleId = ?1 AND configuratorOptions.id is not NULL AND prices.id is not NULL')
                    ->setParameter(1, $oArticle->getId())
                    ->getQuery();
                
                $aStock = $oQuery->getResult(); 
                $iStock = 0;
                foreach($aStock as $aRow){
                    $iStock += (int)$aRow['inStock'];
                }
            }  catch (Exception $oExc){}
        }
        return $iStock;
    }

}
