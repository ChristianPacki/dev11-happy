<?php

MLFilesystem::gi()->loadClass('Dawanda_Helper_Model_Service_OrderData_Normalize');

class ML_ShopwareDawanda_Helper_Model_Service_OrderData_Normalize extends ML_Dawanda_Helper_Model_Service_OrderData_Normalize {
    
    protected function normalizeOrder () {
        parent::normalizeOrder();
        $this->aOrder['Order']['PaymentStatus'] = MLModul::gi()->getConfig('orderimport.paymentstatus');
        return $this;
    }
    
    protected function getPaymentCode($aTotal) {
        return MLModul::gi()->getConfig('orderimport.paymentmethod') == '__automatic__' ? $aTotal['Code'] : MLModul::gi()->getConfig('orderimport.paymentmethod');
    }
}
