<?php

/**
 * 888888ba                 dP  .88888.                    dP                
 * 88    `8b                88 d8'   `88                   88                
 * 88aaaa8P' .d8888b. .d888b88 88        .d8888b. .d8888b. 88  .dP  .d8888b. 
 * 88   `8b. 88ooood8 88'  `88 88   YP88 88ooood8 88'  `"" 88888"   88'  `88 
 * 88     88 88.  ... 88.  .88 Y8.   .88 88.  ... 88.  ... 88  `8b. 88.  .88 
 * dP     dP `88888P' `88888P8  `88888'  `88888P' `88888P' dP   `YP `88888P' 
 *
 *                          m a g n a l i s t e r
 *                                      boost your Online-Shop
 *
 * -----------------------------------------------------------------------------
 * $Id$
 *
 * (c) 2010 - 2014 RedGecko GmbH -- http://www.redgecko.de
 *     Released under the MIT License (Expat)
 * -----------------------------------------------------------------------------
 */
/** @var ML_Hitmeister_Controller_Hitmeister_Prepare_Variations $this */
class_exists('ML', false) or die();
$marketplaceName = MLModul::gi()->getMarketPlaceName();
$mParentValue = $this->getField('variationgroups.value', 'value');

// Getting type of tab (is it variation tab or apply form)
$sChangedSelector = ' ' . $aField['id'];
$ini = strpos($sChangedSelector, $marketplaceName . '_prepare_');
if ($ini == 0) return '';
$ini += strlen($marketplaceName . '_prepare_');
$len = strpos($sChangedSelector, '_field', $ini) - $ini;
$tabType = substr($sChangedSelector, $ini, $len);

//Check if collapsing field should be set
$aActions = $this->getRequest($this->sActionPrefix);
$sAction = isset($aActions['prepareaction']) ? $aActions['prepareaction'] : '';

if (is_array($mParentValue)) {
    reset($mParentValue);
    $mParentValue = key($mParentValue);
}

$sCustomAttribute = $this->getField('attributename', 'value');
if ($sCustomAttribute !== null) {
    $mParentValue = $sCustomAttribute;
}

if (strpos($mParentValue, ':') !== false) {
    $mParentValue = explode(':', $mParentValue);
    $mParentValue = $mParentValue[0];
}

$i18n = $this->getFormArray('aI18n');
if (!empty($mParentValue) && $mParentValue !== 'none' && $mParentValue !== 'new') {
    $aShopAttributes = $this->getShopAttributes();
    $dModificationDate = $this->getModificationDate($mParentValue);

    $aShopAttributes['separator_line_2'] = MLI18n::gi()->get($marketplaceName . '_prepare_variations_separator_line_label');
    $aShopAttributes['freetext'] = MLI18n::gi()->get($marketplaceName . '_prepare_variations_free_text');
    $aShopAttributes['attribute_value'] = MLI18n::gi()->get($marketplaceName . '_prepare_variations_choose_mp_value');
    $aMPAttributes = $this->getMPVariationAttributes($mParentValue);

    $aFieldset = array(
        'id' => $this->getIdent() . '_fieldset_' . $mParentValue,
        'legend' => array(
            'i18n' => $i18n['legend']['variationmatching'],
            'template' => 'two-columns',
        ),
        'row' => array(
            'template' => 'default',
        ),
    );
    $aFieldsetOptional = array(
        'id' => $this->getIdent() . '_fieldset_optional_' . $mParentValue,
        'legend' => array(
            'i18n' => $i18n['legend']['variationmatchingotional'],
            'template' => 'two-columns',
        ),
        'row' => array(
            'template' => 'default',
        ),
        'fields' => array(),
    );
    $aFieldsetCustom = array(
        'id' => $this->getIdent() . '_fieldset_custom_' . $mParentValue,
        'legend' => array(
            'i18n' => $i18n['legend']['variationmatchingcustom'],
            'template' => 'two-columns',
        ),
        'row' => array(
            'template' => 'default',
        ),
        'fields' => array(),
    );

    $optionalAttributesMap = array();

    foreach ($aMPAttributes as $key => $sAttribute) {
        $aMatchedAttributes = $this->getAttributeValues($mParentValue, '', $key);
        $sAttribute['custom'] = !empty($sAttribute['custom']) ? $sAttribute['custom'] : false;
        $sBaseName = "field[variationgroups][$mParentValue][$key]";
        $sName = $sBaseName . '[Code]';
        $sId = 'variationgroups.' . $mParentValue . '.' . $key . '.code';
        $sKind = !empty($sAttribute['values']) ? 'Matching' : 'FreeText';
        $bError = $this->getErrorValue($mParentValue, '', $key);

        $aSelectField = $this->getField($sId);
        $aSelectField['type'] = 'select';
        if (!empty($aSelectField['value'])) {
            $aSelectField['values'] = $aShopAttributes;
        }

        $aSelectField['name'] = $sName;
        $aSelectField['i18n'] = $i18n['field']['webshopattribute'];
        $style = '';
        if ($bError == true) {
            $aSelectField['cssclass'] = 'error';
            $style = 'color:red';
        }

        $aAjaxField = $this->getField($sId . '_ajax');
        $aAjaxField['type'] = 'attributeajax';
        $aAjaxField['cascading'] = true;
        $aAjaxField['breakbefore'] = true;
        $aAjaxField['padding-right'] = 0;
        $aAjaxField['i18n']['label'] = '';
        if (isset($aSelectField['value']) && $aSelectField['value'] != null) {
            // value field on ajax is used to initialize cascading ajax fields in attributematch.php 
            // when variation group is selected
            $aAjaxField['value'] = array(
                $key => $aSelectField['value'],
                'name' => 'variationgroups.' . $mParentValue . '.' . $key,
            );
        }

        $aAjaxField['ajax'] = array(
            'selector' => '#' . $aSelectField['id'],
            'trigger' => 'change',
            'field' => array(
                'id' => $sId . '_ajax_field',
                'type' => 'attributematch',
            ),
        );

        $aSubfield = $this->getField($sId . '_sub');
        $aSubfield['type'] = 'subFieldsContainer';
        $aSubfield['i18n']['hint'] = isset($sAttribute['desc']) ? $sAttribute['desc'] : '';
        $sAttributeValue = (!empty($sAttribute['customAttributeValue']) && $sAttribute['customAttributeValue'] !== null) ? $sAttribute['customAttributeValue'] : $sAttribute['value'];
        $aSubfield['i18n']['label'] = '<p style="display: inline-table;' . $style . '">' . $sAttributeValue . (($sAttribute['required']) ? '<span class="bull">&bull;</span></p>' : '');
        $aSubfield['subfields']['select'] = $aSelectField;
        $aSubfieldExtra = $this->getField($sId . '_sub');
        $aSubfieldExtra['type'] = 'subFieldsContainer';

        if (!empty($aMatchedAttributes)) {
            if (!empty($aSelectField['value'])) {
                $aSubfieldExtra['subfields']['deletebutton'] = array(
                    'id' => $sId . '_button_matching_delete',
                    'type' => 'deletematchingbutton',
                    'name' => $sBaseName,
                    'i18n' => array(
                        'info' => MLI18n::gi()->get($marketplaceName . '_prepare_variations_already_matched'),
                    ),
                    'float' => 'left',
                );
            }

            if (!empty($aSelectField['value']) && !empty($sAttribute['changed']) && !empty($dModificationDate) &&
                strtotime($sAttribute['changed']) > strtotime($dModificationDate)) {
                $aSubfieldExtra['subfields']['warning'] = array(
                    'type' => 'warning',
                    'name' => $sBaseName . '[Warning]',
                    'id' => $sId . '_warning',
                    'i18n' => array(
                        'title' => $sAttribute['value'],
                        'text' => MLI18n::gi()->get($marketplaceName . '_varmatch_attribute_changed_on_mp'),
                    ),
                );
            }

            if (!empty($aSelectField['value']) && isset($sAttribute['modified']) && $sAttribute['modified'] !== false) {
                $aSubfieldExtra['subfields']['warning'] = array(
                    'type' => 'warning',
                    'name' => $sBaseName . '[Warning]',
                    'id' => $sId . '_warning',
                    'i18n' => array(
                        'title' => $sAttribute['value'],
                        'text' => MLI18n::gi()->get($marketplaceName . '_varmatch_attribute_different_on_product'),
                    ),
                );
            }

            if (!empty($aSelectField['value']) && $tabType !== 'variations' && is_array($aMatchedAttributes)) {
                $aSubfieldExtra['subfields']['collapsebutton'] = array(
                    'id' => $sId . '_button_matching_collapse',
                    'type' => 'collapsebutton',
                    'name' => $sBaseName . '[Collapse]',
                    'float' => 'right',
                    'padding-right' => 0,
                );
            }

            if ($sAction === $key) {
                $aSubfieldExtra['Expend'] = true;
            } else {
                $aSubfieldExtra['Expend'] = false;
            }
        } else if (!$sAttribute['required']) {
            if (!$sAttribute['custom']) {
                $aSubfield['classes'] = !empty($aSubfield['classes']) ? $aSubfield['classes'] : array();
                $aSubfield['classes'] = array_merge($aSubfield['classes'], array('hide', 'optionalAttribute', $aSubfield['id']));
                $optionalAttributesMap[$aSelectField['id']] = $aSubfield['i18n']['label'];
            }
            $aSubfieldExtra['subfields']['addebutton'] = array(
                'id' => $sId . '_button_matching_add',
                'type' => 'addmatchingbutton',
                'classes' => array('doNotHide'),
                'value' => $key,
                'float' => 'left',
            );
        }

        $aAttributeMatchingSubfields = array(
            'hidden' => array(
                'type' => 'hidden',
                'name' =>  $sBaseName . '[Kind]',
                'id' => $sId . '_kind',
                'value' => $sKind,
                'padding-right' => 0,
            ),
            'secondhidden' => array(
                'type' => 'hidden',
                'name' =>  $sBaseName . '[Required]',
                'id' => $sId . '_required',
                'value' => $sAttribute['required'] ? true : false, // has to be bool
                'padding-right' => 0,
            ),
            'thirdhidden' => array(
                'type' => 'hidden',
                'name' =>  $sBaseName . '[AttributeName]',
                'id' => $sId . '_attribute_name',
                'value' => $sAttribute['value'],
                'padding-right' => 0,
            ),
        );

        if (!empty($sAttribute['custom'])) {
            $aAttributeMatchingSubfields['customattributetext'] = array(
                'type' => empty($sAttribute['customAttributeValue']) ? 'string' : 'hidden',
                'name' => $sBaseName . '[CustomName]',
                'id' => $aSelectField['id'] . '_custom_name',
                'value' => !empty($sAttribute['customAttributeValue']) ? $sAttribute['customAttributeValue'] : '',
                'padding-right' => 0,
            );
        }

        $aSubfield['subfields'] = array_merge($aSubfield['subfields'], $aAttributeMatchingSubfields);

        if ($sAttribute['required']) {
            $aFieldset['fields'][] = array(
                'subFieldsContainer' => $aSubfield,
                'subFieldsContainerExtra' => $aSubfieldExtra,
                'ajax' => $aAjaxField,
            );
        } else if ($sAttribute['custom']) {
            $aFieldsetCustom['fields'][] = array(
                'subFieldsContainer' => $aSubfield,
                'subFieldsContainerExtra' => $aSubfieldExtra,
                'ajax' => $aAjaxField,
            );
        } else {
            $aFieldsetOptional['fields'][] = array(
                'subFieldsContainer' => $aSubfield,
                'subFieldsContainerExtra' => $aSubfieldExtra,
                'ajax' => $aAjaxField,
            );
        }
    }

    $aSavedValues = $this->getAttributeValues($mParentValue, '');
    foreach ($aSavedValues as $sCode => $aAttribute) {
        if (empty($aMPAttributes[$sCode])) {
            // deleted from marketplace
            $sId = 'variationgroups.' . $mParentValue . '.' . $sCode . '.code';

            $aSubfield = $this->getField($sId . '_sub');
            $aSubfield['type'] = 'subFieldsContainer';
            $aSubfield['i18n']['hint'] = '';
            $aSubfield['i18n']['label'] = '<p class="error">' . $aAttribute['AttributeName'] . '</p>';
            $aSubfield['subfields']['information'] = array(
                'type' => 'information',
                'value' => '<p class="error">' . MLI18n::gi()->get($marketplaceName . '_varmatch_attribute_deleted_from_mp') . '</p>',
            );

            $aFieldset['fields'][] = array('subFieldsContainer' => $aSubfield);
        }
    }
    if (!empty($aFieldset['fields']) || (empty($aFieldset['fields']) && empty($aFieldsetOptional['fields']) && empty($aFieldsetCustom['fields']))) {
        ?>
        <table class="attributesTable" id="attributesTable">
            <?php $this->includeView('widget_form_type_attributefield', array('aFieldset' => $aFieldset)); ?>
        </table>
        <?php
    }
    if (!empty($aFieldsetOptional['fields'])) {
        ?>
        <table class="attributesTable" id="attributesTableOptional">
            <?php $this->includeView('widget_form_type_attributefield', array('aFieldset' => $aFieldsetOptional)); ?>
        </table>
    <?php }
    if (!empty($aFieldsetCustom['fields'])) {
        ?>

        <table class="attributesTable" id="attributesTableCustom">
            <?php $this->includeView('widget_form_type_attributefield', array('aFieldset' => $aFieldsetCustom)); ?>
        </table>
    <?php } ?>
    <p><?php echo MLI18n::gi()->get($marketplaceName . '_prepare_variations_mandatory_fields_info') ?></p>
    <script type="text/javascript">/*<![CDATA[*/
        (function($) {
            $(document).ready(function() {
                var attributesOptions = <?php echo json_encode($aShopAttributes) ?>;

                $('#attributesTable > tbody > tr').removeClass('odd even');

                $('select option[value="separator_line_1"]').attr('disabled', 'disabled');
                $('select option[value="separator_line_2"]').attr('disabled', 'disabled');
                $('#<?php echo $marketplaceName ?>_prepare_variations_field_variationgroups_value').change(function() {
                    $('div.noticeBox').remove();
                });

                function fireAttributeAjaxRequest(eElement, ajaxAdditional, selector, oldValue) {
                    var selectorName = selector.substring(1);
                    $('[id^=attributeDropDown_' + selectorName + ']').css('background-color', '');
                    if ($.trim($(selector + '_button_matched_table').html())) {
                        var d = '<?php echo MLI18n::gi()->get($marketplaceName . '_prepare_variations_change_attribute_info') ?>';
                        $('<div class="ml-modal dialog2" title="<?php echo MLI18n::gi()->get('ML_LABEL_NOTE')?>"></div>').html(d).jDialog({
                            width: (d.length > 1000) ? '700px' : '500px',
                            buttons: {
                                'OK': function() {
                                    $(selector).val(oldValue);
                                    $(this).dialog('close');
                                }
                            }
                        });
                    } else {
                        $('div#attributeExtraFields_' + selectorName + '_sub span').children('*:not(.doNotHide)').hide();
                        $('div#attributeMatchedTable_' + selectorName + '_sub').show();

                        $.blockUI(blockUILoading);
                        var eForm = eElement.parentsUntil('form').parent(),
                            aData = $(eForm).serializeArray(),
                            aAjaxData = $.parseJSON(eElement.attr('data-ajax')),
                            i;

                        for (i in aAjaxData) {
                            if (aAjaxData[i]['value'] === null) {
                                aAjaxData[i]['value'] = ajaxAdditional;
                            }

                            aData.push(aAjaxData[i]);
                        }

                        aData = mlSerializer.prepareSerializedDataForAjax(aData);
                        eElement.hide('slide', {direction: 'right'});
                        $.ajax({
                            url: eForm.attr("action"),
                            type: eForm.attr("method"),
                            data: aData,
                            complete: function (jqXHR, textStatus) {
                                var eRow;
                                try {// need it for ebay-categories and attributes, cant do with global ajax, yet
                                    var oJson = $.parseJSON(data);
                                    var content = oJson.content;
                                    eElement.html(content);
                                } catch (oExeception) {
                                }

                                eRow = eElement.closest('.js-field');
                                if (!eRow.hasClass('hide') && (eElement.text() !== '')) {
                                    eRow.show();
                                } else {
                                    eRow.hide();
                                }

                                initAttributeAjaxForm(eElement, true);
                                $.unblockUI();
                                eElement.show('slide', {direction: 'right'});
                                $(".magnalisterForm select.optional").trigger("change");
                            }
                        });
                    }
                    var selectedOption = $(selector + ' option:selected');
                    var customNameInput = $(selector + '_custom_name');
                    // when nothing is selected textbox should be empty, that is currently checked by selected index
                    // if index of not selected value changes it should be also fixed here
                    var initialOptionIndex = 0;
                    if (customNameInput.attr('type') !== 'hidden') {
                        customNameInput.val(selectedOption.index() === initialOptionIndex ? null : selectedOption.text());
                    }
                }

                function initAttributeAjaxForm(eElements, onlyChildren) {
                    var els = eElements.find('.magnalisterAttributeAjaxForm');
                    if (!onlyChildren) {
                        els = els.andSelf();
                    }

                    els.each(function() {
                        var eElement = $(this),
                            aAjaxController = $.parseJSON(eElement.attr('data-ajax-controller'));

                        if (aAjaxController !== null) {
                            if (eElement.find(aAjaxController.selector).length === 0) {
                                var previous;
                                $(eElements).find(aAjaxController.selector).on('focus', function() {
                                    previous = $(aAjaxController.selector).val();
                                }).change(function(event) {
                                    fireAttributeAjaxRequest(eElement, event.ajaxAdditional, aAjaxController.selector, previous);
                                    previous = $(aAjaxController.selector).val();
                                });
                            } else {
                                var previous;
                                $(eElement).on('focus', $('.magnalisterForm').find(aAjaxController.selector), function() {
                                    previous = $(aAjaxController.selector).val();
                                }).change(function(event) {
                                    fireAttributeAjaxRequest(eElement, event.ajaxAdditional, aAjaxController.selector, previous);
                                    previous = $(aAjaxController.selector).val();
                                });
                            }

                            if (eElement.attr('data-ajax-trigger') === 'true') {
                                // only trigger by first load
                                eElement.attr('data-ajax-trigger', 'false');
                                fireAttributeAjaxRequest(eElement);
                            }
                        }
                    });
                }

                function appendOption(element, key, value, selected, dataType) {
                    var option = $('<option value="' + key + '">' + value + '</option>');
                    if (selected) {
                        option.attr('selected', 'selected');
                    }

                    if (dataType !== undefined) {
                        option.attr('data-type', dataType);
                    }

                    element.append(option);
                }

                function renderOptions(select) {
                    var selectedValue = select.attr('data-value');

                    for (var optionKey in attributesOptions) {
                        if (!attributesOptions.hasOwnProperty(optionKey)) {
                            continue;
                        }

                        var optionValue = attributesOptions[optionKey],
                            selected = selectedValue === optionKey;

                        if (typeof optionValue !== 'object') {
                            // simple option -> render it
                            appendOption(select, optionKey, optionValue, selected);
                        } else {
                            // this is optgroup so we need to render it
                            var optGroup = $('<optgroup label="' + optionKey + '" class="' +
                                optionValue.optGroupClass + '"></optgroup>');
                            for (var attributeKey in optionValue) {
                                // render attributes
                                if (optionValue.hasOwnProperty(attributeKey) && attributeKey !== 'optGroupClass') {
                                    var attribute = optionValue[attributeKey];
                                    appendOption(optGroup, attributeKey, attribute.name, selected, attribute.type);
                                }
                            }

                            select.append(optGroup);
                        }
                    }
                }

                initAttributeAjaxForm($('.magnalisterForm'));
                var  originalForm = $("#<?php echo $this->getField('variationgroups.value', 'id')?>").closest('form');
                originalForm.on('click', "[type=submit]", function(e) {
                    e.preventDefault();
                    var form = $(this).closest('form');
                    mlSerializer.submitSerializedForm(form, {[$(this).attr('name')]:$(this).val()});
                });
                
                $('button.delete-matched-value').click(function() {
                    var form = $(this).closest('form');
                    $(this).closest('table')[0].deleteRow(this.parentNode.parentNode.rowIndex);

                    <?php if ($tabType === 'variations') {?>
                        var actionData = {"ml[action][saveaction]":"0"};
                    <?php } else {?>
                        var actionData = {"ml[action][prepareaction]":this.value};
                    <?php }?>
                    mlSerializer.submitSerializedForm(form, actionData);
                });

                // Optional attributes matching JS logic
                function initOptionalAttributesSelector() {
                    var optionalFieldsetEl = $('#<?php echo $aFieldsetOptional['id']?>'),
                        spacerFieldEl = optionalFieldsetEl.find('.spacer').last(),
                        optionalAttributesMap = <?php echo json_encode($optionalAttributesMap)?>,
                        currentlySelectedAttribute = null,
                        attributesSelectorEl = null,
                        attributesSelectorOptionsTpl = ['<option value="dont_use"><?php echo MLI18n::gi()->get('ML_LABEL_DONT_USE')?></option>'];

                    for (var fieldId in optionalAttributesMap) {
                        if (optionalAttributesMap.hasOwnProperty(fieldId)) {
                            attributesSelectorOptionsTpl.push(
                                '<option value="' + fieldId + '">' + optionalAttributesMap[fieldId] + '</option>'
                            );
                        }
                    }

                    // If there is no optional attributes quit
                    if (attributesSelectorOptionsTpl.length === 1) {
                        return;
                    }

                    $([
                        '<tr class="js-field hide optionalAttribute dont_use_sub">',
                        '<th><label for="dont_use_sub"></label></th>',
                        '<td class="mlhelp ml-js-noBlockUi"></td>',
                        '<td class="input"></td>',
                        '<td class="info"></td>',
                        '</tr>'
                    ].join('')).insertBefore(spacerFieldEl);

                    attributesSelectorEl = $([
                        '<select name="optional_selector" style="width: 100%">',
                        attributesSelectorOptionsTpl.join(''),
                        '</select>'
                    ].join(''));

                    function showConfirmationDialog(attributeIdToShow) {
                        var d = '<?php echo MLI18n::gi()->get($marketplaceName . '_prepare_variations_reset_info') ?>';
                        $('<div class="ml-modal dialog2" title="<?php echo MLI18n::gi()->get('ML_LABEL_INFO')?>"></div>').html(d).jDialog({
                            width: (d.length > 1000) ? '700px' : '500px',
                            buttons: {
                                Cancel: {
                                    'text': '<?php echo MLI18n::gi()->get('ML_BUTTON_LABEL_ABORT'); ?>',
                                    click: function() {
                                        // Reset attribute selector to previous value silently
                                        attributesSelectorEl.val(currentlySelectedAttribute);
                                        $(this).dialog('close');
                                    }
                                },
                                Ok: {
                                    'text': '<?php echo MLI18n::gi()->get('ML_BUTTON_LABEL_OK'); ?>',
                                    click: function() {
                                        $('#' + currentlySelectedAttribute).val('').change();
                                        changeCurrentAttribute(attributeIdToShow);
                                        $(this).dialog('close');
                                    }
                                }
                            }
                        });
                    }

                    function attributeSelectorOnChange() {
                        if (currentlySelectedAttribute) {
                            var attributeValue = $('#' + currentlySelectedAttribute).val();
                            if (attributeValue) {
                                showConfirmationDialog($(this).val());
                                return;
                            }
                        }

                        changeCurrentAttribute($(this).val());
                    }

                    function changeCurrentAttribute(attributeIdToShow) {
                        optionalFieldsetEl.find('.optionalAttribute').addClass('hide').hide();

                        currentlySelectedAttribute = attributeIdToShow;

                        var attributeFieldEl = optionalFieldsetEl.find('.' + currentlySelectedAttribute + '_sub'),
                            currentAttributeLabelEl = attributeFieldEl.find('label[for="' + currentlySelectedAttribute + '_sub"]').hide();

                        attributesSelectorEl.insertBefore(currentAttributeLabelEl);
                        attributeFieldEl.remove().removeClass('hide').show().insertBefore(spacerFieldEl);
                        initAttributeAjaxForm(attributeFieldEl, true);

                        attributesSelectorEl.change(attributeSelectorOnChange);
                    }

                    attributesSelectorEl.change(attributeSelectorOnChange).change();

                }

                initOptionalAttributesSelector();

                // for each attribute if it has predefined values, only direct matching is possible
                // so free text fields and attributes must be disabled
                // if not, matching to attribute value must be disabled
                $('input[id$="_kind"]').each(function() {
                    var $input = $(this),
                        freeText = $input.val() === 'FreeText',
                        name = $input.attr('name').replace('[Kind]', '[Code]'),
                        select = $('select[name="' + name + '"]'),
                        separator = false;

                    var selectElement = document.getElementsByName(name);
                    selectElement = selectElement[0] ? selectElement[0] : null;
                    var dontUseLabel = '<?php echo MLI18n::gi()->get('ML_LABEL_DONT_USE')?>';
                    appendOption(select, '', dontUseLabel, false);

                    if (selectElement) {
                        selectElement.addEventListener('mousedown', function () {
                            if (this.options.length === 1) {
                                $(this).find('option').remove().end();
                                renderOptions($(this));
                                select.find('option').each(function () {
                                    var $optionId = $(this).val();
                                    if (!separator && $optionId === 'separator_line_1') {
                                        separator = true;
                                    }

                                    if (freeText && $optionId === 'attribute_value') {
                                        $(this).attr('disabled', 'disabled');
                                    } else if (!freeText && separator && $optionId !== 'attribute_value') {
                                        $(this).attr('disabled', 'disabled');
                                    }
                                });
                            }
                        });
                    }
                });
            });
        })(jqml);
    /*]]>*/</script>
    <?php
}