<?php
abstract class ML_Productlist_Model_ProductList_Selection extends ML_Productlist_Model_ProductList_Abstract {
    public function isSelected(ML_Shop_Model_Product_Abstract $oProduct){
        $i= MLDatabase::getDbInstance()->fetchOne("
            select 
                count(*)
            from 
                magnalister_selection s 
            where
                s.pid='".$oProduct->get('id')."'
                and 
                s.mpID='".MLModul::gi()->getMarketPlaceId()."'
                and
                s.selectionname='".$this->getSelectionName()."'
                and
                s.session_id='".  MLShop::gi()->getSessionId()."'
        ");
        return $i>0;
    }
    
    protected function getAdditemListData($iFrom, $iCount){
        $aList = array();
        $sSql = "
            select %s
            from 
                magnalister_selection s, 
                magnalister_products p 
            where 
                s.pID=p.ID
                and
                s.session_id='" . MLShop::gi()->getSessionId() . "'
                and
                s.selectionname='checkin'
                and
                mpid='" . MLModul::gi()->getMarketPlaceId() . "'
        ";
        $iCountTotal = MLDatabase::getDbInstance()->fetchOne(sprintf($sSql, ' count(distinct p.ParentId) '));
         foreach(MLDatabase::getDbInstance()->fetchArray(sprintf($sSql,' distinct p.ParentId ')." limit ".$iFrom.", ".$iCount) as $aRow){
            $oProduct = MLProduct::factory()->set("id", $aRow['ParentId']);
            if ($oProduct->exists()) {
                $aList[$aRow['ParentId']] = $oProduct;
            }
        }
        return array(
            'List' => $aList,
            'CountTotal'=> $iCountTotal,
        );
    }

    abstract public function getSelectionName();
}