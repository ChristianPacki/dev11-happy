<?php
/**
 * 888888ba                 dP  .88888.                    dP
 * 88    `8b                88 d8'   `88                   88
 * 88aaaa8P' .d8888b. .d888b88 88        .d8888b. .d8888b. 88  .dP  .d8888b.
 * 88   `8b. 88ooood8 88'  `88 88   YP88 88ooood8 88'  `"" 88888"   88'  `88
 * 88     88 88.  ... 88.  .88 Y8.   .88 88.  ... 88.  ... 88  `8b. 88.  .88
 * dP     dP `88888P' `88888P8  `88888'  `88888P' `88888P' dP   `YP `88888P'
 *
 *                          m a g n a l i s t e r
 *                                      boost your Online-Shop
 *
 * -----------------------------------------------------------------------------
 * $Id$
 *
 * (c) 2010 - 2014 RedGecko GmbH -- http://www.redgecko.de
 *     Released under the MIT License (Expat)
 * -----------------------------------------------------------------------------
 */
class_exists('ML', false) or die();
$marketplaceName = MLModul::gi()->getMarketPlaceName();

$sName = str_replace('field', '', $aField['name']);
$sNameWithoutValue = str_replace('[Values]', '', $sName);
$aNameWithoutValue = explode('][', $sNameWithoutValue);
$sFirst = substr($aNameWithoutValue[0], 1);
$sLast = end($aNameWithoutValue);
$sLast = substr($sLast, 0, -1);
$sSelector = $this->aFields[$sFirst . '.' . strtolower($aNameWithoutValue[1]) . '.' . strtolower($sLast) . '.code']['id'];
$blDisableFreeText = $aField['valuesdst']['from_mp'];

// Getting type of tab (is it variation tab or apply form)
$sChangedSelector = ' ' . $sSelector;
$ini = strpos($sChangedSelector, $marketplaceName . '_prepare_');
if ($ini == 0) return '';
$ini += strlen($marketplaceName . '_prepare_');
$len = strpos($sChangedSelector, '_field', $ini) - $ini;
$tabType = substr($sChangedSelector, $ini, $len);

?>
<span>
    <table style="width:100%;margin-top: 15px">
        <?php if (!empty($aField['i18n']['matching']['titlesrc']) || !empty($aField['i18n']['matching']['titledst'])){ ?>
            <thead>
            <th style="width: 35%"><?php echo $aField['i18n']['matching']['titlesrc']; ?></th>
            <th style="width: 35%"><?php echo $aField['i18n']['matching']['titledst']; ?></th>
        </thead>
        <?php } ?>
        <tbody>
                <tr>
                    <td style="width: 35%">
                        <?php
                        $aSelect = array(
                            'name' => $aField['name'] . '[0][Shop][Key]',
                            'type' => 'select',
                            'i18n' => array(),
                            'values' => $aField['valuessrc'],
                            'value' => 'noselection'
                        );
                        $aHidden = array(
                            'type' => 'hidden',
                            'id' => $sSelector . '_hidden_shop_value',
                            'name' => $aField['name'] . '[0][Shop][Value]'
                        );

                        if (isset($aField['error']) && $aField['error'] == true){
                            $aSelect['cssclass'] = 'error';
                        }

                        $aNewArray = array(
                            'noselection' => MLI18n::gi()->get('form_type_matching_select_optional'),
                            'all' => MLI18n::gi()->get('form_type_matching_select_all'),
                            'separator_line_3' => MLI18n::gi()->get($marketplaceName . '_prepare_variations_separator_line_label'),
                        );
                        foreach ($aSelect['values'] as $sSelectKey => $sSelectValue){
                            $aNewArray[$sSelectKey] = $sSelectValue;
                        }

                        $aSelect['values'] = $aNewArray;
                        $this->includeType($aSelect);
                        $this->includeType($aHidden);
                        ?>
                    </td>
                    <td style="width: 35%">
                        <?php
                        $aSelect = array(
                            'name' => $aField['name'] . '[0][Marketplace][Key]',
                            'type' => 'select',
                            'i18n' => array(),
                            'values' => $aField['valuesdst']['values'],
                            'value' => 'noselection'
                        );
                        $aHidden = array(
                            'type' => 'hidden',
                            'id' => $sSelector . '_hidden_marketplace_value',
                            'name' => $aField['name'] . '[0][Marketplace][Value]'
                        );

                        if (isset($aField['error']) && $aField['error'] == true){
                            $aSelect['cssclass'] = 'error';
                        }

                        // Changed because in previous implementation array keys are recreated.
                        $aNewArray = array(
                            'noselection' => MLI18n::gi()->get('form_type_matching_select_optional'),
                            'auto' => MLI18n::gi()->get('form_type_matching_select_auto'),
                            'reset' => MLI18n::gi()->get('form_type_matching_select_reset'),
                            'manual' => MLI18n::gi()->get('form_type_matching_select_manual'),
                            'separator_line_3' => MLI18n::gi()->get($marketplaceName . '_prepare_variations_separator_line_label'),
                        );

                        foreach ($aSelect['values'] as $sSelectKey => $sSelectValue){
                            $aNewArray[$sSelectKey] = $sSelectValue;
                        }

                        $aSelect['values'] = $aNewArray;
                        $this->includeType($aSelect);
                        $this->includeType($aHidden);

                        if ($blDisableFreeText){ ?>
                            <script>
                                    (function ($) {
                                        $('#<?php echo $sSelector . '_hidden_marketplace_value';?>').parent().find('select option[value="manual"]').attr('disabled', 'disabled');
                                    })(jqml);
                                </script>
                        <?php }
                        ?>

                    </td>
                    <td id="freetext_<?php echo $sLast ?>" style="border: none; display: none;">
                        <input type="text" name="ml[field]<?php echo $sName ?>[FreeText]" style="width:100%;">
                    </td>
                    <td style="border: none">
                       <?php
                       $id = $marketplaceName . '_prepare_match_manual_form_field_prepareaction';
                       ?>
                        <button type="button" value="0"
                                id="<?php echo $marketplaceName ?>_prepare_match_manual_form_field_prepareaction"
                                class="mlbtn action <?php echo $marketplaceName ?>_prepare_match_manual_form_field_prepareaction"
                                name="ml[action][prepareaction]">+</button>
                    </td>
                </tr>
        </tbody>
    </table>
</span>
<?php
if (!empty($aField['values']) && is_array($aField['values'])){
    ?>
    <span id="spanMatchingTable" style="padding-right:2em;">
    <div style="font-weight: bold; background-color: #e9e9e9">
        <?php echo MLI18n::gi()->get($marketplaceName . '_prepare_variations_matching_table'); ?>
    </div>
    <table id="<?php echo $sSelector ?>_button_matched_table" style="width:100%; background-color: #e9e9e9">
        <tbody>
        <?php
        $i = 1;
        foreach ($aField['values'] as $sKey => $aValue){
            // check if value is deleted from marketplace
        if ($aValue['Marketplace']['Key'] !== 'manual' && !isset($aField['valuesdst']['values'][$aValue['Marketplace']['Key']])) {
            ?>
            <tr class="error">
                <td style="width: 35%">
                    <?php echo $aValue['Shop']['Value']; ?>
                </td>
                <td style="width: 35%">
                    <?php echo MLI18n::gi()->get($marketplaceName . '_varmatch_attribute_value_deleted_from_mp') ?>
                </td>
                <td colspan="2" style="border: none">
                    <button type="button" value="0"
                        id="<?php echo $marketplaceName ?>_prepare_match_manual_form_field_prepareaction"
                        class="mlbtn action delete-matched-value" name="ml[action][prepareaction]">+</button>

                </td>
            </tr>
                <?php
        continue;
        }

        $aNewFieldShopKey = array(
            'type' => 'hidden',
            'id' => $sSelector . '_shop_key_' . $i,
            'name' => $aField['name'] . '[' . $i . '][Shop][Key]',
            'value' => $aValue['Shop']['Key']
        );
        $aNewFieldShopValue = array(
            'type' => 'hidden',
            'id' => $sSelector . '_shop_value_' . $i,
            'name' => $aField['name'] . '[' . $i . '][Shop][Value]',
            'value' => $aValue['Shop']['Value']
        );
        $aNewFieldMarketplaceKey = array(
            'type' => 'hidden',
            'id' => $sSelector . '_marketplace_key_' . $i,
            'name' => $aField['name'] . '[' . $i . '][Marketplace][Key]',
            'value' => $aValue['Marketplace']['Key']
        );
        $aNewFieldMarketplaceValue = array(
            'type' => 'hidden',
            'id' => $sSelector . '_marketplace_value_' . $i,
            'name' => $aField['name'] . '[' . $i . '][Marketplace][Value]',
            'value' => $aValue['Marketplace']['Value']
        );
        $aNewFieldMarketplaceInfo = array(
            'type' => 'hidden',
            'id' => $sSelector . '_marketplace_info_' . $i,
            'name' => $aField['name'] . '[' . $i . '][Marketplace][Info]',
            'value' => $aValue['Marketplace']['Info']
        );
        $aSelectMarketplaceValue = array(
            'type' => 'select',
            'name' => $sSelector . '_marketplace_value_select_' . $i,
            'id' => $sSelector . '_marketplace_value_select_' . $i,
            'i18n' => array(),
            'value' => $aValue['Marketplace']['Key'],
            'values' => array(
                $aValue['Marketplace']['Key'] => $aValue['Marketplace']['Info'],
                'freetext' => MLI18n::gi()->get($marketplaceName . '_prepare_variations_free_text'),
            ),
        );
        ?>
            <tr>
                <td style="width: 35%">
                    <?php
                    $this->includeType($aNewFieldShopKey);
                    $this->includeType($aNewFieldShopValue);
                    echo $aValue['Shop']['Value'];
                    ?>
                </td>
                <td style="width: 35%">
                    <?php
                    $this->includeType($aNewFieldMarketplaceKey);
                    $this->includeType($aNewFieldMarketplaceValue);
                    $this->includeType($aNewFieldMarketplaceInfo);
                    $this->includeType($aSelectMarketplaceValue);
                    ?>
                </td>
                <td id="free_text_extra_<?php echo $sSelector . '_marketplace_value_' . $i ?>"
                    style="border: none; display: none;">
                    <input type="hidden" disabled="disabled"
                           id="hidden_<?php echo $sSelector . '_marketplace_value_' . $i ?>"
                           name="<?php echo 'ml[field]' . $sName . '[' . $i . '][Marketplace][Key]' ?>" value="manual">
                    <input type="text" id="text_for_upload_<?php echo $sSelector . '_marketplace_value_' . $i ?>"
                           style="width:100%;">
                </td>
                <td style="border: none">
                        <button type="button" value="0" class="mlbtn action delete-matched-value"
                                id="<?php echo $sSelector . '_button_delete' . $i ?>"
                                name="ml[action][prepareaction]">-</button>
                        <button type="button" value="0" class="mlbtn action save-freetext-value"
                                id="<?php echo $sSelector . '_button_add' . $i ?>"
                                name="ml[action][prepareaction]">+</button>
                </td>
            </tr>
            <script>
                (function ($) {
                    <?php $sName = str_replace('field', '', $aField['name']); ?>
                    var selectEl = $('<?php echo '#' . $sSelector . '_marketplace_value_select_' . $i ?>');
                    $('select[name="<?php echo 'ml[field]' . $sName . '[0][Shop][Key]';?>"] option[value="<?php echo $aValue['Shop']['Key'] ?>"]').hide();

                    $('#<?php echo $sSelector . '_button_add' . $i?>').hide();
                    selectEl.change(function () {
                        if ($(this).val() === "freetext") {
                            $('#<?php echo $sSelector . '_button_delete' . $i?>').hide();
                            $('#<?php echo $sSelector . '_button_add' . $i?>').show();
                            $("td #free_text_extra_<?php echo $sSelector . '_marketplace_value_' . $i?>").show();
                            $("#hidden_<?php echo $sSelector . '_marketplace_value_' . $i?>").removeAttr("disabled");
                        } else {
                            $('#<?php echo $sSelector . '_button_delete' . $i?>').show();
                            $('#<?php echo $sSelector . '_button_add' . $i?>').hide();
                            $("td #free_text_extra_<?php echo $sSelector . '_marketplace_value_' . $i?>").hide();
                            $("#hidden_<?php echo $sSelector . '_marketplace_value_' . $i?>").attr("disabled", "disabled");
                        }
                    }).trigger("change");

                    <?php if ($blDisableFreeText) { ?>
                    selectEl.find('option[value="freetext"]').attr('disabled', 'disabled');
                    <?php } ?>

                    $("#text_for_upload_<?php echo $sSelector . '_marketplace_value_' . $i?>").change(function () {
                        var textVal = $("#text_for_upload_<?php echo $sSelector . '_marketplace_value_' . $i?>").val();
                        $('#<?php echo $sSelector . '_marketplace_value_' . $i?>').val(textVal);
                    });
                })(jqml);
            </script>
            <?php $i++;
        } ?>
        </tbody>
    </table>
</span>
    <?php
}
?>

<script>
    (function ($) {
        $(document).ready(function () {
            $('select option[value="separator_line_3"]').attr('disabled', 'disabled');
        });

        $('[name="ml[field]<?php echo $sName ?>[0][Shop][Key]"]').on('change', function () {
            var val = $('[name="ml[field]<?php echo $sName ?>[0][Shop][Key]"] option:selected').html();
            $('[name="ml[field]<?php echo $sName ?>[0][Shop][Value]"]').val(val);
        });

        $('[name="ml[field]<?php echo $sName ?>[0][Marketplace][Key]"]').on('change', function () {
            var val = $('[name="ml[field]<?php echo $sName ?>[0][Marketplace][Key]"] option:selected').html();
            $('[name="ml[field]<?php echo $sName ?>[0][Marketplace][Value]"]').val(val);
            var oldValue = $('[name="ml[field]<?php echo $sName ?>[0][Marketplace][Key]"]').defaultValue;
            if ($(this).val() === 'reset') {
                var d = '<?php echo addslashes(MLI18n::gi()->get($marketplaceName . '_prepare_variations_reset_info')) ?>';
                $('<div class="ml-modal dialog2" title="<?php echo MLI18n::gi()->get('ML_LABEL_INFO')?>"></div>').html(d).jDialog({
                    width: (d.length > 1000) ? '700px' : '500px',
                    buttons: {
                        Cancel: {
                            'text': '<?php echo MLI18n::gi()->get('ML_BUTTON_LABEL_ABORT'); ?>',
                            click: function () {
                                $('[name="ml[field]<?php echo $sName ?>[0][Marketplace][Key]"]').val(oldValue);
                                $(this).dialog('close');
                            }
                        },
                        Ok: {
                            'text': '<?php echo MLI18n::gi()->get('ML_BUTTON_LABEL_OK'); ?>',
                            click: function () {
                                var form = $('[name="ml[field]<?php echo $sName ?>[0][Marketplace][Key]"]').closest('form'),
                                    button = $('#<?php echo $id?>'),
                                    input = $('<input type="hidden">').attr('name', button.attr('name')).val(button.val());

                                form.append(input).submit();
                                // this does not work for some reason...
                                // $('#<?php echo $id?>').trigger('click');
                                $(this).dialog('close');
                            }
                        }
                    }
                });
            }

            if ($(this).val() === 'manual') {
                $('td #freetext_<?php echo $sLast?>').show();
            } else {
                $('td #freetext_<?php echo $sLast?>').hide();
            }
        });
    })(jqml);

</script>

