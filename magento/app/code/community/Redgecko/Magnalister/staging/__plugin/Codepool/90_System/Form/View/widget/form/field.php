<?php 
/**
 * 888888ba                 dP  .88888.                    dP                
 * 88    `8b                88 d8'   `88                   88                
 * 88aaaa8P' .d8888b. .d888b88 88        .d8888b. .d8888b. 88  .dP  .d8888b. 
 * 88   `8b. 88ooood8 88'  `88 88   YP88 88ooood8 88'  `"" 88888"   88'  `88 
 * 88     88 88.  ... 88.  .88 Y8.   .88 88.  ... 88.  ... 88  `8b. 88.  .88 
 * dP     dP `88888P' `88888P8  `88888'  `88888P' `88888P' dP   `YP `88888P' 
 *
 *                          m a g n a l i s t e r
 *                                      boost your Online-Shop
 *
 * -----------------------------------------------------------------------------
 * $Id$
 *
 * (c) 2010 - 2014 RedGecko GmbH -- http://www.redgecko.de
 *     Released under the MIT License (Expat)
 * -----------------------------------------------------------------------------
 */
class_exists('ML', false) or die();
?>
<?php
    if(!isset($aField['id'])){
        return;
    }
    if(isset($aField['type'])){
        if (
                (
                    isset($aField[$aField['type']]) 
                    && !isset($aField[$aField['type']]['field']['type'])
                )
                ||
                $aField['type']==='hidden'
        ) {
            $blDisplay=false;
        } else {
            $blDisplay=true;
        }
    }else{
        $blDisplay=false;
    }

$translationData = array(
    'label' => array(),
    'help' => array(),
    'hint' => array(),
);
if(MLI18n::gi()->isTranslationActive()) {
    try{
        MLSetting::gi()->get('blFormWysiwigLoaded');
    }catch(Exception $oEx){
        MLSetting::gi()->set('blFormWysiwigLoaded',true);
        MLSettingRegistry::gi()->addJs(array('tiny_mce/tiny_mce.js','jquery.magnalister.form.wysiwyg.js'));
        ?>
        <script type="text/javascript">/*<![CDATA[*/
            <?php echo getTinyMCEDefaultConfigObject(); ?>;
            /*]]>*/</script>
        <?php
    }

    $translationData = array(
        'label' => MLI18n::gi()->getTranslationData($aField['id'] . '_label'),
        'help' => MLI18n::gi()->getTranslationData($aField['id'] . '_help'),
        'hint' => MLI18n::gi()->getTranslationData($aField['id'] . '_hint'),
    );
}
?>
<tr class="js-field <?php echo $sClass.(isset($aField['classes']) ? ' '.implode(' ', $aField['classes']) : ''); ?>"<?php echo $blDisplay?'':' style="display:none"' ?>>
    <?php if (!array_key_exists('fullwidth', $aField) || $aField['fullwidth'] == false) { ?>
        <th class="ml-translate-toolbar-wrapper">
            <label class="<?php echo !empty($translationData['label']['missing_key']) ? 'missing_translation' : ''?>"
                for="<?php echo $aField['id'] ?>"><?php echo $aField['i18n']['label'] ?></label>
            <?php if (isset($aField['requiredField']) === true && $aField['requiredField'] === true) { ?>
                <span>•</span>
            <?php } ?>
            <?php if (MLI18n::gi()->isTranslationActive()) { ?>
                <div class="ml-translate-toolbar">
                    <a href="#" title="Translate label" class="translate-label abutton" <?php echo 'data-ml-translate-modal="#modal-tr-' . str_replace('.', '\\.', $aField['id']) . '-label"'; ?>>&nbsp;</a>
                    <div class="ml-modal-translate dialog2" id="modal-tr-<?php echo str_replace('.', '\\.', $aField['id']) ?>-label">
                        <script type="text/plain" class="data"><?php echo json_encode($translationData['label']); ?></script>
                    </div>
                </div>
            <?php } ?>
        </th>
        <td class="mlhelp ml-js-noBlockUi ml-translate-toolbar-wrapper">
            <?php if (isset($aField['i18n']['help'])) {?>
                <a class="<?php echo !empty($translationData['help']['missing_key']) ? 'missing_translation' : ''?>" data-ml-modal="#modal-<?php echo str_replace('.', '\\.', $aField['id']); ?>">
                    &nbsp;
                </a>
                <div class="ml-modal dialog2" id="modal-<?php echo $aField['id'] ?>" title="<?php echo $aField['i18n']['label'] ;?>">
                    <?php echo $aField['i18n']['help']; ?>
                </div>
                <?php if (MLI18n::gi()->isTranslationActive()) { ?>
                    <div class="ml-translate-toolbar">
                        <a href="#" title="Translate help" class="translate-help abutton" <?php echo 'data-ml-translate-modal="#modal-tr-' . str_replace('.', '\\.', $aField['id']) . '-help"'; ?>>&nbsp;</a>
                        <div class="ml-modal-translate dialog2" id="modal-tr-<?php echo str_replace('.', '\\.', $aField['id']) ?>-help">
                            <script type="text/plain" class="data"><?php echo json_encode($translationData['help']); ?></script>
                        </div>
                    </div>
                <?php } ?>
            <?php } ?>
        </td>
    <?php } ?>
    <td <?php echo (array_key_exists('fullwidth', $aField) && $aField['fullwidth'] == true) ? 'colspan="3"': 'class="input"' ; ?>>
        <?php 
            if (array_key_exists('debug', $aField) && $aField['debug']) {
                new dBug($aField, '', true);
            }
            if (
                array_key_exists('autooptional', $aField) && $aField['autooptional'] == false 
                && !array_key_exists('optional', $aField)
            ) {// field is not autooptional and optional is not defined- force to optional=true
                ?><input type="hidden" name="<?php echo MLHTTP::gi()->parseFormFieldName($this->sOptionalIsActivePrefix.'['. $aField['realname'] . ']') ?>" value="true"><?php
            }
            $this->includeType($aField);
        ?>
    </td>
    <td class="info ml-translate-toolbar-wrapper <?php echo !empty($translationData['hint']['missing_key']) ? 'missing_translation' : ''?>">
        <?php
            if (isset($aField['hint']['template'])) {
                $this->includeView('widget_form_hint_'.$aField['hint']['template'],array('aField'=>$aField));
            }
        ?>
        <?php if (MLI18n::gi()->isTranslationActive() && isset($aField['i18n']['hint'])) {?>
            <div class="ml-translate-toolbar">
                <a href="#" title="Translate hint" class="translate-hint abutton" <?php echo 'data-ml-translate-modal="#modal-tr-' . str_replace('.', '\\.', $aField['id']) . '-hint"'; ?>>&nbsp;</a>
                <div class="ml-modal-translate dialog2" id="modal-tr-<?php echo str_replace('.', '\\.', $aField['id']) ?>-hint">
                    <script type="text/plain" class="data"><?php echo json_encode($translationData['hint']); ?></script>
                </div>
            </div>
        <?php } ?>
    </td>
</tr>