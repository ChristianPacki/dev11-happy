<?php

MLFilesystem::gi()->loadClass('Core_Controller_Abstract');

class ML_Tools_Controller_Main_Tools_Orders extends ML_Core_Controller_Abstract {
    protected $aParameters=array('controller');
    
    protected function getRequestedOrderSpecial() {
        return $this->getRequest('orderspecial');
    }
}
