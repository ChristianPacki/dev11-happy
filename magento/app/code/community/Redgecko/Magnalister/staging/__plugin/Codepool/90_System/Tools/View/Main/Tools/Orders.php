
<form method="post" action="<?php echo $this->getCurrentUrl(); ?>">
    <div style="display:none">
        <?php foreach (MLHttp::gi()->getNeededFormFields() as $sKey => $sValue) { ?>
            <input type="hidden" name="<?php echo $sKey ?>" value="<?php echo $sValue ?>" />
        <?php } ?>
    </div>
    <table>
        <tr>
            <td>
                <label for="ml-sku">OrderSpecial :</label></td><td>
                <input type="text" name="<?php echo MLHttp::gi()->parseFormFieldName('orderspecial') ?>" value="<?php echo $this->getRequestedOrderSpecial() ?>">
            </td>
        </tr>
        <tr><td><button type="sumit" class="mlbtn">Search Order</button></td><td></td></tr>
    </table>
</form>
<?php
    if ($this->getRequestedOrderSpecial()) {
        $oOrder = MLOrder::factory()->getByMagnaOrderId($this->getRequestedOrderSpecial());
        if ($oOrder->exists()) {
            ML::gi()->init(array('mp' => $oOrder->get('mpid')));
            $aOut = array(
                '$oOrder->data()' => $oOrder->data(),
                'shop' => array(
                    '$oOrder->getShopOrderStatus()' => $oOrder->getShopOrderStatus(),
                    '$oOrder->getShopOrderLastChangedDate()' => $oOrder->getShopOrderLastChangedDate(),
                    '$oOrder->getShippingDateTime()' => $oOrder->getShippingDateTime(),
                    '$oOrder->getShippingCarrier()' => $oOrder->getShippingCarrier(),
                    '$oOrder->getShippingTrackingCode()' => $oOrder->getShippingTrackingCode(),
                ),
            );
            ML::gi()->init(array());
            new dBug($aOut, '', true);
        } else {
            ?>Order not found.<?php
        }
    }
?>
