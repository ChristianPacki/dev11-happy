<?php

/**
 * 888888ba                 dP  .88888.                    dP
 * 88    `8b                88 d8'   `88                   88
 * 88aaaa8P' .d8888b. .d888b88 88        .d8888b. .d8888b. 88  .dP  .d8888b.
 * 88   `8b. 88ooood8 88'  `88 88   YP88 88ooood8 88'  `"" 88888"   88'  `88
 * 88     88 88.  ... 88.  .88 Y8.   .88 88.  ... 88.  ... 88  `8b. 88.  .88
 * dP     dP `88888P' `88888P8  `88888'  `88888P' `88888P' dP   `YP `88888P'
 *
 *                          m a g n a l i s t e r
 *                                      boost your Online-Shop
 *
 * -----------------------------------------------------------------------------
 * $Id$
 *
 * (c) 2010 - 2016 RedGecko GmbH -- http://www.redgecko.de
 *     Released under the MIT License (Expat)
 * -----------------------------------------------------------------------------
 */
MLFilesystem::gi()->loadClass('Form_Controller_Widget_Form_PrepareAbstract');

abstract class ML_Form_Controller_Widget_Form_PrepareWithVariationMatchingAbstract extends ML_Form_Controller_Widget_Form_PrepareAbstract
{

    protected $shopAttributes;
    protected $numberOfMaxAdditionalAttributes = 0;
    protected $aParameters = array('controller');

    public function construct()
    {
        parent::construct();
        $this->oPrepareHelper->bIsSinglePrepare = $this->oSelectList->getCountTotal() === '1';
    }

    public function render()
    {
        $this->getFormWidget();
        return $this;
    }

    public function getRequestField($sName = null, $blOptional = false)
    {
        if (count($this->aRequestFields) == 0) {
            $this->aRequestFields = $this->getRequest($this->sFieldPrefix);
            $this->aRequestFields = is_array($this->aRequestFields) ? $this->aRequestFields : array();
        }

        return parent::getRequestField($sName, $blOptional);
    }

    protected function getSelectionNameValue()
    {
        return 'apply';
    }

    public function getModificationDate($sIdentifier)
    {
        $aRows = $this->oPrepareList->getList();
        return count($aRows) > 0 ? current($aRows)->get(MLDatabase::getPrepareTableInstance()->getPreparedTimestampFieldName()) : '';
    }

    protected function getCustomIdentifier()
    {
        $sCustomIdentifier = $this->getRequestField('customidentifier');
        return !empty($sCustomIdentifier) ? $sCustomIdentifier : '';
    }

    protected function triggerBeforeFinalizePrepareAction()
    {
        $aActions = $this->getRequest($this->sActionPrefix);
        $oPrepareTable = MLDatabase::getPrepareTableInstance();
        $savePrepare = $aActions['prepareaction'] === '1';
        $this->oPrepareList->set('preparetype', $this->getSelectionNameValue());
        $this->setPreparedStatusTrue();

        if ($savePrepare && !empty($this->oPrepareHelper->aErrors)) {
            $productIDs = array();
            foreach ($this->oPrepareHelper->aErrors as $error) {
                if (is_array($error)) {
                    $productIDs[] = $error['product_id'];
                    $error = $error['message'];
                }

                MLMessage::gi()->addError(MLI18n::gi()->get($error));
            }

            $this->setPreparedStatusFalse($productIDs);
            return false;
        }


        $aMatching = $this->getRequestField();
        $sIdentifier = isset($aMatching['variationgroups.value']) ? $aMatching['variationgroups.value'] : '';
        $this->oPrepareList->set($oPrepareTable->getPrimaryCategoryFieldName(), $sIdentifier);

        if (empty($sIdentifier) && !empty($aMatching['variationgroups'])){
            $aCache = array_keys($aMatching['variationgroups']);
            $sIdentifier = array_shift($aCache);
            unset($aCache);
        }

        if (empty($sIdentifier)) {
            MLMessage::gi()->addError(MLI18n::gi()->get($this->getMPName() . '_prepareform_category'));

            $this->setPreparedStatusFalse();
            return false;
        }

        $sCustomIdentifier = $this->getCustomIdentifier();
        if (isset($aMatching['variationgroups'])) {
            $aMatching = $aMatching['variationgroups'][$sIdentifier];
            $oVariantMatching = $this->getVariationDb();

            if ($sIdentifier === 'new') {
                $sIdentifier = $aMatching['variationgroups.code'];
                unset($aMatching['variationgroups.code']);
            }

            $aErrors = array();
            foreach ($aMatching as $key => &$value) {
                if (isset($value['Required'])) {
                    $value['Required'] = (bool)$value['Required'];
                }

                $value['Error'] = false;
                $sAttributeName = !empty($value['CustomName']) ? $value['CustomName'] : $value['AttributeName'];

                if ($value['Code'] == '' || empty($value['Values'])) {
                    if (isset($value['Required']) && $value['Required']) {
                        $this->setPreparedStatusFalse();
                    }

                    if (isset($value['Required']) && $value['Required'] && $savePrepare) {
                        $aErrors[] = self::getMessage('_prepare_variations_error_text',
                            array('attribute_name' => $sAttributeName));
                        $value['Error'] = true;
                    } else {
                        unset($aMatching[$key]);
                    }

                    continue;
                }

                if (!is_array($value['Values']) || !isset($value['Values']['FreeText'])) {
                    continue;
                }

                $sInfo = self::getMessage('_prepare_variations_manualy_matched');
                $sFreeText = $value['Values']['FreeText'];
                unset($value['Values']['FreeText']);

                if ($value['Values']['0']['Shop']['Key'] === 'noselection' || $value['Values']['0']['Marketplace']['Key'] === 'noselection') {
                    unset($value['Values']['0']);
                    if (empty($value['Values']) && $value['Required'] && $savePrepare) {
                        $aErrors[] = self::getMessage('_prepare_variations_error_text', array('attribute_name' => $sAttributeName));
                        $value['Error'] = true;
                    }

                    foreach ($value['Values'] as $k => &$v) {
                        if (empty($v['Marketplace']['Info']) || $v['Marketplace']['Key'] === 'manual') {
                            $v['Marketplace']['Info'] = $v['Marketplace']['Value'] . self::getMessage('_prepare_variations_free_text_add');
                        }
                    }

                    continue;
                }

                if ($value['Values']['0']['Marketplace']['Key'] === 'reset') {
                    unset($aMatching[$key]);
                    continue;
                }

                if ($value['Values']['0']['Marketplace']['Key'] === 'manual') {
                    $sInfo = self::getMessage('_prepare_variations_free_text_add');
                    if (empty($sFreeText)) {
                        if ($savePrepare) {
                            $aErrors[] = $key . self::getMessage('_prepare_variations_error_free_text');
                            $value['Error'] = true;
                        }

                        unset($value['Values']['0']);
                        continue;
                    }

                    $value['Values']['0']['Marketplace']['Value'] = $sFreeText;
                }

                if ($value['Values']['0']['Marketplace']['Key'] === 'auto') {
                    $this->autoMatch($sIdentifier, $key, $value);
                    $value['Values'] = $this->fixAttributeValues($value['Values']);
                    continue;
                }

                $this->checkNewMatchedCombination($value['Values']);
                if ($value['Values']['0']['Shop']['Key'] === 'all') {
                    $newValue = array();
                    $i = 0;
                    foreach ($this->getShopAttributeValues($value['Code']) as $keyAttribute => $valueAttribute) {
                        $marketplaceValue = ($value['Values']['0']['Marketplace']['Key'] === 'manual') ?
                            $value['Values']['0']['Marketplace']['Value'] : $value['Values']['0']['Marketplace']['Key'];
                        $newValue[$i]['Shop']['Key'] = $keyAttribute;
                        $newValue[$i]['Shop']['Value'] = $valueAttribute;
                        $newValue[$i]['Marketplace']['Key'] = $value['Values']['0']['Marketplace']['Key'];
                        $newValue[$i]['Marketplace']['Value'] = $marketplaceValue;
                        $newValue[$i]['Marketplace']['Info'] = $value['Values']['0']['Marketplace']['Value'] . $sInfo;
                        $i++;
                    }

                    $value['Values'] = $newValue;
                } else {
                    foreach ($value['Values'] as $k => &$v) {
                        if (empty($v['Marketplace']['Info'])) {
                            $v['Marketplace']['Info'] = $v['Marketplace']['Value'] . $sInfo;
                        }

                        $v['Marketplace']['Value'] = $v['Marketplace']['Key'];
                    }
                }

                $value['Values'] = $this->fixAttributeValues($value['Values']);
            }

            $sMatching = json_encode($aMatching);
            $this->oPrepareList->set('shopvariation', $sMatching);

            if (!empty($aErrors)) {
                foreach ($aErrors as $sError) {
                    MLMessage::gi()->addError($sError);
                }

                $this->setPreparedStatusFalse();
                return false;
            }

            if (!$savePrepare) {
                // stay on prepare form
                return false;
            }

            if (empty($aErrors)) {
                $aShopVariation = $oVariantMatching
                    ->set('Identifier', $sIdentifier)
                    ->set('CustomIdentifier', $sCustomIdentifier)
                    ->get('ShopVariation');

                if (!isset($aShopVariation)) {
                    $oVariantMatching
                        ->set('Identifier', $sIdentifier)
                        ->set('CustomIdentifier', $sCustomIdentifier)
                        ->set('ShopVariation', $sMatching)
                        ->set('ModificationDate', date('Y-m-d H:i:s'))
                        ->save();
                }

                MLMessage::gi()->addSuccess(self::getMessage('_prepare_match_variations_saved'));
            }
        }

        return true;
    }

    private function fixAttributeValues($values) {
        if (isset($values['0'])) {
            $fixedValues = array();
            $i = 1;
            foreach ($values as $value) {
                $fixedValues[$i] = $value;
                $i++;
            }

            return $fixedValues;
        }

        return $values;
    }

    protected function setPreparedStatusTrue() {
        $this->oPrepareList->set('verified', 'OK');
    }
    
    protected function setPreparedStatusFalse($productIDs = array()) {
        if (!empty($productIDs)) {
            foreach ($productIDs as $key) {
                $prepareItem = $this->oPrepareList->getByKey('[' . $key . ']');
                if (isset($prepareItem)) {
                    $prepareItem->set('verified', 'ERROR');
                }
            }
        } else {
            $this->oPrepareList->set('verified', 'ERROR');
        }
    }

    public function triggerBeforeField(&$aField)
    {
        parent::triggerBeforeField($aField);
        $sName = $aField['realname'];
        if ($sName === 'variationgroups.value') {
            return;
        }

        if (MLHttp::gi()->isAjax()) {
            $aRequestTriggerField = MLRequest::gi()->get('ajaxData');
            if ($aRequestTriggerField['method'] === 'variationmatching') {
                unset($aField['value']);
                return;
            }
        }

        if (!isset($aField['value'])) {
            $mValue = null;
            $aRequestFields = $this->getRequestField();
            $aNames = explode('.', $aField['realname']);
            if (count($aNames) > 1 && isset($aRequestFields[$aNames[0]])) {
                // parent real name is in format "variationgroups.qnvjagzvcm1hda____.rm9ybwf0.code"
                // and name in request is "[variationgroups][Buchformat][Format][Code]"
                $sName = $sKey = $aNames[0];
                $aTmp = $aRequestFields[$aNames[0]];
                for ($i = 1; $i < count($aNames); $i++) {
                    if (is_array($aTmp)) {
                        foreach ($aTmp as $key => $value) {
                            if (strtolower($key) === 'code') {
                                break;
                            } elseif (strtolower($key) == $aNames[$i]) {
                                $sName .= '.' . $key;
                                $sKey = $key;
                                $aTmp = $value;
                                break;
                            }
                        }
                    } else {
                        break;
                    }
                }

                if (isset($sKey) && $sKey !== $aNames[0] && !is_array($value)) {
                    $mValue = array($sKey => $value, 'name' => $sName);
                }
            }

            if ($mValue != null) {
                $aField['value'] = reset($mValue);
                $aField['valuearr'] = $mValue;
            }
        }
    }

    public function triggerAfterField(&$aField, $parentCall = false)
    {
        //TODO Check this parent call
        parent::triggerAfterField($aField);

        if ($parentCall) {
            return;
        }

        $sName = $aField['realname'];

        // when top variation groups drop down is changed, its value is updated in getRequestValue
        // otherwise, it should remain empty.
        // without second condition this function will be executed recursevly because of the second line below.
        if (!isset($aField['value'])) {
            $sProductId = $this->getProductId();

            $oPrepareTable = MLDatabase::getPrepareTableInstance();
            $sShopVariationField = $oPrepareTable->getShopVariationFieldName();

            $aPrimaryCategories = $this->oPrepareList->get($oPrepareTable->getPrimaryCategoryFieldName());
            $sPrimaryCategoriesValue = isset($aPrimaryCategories['[' . $sProductId . ']']) ? $aPrimaryCategories['[' . $sProductId . ']'] : reset($aPrimaryCategories);
            if ($sName === 'variationgroups.value') {
                $aField['value'] = $sPrimaryCategoriesValue;
            } else {
                // check whether we're getting value for standard group or for custom variation mathing group
                $sCustomGroupName = $this->getField('variationgroups.value', 'value');
                $aCustomIdentifier = explode(':', $sCustomGroupName);

                if (count($aCustomIdentifier) == 2 && ($sName === 'attributename' || $sName === 'customidentifier')) {
                    $aField['value'] = $aCustomIdentifier[$sName === 'attributename' ? 0 : 1];
                    return;
                }

                $aNames = explode('.', $sName);
                if (count($aNames) == 4 && strtolower($aNames[3]) === 'code') {
                    $aValue = $this->oPrepareList->get($sShopVariationField);
                    $aValueFix = isset($aValue['[' . $sProductId . ']']) ? $aValue['[' . $sProductId . ']'] : reset($aValue);
                    if (!isset($aValueFix) || strtolower($sPrimaryCategoriesValue) !== strtolower($aNames[1])) {
                        // real name is in format "variationgroups.qnvjagzvcm1hda____.rm9ybwf0.code"
                        $sCustomIdentifier = count($aCustomIdentifier) == 2 ? $aCustomIdentifier[1] : '';
                        if (empty($sCustomIdentifier)) {
                            $sCustomIdentifier = $this->getCustomIdentifier();
                        }

                        $aValue = $this->getVariationDb()
                            ->set('Identifier', $aNames[1])
                            ->set('CustomIdentifier', $sCustomIdentifier)
                            ->get('ShopVariation');
                    } else {
                        $aValue = $aValueFix;
                    }

                    if ($aValue) {
                        foreach ($aValue as $sKey => $aMatch) {
                            if (strtolower($sKey) === $aNames[2]) {
                                $aField['value'] = $aMatch['Code'];
                                break;
                            }
                        }
                    }
                }
            }
        }
    }

    protected function variationGroupsField(&$aField)
    {
        $aField['subfields']['variationgroups.value']['values'] = array('' => '..') + $this->getPrimaryCategoryFieldValues();

        foreach ($aField['subfields'] as &$aSubField) {
            //adding current cat, if not in top cat
            if (!array_key_exists((string)$aSubField['value'], $aSubField['values'])) {
                $oCat = MLDatabase::factory(self::getMPName() . '_categories' . $aSubField['cattype']);
                $oCat->init(true)->set('categoryid', $aSubField['value'] ? $aSubField['value'] : 0);
                $sCat = '';
                foreach ($oCat->getCategoryPath() as $oParentCat) {
                    $sCat = $oParentCat->get('categoryname') . ' &gt; ' . $sCat;
                }

                $aSubField['values'][$aSubField['value']] = substr($sCat, 0, -6);
            }
        }
    }

    protected function variationMatchingField(&$aField)
    {
        $aField['ajax'] = array(
            'selector' => '#' . $this->getField('variationgroups.value', 'id'),
            'trigger' => 'change',
            'field' => array(
                'type' => 'switch',
            ),
        );
    }

    protected function variationGroups_ValueField(&$aField)
    {
        $aField['type'] = 'categoryselect';
        $aField['cattype'] = 'marketplace';
    }

    protected function getPrimaryCategoryFieldValues()
    {
        return ML::gi()->instance('controller_' . self::getMPName() . '_config_prepare')->getField('primarycategory', 'values');
    }

    protected function loadCategoryAttributesFromMP($sCategoryId)
    {
        return MagnaConnector::gi()->submitRequestCached(array(
                'ACTION' => 'GetCategoryDetails',
                'DATA' => array('CategoryID' => $sCategoryId))
        );
    }

    public function getMPVariationAttributes($sVariationValue)
    {
        $aValues = $this->loadCategoryAttributesFromMP($sVariationValue);
        $result = array();
        if ($aValues) {
            foreach ($aValues['DATA']['attributes'] as $key => $value) {
                $result[$key] = array(
                    'value' => $value['title'],
                    'required' => isset($value['mandatory']) ? $value['mandatory'] : true,
                    'changed' => isset($value['changed']) ? $value['changed'] : null,
                    'desc' => isset($value['desc']) ? $value['desc'] : '',
                    'values' => !empty($value['values']) ? $value['values'] : array(),
                    'dataType' => !empty($value['type']) ? $value['type'] : 'text',
                );
            }
        }

        $aResultFromDB = $this->getPreparedData($sVariationValue, '');

        if (empty($aResultFromDB)) {
            $aResultFromDB = $this->getAttributesFromDB($sVariationValue);;
        }

        $iFromDb = 0;
        if (!empty($aResultFromDB)) {
            $array = $this->arrayFilterKey($aResultFromDB, function ($key) {
                return strpos($key, 'additional_attribute_') === 0;
            });

            $iFromDb = count($array);
        }

        if ($iFromDb < $this->numberOfMaxAdditionalAttributes || $this->numberOfMaxAdditionalAttributes === -1) {
            $iFromDb++;
        }

        for ($i = 0; $i < $iFromDb; $i++) {
            $result['additional_attribute_' . $i] = array(
                'value' => self::getMessage('_prepare_variations_additional_attribute_label'),
                'customAttributeValue' => isset($aResultFromDB['additional_attribute_' . $i]['CustomName']) ?
                    $aResultFromDB['additional_attribute_' . $i]['CustomName'] : null,
                'custom' => true,
                'required' => false,
            );
        }

        $this->detectChanges($result, $sVariationValue);

        return $result;
    }

    /**
     * @param $sIdentifier
     * @param $sCustomIdentifier
     * @return mixed
     */
    protected function getPreparedData($sIdentifier, $sCustomIdentifier)
    {
        $sProductId = $this->getProductId();

        $oPrepareTable = MLDatabase::getPrepareTableInstance();
        $sShopVariationField = $oPrepareTable->getShopVariationFieldName();
        $sPrimaryCategory = $this->oPrepareList->get($oPrepareTable->getPrimaryCategoryFieldName());

        $sPrimaryCategoryValue = isset($sPrimaryCategory['[' . $sProductId . ']']) ? $sPrimaryCategory['[' . $sProductId . ']'] : reset($sPrimaryCategory);
        if (!empty($sPrimaryCategory)) {
            if ($sPrimaryCategoryValue === $sIdentifier) {
                $aShopVariation = $this->oPrepareList->get($sShopVariationField);
                if (isset($aShopVariation) && !empty($aShopVariation)) {
                    $aValue = isset($aShopVariation['[' . $sProductId . ']']) ? $aShopVariation['[' . $sProductId . ']'] : reset($aShopVariation);
                }
            }
        }

        if (!isset($aValue)) {
            $aValue = $this->getVariationDb()
                ->set('Identifier', $sIdentifier)
                ->set('CustomIdentifier', $sCustomIdentifier)
                ->get('ShopVariation');
            return $aValue;
        }
        return $aValue;
    }

    protected function getAttributeValues($sIdentifier, $sCustomIdentifier, $sAttributeCode = null, $bFreeText = false)
    {
        $aValue = $this->getPreparedData($sIdentifier, $sCustomIdentifier);

        if ($aValue) {
            if ($sAttributeCode !== null) {
                foreach ($aValue as $sKey => $aMatch) {
                    if ($sKey === $sAttributeCode) {
                        return isset($aMatch['Values']) ? $aMatch['Values'] : ($bFreeText ? '' : array());
                    }
                }
            } else {
                return $aValue;
            }
        }

        if ($bFreeText) {
            return '';
        }

        return array();
    }

    protected function getShopAttributes()
    {
        if ($this->shopAttributes == null) {
            $shopAllAttributes = MLFormHelper::getShopInstance()->getPrefixedAttributeList(true);
            $shopVariationAttributes = MLFormHelper::getShopInstance()->getAttributeListWithOptions();

            $shopArticleAttributes = array_diff_key($shopAllAttributes, $shopVariationAttributes);
            $shopVariationFixedAttributes = array_intersect_key($shopAllAttributes, $shopVariationAttributes);

            $shopVariationFixedAttributes['separator_line_1'] = self::getMessage('_prepare_variations_separator_line_label');
            $this->shopAttributes = $shopVariationFixedAttributes + $shopArticleAttributes;
        }

        return $this->shopAttributes;
    }

    protected function getShopAttributeValues($sAttributeCode)
    {
        $shopValues = MLFormHelper::getShopInstance()->getPrefixedAttributeOptions($sAttributeCode);
        if (!isset($shopValues) || empty($shopValues)) {
            $shopValues = MLFormHelper::getShopInstance()->getAttributeOptions($sAttributeCode);
        }

        return $shopValues;
    }

    protected function getMPAttributeValues($sCategoryId, $sMpAttributeCode, $sAttributeCode = false)
    {
        $response = MagnaConnector::gi()->submitRequest(array('ACTION' => 'GetCategoryDetails', 'DATA' => array('CategoryID' => $sCategoryId)));
        $fromMP = false;
        foreach ($response['DATA']['attributes'] as $key => $attribute) {
            if ($key === $sMpAttributeCode && !empty($attribute['values'])) {
                $aValues = $attribute['values'];
                $fromMP = true;
                break;
            }
        }

        if (!isset($aValues)) {
            if ($sAttributeCode) {
                $shopValues = $this->getShopAttributeValues($sAttributeCode);
                foreach ($shopValues as $value) {
                    $aValues[$value] = $value;
                }
            } else {
                $aValues = array();
            }
        }

        return array(
            'values' => isset($aValues) ? $aValues : array(),
            'from_mp' => $fromMP
        );
    }

    protected function getAttributesFromDB($sIdentifier, $sCustomIdentifier = '')
    {
        $aValue = $this->getVariationDb()
            ->set('Identifier', $sIdentifier, false)
            ->set('CustomIdentifier', $sCustomIdentifier)
            ->get('ShopVariation');

        if ($aValue) {
            return $aValue;
        }

        return array();
    }

    protected function getErrorValue($sIdentifier, $sCustomIdentifier, $sAttributeCode)
    {
        $aValue = $this->oPrepareList->get('shopvariation');
        $sProductId = $this->getProductId();

        if (!empty($aValue['[' . $sProductId . ']'])) {
            foreach ($aValue['[' . $sProductId . ']'] as $sKey => $aMatch) {
                if ($sKey === $sAttributeCode) {
                    return $aMatch['Error'];
                }
            }
        }

        return false;
    }

    protected function callApi($actionName, $aData = array(), $iLifeTime = 60)
    {
        try {
            $aResponse = MagnaConnector::gi()->submitRequestCached(array('ACTION' => $actionName, 'DATA' => $aData), $iLifeTime);
            if ($aResponse['STATUS'] == 'SUCCESS' && isset($aResponse['DATA']) && is_array($aResponse['DATA'])) {
                return $aResponse['DATA'];
            }
        } catch (MagnaException $e) {

        }

        return array();
    }

    /**
     * @return ML_Database_Model_Table_VariantMatching_Abstract
     */
    protected function getVariationDb()
    {
        return MLDatabase::getVariantMatchingTableInstance();
    }

    protected function autoMatch($categoryId, $sMpAttributeCode, &$aAttributes)
    {
        $aMPAttributeValues = $this->getMPAttributeValues($categoryId, $sMpAttributeCode, $aAttributes['Code']);
        $sInfo = self::getMessage('_prepare_variations_auto_matched');
        $blFound = false;
        if ($aAttributes['Values']['0']['Shop']['Key'] === 'all') {
            $newValue = array();
            $i = 0;
            foreach ($this->getShopAttributeValues($aAttributes['Code']) as $keyAttribute => $valueAttribute) {
                foreach ($aMPAttributeValues['values'] as $key => $value) {
                    if (strcasecmp($valueAttribute, $value) == 0) {
                        $newValue[$i]['Shop']['Key'] = $keyAttribute;
                        $newValue[$i]['Shop']['Value'] = $valueAttribute;
                        $newValue[$i]['Marketplace']['Key'] = $key;
                        $newValue[$i]['Marketplace']['Value'] = $key;
                        $newValue[$i]['Marketplace']['Info'] = $value . $sInfo;
                        $blFound = true;
                        $i++;
                        break;
                    }
                }
            }

            $aAttributes['Values'] = $newValue;
        } else {
            foreach ($aMPAttributeValues['values'] as $key => $value) {
                if (strcasecmp($aAttributes['Values']['0']['Shop']['Value'], $value) == 0) {
                    $aAttributes['Values']['0']['Marketplace']['Key'] = $key;
                    $aAttributes['Values']['0']['Marketplace']['Value'] = $key;
                    $aAttributes['Values']['0']['Marketplace']['Info'] = $value . $sInfo;
                    $blFound = true;
                    break;
                }
            }
        }

        if (!$blFound) {
            unset($aAttributes['Values']['0']);
        }

        $this->checkNewMatchedCombination($aAttributes['Values']);
    }

    protected function checkNewMatchedCombination(&$aAttributes)
    {
        foreach ($aAttributes as $key => $value) {
            if ($key === 0) {
                continue;
            }

            if (isset($aAttributes['0']) && $value['Shop']['Key'] === $aAttributes['0']['Shop']['Key']) {
                unset($aAttributes[$key]);
                break;
            }
        }
    }

    /**
     * Checks for each attribute whether it is prepared differently in Variation Matching tab, and if so, marks it Modified.
     * Arrays cannot be compared directly because values could be in different order (with different numeric keys).
     *
     * @param $result
     * @param $sIdentifier
     */
    protected function detectChanges(&$result, $sIdentifier)
    {
        // similar validation exists in ML_Productlist_Model_ProductList_Abstract::isPreparedDifferently
        $globalMatching = MLDatabase::getVariantMatchingTableInstance()->getMatchedVariations($sIdentifier, $this->getCustomIdentifier());

        $oPrepareTable = MLDatabase::getPrepareTableInstance();
        $sShopVariationField = $oPrepareTable->getShopVariationFieldName();
        $sProductId = $this->getProductId();

        $productMatching = $oPrepareTable
            ->set($oPrepareTable->getPrimaryCategoryFieldName(), $sIdentifier)
            ->set($oPrepareTable->getProductIdFieldName(), $sProductId)
            ->get($sShopVariationField);

        if (is_array($globalMatching)){
            foreach ($globalMatching as $attributeCode => $attributeSettings){
                if (!empty($productMatching[$attributeCode])){
                    $productAttrs = $productMatching[$attributeCode];
                    if (!isset($productAttrs['Values'])) {
                        continue;
                    }

                    if (!is_array($productAttrs['Values']) || !is_array($attributeSettings['Values'])){
                        $result[$attributeCode]['modified'] = $productAttrs != $attributeSettings;
                        continue;
                    }

                    $productAttrsValues = $productAttrs['Values'];
                    $attributeSettingsValues = $attributeSettings['Values'];
                    unset($productAttrs['Values']);
                    unset($attributeSettings['Values']);

                    // first compare without values (optimization)
                    if ($productAttrs['Code'] == $attributeSettings['Code'] && count($productAttrsValues) === count($attributeSettingsValues)){
                        // compare values
                        // values could be in different order so we need to iterate through array and check one by one
                        $allValuesMatched = true;
                        foreach ($productAttrsValues as $attribute){
                            unset($attribute['Marketplace']['Info']);
                            $found = false;
                            foreach ($attributeSettingsValues as $value){
                                unset($value['Marketplace']['Info']);
                                if ($attribute == $value){
                                    $found = true;
                                    break;
                                }
                            }

                            if (!$found){
                                $allValuesMatched = false;
                                break;
                            }
                        }

                        if ($allValuesMatched && isset($result[$attributeCode])){
                            $result[$attributeCode]['modified'] = false;
                            continue;
                        }
                    }

                    if (isset($result[$attributeCode])){
                        $result[$attributeCode]['modified'] = true;
                    }
                }
            }
        }
    }

    protected function arrayFilterKey($input, $callback)
    {
        if (!is_array($input)) {
            trigger_error('array_filter_key() expects parameter 1 to be array, ' . gettype($input) . ' given', E_USER_WARNING);
            return null;
        }

        if (empty($input)) {
            return $input;
        }

        $filteredKeys = array_filter(array_keys($input), $callback);
        if (empty($filteredKeys)) {
            return array();
        }

        $input = array_intersect_key(array_flip($filteredKeys), $input);

        return $input;
    }

    protected function getProductId()
    {
        if (isset($this->oProduct)) {
            $aVariations = $this->oProduct->getVariants();
            if (isset($aVariations) && count($aVariations) > 1) {
                return $aVariations[0]->get('id');
            }

            return $sProductId = $this->oProduct->get('id');
        }

        return null;
    }

    protected static function getMessage($sIdentifier, $aReplace = array())
    {
        return MLI18n::gi()->get(MLModul::gi()->getMarketPlaceName() . $sIdentifier, $aReplace);
    }

    protected static function getMPName()
    {
        return MLModul::gi()->getMarketPlaceName();
    }
}
