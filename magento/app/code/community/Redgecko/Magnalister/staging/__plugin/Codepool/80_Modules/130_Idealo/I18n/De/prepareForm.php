<?php
MLI18n::gi()->add('idealo_prepare_form',array(
    'legend' => array(
        'categories' => 'Idealo Kategorie',
        'shipping' => 'Versand',
        'details' => 'Angebotseinstellungen',
    ),
    'field' => array(
        'title' => array(
            'label' => 'Title',
            'hint'  => '',
        ),
        'description' => array(
            'label' => 'Beschreibung',
            'hint'  => '',
        ),
        'image' => array(
            'label' => 'Produktbilder',
            'hint' => 'Maximal 3 Produktbilder ',
        ),
        'shippingcountry' => array (
            'label' => 'Versand nach',
        ),
        'shippingmethodandcost' => array(
            'label' => 'Versandkosten',
            'help' => 'Es wird zun&auml;chst versucht die Versandkosten aus der Versandmethode zu berechnen.
                Falls diese kein Ergebnis liefert wird der hier angegebene Wert verwendet.<br>
                Bei Versandkostenmodul "Pauschal" wird ebenfalls der hier angegebene Wert verwendet.',
        ),
        'shippingmethod' => array(
            'label' => '',
            'valuehint' => 'Beispiel: 6.70 (Ohne Angabe einer W&auml;hrung)',
        ),
        'shippingcost' => array(
            'label' => 'Versandkosten',
        )
    )
),false);