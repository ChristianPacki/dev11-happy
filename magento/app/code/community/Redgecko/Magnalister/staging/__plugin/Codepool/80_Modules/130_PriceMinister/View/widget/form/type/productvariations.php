<?php
/**
 * 888888ba                 dP  .88888.                    dP
 * 88    `8b                88 d8'   `88                   88
 * 88aaaa8P' .d8888b. .d888b88 88        .d8888b. .d8888b. 88  .dP  .d8888b.
 * 88   `8b. 88ooood8 88'  `88 88   YP88 88ooood8 88'  `"" 88888"   88'  `88
 * 88     88 88.  ... 88.  .88 Y8.   .88 88.  ... 88.  ... 88  `8b. 88.  .88
 * dP     dP `88888P' `88888P8  `88888'  `88888P' `88888P' dP   `YP `88888P'
 *
 *                          m a g n a l i s t e r
 *                                      boost your Online-Shop
 *
 * -----------------------------------------------------------------------------
 * $Id$
 *
 * (c) 2010 - 2014 RedGecko GmbH -- http://www.redgecko.de
 *     Released under the MIT License (Expat)
 * -----------------------------------------------------------------------------
 */
/** @var ML_Hitmeister_Controller_Hitmeister_Prepare_Variations $this */
class_exists('ML', false) or die();
$marketplaceName = MLModul::gi()->getMarketPlaceName();

// Getting type of tab (is it variation tab or apply form)

//$sCustomAttribute = $this->getField('attributename', 'value');
//if ($sCustomAttribute !== null) {
//    $mParentValue = $sCustomAttribute;
//}

if (strpos($mParentValue, ':') !== false) {
    $mParentValue = explode(':', $mParentValue);
    $mParentValue = $mParentValue[0];
}

$i18n = $this->getFormArray('aI18n');
if (!empty($mParentValue) && $mParentValue !== 'none' && $mParentValue !== 'new') {
    $aShopAttributes = $this->getShopAttributes();
    $dModificationDate = $this->getModificationDate($mParentValue);

    $aShopAttributes['separator_line_2'] = MLI18n::gi()->get($marketplaceName . '_prepare_variations_separator_line_label');
    $aShopAttributes['freetext'] = MLI18n::gi()->get($marketplaceName . '_prepare_variations_free_text');
    $aShopAttributes['attribute_value'] = MLI18n::gi()->get($marketplaceName . '_prepare_variations_choose_mp_value');
    $aMPAttributes = $this->getMPVariationAttributes($mParentValue);

    $aFieldset = array(
        'id' => $this->getIdent() . '_fieldset_' . $mParentValue,
        'legend' => array(
            'i18n' => $i18n['legend']['variationmatching'],
            'template' => 'two-columns',
        ),
        'row' => array(
            'template' => 'default',
        ),
    );

    foreach ($aMPAttributes as $key => $sAttribute) {
        $aMatchedAttributes = $this->getAttributeValues($mParentValue, '', $key);

        $sBaseName = "field[variationgroups][$mParentValue][$key]";
        $sName = $sBaseName . '[Code]';
        $sId = 'variationgroups.' . $mParentValue . '.' . $key . '.code';
        $sKind = !empty($sAttribute['values']) ? 'Matching' : 'FreeText';
        $bError = $this->getErrorValue($mParentValue, '', $key);

        $aSelectField = $this->getField($sId);
        $aSelectField['type'] = 'select';
        $aSelectField['values'] = $aShopAttributes;
        $aSelectField['name'] = $sName;
        $aSelectField['i18n'] = $i18n['field']['webshopattribute'];
        $style = '';
        if ($bError == true) {
            $aSelectField['cssclass'] = 'error';
            $style = 'color:red';
        }

        $aAjaxField = $this->getField($sId . '_ajax');
        $aAjaxField['type'] = 'attributeajax';
        $aAjaxField['cascading'] = true;
        $aAjaxField['breakbefore'] = true;
        $aAjaxField['padding-right'] = 0;
        $aAjaxField['i18n']['label'] = '';
        if (isset($aSelectField['value']) && $aSelectField['value'] != null) {
            // value field on ajax is used to initialize cascading ajax fields in attributematch.php
            // when variation group is selected
            $aAjaxField['value'] = array(
                $key => $aSelectField['value'],
                'name' => 'variationgroups.' . $mParentValue . '.' . $key,
            );
        }

        $aAjaxField['ajax'] = array(
            'selector' => '#' . $aSelectField['id'],
            'trigger' => 'change',
            'field' => array(
                'id' => $sId . '_ajax_field',
                'type' => 'productattributematch',
            ),
        );

        $aSubfield = $this->getField($sId . '_sub');
        $aSubfield['type'] = 'subFieldsContainer';
        $aSubfield['i18n']['hint'] = isset($sAttribute['desc']) ? $sAttribute['desc'] : '';
        $aSubfield['i18n']['label'] = '<p style="display: inline-table;' . $style . '">' . $sAttribute['value'] . (($sAttribute['required']) ? '<span class="bull" style="color:red">&bull;</span></p>' : '');
        $aSubfield['subfields']['select'] = $aSelectField;

        $aSubfieldExtra = $this->getField($sId . '_sub');
        $aSubfieldExtra['type'] = 'subFieldsContainer';

        if (!empty($aMatchedAttributes)) {
            if (!empty($aSelectField['value'])) {
                $aSubfieldExtra['subfields']['deletebutton'] = array(
                    'id' => $sId . '_button_matching_delete',
                    'type' => 'productdeletematchingbutton',
                    'name' => $sBaseName,
                    'i18n' => array(
                        'info' => MLI18n::gi()->get($marketplaceName . '_prepare_variations_already_matched'),
                    ),
                );
            }

            if (!empty($aSelectField['value']) && !empty($sAttribute['changed']) && !empty($dModificationDate)
                && strtotime($sAttribute['changed']) > strtotime($dModificationDate)) {
                $aSubfieldExtra['subfields']['warning'] = array(
                    'type' => 'warning',
                    'name' => $sBaseName . '[Warning]',
                    'id' => $sId . '_warning',
                    'i18n' => array(
                        'title' => $sAttribute['value'],
                        'text' => MLI18n::gi()->get($marketplaceName . '_varmatch_attribute_changed_on_mp'),
                    ),
                );
            }

            if (!empty($aSelectField['value']) && isset($sAttribute['modified']) && $sAttribute['modified'] !== false) {
                $aSubfieldExtra['subfields']['warning'] = array(
                    'type' => 'warning',
                    'name' => $sBaseName . '[Warning]',
                    'id' => $sId . '_warning',
                    'i18n' => array(
                        'title' => $sAttribute['value'],
                        'text' => MLI18n::gi()->get($marketplaceName . '_varmatch_attribute_different_on_product'),
                    ),
                );
            }

            $aSubfieldExtra['Expend'] = true;
        }

        $aSubfield['subfields'] = array_merge($aSubfield['subfields'], array(
            'hidden' => array(
                'type' => 'hidden',
                'name' =>  $sBaseName . '[Kind]',
                'id' => $sId . '_kind',
                'value' => $sKind,
                'padding-right' => 0,
            ),
            'secondhidden' => array(
                'type' => 'hidden',
                'name' =>  $sBaseName . '[Required]',
                'id' => $sId . '_required',
                'value' => $sAttribute['required'] ? true : false, // has to be bool
                'padding-right' => 0,
            ),
            'thirdhidden' => array(
                'type' => 'hidden',
                'name' =>  $sBaseName . '[AttributeName]',
                'id' => $sId . '_attribute_name',
                'value' => $sAttribute['value'],
                'padding-right' => 0,
            ),
        ));

        $aFieldset['fields'][] = array(
            'subFieldsContainer' => $aSubfield,
            'subFieldsContainerExtra' => $aSubfieldExtra,
            'ajax' => $aAjaxField,
        );
    }

    $aSavedValues = $this->getAttributeValues($mParentValue, '');
    foreach ($aSavedValues as $sCode => $aAttribute) {
        if (empty($aMPAttributes[$sCode])) {
            // deleted from marketplace
            $sId = 'variationgroups.' . $mParentValue . '.' . $sCode . '.code';

            $aSubfield = $this->getField($sId . '_sub');
            $aSubfield['type'] = 'subFieldsContainer';
            $aSubfield['i18n']['hint'] = '';
            $aSubfield['i18n']['label'] = '<p class="error">' . $aAttribute['AttributeName'] . '</p>';
            $aSubfield['subfields']['information'] = array(
                'type' => 'information',
                'value' => '<p class="error">' . MLI18n::gi()->get($marketplaceName . '_varmatch_attribute_deleted_from_mp') . '</p>',
            );

            $aFieldset['fields'][] = $aSubfield;
        }
    }
    ?>
    <table class="attributesTable" id="attributesTable">
        <?php $this->includeView('widget_form_type_attributefield', array('aFieldset' => $aFieldset)); ?>
    </table>
    <p><?php echo MLI18n::gi()->get($marketplaceName . '_prepare_variations_mandatory_fields_info') ?></p>
    <script>
        (function($) {
            $(document).ready(function() {

                $('#attributesTable > tbody > tr').removeClass('odd even');

                $('select option[value="separator_line_1"]').attr('disabled', 'disabled');
                $('select option[value="separator_line_2"]').attr('disabled', 'disabled');
                $('#<?php echo $marketplaceName ?>_prepare_variations_field_variationgroups_value').change(function() {
                    $('div.noticeBox').remove();
                });

                function fireAttributeAjaxRequest(eElement, ajaxAdditional, selector, oldValue) {
                    var selectorName = selector.substring(1);
                    $('[id^=attributeDropDown_' + selectorName + ']').css('background-color', '');
                    if ($.trim($(selector + '_button_matched_table').html())) {
                        var d = '<?php echo addslashes(MLI18n::gi()->get($marketplaceName . '_prepare_variations_change_attribute_info')) ?>';
                        $('<div class="ml-modal dialog2" title="<?php echo MLI18n::gi()->get('ML_LABEL_NOTE')?>"></div>').html(d).jDialog({
                            width: (d.length > 1000) ? '700px' : '500px',
                            buttons: {
                                'OK': function() {
                                    $(selector).val(oldValue);
                                    $(this).dialog('close');
                                }
                            }
                        });
                    } else {
                        $('div#attributeExtraFields_' + selectorName + '_sub').hide();
                        $('div#attributeMatchedTable_' + selectorName + '_sub').show();

                        $.blockUI(blockUILoading);
                        var eForm = eElement.parentsUntil('form').parent(),
                            aData = $(eForm).serializeArray(),
                            aAjaxData = $.parseJSON(eElement.attr('data-ajax')),
                            i;

                        for (i in aAjaxData) {
                            if (aAjaxData[i]['value'] === null) {
                                aAjaxData[i]['value'] = ajaxAdditional;
                            }

                            aData.push(aAjaxData[i]);
                        }

                        eElement.hide('slide', {direction: 'right'});
                        $.ajax({
                            url: eForm.attr("action"),
                            type: eForm.attr("method"),
                            data: aData,
                            complete: function (jqXHR, textStatus) {
                                var eRow;
                                try {// need it for ebay-categories and attributes, cant do with global ajax, yet
                                    var oJson = $.parseJSON(data);
                                    var content = oJson.content;
                                    eElement.html(content);
                                } catch (oExeception) {
                                }

                                eRow = eElement.parentsUntil('.js-field').parent();
                                if (eElement.text() !== '') {
                                    eRow.show();
                                } else {
                                    eRow.hide();
                                }

                                initAttributeAjaxForm(eElement, true);
                                $.unblockUI();
                                eElement.show('slide', {direction: 'right'});
                                $(".magnalisterForm select.optional").trigger("change");
                            }
                        });
                    }
                }

                function initAttributeAjaxForm(eElements, onlyChildren) {
                    var els = eElements.find('.magnalisterAttributeAjaxForm');
                    if (!onlyChildren) {
                        els = els.andSelf();
                    }

                    els.each(function() {
                        var eElement = $(this),
                            aAjaxController = $.parseJSON(eElement.attr('data-ajax-controller'));

                        if (aAjaxController !== null) {
                            if (eElement.find(aAjaxController.selector).length === 0) {
                                var previous;
                                $(eElements).find(aAjaxController.selector).on('focus', function() {
                                    previous = $(aAjaxController.selector).val();
                                }).change(function(event) {
                                    fireAttributeAjaxRequest(eElement, event.ajaxAdditional, aAjaxController.selector, previous);
                                    previous = $(aAjaxController.selector).val();
                                });
                            } else {
                                var previous;
                                $(eElement).on('focus', $('.magnalisterForm').find(aAjaxController.selector), function() {
                                    previous = $(aAjaxController.selector).val();
                                }).change(function(event) {
                                    fireAttributeAjaxRequest(eElement, event.ajaxAdditional, aAjaxController.selector, previous);
                                    previous = $(aAjaxController.selector).val();
                                });
                            }

                            if (eElement.attr('data-ajax-trigger') === 'true') {
                                // only trigger by first load
                                eElement.attr('data-ajax-trigger', 'false');
                                fireAttributeAjaxRequest(eElement);
                            }
                        }
                    });
                }

                initAttributeAjaxForm($('.magnalisterForm'));

                // for each attribute if it has predefined values, only direct matching is possible
                // so free text fields and attributes must be disabled
                // if not, matching to attribute value must be disabled
                $('input[id$="_kind"]').each(function() {
                    var $input = $(this),
                        freeText = $input.val() === 'FreeText',
                        name = $input.attr('name').replace('[Kind]', '[Code]'),
                        $select = $('select[name="' + name + '"]'),
                        separator = false;

                    $select.find('option').each(function () {
                        var $optionId = $(this).val();
                        if (!separator && $optionId === 'separator_line_1') {
                            separator = true;
                        }

                        if (freeText && $optionId === 'attribute_value') {
                            $(this).attr('disabled', 'disabled');
                        } else if (!freeText && separator && $optionId !== 'attribute_value') {
                            $(this).attr('disabled', 'disabled');
                        }
                    });
                });

                $('button.delete-matched-value').click(function() {
                    $(this).closest('table')[0].deleteRow(this.parentNode.parentNode.rowIndex);
                    $('.<?php echo $marketplaceName ?>_prepare_match_manual_form_field_prepareaction').trigger('click');
                });

                $('button.save-freetext-value').click(function() {
                    $('.<?php echo $marketplaceName ?>_prepare_match_manual_form_field_prepareaction').trigger('click');
                });
            });
        })(jqml);
    </script>
    <?php
}
