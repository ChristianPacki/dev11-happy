<?php

class Redgecko_Magnalister_Model_PaymentMethod extends Mage_Payment_Model_Method_Abstract {

    protected $_code = 'magnalister';

    public function getTitle() {
        $aInfo = $this->getInfoInstance()->getAdditionalInformation();
        if (is_array($aInfo) && !empty($aInfo)) {
            return parent::getTitle() . ' | ' . $this->walkInfo($aInfo);
        } else {
            return parent::getTitle();
        }
    }

    protected function walkInfo($aArray, $sSeparator = ' | ') {
        if (empty($aArray)) {
            return '';
        }
        $aOut = array('');
        foreach ($aArray as $mKey => $sValue) {
            $sOut = '';
            if (!is_numeric($mKey)) {
                $sOut .= $mKey . ': ';
            }
            if (is_array($sValue)) {
                $sOut .= $this->walkInfo($sValue, ', ') . $sSeparator;
            } else {
                $sOut .= str_replace('<br />', ' | ', nl2br($sValue)) . $sSeparator;
            }
            while (in_array(substr($sOut, -1), array(' ', '|', ',', "\n"))) {
                $sOut = substr($sOut, 0, -1);
            }
            $aOut[] = $sOut;
        }
        return implode($sSeparator, array_filter($aOut));
    }

}
