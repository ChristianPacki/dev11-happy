<?php 
/**
 * 888888ba                 dP  .88888.                    dP                
 * 88    `8b                88 d8'   `88                   88                
 * 88aaaa8P' .d8888b. .d888b88 88        .d8888b. .d8888b. 88  .dP  .d8888b. 
 * 88   `8b. 88ooood8 88'  `88 88   YP88 88ooood8 88'  `"" 88888"   88'  `88 
 * 88     88 88.  ... 88.  .88 Y8.   .88 88.  ... 88.  ... 88  `8b. 88.  .88 
 * dP     dP `88888P' `88888P8  `88888'  `88888P' `88888P' dP   `YP `88888P' 
 *
 *                          m a g n a l i s t e r
 *                                      boost your Online-Shop
 *
 * -----------------------------------------------------------------------------
 * $Id$
 *
 * (c) 2010 - 2015 RedGecko GmbH -- http://www.redgecko.de
 *     Released under the MIT License (Expat)
 * -----------------------------------------------------------------------------
 */

if (Mage::helper('core')->isModuleEnabled('Dhl_Intraship')) {
    /**
     * DHL-Intraship extends Mage_Adminhtml_Block_Sales_Shipment_Grid too. we build parent tree
     */
	class Redgecko_Magnalister_Block_Adminhtml_Sales_Shipment_Grid_Parent extends Dhl_Intraship_Block_Adminhtml_Sales_Shipment_Grid {}
} else {
	class Redgecko_Magnalister_Block_Adminhtml_Sales_Shipment_Grid_Parent extends Mage_Adminhtml_Block_Sales_Shipment_Grid {}
}

class Redgecko_Magnalister_Block_Adminhtml_Sales_Shipment_Grid extends Redgecko_Magnalister_Block_Adminhtml_Sales_Shipment_Grid_Parent {
    
	protected function _prepareColumns(){
		$oParent = parent::_prepareColumns();
		$this->addColumnAfter('mpimage', array(
			'header' => 'magnalister',
			'index' => 'magnalister',
			'renderer' => 'Redgecko_Magnalister_Block_Adminhtml_Widget_Grid_Column_Renderer_MarketplaceImage',
            'mlOrderId' => 'order_increment_id',
			'filter' => false,
			'sortable' => false,
		), 'order_increment_id');
		$this->sortColumnsByOrder();
		return $oParent;
	}
    
}
