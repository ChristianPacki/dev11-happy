<?php
/**
 * Shopping cart api
 *
 * @category    Mage
 * @package     Mage_Checkout
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Html5design_Cedapi_Model_Cart_Api extends Mage_Checkout_Model_Cart_Product_Api
{

    public function __construct()
    {
        $this->_storeIdSessionField = "cart_store_id";
        $this->_attributesMap['quote'] = array('quote_id' => 'entity_id');
        $this->_attributesMap['quote_customer'] = array('customer_id' => 'entity_id');
        $this->_attributesMap['quote_address'] = array('address_id' => 'entity_id');
        $this->_attributesMap['quote_payment'] = array('payment_id' => 'entity_id');
    }

    /**
     * Base preparation of product data
     *
     * @param mixed $data
     * @return null|array
     */

    protected function _prepareProductsData($data)
    {
        return is_array($data) ? $data : null;
    }

    /**
     * Create new quote for shopping cart
     *
     * @param int|string $store
     * @return int
     */

    public function create($store = null)
    {
        $storeId = $this->_getStoreId($store);

        try {
            /*@var $quote Mage_Sales_Model_Quote*/
            $quote = Mage::getModel('sales/quote');
            $quote->setStoreId($storeId)
                ->setIsActive(false)
                ->setIsMultiShipping(false)
                ->save();
        } catch (Mage_Core_Exception $e) {
            $this->_fault('create_quote_fault', $e->getMessage());
        }
        return (int) $quote->getId();
    }

    /**
     * Retrieve full information about quote
     *
     * @param  $quoteId
     * @param  $store
     * @return array
     */

    public function info($quoteId, $store = null)
    {
        $quote = $this->_getQuote($quoteId, $store);

        if ($quote->getGiftMessageId() > 0) {
            $quote->setGiftMessage(
                Mage::getSingleton('giftmessage/message')->load($quote->getGiftMessageId())->getMessage()
            );
        }

        $result = $this->_getAttributes($quote, 'quote');
        $result['shipping_address'] = $this->_getAttributes($quote->getShippingAddress(), 'quote_address');
        $result['billing_address'] = $this->_getAttributes($quote->getBillingAddress(), 'quote_address');
        $result['items'] = array();

        foreach ($quote->getAllItems() as $item) {
            if ($item->getGiftMessageId() > 0) {
                $item->setGiftMessage(
                    Mage::getSingleton('giftmessage/message')->load($item->getGiftMessageId())->getMessage()
                );
            }

            $result['items'][] = $this->_getAttributes($item, 'quote_item');
        }

        $result['payment'] = $this->_getAttributes($quote->getPayment(), 'quote_payment');

        return $result;
    }

    /**
     * @param  $quoteId
     * @param  $productsData
     * @param  $store
     * @return bool
     */

    public function add($quoteId, $productsData, $store = null)
    {
        $quote = $this->_getQuote($quoteId, $store);

        if (empty($store)) {
            $store = $quote->getStoreId();
        }

        $productsData = $this->_prepareProductsData($productsData);

        if (empty($productsData)) {
            return json_encode(array('is_Fault' => 1, 'faultMessage' => 'invalid_product_data'));
        }
        $errors = array();

        foreach ($productsData as $productItem) {
            $id = $productItem['product_id'];
            $qty = $productItem['qty'];
            if (isset($productItem['product_id'])) {
                $productByItem = $this->_getProduct($productItem['product_id'], $store, "id");
            } else if (isset($productItem['sku'])) {
                $productByItem = $this->_getProduct($productItem['sku'], $store, "sku");
            } else {
                $errors[] = Mage::helper('checkout')->__("One item of products do not have identifier or sku");
                continue;
            }
            if ($productByItem->getData('type_id') == 'configurable') {
                $configProd = Mage::getModel('catalog/product')->load($productByItem->getData('entity_id'));
                $super_attrs = array();
                $super_attrs_code = array();
                $configurableAttributeCollection = $configProd->getTypeInstance()->getConfigurableAttributes();
                foreach ($configurableAttributeCollection as $attribute) {
                    $super_attrs[$attribute->getProductAttribute()->getAttributeCode()] = $attribute->getProductAttribute()->getId();
                    $super_attrs_code[] = $attribute->getProductAttribute()->getAttributeCode();
                }
                $res = array_intersect_key($super_attrs, $productItem['options']);
                if (sort(array_keys($res)) != sort(array_keys($productItem['options']))) {
                    return json_encode(array('is_Fault' => 1, 'faultMessage' => 'Please specify correct options.'));
                }

                $super_attribute_values = array();
                foreach ($super_attrs as $supercode => $superid) {
                    $supervalue = $this->setOrAddOptionAttribute('product', $supercode, $productItem['options'][$supercode]);

                    if (!$supervalue) {
                        return json_encode(array('is_Fault' => 1, 'faultMessage' => 'Please specify correct options of product. The option ' . $supercode . ' with value ' . $productItem['options'][$supercode] . ' not exist.'));
                    }
                    $super_attribute_values[$superid] = $supervalue;
                }
                if (count($super_attribute_values) != count($super_attrs)) {
                    return json_encode(array('is_Fault' => 1, 'faultMessage' => 'Please specify correct options.22'));
                }
                $productItem["super_attribute"] = $super_attribute_values;
            } else if ($productByItem->getData('type_id') == 'simple') {
                $product = Mage::getModel('catalog/product')->load($id);
                $attributes = $product->getAttributes();
                foreach ($attributes as $k => $attribute) {
                    $attr[$attribute->getAttributeCode()] = $attribute->getAttributeId();
                }
                $res = array_intersect_key($attr, $productItem['options']);
                $super_attribute_values = array();
                if (is_array($res) && !empty($res)) {
                    foreach ($res as $supercode => $superid) {
                        $supervalue = $this->setOrAddOptionAttribute('product', $supercode, $result['options'][$supercode]);
                        $super_attribute_values[$superid] = $supervalue;
                    }
                    $productItem["super_attribute"] = $super_attribute_values;
                }
            }
            if (!$productsData['customOption']) {
                unset($productsData['options']);
            }
            $productsData1[] = $productItem;
            $productRequest = new Varien_Object($productItem);
            $productRequest->setData($productRequest);
            try {
                $result = $quote->addProduct($productByItem, $productRequest);
                if (is_string($result)) {
                    Mage::throwException($result);
                }
            } catch (Mage_Core_Exception $e) {
                $errors[] = $e->getMessage();
            }
        }

        if (!empty($errors)) {
            return json_encode(array('is_Fault' => 1, 'add_product_fault' => implode(PHP_EOL, $errors)));
        }
        try {
            $quote->collectTotals()->save();
        } catch (Exception $e) {
            return json_encode(array('is_Fault' => 1, 'add_product_quote_save_fault' => $e->getMessage()));
        }

        $result = $this->addItem($quoteId, $productsData1, $store);

        return true;
    }

    /**
     * @param  $quoteId
     * @param  $productsData
     * @param  $store
     * @return bool
     */

    public function addItem($quoteId, $productsData, $store = null)
    {
        $quote = $this->_getQuote($quoteId, $store);
        $errors = array();
        foreach ($productsData as $productItem) {
            if (isset($productItem['product_id'])) {
                $productByItem = $this->_getProduct($productItem['product_id'], $store, "id");
            } else if (isset($productItem['sku'])) {
                $productByItem = $this->_getProduct($productItem['sku'], $store, "sku");
            } else {
                $errors[] = Mage::helper('checkout')->__("One item of products do not have identifier or sku");
                continue;
            }
            /** @var $quoteItem Mage_Sales_Model_Quote_Item */
            $quoteItem = $this->_getQuoteItemByProduct($quote, $productByItem, $this->_getProductRequest($productItem));
            if (is_null($quoteItem->getId())) {
                $errors[] = Mage::helper('checkout')->__("One item of products is not belong any of quote item");
                continue;
            }
            $quoteItem->setCustomDesign($productItem['custom_design']);
            $product = $quoteItem->getProduct();
            $data['microtime'] = microtime(true);
            $product->addCustomOption('do_not_merge', serialize($data));
            $quoteItem->addOption($product->getCustomOption('do_not_merge'));
            $quoteProduct = Mage::getModel('catalog/product')->load($productItem['simpleproduct_id']);
            $tierProduct = Mage::getModel('catalog/product')->load($productItem['product_id']);
            $tierPrice = 0;
            $tierPrices = $tierProduct->getTierPrice();
            if (is_array($tierPrices)) {
                foreach ($tierPrices as $price) {
                    if ($productItem['totalQty'] > 0) {
                        if ($productItem['totalQty'] >= $price['price_qty']) {
                            $tierPrice = $price['price'];
                        }
                    } else {
                        if ($productItem['qty'] >= $price['price_qty']) {
                            $tierPrice = $price['price'];
                        }
                    }
                }
            }
            if ($tierPrice == 0) {
                $c_price = $quoteProduct->getPrice() + $productItem['custom_price'];
            } else {
                $c_price = $tierPrice + $productItem['custom_price'];
            }
            $quoteItem->setCustomPrice($c_price);
            $quoteItem->setOriginalCustomPrice($c_price);
            if ($productItem['qty'] > 0) {
                $quoteItem->setQty($productItem['qty']);
            }
        }
        if (!empty($errors)) {
            return json_encode(array('is_Fault' => 1, 'update_product_fault' => implode(PHP_EOL, $errors)));
        }
        try {
            $quote->collectTotals()->save();
        } catch (Exception $e) {
            return json_encode(array('is_Fault' => 1, 'update_product_quote_save_fault' => $e->getMessage()));
        }
        return true;
    }

    /**
     * Shopping cart api for customer data
     *
     * @category    Mage
     * @package     Mage_Checkout
     * @author      Magento Core Team <core@magentocommerce.com>
     */

    public function saveCustomerInformation($customerId, $websiteId, $quoteId, $store = null)
    {
        $custObj = new Mage_Checkout_Model_Cart_Customer_Api();
        $customerAsLoggedIn = array(
            "entity_id" => $customerId,
            "website_id" => $websiteId,
            "store_id" => $store,
            "mode" => "customer",
        );
        /**
         * Set customer for shopping cart
         *
         * @param int $quoteId
         * @param array|object $customerData
         * @param int | string $store
         * @return int
         */

        $resultCustomerSet = $custObj->set($quoteId, $customerAsLoggedIn);

        /** @var $customer Mage_Customer_Model_Customer */
        $customerData = Mage::getModel('customer/customer')
            ->load($customerId);

        if (!$customer->getId()) {
            return json_encode(array('is_Fault' => 1, 'customer_not_exists' => 'Customer Not exists'));
        }

        $billing_addr_id = false;
        $billing_addr = array();

        if ($billing_addr_id) {
            $billing_addr_id = $customerData->getDefaultBilling();
            $billing_addr = Mage::getModel('customer/address')->load($billing_addr_id)->getData();
        }

        $shipping_addr_id = false;
        $shipping_addr = array();

        if ($shipping_addr_id) {
            $shipping_addr_id = $customerData->getDefaultShipping();
            $shipping_addr = Mage::getModel('customer/address')->load($shipping_addr_id)->getData();
        }

        // Set customer addresses
        $customerAddressData = array(
            array(
                "mode" => "shipping",
                "firstname" => (isset($shipping_addr['firstname']) && $shipping_addr['firstname'] != '') ? $shipping_addr['firstname'] : '',
                "lastname" => (isset($shipping_addr['lastname']) && $shipping_addr['lastname'] != '') ? $shipping_addr['lastname'] : '',
                "company" => (isset($shipping_addr['company'])) ? $shipping_addr['company'] : '',
                "street" => (isset($shipping_addr['street']) && $shipping_addr['street'] != '') ? $shipping_addr['street'] : '',
                "city" => (isset($shipping_addr['city']) && $shipping_addr['city'] != '') ? $shipping_addr['city'] : '',
                "region" => (isset($shipping_addr['region']) && $shipping_addr['region'] != '') ? $shipping_addr['region'] : '',
                "region_id" => (isset($shipping_addr['region_id']) && $shipping_addr['region_id'] != '') ? $shipping_addr['region_id'] : '',
                "postcode" => (isset($shipping_addr['postcode']) && $shipping_addr['postcode'] != '') ? $shipping_addr['postcode'] : '',
                "country_id" => (isset($shipping_addr['country_id']) && $shipping_addr['country_id'] != '') ? $shipping_addr['country_id'] : '',
                "telephone" => (isset($shipping_addr['telephone']) && $shipping_addr['telephone'] != '') ? $shipping_addr['telephone'] : '',
                "fax" => (isset($shipping_addr['fax'])) ? $shipping_addr['fax'] : '',
                "is_default_shipping" => 0,
                "is_default_billing" => 0,
            ),
            array(
                "mode" => "billing",
                "firstname" => (isset($billing_addr['firstname']) && $billing_addr['firstname'] != '') ? $billing_addr['firstname'] : '',
                "lastname" => (isset($billing_addr['lastname']) && $billing_addr['lastname'] != '') ? $billing_addr['lastname'] : '',
                "company" => (isset($billing_addr['company'])) ? $billing_addr['company'] : '',
                "street" => (isset($billing_addr['street']) && $billing_addr['street'] != '') ? $billing_addr['street'] : '',
                "city" => (isset($billing_addr['city']) && $billing_addr['city'] != '') ? $billing_addr['city'] : '',
                "region" => (isset($billing_addr['region']) && $billing_addr['region'] != '') ? $billing_addr['region'] : '',
                "region_id" => (isset($billing_addr['region_id']) && $billing_addr['region_id'] != '') ? $billing_addr['region_id'] : '',
                "postcode" => (isset($billing_addr['postcode']) && $billing_addr['postcode'] != '') ? $billing_addr['postcode'] : '',
                "country_id" => (isset($billing_addr['country_id']) && $billing_addr['country_id'] != '') ? $billing_addr['country_id'] : '',
                "telephone" => (isset($billing_addr['telephone']) && $billing_addr['telephone'] != '') ? $billing_addr['telephone'] : '',
                "fax" => (isset($billing_addr['fax'])) ? $billing_addr['fax'] : '',
                "is_default_shipping" => 0,
                "is_default_billing" => 0,
            ),
        );
        /**
         * @param  int $quoteId
         * @param  array of array|object $customerAddressData
         * @param  int|string $store
         * @return int
         */
        $resultCustomerAddresses = $custObj->setAddresses($quoteId, $customerAddressData, $store);
    }

    /**
     * @param  $quoteId
     * @param  $store
     * @param  $productsData
     * @param  $customerId
     * @return bool
     * @param $action
     */

    public function addToCart($quoteId = false, $store = null, $productsData = array(), $action = 'add', $customerId = null)
    {
        $store = Mage::getModel('core/store')->load($store);
        if (!$store) {
            return json_encode(array('is_Fault' => 1, 'faultMessage' => 'Invalid Store'));
        }
        $websiteId = $store->getWebsiteId();
        if ($customerId && $customerId > 0) {
            $storeId = $this->_getStoreId($store);
            $quoteCollection = Mage::getModel('sales/quote')->getCollection()
                ->addFieldToFilter('customer_id', $customerId)
                ->addFieldToFilter('store_id', $storeId)
                ->addFieldToFilter('is_active', 1)
                ->addOrder('updated_at');
            $quote = $quoteCollection->getFirstItem();
            $quoteId = (int) $quote->getId();
        }
        if (!$quoteId) {
            $quoteId = $this->create($store);
            if (!$quoteId || $quoteId == 0) {
                return json_encode(array('is_Fault' => 1, 'create_quote_fault' => 'Invalid Quote Id'));
            }
        }

        $sessionCustomer = Mage::getSingleton("customer/session");

        if ($sessionCustomer->isLoggedIn() && $sessionCustomer->getId()) {
            $customerid = $sessionCustomer->getId();
            $this->saveCustomerInformation($customerid, $websiteId, $quoteId, $store);
        }

        $result = $this->add($quoteId, $productsData, $store);
        $result = $this->info($quoteId, $store);

        $storeId = $store->getStoreId();
        $url = Mage::app()->getStore($storeId)->getUrl('checkout/cart');

        return json_encode(array('is_Fault' => 0, 'quoteId' => $quoteId, 'checkoutURL' => $url, 'quoteInfo' => $result));
    }

    /**
     * Retrieve attribute option value
     *
     * @param integer $product
     * @param array $arg_attribute
     * @param string $arg_value
     * @return array
     */

    public function setOrAddOptionAttribute($product, $arg_attribute, $arg_value)
    {
        $attribute_model = Mage::getModel('eav/entity_attribute');
        $attribute_options_model = Mage::getModel('eav/entity_attribute_source_table');

        $attribute_code = $attribute_model->getIdByCode('catalog_product', $arg_attribute);
        $attribute = $attribute_model->load($attribute_code);

        $attribute_options_model->setAttribute($attribute);
        $options = $attribute_options_model->getAllOptions(false);

        // determine if this option exists
        $value_exists = false;
        foreach ($options as $option) {
            if ($option['label'] == $arg_value) {
                $value_exists = true;
                return $option['value'];
                break;
            }
        }

        return false;
    }

    /**
     * @param string $lastOrderId
     * @param integer $range
     * @param integer $store
     * @param string $status
     * Retrieve the order the invoice for created for
     * @return Mage_Sales_Model_Order
     */

    public function getOrders($lastOrderId = 0, $store = null, $range = 0, $status = 'pending', $start = 0)
    {

        $orders = Mage::getModel('sales/order')->getCollection()
            ->join(array('item' => 'sales/order_item'), 'main_table.entity_id = item.order_id AND item.custom_design>0 AND main_table.store_id=' . $store . ' ');
        $orders->getSelect()->group('main_table.entity_id');
        $orders->getSelect()->order('main_table.created_at DESC');

        if ((int) $lastOrderId) {
            $orders->addAttributeToFilter('entity_id', array('lt' => $lastOrderId));
        } else {
            $orders->addAttributeToFilter('entity_id', array('gt' => $lastOrderId));
        }

        $start = intval($start);
        $range = intval($range);
        $orders->getSelect()->limit($range, $start);

        $order_array = array();
        $i = 0;
        if ($range) {
            foreach ($orders as $order) {
                $i = ($i < 0) ? 0 : $i;
                $order_array[$i] = array(
                    'order_id' => $order->getId(),
                    'order_incremental_id' => $order->getIncrementId(),
                    'order_status' => $order->getStatusLabel(),
                    'order_date' => $order->getCreatedAtStoreDate()->toString(Varien_Date::DATETIME_INTERNAL_FORMAT),
                    'customer_name' => $order->getCustomerName(),
                );
                $i++;
                $tempId = $order->getId();
                $range--;
            }
        } else {
            foreach ($orders as $order) {
                $i = ($i < 0) ? 0 : $i;
                $order_array[$i] = array(
                    'order_id' => $order->getId(),
                    'order_incremental_id' => $order->getIncrementId(),
                    'order_status' => $order->getStatusLabel(),
                    'order_date' => $order->getCreatedAtStoreDate()->toString(Varien_Date::DATETIME_INTERNAL_FORMAT),
                    'customer_name' => $order->getCustomerName(),
                );
                $i++;
                $tempId = $order->getId();
            }
        }
        return json_encode(array('is_Fault' => 0, 'order_list' => $order_array));
    }

    /**
     * @param date $from
     * @param date $to
     * @param integer $store
     * @param date $lastweek
     * Retrieve the how much order placed last one month
     * @return Mage_Sales_Model_Order
     */

    public function getOrdersGraph($from = '', $to = '', $store = null)
    {
        $lastweek = date('Y-m-d', strtotime("-1 month"));

        $orders = Mage::getModel('sales/order')->getCollection()
            ->join(array('item' => 'sales/order_item'), 'main_table.entity_id = item.order_id AND item.custom_design>0 ')
            ->addAttributeToFilter('main_table.created_at', array('from' => $lastweek))
            ->addFieldToFilter('main_table.store_id', $store);
        $orders->getSelect()->group('main_table.entity_id');

        /* previous code without custom code

        $orders = Mage::getModel('sales/order')->getCollection()
        ->addAttributeToFilter('created_at', array('from'  => $lastweek))
        ->addFieldToFilter('store_id',$store);
        ->addAttributeToFilter('status', array('neq' => Mage_Sales_Model_Order::STATE_COMPLETE));*/

        $i = 0;
        foreach ($orders as $order) {
            $order_data[$i] = array(
                'created_date' => date('Y-m-d', strtotime($order->getCreatedAtStoreDate()->toString(Varien_Date::DATETIME_INTERNAL_FORMAT))),
                'order_id' => $order->getIncrementId(),
            );
            $i++;
        }

        $res = array();
        $tempId = 0;
        $count = -1;
        foreach ($order_data as $k => $v) {
            if ($tempId != $v['created_date']) {
                $i = 0;
                $count++;
                $tempId = $v['created_date'];
                $res[$count]['date'] = $v['created_date'];
                $res[$count]['sales'] = $i + 1;
            } else {
                $i++;
                $res[$count]['date'] = $v['created_date'];
                $res[$count]['sales'] = $i + 1;
            }
        }
        return json_encode($res);
    }

    /**
     * Retrieve full order information
     *
     * @param string $orderId
     * @return array
     */

    public function getOrderDetails($orderId = 0, $store = null, $color, $size)
    {

        $order = Mage::getModel('sales/order')->load($orderId);

        /**
         * Get invoice items collection
         *
         * @return Mage_Sales_Model_Mysql4_Order_Invoice_Item_Collection
         */

        $orderItems = $order->getItemsCollection();

        /**
         * Retrieve billing address
         *
         * @return Mage_Sales_Model_Order_Address
         */

        $shippingAddress = $order->getShippingAddress();
        $shippingStreet = $shippingAddress->getStreet();
        $orderDetails['shipping_address']['first_name'] = $shippingAddress->getFirstname();
        $orderDetails['shipping_address']['last_name'] = $shippingAddress->getLastname();
        $orderDetails['shipping_address']['fax'] = $shippingAddress->getFax();
        $orderDetails['shipping_address']['region'] = $shippingAddress->getRegion();
        $orderDetails['shipping_address']['postcode'] = $shippingAddress->getPostcode();
        $orderDetails['shipping_address']['telephone'] = $shippingAddress->getTelephone();
        $orderDetails['shipping_address']['city'] = $shippingAddress->getCity();
        $orderDetails['shipping_address']['address_1'] = "";
        $orderDetails['shipping_address']['address_2'] = "";
        if (isset($shippingStreet[0])) {
            $orderDetails['shipping_address']['address_1'] = $shippingStreet[0];
        }

        if (isset($shippingStreet[1])) {
            $orderDetails['shipping_address']['address_2'] = $shippingStreet[1];
        }

        $orderDetails['shipping_address']['state'] = $shippingAddress->getRegion();
        $orderDetails['shipping_address']['company'] = $shippingAddress->getCompany();
        $orderDetails['shipping_address']['email'] = $shippingAddress->getEmail();
        $orderDetails['shipping_address']['country'] = $shippingAddress->getCountry();

        /**
         * Retrieve billing address
         *
         * @return Mage_Sales_Model_Order_Address
         */

        $billingAddress = $order->getBillingAddress();
        $billingStreet = $billingAddress->getStreet();
        $orderDetails['billing_address']['first_name'] = $billingAddress->getFirstname();
        $orderDetails['billing_address']['last_name'] = $billingAddress->getLastname();
        $orderDetails['billing_address']['fax'] = $billingAddress->getFax();
        $orderDetails['billing_address']['region'] = $billingAddress->getRegion();
        $orderDetails['billing_address']['postcode'] = $billingAddress->getPostcode();
        $orderDetails['billing_address']['telephone'] = $billingAddress->getTelephone();
        $orderDetails['billing_address']['state'] = $billingAddress->getRegion();
        $orderDetails['billing_address']['city'] = $billingAddress->getCity();
        $orderDetails['billing_address']['address_1'] = "";
        $orderDetails['billing_address']['address_2'] = "";
        if (isset($billingStreet[0])) {
            $orderDetails['billing_address']['address_1'] = $billingStreet[0];
        }

        if (isset($billingStreet[1])) {
            $orderDetails['billing_address']['address_2'] = $billingStreet[1];
        }

        $orderDetails['billing_address']['company'] = $billingAddress->getCompany();
        $orderDetails['billing_address']['email'] = $billingAddress->getEmail();
        $orderDetails['billing_address']['telephone'] = $billingAddress->getTelephone();
        $orderDetails['billing_address']['country'] = $billingAddress->getCountry();

        $orderDetails['order_id'] = $order->getId();
        $orderDetails['order_incremental_id'] = $order->getIncrementId();
        $orderDetails['order_status'] = $order->getStatusLabel();
        $orderDetails['order_date'] = $order->getCreatedAtStoreDate()->toString(Varien_Date::DATETIME_INTERNAL_FORMAT);
        $orderDetails['customer_name'] = $order->getCustomerName();
        $orderDetails['customer_email'] = $order->getCustomerEmail();
        $orderDetails['shipping_method'] = $order->getShippingMethod();

        $orderDetails['order_items'] = array();
        $index = 0;
        $simpindex = 0;
        foreach ($orderItems as $item) {
            $product = Mage::getModel('catalog/product')->load($item->product_id);
            $attributes = $product->getAttributes();
            $customOptions = $product->getOptions();
            if ($item->getParentItemId()) {
                //configure
                $orderDetails['order_items'][$simpindex]['product_id'] = $item->getProductId();
                //$orderDetails['order_items'][$simpindex]['item_id'] = $item->getId();
                $orderDetails['order_items'][$simpindex]['product_sku'] = $item->getSku();
                $orderDetails['order_items'][$simpindex]['product_name'] = $item->getName();
                $orderDetails['order_items'][$simpindex]['quantity'] = $item->getQtyOrdered();
                $attindex = 0;
                foreach ($attributes as $attribute) {
                    $attributeCode = $attribute->getAttributeCode();
                    $xesize = $size;
                    $xecolor = $color;
                    if ($attributeCode == $xesize) {
                        $value = $attribute->getFrontend()->getValue($product);
                        $orderDetails['order_items'][$simpindex]['xe_size'] = $value;
                    } else if ($attributeCode == $xecolor) {
                        $value = $attribute->getFrontend()->getValue($product);
                        $orderDetails['order_items'][$simpindex]['xe_color'] = $value;
                    }
                    if ($attribute->getIsVisibleOnFront()) {
                        $orderDetail[$attindex]['attributeCode'] = $attributeCode;
                        $orderDetail[$attindex]['label'] = $attribute->getFrontend()->getLabel();
                        $orderDetail[$attindex]['value'] = $attribute->getFrontend()->getValue($product);
                        $attindex++;
                    }
                }
                $orderDetails['order_items'][$simpindex]['attribute'] = $orderDetail;
                $simpindex++;
            } else if ($item->getproduct_type() == 'simple') {
                $orderDetails['order_items'][$index]['itemStatus'] = $item->getStatus();
                $orderDetails['order_items'][$index]['ref_id'] = $item->getCustom_design();
                $orderDetails['order_items'][$index]['item_id'] = $item->getId();
                $orderDetails['order_items'][$index]['type_id'] = $item->getproduct_type();
                $orderDetails['order_items'][$index]['print_status'] = $item->getItem_printed();
                $orderDetails['order_items'][$index]['product_price'] = $item->getPrice();
                $orderDetails['order_items'][$index]['config_product_id'] = $item->getProductId();
                $orderDetails['order_items'][$index]['product_id'] = $item->getProductId();
                $orderDetails['order_items'][$index]['product_sku'] = $item->getSku();
                $orderDetails['order_items'][$index]['product_name'] = $item->getName();
                $orderDetails['order_items'][$index]['quantity'] = $item->getQtyOrdered();
                $orderDetail = array();
                $attindex = 0;
                foreach ($attributes as $attribute) {
                    $attributeCode = $attribute->getAttributeCode();
                    if ($attributeCode == $size) {
                        $value = $attribute->getFrontend()->getValue($product);
                        $orderDetails['order_items'][$index]['xe_size'] = $value;
                    } else if ($attributeCode == $color) {
                        $value = $attribute->getFrontend()->getValue($product);
                        $orderDetails['order_items'][$index]['xe_color'] = $value;
                    }
                    if ($attribute->getIsVisibleOnFront()) {
                        $orderDetail[$attindex]['attributeCode'] = $attributeCode;
                        $orderDetail[$attindex]['label'] = $attribute->getFrontend()->getLabel();
                        $orderDetail[$attindex]['value'] = $attribute->getFrontend()->getValue($product);
                        $attindex++;
                    }
                }
                if ($customOptions) {
                    foreach ($customOptions as $customOption) {
                        $values = $customOption->getValues();
                        $orderDetail[$attindex]['attributeCode'] = $customOption['title'];
                        $orderDetail[$attindex]['label'] = $customOption['title'];
                        $attindex++;
                    }
                }
                $orderDetails['order_items'][$index]['attribute'] = $orderDetail;
                $index++;
                $simpindex++;
            } else {
                $orderDetails['order_items'][$index]['itemStatus'] = $item->getStatus();
                $orderDetails['order_items'][$index]['ref_id'] = $item->getCustom_design();
                $orderDetails['order_items'][$simpindex]['item_id'] = $item->getId();
                $orderDetails['order_items'][$index]['print_status'] = $item->getItem_printed();
                $orderDetails['order_items'][$index]['product_price'] = $item->getPrice();
                $orderDetails['order_items'][$simpindex]['config_product_id'] = $item->getProductId();
                $index++;
            }
        }
        return json_encode(array('is_Fault' => 0, 'order_details' => $orderDetails));
    }

    /**
     * @param string $lastOrderId
     * @param integer $range
     * @param integer $store
     * Retrieve the order the invoice for created for
     * @return Mage_Sales_Model_Order getOrders($lastOrderId=0,$store=1,$range=0,$status='pending',$start=0) {
     */

    public function orderIdFromStore($lastOrderId = 0, $range = 0, $store = null)
    {

        $orders = Mage::getModel('sales/order')->getCollection()
            ->join(array('item' => 'sales/order_item'), 'main_table.entity_id = item.order_id AND item.custom_design>0 AND main_table.store_id=' . $store . ' ');
        $orders->getSelect()->group('main_table.entity_id');
        $orders->getSelect()->order('main_table.created_at ASC');
        $orders->addAttributeToFilter('entity_id', array('gt' => $lastOrderId));
        $range = intval($range);
        $orders->getSelect()->limit($range);
        $order_array = array();
        $i = 0;

        if ($range) {
            foreach ($orders as $order) {
                $i = ($i < 0) ? 0 : $i;
                $order_array[$i] = array(
                    'order_id' => $order->getId(),
                    'order_incremental_id' => $order->getIncrementId(),
                );
                $i++;
                $range--;
            }
        } else {
            foreach ($orders as $order) {
                $i = ($i < 0) ? 0 : $i;
                $order_array[$i] = array(
                    'order_id' => $order->getId(),
                    'order_incremental_id' => $order->getIncrementId(),
                );
                $i++;
            }
        }
        return json_encode(array('is_Fault' => 0, 'order_list' => $order_array));
    }
    /**
     * @param integer $quoteId
     * @param integer $store
     * @param integer $customerId
     * @return No of cart qty.
     */

    public function getTotalCartItem($quoteId = 0, $store = null, $customerId = 0)
    {
        if ($customerId && $customerId > 0) {
            $quoteCollection = Mage::getModel('sales/quote')->getCollection()
                ->addFieldToFilter('customer_id', $customerId)
                ->addFieldToFilter('store_id', $store)
                ->addFieldToFilter('is_active', 1)
                ->addOrder('updated_at');
            $quote = $quoteCollection->getFirstItem();
            $quoteId = (int) $quote->getId();
        }
        if ($quoteId > 0) {
            $quote = $this->_getQuote($quoteId, $store);
            $itemQty = (int) $quote->getItemsQty();
        } else {
            $itemQty = 0;
        }
        $url = Mage::app()->getStore($store)->getUrl('checkout/cart');
        $url = ($quoteId > 0) ? $url . '?quoteId=' . $quoteId : $url;
        return json_encode(array('is_Fault' => 0, 'totalCartItem' => $itemQty, 'checkoutURL' => $url), JSON_UNESCAPED_SLASHES);
    }
}
