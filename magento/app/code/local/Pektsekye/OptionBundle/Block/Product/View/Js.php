<?php
class Pektsekye_OptionBundle_Block_Product_View_Js extends  Mage_Catalog_Block_Product_View_Options
{

    protected $_options;
    protected $_boptions;
    protected $_inPreconfigured;
    protected $thumbnailDirUrl = '';
    protected $pickerImageDirUrl = '';


    public function getBoptions()
    {
      if (!$this->_boptions) {
        $product = $this->getProduct();
        $typeInstance = $product->getTypeInstance(true);
        $typeInstance->setStoreFilter($product->getStoreId(), $product);

        $optionCollection = $typeInstance->getOptionsCollection($product);

        $selectionCollection = $typeInstance->getSelectionsCollection(
            $typeInstance->getOptionsIds($product),
            $product
        );

        $this->_boptions = $optionCollection->appendSelections($selectionCollection, true, false);
      }

      return $this->_boptions;
    }



    public function getDataJson()
    {
      $data = array(
        'sortedOIds'         => array(),
        'isRequired'         => array('b' => array(), 'o' => array()),
        'checkedIds'         => array('b' => array(), 'o' => array()),
        'notDepVIdsByOId'    => array('b' => array(), 'o' => array()),
        'optionIds'          => array('b' => array(), 'o' => array()),
        'vIdsByOId'          => array('b' => array(), 'o' => array())
      );

      $rData = Mage::getModel('optionbundle/relation')->getRelationData($this->getProduct()); //includes ids of Out of Stock options

      $position = 1;
      foreach($this->getOptions() as $option){
        $t = $option['type'];
        $id = $option['id'];

        if ($t == 'b'){
          $position++;

          $data['isSingle'][$id] = count($option['values']) == 1 && $option['required'];
        }

        if ($option['required'] == 1)
          $data['isRequired'][$t][$id] = 1;

        if (isset($option['checkedIds']))
          $data['checkedIds'][$t][$id] = $option['checkedIds'];

        $data['layout'][$t][$id] = $option['layout'];
        $data['note'][$t][$id]   = $option['note'];
        $data['popup'][$t][$id]  = (bool) $option['popup'] == 1;

        $data['vIdsByOId'][$t][$id] = array();

        $firstValueId = null;
        foreach($option['values'] as $value){

          $this->prepareImages($value['image']);

          $valueId = $value['id'];
          $hasParent = isset($rData['pVIdsByVId'][$t][$valueId]) || isset($rData['pVIdsByOId'][$t][$id]);
          $hasChildren = isset($rData['cVIdsByVId'][$t][$valueId]) || isset($rData['cOIdsByVId'][$t][$valueId]);

          if (!$hasParent)
            $data['notDepVIdsByOId'][$t][$id][] = $valueId;

          $data['image'][$t][$valueId] = $value['image'];
          $data['description'][$t][$valueId] = $value['description'];
          
          if ($t == 'b' && !empty($value['basePriceLabel'])){
            $data['basePriceLabel'][$valueId] = $value['basePriceLabel'];
          }
          
          if ($firstValueId == null)
            $firstValueId = $valueId;

          $data['optionIds'][$t][] = $id;
          $data['vIdsByOId'][$t][$id][] = $valueId;
        }

        $data['sortedOIds'][] = array('type' => $t, 'id' => $id, 'fvId' => $firstValueId, 'optionType' => $option['option_type']);
      }

      $keys = array('oIdByVId','cOIdsByVId','cOIdsByOId','cVIdsByVId');
      foreach($keys as $key)
        $data[$key] = $rData[$key];

      return Zend_Json::encode($data);
    }



    public function getOptions()
    {
      if (!isset($this->_options)){
        $options = array();

        $filter = Mage::getModel('core/email_template_filter');

        $hasOrder = false;

        if ($this->getProduct()->getTypeId() == Mage_Catalog_Model_Product_Type::TYPE_BUNDLE){
          $position = 1;
          $obOptions = Mage::getModel('optionbundle/boption')->getBoptions($this->getProduct()->getId(), (int)$this->getProduct()->getStoreId());
          foreach ($this->getBoptions() as $_option) {

            if (!$_option->getSelections()) {
                continue;
            }

            $id = (int) $_option->getId();

            $optionType = $_option->getType();
            if ($optionType == 'multi'){
              $optionType = 'multiple';
            } elseif ($optionType == 'select'){
              $optionType = 'drop_down';
            }


            $note = isset($obOptions[$id]['note']) ? $obOptions[$id]['note'] : '';
            $layout = isset($obOptions[$id]['layout']) ? $obOptions[$id]['layout'] : '';
            $popup = isset($obOptions[$id]['popup']) ? (int) $obOptions[$id]['popup'] : 0;
            $useProductImage = isset($obOptions[$id]['use_product_image']) ? $obOptions[$id]['use_product_image'] : 1;
            $useProductDescription = isset($obOptions[$id]['use_product_description']) ? $obOptions[$id]['use_product_description'] : 1;

            $option = array(
              'type' => 'b',
              'id' => $id,
              'option_type' => $optionType,
              'sort_order' => $_option->getPosition(),
              'required' => $_option->getRequired() || $_option->getObRequired(),
              'hasDefault' => 1,
              'note' => $filter->filter($note),
              'layout' => $this->checkLayout($optionType, $layout),
              'popup' => $popup,
              'original_position' => $position,
              'values' => array(),
              'default' => array()
            );


            $obValues = Mage::getModel('optionbundle/bvalue')->getValues($this->getProduct()->getId(), (int)$this->getProduct()->getStoreId());
            foreach ($_option->getSelections() as $selection){
              $valueId = (int) $selection->getSelectionId();

              $image = isset($obValues[$valueId]['image']) ? $obValues[$valueId]['image'] : '';
              $description = isset($obValues[$valueId]['description']) ? $obValues[$valueId]['description'] : '';
                   
              $product = Mage::getModel('catalog/product')->load($selection->getProductId());
              
              if ($useProductImage == 1)
                $image = $product->getImage() != 'no_selection' ? (string) $product->getImage() : '';
                
              if ($useProductDescription == 1)
                $description = (string) $product->getShortDescription();         

              $option['values'][] = array(
              'id' => $valueId,
              'image' => $image,
              'description' => $filter->filter($description),
              'basePriceLabel' => class_exists('DerModPro_BasePrice_Helper_Data') ? Mage::helper('baseprice')->getBasePriceLabel($product) : ''              
              );

              if ($selection->getIsDefault())
                $option['default'][] =  $valueId;
            }


            if ($this->_getInPreconfigured()){
              $configValue = $this->getProduct()->getPreconfiguredValues()->getData('bundle_option/' . $id);
              if (!is_null($configValue)){
                if (is_array($configValue)){
                  foreach($configValue as $valueId)
                    $option['checkedIds'][] = (int) $valueId;
                } else {
                  $option['checkedIds'][] = (int) $configValue;
                }
              }
            } elseif (count($option['default']) > 0) {
              $option['checkedIds'] = $option['default'];
            }


           $options[] = $option;
           $hasOrder |= $_option->getPosition() > 0;
           $position++;
          }
        }

        $obOptions = Mage::getModel('optionbundle/option')->getOptions($this->getProduct()->getId(), (int)$this->getProduct()->getStoreId());
        foreach ($this->getProduct()->getOptions() as $_option) {
            $id = (int) $_option->getOptionId();

            $note = isset($obOptions[$id]['note']) ? $obOptions[$id]['note'] : '';
            $layout = isset($obOptions[$id]['layout']) ? $obOptions[$id]['layout'] : '';
            $popup = isset($obOptions[$id]['popup']) ? (int) $obOptions[$id]['popup'] : 0;

            $option = array(
                'type' => 'o',
                'id' => $id,
                'option_type' => $_option->getType(),
                'sort_order' => $_option->getSortOrder(),
                'required' => $_option->getIsRequire(),
                'note' => $filter->filter($note),
                'layout' => $this->checkLayout($_option->getType(), $layout),
                'popup' => $popup,
                'values' => array()
            );

            if ($this->_getInPreconfigured()){
              $configValue = $this->getProduct()->getPreconfiguredValues()->getData('options/' . $id);
              if (!is_null($configValue)){
                if (is_array($configValue)){
                  foreach($configValue as $valueId)
                    $option['checkedIds'][] = (int) $valueId;
                } else {
                  $option['checkedIds'][] = (int) $configValue;
                }
              }
            } elseif (isset($obOptions[$id]['default']) && $obOptions[$id]['default'] != ''){
              foreach(explode(',', $obOptions[$id]['default']) as $valueId)
                $option['checkedIds'][] = (int) $valueId;
            }

            $obValues = Mage::getModel('optionbundle/value')->getValues($this->getProduct()->getId(), (int)$this->getProduct()->getStoreId());
            foreach ($_option->getValues() as $value){
              $valueId = (int) $value->getOptionTypeId();

              $image = isset($obValues[$valueId]['image']) ? $obValues[$valueId]['image'] : '';
              $description = isset($obValues[$valueId]['description']) ? $obValues[$valueId]['description'] : '';

              $option['values'][] = array(
              'id' => $valueId,
              'image' => $image,
              'description' => $filter->filter($description)
              );
            }
            $options[] = $option;
         }

         if ($hasOrder){
            usort($options, array(Mage::helper('optionbundle'), "sortOptions"));
         }
          $this->_options = $options;
       }

       return $this->_options;
    }



    public function _getInPreconfigured()
    {
      if (!isset($this->_inPreconfigured)){

        if (!$this->getProduct()->hasPreconfiguredValues())
          return $this->_inPreconfigured = false;

        if ($this->getProduct()->getTypeId() == Mage_Catalog_Model_Product_Type::TYPE_BUNDLE){
          foreach ($this->getBoptions() as $option) {
            $configValue = $this->getProduct()->getPreconfiguredValues()->getData('bundle_option/' . $option->getId());
            if (!is_null($configValue))
              return $this->_inPreconfigured = true;
          }
        }

        foreach ($this->getProduct()->getOptions() as $option) {
          $configValue = $this->getProduct()->getPreconfiguredValues()->getData('options/' . $option->getOptionId());
          if (!is_null($configValue))
            return $this->_inPreconfigured = true;
        }

        $this->_inPreconfigured = false;
			}

			return $this->_inPreconfigured;
	 	}


    public function getInPreconfigured()
    {
			return $this->_getInPreconfigured() ? 'true' : 'false';
	 	}

    public function isConfigurable()
    {
      return $this->getProduct()->isConfigurable() ? 'true' : 'false';
    }



    public function prepareImages($image)
    {
      if (!empty($image)){
        $thumbnailUrl = $this->makeThumbnail($image);
        $pickerImageUrl = $this->makePickerImage($image);
        if ($this->thumbnailDirUrl == ''){
          $this->thumbnailDirUrl = str_replace($image, '', $thumbnailUrl);
          $this->pickerImageDirUrl = str_replace($image, '', $pickerImageUrl);
        }
      }
	  }


    public function makeThumbnail($image)
    {
		  $thumbnailUrl = $this->helper('catalog/image')
        ->init($this->getProduct(), 'thumbnail', $image)
        ->keepFrame(true)
  // Uncomment the following line to set Thumbnail RGB Background Color:
  //			->backgroundColor(array(246,246,246))

  // Set Thumbnail Size:
        ->resize(64,43)
        ->__toString();

		  return $thumbnailUrl;
	  }


    public function makePickerImage($image)
    {
			$pickerImageUrl = $this->helper('catalog/image')
				->init($this->getProduct(), 'thumbnail', $image)
				->keepFrame(false)
				->resize(30,30)
				->__toString();
			return $pickerImageUrl;
		}

    public function getThumbnailDirUrl()
    {
			return $this->thumbnailDirUrl;
	 	}


    public function getPickerImageDirUrl()
    {
			return $this->pickerImageDirUrl;
	 	}

    public function getPlaceholderUrl()
    {
			return Mage::getDesign()->getSkinUrl($this->helper('catalog/image')->init($this->getProduct(), 'small_image')->getPlaceholder());
	 	}


    public function getProductBaseMediaUrl()
    {
			return Mage::getSingleton('catalog/product_media_config')->getBaseMediaUrl();
	 	}



    public function checkLayout($optionType, $layout)
    {
      $layouts = array(
        Mage_Catalog_Model_Product_Option::OPTION_TYPE_RADIO => array(
            'above' =>1,
            'before'=>1,
            'below' =>1,
            'swap'  =>1,
            'grid'  =>1,
            'list'  =>1
          ),
        Mage_Catalog_Model_Product_Option::OPTION_TYPE_CHECKBOX => array(
            'above'=>1,
            'below'=>1,
            'grid' =>1,
            'list' =>1
          ),
        Mage_Catalog_Model_Product_Option::OPTION_TYPE_DROP_DOWN => array(
            'above'     =>1,
            'before'    =>1,
            'below'     =>1,
            'swap'      =>1,
            'picker'    =>1,
            'pickerswap'=>1
          ),
        Mage_Catalog_Model_Product_Option::OPTION_TYPE_MULTIPLE => array(
            'above'=>1,
            'below'=>1
          )
      );

      return isset($layouts[$optionType][$layout]) ? $layout : 'above';
    }



    protected function _toHtml()
    {
      if ($this->getProduct()->isSaleable() && count($this->getOptions()) > 0) {
        return parent::_toHtml();
      }
      return '';
    }
}
