<?php

class Pektsekye_OptionBundle_Model_Resource_Relation extends Mage_Core_Model_Resource_Db_Abstract
{

    protected $_op_tables = array(
        'selection_to_boption' => array('optionbundle/selection_to_boption', 'selection_id', 'children_boption_id'),    
        'selection_to_selection'   => array('optionbundle/selection_to_selection', 'selection_id', 'children_selection_id'),
        'selection_to_option'    => array('optionbundle/selection_to_option', 'selection_id', 'children_option_id'),      
        'selection_to_value'     => array('optionbundle/selection_to_value', 'selection_id', 'children_value_id'), 
        'value_to_boption'   => array('optionbundle/value_to_boption', 'value_id', 'children_boption_id'),  
        'value_to_selection'     => array('optionbundle/value_to_selection', 'value_id', 'children_selection_id'),
        'value_to_option'      => array('optionbundle/value_to_option', 'value_id', 'children_option_id'),      
        'value_to_value'       => array('optionbundle/value_to_value', 'value_id', 'children_value_id')
        );


    public function _construct()
    {
      $this->_init('optionbundle/selection_to_selection', 'selection_id');
    }
    
    
    public function getRelations($productId)
    { 
      $data = array();
      foreach($this->_op_tables as $key => $t)
        $data[$key] = $this->_getRelation($productId, $t[0], $t[1], $t[2]);
      return $data;
    }   


    public function _getRelation($productId, $tableKey, $field, $childrenField)
    {
      $select = $this->_getReadAdapter()->select()
        ->from($this->getTable($tableKey), array('id'=>$field, 'cid'=>$childrenField))
        ->where("product_id=?", $productId);
      return $this->_getReadAdapter()->fetchAll($select);      
    }     
        
        
    public function saveRelationsData($productId, $data)
    {    
      foreach($this->_op_tables as $key => $t){
        if (isset($data[$key]))
          $this->_save($productId, $data[$key], $t[0], $t[1], $t[2]);
      }      
    } 


    public function _save($productId, $relation, $tableKey, $field, $childrenField)
    { 
      $table = $this->getTable($tableKey);
              
      $this->_getWriteAdapter()->delete($table, array("product_id=?" => $productId));

      $data = array();          
      foreach($relation as $value => $children){         
        foreach($children as $id)
          $data[] = array('product_id' => $productId, $field => $value, $childrenField => $id);       
      }
      
      if (count($data) > 0)
        $this->_getWriteAdapter()->insertMultiple($table, $data);                                         
    }
      


    public function getUsedPoductSkus()
    {
      $selects = array();
      
      $tables = array('optionbundle/boption','optionbundle/option');
      foreach($this->_op_tables as $t){
        $tables[] = $t[0];
      }
      
      foreach($tables as $table){
        $selects[] = $this->_getReadAdapter()->select()
        ->from(array('rel' => $this->getTable($table)), 'product_id')
        ->join(array('cat' => $this->getTable('catalog/product')), 'cat.entity_id = rel.product_id', 'sku')
        ->distinct(true);
      }
    
      $select = $this->_getReadAdapter()->select()
        ->union($selects, Zend_Db_Select::SQL_UNION); 
               
      return $this->_getReadAdapter()->fetchPairs($select);   
    }

}
