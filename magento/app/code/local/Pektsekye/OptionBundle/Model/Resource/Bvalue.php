<?php

class Pektsekye_OptionBundle_Model_Resource_Bvalue extends Mage_Core_Model_Resource_Db_Abstract
{
    public function _construct()
    {
        $this->_init('optionbundle/bvalue', 'bvalue_id');
    }
  
  
    public function getValues($productId, $storeId)
    {        
      $select = $this->_getReadAdapter()->select()
        ->from(array('main_table' => $this->getMainTable()), array('bvalue_id','image'))
        ->join(array('default_bvalue_description'=>$this->getTable('optionbundle/bvalue_description')),
            '`default_bvalue_description`.bvalue_id=`main_table`.bvalue_id AND `default_bvalue_description`.store_id=0',
            array('default_description'=>'description'))
        ->joinLeft(array('store_bvalue_description'=>$this->getTable('optionbundle/bvalue_description')),
            '`store_bvalue_description`.bvalue_id=`main_table`.bvalue_id AND '.$this->_getWriteAdapter()->quoteInto('`store_bvalue_description`.store_id=?', $storeId),
            array('store_description'=>'description',
            'description'=>new Zend_Db_Expr('IFNULL(`store_bvalue_description`.description,`default_bvalue_description`.description)')))      
        ->where("product_id=?", $productId);  
               
      return $this->_getReadAdapter()->fetchAssoc($select);                                 
    }
 
    
    public function getStoreDescriptions($bvalueId)
    {        
      $select = $this->_getReadAdapter()->select()
        ->from(array('cs' => $this->getTable('core/store')), 'code')        
        ->join(array('obd' => $this->getTable('optionbundle/bvalue_description')), 'obd.store_id = cs.store_id', 'description') 
        ->where('bvalue_id=?', $bvalueId); 
               
      return $this->_getReadAdapter()->fetchPairs($select);                           
    }
        
      
    public function saveValues($productId, $storeId, $values)
    {         
      $storeId = (int) $storeId;
      $read = $this->_getReadAdapter();
      $write = $this->_getWriteAdapter();

      if (count($values) == 0)
        return;
        
      foreach($values as $bvalueId => $bvalue){
        $bvalueId = (int) $bvalueId;
        $image = $bvalue['image'];
        
        if (isset($bvalue['image_json'])){
          $newImage = '';
          $imageInfo = array();          
          if ($bvalue['image_json'] != '') {			
            $imageInfo = Zend_Json::decode($bvalue['image_json']);
            if (isset($imageInfo['file'])){		
              $newImage = $this->_moveImageFromTmp($imageInfo['file']);
            }	
			    }
          if (isset($imageInfo['file']) || !isset($imageInfo['url']))	
            $image = $newImage;
        }	
        			                                       
        $data = array(
          'product_id'=> $productId, 
          'bvalue_id' => $bvalueId,      
          'image'     => $image        
        );         
            
        $statement = $read->select()
          ->from($this->getMainTable())
          ->where('bvalue_id=?', $bvalueId);

        if ($read->fetchRow($statement)) {
            $write->update(
              $this->getMainTable(),
              $data,
              $write->quoteInto('bvalue_id=?', $bvalueId)
            );
        } else {
          $write->insert($this->getMainTable(), $data);
        }
        
        if (isset($bvalue['descriptions'])){ // csv import
          foreach($bvalue['descriptions'] as $sId => $description){          
            $this->saveDescription($description, $bvalueId, $sId, false);
          }
        } else {        
          $description = isset($bvalue['description']) ? $bvalue['description'] : '';           
          $scope = isset($bvalue['scope']);
          $this->saveDescription($description, $bvalueId, $storeId, $scope);  
        }          
      }

                           
    }
 
 
 	  protected function saveDescription($description, $bvalueId, $storeId, $scope)
    {
		    $read = $this->_getReadAdapter();
		    $write = $this->_getWriteAdapter();
		    $descriptionTable = $this->getTable('optionbundle/bvalue_description');
		    		
        if (!$scope) {		
		      $statement = $read->select()
			      ->from($descriptionTable, array('description'))
			      ->where('bvalue_id = '.$bvalueId.' AND store_id = ?', 0);
          $default = $read->fetchRow($statement);
		      if (!empty($default)) {
			      if ($storeId == '0' || $default['description'] == '') {
				      $write->update(
					      $descriptionTable,
						      array('description' => $description),
						      $write->quoteInto('bvalue_id='.$bvalueId.' AND store_id=?', 0)
				      );
			      }
		      } else {
			      $write->insert(
				      $descriptionTable,
					      array(
						      'bvalue_id' => $bvalueId,
						      'store_id' => 0,
						      'description' => $description
			      ));
		      }
        }
        
		    if ($storeId != '0' && !$scope) {
			    $statement = $read->select()
				    ->from($descriptionTable)
				    ->where('bvalue_id = '.$bvalueId.' AND store_id = ?', $storeId);

			    if ($read->fetchRow($statement)) {
				    $write->update(
					    $descriptionTable,
						    array('description' => $description),
						    $write->quoteInto('bvalue_id='.$bvalueId.' AND store_id=?', $storeId));
			    } else {
				    $write->insert(
					    $descriptionTable,
						    array(
							    'bvalue_id' => $bvalueId,
							    'store_id' => $storeId,
							    'description' => $description
				    ));
			    }
		    } elseif ($scope){
            $write->delete(
                $descriptionTable,
                $write->quoteInto('bvalue_id = '.$bvalueId.' AND store_id = ?', $storeId)
            );		    
		    }
	}   
    /**
     * Move image from temporary directory to normal
     *
     * @param string $file
     * @return string
     */
    protected function _moveImageFromTmp($file)
    {

        $ioObject = new Varien_Io_File();
        $destDirectory = dirname($this->_getMadiaConfig()->getMediaPath($file));

        try {
            $ioObject->open(array('path'=>$destDirectory));
        } catch (Exception $e) {
            $ioObject->mkdir($destDirectory, 0777, true);
            $ioObject->open(array('path'=>$destDirectory));
        }

        if (strrpos($file, '.tmp') == strlen($file)-4) {
            $file = substr($file, 0, strlen($file)-4);
        }

        $destFile = dirname($file) . $ioObject->dirsep()
                  . Varien_File_Uploader::getNewFileName($this->_getMadiaConfig()->getMediaPath($file));

			  $ioObject->mv(
					$this->_getMadiaConfig()->getTmpMediaPath($file),
					$this->_getMadiaConfig()->getMediaPath($destFile)
			  );	
			  				
        return str_replace($ioObject->dirsep(), '/', $destFile);
    }
	
    /**
     * Retrive media config
     *
     * @return Mage_Catalog_Model_Product_Media_Config
     */
    protected function _getMadiaConfig()
    {
        return Mage::getSingleton('catalog/product_media_config');
    }	 
      	

}
