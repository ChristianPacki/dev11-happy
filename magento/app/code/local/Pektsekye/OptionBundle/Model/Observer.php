<?php
class Pektsekye_OptionBundle_Model_Observer extends Mage_Core_Model_Abstract
{
  protected $_originalProduct;
  protected $_duplicatedProduct;
  protected $_prevoiusTableName = ''; 
  protected $_innerProductLoad = false;
  
	public function productSaveBefore(Varien_Event_Observer $observer)
	{		
		$product = $observer->getEvent()->getProduct();
		if ($product->getId() == null)
		  return;
		  
		$productId   = (int) $product->getId();
		$storeId	   = (int) $product->getStoreId();	      
    $relation    = $product->getOptionbundleRelation();
    $boption     = $product->getOptionbundleBoption();
    $bvalue      = $product->getOptionbundleBvalue();     
    $option      = $product->getOptionbundleOption();
    $value       = $product->getOptionbundleValue();     
    
    if (!empty($relation))
      Mage::getModel('optionbundle/relation')->saveRelationsData($productId, Zend_Json::decode($relation));
    if (!empty($boption))			
      Mage::getModel('optionbundle/boption')->saveBoptions($productId, $storeId, $boption);
    if (!empty($bvalue))			
      Mage::getModel('optionbundle/bvalue')->saveValues($productId, $storeId, $bvalue);      
    if (!empty($option))			
      Mage::getModel('optionbundle/option')->saveOptions($productId, $storeId, $option);  	         	
    if (!empty($value))			
      Mage::getModel('optionbundle/value')->saveValues($productId, $storeId, $value);                
					    
	}




  public function prepareProductSave($observer)
  {
  
    $product = $observer->getEvent()->getProduct();

    $currentProductId       = (int) $product->getId();
    $importSku              = $product->getOptionbundleImportSku();
    $importBundleOptions    = $product->getOptionbundleImportBundleOptions(); 
    $importCustomOptions    = $product->getOptionbundleImportCustomOptions();    
  
    if (!empty($importSku)){
    
      $originalProduct = Mage::getModel('catalog/product');
      $originalProductId = $originalProduct->getIdBySku($importSku);            	
      if ($originalProductId == null){
        Mage::getSingleton('adminhtml/session')->addError(Mage::helper('optionbundle')->__('Product SKU "%s" does not exist.', $importSku));        
        return;
      } 
      
      $originalProduct->load($originalProductId);
      
      $importType = '';
      if (!empty($importBundleOptions) && $originalProduct->getTypeId() == Mage_Catalog_Model_Product_Type::TYPE_BUNDLE){
        $isComplete = Mage::getModel('optionbundle/boption')->importOptions($originalProduct, $product, $product->getStoreId());
        if (!$isComplete)
          return;      
        $importType = 'bundle';
      }
    }     
    
    return $this;
  }
    
    
    




	public function productSaveAfter(Varien_Event_Observer $observer)
	{		
		$product = $observer->getEvent()->getProduct();
		
    $option = $product->getOptionbundleOption();			    
    if (!empty($option)){			
      Mage::getModel('optionbundle/option')->saveCustomOptionsOrder($option);
    }
    
    $boption = $product->getOptionbundleBoption();	    
    if (!empty($boption)){	      		
      Mage::getModel('optionbundle/boption')->saveBoptionsOrder($boption);	
      Mage::getModel('optionbundle/boption')->saveBoptionsDefault($boption);	            
		}	
		
		$currentProductId       = (int) $product->getId();
    $importSku              = $product->getOptionbundleImportSku();
    $importBundleOptions    = $product->getOptionbundleImportBundleOptions(); 
    $importCustomOptions    = $product->getOptionbundleImportCustomOptions();    
	
    if (!empty($importSku)){
    
      $originalProduct = Mage::getModel('catalog/product');            	      
      $originalProduct->load($originalProduct->getIdBySku($importSku));
      
      $importType = '';
      if ($importBundleOptions == 1 && $originalProduct->getTypeId() == Mage_Catalog_Model_Product_Type::TYPE_BUNDLE){
        $isComplete = Mage::getModel('optionbundle/boption')->copyOptionData($originalProduct, $product);
        if (!$isComplete)
          return;      
        $importType = 'bundle';
      }       

      if ($importCustomOptions == 1){          	
        $isComplete = Mage::getModel('optionbundle/option')->importOptions($originalProduct, $currentProductId);
        if (!$isComplete)
          return;         
        $importType = $importType == '' ? 'option' : 'both';         
      }
      
      if ($importType != '')
        Mage::getModel('optionbundle/relation')->importRelations($originalProduct, $product, $importType);
      
    }
   
  } 
	
	
	
	public function productDuplicate(Varien_Event_Observer $observer)
	{	
	  $this->_originalProduct   = $observer->getEvent()->getCurrentProduct();	  
	  $this->_duplicatedProduct = $observer->getEvent()->getNewProduct();
	  
	  //execution of this function continues in self::resourceGetTablename	 		    
	}
	
	
	
	/*
    the following function is called just after:
    app/code/core/Mage/Catalog/Model/Product.php
    function duplicate
    line 1445 $this->getOptionInstance()->duplicate
    Magento 1.7
	*/
		public function resourceGetTableName(Varien_Event_Observer $observer)
	{
    if (!isset($this->_duplicatedProduct))
      return;	

    $newProductId = $this->_duplicatedProduct->getId();	
	  $tableName = $observer->getEvent()->getTableName();	  
    if ($newProductId != null && $this->_prevoiusTableName == 'catalog_product_option' && $tableName == 'catalog_product_entity'){
    
      $importType = 'option';            
      if ($this->_originalProduct->getTypeId() == Mage_Catalog_Model_Product_Type::TYPE_BUNDLE){
        Mage::getModel('optionbundle/boption')->copyOptionData($this->_originalProduct, $this->_duplicatedProduct);
        $importType = 'both';        
      }              
      
      Mage::getModel('optionbundle/option')->copyOptionData($this->_originalProduct, $newProductId);  
                      
      Mage::getModel('optionbundle/relation')->importRelations($this->_originalProduct, $this->_duplicatedProduct, $importType);                    

      unset($this->_duplicatedProduct);
    }
    	 
    $this->_prevoiusTableName = $tableName;    				    
	}		


  
  public function unsetRequired($observer)
  {      	   
    if ($this->_innerProductLoad)
      return $this;
      
    $collection = $observer->getEvent()->getCollection();

    if ($collection instanceof Mage_Bundle_Model_Resource_Option_Collection){
      
      if (in_array($this->getPath(), array('checkout_cart_configure','admin_sales_order_create_configureQuoteItems','admin_sales_order_create_configureProductToAdd','wishlist_index_configure'))){//to make magento display single value option as selectable.

        if (preg_match('/parent_id\D+(\d+)/', $collection->getSelect(), $matches)){
          $productId = $matches[1];
          $this->_innerProductLoad = true;                        
          $product = Mage::getModel('catalog/product')->load($productId); 
          $singleSelectionOptions = Mage::helper('optionbundle')->getSingleSelectionOptions($product);
          $this->_innerProductLoad = false;                                 
          foreach ($collection as $option) {
            if (isset($singleSelectionOptions[$option->getId()])){
              $option->setRequired(false);
              $option->setObRequired(true);
            }
          }            
        }        
      } 
    }                      
  }

  
  
  public function getPath()
  { 
    $request = Mage::app()->getRequest();
    
    $moduleName = $request->getModuleName();    
    $adminPath  = (string) Mage::getConfig()->getNode(Mage_Adminhtml_Helper_Data::XML_PATH_ADMINHTML_ROUTER_FRONTNAME);
    if ($moduleName == $adminPath)
      $moduleName = 'admin';

    return $moduleName .'_'. $request->getControllerName() .'_'. $request->getActionName(); 
  } 

  
  
  public function unsetBundleRequired($observer)
  {   	         
	  $product = $observer->getEvent()->getProduct();	
    $this->registerHiddenOoptionIds($product);       
  }
  

  public function salesQuoteItemSetProduct($observer)
  { 
	  $product = $observer->getEvent()->getProduct();
	  if (version_compare(Mage::getVersion(), '1.9.1.0') < 0){ 
      $this->registerHiddenOoptionIds($product);    
    }  
    
		$product->setSkipCheckRequiredOption(true);    
  } 



  public function registerHiddenOoptionIds($product)
  {  
      $requestOptions = array('b' => array(), 'o' => array());  
      $buyRequest = $product->getCustomOption('info_buyRequest');
      if ($buyRequest){
        $bArray = unserialize($buyRequest->getValue());
        $request = new Varien_Object($bArray);
        if ($request->getBundleOption()) 
          $requestOptions['b'] = $request->getBundleOption();        
        if ($request->getOptions())  
          $requestOptions['o'] = $request->getOptions();                                 
      }
      $productModel = Mage::getModel('catalog/product')->load($product->getId());
      $hiddenOIds = Mage::helper('optionbundle')->getHiddenOptions($productModel, $requestOptions);       
  
      Mage::unregister('ob_ids');
      Mage::register('ob_ids', $hiddenOIds);    
  }
	  	
}

