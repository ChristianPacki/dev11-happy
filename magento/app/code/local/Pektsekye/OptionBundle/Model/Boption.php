<?php

class Pektsekye_OptionBundle_Model_Boption extends Mage_Core_Model_Abstract
{	
    
    public function _construct()
    {
      parent::_construct();
      $this->_init('optionbundle/boption');
    }


    public function getBoptions($productId, $storeId = 0)
    {        
      return $this->getResource()->getBoptions($productId, $storeId);                            
    } 


    public function getBoptionsAllStores($productId)
    {   
    
      $data = $this->getResource()->getBoptions($productId, 0);
      foreach($data as $k => $r){
        $data[$k]['notes'] = $this->getResource()->getStoreNotes($r['boption_id']);         
      }
      return $data;                          
    } 
    
    
    public function importOptions($originalProduct, $currentProduct, $storeId)
    {          
          
      $data = new Varien_Object(array('event' => new Varien_Object(array('current_product' => $originalProduct, 'new_product' => $currentProduct))));
      
      Mage::getModel('bundle/observer')->duplicateProduct($data);
      
      $currentProduct->setCanSaveBundleSelections(true);
      
    }   



    public function copyOptionData($originalProduct, $currentProduct)
    { 
          
      $storeIds = (array) $originalProduct->getStoreIds();

      array_unshift($storeIds, 0);
            
      $tIds = $this->getTranslatedIds($originalProduct, $currentProduct);   
        
      foreach ($storeIds as $storeId) {

        $optionData = $this->getBoptions($originalProduct->getId(), $storeId);    
        if (count($optionData) == 0)
          return false;

        foreach($optionData as $k => $option){      
          if (is_null($option['store_note']))
            $optionData[$k]['scope'] = 1;  
        }

        $this->saveBoptions($currentProduct->getId(), $storeId ,$this->translateIds($optionData, $tIds));
  
        Mage::getModel('optionbundle/bvalue')->copyValueData($originalProduct, $currentProduct->getId(), $storeId, $tIds);
     
      }
      
      return true;                   
    } 
    
    

    public function translateIds($options, $tIds)
    {    
      $tOptions = array(); 
      foreach ($options as $id => $option){
        if (isset($tIds[0][$id])){
          $tId = $tIds[0][$id];        
          $tOptions[$tId] = $option;
        }   
      }                
      return $tOptions;
    }   
 
 
 
     public function getTranslatedIds($originalProduct, $currentProduct)
    {   
    
      $originalProduct->getTypeInstance(true)->setStoreFilter($originalProduct->getStoreId(), $originalProduct);
      $originalCollection = $originalProduct->getTypeInstance(true)->getOptionsCollection($originalProduct);
      $originalSelectionCollection = $originalProduct->getTypeInstance(true)->getSelectionsCollection(
          $originalProduct->getTypeInstance(true)->getOptionsIds($originalProduct),
          $originalProduct
      );
      $originalCollection->appendSelections($originalSelectionCollection);

      $currentProduct->getTypeInstance(true)->setStoreFilter($currentProduct->getStoreId(), $currentProduct);
      $newCollection = $currentProduct->getTypeInstance(true)->getOptionsCollection($currentProduct);
      $newSelectionCollection = $currentProduct->getTypeInstance(true)->getSelectionsCollection(
          $currentProduct->getTypeInstance(true)->getOptionsIds($currentProduct),
          $currentProduct
      );
      $newCollection->appendSelections($newSelectionCollection);
    
    
      $tIds = array();      
      $newOptions = array_values($newCollection->getItems());                     
      foreach (array_values($originalCollection->getItems()) as $k => $option) {
        $tIds[0][$option->getId()] = $newOptions[$k]->getId();        
        $newValues = array_values($newOptions[$k]->getSelections());
        foreach (array_values($option->getSelections()) as $kk => $value){
          $tIds[1][$value->getSelectionId()] = $newValues[$kk]->getSelectionId();
        }                        
      }
                 
      return $tIds;    
    }  
   
    
    
    
    public function saveBoptions($productId, $storeId, $boptions)
    {
      return $this->getResource()->saveBoptions($productId, $storeId, $boptions);
    }
    
    
    
    public function saveBoptionsOrder($boptions)
    { 
      foreach($boptions as $id => $value){
        if (isset($value['order']) && $value['order'] != $value['previous_order']) 
          $this->getResource()->saveBoptionsOrder((int) $id, (int) $value['order']);  
      }
    }    
    
    
    
    public function saveBoptionsDefault($boptions)
    { 
      foreach($boptions as $id => $value){
        if (!isset($value['previous_default']))
          continue;
        $default = isset($value['default']) ? (array) $value['default'] : array();          
        $previousDefault = explode(',', $value['previous_default']);  
        
        if ($default != $previousDefault){ 
            $this->getResource()->saveBoptionsDefault((int) $id, 0, $previousDefault);          
            $this->getResource()->saveBoptionsDefault((int) $id, 1, $default);
        }    
      }
    } 
   
    
    
    public function saveCsvBoptions($productId, $boptions, $tIds, $storeIds)
    {
      $boptions = $this->translateCsvBoptions($boptions, $tIds, $storeIds);      
      $this->saveBoptions($productId, 0, $boptions);
    }
    
    
    
    public function translateCsvBoptions($boptions, $tIds, $storeIds)
    {
      $t = 'b';
      $tBoptions = array();       
      foreach($boptions as $id => $value){
        if (isset($tIds[0][$t][$id])){
          $tId = $tIds[0][$t][$id];
          $tValue = $value;
          if (isset($value['notes'])){
            $tValue['notes'] = array();
            foreach($value['notes'] as $storeCode => $note){
              $storeId = isset($storeIds[$storeCode]) ? $storeIds[$storeCode] : 0;
              if (!isset($tValue['notes'][$storeId])){
                $tValue['notes'][$storeId] = $note;          
              }
            }         
          }                              
          $tBoptions[$tId] = $tValue;
        }   
      } 
                
      return $tBoptions;
    }
 
}
