<?php

class Pektsekye_OptionBundle_Model_Quote extends Mage_Sales_Model_Quote
{
    
   
    public function addProductAdvanced(Mage_Catalog_Model_Product $product, $request = null, $processMode = null)
    {
		  if ($product->getRequiredOptions() && $request != null && (isset($request['options']) || isset($request['bundle_option']))){		
		 
        $requestOptions = array('b' => array(), 'o' => array());
        $hasSelected = false;
                
        if (isset($request['options'])){
          foreach ($request['options'] as $v){
            if (!empty($v)){
              $hasSelected = true;
              break;
            }                 	  
          }        
          $requestOptions['o'] = $request['options'];          
        }
        
        if (isset($request['bundle_option'])){ 
          foreach ($request['bundle_option'] as $v){
            if (!empty($v)){
              $hasSelected = true;
              break;
            }                 	  
          }                 
          $requestOptions['b'] = $request['bundle_option'];                     
        }
        
        if ($hasSelected){	         
          $productModel = Mage::getModel('catalog/product')->load($product->getId());
          $hiddenOIds = Mage::helper('optionbundle')->getHiddenOptions($productModel, $requestOptions); 
 
          foreach ($product->getOptions() as $option){
            if (isset($hiddenOIds['o'][$option->getId()])) 
              $option->setIsRequire(false);		  
          }
				}  
		  }
		  
		  return parent::addProductAdvanced($product, $request, $processMode);    
    }
}
