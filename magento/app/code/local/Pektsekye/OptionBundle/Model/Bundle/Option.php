<?php

class Pektsekye_OptionBundle_Model_Bundle_Option extends Mage_Bundle_Model_Option
{

    public function getRequired()
    {
        $ids = Mage::registry('ob_ids');
        if ($ids && isset($ids['b'][$this->getId()])){
          return false; 
        }

        $request = Mage::app()->getRequest();    
        $path = $request->getModuleName() .'_'. $request->getControllerName() .'_'. $request->getActionName();
        if ($path == 'catalog_product_view'){
          $trace = debug_backtrace(false, 2);
          if ($trace[1]['function'] == '_showSingle'
              || strpos($trace[0]['file'], 'bundle/option/checkbox.phtml') !== false
              || strpos($trace[0]['file'], 'bundle/option/multi.phtml') !== false){
            return false; 	      
          }            
        }  

        return $this->getData('required');
    }

}
