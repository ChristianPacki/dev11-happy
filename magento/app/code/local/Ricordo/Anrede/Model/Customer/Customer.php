<?php

/**
 * @category	Mage
 * @package	Ricordo_Anrede
 * @developer	Cajetan Oberhaus (cajetan.oberhaus@gmail.com)
 * @version	0.2
 * @license	http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */


class Ricordo_Anrede_Model_Customer_Customer extends Mage_Customer_Model_Customer
{
	
    public function getNameFormal()
    {
        $nameFormal = '';
		if ($this->getPrefix()) {
        if ($this->getPrefix()=='Frau') {
            $nameFormal .= 'liebe '. $this->getPrefix() . ' ';
        }
		if ($this->getPrefix()=='Herr') {
            $nameFormal .= 'lieber '. $this->getPrefix() . ' ';
        }
		
		
        $nameFormal .=  ' ' . $this->getLastname();
        if ($this->getSuffix()) {
            $nameFormal .= ' ' . $this->getSuffix();
        }
        return $nameFormal;
		
		} else {
        $nameFormal .= $this->getFirstname();
        if ($this->getMiddlename()) {
            $nameFormal .= ' ' . $this->getMiddlename();
        }
        $nameFormal .=  ' ' . $this->getLastname();
        if ($this->getSuffix()) {
            $nameFormal .= ' ' . $this->getSuffix();
        }
        return $nameFormal; }
    }

    public function getNameFormalShort()
    {
        $nameFormal = '';
		if ($this->getPrefix()) {

        $nameFormal .= $this->getPrefix() . ' ' . $this->getLastname();
        if ($this->getSuffix()) {
            $nameFormal .= ' ' . $this->getSuffix();
        }
        return $nameFormal;

		} else {
        $nameFormal .= $this->getFirstname();
        if ($this->getMiddlename()) {
            $nameFormal .= ' ' . $this->getMiddlename();
        }
        $nameFormal .=  ' ' . $this->getLastname();
        if ($this->getSuffix()) {
            $nameFormal .= ' ' . $this->getSuffix();
        }
        return $nameFormal; }
    }

}