<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_Optimization
 */


class Amasty_Optimization_Model_Minification_QueueProcessor
{
    const MAX_FILE_COUNT_PER_STEP = 10;

    public function process()
    {
        $tasks = Mage::getResourceModel('amoptimization/task_collection')
            ->setPageSize(self::MAX_FILE_COUNT_PER_STEP)
            ->load();

        foreach ($tasks as $task) {
            $success = $task->process();
            if ($success) {
                $task->delete();
            }
        }
    }
}
