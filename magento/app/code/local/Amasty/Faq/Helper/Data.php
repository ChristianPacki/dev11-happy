<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_Faq
 */

class Amasty_Faq_Helper_Data extends Mage_Core_Helper_Abstract
{
    public function getSearchUrl()
    {
        return $this->getFaqUrl() . 'search';
    }

    public function getFaqUrl()
    {
        return $this->_getUrl(Mage::getStoreConfig('amfaq/general/url_prefix'));
    }

    public function getAskActionUrl()
    {
        return $this->_getUrl(Mage::getStoreConfig('amfaq/general/url_prefix'));
    }

    public function getRateUrl()
    {
        return $this->_getUrl('amfaq/ajax/rate');
    }

    public function getUploadDir(){
        return Mage::getBaseDir('media') . DS . 'amfaq' . DS ;
    }

    public function getUploadFilename($name){
        return $this->getUploadDir() . $name ;
    }

    public function getDownloadUrl($id)
    {
        return $this->_getUrl('amfaq/file/download', array('id' => $id));
    }

    public function getTopicPrefix()
    {
        return 'topic-';
    }

    public function getVisibilities()
    {
        return array(
            'none'      => $this->__('None'),
            'public'    => $this->__('Public'),
            'auth'      => $this->__('For Logged In Only'),
//            'purchased' => $this->__('If Product Purchased'),
        );
    }
/* removed functionality
    public function productPurchased($product)
    {
        $customer = Mage::getSingleton('customer/session')->getCustomer()->getId();
        if ($customer)
        {
            $order = Mage::getResourceModel('sales/order');
            $adapter = $order->getReadConnection();
            $select = $adapter
                ->select()
                ->from(array('order' => $order->getTable('sales/order')), 'entity_id')
                ->join(
                    array('item' => $order->getTable('sales/order_item')),
                    'item.order_id = order.entity_id',
                    array()
                )
                ->where('customer_id = ?', $customer)
                ->where('item_id = ?', $product->getId())
                ->limit(1);
            ;
            $purchased = $adapter->fetchOne($select) !== false;

            return $purchased;
        }

        return false;
    }*/

    public function notifyUser($questionId)
    {
        $model = Mage::getModel('amfaq/question')->load($questionId);
        $store = Mage::getModel('core/store')->load($model->getFromStore());

        $relatedProductLinks = '';
        $productIds = $model->getProductIds();
        if ($productIds && !empty($productIds)) {
            foreach ($productIds as $id) {
                $path = Mage::getResourceModel('core/url_rewrite')
                    ->getRequestPathByIdPath('product/' . $id, $store);
                $url = $store->getBaseUrl($store::URL_TYPE_WEB) . $path;
                $relatedProductLinks .= '<a href="' . $url . '">' . $url . '</a><br>';
            }
        }

        $translate = Mage::getSingleton('core/translate');
        $translate->setTranslateInline(false);

        $tpl = Mage::getModel('core/email_template');
        $tpl->setDesignConfig(array('area' => 'frontend', 'store' => $store->getId()))
            ->sendTransactional(
                Mage::getStoreConfig('amfaq/user_notification/template', $store->getId()),
                Mage::getStoreConfig('amfaq/user_notification/from', $store->getId()),
                $model->getEmail(),
                $this->__('Administrator'),
                array(
                    'website_name'  => $store->getWebsite()->getName(),
                    'group_name'    => $store->getGroup()->getName(),
                    'store_name'    => $store->getName(),
                    'question'	    => $model->getTitle(),
                    'answer'	    => $model->getAnswer(),
                    'username'      => $model->getName(),
                    'product_links' => $relatedProductLinks,
                )
            );

        $translate->setTranslateInline(true);
    }

    public function notifyAdmin($questionId, $currentUrl = false)
    {
        $model = Mage::getModel('amfaq/question');

        $model->load($questionId);

        $translate = Mage::getSingleton('core/translate');
        $translate->setTranslateInline(false);

        $sender = array(
            'name'  => $model->getName(),
            'email' => $model->getEmail()
        );

        $store = Mage::app()->getStore();
        $tpl = Mage::getModel('core/email_template');
        $tpl->setDesignConfig(array('area'=>'frontend', 'store'=>$store->getId()))
            ->sendTransactional(
                Mage::getStoreConfig('amfaq/admin_notification/template'),
                $sender,
                Mage::getStoreConfig('amfaq/admin_notification/to'),
                $this->__('Administrator'),
                array(
                    'website_name'  => $store->getWebsite()->getName(),
                    'group_name'    => $store->getGroup()->getName(),
                    'store_name'    => $store->getName(),
                    'question'	    => $model->getTitle(),
                    'usermail'      => $model->getEmail(),
                    'username'      => $model->getName(),
                    'current_url'   => $currentUrl,
                )
            );
        $translate->setTranslateInline(true);
    }

    public function canVote()
    {
        $customer = Mage::getSingleton('customer/session')->getCustomer();
        return Mage::getSingleton('amfaq/question')->canVote($customer);
    }

    public function getSearchResultsPerPage()
    {
        return 10;
    }
}