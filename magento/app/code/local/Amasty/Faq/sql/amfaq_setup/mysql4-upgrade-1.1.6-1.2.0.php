<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_Faq
 */


$this->startSetup();

$this->run("
    ALTER TABLE `{$this->getTable('amfaq/question_rating')}` ADD `rating_type` TINYINT NOT NULL AFTER `rating_id`;
    ALTER TABLE `{$this->getTable('amfaq/question')}` ADD `yesno_rating` float NOT NULL AFTER `calculated_rating`;
    ALTER TABLE `{$this->getTable('amfaq/question')}` ADD `meta_robots` varchar(127) NOT NULL;
    ALTER TABLE `{$this->getTable('amfaq/question')}` ADD `from_store` smallint(5) unsigned NOT NULL;
");

$this->endSetup();
