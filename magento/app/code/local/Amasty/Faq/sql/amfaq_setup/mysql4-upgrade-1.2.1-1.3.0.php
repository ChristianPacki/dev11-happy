<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_Faq
 */


$this->startSetup();

$this->run("
    ALTER TABLE `{$this->getTable('amfaq/question')}` ADD `canonical_url` VARCHAR(127) NOT NULL AFTER `meta_description`;
    ALTER TABLE `{$this->getTable('amfaq/topic')}` ADD `canonical_url` VARCHAR(127) NOT NULL AFTER `meta_description`;
");

$this->endSetup();
