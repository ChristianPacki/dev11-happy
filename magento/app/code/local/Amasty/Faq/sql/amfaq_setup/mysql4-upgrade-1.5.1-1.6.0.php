<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_Faq
 */


$this->startSetup();

$this->run("
    UPDATE `{$this->getTable('amfaq/question')}` SET `status` = 'answered' WHERE `status` = '';
");

$this->endSetup();
