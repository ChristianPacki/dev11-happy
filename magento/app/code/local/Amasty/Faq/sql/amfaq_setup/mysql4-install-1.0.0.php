<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_Faq
 */

$installer = $this;
$installer->startSetup();

// Question

$installer->run("
CREATE TABLE IF NOT EXISTS `{$this->getTable('amfaq/question')}` (
  `question_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` text NOT NULL,
  `answer` text NOT NULL,
  `visibility` enum('none', 'public','auth','purchased') NOT NULL DEFAULT 'public',
  `status` enum('pending','answered') NOT NULL DEFAULT 'answered',
  `customer_id` int(10) unsigned DEFAULT NULL,
  `name` varchar(128) NOT NULL,
  `email` varchar(128) NOT NULL,
  `position` int(10) NOT NULL,
  `rating` float NOT NULL,
  `calculated_rating` float NOT NULL,
  `url_key` varchar(127) NOT NULL,
  `meta_title` VARCHAR( 128 ) NOT NULL,
  `meta_description` text NOT NULL,
  PRIMARY KEY (`question_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;
");

// Topic

$installer->run("
CREATE TABLE IF NOT EXISTS `{$this->getTable('amfaq/topic')}` (
  `topic_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(128) NOT NULL,
  `sidebar` tinyint(1) NOT NULL,
  `sidebar_position` int( 10 ) NOT NULL,
  `position` int(10) NOT NULL,
  `url_key` varchar(127) NOT NULL,
  `meta_title` VARCHAR( 128 ) NOT NULL,
  `meta_description` TEXT NOT NULL,
  PRIMARY KEY (`topic_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;
");

// Question-Topic

$installer->run("
CREATE TABLE IF NOT EXISTS `{$this->getTable('amfaq/question_topic')}` (
  `question_id` int(10) unsigned NOT NULL,
  `topic_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`question_id`,`topic_id`)
) ENGINE=InnoDB;
");

$installer->run("
ALTER TABLE  `{$this->getTable('amfaq/question_topic')}`
  ADD FOREIGN KEY (`question_id`) REFERENCES `{$this->getTable('amfaq/question')}` (`question_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD FOREIGN KEY (`topic_id`) REFERENCES `{$this->getTable('amfaq/topic')}` (`topic_id`) ON DELETE CASCADE ON UPDATE CASCADE;
");

// Question-Product

$installer->run("
CREATE TABLE IF NOT EXISTS `{$this->getTable('amfaq/question_product')}` (
  `question_id` int(10) unsigned NOT NULL,
  `product_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`question_id`,`product_id`)
) ENGINE=InnoDB;
");

$installer->run("
ALTER TABLE  `{$this->getTable('amfaq/question_product')}`
  ADD FOREIGN KEY (`question_id`) REFERENCES `{$this->getTable('amfaq/question')}` (`question_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD FOREIGN KEY (`product_id`) REFERENCES `{$this->getTable('catalog/product')}` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE;
");

// Topic-Store

$installer->run("
CREATE TABLE IF NOT EXISTS `{$this->getTable('amfaq/topic_store')}` (
  `topic_id` int(10) unsigned NOT NULL,
  `store_id` smallint(5) unsigned NOT NULL,
  PRIMARY KEY (`topic_id`,`store_id`)
) ENGINE=InnoDB;
");

$installer->run("
ALTER TABLE  `{$this->getTable('amfaq/topic_store')}`
  ADD FOREIGN KEY (`topic_id`) REFERENCES `{$this->getTable('amfaq/topic')}` (`topic_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD FOREIGN KEY (`store_id`) REFERENCES `{$this->getTable('core/store')}` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE;
");

// Question-Store

$installer->run("
CREATE TABLE IF NOT EXISTS `{$this->getTable('amfaq/question_store')}` (
  `question_id` int(10) unsigned NOT NULL,
  `store_id` smallint(5) unsigned NOT NULL,
  PRIMARY KEY (`question_id`,`store_id`)
) ENGINE=InnoDB;
");

$installer->run("
ALTER TABLE  `{$this->getTable('amfaq/question_store')}`
  ADD FOREIGN KEY (`question_id`) REFERENCES `{$this->getTable('amfaq/question')}` (`question_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD FOREIGN KEY (`store_id`) REFERENCES `{$this->getTable('core/store')}` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE;
");

// Rating
$installer->run("
CREATE TABLE IF NOT EXISTS `{$this->getTable('amfaq/question_rating')}` (
  `rating_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `question_id` int(10) unsigned NOT NULL,
  `customer_id` int(10) unsigned DEFAULT NULL,
  `session_id` varchar(40) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL,
  `rate` int(10) NOT NULL,
  `used` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`rating_id`),
  KEY `question_id` (`question_id`,`customer_id`,`session_id`)
) ENGINE=InnoDB;
");

$installer->run("
ALTER TABLE  `{$this->getTable('amfaq/question_rating')}`
  ADD FOREIGN KEY (`question_id`) REFERENCES `{$this->getTable('amfaq/question')}` (`question_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD FOREIGN KEY (`customer_id`) REFERENCES `{$this->getTable('customer/entity')}` (`entity_id`) ON DELETE SET NULL ON UPDATE SET NULL;
");

// Files

$installer->run("
CREATE TABLE IF NOT EXISTS `{$this->getTable('amfaq/file')}` (
  `file_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `question_id` int(10) unsigned DEFAULT NULL,
  `name` varchar(128) NOT NULL,
  PRIMARY KEY (`file_id`),
  KEY `question_id` (`question_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;
");

$installer->run("
ALTER TABLE  `{$this->getTable('amfaq/file')}` ADD FOREIGN KEY (`question_id`) REFERENCES `{$this->getTable('amfaq/question')}` (
`question_id`
) ON DELETE SET NULL ON UPDATE SET NULL ;
");

// Related

$installer->run("
CREATE TABLE IF NOT EXISTS `{$this->getTable('amfaq/question_relation')}` (
  `a` int(10) unsigned NOT NULL,
  `b` int(10) unsigned NOT NULL,
  KEY `b` (`b`),
  KEY `a` (`a`)
) ENGINE=InnoDB;
");

$installer->run("
ALTER TABLE `{$this->getTable('amfaq/question_relation')}`
  ADD FOREIGN KEY (`b`) REFERENCES `{$this->getTable('amfaq/question')}` (`question_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD FOREIGN KEY (`a`) REFERENCES `{$this->getTable('amfaq/question')}` (`question_id`) ON DELETE CASCADE ON UPDATE CASCADE;
");

// Tags

$installer->run("
CREATE TABLE IF NOT EXISTS `{$this->getTable('amfaq/tag')}` (
  `tag_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(128) NOT NULL,
  `alias` varchar(128) NOT NULL,
  PRIMARY KEY (`tag_id`),
  KEY `title` (`title`),
  KEY `alias` (`alias`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;
");

$installer->run("
CREATE TABLE IF NOT EXISTS `{$this->getTable('amfaq/question_tag')}` (
  `question_id` int(10) unsigned NOT NULL,
  `tag_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`question_id`,`tag_id`)
) ENGINE=InnoDB;
");

$installer->run("
ALTER TABLE  `{$this->getTable('amfaq/question_tag')}`
  ADD FOREIGN KEY (`question_id`) REFERENCES `{$this->getTable('amfaq/question')}` (`question_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD FOREIGN KEY (`tag_id`) REFERENCES `{$this->getTable('amfaq/tag')}` (`tag_id`) ON DELETE CASCADE ON UPDATE CASCADE;
");

$installer->endSetup();
