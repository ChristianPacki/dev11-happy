<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_Faq
 */


class Amasty_Faq_Adminhtml_Amfaq_TopicController extends Mage_Adminhtml_Controller_Action
{
    /**
     * Keys are database column names of M1 extension
     * Values are appropriate database column names of M2 extension
     *
     * @var array
     */
    protected $_exportMap = array(
        'topic_id' => 'category_id',
        'title' => 'title',
        'url_key' => 'url_key',
        'store_ids' => 'store_codes',
        'status' => 'status',
        'meta_title' => 'meta_title',
        'meta_description' => 'meta_description',
        'position' => 'position',
        'question_ids' => 'question_ids'
    );

    protected function _initAction()
    {
        $this->loadLayout()
            ->_setActiveMenu('cms/amfaq/topics')
            ->_addBreadcrumb($this->__('CMS'), $this->__('CMS'))
            ->_addBreadcrumb($this->__('Topics'), $this->__('Topics'))
        ;
        return $this;
    }

    public function indexAction()
    {
        $this->_title($this->__('CMS'))
            ->_title($this->__(Mage::getStoreConfig('amfaq/general/kb_title')))
            ->_title($this->__('Manage Topics'));

        $this->_initAction()
            ->_addContent($this->getLayout()->createBlock('amfaq/adminhtml_topic'))
            ->renderLayout();
    }

    protected function _initTopic($idFieldName = 'id')
    {
        $this->_title($this->__('Topics'))->_title($this->__('Manage Topics'));

        $topicId = (int) $this->getRequest()->getParam($idFieldName);
        $topic = Mage::getModel('amfaq/topic');

        if ($topicId) {
            $topic->load($topicId);
        }

        Mage::register('current_topic', $topic);
        return $this;
    }


    public function newAction()
    {
        $this->_forward('edit');
    }

    public function editAction()
    {
        $this->_initTopic();
        $this->loadLayout(array('default', 'amfaq_edit_form'));

        $topic = Mage::registry('current_topic');

        $this->_title($topic->getId() ? $topic->getTitle() : $this->__('New Topic'));

        $this->_setActiveMenu('cms/amfaq/topic');

        $this->_addContent($this->getLayout()->createBlock('amfaq/adminhtml_topic_edit'));
        $this->_addLeft($this->getLayout()->createBlock('amfaq/adminhtml_topic_edit_tabs'));

        $this->renderLayout();
    }

    public function saveAction()
    {
        if ($data = $this->getRequest()->getPost()) {

            $id = intVal($this->getRequest()->getParam('topic_id'));
            $model = Mage::getModel('amfaq/topic')->load($id);
            if (!$model->getId() && $id) {
                Mage::getSingleton('adminhtml/session')->addError($this->__('This topic no longer exists.'));
                $this->_redirect('*/*/');
                return;
            }
            $model->setData($data);

            try {
                $model->save();
                Mage::getSingleton('adminhtml/session')->addSuccess($this->__('The topic has been saved.'));

                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', array('id' => $model->getId()));
                    return;
                }
                $this->_redirect('*/*/');
                return;

            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setFormData($data);
                $this->_redirect('*/*/edit', array('topic_id' => $this->getRequest()->getParam('topic_id')));
                return;
            }
        }
        $this->_redirect('*/*/');
    }

    protected function _deleteTopic($id)
    {
        $model = Mage::getModel('amfaq/topic')->load($id);
        if ($model->getId())
        {
            $model->delete();
        }
    }

    public function deleteAction()
    {
        $id = Mage::app()->getRequest()->getParam('id');
        try {
            $this->_deleteTopic($id);
            Mage::getSingleton('adminhtml/session')->addSuccess($this->__('Topic deleted.'));
        } catch (Mage_Core_Exception $e) {
            $this->_getSession()->addError($e->getMessage());
        } catch (Exception $e) {
            Mage::logException($e);
            $this->_getSession()->addError($e->getMessage());
        }

        $this->_redirect('*/*/');
    }

    public function massDeleteAction()
    {
        $ids = Mage::app()->getRequest()->getParam('topic');
        foreach ($ids as $id)
        {
            try {
                $this->_deleteTopic($id);
            } catch (Mage_Core_Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            } catch (Exception $e) {
                Mage::logException($e);
                $this->_getSession()->addError($e->getMessage());
            }
        }
        Mage::getSingleton('adminhtml/session')->addSuccess($this->__('Topics deleted.'));
        $this->_redirect('*/*/');
    }

    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('cms/amfaq/topics');
    }

    public function exportAction()
    {
        $fileName = 'amasty_faq_topics.csv';
        $content = $this->_getCsvFile();
        $this->_prepareDownloadResponse($fileName, $content);
    }

    protected function _getCsvFile()
    {
        $canEscapeCSVData = method_exists('Mage_Core_Helper_Data', 'getEscapedCSVData');

        $path = Mage::getBaseDir('var') . DS . 'export' . DS;
        $name = md5(microtime());
        $file = $path . DS . $name . '.csv';

        $io = new Varien_Io_File();

        $io->setAllowCreateFolders(true);
        $io->open(array('path' => $path));
        $io->streamOpen($file, 'w+');
        $io->streamLock(true);

        $row = array();
        foreach ($this->_exportMap as $mTwoKey) {
            $row[] = $mTwoKey;
        }
        if ($canEscapeCSVData) {
            $row = Mage::helper('core')->getEscapedCSVData($row);
        }
        $io->streamWriteCsv($row);

        $collection = Mage::getModel('amfaq/topic')->getCollection();

        if (0 < $collection->getSize()) {
            foreach ($collection as $topic) {
                $row = array();
                Mage::getResourceModel('amfaq/topic')->loadExternalIds($topic, 'store');
                foreach ($this->_exportMap as $mOneKey => $mTwoKey) {
                    switch ($mOneKey) {
                        case 'store_ids':
                            $row[] = $this->_getStoreCodes($topic->getData($mOneKey));
                            break;
                        case 'status':
                            $row[] = 1;
                            break;
                        case 'question_ids':
                            $row[] = implode(',', $topic->getQuestionIds());
                            break;
                        default:
                            $row[] = $topic->getData($mOneKey);
                    }
                }
                if ($canEscapeCSVData) {
                    $row = Mage::helper('core')->getEscapedCSVData($row);
                }
                $io->streamWriteCsv($row);
            }
        }

        $io->streamUnlock();
        $io->streamClose();

        return array(
            'type'  => 'filename',
            'value' => $file,
            'rm'    => true // can delete file after use
        );
    }

    protected function _getStoreCodes($storeIds)
    {
        $storeCodes = array();
        if (!empty($storeIds)) {
            foreach (Mage::app()->getStores() as $store) {
                if (in_array($store->getId(), $storeIds)) {
                    $storeCodes[] = $store->getCode();
                }
            }
        }
        return implode(',', $storeCodes);
    }
}
