<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_Faq
 */


class Amasty_Faq_Adminhtml_Amfaq_QuestionController extends Mage_Adminhtml_Controller_Action
{
    /**
     * Keys are database column names of M1 extension
     * Values are appropriate database column names of M2 extension
     *
     * @var array
     */
    protected $_exportMap = array(
        'question_id' => 'question_id',
        'title' => 'question',
        'url_key' => 'url_key',
        'store_ids' => 'store_codes',
        'short_answer' => 'short_answer',
        'answer' => 'answer',
        'status' => 'status',
        'visibility' => 'visibility',
        'position' => 'position',
        'meta_title' => 'meta_title',
        'meta_description' => 'meta_description',
        'name' => 'name',
        'email' => 'email',
        'topic_ids' => 'category_ids',
        'product_ids' => 'product_skus'
    );

    public function indexAction()
    {
        $this->_title($this->__('CMS'))
            ->_title($this->__(Mage::getStoreConfig('amfaq/general/kb_title')))
            ->_title($this->__('Manage questions'));

        $this->loadLayout()
            ->_setActiveMenu('cms/amfaq/questions')
            ->_addContent($this->getLayout()->createBlock('amfaq/adminhtml_question'))
            ->renderLayout();
    }

    protected function _initQuestion($idFieldName = 'id')
    {
        $this->_title($this->__('Questions'))->_title($this->__('Manage Questions'));

        $questionId = (int) $this->getRequest()->getParam($idFieldName);
        $question = Mage::getModel('amfaq/question');

        if ($questionId) {
            $question->load($questionId);
        }

        Mage::register('current_question', $question);
        return $this;
    }


    public function newAction()
    {
        $this->_forward('edit');
    }

    public function editAction()
    {
        $this->_initQuestion();
        $this->loadLayout(array('default', 'editor', 'amfaq_uploads', 'amfaq_edit_form'));

        $question = Mage::registry('current_question');

        $this->_title($question->getId() ? $question->getTitle() : $this->__('New Question'));

        $this->_setActiveMenu('cms/amfaq/question');

        $this->_addContent($this->getLayout()->createBlock('amfaq/adminhtml_question_edit'));
        $this->_addLeft($this->getLayout()->createBlock('amfaq/adminhtml_question_edit_tabs'));

        $this->renderLayout();
    }

    protected function _saveFiles($question)
    {
        foreach ($question->files as $i => $data) {
            $file = Mage::getModel('amfaq/file');

            if (isset($data['file_id'])) {
                $file->load($data['file_id']);
                if ($file->getId() && $data['delete']) {
                    $file->delete();
                    continue;
                }
            }

            $file->setData($data);

            if (isset($_FILES['files']['name'][$i]['file'])
                && ($_FILES['files']['name'][$i]['file'] != "")) {
                $file->setUploadName($_FILES['files']['name'][$i]['file']);
                $file->setTmpName($_FILES['files']['tmp_name'][$i]['file']);
                if (!$file->getName()) {
                    $file->setName($_FILES['files']['name'][$i]['file']);
                }
            }
            if ($file->getId() || $file->getTmpName()) {
                $file->setQuestionId($question->getId());
                $file->save();
            }
        }
    }

    public function saveAction()
    {
        if ($data = $this->getRequest()->getPost()) {
            $id = intVal($this->getRequest()->getParam('question_id'));
            $model = Mage::getModel('amfaq/question')->load($id);
            if (!$model->getId() && $id) {
                Mage::getSingleton('adminhtml/session')->addError($this->__('This question no longer exists.'));
                $this->_redirect('*/*/');
                return;
            }

            if ($data['old_rating'] == $data['rating'])
                unset($data['rating']);

            if (!$data['email']) {
                $data['status'] = 'answered';
            }

            $model->setData($data);

            $links = $this->getRequest()->getPost('product_ids');
            $productIds = Mage::helper('adminhtml/js')->decodeGridSerializedInput($links);
            if ($links)
                $model->setProductIds($productIds);

            $related = $this->getRequest()->getPost('question_ids');
            if ($related)
                $model->setQuestionIds(Mage::helper('adminhtml/js')->decodeGridSerializedInput($related));

            try {
                $model->save();
                
                $this->_saveFiles($model);
                
                if ($this->getRequest()->getParam('notify')) {
                    $helper = Mage::helper('amfaq');
                    $helper->notifyUser($model->getId());
                }

                Mage::getSingleton('adminhtml/session')->addSuccess($this->__('The question has been saved.'));

                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', array('id' => $model->getId(), '_current'=>true));
                    return;
                }
                $this->_redirect('*/*/');
                return;

            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setFormData($data);
                $this->_redirect('*/*/edit', array('question_id' => $this->getRequest()->getParam('question_id')));
                return;
            }
        }
        $this->_redirect('*/*/');
    }

    protected function _deleteQuestion($id)
    {
        $model = Mage::getModel('amfaq/question')->load($id);
        if ($model->getId())
        {
            $model->delete();
        }
    }

    public function deleteAction()
    {
        $id = Mage::app()->getRequest()->getParam('id');
        try {
            $this->_deleteQuestion($id);
            Mage::getSingleton('adminhtml/session')->addSuccess($this->__('Question deleted.'));
        } catch (Mage_Core_Exception $e) {
            $this->_getSession()->addError($e->getMessage());
        } catch (Exception $e) {
            Mage::logException($e);
            $this->_getSession()->addError($e->getMessage());
        }

        $this->_redirect('*/*/');
    }

    public function massDeleteAction()
    {
        $ids = Mage::app()->getRequest()->getParam('question');
        foreach ($ids as $id)
        {
            try {
                $this->_deleteQuestion($id);
            } catch (Mage_Core_Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            } catch (Exception $e) {
                Mage::logException($e);
                $this->_getSession()->addError($e->getMessage());
            }
        }
        Mage::getSingleton('adminhtml/session')->addSuccess($this->__('Questions deleted.'));
        $this->_redirect('*/*/');
    }

    public function relatedGridAction()
    {
        $this->_initQuestion();

        $grid = $this->getLayout()->createBlock('amfaq/adminhtml_question_edit_tab_products')
            ->setProductIds($this->getRequest()->getPost('product_ids', null));

        $this->getResponse()->setBody($grid->toHtml());
    }

    public function relatedQuestionsGridAction()
    {
        $this->_initQuestion();

        $grid = $this->getLayout()->createBlock('amfaq/adminhtml_question_edit_tab_related')
            ->setQuestionIds($this->getRequest()->getPost('question_ids', null));

        $this->getResponse()->setBody($grid->toHtml());
    }

    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('cms/amfaq/questions');
    }

    public function exportAction()
    {
        $fileName = 'amasty_faq_questions.csv';
        $content = $this->_getCsvFile();
        $this->_prepareDownloadResponse($fileName, $content);
    }

    protected function _getCsvFile()
    {
        $canEscapeCSVData = method_exists('Mage_Core_Helper_Data', 'getEscapedCSVData');

        $path = Mage::getBaseDir('var') . DS . 'export' . DS;
        $name = md5(microtime());
        $file = $path . DS . $name . '.csv';

        $io = new Varien_Io_File();

        $io->setAllowCreateFolders(true);
        $io->open(array('path' => $path));
        $io->streamOpen($file, 'w+');
        $io->streamLock(true);

        $row = array();
        foreach ($this->_exportMap as $mTwoKey) {
            $row[] = $mTwoKey;
        }

        if ($canEscapeCSVData) {
            $row = Mage::helper('core')->getEscapedCSVData($row);
        }
        $io->streamWriteCsv($row);

        $collection = Mage::getModel('amfaq/question')->getCollection();

        if (0 < $collection->getSize()) {
            foreach ($collection as $question) {
                $row = array();
                Mage::getResourceModel('amfaq/question')->loadExternalIds($question, 'store');
                Mage::getResourceModel('amfaq/question')->loadExternalIds($question, 'topic');
                Mage::getResourceModel('amfaq/question')->loadExternalIds($question, 'product');
                foreach ($this->_exportMap as $mOneKey => $mTwoKey) {
                    switch ($mOneKey) {
                        case 'store_ids':
                            $row[] = $this->_getStoreCodes($question->getData($mOneKey));
                            break;
                        case 'status':
                            $status = 0;
                            if ('answered' == $question->getStatus()) {
                                $status = 1;
                            }
                            $row[] = $status;
                            break;
                        case 'visibility':
                            $visibility = 0;
                            if ('public' == $question->getVisibility()) {
                                $visibility = 1;
                            } elseif ('auth' == $question->getVisibility()) {
                                $visibility = 2;
                            }
                            $row[] = $visibility;
                            break;
                        case 'topic_ids':
                            $row[] = implode(',', $question->getData($mOneKey));
                            break;
                        case 'product_ids':
                            $row[] = $this->_getProductSkus($question->getData($mOneKey));
                            break;
                        default:
                            $row[] = $question->getData($mOneKey);
                    }
                }

                if ($canEscapeCSVData) {
                    $row = Mage::helper('core')->getEscapedCSVData($row);
                }
                $io->streamWriteCsv($row);
            }
        }

        $io->streamUnlock();
        $io->streamClose();

        return array(
            'type'  => 'filename',
            'value' => $file,
            'rm'    => true // can delete file after use
        );
    }

    protected function _getStoreCodes($storeIds)
    {
        $storeCodes = array();
        if (!empty($storeIds)) {
            foreach (Mage::app()->getStores() as $store) {
                if (in_array($store->getId(), $storeIds)) {
                    $storeCodes[] = $store->getCode();
                }
            }
        }
        return implode(',', $storeCodes);
    }

    protected function _getProductSkus($productIds)
    {
        $productSkus = array();
        if (!empty($productIds)) {
            $collection = Mage::getModel('catalog/product')->getResourceCollection();
            $collection->addIdFilter($productIds);
            foreach ($collection as $product) {
                $productSkus[] = $product->getSku();
            }
        }
        return implode(',', $productSkus);
    }
}
