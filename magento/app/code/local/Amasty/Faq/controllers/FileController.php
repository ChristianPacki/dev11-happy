<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_Faq
 */


class Amasty_Faq_FileController extends Mage_Core_Controller_Front_Action {

    public function downloadAction() 
    {
        $id = $this->getRequest()->getParam('id');
        $model = Mage::getModel("amfaq/file");
        $file = $model->load($id);
        if (!$file->getId()) {
            Mage::helper('ambase/utils')->_exit('invalid link');
        }

        $fileName = Mage::helper('amfaq')->getUploadFilename($file->getName());

        if (is_file($fileName)) {
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename="' . $file->getName() . '"');
            header('Content-Transfer-Encoding: binary');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($fileName));
            ob_clean();
            flush();
            readfile($fileName);
            Mage::helper('ambase/utils')->_exit();
        }
    }
}
