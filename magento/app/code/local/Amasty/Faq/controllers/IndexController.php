<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_Faq
 */


class Amasty_Faq_IndexController extends Mage_Core_Controller_Front_Action
{
    protected function _getCaptchaString($request, $formId)
    {
        $captchaParams = $request->getPost(Mage_Captcha_Helper_Data::INPUT_NAME_FIELD_VALUE);
        return $captchaParams[$formId];
    }

    protected function _prepareIndexLayout()
    {
        $this->loadLayout();

        $this->_removeDefaultTitle = true;
        $this->_title(Mage::getStoreConfig('amfaq/faq_page/title'));
        $this->getLayout()->getBlock('head')->setDescription(Mage::getStoreConfig('amfaq/faq_page/description'));

        $layout = Mage::getStoreConfig('amfaq/faq_page/layout');
        $layouts = Mage::getSingleton('page/config')->getPageLayouts();
        $this->getLayout()->getBlock('root')->setTemplate($layouts[$layout]->getTemplate());


        $block = $this->getLayout()->createBlock('amfaq/questions');
        $buttons = $this->getLayout()->createBlock('amfaq/buttons', 'buttons');

        $block->append($buttons);

        if (Mage::getStoreConfig('amfaq/faq_page/search')) {
            $searchForm = $this->getLayout()->createBlock('amfaq/search_form', 'search_form');
            $block->append($searchForm);
        }

        $block->setHeader(Mage::getStoreConfig('amfaq/faq_page/header'));

        if ($cmsBlockId = Mage::getStoreConfig('amfaq/faq_page/cms_block')) {
            $cmsBlock = $this->getLayout()->createBlock('cms/block', 'cms_block')->setBlockId($cmsBlockId);
            $block->append($cmsBlock);
        }

        $breadCrumbs = $this->getLayout()->getBlock('breadcrumbs');
        if ($breadCrumbs) {
            $breadCrumbs->addCrumb('home', array(
                'label'=>Mage::helper('catalog')->__('Home'),
                'title'=>Mage::helper('catalog')->__('Go to Home Page'),
                'link'=>Mage::getBaseUrl()
            ));

            $breadCrumbs->addCrumb('faq', array(
                'label' => $this->__(Mage::getStoreConfig('amfaq/general/kb_title')),
            ));
        }

        $this->getLayout()->getBlock('content')->append($block);

        $this->renderLayout();
    }

    public function indexAction()
    {
        Mage::helper('amfaq/route')->restrictUndispatched();

        $ajax = $this->getRequest()->isXmlHttpRequest();

        if ($data = $this->getRequest()->getPost()) {
            if ($this->getRequest()->getParam('imarobot')) {
                return;
            }

            if (isset($data['name']) && isset($data['email']) && isset($data['title'])) {
                $customer = Mage::getSingleton('customer/session')->getCustomer();

                $formId = 'amfaq_ask_form';
                $captchaModel = Mage::helper('captcha')->getCaptcha($formId);
                if ($captchaModel->isRequired()) {
                    if (!$captchaModel->isCorrect($this->_getCaptchaString($this->getRequest(), $formId))) {
                        $error = $this->__('Wrong captcha');
                        if ($ajax) {
                            $this->getResponse()->setBody($error);
                        } else {
                            Mage::getSingleton('core/session')->addError($error);
                        }

                        return;
                    }
                }

                $data['name'] = Mage::helper('core')->escapeHtml($data['name']);
                $data['email'] = Mage::helper('core')->escapeHtml($data['email']);
                $data['title'] = Mage::helper('core')->escapeHtml($data['title']);

                if (Mage::getStoreConfig('amfaq/gdpr/enabled')
                    && !isset($data['amfaq-gdpr'])
                ) {
                    $data['name'] = '';
                    $data['email'] = '';
                }

                $status = 'answered';
                if ($data['email']) {
                    $status = 'pending';
                }

                $model = Mage::getModel('amfaq/question');
                $model->setData($data);
                $model->setStoreIds(array(0));
                $model->addData(array(
                    'status' => $status,
                    'visibility' => 'none',
                    'from_store' => Mage::app()->getStore()->getId(),
                ));

                if ($customer->getId()) {
                    $model->setCustomerId($customer->getId());
                }

                $model->save();

                if (Mage::getStoreConfig('amfaq/admin_notification/send')) {
                    $helper = Mage::helper('amfaq');
                    $helper->notifyAdmin(
                        $model->getId(),
                        $this->getRequest()->getParam('current_url', false)
                    );
                }

                if (!$ajax) {
                    Mage::getSingleton('core/session')->addSuccess($this->__('Message sent'));
                    $this->getResponse()->setRedirect(Mage::helper('amfaq')->getFaqUrl());
                }
            }
        }

        if (!$ajax
            && (!isset($data['name']) || !isset($data['email']) || !isset($data['title']))
        ) {
            $this->_prepareIndexLayout();
        }
    }
}
