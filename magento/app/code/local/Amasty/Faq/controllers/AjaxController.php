<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_Faq
 */


class Amasty_Faq_AjaxController extends Mage_Core_Controller_Front_Action
{
    public function rateAction()
    {
        if ($data = $this->getRequest()->getPost()) {
            $question = Mage::getModel('amfaq/question');
            $question->load($data['question']);

            $mark = intval($data['mark']);

            $customer = Mage::getSingleton('customer/session')->getCustomer();

            if ($question->getId()
                && $mark >= 1
                && $mark <=5
                && $question->canVote($customer)
            ) {
                $question->vote($mark, session_id(), $customer->getId());
                $ratingType = Mage::getStoreConfig('amfaq/rating/type');
                if (Amasty_Faq_Model_Question_Rating::TYPE_YESNO == $ratingType) {
                    $rating = $question->getYesnoRating();
                    $result = sprintf("%.0f", $rating);
                    $result .= ' ' . $question->getAllVoted();
                } else {
                    $rating = $question->getCalculatedRating();
                    $result = sprintf("%.1f", $rating);
                }
                $this->getResponse()->setBody($result);
            }
        }
    }
}
