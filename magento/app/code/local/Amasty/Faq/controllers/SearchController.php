<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_Faq
 */


class Amasty_Faq_SearchController extends Mage_Core_Controller_Front_Action
{
    public function indexAction()
    {
        $page = $this->getRequest()->getParam('page');
        $query = trim($this->getRequest()->getParam('q'));
        $query = Mage::helper('core/string')->cleanString($query);

        if ($tagId = $this->getRequest()->getParam('tag')) {
            if (is_numeric($tagId)) {
                $field = null;
            } else {
                $field = 'alias';
            }

            $tag = Mage::getModel('amfaq/tag')->load($tagId, $field);
            if (!$tag->getId()) {
                return;
            }
        }

        $questions = Mage::getResourceModel('amfaq/question_collection');

        $this->_modifyCollection($questions, $query, isset($tag) ? $tag->getId() : null, $page);

        $questionsBlock = $this->getLayout()
            ->createBlock('amfaq/search_items', 'items')
            ->setQuestions($questions);

        if ($this->getRequest()->isXmlHttpRequest()) {
            $perPage = Mage::helper('amfaq')->getSearchResultsPerPage();
            $result = array(
                'questionsHtml' => $questionsBlock->toHtml(),
                'count'         => sizeof($questions),
                'stop'          => sizeof($questions) < $perPage ? 1 : 0
            );

            $this->getResponse()->setBody(
                Mage::helper('core')->jsonEncode($result)
            );
        } else {
            $this->loadLayout();
            $this->_removeDefaultTitle = true;
            $this->_title(Mage::getStoreConfig('amfaq/faq_page/title'));
            $this->_title($this->__('Search'));

            $layout = Mage::getStoreConfig('amfaq/faq_page/layout');
            $layouts = Mage::getSingleton('page/config')->getPageLayouts();
            $this->getLayout()->getBlock('root')->setTemplate($layouts[$layout]->getTemplate());

            $buttons = $this->getLayout()->createBlock('amfaq/buttons', 'buttons');

            $mainBlock = $this->getLayout()
                ->createBlock('amfaq/search')
                ->setQuestions($questions)
                ->append($buttons)
                ->append($questionsBlock);

            $head = $this->getLayout()->getBlock('head');
            $head->setRobots('noindex,follow');

            $crumbTitle = '';

            if ($query) {
                $searchForm = $this->getLayout()->createBlock('amfaq/search_form', 'search_form');
                $searchForm->setSearchText($query);
                $mainBlock->append($searchForm);
                $crumbTitle = $query;
            } elseif (isset($tag)) {
                $mainBlock->setTag($tag->getTitle());
                $crumbTitle = $tag->getTitle();
            }

            $breadCrumbs = $this->getLayout()->getBlock('breadcrumbs');

            if ($breadCrumbs) {
                $breadCrumbs
                    ->addCrumb('faq', array(
                        'label' => $this->__(Mage::getStoreConfig('amfaq/general/kb_title')),
                        'title' => $this->__(Mage::getStoreConfig('amfaq/general/kb_title')),
                        'link'  => Mage::helper('amfaq')->getFaqUrl(),
                    ))
                    ->addCrumb('search', array(
                        'label' => $this->__('Search: ') . Mage::helper('amfaq')->escapeHtml($crumbTitle),
                    ));
            }

            $this->getLayout()->getBlock('content')->append($mainBlock);
            $this->renderLayout();
        }
    }

    protected function _modifyCollection($collection, $query = null, $tagId = null, $page = 1)
    {
        $collection->applyDefaultFilters(); // Store, visibility, etc.

        if ($query) {
            $collection->applySearchFilter($query);
        } elseif ($tagId) {
            $collection->applyTagFilter($tagId);
        }

        $collection->applyDefaultOrder();
        $collection->getSelect()->limitPage(
            $page,
            Mage::helper('amfaq')->getSearchResultsPerPage()
        );
    }
}
