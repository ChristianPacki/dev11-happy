<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_Faq
 */


class Amasty_Faq_QuestionController extends Mage_Core_Controller_Front_Action
{
    public function viewAction()
    {
        Mage::helper('amfaq/route')->restrictUndispatched();

        $this->loadLayout();
        $this->_removeDefaultTitle = true;

        $question = Mage::getModel('amfaq/question')->load($this->getRequest()->getParam('id'));
        if (!$question->getId()) {
            $this->getResponse()->setHeader('HTTP/1.1','404 Not Found');
            $this->getResponse()->setHeader('Status','404 File not found');
            $pageId = Mage::getStoreConfig('web/default/cms_no_route');
            if (!Mage::helper('cms/page')->renderPage($this, $pageId)) {
                $this->_forward('defaultNoRoute');
            }
            return;
        }

        if ($question->getMetaTitle()) {
            $title = $question->getMetaTitle();
        } else {
            $titleFormat = Mage::getStoreConfig('amfaq/answer_page/title');
            $title = str_replace('{question}', $question->getTitle(), $titleFormat);
        }
        $this->_title($title);

        if ($question->getMetaDescription()) {
            $description = $question->getMetaDescription();
        } else {
            $descriptionFormat = Mage::getStoreConfig('amfaq/answer_page/description');
            $descriptionLimit = Mage::getStoreConfig('amfaq/answer_page/limit');
            $answer = $question->getShortAnswer();
            $answer = substr(trim(strip_tags($answer['answer'])), 0, $descriptionLimit);
            $description = str_replace('{answer}', $answer, $descriptionFormat);
        }

        if ($canonical = $question->getCanonicalUrl()) {
            $headBlock = $this->getLayout()->getBlock('head');
            $headBlock->addLinkRel(
                'canonical',
                Mage::helper('core')->quoteEscape($canonical)
            );
        }

        $this->getLayout()->getBlock('head')->setDescription($description);

        $layout = Mage::getStoreConfig('amfaq/answer_page/layout');
        $layouts = Mage::getSingleton('page/config')->getPageLayouts();
        $this->getLayout()->getBlock('root')->setTemplate($layouts[$layout]->getTemplate());

        $buttons = $this->getLayout()->createBlock('amfaq/buttons', 'buttons');

        $block = $this->getLayout()->createBlock('core/template')
            ->setTemplate('amasty/amfaq/question.phtml')
            ->setQuestion($question)
            ->append($buttons);

        $this->getLayout()->getBlock('content')->append($block);

        $breadCrumbs = $this->getLayout()->getBlock('breadcrumbs');

        if ($breadCrumbs) {
            $breadCrumbs
                ->addCrumb('home', array(
                    'label'=>Mage::helper('catalog')->__('Home'),
                    'title'=>Mage::helper('catalog')->__('Go to Home Page'),
                    'link'=>Mage::getBaseUrl()
                ))
                ->addCrumb('faq', array(
                    'label' => $this->__(Mage::getStoreConfig('amfaq/general/kb_title')),
                    'title' => $this->__(Mage::getStoreConfig('amfaq/general/kb_title')),
                    'link'  => Mage::helper('amfaq')->getFaqUrl(),
                ));

            $topic = $question->getDefaultTopic();
            if ($topic && $topic->getId()) {
                $breadCrumbs
                    ->addCrumb('topic', array(
                        'label' => $topic->getTitle(),
                        'title' => $topic->getTitle(),
                        'link'  => $topic->getUrl(),
                    ));
            }

            $breadCrumbs
                ->addCrumb('question', array(
                    'label' => $question->getTitle(),
                ));
        }

        if ($question->getMetaRobots()) {
            $this->getLayout()->getBlock('head')->setRobots($question->getMetaRobots());
        }

        $this->renderLayout();

    }
}
