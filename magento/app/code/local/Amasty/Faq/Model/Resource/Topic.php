<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_Faq
 */


class Amasty_Faq_Model_Resource_Topic extends Amasty_Faq_Model_Resource_Relational
{
    protected function _construct()
    {
        $this->_init('amfaq/topic', 'topic_id');
    }

    public function loadTopicItems()
    {
        $adapter = $this->_getReadAdapter();

        $select = $adapter
            ->select()
            ->from($this->getMainTable(), array('value' => 'topic_id', 'label' => 'title'));

        $items = $adapter->fetchAll($select);

        return $items;
    }

    protected function _afterLoad(Mage_Core_Model_Abstract $object)
    {
        $this->loadExternalIds($object, 'store');

        return parent::_afterLoad($object);
    }

    protected function _afterSave(Mage_Core_Model_Abstract $object)
    {
        $this->saveExternalIds($object, 'store');
    }

    public function getQuestionIds($topicId)
    {
        $adapter = $this->_getReadAdapter();

        $select = $adapter
            ->select()
            ->from($this->getTable('amfaq/question_topic'), array('question_id'))
            ->where('topic_id = ?', $topicId);

        return $adapter->fetchCol($select);
    }
}
