<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_Faq
 */

class Amasty_Faq_Model_Config_Source_Columns
{
    public function toOptionArray()
    {
        return array(
            array('value' => 1, 'label' => 1),
            array('value' => 2, 'label' => 2),
            array('value' => 3, 'label' => 3),
        );
    }
}
