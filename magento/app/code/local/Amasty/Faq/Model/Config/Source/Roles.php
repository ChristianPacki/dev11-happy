<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_Faq
 */

class Amasty_Faq_Model_Config_Source_Roles
{
    public function toOptionArray()
    {
        $vals = array(
            'public'    => Mage::helper('amfaq')->__('Anyone'),
            'auth'      => Mage::helper('amfaq')->__('Logged In Only'),
//            'purchased' => Mage::helper('amfaq')->__('If Product Purchased'),
        );

        $options = array();
        foreach ($vals as $k => $v)
            $options[] = array(
                'value' => $k,
                'label' => $v
            );

        return $options;
    }
}
