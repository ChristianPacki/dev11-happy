<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_Faq
 */

class Amasty_Faq_Model_Config_Source_Show
{
    public function toOptionArray()
    {
        return array(
            array('value' => 'collapsed', 'label' => Mage::helper('amfaq')->__('Collapsed Under Question')),
            array('value' => 'scroll', 'label' => Mage::helper('amfaq')->__('Scroll')),
            array('value' => 'separate', 'label' => Mage::helper('amfaq')->__('In Separate Page')),
        );
    }
}
