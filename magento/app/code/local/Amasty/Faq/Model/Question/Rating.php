<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_Faq
 */

class Amasty_Faq_Model_Question_Rating extends Mage_Core_Model_Abstract
{
    const TYPE_STARS = 0;
    const TYPE_YESNO = 1;

    protected function _construct()
    {
        $this->_init('amfaq/question_rating');
    }

    public function myVote($question_id, $session, $customer_id)
    {
        $this->_getResource()->myVote($this, $question_id, $session, $customer_id);
        $this->setOrigData();
        $this->_hasDataChanges = false;
        return $this;
    }

    public function getAllVoted($questionId)
    {
        return $this->_getResource()->getAllVoted($questionId);
    }
}