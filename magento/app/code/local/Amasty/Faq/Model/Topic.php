<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_Faq
 */


class Amasty_Faq_Model_Topic extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->_init('amfaq/topic');
    }

    public function getUrl()
    {
        if ($this->getUrlKey() != '')
            return Mage::getBaseUrl()
                .Mage::getStoreConfig('amfaq/general/url_prefix')
                .'/'.Mage::helper('amfaq')->getTopicPrefix()
                .$this->getUrlKey();


        return Mage::getBaseUrl() . Mage::getStoreConfig('amfaq/general/url_prefix') . '/topic/view/id/' . $this->getId();
    }

    public function getTags()
    {
        $tags = Mage::getResourceModel('amfaq/tag_collection');

        $tags->getSelect()
            ->join(
                array('qtag' => $this->_getResource()->getTable('amfaq/question_tag')),
                'qtag.tag_id = main_table.tag_id',
                array()
            )
            ->join(
                array('qtopic' => $this->_getResource()->getTable('amfaq/question_topic')),
                'qtopic.question_id = qtag.question_id',
                array()
            )
            ->where('qtopic.topic_id = ?', $this->getId())
            ->group('main_table.tag_id')
        ;

        return $tags;
    }

    public function getQuestionIds()
    {
        return $this->getResource()->getQuestionIds($this->getId());
    }
}
