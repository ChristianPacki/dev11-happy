<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_Faq
 */
class Amasty_Faq_Model_Observer
{
    protected $_customTabs = array('TM_EasyTabs_Block_Tabs');
    
    public function handleControllerFrontInitRouters($observer)
    {
        $observer->getEvent()->getFront()
            ->addRouter('amfaq', new Amasty_Faq_Controller_Router());
    }
    
    public function onCoreLayoutBlockCreateAfter($observer)
    {
        if (Mage::getStoreConfig('amfaq/product_page/display_tab')) {
            $block = $observer->getBlock();
            $blockClass = Mage::getConfig()->getBlockClassName('catalog/product_view_tabs');
            $tabs = $this->_customTabs;
            $tabs[] = $blockClass;
            if (in_array(get_class($block), $tabs)) {
                $title = Mage::helper('amfaq')->__('Product Questions');
                $block->addTab('questions', $title, 'amfaq/product_faq', 'amasty/amfaq/product_faq.phtml');
            }
        }
    }
    
    public function addMenuItem($observer)
    {
        if (Mage::getStoreConfig('amfaq/general/top_menu')) {
            $menu = $observer->getMenu();
            $tree = $menu->getTree();
            $node = new Varien_Data_Tree_Node(
                array(
                    'name' => Mage::getStoreConfig('amfaq/general/kb_title'),
                    'id'   => 'amfaq',
                    'url'  => Mage::helper('amfaq')->getFaqUrl(),
                ), 'id', $tree, $menu);
            $menu->addChild($node);
        }
    }
}
