<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_Faq
 */

class Amasty_Faq_Model_Tag extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->_init('amfaq/tag');
    }

    public function getUrl()
    {
        $code = $this->getAlias() ? $this->getAlias() : $this->getId();
        return Mage::helper('amfaq')->getFaqUrl() . 'tag/' . $code . '/';
    }

    public function generateAlias()
    {
        $title = $this->getTitle();

        if (preg_match('/^[\w\s-_]+$/', $title))
        {
            $alias = preg_replace('/[\s]+/', '-', $title);
            $this->setAlias($alias);
        }

        return $this;
    }
}
