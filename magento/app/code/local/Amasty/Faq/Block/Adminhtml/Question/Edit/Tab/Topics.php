<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_Faq
 */

class Amasty_Faq_Block_Adminhtml_Question_Edit_Tab_Topics extends Mage_Adminhtml_Block_Widget_Form
{

    protected function _prepareForm()
    {
        $model = Mage::registry('current_question');

        $topicModel = Mage::getResourceModel('amfaq/topic');

        $form = new Varien_Data_Form(array(
            'id' => 'edit_form',
            'action' => $this->getData('action'),
            'method' => 'post'
        ));


        $fieldset = $form->addFieldset('topics_fieldset',
            array('legend' => $this->__('Topics'))
        );

        $fieldset->addField('topic_ids', 'multiselect', array(
            'label'     => $this->__('Topics'),
            'name'      => 'topic_ids',
            'values'    => $topicModel->loadTopicItems(),
//            'value'     => $model->getTopicIds(),
        ));

        $form->addValues($model->getData());

        $this->setForm($form);
        return parent::_prepareForm();
    }
}
