<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_Faq
 */


class Amasty_Faq_Block_Adminhtml_Topic extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_blockGroup = 'amfaq';
        $this->_controller = 'adminhtml_topic';
        $this->_headerText = $this->__('Topics List');

        $this->_addButton('export_m2', array(
            'label' => Mage::helper('amfaq')->__('Export For M2'),
            'onclick' => 'setLocation(\'' . Mage::helper('adminhtml')->getUrl('adminhtml/amfaq_topic/export') . '\')',
            'class' => 'go'
        ));

        parent::__construct();
    }
}
