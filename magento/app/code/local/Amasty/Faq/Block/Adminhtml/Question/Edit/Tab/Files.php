<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_Faq
 */

class Amasty_Faq_Block_Adminhtml_Question_Edit_Tab_Files extends Mage_Core_Block_Template
{
    protected function _construct()
    {
        parent::_construct();
        $this->setTemplate('amasty/amfaq/files_tab.phtml');
    }
    public function getTabData()
    {
        $results = Mage::getModel('amfaq/file')->getTabData();
        return $results;
    }
}
