<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_Faq
 */

class Amasty_Faq_Block_Adminhtml_Question_Edit_Tab_Related extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('related_grid');
        $this->setDefaultSort('question_id');
        $this->setUseAjax(true);
        if ($this->_getQuestion()->getId()) {
            $this->setDefaultFilter(array('in_questions' => 1));
        }
    }

    protected function _getQuestion()
    {
        return Mage::registry('current_question');
    }

    protected function _addColumnFilterToCollection($column)
    {
        // Set custom filter for in product flag
        if ($column->getId() == 'in_questions') {
            $questionIds = $this->_getSelectedQuestions();
            if (empty($questionIds)) {
                $questionIds = 0;
            }
            if ($column->getFilter()->getValue()) {
                $this->getCollection()->addFieldToFilter('question_id', array('in' => $questionIds));
            } else {
                if($questionIds) {
                    $this->getCollection()->addFieldToFilter('question_id', array('nin' => $questionIds));
                }
            }
        } else {
            parent::_addColumnFilterToCollection($column);
        }
        return $this;
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getModel('amfaq/question')->getCollection();

        $question = $this->_getQuestion();
        if ($question->getId())
            $collection->addFieldToFilter('question_id', array('neq' => $question->getId()));

        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn('in_questions', array(
            'header_css_class'  => 'a-center',
            'type'              => 'checkbox',
            'name'              => 'in_questions',
            'values'            => $this->_getSelectedQuestions(),
            'align'             => 'center',
            'index'             => 'question_id'
        ));

        $this->addColumn('related_question_id', array(
            'header'    => Mage::helper('amfaq')->__('ID'),
            'sortable'  => true,
            'width'     => 60,
            'index'     => 'question_id'
        ));

        $this->addColumn('related_title', array(
            'header'    => Mage::helper('amfaq')->__('Title'),
            'index'     => 'title'
        ));

        return parent::_prepareColumns();
    }

    public function getGridUrl()
    {
        return $this->getData('grid_url')
            ? $this->getData('grid_url')
            : $this->getUrl('*/*/relatedQuestionsGrid', array('_current' => true));
    }

    protected function _getSelectedQuestions()
    {
        $questions = $this->getQuestionIds();
        if (!is_array($questions)) {
            $questions = $this->getSavedQuestions();
        }
        return $questions;
    }

    public function getSavedQuestions()
    {
        return Mage::registry('current_question')->getRelatedQuestions()->getAllIds();
    }

    public function getRowUrl($item)
    {
        return 'javascript:void(0)';
    }
}
