<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_Faq
 */

class Amasty_Faq_Block_Adminhtml_Topic_Edit_Tab_Main extends Mage_Adminhtml_Block_Widget_Form
{

    protected function _prepareForm()
    {
        $model = Mage::registry('current_topic');

        $form = new Varien_Data_Form(array(
            'id' => 'edit_form',
            'action' => $this->getData('action'),
            'method' => 'post'
        ));


        $fieldset = $form->addFieldset('common_fieldset',
            array('legend' => $this->__('Main Info'))
        );

        $fieldset->addField('title', 'text', array(
            'name'  => 'title',
            'label' => $this->__('Topic Title'),
            'title' => $this->__('Topic Title'),
            'required' => true,
        ));

        $fieldset->addField('url_key', 'text', array(
            'name'  => 'url_key',
            'label' => $this->__('Url Key'),
            'title' => $this->__('Url Key'),
            'required' => true,
        ));

        if (!Mage::app()->isSingleStoreMode()) {
            $field = $fieldset->addField('store_ids', 'multiselect', array(
                'name'      => 'store_ids',
                'label'     => Mage::helper('cms')->__('Store View'),
                'title'     => Mage::helper('cms')->__('Store View'),
                'required'  => true,
                'values'    => Mage::getSingleton('adminhtml/system_store')->getStoreValuesForForm(false, true),
            ));
            $renderer = $this->getLayout()->createBlock('adminhtml/store_switcher_form_renderer_fieldset_element');
            $field->setRenderer($renderer);
        }
        else {
            $fieldset->addField('store_id', 'hidden', array(
                'name'      => 'stores[]',
                'value'     => Mage::app()->getStore(true)->getId()
            ));
            $model->setStoreId(Mage::app()->getStore(true)->getId());
        }

        $fieldset->addField('position', 'text', array(
            'name'  => 'position',
            'label' => $this->__('Position'),
            'title' => $this->__('Position'),
        ));

        if ($model->getId()) {
            $fieldset->addField('topic_id', 'hidden', array(
                'name' => 'topic_id',
            ));
        }

        $form->addValues($model->getData());

        $this->setForm($form);
        return parent::_prepareForm();
    }
}
