<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_Faq
 */

class Amasty_Faq_Block_Adminhtml_Question_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        $this->_blockGroup = 'amfaq';
        $this->_objectId = 'id';
        $this->_controller = 'adminhtml_question';
        $this->_addButton('saveandcontinue', array(
            'label'     => Mage::helper('adminhtml')->__('Save and Continue Edit'),
            'onclick'   => 'saveAndContinueEdit(\''.$this->_getSaveAndContinueUrl().'\')',
            'class'     => 'save',
        ), -100);

        if (Mage::registry('current_question')->getEmail()) {
            $this->_addButton('saveandnotify', array(
                'label'     => $this->__('Save and Notify User'),
                'onclick'   => 'saveAndNotify(\''.$this->_getSaveAndNotifyUrl().'\')',
                'class'     => 'save',
            ), -100);
        }

        parent::__construct();
    }

    public function getHeaderText()
    {
        if (Mage::registry('current_question')->getId()) {
            return $this->htmlEscape(Mage::registry('current_question')->getTitle());
        }
        else {
            return Mage::helper('amfaq')->__('New Question');
        }
    }

    protected function _getSaveAndContinueUrl()
    {
        return $this->getUrl('*/*/save', array(
            '_current'   => true,
            'back'       => 'edit',
            'active_tab' => '{{tab_id}}',
            'notify'     => false,
        ));
    }

    protected function _getSaveAndNotifyUrl()
    {
        return $this->getUrl('*/*/save', array(
            '_current'   => true,
            'back'       => 'edit',
            'active_tab' => '{{tab_id}}',
            'notify'     => 1,
        ));
    }

    protected function _prepareLayout()
    {
        $tabsBlockJsObject = 'question_tabsJsTabs';
        $tabsBlockPrefix   = 'question_tabs_';

        $this->_formScripts[] = "
            Validation.add('validate-amfaq-rating','".$this->__('Rating must be numeric value between 1 and 5, or 0 if not rated')."',function(val){
                if (val == '' || val == 0 || (val <= 5 && val >= 1))
                {
                    return true;
                }
                return false;
            });
            function saveAndNotify(urlTemplate)
            {
                $$('input[name=status]')[0].value = 'answered';
                saveAndContinueEdit(urlTemplate)
            }
            function saveAndContinueEdit(urlTemplate) {
                var tabsIdValue = " . $tabsBlockJsObject . ".activeTab.id;
                var tabsBlockPrefix = '" . $tabsBlockPrefix . "';
                if (tabsIdValue.startsWith(tabsBlockPrefix)) {
                    tabsIdValue = tabsIdValue.substr(tabsBlockPrefix.length)
                }
                var template = new Template(urlTemplate, /(^|.|\\r|\\n)({{(\w+)}})/);
                var url = template.evaluate({tab_id:tabsIdValue});
                editForm.submit(url);
            }
        ";
        return parent::_prepareLayout();
    }

    public function getSaveUrl()
    {
        return $this->getUrl('*/*/save', array('_current'=>true, 'back'=>null));
    }
}