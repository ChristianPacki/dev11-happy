<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_Faq
 */

class Amasty_Faq_Block_Adminhtml_Question_Edit_Tab_Main extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        $model = Mage::registry('current_question');

        $form = new Varien_Data_Form(array(
            'id' => 'edit_form',
            'action' => $this->getData('action'),
            'method' => 'post'
        ));


        $fieldset = $form->addFieldset('common_fieldset',
            array('legend' => $this->__('Main Info'), 'class' => 'fieldset-wide')
        );

        if (!Mage::app()->isSingleStoreMode()) {
            $field = $fieldset->addField('store_ids', 'multiselect', array(
                'name'      => 'store_ids',
                'label'     => Mage::helper('cms')->__('Store View'),
                'title'     => Mage::helper('cms')->__('Store View'),
                'required'  => true,
                'values'    => Mage::getSingleton('adminhtml/system_store')->getStoreValuesForForm(false, true),
            ));
            $renderer = $this->getLayout()->createBlock('adminhtml/store_switcher_form_renderer_fieldset_element');
            $field->setRenderer($renderer);
        }
        else {
            $fieldset->addField('store_id', 'hidden', array(
                'name'      => 'stores[]',
                'value'     => Mage::app()->getStore(true)->getId()
            ));
            $model->setStoreId(Mage::app()->getStore(true)->getId());
        }

        $fieldset->addField('title', 'text', array(
            'name'  => 'title',
            'label' => $this->__('Question'),
            'title' => $this->__('Question'),
            'required' => true,
        ));

        $fieldset->addField('url_key', 'text', array(
            'name'  => 'url_key',
            'label' => $this->__('Url Key'),
            'title' => $this->__('Url Key'),
            'required' => true,
        ));

        $fieldset->addField('answer', 'editor', array(
            'name'  => 'answer',
            'label' => $this->__('Answer'),
            'title' => $this->__('Answer'),
            'config' => Mage::getSingleton('amfaq/wysiwygConfig')->getConfig(),
            'wysiwyg' => true,
            'required' => true,
        ));

        $fieldset->addField('from_store', 'select', array(
            'name'  => 'from_store',
            'label' => $this->__('Send From'),
            'title' => $this->__('Send From'),
            'values' => Mage::getSingleton('adminhtml/system_store')->getStoreValuesForForm(),
        ));

        $fieldset->addField('visibility', 'select', array(
            'name'  => 'visibility',
            'label' => $this->__('Visibility'),
            'title' => $this->__('Visibility'),
            'values' => Mage::helper('amfaq')->getVisibilities(),
            'required' => true,
        ));

        $fieldset->addField('position', 'text', array(
            'name'  => 'position',
            'label' => $this->__('Position'),
            'title' => $this->__('Position'),
        ));

        $fieldset->addField('tags', 'text', array(
            'name'  => 'tags',
            'label' => $this->__('Tags'),
            'title' => $this->__('Tags'),
        ));

        if(Mage::getStoreConfig('amfaq/rating/enabled'))
        {
            $fieldset->addField('calculated_rating', 'text', array(
                'name'  => 'rating',
                'label' => $this->__('Force Question Rating'),
                'title' => $this->__('Force Question Rating'),
                'class' => 'validate-amfaq-rating',
            ));

            $fieldset->addField('old_rating', 'hidden', array(
                'name'  => 'old_rating',
                'value' => $model->getCalculatedRating(),
            ));
        }

        if ($model->getId()) {
            $fieldset->addField('question_id', 'hidden', array(
                'name' => 'question_id',
            ));
        }

        $form->addValues($model->getData());

        $this->setForm($form);
        return parent::_prepareForm();
    }
}
