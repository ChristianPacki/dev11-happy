<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_Faq
 */

class Amasty_Faq_Block_Adminhtml_Topic_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{

    public function __construct()
    {
        parent::__construct();
        $this->setId('topic_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle($this->__('Topic Information'));
    }

    protected function _beforeToHtml()
    {
        $this->addTab('main', array(
            'label'     => $this->__('Main Info'),
            'title'     => $this->__('Main Info'),
            'content'   => $this->getLayout()->createBlock('amfaq/adminhtml_topic_edit_tab_main')->toHtml(),
            'active'    => true
        ));

        /*$this->addTab('sidebar', array(
            'label'     => $this->__('Sidebar'),
            'title'     => $this->__('Sidebar'),
            'content'   => $this->getLayout()->createBlock('amfaq/adminhtml_topic_edit_tab_sidebar')->toHtml(),
        ));*/

        $this->addTab('meta_tags', array(
            'label'     => $this->__('Meta Tags'),
            'title'     => $this->__('Meta Tags'),
            'content'   => $this->getLayout()->createBlock('amfaq/adminhtml_topic_edit_tab_meta')->toHtml(),
        ));

        return parent::_beforeToHtml();
    }

}