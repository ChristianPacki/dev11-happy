<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_Faq
 */

class Amasty_Faq_Block_Adminhtml_Question_Edit_Tab_Sender extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        $model = Mage::registry('current_question');

        $form = new Varien_Data_Form(array(
            'id' => 'edit_form',
            'action' => $this->getData('action'),
            'method' => 'post'
        ));


        $fieldset = $form->addFieldset('sender_fieldset',
            array('legend' => $this->__('Customer Info'), 'class' => 'fieldset-wide')
        );

         $fieldset->addField('name', 'text', array(
            'name'  => 'name',
            'label' => $this->__('Name'),
            'title' => $this->__('Name'),
        ));

        $fieldset->addField('email', 'text', array(
            'name'  => 'email',
            'label' => $this->__('Email'),
            'title' => $this->__('Email'),
        ));

        $fieldset->addField('status', 'hidden', array(
            'name'  => 'status',
        ));

        $form->addValues($model->getData());

        $this->setForm($form);
        return parent::_prepareForm();
    }
}
