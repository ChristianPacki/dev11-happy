<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_Reports
 */

class Amasty_Reports_Adminhtml_Amreports_ReportsController extends Mage_Adminhtml_Controller_Action
{
    public function ajaxAction()
    {
        switch($this->getRequest()->getParam('action')) {
            case 'getReport':
                $body = $this->getReport();
                break;
            case 'getTranslate':
                $body = $this->getTranslate();
                break;
            default:
                $body = '';
        }
        $this->getResponse()->setBody($body);
    }

    protected function getReport()
    {
        $model   = Mage::getModel('amreports/reports');
        $records = $model->getRecords($this->getRequest()->getParam('report_type'), $this->getRequest()->getParams());

        return Mage::helper('core')->jsonEncode($records);
    }

    protected function getTranslate()
    {
        $hlp = Mage::helper('amreports/translator');
        $translateArray = $hlp->loadModuleTranslation('Amasty_Reports');

        return Mage::helper('core')->jsonEncode($translateArray);
    }

    /**
     * Is current admin allowed to use this contrloller
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('report/amreports');
    }
}
