<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_Reports
 */

class Amasty_Reports_Model_Reports_Filters_Collection extends Amasty_Reports_Model_Reports_Abstract
{
    /**
     * Abstract
     *
     * @var array
     */
    protected $_allowedFilters = array();
    protected $_orderBy = '';

    /**
     * @param Mage_Sales_Model_Resource_Order_Collection $ordersCollection
     * @param array $fields
     *
     * @return Mage_Sales_Model_Resource_Order_Collection
     */
    protected function _addFieldsToSelect($ordersCollection, array $fields)
    {
        $ordersCollection->getSelect()
            ->reset(Zend_Db_Select::COLUMNS)
            ->columns($fields);

        return $ordersCollection;
    }

    /**
     * @param Mage_Sales_Model_Resource_Order_Collection $ordersCollection
     * @param string $value
     *
     * @return Mage_Sales_Model_Resource_Order_Collection
     */
    protected function _addDateTo($ordersCollection, $value)
    {
        if (!empty($value)) {
            $value = $this->_phpDateToMysqlTo($value);
            $ordersCollection->addFieldToFilter('main_table.' . $this->_orderBy, array('lteq' => $value));
        }

        return $ordersCollection;
    }

    /**
     * @param Mage_Sales_Model_Resource_Order_Collection $ordersCollection
     * @param string $value
     *
     * @return Mage_Sales_Model_Resource_Order_Collection
     */
    protected function _addDateFrom($ordersCollection , $value)
    {
        if (!empty($value)) {
            $value = $this->_phpDateToMysqlFrom($value);
            $ordersCollection->addFieldToFilter('main_table.' . $this->_orderBy, array('gteq' => $value));
        }

        return $ordersCollection;
    }

    /**
     * @param Mage_Sales_Model_Resource_Order_Collection $ordersCollection
     * @param array|string $value
     *
     * @return Mage_Sales_Model_Resource_Order_Collection
     */
    protected function _addStoreSelect($ordersCollection,  $value)
    {
        if (!empty($value)) {
            if (!is_array($value)) {
                $value = explode(',', $value);
            }
            $ordersCollection->addFieldToFilter('main_table.store_id', array('in' => $value));
        }

        return $ordersCollection;
    }

    /**
     * @param Mage_Sales_Model_Resource_Order_Collection $ordersCollection
     * @param array|string $value
     *
     * @return Mage_Sales_Model_Resource_Order_Collection
     */
    protected function _addOrderStatus($ordersCollection, $value)
    {
        if (!empty($value)) {
            if (!is_array($value)) {
                $value = explode(',', $value);
            }
            $ordersCollection->addFieldToFilter('main_table.status', array('in' => $value));
        }
        return $ordersCollection;
    }

    /**
     * @param Mage_Sales_Model_Resource_Order_Collection $ordersCollection
     * @param string $value
     *
     * @return Mage_Sales_Model_Resource_Order_Collection
     */
    protected function _addOrdersBy($ordersCollection, $value)
    {
        switch ($value) {
            case 'created_at':
                $ordersCollection->getSelect()->order('main_table.created_at');
                $this->_orderBy = 'created_at';
                break;
            case 'updated_at':
                $ordersCollection->getSelect()->order('main_table.updated_at');
                $this->_orderBy = 'updated_at';
                break;
        }
        return $ordersCollection;
    }

    /**
     * @param Mage_Sales_Model_Resource_Order_Collection $ordersCollection
     * @param string $value
     *
     * @return Mage_Sales_Model_Resource_Order_Collection
     */
    protected function _addPeriod($ordersCollection, $value = null)
    {
        $ordersCollection->getSelect()->group('period');
        return $ordersCollection;
    }

    protected function _addInvoiced($ordersCollection , $value)
    {
        //$ordersCollection->addFieldToFilter( 'main_table.base_total_invoiced' , array('gteq' => $value));
        return $ordersCollection;
    }

    protected function _applyFilters($ordersCollection, $filters)
    {
        foreach ($filters as $name => $filter) {
            if (in_array($name, $this->_allowedFilters) && !empty($filter)) {
                $funcName = '_add' . ucfirst($name);
                $this->$funcName($ordersCollection, $filter);
            }
        }
        $this->_addInvoiced($ordersCollection, 1);

        return $ordersCollection;
    }

    /**
     * Columns of Report
     *
     * @param array $filters
     *
     * @return array
     */
    protected function _getSelectedFields($filters)
    {
        return parent::_getSelectedFields($filters);
    }

    /**
     * @param array $filter
     *
     * @return array
     */
    public function getReport($filter)
    {
        return parent::getReport($filter);
    }

    public function getReportFields()
    {
        return parent::getReportFields();
    }
}