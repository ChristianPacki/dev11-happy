<?php
class Aitoc_Aitcg_Helper_Category extends Aitoc_Aitcg_Helper_Abstract
{
    public function getPredefinedCatsOptionHtml($ids = null)
    {
        $model = Mage::getModel('aitcg/category');
        $collection = $model->getCollection();
        $return = '';
        if ($ids !== null) {
            $collection->addFieldToFilter('category_id', array('in' => explode(',' ,$ids)) );
        }

        foreach ($collection->load() as $category) {
            $return .= '\'<option value="' . $category->getCategoryId() . '">' . htmlentities($category->getName(), ENT_QUOTES) . '</option>\'+' . "\r\n";
        }
        
        return $return;
    }
    
    public function getCategoryImagesRadio($category_id, $rand)
    {
            $imageCollection = Mage::getModel('aitcg/category_image')->getCollection()
                    ->addFieldToFilter('category_id', $category_id)
                    ->addFieldToFilter('filename', array('neq' => ''));
            $return = '';
            foreach($imageCollection->load() as $image)
            {
                $return .= '<div><input type="radio" value="'.$image->getCategoryImageId().'" name="predefined-image'.$rand.'" />'.
                        '<img src="'.$image->getImagesUrl().'preview/'.$image->getFilename().'" /></div>';
            }
    
            return $return;
    }
    
    public function copyPredefinedImage($id)
    {
        $path = Mage::getBaseDir('media') . DS . 'custom_product_preview' . DS . 'quote' . DS;
        $image = Mage::getModel('aitcg/category_image')->load($id);
        
        $fileName = $image->getFilename();
        $fileNameExploded = explode('.',$fileName);
        $ext = '.'.array_pop($fileNameExploded);
        
        $filename = Mage::helper('aitcg')->uniqueFilename($ext);
        while(file_exists($path.$filename))
        {
            $filename = Mage::helper('aitcg')->uniqueFilename($ext);
        }
        
        @copy($image->getImagesPath().$image->getFilename(),$path.$filename);
        return $filename;
    }
}