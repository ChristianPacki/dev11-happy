<?php

class Aitoc_Aitcg_Helper_Image extends Mage_Catalog_Helper_Image
{
    protected $baseDir = '';

    public function init(Mage_Catalog_Model_Product $product, $attributeName, $imageFile = null)
    {
        return $this->load($attributeName, $imageFile);
    }

    public function loadLite($imageFile, $baseDir = 'catalog/product_media_config')
    {
        if ($baseDir) {
            if ($baseDir == 'media') {
                $this->setBaseDir(Mage::getBaseDir('media'));
            } elseif ($baseDir == 'adjconfigurable') {//compatibility with VYA extension
                $this->setBaseDir(Mage::getBaseDir('media') . DS . $baseDir);
            } else {
                $this->setBaseDir(Mage::getSingleton($baseDir)->getBaseMediaPath());
            }
        }

        return $this->load(Aitoc_Aitcg_Helper_Options::OPTION_TYPE_AITCUSTOMER_IMAGE, $imageFile);
    }

    public function load($attributeName, $imageFile)
    {
        $this->_reset();
        $this->_setModel(Mage::getModel('aitcg/product_image'));
        $this->_getModel()->setDestinationSubdir($attributeName);
        $this->setWatermark(Mage::getStoreConfig("design/watermark/{$this->_getModel()->getDestinationSubdir()}_image"));
        if (version_compare(Mage::getVersion(), '1.4.0.0', '>=')) {
            $this->setWatermarkImageOpacity(Mage::getStoreConfig("design/watermark/{$this->_getModel()->getDestinationSubdir()}_imageOpacity"));
        }
        $this->setWatermarkPosition(Mage::getStoreConfig("design/watermark/{$this->_getModel()->getDestinationSubdir()}_position"));
        $this->setWatermarkSize(Mage::getStoreConfig("design/watermark/{$this->_getModel()->getDestinationSubdir()}_size"));
        $this->setImageFile($imageFile);

        return $this;
    }

    public function getBaseDir()
    {
        return $this->_baseDir;
    }

    public function setBaseDir($baseDir)
    {
        $this->_baseDir = $baseDir;
    }

    public function __toString()
    {
        try {
            if ($this->getImageFile()) {
                $this->_getModel()->setBaseFile($this->getImageFile(), $this->getBaseDir());
            } else {
                $this->_getModel()->setBaseFile(
                    $this->getProduct()->getData(
                        $this->_getModel()
                            ->getDestinationSubdir()
                    )
                );
            }
            if ($this->_getModel()->isCached()) {
                return $this->_getModel()->getUrl();
            } else {
                if ($this->_scheduleResize) {
                    $this->_getModel()->resize();
                }
                if ($this->_scheduleRotate) {
                    $this->_getModel()->rotate($this->getAngle());
                }
                if ($this->getWatermark()) {
                    $this->_getModel()->setWatermark($this->getWatermark());
                }
                $url = $this->_getModel()->saveFile()->getUrl();
            }
        } catch (Exception $e) {
            $url = Mage::getDesign()->getSkinUrl($this->getPlaceholder());
        }

        return $url;
    }

    public function getNewFile()
    {
        return $this->_getModel()->getNewFile();
    }

    public function getOriginalSize()
    {
        return $this->_getModel()->calcOriginalSize();
    }

    public function setBlackWhiteImage($url, $filename)
    {
        $path  = Mage::getBaseDir('media') . DS . 'custom_product_preview' . DS . 'quote' . DS;
        $image = $this->_getImg($url);
        $srcW  = imagesx($image);
        $srcH  = imagesy($image);
        for ($x = 0; $x < $srcW; $x++) {
            for ($y = 0; $y < $srcH; $y++) {
                $srcColor = imagecolorsforindex($image, imagecolorat($image, $x, $y));
                $r        = $srcColor['red'];
                $g        = $srcColor['green'];
                $b        = $srcColor['blue'];
                $pixel    = ($r + $g + $b) / 3;
                $srcColor = imagecolorallocatealpha(
                    $image, $pixel, $pixel, $pixel, $srcColor['alpha']
                );
                imagesetpixel($image, $x, $y, $srcColor);
            }
        }
        imagepng($image, $path . $filename);

        return true;
    }

    public function _getImg($file)
    {
        $extension = strrchr($file, '.');
        $extension = strtolower($extension);
        switch ($extension) {
            case '.jpg':
            case '.jpeg':
                $im = @imagecreatefromjpeg($file);
                break;
            case '.gif':
                $im = @imagecreatefromgif($file);
                break;
            case '.png':
                $im = @imagecreatefrompng($file);
                imageAlphaBlending($im, true);
                imageSaveAlpha($im, true);
                break;
            default:
                $im = false;
                break;
        }

        return $im;
    }
}