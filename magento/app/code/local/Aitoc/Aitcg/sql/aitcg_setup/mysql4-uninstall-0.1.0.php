<?php

$installer = $this;

$installer->startSetup();

$installer->run("
UPDATE {$this->getTable('catalog/product_option')} SET `type` = 'file' WHERE `type`='aitcustomer_image';
");

$installer->endSetup(); 