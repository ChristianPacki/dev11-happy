<?php
class Aitoc_Aitcg_Block_Catalog_Product_View_Options_Type_File extends Mage_Catalog_Block_Product_View_Options_Type_File 
{
    protected function _beforeToHtml()
    {
        $option = $this->getOption();
        $info = $this->getProduct()->getPreconfiguredValues();
        
        if($info) {
            $info = $info->getData('options/' . $option->getId());
        }

        $template_id = 0;
        if(isset($info['aitcg_data']) && $info['aitcg_data']['template_id'] > 0 ) {
            $template_id = $info['aitcg_data']['template_id'];
        } else {
            $sessionData = Mage::getSingleton('aitcg/session')
                ->getData('options_' . $option->getId() . '_file');
            $template_id = intval($sessionData['template_id']);
        }
        
        $data = $this->_getDataTemplate('image', $option, $template_id);
       
        $this->setImage($data["image"]);
        $this->setPreview($data['preview']);
        
        return parent::_beforeToHtml();
    }
    
    protected function _getDataTemplate($model, $option, $template_id)
    {
        $data = array('image' => false, 'preview' => false); 
        $model = Mage::getModel('aitcg/'.$model);
        if($template_id > 0) {
            $model->load( $template_id );
            if($model->hasData()) {
                $data['preview'] = new Varien_Object($model->getFullData( ));
                if($data['preview']['id'] == 0) {
                    $data['preview'] = false;
                }
            }
        }

        $productId  = $option->getProductId();
        $templateId = $option->getImageTemplateId();
        $position   = Mage::getStoreConfig('catalog/aitcg/aitcg_editor_position');
        
        $data["image"] = $model->getMediaImage( $productId, $templateId, $position );
         
        return $data;
        
    }
    
    public function getColorset()
    {
        $id = $this->getOption()->getColorSetId();
        $colorsetModel = Mage::getModel('aitcg/font_color_set');
        $hasId = $colorsetModel->hasId($id);
        if($hasId)
        {
            $colorsetModel  = $colorsetModel->load($id);
            $status = $colorsetModel->getStatus();
            if($status !== '0')
            {
                return $colorsetModel;
            }
        }
        return $colorsetModel->load(Aitoc_Aitcg_Helper_Font_Color_Set::XPATH_CONFIG_AITCG_FONT_COLOR_SET_DFLT);
    }
    
    public function getAllowPredefinedColors()
    {
        $value = $this->getOption()->getAllowPredefinedColors();
        if($value == null)
        {
            return Mage::getStoreConfig('catalog/aitcg/aitcg_font_color_predefine');
        }
        return $value;
    }

    public function getAllowPlaceBehind()
    {
        //TODO: Realize "allow_place_behind" in aitcg product option
        $optValue = $this->getOption()->getAllowPlaceBehind();

        if ($optValue == null) {
            return Mage::getStoreConfig('catalog/aitcg/aitcg_allow_place_behind');
        }

        return $optValue;
    }
    
    public function isObjectDistortionAllowed()
    {
        $optValue = $this->getOption()->getAllowTextDistortion();
        
        if ($optValue == null) {
            $optValue = Mage::getStoreConfig('catalog/aitcg/aitcg_allow_text_distortion'); 
        }
        
        return $optValue;
    }

    public function canEmailToFriend()
    {
        $sendToFriendModel = Mage::registry('send_to_friend_model');
        return $sendToFriendModel && $sendToFriendModel->canEmailToFriend();
    }

    public function getSavePdfUrl($front = 'aitcg')
    {
        if (Mage::app()->getStore()->getConfig('catalog/aitcg/aitcg_enable_svg_to_pdf') == 1)
        {
            return Mage::getUrl($front.'/index/pdf');
        }
        return false;
    }

    public function isMageGtEq19()
    {
        return Mage::helper('aitoc_common')->isMageGtEq19();
    }  
}
