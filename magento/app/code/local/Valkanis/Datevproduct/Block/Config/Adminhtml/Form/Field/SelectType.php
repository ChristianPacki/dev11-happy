<?php

class Valkanis_Datevproduct_Block_Config_Adminhtml_Form_Field_SelectType extends Mage_Core_Block_Html_Select {

    public function __construct(array $args = array()) {
        parent::__construct($args);
        if (isset($args['selectOptions'])) {
            foreach ($args['selectOptions'] as $value => $label) {
                $this->addOption($value, $label);
            }
        }
        if (isset($args['extraParams'])) {
            $this->setExtraParams($args['extraParams']);
        }
    }

//    public function _toHtml() {
//        return parent::_toHtml();
//    }

    public function setInputName($value) {
        return $this->setName($value);
    }

}
