<?php

class Valkanis_Datevproduct_Block_Config_InvoiceCsvData extends Mage_Adminhtml_Block_System_Config_Form_Field_Array_Abstract {

    protected $_typeRenderer;
    protected $_renderRenderer;

    public function _prepareToRender() {
        # Creating renderes
        $this->_renderRenderer = $this->_createDropdownRender(array(
            'selectOptions' => $this->getOptionsRender(),
            'extraParams' => 'style="width:150px"'
        ));
        $this->_typeRenderer = $this->_createDropdownRender(array(
            'selectOptions' => $this->getOptionsType(),
            'extraParams' => 'style="width:100px"'
        ));
        # Adding Columns
        $this->addColumn('label', array(
            'label' => Mage::helper('datevproduct')->__('Label'),
            'type' => 'text',
            'style' => 'width:140px',
        ));
        $this->addColumn('render', array(
            'label' => Mage::helper('datevproduct')->__('Render'),
            'renderer' => $this->_renderRenderer,
        ));
        $this->addColumn('type', array(
            'label' => Mage::helper('datevproduct')->__('Type'),
            'renderer' => $this->_typeRenderer,
        ));

        $this->addColumn('value', array(
            'label' => Mage::helper('datevproduct')->__('Value'),
            'type' => 'textarea',
            'style' => 'width:100px',
        ));

        $this->_addAfter = false;
        $this->_addButtonLabel = Mage::helper('datevproduct')->__('Add');
    }

    /**
     * setting Valkanis_Datevproduct_Block_Config_Adminhtml_Form_Field_Country renderer
     * @return type
     */
    protected function _createDropdownRender($options) {
        return $this->getLayout()->createBlock('datevproduct/config_adminhtml_form_field_selectType', '', array_merge(array('is_render_to_js_template' => true), $options));
    }

    /** Setting Default Values To table */
    protected function _getElementHtml(Varien_Data_Form_Element_Abstract $element) {
        $this->setElement($element);

        // Workaround for Magento bug within 1.9.3.x
        // [Default configuration value for serialized_array settings is ignored](http://magento.stackexchange.com/questions/146978)
        if (!$this->getElement()->getData('value')) {
            $this->getElement()->setData('value', $this->_getDefaultOptions());
        }
        $html = $this->_toHtml();
        $this->_arrayRowsCache = null; // doh, the object is used as singleton!
        return $html;
    }

    /**
     * Type Options 
     */
    protected function getOptionsType() {
        return array(
            'string' => Mage::helper('datevproduct')->__('String'),
            'number' => Mage::helper('datevproduct')->__('Number'),
        );
    }

    /**
     * Render Options 
     */
    protected function getOptionsRender() {
        return array(
            'text' => Mage::helper('datevproduct')->__('Text'),
            'rowTotalWithTax' => Mage::helper('datevproduct')->__('Row Total With Tax'),
        );
    }

    /**
     *  Default Data For invoice 
     */
    protected function _getDefaultOptions() {
        return array(
            0 => array(
                'label' => 'Umsatz',
                'render' => 'rowTotalWithTax',
                'type' => 'number'
            ),
            1 => array(
                'label' => 'Soll/Haben-Kennzeichen',
                'render' => 'text',
                'type' => 'string',
                'value' => 'S'
            ),
            2 => array(
                'label' => 'WKZ Umsatz',
                'render' => 'string',
                'type' => 'string'
            ),
            3 => array(
                'label' => 'Kurs',
                'render' => '',
                'type' => 'number'
            ),
            4 => array(
                'label' => 'Basis-Umsatz',
                'render' => '',
                'type' => 'number'
            ),
            5 => array(
                'label' => 'WKZ Basis-Umsatz',
                'render' => '',
                'type' => 'string'
            ),
            6 => array(
                'label' => 'Konto',
                'render' => '',
                'type' => 'number'
            ),
            7 => array(
                'label' => 'Gegenkonto (ohne BU-Schlüssel)',
                'render' => '',
                'type' => 'number'
            ),
            8 => array(
                'label' => 'BU-Schlüssel',
                'render' => '',
                'type' => 'string'
            ),
            9 => array(
                'label' => 'Belegdatum',
                'render' => '',
                'type' => 'string'
            ),
            10 => array(
                'label' => 'Belegfeld 1',
                'render' => '',
                'type' => 'number'
            ),
            11 => array(
                'label' => 'Belegfeld 2',
                'render' => '',
                'type' => 'string'
            ),
            12 => array(
                'label' => 'Skonto',
                'render' => '',
                'type' => 'number'
            ),
            13 => array(
                'label' => 'Buchungstext',
                'render' => '',
                'type' => 'string'
            ),
            14 => array(
                'label' => 'Postensperre',
                'render' => '',
                'type' => 'number'
            ),
            15 => array(
                'label' => 'Diverse Adressnummer',
                'render' => '',
                'type' => 'string'
            ),
            16 => array(
                'label' => 'Geschäftspartnerbank',
                'render' => '',
                'type' => 'number'
            ),
            17 => array(
                'label' => 'Sachverhalt',
                'render' => '',
                'type' => 'number'
            ),
            18 => array(
                'label' => 'Zinssperre',
                'render' => '',
                'type' => 'number'
            ),
            19 => array(
                'label' => 'Beleglink',
                'render' => '',
                'type' => 'string'
            ),
            20 => array(
                'label' => 'Beleginfo - Art 1',
                'render' => '',
                'type' => 'string'
            ),
            21 => array(
                'label' => 'Beleginfo - Inhalt 1',
                'render' => '',
                'type' => 'string'
            ),
            22 => array(
                'label' => 'Beleginfo - Art 2',
                'render' => '',
                'type' => 'string'
            ),
            23 => array(
                'label' => 'Beleginfo - Inhalt 2',
                'render' => '',
                'type' => 'string'
            ),
            24 => array(
                'label' => 'Beleginfo - Art 3',
                'render' => '',
                'type' => 'string'
            ),
            25 => array(
                'label' => 'Beleginfo - Inhalt 3',
                'render' => '',
                'type' => 'string'
            ),
            26 => array(
                'label' => 'Beleginfo - Art 4',
                'render' => '',
                'type' => 'string'
            ),
            27 => array(
                'label' => 'Beleginfo - Inhalt 4',
                'render' => '',
                'type' => 'string'
            ),
            28 => array(
                'label' => 'Beleginfo - Art 5',
                'render' => '',
                'type' => 'string'
            ),
            29 => array(
                'label' => 'Beleginfo - Inhalt 5',
                'render' => '',
                'type' => 'string'
            ),
            30 => array(
                'label' => 'Beleginfo - Art 6',
                'render' => '',
                'type' => 'string'
            ),
            31 => array(
                'label' => 'Beleginfo - Inhalt 6',
                'render' => '',
                'type' => 'string'
            ),
            32 => array(
                'label' => 'Beleginfo - Art 7',
                'render' => '',
                'type' => 'string'
            ),
            33 => array(
                'label' => 'Beleginfo - Inhalt 7',
                'render' => '',
                'type' => 'string'
            ),
            34 => array(
                'label' => 'Beleginfo - Art 8',
                'render' => '',
                'type' => 'string'
            ),
            35 => array(
                'label' => 'Beleginfo - Inhalt 8',
                'render' => '',
                'type' => 'string'
            ),
            36 => array(
                'label' => 'KOST1 - Kostenstelle',
                'render' => '',
                'type' => 'string'
            ),
            37 => array(
                'label' => 'KOST2 - Kostenstelle',
                'render' => '',
                'type' => 'string'
            ),
            38 => array(
                'label' => 'Kost-Menge',
                'render' => '',
                'type' => 'number'
            ),
            39 => array(
                'label' => 'EU-Land u. UStID',
                'render' => '',
                'type' => 'string'
            ),
        );
        //1;"2";"3";4;5;"6";7;8;"9";10;"11";"12";13;"14";15;"16";17;18;19;"20";"21";"22";"23";"24";"25";"26";"27";"28";"29";"30";"31";"32";"33";"34";"35";"36";"37";"38";39;"40";41;"42";43;44;45;46;47;"48";"49";"50";"51";"52";"53";"54";"55";"56";"57";"58";"59";"60";"61";"62";"63";"64";"65";"66";"67";"68";"69";"70";"71";"72";"73";"74";"75";"76";"77";"78";"79";"80";"81";"82";"83";"84";"85";"86";"87";88;89;90;"91";92;93;94;"95";"96";97;"98";99;100;101;"102";"103";104;"105";106;"107";108;"109";"110";111;"112";113;114;115;
    }

    /**
     *  This functions make selected dropdown
     * @param Varien_Object $row
     */
    protected function _prepareArrayRow(Varien_Object $row) {

        $row->setData('option_extra_attr_' . $this->_typeRenderer->calcOptionHash($row->getData('type')), 'selected="selected"');
        $row->setData('option_extra_attr_' . $this->_renderRenderer->calcOptionHash($row->getData('render')), 'selected="selected"');
    }

}

//
//class Valkanis_Datevproduct_Block_Config_ShippingCosts extends Mage_Adminhtml_Block_System_Config_Form_Field_Array_Abstract {
//
//    protected $_itemRenderer;
//
//    public function _prepareToRender() {
//        $this->addColumn('country_id', array(
//            'label' => Mage::helper('datevproduct')->__('Label'),
//            'renderer' => function() {
//                return '';
//            }
//                ,
//        ));
//        $this->addColumn('from_price', array(
//            'label' => Mage::helper('datevproduct')->__('Type'),
//            'style' => 'width:100px',
//        ));
//        $this->addColumn('cost', array(
//            'label' => Mage::helper('datevproduct')->__('Value'),
//            'style' => 'width:100px',
//        ));
//
//        $this->_addAfter = false;
//        $this->_addButtonLabel = 'asdasd';
////        $this->_addButtonLabel = Mage::helper('datev')->__('Add');
//    }
//
//    public function getTemplate() {
//        return 'datevproduct/config/table.phtml';
////                  $this->setTemplate('system/config/form/field/array.phtml');
//    }
//
//    protected function _getRenderer() {
//        if (!$this->_itemRenderer) {
//            $this->_itemRenderer = $this->getLayout()->createBlock(
//                    'datevproduct/config_adminhtml_form_field_country', '', array('is_render_to_js_template' => true)
//            );
//        }
//        return $this->_itemRenderer;
//    }
//


//
//}

    