$( document ).ready(function() {
		$('.carousel').carousel({
   			 interval: false
		}); 
		
		$('#menu').affix({
			  offset: {
				top: $('header').height()
			  }
		});	
			
		
		if ($('#back-to-top').length) {
			var scrollTrigger = 100, // px
				backToTop = function () {
					var scrollTop = $(window).scrollTop();
					if (scrollTop > scrollTrigger) {
						$('#back-to-top').addClass('show');
					} else {
						$('#back-to-top').removeClass('show');
					}
				};
			backToTop();
			$(window).on('scroll', function () {
				backToTop();
			});
			$('#back-to-top').on('click', function (e) {
				e.preventDefault();
				$('html,body').animate({
					scrollTop: 0
				}, 700);
			});
		}
		
		$('.scroll').click(function(event) {
				
			event.preventDefault();
		 
			var full_url = this.href,
				parts = full_url.split('#'),
				trgt = parts[1],
				target_offset = $('#'+trgt).offset(),
				target_top = target_offset.top;
				
			$('html, body').animate({scrollTop:target_top}, 600);
			
			$(this).parent().addClass('active');
		});	
		
		/* Every time the window is scrolled ... */
			$(window).scroll( function(){
			
				/* Check the location of each desired element */
				$('.fadeInBlock').each( function(i){
					
					var bottom_of_object = $(this).offset().top + $(this).outerHeight();
					var bottom_of_window = $(window).scrollTop() + $(window).height();
					
					/* If the object is completely visible in the window, fade it it */
					if( bottom_of_window > bottom_of_object ){
						
						$(this).animate({'opacity':'1'},500);
						
						
							
					}
					
				}); 
			
			});
		
		jQuery(document).ready(function($){
			var timelineBlocks = $('.cd-timeline-block'),
				offset = 0.8;
		
			//hide timeline blocks which are outside the viewport
			hideBlocks(timelineBlocks, offset);
		
			//on scolling, show/animate timeline blocks when enter the viewport
			$(window).on('scroll', function(){
				(!window.requestAnimationFrame) 
					? setTimeout(function(){ showBlocks(timelineBlocks, offset); }, 100)
					: window.requestAnimationFrame(function(){ showBlocks(timelineBlocks, offset); });
			});
		
			function hideBlocks(blocks, offset) {
				blocks.each(function(){
					( $(this).offset().top > $(window).scrollTop()+$(window).height()*offset ) && $(this).find('.cd-timeline-img, .cd-timeline-content').addClass('is-hidden');
				});
			}
		
			function showBlocks(blocks, offset) {
				blocks.each(function(){
					( $(this).offset().top <= $(window).scrollTop()+$(window).height()*offset && $(this).find('.cd-timeline-img').hasClass('is-hidden') ) && $(this).find('.cd-timeline-img, .cd-timeline-content').removeClass('is-hidden').addClass('bounce-in');
				});
			}
		});
		
		$('#messagebox').delay(5000).fadeIn("slow");
		$( ".closer" ).click(function() {
		  $( "#messagebox" ).hide( "slow");
		});
});