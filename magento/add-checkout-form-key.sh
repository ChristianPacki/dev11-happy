find -L app/design/frontend -name 'shipping.phtml' -or -name 'billing.phtml' -or -name 'shipping_method.phtml' -or -name 'payment.phtml' -or -name 'addresses.phtml' \
      | xargs grep -L formkey \
      | xargs perl -i -pe 's/<\/form>/<?php echo \$this->getBlockHtml("formkey") ?>\n<\/form>/g'

find -L skin/frontend -name 'opcheckout.js' \
      | xargs grep -L form_key \
      | xargs perl -i -pe 's/if \(elements\[i\]\.name=='\''payment\[method\]'\''\) \{/if (elements[i].name=='\''payment[method]'\'' || elements[i].name == '\''form_key'\'') {/g'

find -L js -name 'payment.js' \
      | xargs grep -L form_key \
      | xargs perl -i -pe 's/if \(elements\[i\]\.name=='\''payment\[method\]'\''\) \{/if (elements[i].name=='\''payment[method]'\'' || elements[i].name == '\''form_key'\'') {/g'