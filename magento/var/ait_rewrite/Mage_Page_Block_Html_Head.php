<?php
/* DO NOT MODIFY THIS FILE! THIS IS TEMPORARY FILE AND WILL BE RE-GENERATED AS SOON AS CACHE CLEARED. */

/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Page
 * @copyright   Copyright (c) 2014 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Html page block
 *
 * @category   Mage
 * @package    Mage_Page
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Aitoc_Aitcg_Block_Rewrite_Page_Html_Head extends Mage_Page_Block_Html_Head
{
    /**
     * Initialize template
     *
     */
    protected function _construct()
    {
        parent::_construct();
    	if(Mage::helper('ves_tempcp/theme')->isVenusTheme()) {
        	$this->setTemplate('venustheme/tempcp/head.phtml');
        }
    }
}



class Ves_Tempcp_Block_Html_Head extends Aitoc_Aitcg_Block_Rewrite_Page_Html_Head 
{
    public function aitocAddPositionedItem($aItem, $aPosition)
    {
        if(array_key_exists(key($aItem), $this->getItems()))
        {
            return false;
        }
        
        if(array_key_exists($aPosition['item'], $this->getItems()))
        {
            $insertion = 0;
            foreach($this->getItems()as $key=>$item) {
                if($key===$aPosition['item'])
                {
                    break;
                }
                $insertion++;
            }
            
            switch($aPosition['place'])
            {
                case 'after':
                    $insertion = $insertion + 1;
                    break;
                case 'before':
                    break;
                default :
                    return false;      
            }
            //$this->_hasDataChanges = true;
            //insert in to elements
            $arrayHead = array_slice($this->getItems(),0,$insertion);
            $arrayTail = array_slice($this->getItems(),$insertion);
            $this->unsetData('items');
            $this->setItems(array_merge($arrayHead,$aItem,$arrayTail));
            return;
        }
        return  false;
    }
}

