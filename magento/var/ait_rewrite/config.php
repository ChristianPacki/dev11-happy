<?php
$rewriteConfig = array(
	'Ves_BlockBuilder_Block_Product_View' => 'Mage_Catalog_Block_Product_View',
	'Aitoc_Aitcg_Block_Rewrite_Catalog_Product_View' => 'Mage_Catalog_Block_Product_View',
	'DerModPro_BasePrice_Block_Catalog_Product_View' => 'Mage_Catalog_Block_Product_View',
	'Ves_Tempcp_Block_Html_Head' => 'Mage_Page_Block_Html_Head',
	'Aitoc_Aitcg_Block_Rewrite_Page_Html_Head' => 'Mage_Page_Block_Html_Head',
	'Mirasvit_Rewards_Model_Rewrite_Sendfriend' => 'Mage_Sendfriend_Model_Sendfriend',
	'Aitoc_Aitcg_Model_Rewrite_Sendfriend_Sendfriend' => 'Mage_Sendfriend_Model_Sendfriend'
);
