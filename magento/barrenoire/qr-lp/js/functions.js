$( document ).ready(function() {
		$('.carousel').carousel({
   			 interval: false
		}); 
		
			
		
		if ($('#back-to-top').length) {
			var scrollTrigger = 100, // px
				backToTop = function () {
					var scrollTop = $(window).scrollTop();
					if (scrollTop > scrollTrigger) {
						$('#back-to-top').addClass('show');
					} else {
						$('#back-to-top').removeClass('show');
					}
				};
			backToTop();
			$(window).on('scroll', function () {
				backToTop();
			});
			$('#back-to-top').on('click', function (e) {
				e.preventDefault();
				$('html,body').animate({
					scrollTop: 0
				}, 700);
			});
		}
		
		$('#show-video').click(function(){
			$('.overlay').addClass('hider');
			$('.slide-container-right').addClass('visible');
		});
		
		$('.slide-container-right').click(function(){					
			$('.slide-container-right').removeClass('visible');
			$('.slide-container-left').addClass('visible');
		});
		$('.slide-container-left').click(function(){					
			$('.slide-container-left').removeClass('visible');
			$('.slide-container-right').addClass('visible');
		});
		
		
		
});