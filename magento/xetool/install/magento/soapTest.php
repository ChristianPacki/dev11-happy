<?php
echo (extension_loaded('soap')) ? 'SOAP Enabled' : 'SOAP Disabled';
echo '<br />';
echo (class_exists('SOAPClient')) ? 'SOAP Client Present' : 'SOAP Client Absent';
echo '<br />';
echo (class_exists('SOAPServer')) ? 'SOAP Server Present' : 'SOAP Server Absent';
echo '<br />';

$apiUrl = 'http://your-site/index.php/api/soap?wsdl'; //'http://your-site/index.php/api/v2_soap/?wsdl=1';
$apiUser = 'soap_userName';
$apiKey = 'soap_apiKey';

ini_set("soap.wsdl_cache_enabled", "0");
try {
    $client = new SoapClient($apiUrl, array('cache_wsdl' => WSDL_CACHE_NONE));
} catch (SoapFault $e) {
    echo 'Error in Soap Connection : ' . $e->getMessage();
}
try {
    $session = $client->login($apiUser, $apiKey);
    if ($session) {
        echo 'SOAP Connection Successful.';
    } else {
        echo 'SOAP Connection Failed.';
    }

} catch (SoapFault $e) {
    echo 'Wrong Soap credentials : ' . $e->getMessage();
}
