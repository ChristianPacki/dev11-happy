<?php
require_once 'function.php';
require_once 'storeFunction.php';
require_once $docAbsPath . 'app/Mage.php';
Mage::app();
$usermodel = Mage::getModel('api/user')->getCollection()->addFieldToFilter('is_active', 1);
$id        = $usermodel->getColumnValues('user_id');
$name      = $usermodel->getColumnValues('username');
$ulist     = array_combine($id, $name);
$step      = array(6, 4);
checkStepStatus(array('checkStartInstall', 'checkAttributeCreate', 'checkProductCreationStatus', '', 'checkWriteXMLToApp'));
checkCurrentStep('checkIntegrationStatus', 4);

if (isset($_POST['soap_form']) && $_POST['soap_form'] == 'Next' && isset($_POST['type'])) {
    extract($_POST);
    $status     = 0;
    $userStatus = 0;
    $msg        = '';
    Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
    $base_url    = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB);
    $admin_model = Mage::getModel('admin/user');
    $all_admins  = $admin_model->getCollection()->load()->getData();
    $unames      = array();
    foreach ($all_admins as $k => $v) {
        $unames[$k] = $admin_model->authenticate($v['username'], $apipwd);
    }
    if (in_array(1, $unames)) {
        if ($type == 'n') {
            $rolename   = 'inkXE Role';
            $all        = '1'; // all / customer
            $model      = Mage::getModel('api/roles'); //roles//Mage_Api_Model_Roles
            $role_exist = $model->getCollection()->addFieldToFilter('role_name', $rolename)->getData();
            if (empty($role_exist)) {
                $api_role = $model->setName($rolename)->setPid(false)->setRoleType('G')->save();
                $role_id  = $api_role->getId();
                $user_id  = $api_role->getUserId();
                Mage::getModel('api/rules')->setRoleId($role_id)->setResources(array('all'))->saveRel();
            } else {
                $role_id = $role_exist[0]['role_id'];
                $user_id = $role_exist[0]['user_id'];
            }
            if ($user_id) {
                $userStatus = 1;
                $status     = 1;
            } else {
                $userStatus = 1;
                $api_user   = Mage::getModel('api/user');
                $api_user->setData(array(
                    'username'             => $username,
                    'firstname'            => $fname,
                    'lastname'             => $lname,
                    'email'                => $email,
                    'api_key'              => $api_key,
                    'api_key_confirmation' => $api_key,
                    'is_active'            => 1,
                    'roles'                => array($role_id),
                ));
                try {
                    $api_user->save()->load($api_user->getUserId());
                    $api_user->setRoleIds(array($role_id))->setRoleUserId($api_user->getUserId())->saveRelations();
                    $status = 1;
                    $msg    = '';
                } catch (Exception $e) {
                    //checks duplicate username and email
                    $status = 0;
                    $msg    = "A SOAP user is already present with same 'API USER NAME' and or 'EMAIL'. Please provide different info." . "\n";
                    xe_log("\n" . date("Y-m-d H:i:s") . ': Error in 4th Step Soap Connection : Duplicate SOAP username and email : ' . $e->getMessage() . "\n");
                }
            }
        } elseif ($type == 'e') {
            if (isset($user_id) && isset($role_id) && isset($apiuser) && isset($apipwd) && isset($api_key)) {
                ini_set("soap.wsdl_cache_enabled", "0");
                try {
                    $client = new SoapClient($base_url . 'index.php/api/soap?wsdl', array('cache_wsdl' => WSDL_CACHE_NONE));
                } catch (Exception $e) {
                    $status = 0;
                    $msg .= 'Error in Soap Connection.';
                    xe_log("\n" . date("Y-m-d H:i:s") . ': Error in 4th Step Soap Connection : ' . $e->getMessage() . "\n");
                }
                try {
                    $session    = $client->login($apiuser, $api_key);
                    $userStatus = 1;
                    $username   = $apiuser;
                    $status     = 1;
                    $msg        = '';
                } catch (Exception $e) {
                    $status = 0;
                    $msg .= 'Wrong SOAP connection info.' . "\n";
                    xe_log("\n" . date("Y-m-d H:i:s") . ': Error in 4th Step : Wrong Soap credentials : ' . $e->getMessage() . "\n");
                }
            } else {
                $status = 0;
                $msg .= 'Please Select existing User and corresponding role and enter correct API Key.' . "\n";
            }
        }
    } else {
        $status = 0;
        $msg    = 'Incorrect Magento password.';
        xe_log("\n" . date("Y-m-d H:i:s") . ': Error in 4th Step : Incorrect Magento Password.' . "\n");
    }
    if ($userStatus) {
        $sourceF = '../../xeconfig.xml'; //'../app/xeconfig.xml';
        $dom     = new DomDocument();
        $dom->load($sourceF);
        $dom->getElementsByTagName('apiuser')->item(0)->nodeValue = $username;
        $dom->getElementsByTagName('apipass')->item(0)->nodeValue = $api_key; //$apipwd;
        $dom->save($sourceF);
    } else {
        $msg .= '- Api user not created properly.';
        $status = 0;
        xe_log("\n" . date("Y-m-d H:i:s") . ': Error in 4th Step : ' . $msg . "\n");
    }
    if ($status == 0) {
        $errorMsg = $msg;
    } else {
        header("location:setup-db.php");
        exit;
    }
}

require_once 'header.php';
?>
<form name='soap_form' id='soap_form' onSubmit="return validate_soap();" method="post" role="form" class="">
    <div class="content-box">
        <div class="row">
         <div class="col-sm-12">
            <?php if (isset($errorMsg)) {?>
            <div class="error-msg">
            <?php echo "The following Errors occured:<br/>" . nl2br($errorMsg); ?>
            </div>
            <?php }?>
        </div>
            <div class="col-sm-12"><h3>Magento web service(SOAP/XML-RPC) details.</h3></div>
            <div class="col-sm-12">
                    <div class="row m-b-sm">
                        <div class="col-sm-6">
                            <div class="radio radio-success">
                                <input type="radio" name="type" value="n" onClick="checkType(this);" id="yes" checked="checked">
                                <label for="yes">NEW SOAP USER</label>
                                <input type="radio" name="type" value="e" onClick="checkType(this);" id="no">
                                <label for="no">EXISTING SOAP USER</label>
                            </div>
                        </div>
                        <div class="col-sm-6"></div>
                    </div>

                    <div id='new'>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group form-group-default required">
                                    <label>First Name</label>
                                    <input type="text" name="fname" id="fname" autocomplete="off" class="form-control">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group form-group-default required">
                                    <label>Last Name</label>
                                    <input type="text" name="lname" id="lname" autocomplete="off" class="form-control">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group form-group-default required">
                                    <label>User Name</label>
                                    <input type="text" name="username" id="username" autocomplete="off" class="form-control">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group form-group-default required">
                                    <label>Email</label>
                                    <input type="text" id="email" name="email" autocomplete="off" class="form-control">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id='existing' style='display:none;'>
                        <div class="row m-b-sm">
                            <input type='hidden' id='ul' value='<?php echo count($ulist); ?>'>
                            <input type='hidden' name='apiuser' id='apiuser'>
                            <div class="col-sm-6 p-l-0" id="ulDiv">
                                <select name='user_id' id='user_id' onchange='setRole(this);' class="cs-select" data-init-plugin="cs-select">
                                    <option value="0">SELECT USER</option>
                                    <?php foreach ($ulist as $k => $v) {?>
                                        <option value="<?php echo $k ?>"><?php echo $v; ?></option>
                                    <?php }?>
                                </select>
                            </div>

                            <div class="col-sm-6" id='roletd' style='display:none;'></div>
                            <span id="spanId" class="alert alert-danger text-primary"><?php echo 'NO EXSTING SOAP USER PRESENT CURRENTLY. PLEASE CREATE A NEW SOAP USER.'; ?> </span>
                        </div>
                    </div>
                    <div class="confdiv form-group form-group-default required">
                        <label>SOAP API Key</label>
                        <input type="password" autocomplete="off" name="api_key" id="api_key" class="form-control">
                    </div>

                    <div class="confdiv form-group form-group-default required">
                        <label>Magento Admin Password</label>
                        <input type="password" autocomplete="off" name="apipwd" id="apipwd" class="form-control">
                    </div>
                </div>
            </div>
        </div>
            <div class="row m-t-sm">
            <div class="col-sm-12">
                <input type="submit" value="Next" name="soap_form" class="btn btn-lg btn-aqua m-w-150 pull-right" />
                <span class="text-right pull-right m-t-sm m-r-md"><a href="http://inkxe.com/support/kb/faq.php?id=209" target="_blank">Click here</a> to know more about SOAP connection.</span>
            </div>
        </div>
            </div>
<?php require_once 'footer.php';?>
</body>
</html>

