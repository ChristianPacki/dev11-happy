<?php
require_once 'function.php';
require_once 'storeFunction.php';
require_once DOCABSPATH . "/app/Mage.php";
function getXeConfigParam()
{
    $file = '../../xeconfig.xml';
    if (file_exists($file)) {
        $dom = new DomDocument();
        $dom->load($file);
        $array = array();
        $array['APPNAME'] = $dom->getElementsByTagName('appname')->item(0)->nodeValue;
        $array['XEPATH'] = $dom->getElementsByTagName('base_url')->item(0)->nodeValue;
        $array['SERVER'] = $dom->getElementsByTagName('host')->item(0)->nodeValue;
        $array['USER'] = $dom->getElementsByTagName('dbuser')->item(0)->nodeValue;
        $array['PASSWORD'] = $dom->getElementsByTagName('dbpass')->item(0)->nodeValue;
        $array['DBNAME'] = $dom->getElementsByTagName('dbname')->item(0)->nodeValue;
        $array['APIUSER'] = $dom->getElementsByTagName('apiuser')->item(0)->nodeValue;
        $array['APIPASS'] = $dom->getElementsByTagName('apipass')->item(0)->nodeValue;
        $array['FOLDER_NAME'] = $file_folder_name;
        $array['DOC_ROOT'] = $base_path;
        $array['ASSET_PATH'] = '/assets/' . $file_folder_name;
        $array['TABLE_PREFIX'] = '';
    }
    return $array;
}

function checkStepStatus($methods, $lastStep = false)
{
    if (IGNORESTEP == false) {
        $kounter = 1;
        $stepCount = count($methods);

        foreach ($methods as $method) {
            if (function_exists($method)) {
                $status = $method();
                if (is_array($status)) {
                    $status = $status['0'];
                }
                if ($status == 0) {
                    $redirectStep = $kounter;
                    redirctToStep($redirectStep);
                    exit;
                }
            } else {
                return false;
            }

            if (($kounter == $stepCount) && $lastStep == false) {
                header('location:finish.php');
                exit;
            }
            $kounter++;

        }
    }
}

function checkCurrentStep($method, $currentStep)
{
    if (IGNORESTEP == false) {
        if (function_exists($method)) {
            $status = $method();
            if (is_array($status)) {
                $status = $status['0'];
            }

            if ($status == 1) {
                redirctToStep($currentStep + 1);
                exit;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
}

function redirctToStep($step)
{
    $redirectToPath = "";
    switch ($step) {
        case (1):
            $redirectToPath = "index.php";
            break;
        case (2):
            $redirectToPath = "create-attribute.php";
            break;
        case (3):
            $redirectToPath = "create-product.php";
            break;
        case (4):
            $redirectToPath = "create-soap-user.php";
            break;
        case (5):
            $redirectToPath = "setup-db.php";
            break;
        default:
            $redirectToPath = "finish.php";
    }
    header('location:' . $redirectToPath);
    exit;
}
function checkAttributeCreate()
{
    $status = 0;
    $dom = new DomDocument();
    $dom->load('../../xeconfig.xml') or die("error");
    $msg = " - Attribute is not yet created. \n";
    if ($dom->getElementsByTagName('inkXE')->item(0)->nodeValue != "") {
        $attribute_set_name = $dom->getElementsByTagName('inkXE')->item(0)->nodeValue;
        $mageRunCode = isset($_SERVER['MAGE_RUN_CODE']) ? $_SERVER['MAGE_RUN_CODE'] : ''; //Store or website code
        $mageRunType = isset($_SERVER['MAGE_RUN_TYPE']) ? $_SERVER['MAGE_RUN_TYPE'] : 'store'; // Run store or run website
        Mage::init($mageRunCode, $mageRunType, array());
        $attributeSetId = Mage::getModel('eav/entity_attribute_set')->load($attribute_set_name, 'attribute_set_name')->getAttributeSetId();
        if ($attributeSetId > 0) {
            $status = 1;
            $msg = "";
        }
    }
    return array($status, $msg);
}
function createAttribute()
{
    $status = 0;
    $attribute_set_name = 'inkXE';
    $returnValue = checkAttributeCreate();
    if ($returnValue['0'] == 0) {
        createAttributeSet($attribute_set_name);
        $attribute = array(
            'attribute_set' => $attribute_set_name,
            'label' => 'Color',
            'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
            'group' => '',
            'input' => 'select',
            'type' => 'int',
            'source' => 'eav/entity_attribute_source_table',
            'user_defined' => 1,
            'is_configurable' => 1,
        );

        $arr = array('xe_size' => array('value' => array(
            'optionone' => array('XL'),
            'optiontwo' => array('L'),
            'optionthree' => array('M'),
        ),
            'label' => 'Size',
        ),
            'xe_color' => array('value' => array(
                'optionone' => array('red'),
                'optiontwo' => array('blue'),
                'optionthree' => array('black'),
            ),
                'label' => 'Color',
            ),
        );

        $installer = new Mage_Eav_Model_Entity_Setup;
        $option = array();
        foreach ($arr as $k => $v) {
            //$attribute['option'] = $v['value'];
            if (!($installer->getAttributeId('catalog_product', $k))) {
                $attribute['label'] = $v['label'];
                $addAttribute = $installer->addAttribute('catalog_product', $k, $attribute);
                $option['attribute_id'] = $installer->getAttributeId('catalog_product', $k);
                $c = 1;
                foreach ($v['value'] as $k1 => $v1) {
                    $option['value']['any_option_name'][0] = $v1[0];
                    $option['value']['any_option_name'][1] = $v1[0];
                    $option['order']['any_option_name'] = $c;
                    $installer->addAttributeOption($option);
                    $c++;
                }
            }
        }

        $attribute1 = array(
            'attribute_set' => $attribute_set_name,
            'label' => 'Show in Designer ',
            'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL, //scope can be SCOPE_STORE or SCOPE_GLOBAL or SCOPE_WEBSITE
            'group' => '',
            'type' => 'int',
            'input' => 'boolean',
            'source' => '', //'eav/entity_attribute_source_boolean',//this is necessary for select and multilelect, for the rest leave it blank
            'user_defined' => 1,
            'used_in_product_listing' => 1
        );
        $attribute2 = array(
            'attribute_set' => $attribute_set_name,
            'label' => 'Pre Decorated Product',
            'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL, //scope can be SCOPE_STORE or SCOPE_GLOBAL or SCOPE_WEBSITE
            'group' => '',
            'type' => 'int',
            'input' => 'boolean',
            'source' => '', //'eav/entity_attribute_source_boolean',//this is necessary for select and multilelect, for the rest leave it blank
            'user_defined' => 1,
            'used_in_product_listing' => 1,
            'apply_to' => 'configurable'
        );
        $disableAttribute = array(
            'attribute_set' => $attribute_set_name,
            'label' => 'Disable Addtocart Button',
            'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL, //scope can be SCOPE_STORE or SCOPE_GLOBAL or SCOPE_WEBSITE
            'group' => '',
            'type' => 'int',
            'input' => 'boolean',
            'source' => '', //'eav/entity_attribute_source_boolean',//this is necessary for select and multilelect, for the rest leave it blank
            'user_defined' => 1,
            'used_in_product_listing' => 1,
            'apply_to' => 'configurable'
        );
        $addAttribute = $installer->addAttribute('catalog_product', 'xe_is_designer', $attribute1);
        $addAttribute = $installer->addAttribute('catalog_product', 'xe_is_template', $attribute2);
        $addAttribute = $installer->addAttribute('catalog_product', 'disable_addtocart', $disableAttribute);

        /////// Add that attribute to attributeset //////////
        $group_name = 'General';
        $attribute_code = array('xe_color', 'xe_size', 'xe_is_designer', 'xe_is_template', 'disable_addtocart');

        $attribute_set_id = $installer->getAttributeSetId('catalog_product', $attribute_set_name);
        $attribute_group_id = $installer->getAttributeGroupId('catalog_product', $attribute_set_id, $group_name);

        try {
            foreach ($attribute_code as $att) {
                if ($att == 'xe_is_designer') {
                    $installer->updateAttribute(Mage_Catalog_Model_Product::ENTITY, $att, array('is_required' => false));
                } elseif ($att == 'xe_color') {
                    $installer->updateAttribute(Mage_Catalog_Model_Product::ENTITY, $att, array('required' => 1, 'is_html_allowed_on_front' => 1, 'is_visible_on_front' => 1, 'used_in_product_listing' => true));
                } elseif ($att == 'xe_size') {
                    $installer->updateAttribute(Mage_Catalog_Model_Product::ENTITY, $att, array('required' => 1, 'is_html_allowed_on_front' => 1, 'is_visible_on_front' => 1, 'used_in_product_listing' => true));
                } elseif ($att == 'xe_is_template') {
                    $installer->updateAttribute(Mage_Catalog_Model_Product::ENTITY, $att, array('is_required' => false, 'apply_to' => 'configurable'));
                } elseif ($att == 'disable_addtocart') {
                    $installer->updateAttribute(Mage_Catalog_Model_Product::ENTITY, $att, array('is_required' => false, 'apply_to' => 'configurable'));
                }
                $attribute_id = $installer->getAttributeId('catalog_product', $att);
                $installer->addAttributeToSet($entityTypeId = 'catalog_product', $attribute_set_id, $attribute_group_id, $attribute_id);
                $msg = '';
                $status = 1;
            }
            $installer->endSetup();
        } catch (Exception $e) {
            xe_log("\n" . date("Y-m-d H:i:s") . ': Error in 2nd Step: ' . $e->getMessage() . "\n");
            $msg = 'Your magento is unable to associate attributes to attribute set.';
        }
    }
    return array($status, $msg);
}
//create attribute set name
function createAttributeSet($setName)
{
    try {
        $baseSetId = 4; // default set usually...
        $entityTypeId = Mage::getModel('catalog/product')->getResource()->getEntityType()->getId();
        $attributeSet = Mage::getModel('eav/entity_attribute_set')->setEntityTypeId($entityTypeId)->setAttributeSetName($setName);
        $attributeSet->validate();
        $attributeSet->save();
        $attributeSet->initFromSkeleton($baseSetId)->save();
    } catch (Exception $e) {
        xe_log("\n" . date("Y-m-d H:i:s") . ': Error in 2nd Step: ' . $e->getMessage() . "\n");
        $msg = 'Your magento is unable to create associate set.';
    }
}
function checkProductCreationStatus()
{
    $msg = " - Product is not yet created. \n";
    $status = 0;
    $s_product = Mage::getModel('catalog/product')->loadByAttribute('name', 'InkXE Tshirt_001');
    if ($s_product) {
        $status = 1;
        $msg = "";
    }
    return array($status, $msg);
}
function createCms()
{
    $identifier = 'product-designer';
    $exist = Mage::getModel('cms/page')->getCollection()->addFieldToFilter('identifier', $identifier)->getData();
    if (empty($exist)) {
        $content = '<p>{{block type="core/template" name="myDesignerSesId" template="cedapi/productdesigner.phtml"}}<iframe id="tshirtIFrame" style="border: none;" src="{{config path="web/secure/base_url"}}xetool/index.html" height="780" width="100%"></iframe></p>';
        $cmsPage = array(
            'title' => 'Product Designer Tool',
            'identifier' => $identifier,
            'content_heading' => 'Product Designer Tool',
            'content' => $content,
            'is_active' => 1,
            'root_template' => 'one_column',
            'sort_order' => 0,
            'stores' => array(0),
        );
        Mage::getModel('cms/page')->setData($cmsPage)->save();
    } else {
        xe_log("\n" . date("Y-m-d H:i:s") . ':You have already created this CMS Page.' . "\n");
    }
}
function createProduct()
{
    $status = 0;
    Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
    //createCategory();
    $fpath = 'wizard/images/install_image/';
    $dom = new DomDocument();
    $dom->load('../../xeconfig.xml') or die("error");
    $attribute_set_name = $dom->getElementsByTagName('inkXE')->item(0)->nodeValue;
    $attributeColorCode = $dom->getElementsByTagName('xe_color')->item(0)->nodeValue;
    $attributeSizeCode = $dom->getElementsByTagName('xe_size')->item(0)->nodeValue;
    $colorMethod = 'set' . ucfirst($attributeColorCode);
    $sizeMethod = 'set' . ucfirst($attributeSizeCode);
    if ($attribute_set_name == 'Default') {
        $attribute_set_id = 4;
    } else {
        $attribute_set_id = Mage::getModel('eav/entity_attribute_set')->load($attribute_set_name, 'attribute_set_name')->getAttributeSetId();
    }
    $attribute_colorid = Mage::getSingleton("eav/config")->getAttribute('catalog_product', $attributeColorCode)->getData('attribute_id');
    $attribute_sizeid = Mage::getSingleton("eav/config")->getAttribute('catalog_product', $attributeSizeCode)->getData('attribute_id');
    $productModel = Mage::getModel('catalog/product');
    $attr = $productModel->getResource()->getAttribute($attributeColorCode);
    $optarr = array();
    if ($attr->usesSource()) {
        $optarr = $attr->getSource()->getAllOptions();
        $optionColorId = $optarr[1]['value'];
    }
    $attr = $productModel->getResource()->getAttribute($attributeSizeCode);
    if ($attr->usesSource()) {
        $optarr = $attr->getSource()->getAllOptions();
        $optionSizeId = $optarr[1]['value'];
    }
    $store = Mage::getModel('core/store')->load(Mage_Core_Model_App::DISTRO_STORE_ID);
    $rootcatId = $store->getRootCategoryId();
    $mediaAttribute = array('thumbnail', 'small_image', 'image');
    $s_product = Mage::getModel('catalog/product')->loadByAttribute('name', 'InkXE Tshirt_001');
    $productId = 0;
    $simpleProductArr = array();
    if (empty($s_product)) {
        $simpleProduct = Mage::getModel('catalog/product');
        try {
            $simpleProduct
                ->setWebsiteIds(array(1)) //website ID the product is assigned to, as an array
                ->setAttributeSetId($attribute_set_id) //ID of a attribute set named 'default'
                ->setTypeId(Mage_Catalog_Model_Product_Type::TYPE_SIMPLE) //product type
                ->setCreatedAt(date('Y-m-d H:i:s')) //product creation time
                ->setSku('inkxe_tshirt_001') //SKU
                ->setName('InkXE Tshirt_001') //product name
                ->setWeight(1)
                ->setStatus(1) //product status (1 - enabled, 2 - disabled)
                ->setTaxClassId(0) //tax class (0 - none, 1 - default, 2 - taxable, 4 - shipping)
                ->setVisibility(1) //catalog and search visibility
                ->setNewsFromDate('') //product set as new from
                ->setNewsToDate('') //product set as new to
                ->setPrice(100) //price in form 11.22
                ->setSpecialPrice('') //special price in form 11.22
                ->setSpecialFromDate('') //special price from (MM-DD-YYYY)
                ->setSpecialToDate('') //special price to (MM-DD-YYYY)
                ->setMetaTitle('metatitle')
                ->setMetaKeyword('metakeyword')
                ->setMetaDescription('metadescription')
                ->setDescription("This is one of the variant of the dummy product, 'InkXE Tshirt' which is created to check the designer tool.")
                ->setShortDescription('simple InkXE tshirt short description')
                ->$colorMethod($optionColorId)
                ->$sizeMethod($optionSizeId);
            // We set up a $count variable - the first image gets used as small, thumbnail and base
            $count = 0;
            $imgArray = array($fpath . 'simple.png');
            foreach ($imgArray as $image):
                $imgUrl = _save_image($image);
                if ($count == 0):
                    $simpleProduct->addImageToMediaGallery($imgUrl, $mediaAttribute, true, false);
                else:
                    $simpleProduct->addImageToMediaGallery($imgUrl, null, true, false);
                endif;
                $count++;
            endforeach;
            $simpleProduct->setCategoryIds(array($rootcatId));
            $simpleProduct->save();
            $productId = $simpleProduct->getId();
            $simpleProductArr[] = $productId;
            $stockItem = Mage::getModel('cataloginventory/stock_item');
            $stockItem->assignProduct($simpleProduct);
            $stockItem->setData('is_in_stock', 1);
            $stockItem->setData('stock_id', 1);
            $stockItem->setData('store_id', 1);
            $stockItem->setData('manage_stock', 1);
            $stockItem->setData('use_config_manage_stock', 0);
            $stockItem->setData('min_sale_qty', 1);
            $stockItem->setData('use_config_min_sale_qty', 0);
            $stockItem->setData('max_sale_qty', 30);
            $stockItem->setData('use_config_max_sale_qty', 0);
            $stockItem->setData('qty', 12123);
            $stockItem->save();
        } catch (Exception $e) {
            xe_log("\n" . date("Y-m-d H:i:s") . ': Error in 3rd Step: ' . $e->getMessage() . "\n");
        }
    } else {
        $spid = $s_product->getData();
        $productId = $spid['entity_id'];
        $simpleProductArr[] = $productId;
        xe_log("\n" . date("Y-m-d H:i:s") . ': -Simple Product already created' . "\n");
    }
    /* Configurable Product Insert Section */
    if (count($simpleProductArr) > 0) {
        $c_product = Mage::getModel('catalog/product')->loadByAttribute('name', 'InkXE Tshirt');
        if (empty($c_product)) {
            $configProduct = Mage::getModel('catalog/product');
            try {
                $configProduct
                // ->setStoreId(1) //you can set data in store scope
                ->setWebsiteIds(array(1)) //website ID the product is assigned to, as an array
                    ->setAttributeSetId($attribute_set_id) //ID of a attribute set named 'default'
                    ->setTypeId('configurable') //product type
                    ->setCreatedAt(date('Y-m-d H:i:s')) //product creation time
                    ->setSku('inkXE_tshirt') //SKU
                    ->setName('InkXE Tshirt') //product name
                    ->setWeight(21312)
                    ->setStatus(1) //product status (1 - enabled, 2 - disabled)
                    ->setTaxClassId(0) //tax class (0 - none, 1 - default, 2 - taxable, 4 - shipping)
                    ->setVisibility(Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH) //catalog and search visibility
                    ->setXeIsDesigner(1) //manufacturer id
                    ->setNewsFromDate('') //product set as new from
                    ->setNewsToDate('') //product set as new to
                    ->setPrice(100) //price in form 11.22
                    ->setSpecialPrice('') //special price in form 11.22
                    ->setSpecialFromDate('') //special price from (MM-DD-YYYY)
                    ->setSpecialToDate('') //special price to (MM-DD-YYYY)
                    ->setMetaTitle('InkXE Tshirt')
                    ->setMetaKeyword('metakeyword')
                    ->setMetaDescription('metadescription')
                    ->setDescription("This is the dummy product, 'InkXE Tshirt' which is created to check the designer tool.")
                    ->setShortDescription('InkXE tshirt short Description');

                $count = 0;
                $imgArray = array($fpath . 'configurable.png');
                foreach ($imgArray as $image):
                    $imgUrl = _save_image($image);
                    if ($count == 0):
                        $configProduct->addImageToMediaGallery($imgUrl, $mediaAttribute, true, false);
                    else:
                        $configProduct->addImageToMediaGallery($imgUrl, null, true, false);
                    endif;
                    $count++;
                endforeach;
                $configProduct->setCategoryIds(array($rootcatId)); //assign product to categories
                $simpleProducts = Mage::getResourceModel('catalog/product_collection')->addIdFilter($simpleProductArr)->addAttributeToSelect($attributeColorCode)->addAttributeToSelect($attributeSizeCode);
                $configProduct->setCanSaveConfigurableAttributes(true);
                $configProduct->setCanSaveCustomOptions(true);

                $configProduct->getTypeInstance()->setUsedProductAttributeIds(array($attribute_colorid, $attribute_sizeid)); //attribute ID of attribute 'color' in  store
                $configurableAttributesData = $configProduct->getTypeInstance()->getConfigurableAttributesAsArray();
                $configProduct->setCanSaveConfigurableAttributes(true);
                $configProduct->setConfigurableAttributesData($configurableAttributesData);
                $configurableProductsData = array();

                foreach ($simpleProducts as $simple) {
                    $productData = array(
                        'label' => $simple->getAttributeText($attributeColorCode),
                        'attribute_id' => $attribute_colorid,
                        'value_index' => (int) $simple->getColor(),
                        'is_percent' => 0,
                        'pricing_value' => $simple->getPrice(),
                    );
                    $configurableProductsData[$simple->getId()] = $productData;
                    $configurableAttributesData[0]['values'][] = $productData;
                    $productData = array(
                        'label' => $simple->getAttributeText($attributeSizeCode),
                        'attribute_id' => $attribute_sizeid,
                        'value_index' => (int) $simple->getSize(),
                        'is_percent' => 0,
                        'pricing_value' => $simple->getPrice(),
                    );

                    $configurableProductsData[$simple->getId()] = $productData;
                    $configurableAttributesData[1]['values'][] = $productData;
                }
                $configProduct->setConfigurableProductsData($configurableProductsData);
                $configProduct->setConfigurableAttributesData($configurableAttributesData);
                $configProduct->setCanSaveConfigurableAttributes(true);
                $configProduct->save();
                $confId = $configProduct->getId();
                $stockItem = Mage::getModel('cataloginventory/stock_item');
                $stockItem->assignProduct($configProduct);
                $stockItem->setData('is_in_stock', 1);
                $stockItem->setData('stock_id', 1);
                $stockItem->setData('store_id', 1);
                $stockItem->setData('manage_stock', 1);
                $stockItem->setData('use_config_manage_stock', 0);
                $stockItem->setData('min_sale_qty', 1);
                $stockItem->setData('use_config_min_sale_qty', 0);
                $stockItem->setData('max_sale_qty', 30);
                $stockItem->setData('use_config_max_sale_qty', 0);
                $stockItem->setData('qty', 12123);
                $stockItem->save();
                $status = 1;
                $msg = '';
            } catch (Exception $e) {
                $simpleProductArr = array();
                $msg = '- Unable to create simple product and configurable product. \n';
                xe_log("\n" . date("Y-m-d H:i:s") . ': Error in 3rd Step: ' . $e->getMessage() . "\n");
            }
        } else {
            $cpid = $c_product->getData();
            $cpid = $cpid['entity_id'];
            xe_log("\n" . date("Y-m-d H:i:s") . ': -Product already created' . "\n");
        }
    }
    return array($status, $msg);
}

function _save_image($img)
{
    $imageFilename = basename($img);
    $image_type = substr(strrchr($imageFilename, "."), 1); //find the image extension
    $filename = md5($img . strtotime('now')) . '.' . $image_type; //give a new name, you can modify as per your requirement
    if (!file_exists(DOCABSPATH . "media/import")) {
        mkdir(DOCABSPATH . 'media/import', 0777, true);
    }

    $filepath = Mage::getBaseDir('media') . DS . 'import' . DS . $filename; //path for temp storage folder: ./media/import/
    $newImgUrl = file_put_contents($filepath, file_get_contents(trim($img))); //store the image from external url to the temp storage folder
    return $filepath;
}
function createCategory()
{
    $store = Mage::getModel('core/store')->load(Mage_Core_Model_App::DISTRO_STORE_ID);
    $rootcatId = $store->getRootCategoryId();
    $catName = 'inkXE';
    $category = Mage::getResourceModel('catalog/category_collection')
        ->addFieldToFilter('name', $catName)
        ->getFirstItem(); // The parent category
    $categoryId = $category->getId();
    if (!$categoryId) {
        try {
            $category = Mage::getModel('catalog/category');
            $category->setName($catName);
            $category->setUrlKey($catName);
            $category->setIsActive(1);
            $category->setDisplayMode('PRODUCTS');
            $category->setIsAnchor(1); //for active achor
            $category->setStoreId(Mage::app()->getStore()->getId());
            $parentCategory = Mage::getModel('catalog/category')->load($rootcatId);
            $category->setPath($parentCategory->getPath());
            $category->save();
            $categoryId = $category->getId();
        } catch (Exception $e) {
            $msg = 'exception: ' . $e->getMessage();
            xe_log("\n" . date("Y-m-d H:i:s") . ':' . $msg);
        }
    }
}
function checkIntegrationStatus()
{
    $status = 1;
    $errorMsg = '';
    $sourceF = '../../xeconfig.xml';
    $dom = new DomDocument();
    $dom->load($sourceF) or die("xeconfig.xml either doesn't exists or malfunctioned");
    if ($dom->getElementsByTagName('apiuser')->item(0)->nodeValue == "") {
        $errorMsg = '- Private app\'s details not written properly in xml.';
        $status = 0;
    }
    return array($status, $errorMsg);
}
function getAttributeSet()
{
    $attributeSetList = array();
    try {
        Mage::app();
        $attributeSetCollection = Mage::getResourceModel('eav/entity_attribute_set_collection');
        $attributeSetCollection->setEntityTypeFilter('4');
        foreach ($attributeSetCollection as $id => $attributeSet) {
            $attributeSetList[$id]['value'] = $attributeSet->getId();
            $attributeSetList[$id]['label'] = $attributeSet->getAttributeSetName();
        }
        return $attributeSetList;
    } catch (Exception $e) {
        $msg = 'attributeSetList >>' . $e->getMessage();
        xe_log("\n" . date("Y-m-d H:i:s") . ':' . $msg);
    }
}
function getStoreDetails()
{
    try {
        $websites = Mage::app()->getWebsites();
        $val = '';
        foreach ($websites as $website) {
            foreach ($website->getStores() as $pk_id => $store) {
                $pk_id += 1;
                $storeObj = Mage::app()->getStore($store);
                $url = $storeObj->getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB);
                $url = explode('/', $url);
                $url = $url[2];
                $url = str_ireplace('www.', '', $url);
                $val .= ",(" . $pk_id . ",'" . $url . "','" . $storeObj->getId() . "')";
            }
        }
    } catch (Exception $e) {
        $msg = "Update your domain_store_rel table in your inkXE database.";
        xe_log("\n" . date("Y-m-d H:i:s") . ':' . $msg);
    }
    return $val;
}
function getcategoryId()
{
    $categoryId = 0;
    Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
    $catName = 'inkXE';
    $category = Mage::getResourceModel('catalog/category_collection')->addFieldToFilter('name', $catName)->getFirstItem(); // The parent category
    $categoryId = $category->getId();
    return $categoryId;
}
function setAttributes($color_id, $size_id, $attribute_id)
{
    $status = 0;
    umask(0);
    Mage::app();
    $attributeSetModel = Mage::getModel("eav/entity_attribute_set")->load($attribute_id);
    $xset = $attributeSetModel->getAttributeSetName();
    $attributes = Mage::getModel('catalog/product')->getResource()->loadAllAttributes()->getSortedAttributes($attribute_id);
    foreach ($attributes as $k => $value) {
        $attrData = $value->getData();
        if ($attrData['is_html_allowed_on_front'] == 1 && $attrData['frontend_input'] == 'select') {
            $attributeId[$k] = $attrData['attribute_id'];
            $attributeValue[$k] = $attrData['attribute_code'];
            $attributeList = array_combine($attributeId, $attributeValue);
        }
    }
    foreach ($attributeList as $k => $value) {
        if ($size_id == $k) {
            $sizeValue = $value;
        }
        if ($color_id == $k) {
            $colorValue = $value;
        }
    }
    if (isset($xset) && isset($sizeValue) && isset($colorValue)) {
        writeAttributeXml($colorValue, $sizeValue, $xset);
        createShowInDesigner($xset, $colorValue, $sizeValue);
        $msg = "";
        $status = 1;
    } else {
        $msg = '-Unable to write attribute in xml file';
    }
    return array($status, $msg);
}
function writeAttributeXml($xcolor, $xsize, $xset)
{
    $dom = new DomDocument();
    $dom->load('../../xeconfig.xml') or die("error");
    $dom->getElementsByTagName('xe_color')->item(0)->nodeValue = $xcolor;
    $dom->getElementsByTagName('xe_size')->item(0)->nodeValue = $xsize;
    $dom->getElementsByTagName('inkXE')->item(0)->nodeValue = $xset;
    $dom->save('../../xeconfig.xml');
}
function createShowInDesigner($attribute_set_name, $xe_color, $xe_size)
{
    $status = 0;
    $installer = new Mage_Eav_Model_Entity_Setup;
    $attribute1 = array(
        'attribute_set' => $attribute_set_name,
        'label' => 'Show in Designer ',
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL, //scope can be SCOPE_STORE or SCOPE_GLOBAL or SCOPE_WEBSITE
        'group' => '',
        'type' => 'int',
        'input' => 'boolean',
        'source' => '', //'eav/entity_attribute_source_boolean',//this is necessary for select and multilelect, for the rest leave it blank
        'user_defined' => 1,
        'used_in_product_listing' => 1
    );
    $attribute2 = array(
        'attribute_set' => $attribute_set_name,
        'label' => 'Pre Decorated Product',
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL, //scope can be SCOPE_STORE or SCOPE_GLOBAL or SCOPE_WEBSITE
        'group' => '',
        'type' => 'int',
        'input' => 'boolean',
        'source' => '', //'eav/entity_attribute_source_boolean',//this is necessary for select and multilelect, for the rest leave it blank
        'user_defined' => 1,
        'used_in_product_listing' => 1,
        'apply_to' => 'configurable'
    );
    $disableAttribute = array(
        'attribute_set' => $attribute_set_name,
        'label' => 'Disable Addtocart Button',
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL, //scope can be SCOPE_STORE or SCOPE_GLOBAL or SCOPE_WEBSITE
        'group' => '',
        'type' => 'int',
        'input' => 'boolean',
        'source' => '', //'eav/entity_attribute_source_boolean',//this is necessary for select and multilelect, for the rest leave it blank
        'user_defined' => 1,
        'used_in_product_listing' => 1,
        'apply_to' => 'configurable'
    );
    $addAttribute = $installer->addAttribute('catalog_product', 'xe_is_designer', $attribute1);
    $addAttribute = $installer->addAttribute('catalog_product', 'xe_is_template', $attribute2);
    $addAttribute = $installer->addAttribute('catalog_product', 'disable_addtocart', $disableAttribute);
    $group_name = 'General';
    $attribute_code = array($xe_color, $xe_size, 'xe_is_designer', 'xe_is_template', 'disable_addtocart');
    $attribute_set_id = $installer->getAttributeSetId('catalog_product', $attribute_set_name);
    $attribute_group_id = $installer->getAttributeGroupId('catalog_product', $attribute_set_id, $group_name);
    try {
        foreach ($attribute_code as $att) {
            if ($att == 'xe_is_designer') {
                $installer->updateAttribute(Mage_Catalog_Model_Product::ENTITY, $att, array('is_required' => false));
            } elseif ($att == $xe_color) {
                $installer->updateAttribute(Mage_Catalog_Model_Product::ENTITY, $att, array('required' => 1, 'is_html_allowed_on_front' => 1, 'is_visible_on_front' => 1, 'used_in_product_listing' => true));
            } elseif ($att == $xe_size) {
                $installer->updateAttribute(Mage_Catalog_Model_Product::ENTITY, $att, array('required' => 1, 'is_html_allowed_on_front' => 1, 'is_visible_on_front' => 1, 'used_in_product_listing' => true));
            } elseif ($att == 'xe_is_template') {
                $installer->updateAttribute(Mage_Catalog_Model_Product::ENTITY, $att, array('is_required' => false, 'apply_to' => 'configurable'));
            } elseif ($att == 'disable_addtocart') {
                $installer->updateAttribute(Mage_Catalog_Model_Product::ENTITY, $att, array('is_required' => false, 'apply_to' => 'configurable'));
            }
            $attribute_id = $installer->getAttributeId('catalog_product', $att);
            $installer->addAttributeToSet($entityTypeId = 'catalog_product', $attribute_set_id, $attribute_group_id, $attribute_id);
            $msg = '';
            $status = 1;
        }
        $installer->endSetup();
    } catch (Exception $e) {
        xe_log("\n" . date("Y-m-d H:i:s") . ': Error in 2nd Step: ' . $e->getMessage() . "\n");
        $msg = 'Your magento is unable to associate attributes to attribute set.';
    }
}
function getVersion()
{
    try {
        $mversion = Mage::getVersion();
    } catch (Exception $e) {
        $msg = $e->getMessage();die();
    }
    return $mversion;
}
function disableStoreCache()
{
	try {
		$cache_arr = Mage::app()->useCache();
		$new_status_arr = array_fill(0, count($cache_arr), 0);
		$new_cache_arr = array_combine(array_keys($cache_arr), $new_status_arr);
		Mage::app()->saveUseCache($new_cache_arr);
	} catch (Exception $e) {
		$msg = "Cache disable failed.";
		xe_log("\n" . date("Y-m-d H:i:s") . ': Error in 1st Step : ' . $msg . "\n");
	}
	require_once "reindex.php";
}
