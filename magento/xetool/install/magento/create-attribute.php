<?php
require_once 'function.php';
require_once 'storeFunction.php';
require_once DOCABSPATH . "/app/Mage.php";
$step = array(6, 2);
checkStepStatus(array('checkStartInstall', '', 'checkProductCreationStatus', 'checkIntegrationStatus', 'checkWriteXMLToApp'));
checkCurrentStep('checkAttributeCreate', 2);

$attributeSetList = getAttributeSet();
if (isset($_POST['create_attribute']) && $_POST['create_attribute'] == 'Next' && isset($_POST['type'])) {
    extract($_POST);
    if ($type == 'n') {
        $attribute_set_name = 'inkXE';
        $xe_color = 'xe_color';
        $xe_size = 'xe_size';
        writeAttributeXml($xe_color, $xe_size, $attribute_set_name);
        $returnValue = createAttribute();
    } else if ($type == 'e') {
        $status = 0;
        if ($attribute_id != "0") {
            if ($color_id != "0" && $size_id != "0") {
                if ($color_id != $size_id) {
                    $returnValue = setAttributes($color_id, $size_id, $attribute_id);
                } else {
                    $msg = "color attribute and size attribute shouldn't be same.";
                    $returnValue = array($status, $msg);
                }
            } else {
                $msg = "Select the attribute in drop-down list.";
                $returnValue = array($status, $msg);
            }
        } else {
            $msg = "Select the attribute set in drop-down list.";
            $returnValue = array($status, $msg);
        }
    }
    if ($returnValue['0'] == 0) {
        $errorMsg = $returnValue['1'];
    } else {
        header("location:create-product.php");
        exit;
    }
}

require_once 'header.php';
?>
<form name='create_attribute' id='create_attribute' class="setdb_form" method="post" action="">
	<div class="content-box">
		<div class="row">
			<div class="col-sm-12">
				<?php if (isset($errorMsg)) {?>
				<div class="error-msg">
					<?php echo "The following Errors occured:<br/>" . nl2br($errorMsg); ?>
				</div>
				<?php }?>
			</div>
		<div class="col-sm-12"><h3>Create Attribute for Product</h3></div>
			<div class="col-sm-12">
				<div class="row m-b-sm">
					<div class="col-sm-6">
						<div class="radio radio-success">
							<input type="radio" name="type" value="n" onClick="checkAttributeType(this);" id="yes" checked="checked">
							<label for="yes">CREATE NEW ATTRIBUTE</label>
							<input type="radio" name="type" value="e" onClick="checkAttributeType(this);" id="no">
							<label for="no">EXISTING ATTRIBUTE</label>
						</div>
					</div>
					<div class="col-sm-6"></div>
				</div>

				<div id='new'>
					<div class="row">
						<h4>This will create required  new attribute set and attributes for inkXE.</h4>
					</div>
				</div>

				<div id='existing' style='display:none;'>
					<input type='hidden' id='ul' value='<?php echo $ul = count($attributeSetList); ?>'>
					<?php if ($ul) {?>
					<div class="row m-b-sm">
						<div class="col-sm-6 p-l-0" id="ulDiv">
							<select name='attribute_id' id='attribute_id' onchange='attributeList(this);' class="cs-select" data-init-plugin="cs-select">
							<option value="0">SELECT ATTRIBUTE SET</option>
							<?php foreach ($attributeSetList as $v) {?>
							<option value="<?php echo $v['value'] ?>"><?php echo $v['label']; ?></option>
							<?php }?>
							</select>
						</div>
					</div>
					<?php } else {?>
					<div class="row m-b-sm alert alert-danger text-primary">
					NO EXISTING ATTRIBUTE SET PRESENT CURRENTLY. PLEASE CHOOSE FIRST ONE.						</div>
					<?php }?>
					<div id='attribute' style='display:none;'>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row m-t-sm">
		<div class="col-sm-12">
		<input type="submit" name="create_attribute" value="Next" class="btn btn-lg btn-aqua m-w-150 pull-right" />
		</div>
	</div>
</div>
<?php require_once 'footer.php';?>
</body>
</html>