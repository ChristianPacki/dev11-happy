<?php
$queryString = explode('xetool/install/', substr($_SERVER['SCRIPT_NAME'], 1));
$appname = '';
if (isset($queryString['0']) && $queryString['0'] != '') {
    $appname = $queryString['0'];
}
$docAbsPath = $_SERVER['DOCUMENT_ROOT'] . "/" . $appname;
require_once $docAbsPath . 'app/Mage.php';
Mage::app();

$rolemodel = Mage::getModel('api/role')->getCollection();
$role_id   = $rolemodel->getColumnValues('role_id');
$role_name = $rolemodel->getColumnValues('role_name');
$list      = array_combine($role_id, $role_name);

if (isset($_GET['id'])) {
    $usermodel = Mage::getModel('api/user')->getCollection();
    $id        = $usermodel->getColumnValues('user_id');
    $name      = $usermodel->getColumnValues('username');
    $ulist     = array_combine($id, $name);

    $role_id_under_selected_user = Mage::getModel('api/user')->loadByUsername($ulist[$_GET['id']])->getRoles();
    $role_id_under_selected_user = $role_id_under_selected_user[0];
    $rlist                       = array();
    if ($role_id_under_selected_user) {
        $rlist[$role_id_under_selected_user] = $list[$role_id_under_selected_user];
    }

    $res = "<select name='role_id' id='role_id' class='cs-select' data-init-plugin='cs-select'>";
    //echo count($rlist);exit;
    if (empty($rlist)) {
        $res .= "<option value='0'>NO ROLE ASSIGN TO THIS USER</option>";
    } else {
        $res .= "<option value='0'>SELECT ROLE</option>";
        foreach ($rlist as $k => $v) {
            $res .= "<option value='" . $k . "'>" . $v . "</option>";
        }
    }
    $res .= "</select>";
    echo $res;exit(0);
}
