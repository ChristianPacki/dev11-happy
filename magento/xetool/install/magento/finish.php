<?php
require_once 'function.php';
require_once 'storeFunction.php';
$step = array(6, 6);
require_once 'header.php';
$adminPath = checkAdminPath();
$upath = '../../../inkxe-tshirt.html';
$apath = '../../admin/index.html';
$apath = str_replace('admin', $adminPath, $apath);
checkStepStatus(array('checkStartInstall', 'checkAttributeCreate', 'checkProductCreationStatus', 'checkIntegrationStatus', 'checkWriteXMLToApp'), $lastStep = true);
?>
<div class="content-box">
    <div class="row">
       <div class="col-sm-12">
            <h3>Congratulations !! inkXE Designer Tool  is successfully installed in your store. <br /><br />

            After making changes in theme files, you can customize the Demo Product added to your store in <a target="_blank" href="<?php echo $upath ?>" class="text-u-l">inkXE designer Tool</a>.<br>
            </h3>
        </div>
        <div class="col-sm-12">
            <h3> <a target="_blank" href="<?php echo $apath ?>" class="text-u-l">Login to inkXE admin</a>. </h3>
        </div>
        <div class="col-sm-12">
            <h3> Thank you!! </h3>
        </div>
    </div>
    <hr />
</div>
</div>
    <?php require_once 'footer.php';?>
</body>
</html>
