<?php
require_once "../../../app/Mage.php";
umask(0);
Mage::app();
$attributeSetId = $_POST['id'];

$attributes = Mage::getModel('catalog/product')->getResource()->loadAllAttributes()->getSortedAttributes($attributeSetId);
foreach ($attributes as $k => $value) {
    $attrData = $value->getData();
    if ($attrData['is_html_allowed_on_front'] == 1 && $attrData['frontend_input'] == 'select') {
        $attributeId[$k] = $attrData['attribute_id'];
        $attributeValue[$k] = $attrData['frontend_label'];
        $attributeList = array_combine($attributeId, $attributeValue);
    }
}

$str = '<div class="row"><div class="col-sm-6 p-l-0" id="ulDiv">
			<select name="color_id" id="color_id" class="cs-select" data-init-plugin="cs-select">
			<option value="0">SELECT COLOR ATTRIBUTE</option>';
foreach ($attributeList as $k => $v) {
    $str .= '<option value="' . $k . '">' . $v . '</option>';
}

$str .= '</select></div>
		<div class="col-sm-6 p-l-0" id="ulDiv">
			<select name="size_id" id="size_id" class="cs-select" data-init-plugin="cs-select">
			<option value="0">SELECT SIZE ATTRIBUTE</option>';
foreach ($attributeList as $k => $v) {
    $str .= '<option value="' . $k . '">' . $v . '</option>';
}
$str .= '</select></div></div>';

echo $str;exit(0);
