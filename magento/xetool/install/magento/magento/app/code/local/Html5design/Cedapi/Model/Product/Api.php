<?php
class Html5design_Cedapi_Model_Product_Api extends Mage_Catalog_Model_Product_Api
{

    /**
     *
     * @return string $vesrion.
     */
    public function storeVersion()
    {
        return Mage::getVersion();
    }

    public function checkDesignerTool()
    {
        $module = Mage::getStoreConfigFlag('advanced/modules_disable_output/Html5design_Cedapi');
        return ($module) ? 'Disabled' : 'Enabled';
    }

    /**
     *
     * @Purpose: returns refIds of all the quotes which are not abandoned
     * @param int $storeId
     * @return array of refIdCartArr and duration
     *
     */
    public function getLiveQuoteRefIds($storeId = null)
    {
        ini_set("memory_limit", "2000M"); // extending limit
        $result = array();
        $lifetimes = Mage::getConfig()->getStoresConfigByPath('checkout/cart/delete_quote_after'); //30 days usually

        // cleaning expired quotes that have been converted to orders
        foreach ($lifetimes as $k => $lifetime) {
            if ($k == $storeId) {
                $lifetime *= 86400;
                $quoteCollection = Mage::getModel('sales/quote')->getCollection();
                $quoteCollection->addFieldToSelect('entity_id');

                //All living quotes
                $quoteCollection->getSelect()->joinLeft(
                    array('quote_item' => Mage::getConfig()->getTablePrefix() . sales_flat_quote_item),
                    'quote_item.quote_id = main_table.entity_id',
                    'quote_item.custom_design AS refId'
                )->where(
                    'quote_item.custom_design IS NOT NULL
                                    AND DATEDIFF(main_table.updated_at , "' . date("Y-m-d h:i:s", time() - $lifetime) . '") > 0
                                    AND main_table.store_id=' . $k
                )->order('entity_id ASC');

                $quotes['duration'] = $lifetime;
                $quotes['refIds'] = $quoteCollection->getData();
                return $quotes;
            }
        }
    }

    public function addAttributeColorOptionValue($colorname = null, $color)
    {
        $arg_attribute = $color;
        $arg_value = $colorname;

        $attr_model = Mage::getModel('catalog/resource_eav_attribute');
        $attr = $attr_model->loadByCode('catalog_product', $arg_attribute);
        $attr_id = $attr->getAttributeId();
        $option['attribute_id'] = $attr_id;
        $option['value']['attribute_value'][0] = $arg_value;
        $setup = new Mage_Eav_Model_Entity_Setup('core_setup');
        $setup->addAttributeOption($option);
        $setup->endSetup();
        // getting new id
        $attribute = Mage::getModel("eav/entity_attribute")->loadByCode("catalog_product", $arg_attribute);
        $source = $attribute->getSource();
        $options = $source->getAllOptions();

        foreach ($options as $optionValue) {
            if ($arg_value == $optionValue["label"]) {
                $value = $optionValue["value"];
            }
        }
        $optionss['attribute_id'] = $value;
        $optionss['attribute_value'] = $arg_value;
        $optionss['status'] = 'success';

        return json_encode($optionss);
    }

    public function editAttributeColorOptionValue($option_id, $colorname, $color)
    {
        $arg_attribute = $color;
        $arg_value = $colorname;
        $attr_model = Mage::getModel('catalog/resource_eav_attribute');
        $attr = $attr_model->loadByCode('catalog_product', $arg_attribute);
        $attr_id = $attr->getAttributeId();
        $attr_model->load($attr_id);
        $data = array();
        $values = array(
            $option_id => array(
                0 => $colorname, //0 is current store id, Apple is the new label for the option
            ),
        );
        $data['option']['value'] = $values;
        $attr_model->addData($data);
        try {
            $attr_model->save();
            $session = Mage::getSingleton('adminhtml/session');
            $session->addSuccess(Mage::helper('catalog')->__('The product attribute has been saved.'));
            /**
             * Clear translation cache because attribute labels are stored in translation
             */
            Mage::app()->cleanCache(array(Mage_Core_Model_Translate::CACHE_TAG));
            $session->setAttributeData(false);
            $product = Mage::getModel('catalog/product')
                ->setData($arg_attribute, $option_id);
            $text = $product->getAttributeText($arg_attribute);

            $optionss['attribute_id'] = $option_id;
            $optionss['attribute_value'] = $text;
            $optionss['status'] = 'success';
            return json_encode($optionss);
        } catch (Exception $e) {
            $session->addError($e->getMessage());
            $session->setAttributeData($data);
            return json_encode(array('isFault' => 1, 'faultMessage' => $e->getMessage()));
        }
    }
    /**
     *
     *date created 15-06-2016(dd-mm-yy)
     *date modified (dd-mm-yy)
     *Get color array
     *
     */
    public function getColorArr($lastLoaded, $loadCount, $oldConfId, $color)
    {
        $optarr = array();
        $res = array();
        if ($oldConfId == 0) {
            $productModel = Mage::getModel('catalog/product');
            $attr = $productModel->getResource()->getAttribute($color);
            if ($attr->usesSource()) {
                $optarr = $attr->getSource()->getAllOptions('label'); //array(12=>'#000000',13=>'#003244',14=>'#00499B');
                array_shift($optarr);
                rsort($optarr);
            }
        } else {
            $product = Mage::getModel('catalog/product')->load($oldConfId);
            $configurable = Mage::getModel('catalog/product_type_configurable')->setProduct($product);
            $simpleCollection = $configurable->getUsedProductCollection()->addAttributeToSelect('*')->addFilterByRequiredOptions();
            //Get all attribute of old configure product
            if (!empty($simpleCollection)) {
                $k = 0;
                $temp = array();
                foreach ($simpleCollection as $simple) {
                    $attr = $simple->getResource()->getAttribute($color);
                    if ($attr->usesSource()) {
                        $attrId = $color_id = $attr->getSource()->getOptionId($simple->getAttributeText($color));
                        if (!in_array($attrId, $temp)) {
                            $optarr[$k]['value'] = $attrId;
                            $optarr[$k]['label'] = $attr->getSource()->getOptionText($attrId);
                            $k++;
                        }
                        $temp[] = $attrId;
                    }
                }
            }
        }
        $res = $optarr;
        if ($loadCount) {
            $res = array_slice($optarr, $lastLoaded, $loadCount);
        }

        return json_encode($res);
    }

    public function getSizeArr($size)
    {
        $productModel = Mage::getModel('catalog/product');
        $attr = $productModel->getResource()->getAttribute($size);
        $optarr = array();
        if ($attr->usesSource()) {
            $optarr = $attr->getSource()->getAllOptions(); //array(9=>'L',10=>'XL',11=>'XXL');
            array_shift($optarr);
        }
        return json_encode($optarr);
    }

    public function checkDuplicateSku($sku_arr)
    {
        $data = Mage::getModel('catalog/product')->getCollection()->addFieldToFilter('sku', array('in' => $sku_arr))->getData();
        $exists = array();
        if (!empty($data)) {
            foreach ($data as $v) {
                $exists[] = $v['sku'];
            }
        }
        return json_encode($exists);
    }

    public function getCategoriesByProduct($productId)
    {
        $currentCatIds = Mage::getModel('catalog/product')->load($productId)->getCategoryIds();
        return json_encode($currentCatIds);
    }
    /**
     *
     *date created 15-06-2016(dd-mm-yy)
     *date modified 04-08-2016(dd-mm-yy)
     *Get Product data of a simple product
     *@param (int)configId
     *@param (int)colorId
     *@param (int)sizeId
     *@param (int)qty
     *
     */
    public function getProductInfo($configId, $colorId, $sizeId, $qty, $color, $size)
    {
        $nextarr = array();
        $productData = array();
        $productData['qty'] = $qty;
        $productData['id'] = $configId;
        $product1 = Mage::getModel('catalog/product')->load($configId);
        $configurable = Mage::getModel('catalog/product_type_configurable')->setProduct($product1);
        $attribute = Mage::getSingleton('eav/config')->getAttribute(Mage_Catalog_Model_Product::ENTITY, $color);
        $valueExist = false;
        if ($attribute->usesSource()) {
            $options = $attribute->getSource()->getAllOptions(false);
            foreach ($options as $k => $option) {
                if ($option['value'] == $colorId) {
                    $valueExist = true;
                    break;
                }
            }
        }
        if ($valueExist) {
            $filterColorId = $colorId;
            $filterSizeId = $sizeId;
        } else {
            $filterColorId = $sizeId;
            $filterSizeId = $colorId;
        }
        $simpleCollection = $configurable->getUsedProductCollection()
            ->addAttributeToSelect('*')
            ->addAttributeToFilter($color, $filterColorId)
            ->addAttributeToFilter($size, $filterSizeId)
            ->addFilterByRequiredOptions();
        if (!empty($simpleCollection)) {
            $data = array();
            foreach ($simpleCollection as $simple) {
                $attributes = $simple->getAttributes();
                $prodName = $simple->getData();
                $product_name = $simple->getName();
                $price = $simple->getPrice();
                $oldSKU = $simple->getSku();
                $data['simpleProductId'] = $simple->getId();
                foreach ($attributes as $attribute) {
                    $attrCode = $attribute->getAttributeCode();
                    $attrData = $attribute->getData();
                    if ($attrData['is_html_allowed_on_front'] == 1) {
                        $attr = $simple->getResource()->getAttribute($attrCode);
                        $attrText = $simple->getAttributeText($attrCode);
                        $attrId = $attr->getSource()->getOptionId($simple->getAttributeText($attrCode));
                        if ($attrText) {
                            if ($attrCode == $color) {
                                $data['xe_color'] = $attrText;
                                $data['xe_color_id'] = $attrId;
                            } else if ($attrCode == $size) {
                                $data['xe_size'] = $attrText;
                                $data['xe_size_id'] = $attrId;
                            } else {
                                $data[$attrCode] = $attrText;
                                $data[$attrCode . "_id"] = $attrId;
                            }
                        }
                    }
                }
            }
        }
        $productData['simple_product'] = $data;
        $result[] = $productData;
        return json_encode($result);
    }

    public function createConfigurableProduct($data, $configFile)
    {
        extract($data);
        $c_product = Mage::getModel('catalog/product')->loadByAttribute('sku', $sku); //->getData();
        if (empty($c_product)) {
            $configProduct = Mage::getModel('catalog/product');
            try {
                $configProduct
                    ->setStoreId($storeId) //you can set data in store scope
                    ->setWebsiteIds($websiteId) //website ID the product is assigned to, as an array
                    ->setAttributeSetId($attribute_set_id) //ID of a attribute set named 'default'
                    ->setTypeId('configurable') //product type
                    ->setCreatedAt(strtotime('now')) //product creation time
                    // ->setUpdatedAt(strtotime('now')) //product update time
                    ->setSku($sku) //SKU
                    ->setName($product_name) //product name
                    ->setWeight($weight)
                    ->setStatus(1) //product status (1 - enabled, 2 - disabled)
                    ->setTaxClassId(0) //tax class (0 - none, 1 - default, 2 - taxable, 4 - shipping)
                    ->setVisibility(Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH) //catalog and search visibility
                    ->setXeIsDesigner(1) //manufacturer id
                    //->setManufacturer(28) //manufacturer id
                    //->setColor($value[15])
                    //->setSize($value[16])
                    ->setNewsFromDate('') //product set as new from
                    ->setNewsToDate('') //product set as new to
                    ->setPrice($price) //price in form 11.22
                    ->setSpecialPrice('') //special price in form 11.22
                    ->setSpecialFromDate('') //special price from (MM-DD-YYYY)
                    ->setSpecialToDate('') //special price to (MM-DD-YYYY)
                    ->setMetaTitle($product_name)
                    ->setMetaKeyword('metakeyword')
                    ->setMetaDescription('metadescription')
                    ->setDescription($description)
                    ->setShortDescription($short_description);

                if (!empty($configFile) && $configFile['error'] == 0 && file_exists($configFile['tmp_name'])) {
                    $count = 0;
                    if (!file_exists($filepath)) {
                        mkdir($filepath, 0777, true);
                    } else {
                        chmod($filepath, 0777);
                    }

                    $ext = pathinfo($configFile['name'], PATHINFO_EXTENSION);
                    $filename = md5(strtotime('now')) . '.' . $ext;
                    $file = $filepath . '/' . $filename;

                    $bin_string = file_get_contents($configFile["tmp_name"]);
                    file_put_contents($file, $bin_string);
                    //Mage::log("mediaAttribute: ".json_encode($mediaAttribute), null, confimgips.log);
                    //Mage::log("confimg path: ".$file, null, confimgips.log);
                    //Mage::log("confimg data: ".$bin_string, null, confimgips.log);

                    if ($count == 0):
                        $configProduct->addImageToMediaGallery($file, $mediaAttribute, true, false);
                    else:
                        $configProduct->addImageToMediaGallery($file, null, true, false);
                    endif;
                    $count++;
                    //Mage::log("count: ".$count, null, confimgips.log);
                }

                $configProduct->setStockData(array(
                    'use_config_manage_stock' => 0, //'Use config settings' checkbox
                    'manage_stock' => 1, //manage stock
                    'min_sale_qty' => 1, //Minimum Qty Allowed in Shopping Cart
                    'max_sale_qty' => 20, //Maximum Qty Allowed in Shopping Cart
                    'is_in_stock' => 1, //Stock Availability
                    'qty' => $qty, //qty
                )
                )
                    ->setCategoryIds($cat_id); //assign product to categories

                $configProduct->setCanSaveConfigurableAttributes(true);
                $configProduct->setCanSaveCustomOptions(true);

                $configProduct->getTypeInstance()->setUsedProductAttributeIds(array($attribute_colorid, $attribute_sizeid));
                $configurableAttributesData = $configProduct->getTypeInstance()->getConfigurableAttributesAsArray();
                $configProduct->setCanSaveConfigurableAttributes(true);
                $configProduct->setConfigurableAttributesData($configurableAttributesData);

                $configProduct->setConfigurableAttributesData($configurableAttributesData);
                $configProduct->setCanSaveConfigurableAttributes(true);

                $configProduct->save();
                $conf_id = $configProduct->getId();

            } catch (Exception $e) {
                $simpleProductArr = array();
                Mage::log($e->getMessage());
                echo "SKU:" . $value[0] . '&nbsp;added unsucessfully2' . "<br />";
            }
        } else {echo 'Duplicate sku';}
        return $conf_id;
    }

    public function addProducts($storeId, $data, $configFile, $simpleFile, $color, $size, $attrSet)
    {
        //Mage::log("Product json: ".json_encode($data), null, ProductJson.log);
        $data = $data['productData'];
        $attribute_set_name = $attrSet;
        $data['attribute_set_name'] = $attribute_set_name;
        $attributeColorCode = $color;
        $data['attributeColorCode'] = $attributeColorCode;
        $attributeSizeCode = $size;
        $data['attributeSizeCode'] = $attributeSizeCode;

        $attribute_set_id = Mage::getModel('eav/entity_attribute_set')->load($attribute_set_name, 'attribute_set_name')->getAttributeSetId();
        $data['attribute_set_id'] = $attribute_set_id;
        $attribute_colorid = Mage::getSingleton("eav/config")->getAttribute('catalog_product', $attributeColorCode)->getData('attribute_id');
        $data['attribute_colorid'] = $attribute_colorid;
        $attribute_sizeid = Mage::getSingleton("eav/config")->getAttribute('catalog_product', $attributeSizeCode)->getData('attribute_id');
        $data['attribute_sizeid'] = $attribute_sizeid;

        $mediaAttribute = array('thumbnail', 'small_image', 'image');
        $data['mediaAttribute'] = $mediaAttribute;
        $filepath = Mage::getBaseDir('media') . DS . 'import'; ///home/betainkxe/public_html/magento/media/import
        $data['filepath'] = $filepath;
        $data['storeId'] = $storeId;
        $websiteId = Mage::getModel('core/store')->load($storeId)->getWebsiteId();
        $data['websiteId'] = array($websiteId);

        if (empty($data['cat_id'])) {
            $store = Mage::getModel('core/store')->load(Mage_Core_Model_App::DISTRO_STORE_ID);
            $rootcatId = $store->getRootCategoryId();
            $data['cat_id'] = array($rootcatId);
        }

        $childIds = $this->createSimpleProduct($data, $simpleFile);
        if (!empty($childIds)) {
            $confId = ($data['conf_id'] == 0) ? $this->createConfigurableProduct($data, $configFile) : $data['conf_id'];
        }

        $response = array();
        $variants = array();
        if ($confId && !empty($childIds)) {
            $assigned_splist = $this->fetchSimpleProductOfConfigurable($confId);
            if (!empty($assigned_splist)) {
                $childIds = array_values(array_merge($assigned_splist, $childIds));
            }
            $childIds = array_unique($childIds);

            $req = $this->associateSimpleToConfigurableProduct($confId, $childIds, $attributeColorCode, $attributeSizeCode, $attribute_colorid, $attribute_sizeid);

            $res = array();
            foreach ($req['variants'] as $v) {
                $x = array_search($v['color_id'], $req['colors']);
                if ($x) {
                    $res[$x]['color_id'] = $v['color_id'];
                    $res[$x]['size_id'][] = $v['size_id'];
                } else {
                    $res[$x]['color_id'] = $v['color_id'];
                    $res[$x]['size_id'][] = $v['size_id'];
                }
            }

            $response['conf_id'] = $confId;
            $response['variants'] = array_values($res);
        }
        Mage::getModel('index/process')->load(2)->reindexAll();
        Mage::getModel('index/process')->load(3)->reindexAll();
        Mage::getModel('index/process')->load(6)->reindexAll();

        $indexingProcesses = Mage::getSingleton('index/indexer')->getProcessesCollection();
        foreach ($indexingProcesses as $process) {
            $process->reindexEverything();
        }
        return json_encode($response);
    }
    /**
     *
     *date created 15-06-2016(dd-mm-yy)
     *date modified (dd-mm-yy)
     *Add Pre-decorated product as product
     * @param (int)storeId
     * @param (array)data
     * @param (array)configFile
     * @param (int)oldConfId
     * @param (int)varColor
     * @param (array)varSize
     * @param (int)productStatus
     *
     */
    public function addTemplateProducts($storeId, $data, $configFile, $oldConfId, $varColor, $varSize, $color, $size, $attrSet, $productStatus)
    {
        $attribute_set_name = $attrSet;
        $data['attribute_set_name'] = $attribute_set_name;
        $attributeColorCode = $color;
        $data['attributeColorCode'] = $attributeColorCode;
        $attributeSizeCode = $size;
        $data['attributeSizeCode'] = $attributeSizeCode;
        $attribute_set_id = Mage::getModel('eav/entity_attribute_set')->load($attribute_set_name, 'attribute_set_name')->getAttributeSetId();
        $data['attribute_set_id'] = $attribute_set_id;
        $attribute_colorid = Mage::getSingleton("eav/config")->getAttribute('catalog_product', $attributeColorCode)->getData('attribute_id');
        $data['attribute_colorid'] = $attribute_colorid;
        $attribute_sizeid = Mage::getSingleton("eav/config")->getAttribute('catalog_product', $attributeSizeCode)->getData('attribute_id');
        $data['attribute_sizeid'] = $attribute_sizeid;
        $mediaAttribute = array('thumbnail', 'small_image', 'image');
        $data['mediaAttribute'] = $mediaAttribute;
        $filepath = Mage::getBaseDir('media') . DS . 'import'; //home/betainkxe/public_html/magento/media/import
        $data['filepath'] = $filepath;
        $data['storeId'] = $storeId;
        $simpleProductName = $data['product_name'];
        $websiteId = Mage::getModel('core/store')->load($storeId)->getWebsiteId();
        $data['websiteId'] = array($websiteId);
        if (empty($data['cat_id'])) {
            $store = Mage::getModel('core/store')->load(Mage_Core_Model_App::DISTRO_STORE_ID);
            $rootcatId = $store->getRootCategoryId();
            $data['cat_id'] = array($rootcatId);
        }
        //Create new configure product
        $confId = ($data['conf_id'] == 0) ? $this->createTemplateConfigurableProduct($data, $configFile, $productStatus) : $data['conf_id'];
        $product1 = Mage::getModel('catalog/product')->load($oldConfId);
        $configurable = Mage::getModel('catalog/product_type_configurable')->setProduct($product1);
        $simpleCollection = $configurable->getUsedProductCollection()->addAttributeToSelect('*')->addFilterByRequiredOptions();
        //Get all simple product of old configure product
        if (!empty($simpleCollection)) {
            $childIds = array();
            $temp = array();
            foreach ($simpleCollection as $simple) {
                //Get simple product data
                $prodName = $simple->getData();
                $rand = rand(1, 9999);
                $weight = $simple->getWeight();
                $description = $simple->getDescription();
                $short_description = $simple->getShortDescription();
                $oldSKU = $simple->getSku();
                $qty = $simple->getStockItem()->getQty();
                $attr = $simple->getResource()->getAttribute($color);
                $attr1 = $simple->getResource()->getAttribute($size);
                $color_id = $attr->getSource()->getOptionId($simple->getAttributeText($color));
                $colorn = $simple->getAttributeText($color);
                $data['weight'] = $weight;
                $data['description'] = $description;
                $data['short_description'] = $short_description;
                $data['color_id'] = $color_id;
                //get product ID
                $product = Mage::getModel('catalog/product')->load($simple->getId());
                if (!in_array($color_id, $temp)) {
                    //Create new Simple product
                    if ($varColor == $color_id) {
                        foreach ($varSize as $size_id) {
                            $sizen = $attr1->getSource()->getOptionText($size_id);
                            $oldSKU = $oldSKU . $rand;
                            $data['sku'] = $oldSKU;
                            $data['size_id'] = $size_id;
                            $data['product_name'] = $simpleProductName . '-' . $sizen . '-' . $colorn;
                            $childIds[] = $this->createTemplateSimpleProduct($data, $product);
                        }
                    }
                }
                $temp[] = $color_id;
            }
        }
        $response = array();
        $variants = array();
        if ($confId && !empty($childIds)) {
            $assigned_splist = $this->fetchSimpleProductOfConfigurable($confId);
            $assigned_splist = array_filter($assigned_splist);
            if (is_array($assigned_splist) && !empty($assigned_splist)) {
                $childIds[] = array_values(array_merge($assigned_splist, $childIds));
            }
            //associate new simple product with new configure product
            $req = $this->associateSimpleToConfigurableProduct($confId, $childIds, $attributeColorCode, $attributeSizeCode, $attribute_colorid, $attribute_sizeid, $color, $size);
            $res = array();
            foreach ($req['variants'] as $v) {
                $x = array_search($v['color_id'], $req['colors']);
                if ($x) {
                    $res[$x]['color_id'] = $v['color_id'];
                    $res[$x]['size_id'][] = $v['size_id'];
                } else {
                    $res[$x]['color_id'] = $v['color_id'];
                    $res[$x]['size_id'][] = $v['size_id'];
                }
            }
            $response['conf_id'] = $confId;
            $response['old_conf_id'] = $oldConfId;
            $response['variants'] = array_values($res);
        }
        Mage::getModel('index/process')->load(2)->reindexAll();
        Mage::getModel('index/process')->load(3)->reindexAll();
        Mage::getModel('index/process')->load(6)->reindexAll();
        $indexingProcesses = Mage::getSingleton('index/indexer')->getProcessesCollection();
        foreach ($indexingProcesses as $process) {
            $process->reindexEverything();
        }
        return json_encode($response);
    }
    /**
     *
     *date created 15-06-2016(dd-mm-yy)
     *date modified (dd-mm-yy)
     *Create configurable product
     * @param (array) data
     * @param (array) configFile
     *
     */
    public function createTemplateConfigurableProduct($data, $configFile, $productStatus)
    {
        extract($data);
        $c_product = Mage::getModel('catalog/product')->loadByAttribute('sku', $sku); //->getData();
        if (empty($c_product)) {
            $configProduct = Mage::getModel('catalog/product');
            try {
                $configProduct
                    ->setStoreId($storeId) //you can set data in store scope
                    ->setWebsiteIds($websiteId) //website ID the product is assigned to, as an array
                    ->setAttributeSetId($attribute_set_id) //ID of a attribute set named 'default'
                    ->setTypeId('configurable') //product type
                    ->setCreatedAt(strtotime('now')) //product creation time
                    ->setSku($sku) //SKU
                    ->setName($product_name) //product name
                    //->setWeight($weight)
                    ->setStatus(1) //product status (1 - enabled, 2 - disabled)
                    ->setTaxClassId(0) //tax class (0 - none, 1 - default, 2 - taxable, 4 - shipping)
                    ->setVisibility(Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH) //catalog and search visibility
                    ->setXeIsDesigner($is_customized) //manufacturer id
                    ->setNewsFromDate('') //product set as new from
                    ->setNewsToDate('') //product set as new to
                    ->setPrice($price) //price in form 11.22
                    ->setSpecialPrice('') //special price in form 11.22
                    ->setSpecialFromDate('') //special price from (MM-DD-YYYY)
                    ->setSpecialToDate('') //special price to (MM-DD-YYYY)
                    ->setMetaTitle($product_name)
                    ->setMetaKeyword('metakeyword')
                    ->setMetaDescription('metadescription')
                    ->setDescription($description)
                    ->setShortDescription($short_description)
                    ->setXeIsTemplate(1);
                if ($productStatus == 'without_image') {
                    $configProduct->setDisableAddtocart(1);
                }
                if (!empty($configFile)) {
                    $count = 0;
                    foreach ($configFile as $configImg) {
                        if (!file_exists($filepath)) {
                            mkdir($filepath, 0777, true);
                        } else {
                            chmod($filepath, 0777);
                        }

                        $imageFilename = basename($configImg);
                        $image_type = substr(strrchr($imageFilename, "."), 1);
                        $filename = md5($configImg . strtotime('now')) . '.' . $image_type;
                        $filepath2 = Mage::getBaseDir('media') . DS . 'import' . DS . $filename;
                        $newImgUrl = file_put_contents($filepath2, file_get_contents(trim($configImg)));
                        if ($count == 0):
                            //Mage::log("simple data: ".$count, null, count1.log);
                            $configProduct->addImageToMediaGallery($filepath2, $mediaAttribute, false, false);
                        else:
                            $configProduct->addImageToMediaGallery($filepath2, null, false, false);
                        endif;
                        $count++;
                    }
                }
                $configProduct->setStockData(array(
                    'use_config_manage_stock' => 0, //'Use config settings' checkbox
                    'manage_stock' => 1, //manage stock
                    //'min_sale_qty' => 1, //Minimum Qty Allowed in Shopping Cart
                    //'max_sale_qty' => 20, //Maximum Qty Allowed in Shopping Cart
                    'is_in_stock' => 1, //Stock Availability
                    'qty' => $qty, //qty
                )
                )
                    ->setCategoryIds($cat_id); //assign product to categories
                $configProduct->setCanSaveConfigurableAttributes(true);
                $configProduct->setCanSaveCustomOptions(true);
                $configProduct->getTypeInstance()->setUsedProductAttributeIds(array($attribute_colorid, $attribute_sizeid));
                $configurableAttributesData = $configProduct->getTypeInstance()->getConfigurableAttributesAsArray();
                $configProduct->setConfigurableAttributesData($configurableAttributesData);
                $configProduct->setCanSaveConfigurableAttributes(true);
                $configProduct->save();
                $conf_id = $configProduct->getId();
            } catch (Exception $e) {
                $simpleProductArr = array();
                Mage::log($e->getMessage());
                echo "SKU:" . $value[0] . '&nbsp;added unsucessfully' . "<br />";
            }
        } else {echo 'Duplicate sku';}
        return $conf_id;
    }
    /**
     *
     *date created 15-06-2016(dd-mm-yy)
     *date modified (dd-mm-yy)
     *Create simple product
     * @param (array) data
     * @param (int) oldSimpleId
     *
     */
    public function createTemplateSimpleProduct($data, $oldSimpleId)
    {
        extract($data);
        $simpleProductArr = array();
        $setXeColor = "set" . $data['attributeColorCode'];
        $setXeSize = "set" . $data['attributeSizeCode'];
        if (isset($color_id)) {
            $simpleProduct = Mage::getModel('catalog/product');
            try {
                $simpleProduct
                //Set all simple product data
                ->setStoreId($storeId)
                    ->setWebsiteIds($websiteId)
                    ->setAttributeSetId($attribute_set_id)
                    ->setTypeId(Mage_Catalog_Model_Product_Type::TYPE_SIMPLE)
                    ->setCreatedAt(strtotime('now'))
                    ->setSku($data['sku'])
                    ->setName($data['product_name'])
                    ->setWeight($data['weight'])
                    ->setStatus(1)
                    ->setTaxClassId(0)
                    ->setVisibility(1)
                    ->setNewsFromDate('')
                    ->setNewsToDate('')
                    ->setPrice($data['price'])
                    ->setSpecialPrice('')
                    ->setSpecialFromDate('')
                    ->setSpecialToDate('')
                    ->setMetaTitle('metatitle')
                    ->setMetaKeyword('metakeyword')
                    ->setMetaDescription('metadescription')
                    ->setDescription($data['description'])
                    ->setShortDescription($data['short_description'])
                    ->$setXeColor($data['color_id'])
                    ->$setXeSize($data['size_id']);
                $simpleProduct->setStockData(array(
                    'use_config_manage_stock' => 0, //'Use config settings' checkbox
                    'manage_stock' => 1, //manage stock
                    // 'min_sale_qty' => $data['mini_qty'], //Minimum Qty Allowed in Shopping Cart
                    //'max_sale_qty' => 30, //Maximum Qty Allowed in Shopping Cart
                    'is_in_stock' => 1, //Stock Availability
                    'qty' => $data['qty'], //qty
                )
                )
                    ->setCategoryIds($cat_id); //assign product to categories
                if (!empty($oldSimpleId)) {
                    $img = array();
                    foreach ($oldSimpleId->getMediaGalleryImages() as $image) {
                        $imgData = $image->getUrl();
                        $parts = parse_url($imgData);
                        $str = $parts['path'];

                        $str1 = $test1 = explode('media', $str);
                        $dir = Mage::getBaseDir('media');
                        $img[] = $dir . $str1[1];
                    }
                    $count = 0;
                    foreach ($img as $simpleImg) {
                        if (!file_exists($filepath)) {
                            mkdir($filepath, 0777, true);
                        } else {
                            chmod($filepath, 0777);
                        }

                        $imageFilename = basename($simpleImg);
                        $image_type = substr(strrchr($imageFilename, "."), 1);
                        $filename = md5($simpleImg . strtotime('now')) . '.' . $image_type;
                        $filepath2 = Mage::getBaseDir('media') . DS . 'import' . DS . $filename;
                        $newImgUrl = file_put_contents($filepath2, file_get_contents(trim($simpleImg)));
                        if ($count == 0):
                            $simpleProduct->addImageToMediaGallery($filepath2, $mediaAttribute, false, false);
                        else:
                            $simpleProduct->addImageToMediaGallery($filepath2, null, false, false);
                        endif;
                        $count++;
                    }
                }
                $simpleProduct->save();
                $simpleProductArr[] = $simpleProduct->getId();
            } catch (Exception $e) {
                Mage::log("simple Product error: " . $e->getMessage(), null, simpleProductError . log);
            }
        }
        return $simpleProductArr;
    }

    public function createSimpleProduct($data, $simpleFile)
    {
        extract($data);
        $simpleProductArr = array();
        extract($data['variants'][0]);
        if (isset($color_id) && !empty($simpleProducts)) {
            foreach ($simpleProducts as $k => $v) {
                //$sp_exists[$k] = Mage::getModel('catalog/product')->loadByAttribute('sku', $v['sku']);//         print_R($sp_exists[$k]);exit;
                //if(empty($sp_exists[$k])){
                $simpleProduct[$k] = Mage::getModel('catalog/product');
                try {
                    $simpleProduct[$k]
                        ->setStoreId($storeId) //you can set data in store scope
                        ->setWebsiteIds($websiteId) //website ID the product is assigned to, as an array
                        ->setAttributeSetId($attribute_set_id) //ID of a attribute set named 'default'
                        ->setTypeId(Mage_Catalog_Model_Product_Type::TYPE_SIMPLE) //product type
                        ->setCreatedAt(strtotime('now')) //product creation time
                        ->setSku($v['sku']) //SKU
                        ->setName($v['product_name']) //product name
                        ->setWeight($v['weight'])
                        ->setStatus(1) //product status (1 - enabled, 2 - disabled)
                        ->setTaxClassId(0) //tax class (0 - none, 1 - default, 2 - taxable, 4 - shipping)
                        ->setVisibility(1) //catalog and search visibility
                        ->setNewsFromDate('')
                        ->setNewsToDate('')
                        ->setPrice($v['price']) //price in form 11.22
                        ->setSpecialPrice('') //special price in form 11.22
                        ->setSpecialFromDate('') //special price from (MM-DD-YYYY)
                        ->setSpecialToDate('')
                        ->setMetaTitle('metatitle')
                        ->setMetaKeyword('metakeyword')
                        ->setMetaDescription('metadescription')
                        ->setDescription($v['description'])
                        ->setShortDescription($v['short_description'])
                        ->setXeColor($color_id)
                        ->setXeSize($v['sizeId']);

                    if (!empty($simpleFile)) {
                        $count = 0;
                        if (!file_exists($filepath)) {
                            mkdir($filepath, 0777, true);
                        } else {
                            chmod($filepath, 0777);
                        }

                        foreach ($simpleFile['name'] as $k2 => $v2) {
                            if ($simpleFile['error'][$k2] == 0 && file_exists($simpleFile['tmp_name'][$k2])) {
                                $ext[$k2] = pathinfo($simpleFile['name'][$k2], PATHINFO_EXTENSION);
                                $filename[$k2] = md5(strtotime('now')) . '.' . $ext[$k2];
                                $file[$k2] = $filepath . '/' . $filename[$k2];

                                //Mage::log("file: ".$file[$k2], null, ips.log);
                                $bin_string[$k2] = file_get_contents($simpleFile['tmp_name'][$k2]);
                                file_put_contents($file[$k2], $bin_string[$k2]);

                                if ($count == 0):
                                    $simpleProduct[$k]->addImageToMediaGallery($file[$k2], $mediaAttribute, true, false);
                                else:
                                    $simpleProduct[$k]->addImageToMediaGallery($file[$k2], null, true, false);
                                endif;
                                $count++;
                            }
                        }
                    }

                    $simpleProduct[$k]->setStockData(array(
                        'use_config_manage_stock' => 0, //'Use config settings' checkbox
                        'manage_stock' => 1, //manage stock
                        'min_sale_qty' => 1, //Minimum Qty Allowed in Shopping Cart
                        'max_sale_qty' => 30, //Maximum Qty Allowed in Shopping Cart
                        'is_in_stock' => 1, //Stock Availability
                        'qty' => $v['qty'], //qty
                    )
                    )
                        ->setCategoryIds($cat_id); //assign product to categories
                    //$simpleProduct[$k]->getResource()->save($simpleProduct[$k]);
                    $simpleProduct[$k]->save();
                    $simpleProductArr[] = $simpleProduct[$k]->getId();

                } catch (Exception $e) {
                    Mage::log("simple Product error: " . $e->getMessage(), null, ips . log);
                }
                //}
            }
        }
        return $simpleProductArr;
    }

    public function associateSimpleToConfigurableProduct($confId, $childIds, $attributeColorCode, $attributeSizeCode, $attribute_colorid, $attribute_sizeid, $color, $size)
    {
        $configProduct = Mage::getModel('catalog/product')->load($confId);

        $simpleProducts = Mage::getResourceModel('catalog/product_collection')
            ->addIdFilter($childIds)
            ->addAttributeToSelect($attributeColorCode)
            ->addAttributeToSelect($attributeSizeCode)
            ->addAttributeToSelect('price');
        $configProduct->setCanSaveConfigurableAttributes(true);
        $configProduct->setCanSaveCustomOptions(true);

        //$configProduct->getTypeInstance()->setUsedProductAttributeIds(array($attribute_colorid,$attribute_sizeid));
        $configurableAttributesData = $configProduct->getTypeInstance()->getConfigurableAttributesAsArray();
        $configProduct->setCanSaveConfigurableAttributes(true);
        $configProduct->setConfigurableAttributesData($configurableAttributesData);
        $configurableProductsData = array();

        $variants = array();
        foreach ($simpleProducts as $i => $simple) {
            $attr = $simple->getResource()->getAttribute($color);
            $attr1 = $simple->getResource()->getAttribute($size);
            $variants[$i]['color_id'] = (int) $attr->getSource()->getOptionId($simple->getAttributeText($color));
            $variants[$i]['size_id'] = (int) $attr1->getSource()->getOptionId($simple->getAttributeText($size));
            $colors[] = (int) $attr->getSource()->getOptionId($simple->getAttributeText($color));

            $productData = array(
                'label' => $simple->getAttributeText($attributeColorCode),
                'attribute_id' => $attribute_colorid,
                'value_index' => (int) $simple->getColor(),
                'is_percent' => 0,
                'pricing_value' => $simple->getPrice(),
            );

            $configurableProductsData[$simple->getId()] = $productData;
            $configurableAttributesData[0]['values'][] = $productData;

            $productData = array(
                'label' => $simple->getAttributeText($attributeSizeCode),
                'attribute_id' => $attribute_sizeid,
                'value_index' => (int) $simple->getSize(),
                'is_percent' => 0,
                'pricing_value' => $simple->getPrice(),
            );

            $configurableProductsData[$simple->getId()] = $productData;
            $configurableAttributesData[1]['values'][] = $productData;
        }
        $configProduct->setConfigurableProductsData($configurableProductsData);

        $configProduct->setConfigurableAttributesData($configurableAttributesData);
        $configProduct->setCanSaveConfigurableAttributes(true);
        $configProduct->save();
        return array('colors' => array_unique($colors), 'variants' => array_values($variants));
    }

    public function fetchSimpleProductOfConfigurable($confId)
    {
        $childProducts = Mage::getModel('catalog/product_type_configurable')->getChildrenIds($confId);
        return $childProducts;
    }

    public function getVariantList($confId, $color, $size)
    {
        $childProducts = Mage::getModel('catalog/product_type_configurable')->getChildrenIds($confId);
        $childIds = array_keys($childProducts[0]);
        $attributeColorCode = $color;
        $attributeSizeCode = $size;
        $simpleProducts = Mage::getResourceModel('catalog/product_collection')
            ->addIdFilter($childIds)
            ->addAttributeToSelect($attributeColorCode)
            ->addAttributeToSelect($attributeSizeCode)
            ->addAttributeToSelect('price');

        $variants = array();
        foreach ($simpleProducts as $i => $simple) {
            $attr = $simple->getResource()->getAttribute($color);
            $attr1 = $simple->getResource()->getAttribute($size);
            $variants[$i]['color_id'] = (int) $attr->getSource()->getOptionId($simple->getAttributeText($color));
            $variants[$i]['size_id'] = (int) $attr1->getSource()->getOptionId($simple->getAttributeText($size));
            $colors[] = (int) $attr->getSource()->getOptionId($simple->getAttributeText($color));
        }
        $variants = array_values($variants);
        $colors = array_unique($colors);
        $res = array();
        foreach ($variants as $v) {
            $x = array_search($v['color_id'], $colors);
            if ($x) {
                $res[$x]['color_id'] = $v['color_id'];
                $res[$x]['size_id'][] = $v['size_id'];
            } else {
                $res[$x]['color_id'] = $v['color_id'];
                $res[$x]['size_id'][] = $v['size_id'];
            }
        }

        $response['conf_id'] = $confId;
        $response['variants'] = array_values($res);

        //Mage::log("childProducts: ".json_encode($response), null, childProducts.log);
        return json_encode($response);
    }

    public function getProductCount($filters = null, $store = null)
    {
        $collection = Mage::getModel('catalog/product')->getCollection()
            ->addStoreFilter($this->_getStoreId($store))
            ->addAttributeToFilter('type_id', 'configurable')
            ->addAttributeToFilter('status', 1) // enabled
            ->addAttributeToFilter('xe_is_designer', 1); //is a designer product
        $totalProduct = $collection->getSize();
        return json_encode(array('size' => $totalProduct));
    }

    public function getAllProducts($filters = null, $categoryid = null, $searchstring = '', $store = null, $range = null, $loadVariants = false, $offset = 1, $limit = 10, $preDecorated = false, $color, $size, $preDecoStatus = false)
    {
        if (!$store) {
            $store = Mage::app('default')->getStore()->getStoreId();
        }
        $collection = Mage::getModel('catalog/product')->getCollection()
            ->addAttributeToSelect('*')
            ->addAttributeToFilter('status', Mage_Catalog_Model_Product_Status::STATUS_ENABLED)
            ->addAttributeToFilter('visibility', '4')
            //->addAttributeToFilter('type_id', array('in' => array('simple', 'configurable')))
            ->addAttributeToFilter('xe_is_designer', 1)
            ->joinField('is_in_stock', 'cataloginventory/stock_item', 'is_in_stock', 'product_id=entity_id', '{{table}}.stock_id=1', 'left')
            ->addAttributeToFilter('is_in_stock', array('neq' => 0))
            ->addStoreFilter($store)
            ->setPageSize($limit) // limit number of results returned
            ->setCurPage($offset); // set the offset (useful for pagination)
        if(!$preDecorated){
            $collection = $collection->addAttributeToFilter('xe_is_template', array(array('null' => true), array('neq' => 1)), 'left');
        }
        if (!$preDecoStatus) {
			$collection = $collection->addAttributeToFilter('type_id', array('in' => array('simple', 'configurable')));
        }else{
			$collection = $collection->addAttributeToFilter('type_id', array('eq' =>'configurable'));
		}
        if ($categoryid && $categoryid != '') {
            $collection = $collection->joinField('category_id', 'catalog/category_product', 'category_id', 'product_id = entity_id', null, 'left')
                ->addAttributeToFilter('category_id', $categoryid);
        }
        if ($searchstring && $searchstring != '') {
            $collection = $collection->addAttributeToFilter('name', array('like' => '%' . $searchstring . '%'));
        }

        /* $length = 0;
        $length = $collection->getSize(); */
        $pages = $collection->getLastPageNumber();
        $collection->addAttributeToSort('entity_id', Varien_Data_Collection::SORT_ORDER_ASC)->getSelect()->group('e.entity_id');

        $products = array();
        if (!empty($collection) && $offset <= $pages) {
            $counter = 0;
            foreach ($collection as $productData) {
                $products[$counter] = array(
                    'id' => $productData->getId(),
                    'name' => $productData->getName(),
                    'ptype' => $productData->getTypeId(),
                    'description' => strip_tags($productData->getDescription()),
                    'price' => $productData->getPrice(),
                    'thumbnail' => (string) Mage::helper('catalog/image')->init($productData, 'thumbnail')->resize(140),
                    'category' => $productData->getCategoryIds(),
                    'store' => $productData->getStoreIds(),
                );
                $counter++;
            }
        }
        return json_encode(array('product' => $products, 'count' => $counter));
    }

    public function getSimpleProduct($productId, $store = null, $attributes = array(), $configPid = 0, $color, $size, $identifierType = null)
    {
        if (!$store) {
            $store = Mage::app('default')->getStore()->getStoreId();
        }
        $configProductId = $configPid;
        $product = $this->_getProduct($productId, $store, $identifierType);
        $configPname = '';
        $simplePid = '';
        $images = array();
        $thumbs = array();
        $labels = array();
        $result = array();
        if ($product->getTypeId() == 'configurable') {
            $simpleProduct = Mage::getModel('catalog/product_type_configurable')
                ->getUsedProductCollection($product)
                ->addAttributeToSelect('*')
                ->joinField('is_in_stock', 'cataloginventory/stock_item', 'is_in_stock', 'product_id=entity_id', '{{table}}.stock_id=1', 'left')
                ->addAttributeToFilter('is_in_stock', array('neq' => 0))
                ->addStoreFilter($store)
                ->addAttributeToFilter('status', Mage_Catalog_Model_Product_Status::STATUS_ENABLED)
                ->addAttributeToSort('entity_id', Varien_Data_Collection::SORT_ORDER_ASC)
                ->getFirstItem();
            $simplePid = $simpleProduct->getId();
            //$simpleProduct = $this->_getProduct($simplePid, $store, $identifierType);
            $simpleProduct = Mage::getModel('catalog/product')->setStoreId($store)->load($simplePid);
            $configPname = $product->getName();
            $configPid = $product->getId();
            $productType = $product->getTypeId();
        } else {
            $simplePid = $productId;
            if ($configPid <= 0) {
                $configPids = Mage::getResourceSingleton('catalog/product_type_configurable')->getParentIdsByChild($simplePid);
                $configPid = $configPids[0];
            }
            $productResourceModel = Mage::getResourceModel('catalog/product');
            $configPname = $productResourceModel->getAttributeRawValue($configPid, 'name', $store);
            $configureProduct = Mage::getModel('catalog/product')->load($configProductId);
            $configPid = $configureProduct->getId();
            $productType = $configureProduct->getTypeId();
            $simpleProduct = Mage::getModel('catalog/product')->setStoreId($store)->load($simplePid);
        }
        $configProduct = Mage::getModel('catalog/product')->setStoreId($store)->load($configPid);
        $tierPrices = $configProduct->getFormatedTierPrice();
        $productFinalPrice = $simpleProduct->getFinalPrice();
        $tier = array();
        if (is_array($tierPrices)) {
            foreach ($tierPrices as $k => $price) {
                $tier[$k]['tierQty'] = (int) $price['price_qty'];
                $tier[$k]['percentage'] = round(100 - $price['price'] / $productFinalPrice * 100);
                $tier[$k]['tierPrice'] = number_format($price['price'], 2);
            }
        }
        $attr = Mage::getModel('catalog/resource_eav_attribute')->loadByCode('catalog_product', 'xe_is_template');
        if (null === $attr->getId()) {
            $preDecorated = 0;
        } else {
            if (function_exists('boolval')) {
                $preDecorated = boolval($product->getData("xe_is_template"));
            } else {
                $preDecorated = (bool) $product->getData("xe_is_template");
            }
        }
        if ($simpleProduct) {
            $productStock = Mage::getModel('cataloginventory/stock_item')->loadByProduct($simpleProduct);
            $productImages = $simpleProduct->getMediaGalleryImages()->setOrder('position', 'ASC');
            $productImagesLength = $productImages->getSize();
            if ($productImagesLength > 0) {
                foreach ($productImages as $_image) {
                    //$curImage = $_image->getUrl();
                    $curImage = Mage::app()->getStore($store)->getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA) . 'catalog/product' . $_image->getFile();
                    $curThumb = (string) Mage::helper('catalog/image')->init($simpleProduct, 'thumbnail', $_image->getFile())->resize(90, 90);
                    array_push($images, $curImage);
                    array_push($thumbs, $curThumb);
                    array_push($labels, $_image->getLabel());
                }
            }
            /*$productFinalPrice = $simpleProduct->getFinalPrice();
            $productPrice = Mage::helper('tax')->getPrice($simpleProduct, $productFinalPrice);
            $tax = $productPrice-$productFinalPrice;*/
            $taxCalculation = Mage::getModel('tax/calculation');
            $request = $taxCalculation->getRateRequest(null, null, null);
            $taxClassId = $product->getTaxClassId();
            $percent = $taxCalculation->getRate($request->setProductClassId($taxClassId));
            //$taxonly = (($percent)/100) *  ($simpleProduct->getPrice());
            //$tax = number_format($taxonly,2);
            $colorAttribute = $simpleProduct->getAttributeText($color);
            $colorCode = $simpleProduct->getAttributeText($color);
            if (!$colorAttribute) {
                $colorAttribute = '';
                $colorCode = '';
            }
            $sizeAttribute = $simpleProduct->getAttributeText($size);
            if (!$sizeAttribute) {
                $sizeAttribute = '';
            }
            $attr = $simpleProduct->getResource()->getAttribute($color);
            $attr1 = $simpleProduct->getResource()->getAttribute($size);
            $attributes = $simpleProduct->getAttributes();
            $result = array(
                'pid' => $configPid,
                'pidtype' => $productType,
                'pname' => $configPname,
                'shortdescription' => $simpleProduct->getShortDescription(),
                'category' => $simpleProduct->getCategoryIds(),
                'pvid' => $simpleProduct->getId(),
                'pvname' => $simpleProduct->getName(),
                'xecolor' => $colorAttribute,
                'xesize' => $sizeAttribute,
                'xeColorCode' => $colorCode,
                'xe_color_id' => $attr->getSource()->getOptionId($simpleProduct->getAttributeText($color)),
                'xe_size_id' => $attr1->getSource()->getOptionId($simpleProduct->getAttributeText($size)),
                'quanntity' => (int) $productStock->getQty(),
                'minQuantity' => (int) $productStock->getMinSaleQty(),
                'maxQuantity' => (int) $productStock->getMaxSaleQty(),
                'price' => $simpleProduct->getFinalPrice(),
                'tierPrices' => $tier,
                'taxrate' => $percent,
                'thumbsides' => $thumbs,
                'sides' => $images,
                'isPreDecorated' => $preDecorated,
                'labels' => $labels,
            );
			$customOptions = $product->getOptions();
			if($customOptions && $product->getTypeId() == 'simple'){
				$result['pidtype'] = 'custom';
			}
            foreach ($attributes as $attribute) {
                $attrCode = $attribute->getAttributeCode();
                $attrData = $attribute->getData();
                if ($attrData['is_html_allowed_on_front'] == 1) {
                    $attr = $simpleProduct->getResource()->getAttribute($attrCode);
                    $attrText = $simpleProduct->getAttributeText($attrCode);
                    $attrId = $attr->getSource()->getOptionId($simpleProduct->getAttributeText($attrCode));
                    if ($attrText) {
                        $result['attributes'][$attrCode] = $attrText;
                        $result['attributes'][$attrCode . "_id"] = $attrId;
                    }
                }
            }
        }
        return json_encode($result);
    }

    public function getVariants($productId = 0, $start = 0, $limit = 0, $store = null, $offset = 1, $color, $size, $identifierType = null)
    {
        if (!$store) {
            $store = Mage::app('default')->getStore()->getStoreId();
        }

        try {
            $ids = Mage::getResourceSingleton('catalog/product_type_configurable')
                ->getChildrenIds($productId);
            $configProduct = Mage::getModel('catalog/product');
            $configProduct->setId($productId);
            $categoryIds = $configProduct->getResource()->getCategoryIds($configProduct);
            $configProductModel = Mage::getModel('catalog/product')->setStoreId($store)->load($productId);

            $_subproducts = Mage::getModel('catalog/product')->getCollection()
                ->addIdFilter($ids)
                ->addAttributeToSelect('*')
                ->joinField('is_in_stock', 'cataloginventory/stock_item', 'is_in_stock', 'product_id=entity_id', '{{table}}.stock_id=1', 'left')
                ->addAttributeToFilter('is_in_stock', array('neq' => 0))
                ->groupByAttribute($color)
                ->addStoreFilter($store)
                ->addAttributeToFilter('status', Mage_Catalog_Model_Product_Status::STATUS_ENABLED)
            // ->addAttributeToSort('entity_id', Varien_Data_Collection::SORT_ORDER_ASC)
                ->setPageSize($limit) // limit number of results returned
                ->setCurPage($offset); // set the offset (useful for pagination)

            $_subproducts->getSelect()->join(array('attribute_options' => $tablePrefix . 'eav_attribute_option'), 'attribute_options.option_id = `at_' . $color . '`.`value`', array('attribute_options.sort_order'));
            $_subproducts->getSelect()->order('attribute_options.sort_order ASC');
            $variants = array();
            $pages = $_subproducts->getLastPageNumber();
            $productCount = 0;
            if (!empty($_subproducts) && $offset <= $pages) {
                $productCount = $_subproducts->getSize();
                foreach ($_subproducts as $childProduct) {
                    $attr = $childProduct->getResource()->getAttribute($color);
                    $attr1 = $childProduct->getResource()->getAttribute($size);
                    $color_id = $attr->getSource()->getOptionId($childProduct->getAttributeText($color));
                    $productFinalPrice = $childProduct->getFinalPrice();
                    $productPrice = Mage::helper('tax')->getPrice($childProduct, $productFinalPrice);
                    $tax = $productPrice - $productFinalPrice;
                    /*$taxCalculation = Mage::getModel('tax/calculation');
                    $request = $taxCalculation->getRateRequest(null, null, null, $store);
                    $taxClassId = $child->getTaxClassId();
                    $percent = $taxCalculation->getRate($request->setProductClassId($taxClassId));
                    $taxonly = (($percent)/100) *  ($child->getPrice());
                    $tax = number_format($taxonly,2);*/
                    $attributes = $childProduct->getAttributes();
                    $curVariant = array(
                        'id' => $childProduct->getId(),
                        'name' => $childProduct->getName(),
                        'description' => strip_tags($childProduct->getDescription()),
                        'thumbnail' => (string) Mage::helper('catalog/image')->init($configProductModel, 'thumbnail')->resize(140),
                        'price' => $productPrice,
                        'tax' => $tax,
                        'xeColor' => $childProduct->getAttributeText($color),
                        'xe_color_id' => $color_id,
                        'xe_size_id' => $attr1->getSource()->getOptionId($childProduct->getAttributeText($size)),
                        'colorUrl' => $color_id . ".png",
                        'ConfcatIds' => $categoryIds,
                    );
                    foreach ($attributes as $attribute) {
                        $attrCode = $attribute->getAttributeCode();
                        $attrData = $attribute->getData();
                        if ($attrData['is_html_allowed_on_front'] == 1) {
                            $attr = $childProduct->getResource()->getAttribute($attrCode);
                            $attrText = $childProduct->getAttributeText($attrCode);
                            $attrId = $attr->getSource()->getOptionId($childProduct->getAttributeText($attrCode));
                            if ($attrText) {
                                $curVariant['attributes'][$attrCode] = $attrText;
                                $curVariant['attributes'][$attrCode . "_id"] = $attrId;
                            }
                        }
                    }
                    array_push($variants, $curVariant);
                }
            }
            return json_encode(array('variants' => $variants, 'count' => $productCount));
        } catch (Exception $e) {
            return json_encode(array('isFault' => 1, 'faultMessage' => 'Invalid store Id'));
        }
    }

    public function getSizeAndQuantity($productId, $store = null, $simpleProductId = '', $color, $size, $identifierType = null)
    {
        if (!$store) {
            $store = Mage::app('default')->getStore()->getStoreId();
        }
        $productModel = Mage::getModel('catalog/product')->load($productId);
        if ($productModel->getTypeId() == 'configurable') {
            $ids = Mage::getResourceSingleton('catalog/product_type_configurable')
                ->getChildrenIds($productId);
            $productColor = Mage::getResourceModel('catalog/product')->getAttributeRawValue($simpleProductId, $color, $store);
            $productSize = Mage::getResourceModel('catalog/product')->getAttributeRawValue($simpleProductId, $size, $store);
            $tablePrefix = (string) Mage::getConfig()->getTablePrefix();

            $_subproducts = Mage::getModel('catalog/product')->getCollection()
                ->addIdFilter($ids)
                ->addAttributeToSelect('*')
                ->joinField('is_in_stock', 'cataloginventory/stock_item', 'is_in_stock', 'product_id=entity_id', '{{table}}.stock_id=1', 'left')
                ->joinField('qty', 'cataloginventory/stock_item', 'qty', 'product_id=entity_id', '{{table}}.stock_id=1', 'left')
                ->addAttributeToFilter('is_in_stock', array('neq' => 0))
                ->addAttributeToFilter($color, $productColor)
                ->addStoreFilter($store)
                ->addAttributeToFilter('status', Mage_Catalog_Model_Product_Status::STATUS_ENABLED)
                ->groupByAttribute($size);
            $_subproducts->getSelect()->join(array('attribute_options' => $tablePrefix . 'eav_attribute_option'), 'attribute_options.option_id = `at_' . $size . '`.`value`', array('attribute_options.sort_order'));
            $_subproducts->getSelect()->order('attribute_options.sort_order ASC');
            $configProduct = Mage::getModel('catalog/product')->setStoreId($store)->load($productId);
            $variants = array();
            $productCount = 0;
            if (!empty($_subproducts)) {
                $productCount = $_subproducts->getSize();
                $tier = array();
                foreach ($_subproducts as $childProduct) {
                    $attr = $childProduct->getResource()->getAttribute($color);
                    $attr1 = $childProduct->getResource()->getAttribute($size);
                    $productFinalPrice = $childProduct->getFinalPrice();
                    $productPrice = Mage::helper('tax')->getPrice($childProduct, $productFinalPrice);
                    $tax = $productPrice - $productFinalPrice;
                    $tierPrices = $configProduct->getFormatedTierPrice();
                    if (is_array($tierPrices)) {
                        foreach ($tierPrices as $k => $price) {
                            $tier[$k]['tierQty'] = (int) $price['price_qty'];
                            $tier[$k]['percentage'] = round(100 - $price['price'] / $productFinalPrice * 100);
                            $tier[$k]['tierPrice'] = number_format($price['price'], 2);
                        }
                    }
                    /*$taxCalculation = Mage::getModel('tax/calculation');
                    $request = $taxCalculation->getRateRequest(null, null, null, $store);
                    $taxClassId = $child->getTaxClassId();
                    $percent = $taxCalculation->getRate($request->setProductClassId($taxClassId));
                    $taxonly = (($percent)/100) *  ($child->getPrice());
                    $tax = number_format($taxonly,2);*/
                    $productQuantity = Mage::getModel("cataloginventory/stock_item")->loadByProduct($childProduct->getId());
                    $attributes = $childProduct->getAttributes();
                    $curVariant = array(
                        'simpleProductId' => $childProduct->getId(),
                        'xe_color' => $childProduct->getAttributeText($color),
                        'xe_size' => $childProduct->getAttributeText($size),
                        'xe_color_id' => $attr->getSource()->getOptionId($childProduct->getAttributeText($color)),
                        'xe_size_id' => $attr1->getSource()->getOptionId($childProduct->getAttributeText($size)),
                        'quantity' => (int) $childProduct->getQty(),
                        'tierPrices' => $tier,
                        'minQuantity' => (int) $productQuantity->getMinSaleQty(),
                        'maxQuantity' => (int) $productQuantity->getMaxSaleQty(),
                        'price' => $productPrice,
                    );
                    if ($attributes) {
                        foreach ($attributes as $attribute) {
                            $attrCode = $attribute->getAttributeCode();
                            $attrData = $attribute->getData();
                            if ($attrData['is_html_allowed_on_front'] == 1) {
                                $attr = $childProduct->getResource()->getAttribute($attrCode);
                                $attrText = $childProduct->getAttributeText($attrCode);
                                $attrId = $attr->getSource()->getOptionId($childProduct->getAttributeText($attrCode));
                                if ($attrText) {
                                    $curVariant['attributes'][$attrCode] = $attrText;
                                    $curVariant['attributes'][$attrCode . "_id"] = $attrId;
                                }
                            }
                        }
                    }
                    array_push($variants, $curVariant);
                }
            }
        } else if ($productModel->getTypeId() == 'simple') {
            $variants = array();
            $colorAttribute = $productModel->getAttributeText($color);
            if (!$colorAttribute) {
                $colorAttribute = '';
            }
            $sizeAttribute = $productModel->getAttributeText($size);
            if (!$sizeAttribute) {
                $sizeAttribute = '';
            }
            $attr = $productModel->getResource()->getAttribute($color);
            $attr1 = $productModel->getResource()->getAttribute($size);
            $productQuantity = Mage::getModel("cataloginventory/stock_item")->loadByProduct($productModel->getId());
            $productFinalPrice = $productModel->getFinalPrice();
            $productPrice = Mage::helper('tax')->getPrice($productModel, $productFinalPrice);
            $curVariant = array(
                'simpleProductId' => $productModel->getId(),
                'xe_color' => $colorAttribute,
                'xe_size' => $sizeAttribute,
                'xe_color_id' => $attr->getSource()->getOptionId($productModel->getAttributeText($color)),
                'xe_size_id' => $attr1->getSource()->getOptionId($productModel->getAttributeText($size)),
                'quantity' => (int) $productModel->getQty(),
                'minQuantity' => (int) $productQuantity->getMinSaleQty(),
                'price' => $productPrice,
            );
            array_push($variants, $curVariant);
        }
        return json_encode(array('quantities' => $variants));
    }

    public function getSizeVariants($productId, $store = null, $simpleProductId = '', $color, $size, $identifierType = null)
    {
        if (!$store) {
            $store = Mage::app('default')->getStore()->getStoreId();
        }

        $ids = Mage::getResourceSingleton('catalog/product_type_configurable')
            ->getChildrenIds($productId);
        $tablePrefix = (string) Mage::getConfig()->getTablePrefix();
        $_subproducts = Mage::getModel('catalog/product')->getCollection()
            ->addIdFilter($ids)
            ->addAttributeToSelect('*')
            ->joinField('is_in_stock', 'cataloginventory/stock_item', 'is_in_stock', 'product_id=entity_id', '{{table}}.stock_id=1', 'left')
            ->joinField('qty', 'cataloginventory/stock_item', 'qty', 'product_id=entity_id', '{{table}}.stock_id=1', 'left')
            ->addAttributeToFilter('is_in_stock', array('neq' => 0))
            ->addStoreFilter($store)
            ->addAttributeToFilter('status', Mage_Catalog_Model_Product_Status::STATUS_ENABLED)
            ->groupByAttribute($size);
        $_subproducts->getSelect()->join(array('attribute_options' => $tablePrefix . 'eav_attribute_option'), 'attribute_options.option_id = `at_' . $size . '`.`value`', array('attribute_options.sort_order'));
        $_subproducts->getSelect()->order('attribute_options.sort_order ASC');

        $variants = array();
        $productCount = 0;
        if (!empty($_subproducts)) {
            $productCount = $_subproducts->getSize();
            foreach ($_subproducts as $childProduct) {
                $attr = $childProduct->getResource()->getAttribute($color);
                $attr1 = $childProduct->getResource()->getAttribute($size);
                $productFinalPrice = $childProduct->getFinalPrice();
                $productPrice = Mage::helper('tax')->getPrice($childProduct, $productFinalPrice);
                $productQuantity = Mage::getModel("cataloginventory/stock_item")->loadByProduct($childProduct->getId());
                $tax = $productPrice - $productFinalPrice;
                $attributes = $childProduct->getAttributes();
                $curVariant = array(
                    'simpleProductId' => $childProduct->getId(),
                    'xe_color' => $childProduct->getAttributeText($color),
                    'xe_size' => $childProduct->getAttributeText($size),
                    'xe_color_id' => $attr->getSource()->getOptionId($childProduct->getAttributeText($color)),
                    'xe_size_id' => $attr1->getSource()->getOptionId($childProduct->getAttributeText($size)),
                    'quantity' => (int) $childProduct->getQty(),
                    'minQuantity' => (int) $productQuantity->getMinSaleQty(),
                    'price' => $productPrice,
                );
                foreach ($attributes as $attribute) {
                    $attrCode = $attribute->getAttributeCode();
                    $attrData = $attribute->getData();
                    if ($attrData['is_html_allowed_on_front'] == 1) {
                        $attr = $childProduct->getResource()->getAttribute($attrCode);
                        $attrText = $childProduct->getAttributeText($attrCode);
                        $attrId = $attr->getSource()->getOptionId($childProduct->getAttributeText($attrCode));
                        if ($attrText) {
                            $curVariant['attributes'][$attrCode] = $attrText;
                            $curVariant['attributes'][$attrCode . "_id"] = $attrId;
                        }
                    }
                }
                array_push($variants, $curVariant);
            }
        }
        return json_encode(array('quantities' => $variants));
    }

    public function getCategories($store = null)
    {
        try {
            /* $collection = Mage::getModel('catalog/category')->getCollection()
            ->addAttributeToSelect('*')//or you can just add some attributes
            ->addAttributeToFilter('level', 2)//2 is actually the first level
            ->addAttributeToFilter('is_active', 1); */
            $rootCategoryId = Mage::app()->getStore($store)->getRootCategoryId();
            $rootpath = Mage::getModel('catalog/category')
                ->setStoreId($store)
                ->load($rootCategoryId)
                ->getPath();
            $collection = Mage::getModel('catalog/category')->setStoreId($store)
                ->getCollection()
                ->addAttributeToSelect('*')
                ->addAttributeToFilter('level', 2)
                ->addAttributeToFilter('is_active', 1)
                ->addAttributeToFilter('path', array("like" => $rootpath . "/" . "%"))
                ->addAttributeToSort('position');
            //->addAttributeToSort('name', ASC); for category name sorting
            $categories = array();
            if ($collection->count() > 0) {
                foreach ($collection as $category) {
                    $categories[] = array(
                        'id' => $category->getId(),
                        'name' => $category->getName(),
                    );
                }
            }
            return json_encode(array('categories' => $categories));
        } catch (Exception $e) {
            return json_encode(array('isFault' => 1, 'faultMessage' => 'Invalid store Id'));
        }

    }

    public function getsubCategories($catid, $store = null)
    {
        try {
            //get 2nd level sub category
            // $catid = 3;
            // Mage::log("store: ".$store." catid: ".$catid, null, catid.log);
            $cat = Mage::getModel('catalog/category')->load($catid);
            $subcats = $cat->getChildren();
            $subcategories = array();
            if (count($subcats) > 0) {
                foreach (explode(',', $subcats) as $subCatid) {
                    $_category = Mage::getModel('catalog/category')->load($subCatid);
                    if ($_category->getIsActive()) {
                        $subcategories[] = array('id' => $_category->getId(), 'name' => $_category->getName());
                    }
                    //get 3rd level sub sub category
                    $sub_cat = Mage::getModel('catalog/category')->load($_category->getId());
                    $sub_subcats = $sub_cat->getChildren();
                    if (count($sub_subcats) > 0) {
                        foreach (explode(',', $sub_subcats) as $sub_subCatid) {
                            $_sub_category = Mage::getModel('catalog/category')->load($sub_subCatid);
                            if ($_sub_category->getIsActive()) {
                                $subcategories[] = array('id' => $_sub_category->getId(), 'name' => $_sub_category->getName());
                            }
                        }
                    }

                    //get 4th level sub sub category
                    $sub_sub_cat = Mage::getModel('catalog/category')->load($sub_subCatid);
                    if (count($sub_sub_cat) > 0) {
                        $sub_sub_subcats = $sub_sub_cat->getChildren();
                        foreach (explode(',', $sub_sub_subcats) as $sub_sub_subCatid) {
                            $_sub_sub_category = Mage::getModel('catalog/category')->load($sub_sub_subCatid);
                            if ($_sub_sub_category->getIsActive()) {
                                $categories[] = array('id' => $_sub_sub_category->getId(), 'name' => $_sub_sub_category->getName());
                            }
                        }
                    }
                }
            }
            sort($subcategories);
            return json_encode(array('subcategories' => $subcategories));
        } catch (Exception $e) {
            return json_encode(array('isFault' => 1, 'faultMessage' => 'Invalid store Id'));
        }
    }

    public function getCurrentUser($customerId, $attributes = null)
    {
        $customer = Mage::getModel('customer/customer')->load($customerId);
        $customerId = 0;
        //Mage::getSingleton('customer/session')->getCustomer()
        if (Mage::getSingleton('customer/session')->getId() && Mage::getSingleton('customer/session')->getId() != '') {
            $customerId = Mage::getSingleton('customer/session')->getId();
        } else if (Mage::getSingleton('customer/session')->getCustomer()) {
            $customerId = Mage::getSingleton('customer/session')->getCustomer()->getId();
        }

        if (!$customer->getId()) {
            return json_encode(array('is_Fault' => 1, 'faultMessage' => 'Customer not exist', 'customerId' => $customerId));
        }

        if (!is_null($attributes) && !is_array($attributes)) {
            $attributes = array($attributes);
        }

        $result = array();

        foreach ($this->_mapAttributes as $attributeAlias => $attributeCode) {
            $result[$attributeAlias] = $customer->getData($attributeCode);
        }
        foreach ($this->getAllowedAttributes($customer, $attributes) as $attributeCode => $attribute) {
            $result[$attributeCode] = $customer->getData($attributeCode);
        }
        return json_encode($result);
    }

    /**
     *
     * @api
     * @param int $store.
     * @return string all attributes in a json format.
     */
    public function getAttributes($store)
    {
        $result = array();
        $attributes = Mage::getResourceModel('catalog/product_attribute_collection')->getItems();
        $i = 0;
        foreach ($attributes as $attribute) {
            $attrData = $attribute->getData();
            if ($attrData['is_html_allowed_on_front'] == 1) {
                $result[$i]['id'] = $attribute->getAttributeId();
                $result[$i]['attribute_code'] = $attribute->getAttributecode();
                $i++;
            }
        }
        return json_encode($result);
    }

    /**
     *
     * @api
     * @param int $productId.
     * @param int $store.
     * @return string all custom options in a json format.
     */
    public function getCustomOption($productId, $store = null)
    {
        $result = array();
        $product = Mage::getModel('catalog/product')->setStoreId($store)->load($productId);
        $customOptions = $product->getOptions();
        $i = 0;
        if (!empty($customOptions)) {
            foreach ($customOptions as $customOption) {
                $values = $customOption->getValues();
                $result[$i]['option_title'] = $customOption['title'];
                $result[$i]['option_id'] = $customOption['option_id'];
                $result[$i]['type'] = $customOption->getType();
                $result[$i]['is_require'] = $customOption['is_require'];
                foreach ($values as $value) {
                    $data = $value->getData();
                    if ($data) {
                        $result_options['option_type_id'] = $data['option_type_id'];
                        $result_options['sku'] = ($data['sku']) ? $data['sku'] : '';
                        $result_options['sort_order'] = $data['sort_order'];
                        $result_options['title'] = $data['title'];
                        $result_options['price'] = number_format((float) $data['price'], 2, '.', '');
                    }
                    $result[$i]['option_values'][] = $result_options;
                }
                $i++;
            }
        }
        return json_encode($result);
    }
}
