<?php
$absPath = getcwd();
$final = str_replace('\\', '/', $absPath);
$final = str_replace("/xetool/install/magento", "", $final);
$path = $final . "/xetool/api/vendor/";
require_once $path . 'BarcodeGenerator.php';
require_once $path . 'BarcodeGeneratorPNG.php';

class Html5design_Cedapi_Model_Observer
{

    private function getAssetPath()
    {

        $path = $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI']; //$_SERVER['HTTP_REFERER'];
        $protocol = strchr($path, '//', true);
        $path = str_replace($protocol . '//', '', $path);
        $path = strchr($path, '/', true);
        $file_folder_name = str_replace(array('www.', '.'), array('', '_'), $path);
        $asset_path = '/assets/' . $file_folder_name;
        return $asset_path;
    }

    public function checkoutCartPredispatch(Varien_Event_Observer $observer)
    {
        $quoteId = intval(Mage::app()->getFrontController()->getRequest()->getParam('quoteId'));
        if ($quoteId == '' || $quoteId <= 0) {
            $quoteId == 0;
        }
        if ($quoteId && $quoteId > 0) {
            try {
                $cartsess = Mage::getSingleton('checkout/session');
                $customer = Mage::getSingleton('customer/session');
                /* if(!$customer->isLoggedIn()) {
                $cartsess->setCustomer($customer);
                } */
                $cartsess->setQuoteId($quoteId);
                $cartsess->setLoadInactive(true);
                $quote = $cartsess->getQuote();
                $quote->setIsActive(true);
                $quote->save();
                Mage::getSingleton('core/session')->addSuccess('Product is successfully added in to cart.');
            } catch (Exception $e) {
                Mage::getSingleton('core/session')->addError($e->getMessage());
            }
            Mage::app()->getResponse()->setRedirect(Mage::getUrl('checkout/cart'))->sendResponse();
        }
    }

    public function checkoutSuccess(Varien_Event_Observer $observer)
    {
        $order_id = $observer->getData('order_ids');
        $order = Mage::getModel('sales/order')->load($order_id);
        $increment_id = $order->increment_id;
        $orderId = $order->getId();
        $params['order_id'] = $orderId;
        $baseUrl = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB);
        $url = $baseUrl . 'xetool/api/index.php?reqmethod=downloadOrderDetail';
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        $response = curl_exec($ch);
        curl_close($ch);

        Mage::getSingleton('checkout/session')->clear();
        $expire = time() + 60 * 60 * 24 * 30; //30 days
        setcookie("quoteId", "", $expire, "/");
		$_COOKIE['quoteId'] = "";
    }

    public function salesQuoteSaveAfter($observer)
    {
        $quote = $observer->getEvent()->getQuote();
        /* @var $quote Mage_Sales_Model_Quote */
        if ($quote->getIsCheckoutCart()) {
            Mage::getSingleton('checkout/session')->getQuoteId($quote->getId());
        }
        $expire = time() + 60 * 60 * 24 * 30;
        setcookie("quoteId", $quote->getId(), $expire, "/");
    }

    public function addPostData(Varien_Event_Observer $observer)
    {
        $action = Mage::app()->getFrontController()->getAction();
        if (gettype($action) == object) {
            if ($action->getFullActionName() == 'checkout_cart_add') {
                $item = $observer->getProduct();
                $additionalOptions = array();
                $additionalOptions[] = array(
                    'label' => 'line_item_separate',
                    'value' => 143,
                );
                $item->addCustomOption('additional_options', serialize($additionalOptions));
            }
        }
    }

    public function addDownloadButton(Varien_Event_Observer $observer)
    {
        $reff_status = 0;
        $block = $observer->getEvent()->getBlock();
        if ($block instanceof Mage_Adminhtml_Block_Sales_Order_View) {
            $orderId = Mage::app()->getRequest()->getParam('order_id');
            $order = Mage::getModel('sales/order')->load($orderId);
            $items = $order->getAllItems();
            $incrementId = $order->getIncrementId();
            foreach ($items as $item) {
                if (!$item->getParentItemId()) {
                    $ref_id = $item->getCustom_design();
                    if ($reff_status == 0 && $ref_id != null) {
                        $reff_status = 1;
                    }

                }
            }
            if ($reff_status) {
                $baseUrl = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB);
                $block->addButton('download',
                    array('label' => Mage::helper('cedapi')->__('Download'),
                        'onclick' => "downloadOrder('$orderId','$incrementId','$baseUrl')", 'class' => 'go'));
            }

        }
    }
    public function ClearCookie(Varien_Event_Observer $observer)
    {
        setcookie('quoteId', "", time() - 60 * 60 * 24 * 30, '/');
        $_COOKIE['quoteId'] = "";
    }
    public function catalogProductView(Varien_Event_Observer $observer)
    {
        return true;
    }

    /**
     * Sent mail to customer while order placed
     * @param (String)toMail
     * @param (String)html
     * @return status successful and failure messages.
     */
    public function sentCustomerMail($toMail, $html)
    {
        $boundary = str_replace(" ", "", date('l jS \of F Y h i s A'));
        $fromMail = SENDER_EMAIL;
        $msg = array();
        $subjectMail = "Thank you for Order";
        $headersMail .= 'From: ' . $fromMail . "\r\n" . 'Reply-To: ' . $fromMail . "\r\n";
        $headersMail .= "MIME-Version: 1.0\r\n";
        $headersMail .= "Content-Type: multipart/alternative; boundary = \"" . $boundary . "\"\r\n\r\n";
        $headersMail .= "--" . $boundary . "\r\n";
        $headersMail .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
        $headersMail .= "Content-Transfer-Encoding: base64\r\n\r\n";
        $headersMail .= rtrim(chunk_split(base64_encode($html)));
        if (mail($toMail, $subjectMail, "", $headersMail)) {
            $msg = array("status" => 'Your mail has been sent successfully for Orders.');
        } else {
            $msg = array("status" => 'Unable to send email. Please try again.');
        }
        return $msg;
    }
}
