<?php
class Html5design_Cedapi_Block_Product_Customize extends Mage_Catalog_Block_Product_View
{

    public $superAttributes = array();

    public function isCustomizeButton()
    {
        $product = $this->getProduct();
        if ($product->getTypeId() == 'configurable' || $product->getTypeId() == 'simple') {
            $productId = $product->getId();
            $productModel = Mage::getModel('catalog/product')->load($productId);
            if (function_exists('boolval')) {
                $xeIsDesigner = boolval($productModel->getData("xe_is_designer"));
            } else {
                $xeIsDesigner = (bool) $productModel->getData("xe_is_designer");
            }
            if ($xeIsDesigner) {
                return true;
            }
        }
        return false;
    }

    public function getSuperAttributes()
    {
        $product = $this->getProduct();
        $configurableAttributeCollection = $product->getTypeInstance()->getConfigurableAttributes();
        $super_attrs_code = array();
        foreach ($configurableAttributeCollection as $attribute) {
            $super_attrs[$attribute->getProductAttribute()->getAttributeCode()] = $attribute->getProductAttribute()->getId();
            $super_attrs_code[] = $attribute->getProductAttribute()->getAttributeCode();
        }
        $this->superAttributes = $super_attrs_code;
    }
    public function isPredecoProduct()
    {
        $product = $this->getProduct();
        $attr = Mage::getModel('catalog/resource_eav_attribute')->loadByCode('catalog_product', 'xe_is_template');
        if (null === $attr->getId()) {
            $preDecoratedProduct = 0;
        } else {
            if (function_exists('boolval')) {
                $preDecoratedProduct = boolval($product->getData("xe_is_template"));
            } else {
                $preDecoratedProduct = (bool) $product->getData("xe_is_template");
            }
        }
        return $preDecoratedProduct;
    }
    public function getMinimumQuantity()
    {
        $minimumQty = 1;
        $product = $this->getProduct();
        if ($product->getTypeId() == 'configurable') {
            $configurable = Mage::getModel('catalog/product_type_configurable')->setProduct($product);
            $simpleCollection = $configurable
                ->getUsedProductCollection()
                ->addAttributeToSelect('*')
                ->addFilterByRequiredOptions();
            if (!empty($simpleCollection)) {
                foreach ($simpleCollection as $simple) {
                    $minimumQty = intval($simple->getStockItem()->getMinSaleQty());
                }
            }
        } else if ($product->getTypeId() == 'simple') {
            $stock = Mage::getModel('cataloginventory/stock_item')->loadByProduct($product);
            $minimumQty = $stock->getMinSaleQty();
        }
        return $minimumQty;
    }

}
