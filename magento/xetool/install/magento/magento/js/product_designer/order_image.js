jQuery(document).ready(function() {
    var pathArray = location.href.split('/'),
        protocol = pathArray[0],
        host = pathArray[2],
        host2nd = pathArray[3],
        ul = protocol + '//' + host + '/';
    var url = ul + 'xetool/api/index.php';
    var refids = "";
    jQuery("#my-orders-table tbody").each(function() {
        var refid = jQuery(this).find(".refid").val();
        if (refid != undefined) {
            if (refids == "") refids = refid;
            else refids += "," + refid;
        }
    });
    var data = {
        reqmethod: 'getCartPreviewImages',
        refids: refids
    };
    jQuery.ajax({ //Process the form using $.ajax()
        type: 'POST', //Method type
        url: url, //Your form processing file url
        data: data, //Forms name
        dataType: 'html',
        success: function(data) {
            var jsonObj = jQuery.parseJSON(data);
            if (!jsonObj.hasOwnProperty('status')) {
                jQuery("#my-orders-table tbody tr td:first-child ").each(function(index) {
                    var sid = jQuery(this).parent().parent().find(".refid").val();
                    //jQuery(this).find("img").hide();
                    var obj = jQuery(this);
                    if (sid != undefined) {
                        jQuery.each(jsonObj, function(key, val) {
                            if (key == sid && val.length > 0) {
                                var count = 0;
                                jQuery.each(val, function(imgKey, imgObj) {
                                    count++;
                                    if (count == 1) {
                                        /* jQuery(obj).find("img").attr("src",imgObj['svg']);
                                        jQuery(obj).find("img").show(); */
                                        jQuery(obj).find("a").remove();
                                    } //else{
                                    //var newElement='<a style="float:left;width:80px" class="product-image" title="" href="javascript:void(0)">'+'		<img width="75" hei1ht="75" class="previewimg" style="display:block;" rel="'+sid+'" alt="" src="'+imgObj['svg']+'">'+'</a>';
                                    var newElement = '<a class="product-customize-image" title="" href="javascript:void(0)">' + '<object  class="previewimg" style="display:block; -webkit-transform: scale3d(0.15, 0.15, 0.15); -moz-transform: scale3d(0.15, 0.15, 0.15); -o-transform: scale3d(0.15, 0.15, 0.15); -s-transform: scale3d(0.15, 0.15, 0.15); -moz-transform-origin: 0 0;-o-transform-origin: 0 0;-webkit-transform-origin: 0 0;transform-origin: 0 0; position:absolute;" rel="' + sid + '" alt="" data="' + imgObj['svg'] + '" type="image/svg+xml"></object>' + '</a>';
                                    //var newElement='<a style="float:left;width:80px" class="product-image" title="" href="javascript:void(0)">'+'		<div width="75" hei1ht="75" class="previewimg" style="display:block;" rel="'+sid+'" alt=""></div>'+'</a>';
                                    jQuery(obj).append(newElement);
                                    //jQuery(obj).find(".previewimg").load(imgObj['svg'], null, function() {});
                                    //}								
                                });
                            }
                        });
                    }
                });
            }
        },
        error: function(xhr, status, error) {
            console.log(xhr);
        }
    });
    // event.preventDefault(); //Prevent the default submit
});