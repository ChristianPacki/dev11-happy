<?php
require_once 'function.php';
require_once 'storeFunction.php';
$step = array(6, 3);
checkStepStatus(array('checkStartInstall', 'checkAttributeCreate', '', 'checkIntegrationStatus', 'checkWriteXMLToApp'));
checkCurrentStep('checkProductCreationStatus', 3);

if (isset($_POST['create_cms']) && $_POST['create_cms'] == 'Next') {
    createCms();
    $returnValue = createProduct();
    if ($returnValue['0'] == 0) {
        $errorMsg = $returnValue['1'];
    } else {
        header("location:create-soap-user.php");
        exit;
    }
}

require_once 'header.php';
?>
<form name='create_cms' id='create_cms' class="setdb_form" method="post" action="">
<div class="content-box">
    <div class="row">
    <div class="col-sm-12">
            <?php if (isset($errorMsg)) {?>
            <div class="error-msg">
            <?php echo "The following Errors occured:<br/>" . nl2br($errorMsg); ?>
            </div>
            <?php }?>
        </div>
        <div class="col-sm-12">
            <h4>A page will be created to display the designer tool in an iFrame.<br />
                    A dummy product named 'Inkxe test product' is created.<br />
                    You may delete this product from your store admin after installation.</h4>
        </div>
    </div>
</div>
<div class="row m-t-sm">
    <div class="col-sm-12">
        <input type="submit" name="create_cms" value="Next" class="btn btn-lg btn-aqua m-w-150 pull-right" />
    </div>
</div>
</div>
<?php require_once 'footer.php';?>
</body>
</html>

