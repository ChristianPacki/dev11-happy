<?php 
	/* Redirect to install directory */
	$currDir = getcwd();
	$files = scandir($currDir);
	$directories = '';
	foreach($files as $file){
	   	if(($file != '.') && ($file != '..')){
	      	if(is_dir('./'.$file)){
	         	$directories = $file;
	      	}
	   	}
	}
	if(file_exists($directories)){
		header("Location:".$directories);	
		exit;
	}else{
		die("No files to install");
	}
