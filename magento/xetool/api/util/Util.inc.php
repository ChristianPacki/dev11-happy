<?php

class UTIL extends REST {

    //Designer Tool DATABASE DETAILS
    const DB_SERVER = SERVER;
    const DB_USER = USER;
    const DB_PASSWORD = PASSWORD;
    const DB_NAME = DBNAME;
    const DB_PORT = '';
    const APIURL = APIURL;
    const APIUSER = APIUSER;
    const APIPASS = APIPASS;
    const TABLE_PREFIX = TABLE_PREFIX;

    public $db = null;
    public $proxy = null;
    public $storeApiLogin = false;
    public $lsStore_type = false;

    public function __construct() {
        parent::__construct();
        if (DBNAME != '') {
            $this->dbConnect();
        }
        $this->setStoreType();
    }

    ####################################################
    ############## syncOrdersZip ###################
    ####################################################

    public function dbConnect() {
        if (self::DB_PORT == '') {
            $db = mysqli_connect(self::DB_SERVER, self::DB_USER, self::DB_PASSWORD);
            if ($db) {
                mysqli_select_db($db, self::DB_NAME) or die('ERRROR:' . mysqli_error());
                $this->db = $db;
            }
        } else {
            $db = mysqli_connect(self::DB_SERVER, self::DB_USER, self::DB_PASSWORD, self::DB_NAME, self::DB_PORT) or die('ERRROR:' . mysqli_error());
            if ($db) {
                $this->db = $db;
            }
        }
        mysqli_set_charset($this->db, 'utf8mb4');
        mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
    }

    ####################################################
    ############## syncOrdersZip ###################
    ####################################################

    public function executeFetchAssocQuery($query, &$params = array()) {
        try {
            if (!$this->db) {
                $this->dbConnect();
            }

            if (count($params) > 0) {
                $stmt = $this->db->prepare($query);
                call_user_func_array([$stmt, 'bind_param'], $params);
                $stmt->execute();
                $result = $stmt->get_result();
            } else {
                $result = mysqli_query($this->db, $query);
            }
            $rows = array();
            if (mysqli_num_rows($result) > 0) {
                while ($row = mysqli_fetch_array($result)) {
                    array_push($rows, $row);
                }
                return $rows;
            } else {
                return array();
            }
        } catch (Exception $e) {
            $response = array();
            $response['Caught exception'] = $e->getMessage();
            $this->response($this->json($response), 200);
        }
    }

    ####################################################
    ############## syncOrdersZip ###################
    ####################################################

    public function executeGenericDQLQuery($query, &$params = array()) {
        try {
            if (!$this->db) {
                $this->dbConnect();
            }

            if (count($params) > 0) {
                $stmt = $this->db->prepare($query);
                call_user_func_array([$stmt, 'bind_param'], $params);
                $stmt->execute();
                $result = $stmt->get_result();
            } else {
                $result = mysqli_query($this->db, $query);
            }
            $rows = array();
            if (mysqli_num_rows($result) > 0) {
                while ($row = mysqli_fetch_array($result)) {
                    array_push($rows, $row);
                }
                return $rows;
            } else {
                return array();
            }
        } catch (Exception $e) {
            $response = array();
            $response['Caught exception'] = $e->getMessage();
            $this->response($this->json($response), 200);
        }
    }

    ####################################################
    ############## syncOrdersZip ###################
    ####################################################

    public function executeGenericDMLQuery($query) {
// Delete, Update
        try {
            if (!$this->db) {
                $this->db = mysqli_connect(self::DB_SERVER, self::DB_USER, self::DB_PASSWORD);
            }

            mysqli_set_charset($this->db, 'utf8mb4');
            $result = mysqli_query($this->db, $query);
            if (mysqli_errno($this->db) != 0) {
                throw new Exception("Error   :" . mysqli_errno($this->db) . "   :  " . mysqli_error($this->db));
            } else {
                return $result;
            }
        } catch (Exception $e) {
            $response = array();
            $response['Caught exception'] = $e->getMessage();
            //echo json_encode($response);
            $this->response($this->json($response), 200);
        }
    }

    ####################################################
    ############## syncOrdersZip ###################
    ####################################################

    public function executeGenericInsertQuery($query) {
        try {
            if (!$this->db) {
                $this->db = mysqli_connect(self::DB_SERVER, self::DB_USER, self::DB_PASSWORD);
            }

            mysqli_set_charset($this->db, 'utf8mb4');
            $result = mysqli_query($this->db, $query);
            if (mysqli_errno($this->db) != 0) {
                throw new Exception("Error   :" . mysqli_errno($this->db) . "   :  " . mysqli_error($this->db));
            }
            return mysqli_insert_id($this->db);
        } catch (Exception $e) {
            $response = array();
            $response['Caught exception'] = $e->getMessage();
            //echo json_encode($response);
            $this->response($this->json($response), 200);
        }
    }

    ####################################################
    ############## syncOrdersZip ###################
    ####################################################

    public function executeGenericCountQuery($query) {
        try {
            if (!$this->db) {
                $this->db = mysqli_connect(self::DB_SERVER, self::DB_USER, self::DB_PASSWORD);
            }

            $result = mysqli_query($this->db, $query);
            $count = mysqli_num_rows($result);
            if (mysqli_errno($this->db) != 0) {
                throw new Exception("Error   :" . mysqli_errno($this->db) . "   :  " . mysqli_error($this->db));
            }
            return $count;
        } catch (Exception $e) {
            $response = array();
            $response['Caught exception'] = $e->getMessage();
            //echo json_encode($response);
            $this->response($this->json($response), 200);
        }
    }

    ####################################################
    ############## syncOrdersZip ###################
    ####################################################

    public function executeEscapeStringQuery($str) {
        try {
            if (!$this->db) {
                $this->db = mysqli_connect(self::DB_SERVER, self::DB_USER, self::DB_PASSWORD);
            }

            $result = mysqli_real_escape_string($this->db, $str);
            if (mysqli_errno($this->db) != 0) {
                throw new Exception("Error   :" . mysqli_errno($this->db) . "   :  " . mysqli_error($this->db));
            }
            return $result;
        } catch (Exception $e) {
            $response = array();
            $response['Caught exception'] = $e->getMessage();
            //echo json_encode($response);
            $this->response($this->json($response), 200);
        }
    }

    ####################################################
    ############## syncOrdersZip ###################
    ####################################################

    public function closeConnection() {
        if ($this->db) {
            mysqli_close($this->db);
        }
    }

    ####################################################
    ############## syncOrdersZip ###################
    ####################################################

    public function formatJson($jsonData, $slash) {
        $formatted = $jsonData;
        if ($slash == 0) {
            $formatted = str_replace('"{', '{', $formatted);
            $formatted = str_replace('}"', '}', $formatted);
            $formatted = str_replace('\n', '<br/>', $formatted);
            $formatted = str_replace('\\', '', $formatted);
        }
        return $formatted;
    }

    ####################################################
    ############## syncOrdersZip ###################
    ####################################################

    public function json($data, $slash = 0) {
        if (is_array($data)) {
            $formatted = json_encode($data, JSON_UNESCAPED_UNICODE);
            return $this->formatJson($formatted, $slash);
        }
    }

    /**
     *
     * date created 13-09-2017(dd-mm-yy)
     * date modified (dd-mm-yy)
     * Convert to utf8
     *
     * @param (String)data
     * @return utf8 encoded data
     *
     */
    public function arrayUtf8Encode($data) {
        if (is_string($data))
            return utf8_encode($dat);
        if (!is_array($data))
            return $data;
        $result = array();
        foreach ($data as $i => $d)
            $result[$i] = self::arrayUtf8Encode($d);
        return $result;
    }

    ####################################################
    ############## syncOrdersZip ###################
    ####################################################

    public function clearArray($arr) {
        unset($arr);
        $arr = array();
        return $arr;
    }

    ####################################################
    ############## syncOrdersZip ###################
    ####################################################

    public function processApi() {
        $func = '';
        if (isset($_REQUEST['service'])) {
            $func = strtolower(trim(str_replace("/", "", $_REQUEST['service'])));
        } else if (isset($_REQUEST['reqmethod'])) {
            $func = strtolower(trim(str_replace("/", "", $_REQUEST['reqmethod'])));
        }

        if ($func) {
            //if(function_exists($func))
            if (method_exists($this, $func)) {
                $this->$func();
            } else {
                $this->log('invalid service:' . $func, true, 'log_invalid.txt');
                $this->response('invalid service', 406);
            }
        }
    }

    ####################################################
    ############## syncOrdersZip ###################
    ####################################################

    public function isValidCall($apiKey) {
        $flag = false;
        $apiKey = mysqli_real_escape_string($this->db, $apiKey);

        $sql = "SELECT api_key FROM " . TABLE_PREFIX . "api_data WHERE api_key ='$apiKey' ";
        $result = mysqli_query($this->db, $sql);
        if (mysqli_num_rows($result) > 0) {
            $rows = mysqli_fetch_array($result, MYSQLI_ASSOC);
            $apiKeyDB = $rows['api_key'];
            $flag = true;
        }
        return $flag;
    }

    ####################################################
    ############## syncOrdersZip ###################
    ####################################################

    public function log($text, $append = true, $fileName = '') {
        $file = 'log.log';
        if ($fileName) {
            $file = $fileName;
        }

        // Write the contents to the file,
        // using the FILE_APPEND flag to append the content to the end of the file
        // and the LOCK_EX flag to prevent anyone else writing to the file at the same time
        //file_put_contents($file, $text, FILE_APPEND | LOCK_EX);

        if ($append) {
            file_put_contents($file, $text . PHP_EOL, FILE_APPEND | LOCK_EX);
        } else {
            file_put_contents($file, $text);
        }
    }

    ####################################################
    ############## syncOrdersZip ###################
    ####################################################

    public function getCurrentUrl($full = false) {
        $s = &$_SERVER;
        $ssl = (!empty($s['HTTPS']) && $s['HTTPS'] == 'on') ? true : false;
        $sp = strtolower($s['SERVER_PROTOCOL']);
        $protocol = substr($sp, 0, strpos($sp, '/')) . (($ssl) ? 's' : '');
        $port = $s['SERVER_PORT'];
        $port = ((!$ssl && $port == '80') || ($ssl && $port == '443')) ? '' : ':' . $port;
        $host = isset($s['HTTP_X_FORWARDED_HOST']) ? $s['HTTP_X_FORWARDED_HOST'] : isset($s['HTTP_HOST']) ? $s['HTTP_HOST'] : $s['SERVER_NAME'];
        $uri = $protocol . '://' . $host;
        if ($full) {
            $uri = $protocol . '://' . $host . $port . $s['REQUEST_URI'];
        }

        $segments = explode('?', $uri, 2);
        $url = $segments[0];
        $url .= TOOL_CONTAINER_DIR;
        return $url;
    }

    ####################################################
    ############## syncOrdersZip ###################
    ####################################################

    public function getDefaultStoreId() {
        $url = $this->getCurrentUrl();
        $url = explode('/', $url);
        $result = $url[2];
        $result = preg_replace('/^www\./', '', $result);
        $sql = "Select store_id FROM " . TABLE_PREFIX . "domain_store_rel Where domain_name='" . $result . "' limit 1";
        $data = $this->executeFetchAssocQuery($sql);
        $domain = $data[0]['store_id'];
        return (!empty($data)) ? $domain : 0;
    }

    /**
     *
     * date created 09-02-2017(dd-mm-yy)
     * date modified (dd-mm-yy)
     * Get store attributes
     */
    public function getStoreAttributes($result) {
        if (!isset($result) && empty($result))
            $result = $this->_request['result'];
        $sql = "Select attr_value FROM " . TABLE_PREFIX . "store_attributes Where attr_key='" . $result . "'";
        $data = $this->executeFetchAssocQuery($sql);
        $attribute = $data[0]['attr_value'];
        if ($this->_request['result']) {
            $this->response(($attribute), 200);
        } else {
            return ($attribute);
        }
    }

    ####################################################
    ############## syncOrdersZip ###################
    ####################################################

    public function getFileContents($url) {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HEADER, false);
        $data = curl_exec($curl);
        curl_close($curl);
        return $data;
    }

    ####################################################
    ############## syncOrdersZip ###################
    ####################################################

    public function executePrepareBindQuery($query, &$params = array(), $returnType = 'array') {
        try {
            if (!$this->db) {
                $this->dbConnect();
            }

            if (count($params) > 0) {
                $stmt = $this->db->prepare($query);
                call_user_func_array([$stmt, 'bind_param'], $params);
                $stmt->execute();
                $result = $stmt->get_result();
            } else {
                $result = mysqli_query($this->db, $query);
            }
            $rows = array();

            switch ($returnType) {
                case 'array':
                    if (mysqli_num_rows($result) > 0) {
                        while ($row = mysqli_fetch_array($result)) {
                            array_push($rows, $row);
                        }
                        return $rows;
                    }
                    return array();
                    break;
                case 'assoc':
                    if (mysqli_num_rows($result) > 0) {
                        while ($row = mysqli_fetch_assoc($result)) {
                            array_push($rows, $row);
                        }
                        return $rows;
                    }
                    return array();
                    break;
                case 'insert':
                    return mysqli_insert_id($this->db);
                    break;
                case 'count':
                    return mysqli_num_rows($result);
                    break;
                case 'dml':
                    return (mysqli_errno($this->db) == 0) ? TRUE : FALSE;
                    break;
                default:
                    throw new Exception("Please specify query type", 1);
                    break;
            }
        } catch (Exception $e) {
//             print_r($query);
//             echo '---------------';
//             print_r($params);exit;
            $response = array();
            $response['Caught exception'] = $e->getMessage();
            $this->response($this->json($response), 200);
        }
    }

    //@ All the services which defines path @//
    function setStoreType(){
        $localSettingPath = $this->getBasePath() .'/localsettings.js';
        if(file_exists($this->getBasePath() .'/localsettings.js')){
            $settingfile = file_get_contents($localSettingPath);
            $tarray = array(" ", "\n", "\r");
            $contents = trim(str_replace($tarray, "", $settingfile));
            $contents = substr($contents, 0, -1);
            $contents = explode("localSettings=", $contents);
            $contents = json_decode($contents['1'], true);
            $this->lsStore_type = isset($contents['store_type'])?$contents['store_type']:false;
        }
    }
}
