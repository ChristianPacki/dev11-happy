<?php

/* Check Un-authorize Access */
if (!defined('accessUser')) {
    die("Error");
}

class ImageEdit extends UTIL {

    /**
     *
     * date created (dd-mm-yy)
     * date modified 15-4-2016 (dd-mm-yy)
     * Get  mask image
     *
     * @param (String)apikey
     * @param (Int)srtIndex
     * @param (Int)range
     * @return json data
     *
     */
    public function getMaskImageList() {
        $apiKey = (isset($this->_request['apikey']) && $this->_request['apikey']) ? $this->_request['apikey'] : '';
        if ($this->isValidCall($apiKey)) {
            try {
                $srtIndex = (isset($this->_request['srtIndex'])) ? $this->_request['srtIndex'] : 0;
                $range = (isset($this->_request['range'])) ? $this->_request['range'] : 30;
                $sql = "Select name,mask_id,thumb_image,svg_image from  " . TABLE_PREFIX . "mask_paths ORDER BY id DESC LIMIT ?,?";
                $params = array();
                $params[] = 'ii';
                $params[] = &$srtIndex;
                $params[] = &$range;
                $result = $this->executePrepareBindQuery($sql, $params);
                if (!empty($result)) {
                    $maskedImageData = array();
                    foreach ($result as $rows) {
                        $mask_id = $rows['mask_id'];
                        $name = $rows['name'];
                        $thumb_image = $rows['thumb_image'];
                        $svg_image = $rows['svg_image'];
                        $thumb_imageURL = $this->getMaskImageURL() . $thumb_image;
                        $svg_imageURL = $this->getMaskImageURL() . $svg_image;
                        $data = array("name" => $name, "mask_id" => $mask_id, "thumb_image" => $thumb_image, "thumb_imageURL" => $thumb_imageURL, "svg_image" => $svg_image, "svg_imageURL" => $svg_imageURL);
                        $maskedImageData[] = $data;
                    }
                    $this->closeConnection();
                    $this->response($this->json($maskedImageData, 1), 200);
                } else {
                    $msg['status'] = array('nodata');
                    $this->response($this->json($msg), 200);
                }
            } catch (Exception $e) {
                $result = array('Caught exception:' => $e->getMessage());
                $this->response($this->json($result), 200);
            }
        } else {
            $msg = array("status" => "invalid");
            $this->response($this->json($msg), 200);
        }
    }

    /**
     *
     * date created 9-9-2015 (dd-mm-yy)
     * date modified 15-4-2016 (dd-mm-yy)
     * get effect list
     *
     * @param (String)apikey
     * @return json data
     *
     */
    public function getEffectList() {
        header('HTTP/1.1 200 OK');
        try {
            $effectName = array();
            $sql = "SELECT file_name FROM " . TABLE_PREFIX . "effect_list";
            $result = $this->executePrepareBindQuery($sql);
            foreach ($result as $k => $row) {
                $effectName[$k] = $row['file_name'];
            }
            echo json_encode($effectName);
            exit;
        } catch (Exception $e) {
            echo 'Caught exception: ', $e->getMessage(), "\n";
        }
        $this->closeConnection();
    }

    /*
     *
     * date created 28-9-2016(dd-mm-yy)
     * date modified (dd-mm-yy)
     * getRangeFromAdmin
     * purpose:for fetching the value from database to be displayed in the drop-down
     *
     */

    public function getRangeFromAdmin() {
        try {
            $sql = "SELECT max_number_of_color FROM " . TABLE_PREFIX . "image_edit_select_color ORDER by id DESC LIMIT 1";
            $rows = $this->executePrepareBindQuery($sql);
            $range = array();
            for ($i = 0; $i < sizeof($rows); $i++) {
                $range[$i]['max_number_of_color'] = $rows[$i]['max_number_of_color'];
            }
            //echo $rows[$i]['max_number_of_color'];
            $this->response($this->json($range), 200);
            //break;
            $msg['status'] = ($rows) ? 'success' : 'failed';
            //$this->response($this->json($msg), 200);
        } catch (Exception $e) {
            $result = array('Caught exception:' => $e->getMessage());
            $this->response($this->json($result), 200);
        }
    }

    /*
      Fetch percentage of file uploaded.
      @return integer
     */

    public function imageUploadedPercentage() {
        try {
            session_start();
            $randomString = $this->_request['randomString'];

            $key = ini_get("session.upload_progress.prefix") . $randomString;

            if (!empty($_SESSION[$key])) {
                $current = $_SESSION[$key]["bytes_processed"];
                $total = $_SESSION[$key]["content_length"];
                $result = $current < $total ? ceil($current / $total * 100) : 100;
                $this->response($this->json(array($result)), 200);
            } elseif ($_SESSION[$randomString] == 'true') {
                unset($_SESSION[$randomString]);
                $this->response($this->json(array(100)), 200);
            } else {
                $this->response($this->json(''), 200);
            }
        } catch (Exception $e) {
            $result = array('Caught exception:' => $e->getMessage());
            $this->response($this->json($result), 200);
        }
    }

    /**
     *
     * date created (dd-mm-yy)
     * date modified 15-4-2016 (dd-mm-yy)
     * save  mask data
     *
     * @param (String)apikey
     * @param (String)name
     * @param (String)maskdata
     * @param (String)thumbdata
     * @return json data
     *
     */
    public function saveMaskPaths() {
        if ($this->get_request_method() != "POST") {
            $this->response('', 406);
        }
        $apiKey = $this->_request['apikey'];
        $name = addslashes($this->_request['name']);
        // $maskdata = $this->_request['maskdata'];
        // $thumbdata = $this->_request['thumbdata']; // design/pattern/textfx
        $id = $this->_request['id']; //file name


        if ($this->isValidCall($apiKey)) {
            try {
                // $maskBase64Data = base64_decode($maskdata);
                // $thumbBase64Data = base64_decode($thumbdata);
                $dir = $this->getMaskImagePath();

                if (!$dir) {
                    $this->response('', 204); //204 - immediately termiante this request
                }
                if (!file_exists($dir)) {
                    mkdir($dir, 0777, true);
                }
                $imageName = $id . '_0.png';
                $svgName = $id . '_0.svg';

                $thumbImageFilePath = $dir . $imageName;
                $svgImageFilePath = $dir . $svgName;

                $msg = '';
                // $status='';
                if (isset($_FILES['thumbdata'])) {
                    // $status = file_put_contents($thumbImageFilePath, $thumbBase64Data);
                    $status = move_uploaded_file($_FILES['thumbdata']['tmp_name'],$thumbImageFilePath);
                }
                $status = move_uploaded_file($_FILES['maskdata']['tmp_name'],$svgImageFilePath);
                // $status = file_put_contents($svgImageFilePath, $maskBase64Data);
                $sql = "INSERT INTO " . TABLE_PREFIX . "mask_paths (name,svg_image, thumb_image, mask_id, date_created) VALUES (?,?,?,?,now())";
                $params = array();
                $params[] = 'ssss';
                $params[] = &$name;
                $params[] = &$svgName;
                $params[] = &$imageName;
                $params[] = &$id;
                $status = $this->executePrepareBindQuery($sql, $params, 'dml');
                if ($status) {
                    $msg = array("status" => "success", "thumb_image" => $imageName, "svg_image" => $svgName);
                } else {
                    $msg = array("status" => "failed");
                }
                $this->closeConnection();
                $this->response($this->json($msg), 200);
            } catch (Exception $e) {
                $result = array('Caught exception:' => $e->getMessage());
                $this->response($this->json($result), 200);
            }
        }
    }

    /**
     *
     * date created (dd-mm-yy)
     * date modified 15-4-2016 (dd-mm-yy)
     * update mask path by mask id
     *
     * @param (String)apikey
     * @param (Int)maskId
     * @return json data
     *
     */
    public function updateMaskPaths() {
        if (!empty($this->_request) && isset($this->_request['apikey']) && $this->isValidCall($this->_request['apikey']) && isset($this->_request['maskId']) && $this->_request['maskId']) {
            extract($this->_request);
            try {
                $sql = "SELECT name,mask_id,thumb_image,svg_image FROM " . TABLE_PREFIX . "mask_paths WHERE mask_id=? LIMIT 1";
                $params = array();
                $params[] = 'i';
                $params[] = &$maskId;
                $rec = $this->executePrepareBindQuery($sql, $params, 'assoc');
                if (!empty($rec[0])) {
                    $sql = "UPDATE " . TABLE_PREFIX . "mask_paths SET";
                    $msg = array("status" => "success");
                    $maskName = addslashes($maskName);
                    $params = array();
                    $params[0] = '';
                    if ($rec[0]['maskName'] !== $maskName) {
                        $sql .= " name=?,";
                        $msg['name'] = $maskName;
                        $params[0] .= 's';
                        $params[] = &$maskName;
                    }
                    if ($rec[0]['mask_id'] == $maskId) {
                        $msg['mask_id'] = $maskId;
                        $dir = $this->getMaskImagePath();
                        if (!$dir) {
                            $this->response('', 204);
                        }
                        //204 - immediately termiante this request
                        if (!file_exists($dir)) {
                            mkdir($dir, 0777, true);
                        }

                        if (isset($_FILES['imgbase64'])) {
                            if (file_exists($dir . $rec[0]['thumb_image'])) {
                                unlink($dir . $rec[0]['thumb_image']);
                            }

                            move_uploaded_file($_FILES['imgbase64']['tmp_name'],$dir . $imgFileName);
                            // $thumbBase64Data = base64_decode($imgbase64); //$thumbImage = $mask_id.'.png';
                            // file_put_contents($dir . $imgFileName, $thumbBase64Data);
                            $sql .= " thumb_image=?,";
                            $msg['thumb_imageURL'] = $dir . $imgFileName;
                            $params[0] .= 's';
                            $params[] = &$imgFileName;
                        }
                        if (isset($_FILES['svgBase64'])) {
                            if (file_exists($dir . $rec[0]['svg_image'])) {
                                unlink($dir . $rec[0]['svg_image']);
                            }

                            move_uploaded_file($_FILES['svgBase64']['tmp_name'],$dir . $svgFileName);
                            // $maskBase64Data = base64_decode($svgBase64);
                            // file_put_contents($dir . $svgFileName, $maskBase64Data);

                            $sql .= " svg_image=?,";
                            $msg['svg_imageURL'] = $dir . $svgFileName;
                            $params[0] .= 's';
                            $params[] = &$svgFileName;
                        }
                    }
                    $sql .= " date_modified = NOW() WHERE mask_id=?";
                    $params[0] .= 'i';
                    $params[] = &$maskId;
                    $status .= $this->executePrepareBindQuery($sql, $params, 'dml');

                    if (!$status) {
                        $msg['status'] = "failed";
                    }

                    $this->closeConnection();
                    $this->response($this->json($msg, 1), 200);
                }
            } catch (Exception $e) {
                $result = array('Caught exception:' => $e->getMessage());
                $this->response($this->json($result), 200);
            }
        }
    }

    /**
     *
     * date created (dd-mm-yy)
     * date modified 15-4-2016 (dd-mm-yy)
     * Remove Design mask by design mask id
     *
     * @param (String)apikey
     * @param (Int)designmaskIds
     * @return json data
     *
     */
    public function removeDesignMasks() {
        $apiKey = $this->_request['apikey'];
        if ($this->isValidCall($apiKey)) {
            try {
                $designmaskIdsArray = $this->_request['designmaskIds'];
                $svgPathArray = array();
                $thumbPathArray = array();
                for ($j = 0; $j < sizeof($designmaskIdsArray); $j++) {
                    $sql = "Select thumb_image,svg_image from  " . TABLE_PREFIX . "mask_paths where mask_id=?";
                    $params = array();
                    $params[] = 'i';
                    $params[] = &$designmaskIdsArray[$j];
                    $result = $this->executePrepareBindQuery($sql, $params);
                    if (!empty($result)) {
                        foreach ($result as $rows) {
                            $svg_image = $rows['svg_image'];
                            $thumb_image = $rows['thumb_image'];
                            $svgPathArray[$j] = $this->getMaskImagePath() . $svg_image;
                            $thumbPathArray[$j] = $this->getMaskImagePath() . $thumb_image;
                        }
                    }
                }
                $status = 0;
                $id_str = implode(',', array_fill(0, count($designmaskIdsArray), '?'));
                $in_pattern = implode('', array_fill(0, count($designmaskIdsArray), 's'));
                $sql = "DELETE FROM " . TABLE_PREFIX . "mask_paths WHERE  mask_id in (" . $id_str . ")";
                $params = array();
                $params[] = $in_pattern;
                for ($i = 0; $i < count($designmaskIdsArray); $i++) {
                    $params[] = &$designmaskIdsArray[$i];
                }
                $this->log('removeDesignMasks:' . $sql);
                $status .= $this->executePrepareBindQuery($sql, $params, 'dml');
                if ($status) {
                    for ($i = 0; $i < sizeof($designmaskIdsArray); $i++) {
                        if (file_exists($svgPathArray[$i])) {
                            @chmod($svgPathArray[$i], 0777);
                            @unlink($svgPathArray[$i]);
                        }
                        if (file_exists($thumbPathArray[$i])) {
                            @chmod($thumbPathArray[$i], 0777);
                            @unlink($thumbPathArray[$i]);
                        }
                    }
                    $msg = array("status" => "success");
                    $this->closeConnection();
                    $this->response($this->json($msg), 200);
                } else {
                    $msg = array("status" => "failed", "sql" => $sql);
                    $this->closeConnection();
                    $this->response($this->json($msg), 200);
                }
            } catch (Exception $e) {
                $result = array('Caught exception:' => $e->getMessage());
                $this->response($this->json($result), 200);
            }
        }
    }

    /**
     *
     * date created 26-10-2017 (dd-mm-yy)
     * date modified (dd-mm-yy)
     * Get File Width,Height and dpi
     *
     * @param (String)apikey
     * @param (String)fileData
     * @return json data
     *
     */
    public function getFileValidate() {
        try {
            $postData = $this->formatJSONToArray($this->_request['data'], false);
            $resolution = 90;
            $baseImagePath = $this->getUserImagePath();
            $ext = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
            $tempPath = $baseImagePath . '/temp/';
            if (!file_exists($tempPath)) {
                mkdir($tempPath, 0777, true);
                chmod($tempPath, 0777);
            }
            $random = rand();
            $tempPath = $tempPath . $random . "1." . $ext;
            $status = move_uploaded_file($_FILES['file']['tmp_name'], $tempPath);

            if ($status) {
                if (extension_loaded('imagick')) {
                    $image = new Imagick($tempPath);
                    $resolutions = $image->getImageResolution();
                    if ($resolutions['x'] == 0) {
                        $image->setImageResolution($resolution, $resolution);
                        $resolutions = $image->getImageResolution();
                    }
                    $imageGeometry = $image->getImageGeometry();
                    unlink($tempPath);
                    if ($ext == 'bmp' || $ext == 'png') {
                        $xresolution = $resolutions['x'] * 2.54;
                        $yresolution = $resolutions['y'] * 2.54;
                    } else {
                        $xresolution = $resolutions['x'];
                        $yresolution = $resolutions['y'];
                    }
                    if ($xresolution && $yresolution) {
                        $msg = array("status" => "success", "imageHeight" => $imageGeometry['height'], "imageWidth" => $imageGeometry['width'], "HorizontalResolution" => $xresolution, "VerticalResolution" => $yresolution);
                    } else {
                        $msg = array("status" => "failed");
                    }
                } else {
                    $msg = array("status" => "success","message"=>"imagick not installed");
                }
            }
            $this->response($this->json($msg), 200);
        } catch (Exception $e) {
            $result = array('Caught exception:' => $e->getMessage());
            $this->response($this->json($result), 200);
        }
    }
     /**
     *
     * date created 26-10-2017 (dd-mm-yy)
     * date modified (dd-mm-yy)
     * Get File Width,Height and dpi
     *
     * @param (String)apikey
     * @param (String)fileData
     * @return json data
     * 
     */
    public function uploadUserImageToServer(){
        try{
            $postData = $this->formatJSONToArray($this->_request['data'], false);
            $resolution = 90;
            $additionalExtensions = false;
            $baseImagePath = $this->getUserImagePath();
            $ext = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
            $filePath = ''; 
            $customerId = (isset($this->_request['customerId']))?$this->_request['customerId']:0;
            // $uid = (isset($this->_request['uid'])) ? $this->_request['uid'] : $this->getDBUniqueId(TABLE_PREFIX . 'image_data', 'uid');
            $uid = (isset($postData->uid)? $postData->uid: 0);
            if (!$uid && $uid == 0) 
                $uid = $this->getDBUniqueId(TABLE_PREFIX . 'image_data', 'uid');
            $count = 0;
            $sql0 = "Select max(id) from  " . TABLE_PREFIX . "image_data where uid='" . $uid . "'";
            if ($customerId && $customerId > 0) {
                $sql0 = "Select max(id) from  " . TABLE_PREFIX . "image_data where customer_id=" . $customerId;
            }
            $rows0 = $this->executeGenericDQLQuery($sql0);
            if (!empty($rows0)) {
                $count = $rows0[0][0];
            }

            $count = $count + 1;
            $imageType = '';
            $extensions = array("pdf", "ai", "psd", "eps", "gif","cdr","dxf","tiff","bmp");
            if (in_array($ext, $extensions)){
                $imageType = $ext;
                $tempPath = $baseImagePath.'temp/';
                if(!file_exists($tempPath)){
                    mkdir($tempPath,0777,true);
                    chmod($tempPath, 0777);
                }
                $random = rand();
                $tempFilePath = $tempPath.$random."1.".$ext;
                $status = move_uploaded_file($_FILES['file']['tmp_name'], $tempFilePath);
                $ext = "png";
                $additionalExtensions = true;
            }

            $fileName = $count . '.' . $ext;
            $thumbFileName = 'thumb_' . $count . '.' . $ext;
            $directory = $uid;
            if ($customerId && $customerId > 0) {
                $directory = $customerId;
            }
            $savePath = $baseImagePath . $directory . '/';
            $baseImageURL = $this->getUserImageURL();
            $imageURL = $baseImageURL . $directory . '/';
            if (!file_exists($savePath)) {
                mkdir($savePath, 0777, true);
                chmod($savePath, 0777);
            }
            $filePath = $savePath . $fileName;
            $imageFullUrl = $imageURL . $fileName;
            $thumbFilePath = $savePath . $thumbFileName;
        
            if ($additionalExtensions){
               if($imageType == "psd") $tempFilePath = $tempFilePath.'[0]';
               else if($imageType == "gif") $tempFilePath = $tempFilePath.'[11]';
               $status = shell_exec("convert ".$tempFilePath." ".$filePath."");
               unlink($tempFilePath);   
            }else{
                $status = move_uploaded_file($_FILES['file']['tmp_name'], $filePath);
            }

            $resizeImage = $this->resize($filePath, $thumbFilePath, 70, 70);

            $sql = "INSERT INTO " . TABLE_PREFIX . "image_data (customer_id, image, thumbnail, type, uid, date_created) VALUES ($customerId, '$fileName', '$thumbFileName', '$ext', '$uid', now())";
            $id = $this->executeGenericInsertQuery($sql);
                if (true) {
                    $resolutions = shell_exec("convert ".$filePath." -ping -format '%w x %h x %x x%y' info:");
                    $resolutions = explode("x",$resolutions);
                    if($ext == 'bmp' || $ext == 'png'){
                        $xresolution = $resolutions[2]*2.54;
                        $yresolution = $resolutions[3]*2.54;
                    }else{
                        $xresolution = $resolutions[2];
                        $yresolution = $resolutions[3];
                    }
                    if($xresolution && $yresolution){
                        $msg = array("status" => "success","imageHeight" => $resolutions[1],"imageWidth"=> $resolutions[0],"HorizontalResolution" => $xresolution,"VerticalResolution" => $yresolution,"imageUrl" => $imageFullUrl);
                    }else{
                        $msg = array("status" => "failed");
                    }
                }else {
                    $msg = array("status" => "success","imageHeight" => 0,"imageWidth"=>0,"HorizontalResolution" => 0,"VerticalResolution" => 0,"imageUrl" =>$imageFullUrl);
                }
            $this->response($this->json($msg),200);
        } catch (Exception $e) {
            $result = array('Caught exception:' => $e->getMessage());
            $this->response($this->json($result), 200);
        }
    }

}
