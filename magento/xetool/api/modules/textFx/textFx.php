<?php

/* Check Un-authorize Access */
if (!defined('accessUser')) {
    die("Error");
}

class TextFx extends UTIL {

    /**
     *
     * date created (dd-mm-yy)
     * date modified 15-4-2016(dd-mm-yy)
     * Get textfx style
     *
     * @param (String)apikey
     * @param (int)charecter_nos
     * @param (int)style_nos
     * @return json data
     *
     */
    public function getTextfxStyle($style_nos = 0, $charecter_nos = 0) {
        try {
            $params = array();
            $params[0] = '';
            $sql = "SELECT s.pk_id as character_id,s.textfx_style_id,s.alphabate FROM " . TABLE_PREFIX . "textfx_charecters s INNER JOIN " . TABLE_PREFIX . "textfx_style t WHERE t.pk_id=s.textfx_style_id";
            if (isset($this->_request['style_nos']) && $this->_request['style_nos']) {
                $sql .= " AND (SELECT  COUNT(*) FROM " . TABLE_PREFIX . "textfx_style ts WHERE ts.pk_id >= t.pk_id) <= ?";
                $params[0] .= 'i';
                $params[] = &$this->_request['style_nos'];
            }
            if (isset($this->_request['charecter_nos']) && $this->_request['charecter_nos']) {
                $sql .= " AND (SELECT  COUNT(*) FROM " . TABLE_PREFIX . "textfx_charecters f WHERE f.textfx_style_id = s.textfx_style_id AND f.pk_id >= s.pk_id) <= ?";
                $params[0] .= 'i';
                $params[] = &$this->_request['charecter_nos'];
            }

            $rec = $this->executePrepareBindQuery($sql, $params, 'assoc');
            $sql1 = "SELECT * FROM " . TABLE_PREFIX . "textfx_style ORDER BY pk_id";
            $params = array();
            $rec1 = $this->executePrepareBindQuery($sql1, $params, 'assoc');
            $res = array();
            $alph = array();

            foreach ($rec1 as $k1 => $v1) {
                $i = 0;
                $res[$k1]['textfx_style_id'] = $v1['pk_id'];
                $res[$k1]['name'] = $v1['name'];
                $res[$k1]['price'] = $v1['price'];

                foreach ($rec as $k => $v) {
                    if ($v['textfx_style_id'] == $v1['pk_id']) {
                        $res[$k1]['alphabate'][$i]['charecter'] = $v['alphabate'];
                        $res[$k1]['alphabate'][$i]['id'] = $v['character_id'];
                        $i++;
                    }
                }
            }
            $this->response($this->json($res, 1), 200);
        } catch (Exception $e) {
            $result = array('Caught exception:' => $e->getMessage());
            $this->response($this->json($result), 200);
        }
    }

    /**
     *
     * date created (dd-mm-yy)
     * date modified 15-4-2016 (dd-mm-yy)
     * Update Textfx data
     *
     * @param (String)apikey
     * @param (String)name
     * @param (int)id
     * @param (Float)price
     * @return json data
     *
     */
    public function updateTextfxData() {
        $apiKey = $this->_request['apikey'];
        if ($this->isValidCall($apiKey)) {
            $id = $this->_request['id'];
            $name = $this->_request['name'];
            $price = $this->_request['price'];
            $status = 0;
            try {
                $sql = "UPDATE " . TABLE_PREFIX . "textfx SET name = ?, price = ? WHERE id = ?";
                $params = array();
                $params[] = 'sss';
                $params[] = &$name;
                $params[] = &$price;
                $params[] = &$id;
                $status = $this->executePrepareBindQuery($sql, $params, 'dml');
            } catch (Exception $e) {
                $msg = array('Caught exception:' => $e->getMessage());
            }
            $msg['status'] = ($status) ? 'success' : 'failed';
        } else {
            $msg = array("status" => "invalid");
        }
        $this->response($this->json($msg), 200);
    }

    /**
     *
     * date created (dd-mm-yy)
     * date modified 15-4-2016(dd-mm-yy)
     * Update TextFx
     *
     * @param (String)apikey
     * @param (int)textfx_style_id
     * @param (String)name
     * @return json data
     *
     */
    public function updateTextfx() {
        $status = 0;
        if (!empty($this->_request) && isset($this->_request['name']) && $this->_request['textfx_style_id']) {
            extract($this->_request);
            try {
                $name = addslashes($name);
                $sql = "UPDATE " . TABLE_PREFIX . "textfx_style SET name=?,price=? WHERE pk_id=?";
                $params = array();
                $params[] = 'sss';
                $params[] = &$name;
                $params[] = &$this->_request['price'];
                $params[] = &$textfx_style_id;
                $status = $this->executePrepareBindQuery($sql, $params, 'dml');
                if (!empty($alphabets)) {
                    $usql1 = '';
                    $usql2 = '';
                    $params = array();
                    $params1 = array();
                    $params2 = array();
                    $params_pattern = array();
                    $params_pattern[1] .= '';
                    $params_pattern[2] .= '';
                    foreach ($alphabets as $k => $v) {
                        $usql1 .= " WHEN ? THEN ?";
                        $usql2 .= ',?';
                        $params_pattern[1] .= 'ss';
                        $params_pattern[2] .= 's';
                        $alphabets[$k]['charecter'] = strtolower($alphabets[$k]['charecter']);
                        $params1[] = &$alphabets[$k]['id'];
                        $params1[] = &$alphabets[$k]['charecter'];
                        $params2[] = &$alphabets[$k]['id'];
                    }

                    $usql = 'UPDATE ' . TABLE_PREFIX . 'textfx_charecters SET alphabate = CASE pk_id ' . $usql1 . ' END WHERE pk_id IN(' . substr($usql2, 1) . ')';
                    $params[0] .= $params_pattern[1];
                    $params[0] .= $params_pattern[2];
                    $params = array_merge($params, $params1);
                    $params = array_merge($params, $params2);
                    $status = $this->executePrepareBindQuery($usql, $params, 'dml');
                }
            } catch (Exception $e) {
                $msg = array('Caught exception:' => $e->getMessage());
            }
        }
        $msg['status'] = ($status) ? 'Success' : 'Failed';
        $this->response($this->json($msg), 200);
    }

    /**
     *
     * date created (dd-mm-yy)
     * date modified 15-4-2016(dd-mm-yy)
     * delete TextFx Style
     *
     * @param (String)apikey
     * @param (int)textfx_style_id
     * @return json data
     *
     */
    public function deleteTextfxStyle() {
        try {
            $status = 0;
            if (isset($this->_request['textfx_style_id']) && $this->_request['textfx_style_id']) {
                $textfx_style_id = explode(",", $this->_request['textfx_style_id']);
                $id_str = implode(',', array_fill(0, count($textfx_style_id), '?'));
                $in_pattern = implode('', array_fill(0, count($textfx_style_id), 's'));
                $dir = $this->getTextfxSvgPath();
                if (!$dir) {
                    $this->response('', 204);
                }
                if (!file_exists($dir)) {
                    mkdir($dir, 0777, true);
                }
                $sql = "SELECT CONCAT(textfx_style_id,'_',alphabate,'.svg') AS file_name FROM " . TABLE_PREFIX . "textfx_charecters WHERE textfx_style_id IN(" . $id_str . ")";
                $params = array();
                $params[] = $in_pattern;
                for ($i = 0; $i < count($textfx_style_id); $i++) {
                    $params[] = &$textfx_style_id[$i];
                }
                $rec = $this->executePrepareBindQuery($sql, $params, 'assoc');
                foreach ($rec as $v) {
                    if (file_exists($dir . $v['file_name'])) {
                        unlink($dir . $v['file_name']);
                    }
                }
                $sql = "DELETE FROM " . TABLE_PREFIX . "textfx_charecters WHERE textfx_style_id IN(" . $id_str . ")";
                $status = $this->executePrepareBindQuery($sql, $params, 'dml');
                $sql = "DELETE FROM " . TABLE_PREFIX . "textfx_style WHERE pk_id IN(" . $id_str . ")";
                $this->executePrepareBindQuery($sql, $params, 'dml');
            }
        } catch (Exception $e) {
            $msg = array('Caught exception:' => $e->getMessage());
        }
        $msg['status'] = ($status) ? 'Success' : 'Failed';
        $this->response($this->json($msg), 200);
    }

    /**
     *
     * date created (dd-mm-yy)
     * date modified 15-4-2016(dd-mm-yy)
     * Add bulk TextFx
     *
     * @param (String)apikey
     * @param (Array)files
     * @param (String)name
     * @return json data
     *
     */
    public function addBulkTextfx() {
        $status = 0;
        if (!empty($this->_request) && isset($this->_request['name']) && $this->_request['name']) {
            if(strpos($this->lsStore_type, "woo") == 0){
                $this->_request['files'] = str_replace('\"','"',$_POST['files']);
            }
            $price = ($this->_request['price']) ? $this->_request['price'] : 0;
            $name = addslashes($this->_request['name']);
            $styl_sql = "INSERT INTO " . TABLE_PREFIX . "textfx_style (name,price) VALUES (?,?)";
            $params = array();
            $params[] = 'ss';
            $params[] = &$name;
            $params[] = &$price;
            $textfx_style_id = $this->executePrepareBindQuery($styl_sql, $params, 'insert');
            try {
                if (!empty($this->_request['files'])) {
                    $this->_request['files'] = json_decode($this->_request['files'],true);
                    $dir = $this->getTextfxSvgPath();
                    if (!$dir) {
                        $this->response('', 204);
                    }
                    if (!file_exists($dir)) {
                        mkdir($dir, 0777, true);
                    }
                    $data = '';
                    $params = array();
                    $params[0] = '';
                    foreach ($this->_request['files'] as $k => $v) {
                        $data .= ",(?,?)";
                        $params[0] .= 'ss';
                        $alphabetsTemp = strtolower($this->_request['files'][$k]['alphabate']);
                        $params[] = &$textfx_style_id;
                        $params[] = &$alphabetsTemp;
                        if ($v['base64Data']) {
                            $type = 'svg';
                            $fname[$k] = $textfx_style_id . '_' . strtolower($v['alphabate']) . '.' . $type;
                            // $thumbBase64Data[$k] = base64_decode($v['base64Data']);
                            // file_put_contents($dir . $fname[$k], $thumbBase64Data[$k]);
                            move_uploaded_file($_FILES[$v['base64Data']]['tmp_name'], $dir . $fname[$k]);
                            list($width[$k], $height[$k]) = getimagesize($dir . $fname[$k]);
                            $resizeImage = $this->resize($dir . $fname[$k], $dir . 'thumb_' . $fname[$k], 80, 80);
                            if ($resizeImage != true) {
                                $msg = array("status" => "ThumbnailForTextfx generation failed");
                                $this->response($this->json($msg), 200);
                            }
                        }
                    }
                    $sql = "INSERT INTO " . TABLE_PREFIX . "textfx_charecters (textfx_style_id, alphabate) VALUES " . substr($data, 1);
                    $status = $this->executePrepareBindQuery($sql, $params, 'dml');
                }
            } catch (Exception $e) {
                $msg = array('Caught exception:' => $e->getMessage());
            }
        }
        $msg['status'] = ($status) ? 'Success' : 'Failed';
        $this->response($this->json($msg), 200);
    }

    /**
     *
     * date created (dd-mm-yy)
     * date modified 15-4-2016 (dd-mm-yy)
     * Save Textfx data
     *
     * @param (String)apikey
     * @param (int)range
     * @param (int)srtIndex
     * @param (int)char_nos
     * @return json data
     *
     */
    public function getTextfxDetails() {
        try {
            $apiKey = $this->_request['apikey'];
            $range = $this->_request['range'];
            $startIndex = $this->_request['srtIndex'];
            $params = array();
            if ($this->isValidCall($apiKey)) {
                $sql = "SELECT s.pk_id as character_id,s.textfx_style_id,s.alphabate FROM " . TABLE_PREFIX . "textfx_charecters s INNER JOIN " . TABLE_PREFIX . "textfx_style t WHERE t.pk_id=s.textfx_style_id";
                if (isset($this->_request['char_nos']) && $this->_request['char_nos']) {
                    $sql .= ' AND (SELECT  COUNT(*) FROM ' . TABLE_PREFIX . 'textfx_charecters f WHERE f.textfx_style_id = s.textfx_style_id AND f.pk_id >= s.pk_id) <= ?';
                    $params[] = 's';
                    $params[] = &$this->_request['char_nos'];
                }
                $rec = $this->executePrepareBindQuery($sql, $params, 'assoc');
                $sql1 = "SELECT * FROM " . TABLE_PREFIX . "textfx_style ORDER BY pk_id limit ?,?";
                $params = array();
                $params[] = 'ss';
                $params[] = &$startIndex;
                $params[] = &$range;
                $rec1 = $this->executePrepareBindQuery($sql1, $params, 'assoc');
                $res = array();
                $alph = array();
                foreach ($rec1 as $k1 => $v1) {
                    $i = 0;
                    $res[$k1]['id'] = $v1['pk_id'];
                    $res[$k1]['name'] = $v1['name'];
                    $res[$k1]['price'] = $v1['price'];
                    foreach ($rec as $k => $v) {
                        if ($v['textfx_style_id'] == $v1['pk_id']) {
                            $res[$k1]['displaychar'][] = $v['alphabate'];
                            $i++;
                        }
                    }
                }
                $msg['fxfonts'] = $res;
            } else {
                $msg = array("status" => "invalid");
            }
        } catch (Exception $e) {
            $msg = array('Caught exception:' => $e->getMessage());
        }
        $this->response($this->json($msg), 200);
    }

    /**
     *
     * date created (dd-mm-yy)
     * date modified 15-4-2016 (dd-mm-yy)
     * Save Textfx data
     *
     * @param (String)apikey
     * @param (int)textFxId
     * @param (String)textFxName
     * @param (String)textFxDisplaychar
     * @param (Float)textFxPrice
     * @return json data
     *
     */
    public function saveTextFxDetails() {
        $apiKey = $this->_request['apikey'];
        if ($this->isValidCall($apiKey)) {
            try {
                $textFxId = $this->_request["textFxId"];
                $textFxName = $this->_request["textFxName"];
                $textFxDisplaychar = $this->_request["textFxDisplaychar"];
                $textFxPrice = floatval($this->_request["textFxPrice"]);
                $sql = "insert into " . TABLE_PREFIX . "textfx(id,name,displaychar,price) values(?,?,?,?)";
                $params = array();
                $params[] = 'ssss';
                $params[] = &$textFxId;
                $params[] = &$textFxName;
                $params[] = &$textFxDisplaychar;
                $params[] = &$textFxPrice;
                $status = $this->executePrepareBindQuery($sql, $params, 'dml');
                if ($status) {
                    $msg = array("status" => "success");
                } else {
                    $msg = array("status" => "failed", "sql" => $sql);
                }

                $this->closeConnection();
            } catch (Exception $e) {
                $msg = array('Caught exception:' => $e->getMessage());
            }
        } else {
            $msg = array("status" => "invalid");
        }
        $this->response($this->json($msg), 200);
    }

    /**
     *
     * date created (dd-mm-yy)
     * date modified 15-4-2016 (dd-mm-yy)
     * Update Textfx data
     *
     * @param (String)apikey
     * @param (int)textfxIds
     * @return json data
     *
     */
    public function removeTextfxFonts() {
        $apiKey = $this->_request['apikey'];
        $textfxIdsArray = $this->_request['textfxIds'];
        if ($this->isValidCall($apiKey)) {
//            $ids = implode(',', $textfxIdsArray);
            $id_str = implode(',', array_fill(0, count($textfxIdsArray), '?'));
            $in_pattern = implode('', array_fill(0, count($textfxIdsArray), 's'));
            try {
                $sql = "DELETE FROM " . TABLE_PREFIX . "textfx WHERE id in (" . $id_str . ")";
                $params = array();
                $params[] = &$shape_name;
                for ($i = 0; $i < count($textfxIdsArray); $i++) {
                    $params[] = &$textfxIdsArray[$i];
                }
                $status = $this->executePrepareBindQuery($sql, $params, 'dml');
                if ($status) {
                    $dir = '';
                    $dir = $this->getTextFXImagePath();

                    if (!$dir) {
                        $this->response('', 204);
                    }
                    //204 - immediately termiante this request

                    for ($j = 0; $j < sizeof($textfxIdsArray); $j++) {
                        $filePath = $dir . $textfxIdsArray[$j];
                        $msg = '';
                        if (file_exists($filePath)) {
                            if (is_file($filePath)) {
                                unlink($filePath);
                            }

                            $dirPath = $filePath . '/';
                            $files = glob($dirPath . '*', GLOB_MARK);
                            foreach ($files as $file) {
                                unlink($file);
                            }
                            rmdir($dirPath);
                        }
                    }
                    $msg = array("status" => "success");
                } else {
                    $msg = array("status" => "failed", "sql" => $sql);
                }

                $this->closeConnection();
            } catch (Exception $e) {
                $msg = array('Caught exception:' => $e->getMessage());
            }
        } else {
            $msg = array("status" => "invalid");
        }
        $this->response($this->json($msg), 200);
    }

}
