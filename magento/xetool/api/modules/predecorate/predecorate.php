<?php

/* Check Un-authorize Access */
if (!defined('accessUser')) {
    die("Error");
}

class Predecorate extends UTIL {

    /**
     *
     * date of created 16-6-2016(dd-mm-yy)
     * date of Modified 17-6-2016(dd-mm-yy)
     * save predecorated product images
     *
     * @param (String)apikey
     * @param (Array)designData
     * @param (String)name
     * @param (Int)ref_id
     * @param (Int)product_id
     * @param (Float)product_price
     * @param (Float)custom_price
     * @param (Int)print_method_id
     * @param (Array)template_image_json
     * @return JSON  data
     *
     */
    public function saveDecoratedImage() {
        if (isset($this->_request['cartEncData'])) {
            $cartData = $this->formatJSONToArray(stripslashes($this->rcEncDec5(self::POST_KEY, $this->_request['cartEncData'])));
            $msg = array('status' => 'checking', 'cartData' => $this->_request['cartEncData']);
            $this->response($this->json($msg), 200);
            $apikey = $cartData['apikey'];
            $designData = $cartData['designData'];
            $name = $this->_request['name'];
            $refid = $this->_request['ref_id'];
            $product_id = $this->_request['product_id'];
            $product_price = $this->_request['product_price'];
            $custom_price = $this->_request['custom_price'];
            $print_method_id = $this->_request['print_method_id'];
            $mini_qty = $this->_request['mini_qty'];
            $boundaryType = $this->_request['boundary_type'];
            $template_image_json = $this->_request['template_image_json'];
            $template_without_product = $this->_request['template_image_json_without_product'];
            $template_image_Sides = $this->formatJSONToArray($template_image_json);
            $imageNames = array();
            foreach ($template_image_Sides as $k => $v) {
                $dir = $this->getPreDecoProductImageUrl();
                if (strpos($v, $dir) !== false) {
                    $imageNames[$k] = basename($v);
                } else {
                    $imageNames[$k] = $v;
                }
            }
            $template_image_Sides = implode(',', $imageNames);
        } else {
            $apikey = $this->_request['apikey'];
            $designData = $this->_request['designData'];
            $name = $this->_request['name'];
            $refid = $this->_request['ref_id'];
            $product_id = $this->_request['product_id'];
            $product_price = $this->_request['product_price'];
            $custom_price = $this->_request['custom_price'];
            $print_method_id = $this->_request['print_method_id'];
            $mini_qty = $this->_request['mini_qty'];
            $boundaryType = $this->_request['boundary_type'];
            $template_image_json = $this->_request['template_image_json'];
            $template_without_product = $this->_request['template_image_json_without_product'];
            $template_image_Sides = $this->formatJSONToArray($template_image_json);
            $imageNames = array();
            foreach ($template_image_Sides as $k => $v) {
                $dir = $this->getPreDecoProductImageUrl();
                if (strpos($v, $dir) !== false) {
                    $imageNames[$k] = basename($v);
                } else {
                    $imageNames[$k] = $v;
                }
            }
            $template_image_Sides = implode(',', $imageNames);
        }
        $designData = urldecode($designData);
        if (STORE_TYPE == "woocommerce") {
            $designData = addslashes($designData);
        }

        $status = 0;
        $checkExistSql = "SELECT count(*) AS nos FROM " . TABLE_PREFIX . "decorated_product WHERE name = ?";
        $params = array();
        $params[] = 's';
        $params[] = &$name;
        $exist = $this->executePrepareBindQuery($checkExistSql, $params, 'assoc'); 
        if (!empty($exist) && $exist[0]['nos']) {
            $msg['status'] = 'Duplicate pre-decorated product name.';
        } else {
            if ($refid == 0) {
                $cartObj = Flight::carts();
                $refid = $cartObj->saveDesignStateCart($apikey, $refid, $designData); // private
                if ($refid > 0) {
                    $dbstat = $cartObj->saveProductPreviewSvgImagesOnAddToCart($apikey, $refid, $designData);
                }
            }

            $sql = "INSERT INTO " . TABLE_PREFIX . "decorated_product (name,refid,product_id,product_price,custom_price,print_method_id,mini_qty,boundary_type,template_image_json,without_prod_img,date_created,date_madified)
            values(?,?,?,?,?,?,?,?,?,?,NOW(),NOW())";
            $params = array();
            $params[] = 'siississss';
            $params[] = &$name;
            $params[] = &$refid;
            $params[] = &$product_id;
            $params[] = &$product_price;
            $params[] = &$custom_price;
            $params[] = &$print_method_id;
            $params[] = &$mini_qty;
            $params[] = &$boundaryType;
            $params[] = &$template_image_Sides;
            $params[] = &$template_without_product;
            $status = $this->executePrepareBindQuery($sql, $params, 'dml'); 
            $msg['status'] = ($status) ? 'success' : 'failed';
        }
        $this->response($this->json($msg), 200);
    }

    /**
     *
     * date of created 2-2-2016(dd-mm-yy)
     * date of Modified 13-4-2016(dd-mm-yy)
     * get predecorated product images data
     *
     * @param (String)apikey
     * @return JSON  data
     *
     */
    public function getDecoratedImage() {
        $apikey = $this->_request['apikey'];
        $start = $this->_request['srtIndex'];
        $range = $this->_request['range'];
        if (isset($apikey) && ($this->isValidCall($apikey)) && isset($start) && isset($range)) {
            $sql = "SELECT * FROM " . TABLE_PREFIX . "decorated_product order by pk_id DESC LIMIT ?,?";
            $params = array();
            $params[] = 'ii';
            $params[] = &$start;
            $params[] = &$range;
            $rows = $this->executePrepareBindQuery($sql, $params, 'assoc'); 
            $result = array();
            foreach ($rows as $k => $v) {
                $dir = $this->getPreDecoProductImageUrl();
                $template_image_Sides = explode(',', $v['template_image_json']);
                $template_image_Sides = str_replace(array(']', '[', '"'), array('', '', ''), $template_image_Sides);
                $imagePath = array();
                foreach ($template_image_Sides as $key => $val) {
                    if (preg_match('|^http(s)?://|i', $val)) {
                        $imagePath[$key] = $val;
                    } else {
                        $imagePath[$key] = $dir . $val;
                    }
                }
                $result[$k]['id'] = $v['pk_id'];
                $result[$k]['name'] = $v['name'];
                $result[$k]['ref_id'] = $v['refid'];
                $result[$k]['print_method_id'] = $v['print_method_id'];
                $result[$k]['mini_qty'] = $v['mini_qty'];
                $result[$k]['product_id'] = $v['product_id'];
                $result[$k]['boundary_type'] = $v['boundary_type'];
                $result[$k]['product_price'] = $v['product_price'];
                $result[$k]['custom_price'] = $v['custom_price'];
                $result[$k]['template_image_json'] = $imagePath;
                unset($imagePath);
                $result[$k]['template_without_product'] = $this->formatJSONToArray($v['without_prod_img']);
            }
            $resultArr = array();
            $resultArr['pre_decorated_product'] = $result;
        } else {
            $resultArr['status'] = 'Invalid key';
        }
        $this->response($this->json($resultArr, 1), 200);
    }

    /**
     *
     * date created 22-08-2016(dd-mm-yy)
     * date modified 16-09-2016(dd-mm-yy)
     * Remove Predecorated Template
     *
     * @param (Int)ids
     * @return json data
     *
     */
    public function removePreDecoTemplate() {
        $apiKey = $this->_request['apikey'];
        if ($this->isValidCall($apiKey)) {
            if (!empty($this->_request['ids'])) {
                try {
                    $templateIdsArray = $this->_request['ids'];
//                    $ids = implode(',', $templateIdsArray);
                    $id_str = implode(',', array_fill(0, count($templateIdsArray), '?'));
                    $in_pattern = implode('', array_fill(0, count($templateIdsArray), 's'));
                    $sql = "select refid from decorated_product where pk_id IN(" . $id_str . ")";
                    $params = array();
                    $params[] = $in_pattern;
                    for ($i = 0; $i < count($templateIdsArray); $i++) {
                        $params[] = &$templateIdsArray[$i];
                    }
                    $rows = $this->executePrepareBindQuery($sql, $params, 'assoc'); 
                    if (!empty($rows)) {
                        $refIdDecoArr = array();
                        foreach ($rows as $row) {
                            $refIdDecoArr[] = $row['refid'];
                        }

                        $path = $this->getPreviewImagePath();
                        if (is_dir($path) === true && !empty($refIdDecoArr)) {
                            $dirs = array_diff(scandir($path), array('.', '..'));
                            foreach ($dirs as $dir) {
                                if (in_array($dir, $refIdDecoArr)) {
                                    $this->rrmdir($path . $dir);
                                }
                            }
                            $sql = "DELETE FROM " . TABLE_PREFIX . "decorated_product WHERE pk_id IN(" . $id_str . ")";
                            $params = array();
                            $params[] = $in_pattern;
                            for ($i = 0; $i < count($templateIdsArray); $i++) {
                                $params[] = &$templateIdsArray[$i];
                            }
                            $status = $this->executePrepareBindQuery($sql, $params, 'dml'); 
                        }
                        $response['status'] = true;
                        $response['message'] = "Predecorated product template is deleted successfully !!";
                    }
                    $this->closeConnection();
                    $this->response($this->json($response), 200);
                } catch (Exception $e) {
                    $result = array('Caught exception:' => $e->getMessage());
                    $this->response($this->json($result), 200);
                }
            }
        } else {
            $msg = array("status" => "invalid");
            $this->response($this->json($msg), 200);
        }
    }

    /**
     *
     * date created 07-06-2016(dd-mm-yy)
     * date modified (dd-mm-yy)
     * Add template to cart
     *
     *
     */
    public function addTemplateToCartById() {
        $confProductId = $this->_request['pid'];
        $xe_size = $this->_request['xe_size'];
        $xe_color = $this->_request['xe_color'];
        $qty = $this->_request['orderQty'];
        try {
            $sql = "SELECT ref_id FROM " . TABLE_PREFIX . "template_state_rel WHERE temp_id=?";
            $params = array();
            $params[] = 'i';
            $params[] = &$confProductId;
            $rows = $this->executePrepareBindQuery($sql, $params, 'assoc'); 
        } catch (Exception $e) {
            $msg = array('Caught exception:' => $e->getMessage());
            return $msg;
        }
        $refId = $rows[0]['ref_id'];
        $cartStoreObj = Flight::carts();
        return $result = $cartStoreObj->addTemplateToCart($confProductId, $xe_size, $xe_color, $qty, $refId);
    }

    /**
     *
     * date created 21-06-2016(dd-mm-yy)
     * date modified (dd-mm-yy)
     * Get ref id by template id
     *
     *
     */
    public function getRefId() {
        $templateId = $this->_request['pid'];
        try {
            $sql = "SELECT ref_id FROM " . TABLE_PREFIX . "template_state_rel WHERE temp_id=?";
            $params = array();
            $params[] = 'i';
            $params[] = &$templateId;
            $rows = $this->executePrepareBindQuery($sql, $params, 'assoc'); 
            $result = $rows[0]['ref_id'];
        } catch (Exception $e) {
            $msg = array('Caught exception:' => $e->getMessage());
            $this->response($msg, 200);
        }
        if (empty($result)) {
            $result = 0;
        }
        $this->response($result, 200);
    }

    /**
     *
     * date created 03-04-2017(dd-mm-yy)
     * date modified (dd-mm-yy)
     * Save product id,category id,status
     *
     *
     */
    public function saveTemplateInfo($pid, $catId, $status) {
        try {
            if (!$catId) {
                $catId = 0;
            }
            if ($status == 'without_image') {
                $status = 1;
            } else {
                $status = 0;
            }
            $sql = "INSERT INTO " . TABLE_PREFIX . "predeco_cat_rel (product_id, cat_id, template_type) values(?,?,?)";
            $params = array();
            $params[] = 'iis';
            $params[] = &$pid;
            $params[] = &$catId;
            $params[] = &$status;
            $result = $this->executePrepareBindQuery($sql, $params, 'dml'); 
        } catch (Exception $e) {
            $msg = array('Caught exception:' => $e->getMessage());
            $this->response($msg, 200);
        }
        $msg['status'] = ($result) ? 'success' : 'failed';
    }

    /**
     *
     * date created 03-04-2017(dd-mm-yy)
     * date modified (dd-mm-yy)
     * Get category id,status
     *
     *
     */
    public function getTemplateInfo($confProductId) {
        try {
            $sql = "SELECT cat_id,template_type FROM " . TABLE_PREFIX . "predeco_cat_rel WHERE product_id=?";
            $params = array();
            $params[] = 'i';
            $params[] = &$confProductId;
            $rows = $this->executePrepareBindQuery($sql, $params, 'assoc'); 
            $result['cat_id'] = $rows[0]['cat_id'];
            if ($rows[0]['template_type'] == 1) {
                $status = 'without_image';
            } else {
                $status = 'with_image';
            }
            $result['status'] = $status;
        } catch (Exception $e) {
            $msg = array('Caught exception:' => $e->getMessage());
            $this->response($msg, 200);
        }
        return $result;
    }
     /**
     *
     *date created (dd-mm-yy)
     *date modified 15-01-2018 (dd-mm-yy)
     *Gets Design state data by refid
     *
     *@param (Int)refid
     *@return json data
     *
     */
    public function getDesignStateByRefid() {
        $ref_id = $this->_request['refid'];
        $refid = $this->_request['refid'];
        if (!empty($refid)) {
            try {
                $jsonData = '';
                $fileName = 'designState.json';
                $baseImagePath = $this->getPreviewImagePath();
                $savePath = $baseImagePath . $ref_id . '/';
                $stateDesignPath = $savePath . 'svg/';
                $stateDesignPath = $stateDesignPath . $fileName;
                $jsonData = $this->formatJSONToArray(file_get_contents($stateDesignPath));
                foreach ($jsonData['sides'] as $k => $v) {
                    $html2 = new simple_html_dom();
                    $html2->load($v['svg'], false);
                    $rectPath = $html2->find('rect#boundRectangle', 0);
                    if ($rectPath) {
                        $result[$k]['x']       = $rectPath->x;
                        $result[$k]['y']       = $rectPath->y;
                        $result[$k]['width']   = $rectPath->width;
                        $result[$k]['height']  = $rectPath->height;
                        $result[$k]['aHeight'] = $rectPath->aHeight;
                        $result[$k]['aWidth']  = $rectPath->aWidth;
                    }else{
                        $path = $html2->find('path#boundMask', 0); //find mask actual height and width
                        $x1   = $path->aX;
                        $y1   = $path->aY;
                        $x    = $path->x;
                        $y    = $path->y;
                        if (!$x1 || !isset($x1) || ($x1 == null)) {
                            $x1 = $x;
                            $y1 = $y;
                        }
                        $result[$k]['x']       = $x;
                        $result[$k]['y']       = $y;
                        $result[$k]['width']   = $path->width;
                        $result[$k]['height']  = $path->height;
                        $result[$k]['aHeight'] = $path->aHeight;
                        $result[$k]['aWidth']  = $path->aWidth;
                    }
                }
            } catch (Exception $e) {
                $result = array('Caught exception:' => $e->getMessage());
                $this->response($this->json($result), 200);
            }
        } else {
            $result = array("status" => "No data found!!");
        }
        $this->response($this->json($result, 1), 200);
    }
    /**
     *
     *date created (dd-mm-yy)
     *date modified 15-01-2018 (dd-mm-yy)
     *Gets predeco product images by refid
     *
     *@param (Int)refid
     *@return json data
     *
     */
    public function getPreDecoratedImageByRefid() {
        $refid = $this->_request['refid'];
        if (!empty($refid)) {
            try {
                $sql = "SELECT * FROM " . TABLE_PREFIX . "decorated_product WHERE refid=?";
                $params = array();
                $params[] = 'i';
                $params[] = &$refid;
                $rows = $this->executePrepareBindQuery($sql, $params, 'assoc'); 
                foreach ($rows as $k => $v) {
                    $result['product_boundary_capture'] = $this->formatJSONToArray($v['without_prod_img']);
                }
            } catch (Exception $e) {
                $result = array('Caught exception:' => $e->getMessage());
                $this->response($this->json($result), 200);
            }
        } else {
            $result = array("status" => "No data found!!");
        }
        $this->response($this->json($result, 1), 200);
    }

}

