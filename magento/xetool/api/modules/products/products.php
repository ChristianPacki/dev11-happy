<?php

/* Check Un-authorize Access */
if (!defined('accessUser')) {
    die("Error");
}

class Products extends ProductsStore {

    /**
     *
     *date created (dd-mm-yy)
     *date modified 19-07-2017 (dd-mm-yy)
     *Get Color Swatch
     *
     *@param (Int)colorId
     *@return json data
     *
     */
    public function getColorSwatch($colorId) {
        try {
            $sqlSwatch = "SELECT  hex_code,image_name,attribute_id FROM " . TABLE_PREFIX . "swatches WHERE attribute_id=?";
            $params = array();
            $params[] = 's';
            $params[] = &$colorId;
            $res = $this->executePrepareBindQuery($sqlSwatch, $params, 'assoc');
            if ($res) {
                if ($res[0]['hex_code']) {
                    $colorSwatch = $res[0]['hex_code'];
                } else {
                    $imageName = $res[0]['attribute_id'].".png";
                    $swatchWidth = '45';
                    $swatchDir = $this->getSwatchURL();
                    $colorSwatch = $swatchDir . $swatchWidth . 'x' . $swatchWidth . '/' . $imageName;
                }
            } else {
                $colorSwatch = '';
            }
            return $colorSwatch;
        } catch (Exception $e) {
            $result = array('Caught exception:' => $e->getMessage());
            $this->response($this->json($result), 200);
        }
    }

    /**
     *
     *date created (dd-mm-yy)
     *date modified 15-4-2016 (dd-mm-yy)
     *Get mask data
     *
     *@param (String)apikey
     *@param (Int)productid
     *@return json data
     *
     */
    public function getMaskDataClient($sides = 1, $maskfilter) {
        //Modified
        $apiKey = $this->_request['apikey'];
        if (isset($this->_request['isTemplate'])) {
            $isTemplate = $this->_request['isTemplate'];
        }
        if (isset($this->_request['productid'])) {
            $productid = $this->_request['productid'];
        } else {
            $this->response('invalid info', 204);
        }
        //204 - immediately termiante this request

        if ($this->isValidCall($apiKey)) {
            try {
                // compulsary for all type
                $required_data_to_phase = 'scale_ratio,side,scaleRatio_unit,cust_bound_price,custom_boundary_unit,is_cropMark,is_safeZone,cropValue,safeValue,isBorderEnable,isDimensionEnable';
                if ($maskfilter == "bounds") {
                    $required_data_to_phase .= ',bounds_json_data,isSidesAdded,sidesAllowed';
                    $dimensions_required = 'true';
                } else if ($maskfilter == "mask") {
                    $required_data_to_phase .= ',mask_json_data,mask_height,mask_width,mask_price,mask_name,mask_id';
                    $dimensions_required = 'true';
                } else if ($maskfilter == "custom_size") {
                    $required_data_to_phase .= ',custom_size_data,isSidesAdded,sidesAllowed,cust_min_height,cust_min_width,cust_max_height,cust_max_width';
                    $dimensions_required = 'false';
                } else if ($maskfilter == "customMask") {
                    $required_data_to_phase .= ',custom_mask,isSidesAdded,sidesAllowed,custom_mask_min_height,custom_mask_min_width,custom_mask_max_height,custom_mask_max_width';
                    $dimensions_required = 'false';
                }

                if (isset($required_data_to_phase)) {
                    $sql = "Select $required_data_to_phase from " . TABLE_PREFIX . "mask_data where productid=? order by LENGTH(side), side";
                } else {
                    $sql = "Select * from " . TABLE_PREFIX . "mask_data where productid=? order by LENGTH(side), side";
                }
                $params = array();
                $params[] = 's';
                $params[] = &$productid;
                $result = $this->executePrepareBindQuery($sql, $params, 'assoc');
                $data = array();
                if (!empty($result)) {
                    if ($dimensions_required == "true") {
                        $dimension_sql = "SELECT ps.height,ps.width FROM " . TABLE_PREFIX . "product_sides_sizes AS pss JOIN " . TABLE_PREFIX . "print_size AS ps ON pss.printsize = ps.name WHERE pss.productid=? ORDER BY pss.side";
                        $params = array();
                        $params[] = 's';
                        $params[] = &$productid;
                        $dimensions = $this->executePrepareBindQuery($dimension_sql, $params, 'assoc');
                    }
                    $resultLength = sizeof($result);
                    foreach ($result as $k => $v) {
                        if ($dimensions_required == "true") {
                            $data[$k]['dimensions']['boundheight'] = round($dimensions[$k]['height'], 2);
                            $data[$k]['dimensions']['boundwidth'] = round($dimensions[$k]['width'], 2);
                        }
                        if ($maskfilter == "mask") {
                            $mask_sql = "Select name,maskwidth,maskheight,price from " . TABLE_PREFIX . "custom_maskdata where id=?";
                            $params = array('s',&$v['mask_id']);
                            $custom_mask_result = $this->executePrepareBindQuery($mask_sql, $params, 'assoc');
                            $data[$k]['mask'] = str_replace('\"', '', $v['mask_json_data']);
                            $data[$k]['mask_height'] = round($custom_mask_result[0]['maskheight'], 2);
                            $data[$k]['mask_width'] = round($custom_mask_result[0]['maskwidth'], 2);
                            $data[$k]['mask_price'] = $custom_mask_result[0]['price'];
                            $data[$k]['mask_name'] = $custom_mask_result[0]['name'];
                            $data[$k]['mask_id'] = $v['mask_id'];
                        }else if($maskfilter == "bounds"){
                            $data[$k]['bounds'] = $v['bounds_json_data'];
                        }else if($maskfilter == "custom_size"){
                            $data[$k]['customsize'] = $v['custom_size_data'];
                            $data[$k]['custom_min_height'] = $v['cust_min_height'];
                            $data[$k]['custom_min_width'] = $v['cust_min_width'];
                            $data[$k]['custom_max_height'] = $v['cust_max_height'];
                            $data[$k]['custom_max_width'] = $v['cust_max_width'];
                        }else if($maskfilter == "customMask"){
                            $data[$k]['custom_mask'] = $v['custom_mask'];
                            $data[$k]['custom_min_height_mask'] = $v['custom_mask_min_height'];
                            $data[$k]['custom_min_width_mask'] = $v['custom_mask_min_width'];
                            $data[$k]['custom_max_height_mask'] = $v['custom_mask_max_height'];
                            $data[$k]['custom_max_width_mask'] = $v['custom_mask_max_width'];
                        }
                        $data[$k]['scale_ratio'] = $v['scale_ratio'];
                        $data[$k]['side'] = $v['side'];
                        $data[$k]['is_cropMark'] = (isset($v['is_cropMark']) && strlen($v['is_cropMark'])) ? $v['is_cropMark'] : 0;
                        $data[$k]['is_safeZone'] = (isset($v['is_safeZone']) && strlen($v['is_cropMark'])) ? $v['is_safeZone'] : 1;
                        $data[$k]['cropValue'] = $v['cropValue'];
                        $data[$k]['safeValue'] = $v['safeValue'];
                        $data[$k]['scaleRatio_unit'] = $v['scaleRatio_unit'];
                        $data[$k]['custom_boundary_price'] = $v['cust_bound_price'];
                        $data[$k]['custom_boundary_unit'] = $v['custom_boundary_unit'];
                        if($v['isBorderEnable'] != 0){
                            $data[$k]['isBorderEnable'] = $v['isBorderEnable'];
                        }
                        if($v['isDimensionEnable'] != 0){
                            $data[$k]['isDimensionEnable'] = $v['isDimensionEnable'];
                        }
                        if ($maskfilter != "mask") {
                            $data[$k]['isSidesAdded'] = $v['isSidesAdded'];
                            $data[$k]['sidesAllowed'] = $v['sidesAllowed'];
                        }
                    }
                    //$this->response($this->json($data), 200);exit;
                    //$this->log('maskDataRow :: mask : ' . json_encode($data));
                    if ($resultLength < $sides) {
                        for ($i = $resultLength; $i < $sides; $i++) {
                            $data[$i] = $data[0];
                        }
                    }
                } else {
                    $sql = "SELECT bounds FROM " . TABLE_PREFIX . "general_setting LIMIT 1";
                    $res = $this->executeFetchAssocQuery($sql);
                    $a = $this->formatJSONToArray($res[0]['bounds']);
                    $sql = "SELECT height AS boundheight,width AS boundwidth FROM " . TABLE_PREFIX . "print_size WHERE name='A3' LIMIT 1";
                    $params = array();
                    $dimensions = $this->executePrepareBindQuery($sql, $params, 'assoc');

                    $arr = array(
                        'dimensions' => $dimensions[0],
                        'scale_ratio' => $a['scale_ratio'],
                        'side' => $a['side'],
                        'is_cropMark' => 0,
                        'is_safeZone' => 0,
                        'cropValue' => 0.00,
                        'safeValue' => 0.00,
                        'scaleRatio_unit' => 1,
                        'isBorderEnable' => 0,
                        'isSidesAdded' => 0,
                        'sidesAllowed' => 0,
                        'isDimensionEnable' => 0
                    );
                    if ($maskfilter == 'bounds') {
                        $arr['bounds'] = $a['bounds'];
                    }
                    if ($maskfilter == 'mask') {
                        $arr['mask'] = $a['mask'];
                        $arr['customsize'] = $a['customsize'];
                        $arr['custom_mask'] = $a['custom_mask'];
                        $arr['mask_height'] = $a['mask_height'];
                        $arr['mask_width'] = $a['mask_width'];
                        $arr['mask_price'] = 0.00;
                        $arr['mask_name'] = '';
                        $arr['mask_id'] = '0';
                    }
                    for ($i = 0; $i < $sides; $i++) {
                        $data[$i] = $arr;
                    }
                }
                if (isset($this->_request['returns']) && $this->_request['returns'] == true) {
                    $this->log('if');

                    return $this->json($data);
                } else {
                    $this->log('if');
                    if ($isTemplate == 1) {
                        return $this->json($data);
                    } else {
                        $this->response($this->json($data), 200);
                    }
                }
            } catch (Exception $e) {
                $result = array('Caught exception:' => $e->getMessage());
                $this->response($this->json($result), 200);
            }
        } else {
            $msg = array("status" => "invalidkey");
            if (isset($this->_request['returns']) && $this->_request['returns'] == true) {
                return $this->json($msg);
            } else {
                $this->response($this->json($msg), 200);
            }
        }
    }

    /**
     *
     *date created (dd-mm-yy)
     *date modified 15-4-2016 (dd-mm-yy)
     *Get mask data
     *
     *@param (String)apikey
     *@param (Int)productid
     *@return json data
     *
     */
    public function getMaskData($sides = 1) {
        //Modified
        $apiKey = $this->_request['apikey'];
        if (isset($this->_request['isTemplate'])) {
            $isTemplate = $this->_request['isTemplate'];
        }
        if (isset($this->_request['productid'])) {
            $productid = $this->_request['productid'];
        } else {
            $this->response('invalid info', 204);
        }
        //204 - immediately termiante this request

        if ($this->isValidCall($apiKey)) {
            try {
                $sql = "Select * from " . TABLE_PREFIX . "mask_data where productid=? order by LENGTH(side), side";
                $params = array();
                $params[] = 's';
                $params[] = &$productid;
                $result = $this->executePrepareBindQuery($sql, $params, 'assoc');
                $data = array();
                if (!empty($result)) {
                    $dimension_sql = "SELECT ps.height,ps.width FROM " . TABLE_PREFIX . "product_sides_sizes AS pss JOIN " . TABLE_PREFIX . "print_size AS ps ON pss.printsize = ps.name WHERE pss.productid=? ORDER BY pss.side";
                    $params = array();
                    $params[] = 's';
                    $params[] = &$productid;
                    $dimensions = $this->executePrepareBindQuery($dimension_sql, $params);
                    $resultLength = sizeof($result);
                    foreach ($result as $k => $v) {
                        $data[$k]['dimensions']['boundheight'] = round($dimensions[$k]['height'], 2);
                        $data[$k]['dimensions']['boundwidth'] = round($dimensions[$k]['width'], 2);
                        $data[$k]['mask'] = str_replace('\"', '', $v['mask_json_data']);
                        $data[$k]['bounds'] = $v['bounds_json_data'];
                        $data[$k]['customsize'] = $v['custom_size_data'];
                        $data[$k]['custom_mask'] = $v['custom_mask'];
                        $data[$k]['mask_height'] = round($v['mask_height'], 2);
                        $data[$k]['mask_width'] = round($v['mask_width'], 2);
                        $data[$k]['mask_price'] = $v['mask_price'];
                        $data[$k]['scale_ratio'] = $v['scale_ratio'];
                        $data[$k]['side'] = $v['side'];
                        $data[$k]['is_cropMark'] = (isset($v['is_cropMark']) && strlen($v['is_cropMark'])) ? $v['is_cropMark'] : 0;
                        $data[$k]['is_safeZone'] = (isset($v['is_safeZone']) && strlen($v['is_cropMark'])) ? $v['is_safeZone'] : 1;
                        $data[$k]['cropValue'] = $v['cropValue'];
                        $data[$k]['safeValue'] = $v['safeValue'];
                        $data[$k]['scaleRatio_unit'] = $v['scaleRatio_unit'];
                        $data[$k]['custom_min_height'] = $v['cust_min_height'];
                        $data[$k]['custom_min_width'] = $v['cust_min_width'];
                        $data[$k]['custom_max_height'] = $v['cust_max_height'];
                        $data[$k]['custom_max_width'] = $v['cust_max_width'];
                        $data[$k]['custom_boundary_price'] = $v['cust_bound_price'];
                        $data[$k]['mask_name'] = $v['mask_name'];
                        $data[$k]['mask_id'] = $v['mask_id'];
                        $data[$k]['custom_boundary_unit'] = $v['custom_boundary_unit'];
                        $data[$k]['custom_min_height_mask'] = $v['custom_mask_min_height'];
                        $data[$k]['custom_min_width_mask'] = $v['custom_mask_min_width'];
                        $data[$k]['custom_max_height_mask'] = $v['custom_mask_max_height'];
                        $data[$k]['custom_max_width_mask'] = $v['custom_mask_max_width'];
                        $data[$k]['isBorderEnable'] = $v['isBorderEnable'];
                        $data[$k]['isSidesAdded'] = $v['isSidesAdded'];
                        $data[$k]['sidesAllowed'] = $v['sidesAllowed'];
                        $data[$k]['isDimensionEnable'] = $v['isDimensionEnable'];
                    }
                    //$this->log('maskDataRow :: mask : ' . json_encode($data));
                    if ($resultLength < $sides) {
                        for ($i = $resultLength; $i < $sides; $i++) {
                            $data[$i] = $data[0];
                        }
                    }
                } else {
                    $params = array();
                    $sql = "SELECT bounds FROM " . TABLE_PREFIX . "general_setting LIMIT 1";
                    $res = $this->executePrepareBindQuery($sql, $params, 'assoc');
                    $a = $this->formatJSONToArray($res[0]['bounds']);
                    $sql = "SELECT height AS boundheight,width AS boundwidth FROM " . TABLE_PREFIX . "print_size WHERE name='A3' LIMIT 1";
                    $params = array();
                    $dimensions = $this->executePrepareBindQuery($sql, $params, 'assoc');

                    $arr = array(
                        'dimensions' => $dimensions[0],
                        'mask' => $a['mask'],
                        'bounds' => $a['bounds'],
                        'customsize' => $a['customsize'],
                        'custom_mask' => $a['custom_mask'],
                        'mask' => $a['mask'],
                        'mask_height' => $a['mask_height'],
                        'mask_width' => $a['mask_width'],
                        'mask_price' => 0.00,
                        'scale_ratio' => $a['scale_ratio'],
                        'side' => $a['side'],
                        'is_cropMark' => 0, 'is_safeZone' => 0, 'cropValue' => 0.00, 'safeValue' => 0.00, 'scaleRatio_unit' => 1, 'mask_name' => '', 'mask_id' => '0', 'isBorderEnable' => 0,'isSidesAdded' => 0,'sidesAllowed' => 0,'isDimensionEnable' =>0
                    );
                    for ($i = 0; $i < $sides; $i++) {
                        $data[$i] = $arr;
                    }
                }
                if (isset($this->_request['returns']) && $this->_request['returns'] == true) {
                    $this->log('if');

                    return $this->json($data);
                } else {
                    $this->log('if');
                    if ($isTemplate == 1) {
                        return $this->json($data);
                    } else {
                        $this->response($this->json($data), 200);
                    }
                }
            } catch (Exception $e) {
                $result = array('Caught exception:' => $e->getMessage());
                $this->response($this->json($result), 200);
            }
        } else {
            $msg = array("status" => "invalidkey");
            if (isset($this->_request['returns']) && $this->_request['returns'] == true) {
                return $this->json($msg);
            } else {
                $this->response($this->json($msg), 200);
            }
        }
    }

    /**
     *
     *date created (dd-mm-yy)
     *date modified 15-4-2016 (dd-mm-yy)
     *Get Dtg print sizes of prodfuct side wise
     *
     *@param (String)apikey
     *@param (Int)productid
     *@return json data
     *
     */
    public function getDtgPrintSizesOfProductSides($productid) {
        if ($productid) {
            $printArray = array();
            try {
                $sql = "SELECT pss.printsize,pss.side,pss.is_transition, ps.pk_id FROM " . TABLE_PREFIX . "product_sides_sizes AS pss JOIN " . TABLE_PREFIX . "print_size AS ps ON pss.printsize = ps.name WHERE pss.productid=? ORDER BY pss.side";
                $params = array();
                $params[] = 's';
                $params[] = &$productid;
                $res = $this->executePrepareBindQuery($sql, $params, 'assoc');
                if (!empty($res)) {
                    foreach ($res as $k => $v) {
                        $printArray[$k]['size'] = $v['printsize'];
                        $printArray[$k]['side'] = $v['side'];
                        $printArray[$k]['id'] = $v['pk_id'];
                        $printArray[$k]['is_transition'] = $v['is_transition'];
                    }
                } else {
                    $printArray = array("status" => "nodata");
                }
                //$this->closeConnection();
                return $printArray;
            } catch (Exception $e) {
                $result = array('Caught exception:' => $e->getMessage());
                return $result;
            }
        } else {
            $msg = array("status" => "invalid");
            return $msg;
        }
    }

    /**
     *
     *date created (dd-mm-yy)
     *date modified (dd-mm-yy)
     *Get print area type
     *@param (int)productId
     *
     */
    public function getPrintareaTypeClient($productId) {
        $apiKey = $this->_request['apikey'];
        $dataArray = array();
        $sql = "SELECT * FROM " . TABLE_PREFIX . "product_printarea_type WHERE productid = ?";
        $params = array();
        $params[] = 'i';
        $params[] = &$productId;
        $dataFromValue = $this->executePrepareBindQuery($sql, $params);
        if (count($dataFromValue) > 0) {
            foreach ($dataFromValue as $row) {
                $dataArray['mask'] = $row['mask'];
                $dataArray['bounds'] = $row['bounds'];
                $dataArray['custom_size'] = $row['custom_size'];
                $dataArray['customMask'] = $row['custom_mask'];
                //$dataArray['multipleBoundary'] = "false";
                if ($dataArray['custom_size'] == "true" || $dataArray['customMask'] == "true") {
                    $dataArray['unit_id'] = intval($row['unit_id']);
                    $dataArray['pricePerUnit'] = floatval($row['price_per_unit']);
                    $dataArray['maxWidth'] = floatval($row['max_width']);
                    $dataArray['maxHeight'] = floatval($row['max_height']);
                    $sql = "SELECT name FROM " . TABLE_PREFIX . "units WHERE id = " . $dataArray['unit_id'];
                    $unitData = mysqli_fetch_assoc(mysqli_query($this->db, $sql));
                    $dataArray['unit_name'] = $unitData['name'];
                }
            }
        } else {
            $dataArray['mask'] = "false";
            $dataArray['bounds'] = "true";
            $dataArray['custom_size'] = "false";
            $dataArray['customMask'] = "false";
        }
        return $dataArray;
    }

    /**
     *
     *date created (dd-mm-yy)
     *date modified (dd-mm-yy)
     *Get print area type
     *@param (int)productId
     *
     */
    public function getPrintareaType($productId) {
        $apiKey = $this->_request['apikey'];
        $dataArray = array();
        $sql = "SELECT * FROM " . TABLE_PREFIX . "product_printarea_type WHERE productid = ?";
        $params = array();
        $params[] = 'i';
        $params[] = &$productId;
        $dataFromValue = $this->executePrepareBindQuery($sql, $params);
        if (count($dataFromValue) > 0) {
            foreach ($dataFromValue as $row) {
                $dataArray['mask'] = $row['mask'];
                $dataArray['bounds'] = $row['bounds'];
                $dataArray['custom_size'] = $row['custom_size'];
                $dataArray['customMask'] = $row['custom_mask'];
                $dataArray['multipleBoundary'] = "false";
                $dataArray['unit_id'] = intval($row['unit_id']);
                $dataArray['pricePerUnit'] = floatval($row['price_per_unit']);
                $dataArray['maxWidth'] = floatval($row['max_width']);
                $dataArray['maxHeight'] = floatval($row['max_height']);

                $sql = "SELECT name FROM " . TABLE_PREFIX . "units WHERE id = " . $dataArray['unit_id'];
                $unitData = mysqli_fetch_assoc(mysqli_query($this->db, $sql));
                $dataArray['unit_name'] = $unitData['name'];
            }
        } else {
            $dataArray['mask'] = "false";
            $dataArray['bounds'] = "true";
            $dataArray['custom_size'] = "false";
            $dataArray['customMask'] = "false";
        }
        return $dataArray;
    }

    ####################################################
    ############## syncOrdersZip ###################
    ####################################################

    public function fetchProductFeatures($productId, $productCategoryIdArray) {
        $apiKey = $this->_request['apikey'];
        if ($this->isValidCall($apiKey)) {
            $featureIdsArray = array();
            $featureNamesArray = array();
            $featureTypesArray = array();
            $result = array();
            $sql = "SELECT id,name,type FROM " . TABLE_PREFIX . "features WHERE mandatory_status=0 && status=1 && product_level_status=1";
            $result1 = $this->executePrepareBindQuery($sql);
            foreach ($result1 as $row) {
                array_push($featureIdsArray, $row['id']);
                array_push($featureNamesArray, $row['name']);
                array_push($featureTypesArray, $row['type']);
            }
            $productFeaturesStatusData = array();
            $sql = "SELECT feature_id FROM " . TABLE_PREFIX . "product_feature_rel WHERE product_id= ? && status = 1";
            $params = array();
            $params[] = 's';
            $params[] = &$productId;
            $productFeatures = $this->executePrepareBindQuery($sql, $params);
            if (count($productFeatures) > 0) {
                foreach ($productFeatures as $row) {
                    $productFeatureId = $row['feature_id'];
                    $key = array_search($productFeatureId, $featureIdsArray);
                    $productFeature = $featureTypesArray[$key];
                    array_push($result, $productFeature);
                }
            } else {
                $categoryFeaturesList = array();
                for ($j = 0; $j < sizeof($productCategoryIdArray); $j++) {
                    $sql = "SELECT feature_id FROM " . TABLE_PREFIX . "productcategory_feature_rel WHERE product_category_id= ? && status = 1";
                    $params = array();
                    $params[] = 's';
                    $params[] = &$productCategoryIdArray[$j];
                    $categoryFeatures = $this->executePrepareBindQuery($sql, $params);
                    if (count($categoryFeatures) > 0) {
                        foreach ($categoryFeatures as $row) {
                            $categoryFeatureId = $row['feature_id'];
                            $key = array_search($categoryFeatureId, $featureIdsArray);
                            $categoryFeature = $featureTypesArray[$key];
                            if (!in_array($categoryFeature, $categoryFeaturesList)) {
                                array_push($result, $productFeature);
                            }
                            if (sizeof($categoryFeaturesList) == sizeof($featureTypesArray)) {
                                break;
                            }
                        }
                    }
                }
                $result = $categoryFeaturesList;
            }
            return $result;
        } else {
            $msg = array("status" => "invalid");
            return $this->json($msg);
        }
    }

    /**
     *
     *date created (dd-mm-yy)
     *date modified 15-4-2016(dd-mm-yy)
     *fetch Size variant additional price in client
     *
     *@param (String)apikey
     *@param (Int)confProductId
     *@param (Int)print_method_id
     *@return json data
     *
     */
    public function getSizeVariantAdditionalPriceClient($confProductId, $print_method_id) {
        if (isset($confProductId) && $confProductId && isset($print_method_id) && $print_method_id) {
            try {
                $sql = "SELECT svap.xe_size_id,svap.percentage FROM " . TABLE_PREFIX . "size_variant_additional_price as svap WHERE svap.product_id=? AND svap.print_method_id=? ORDER BY svap.xe_size_id DESC";
                $params = array();
                $params[] = 'ii';
                $params[] = &$confProductId;
                $params[] = &$print_method_id;
                $result = $this->executePrepareBindQuery($sql, $params, 'assoc');
            } catch (Exception $e) {
                $result = array('Caught exception:' => $e->getMessage());
            }
            return $result;
        } else {
            $msg = array("invalid");
            return $msg;
        }
    }

    /**
     *
     *date created 3-6-2016(dd-mm-yy)
     *date modified (dd-mm-yy)
     *get template side by product id and product category id
     *
     *@param (String)apikey
     *@param (int)confProductId
     *@return json data
     *
     */
    public function getProductTemplateByProductId($confProductId){
        $imageurl = $this->getProductTemplatePath();
        $side_sql = "SELECT distinct pts.sort_order, pts.pk_id,pts.side_name,pts.image,ptr.temp_id
                    FROM " . TABLE_PREFIX . "product_template AS pt ," . TABLE_PREFIX . "product_temp_rel AS ptr ,
                    " . TABLE_PREFIX . "product_temp_side AS pts
                    WHERE ptr.product_id=? AND pts.product_temp_id =ptr.temp_id ORDER BY pts.sort_order";
        $params = array();
        $params[] = 's';
        $params[] = &$confProductId;
        $row = $this->executePrepareBindQuery($side_sql, $params, 'assoc');

        $side_arr = array();
        if (!empty($row)) {
            $side_arr['tepmlate_id'] = $row[0]['temp_id'];
            foreach ($row as $key => $v) {
                $side_arr['side_id'][] = $v['pk_id'];
                $side_arr['thumbsides'][] = $imageurl . $v['temp_id'] . '/' . $v['pk_id'] . '.' . $v['image'];
                $side_arr['sides'][] = $imageurl . $v['temp_id'] . '/' . $v['pk_id'] . '.' . $v['image'];
            }
        }
        return $side_arr;
    }

    /**
     *
     *date created (dd-mm-yy)
     *date modified 15-4-2016 (dd-mm-yy)
     *Get additional printing prices of each color variants
     *
     *@param (String)apikey
     *@param (Int)productid
     *@param (Int)variantid
     *@return json data
     *
     */
    public function getAdditionalPrintingPriceOfVariants($productid, $variantid) {
        if ($productid) {
            try {
                $sql = "SELECT pm.pk_id, pm.name, pap.price,pap.is_whitebase FROM " . TABLE_PREFIX . "print_method as pm JOIN " . TABLE_PREFIX . "product_additional_prices AS pap ON pm.pk_id=pap.print_method_id
                WHERE pap.product_id=? AND variant_id=?"; //." AND pm.pk_id=".$printingType;
                $params = array();
                $params[] = 'ii';
                $params[] = &$productid;
                $params[] = &$variantid;
                $res = $this->executePrepareBindQuery($sql, $params, 'assoc');
                $printArray = array();
                $printArray = array();
                if (empty($res)) {
                    $sql = "SELECT distinct pm.pk_id as printid,pm.name as printName
                            FROM " . TABLE_PREFIX . "print_method pm
                            JOIN " . TABLE_PREFIX . "print_setting  pst ON pm.pk_id=pst.pk_id
                            LEFT JOIN " . TABLE_PREFIX . "print_method_setting_rel pmsr ON pst.pk_id=pmsr.print_setting_id where pst.is_default=1";
                    $params = array();
                    $default_id = $this->executePrepareBindQuery($sql, $params, 'assoc');
                    $printArray[0]['prntMthdId'] = $default_id[0]["printid"];
                    $printArray[0]['prntMthdName'] = $default_id[0]["printName"];
                    $printArray[0]['prntMthdPrice'] = '0.00';
                    $printArray[0]['is_whitebase'] = '0';
                } else {
                    foreach ($res as $k => $v) {
                        $printArray[$k]['prntMthdId'] = $v['pk_id']; //$productid;
                        $printArray[$k]['prntMthdName'] = $v['name'];
                        $printArray[$k]['prntMthdPrice'] = $v['price'];
                        $printArray[$k]['is_whitebase'] = intval($v['is_whitebase']);
                    }
                }
                return $printArray;
            } catch (Exception $e) {
                $result = array('Caught exception:' => $e->getMessage());
                $this->response($this->json($result), 200);
            }
        } else {
            $msg = array("inputData" => "invalid");
            return $msg;
        }
    }

    /**
     *
     *date of created 8-2-2016(dd-mm-yy)
     *date of Modified 13-4-2016(dd-mm-yy)
     *get discount data map to product by product id
     *
     * @param (String)apikey
     * @param (int)pid
     * @return JSON  data
     *
     */
    public function getDiscountToProduct($pid) {
        try {
            $sql = "SELECT * FROM " . TABLE_PREFIX . "product_print_discount_rel WHERE product_id =?";
            $params = array();
            $params[] = 's';
            $params[] = &$pid;
            $rows = $this->executePrepareBindQuery($sql, $params);
            $countRows = sizeof($rows);
            $result = array();
            $resultData = array();
            $resultDiscount = array();
            for ($j = 0; $j < $countRows; $j++) {
                $resultData[$j]['print_id'] = $rows[$j]['print_id'];
                $resultData[$j]['discount_id'] = $rows[$j]['discount_id'];
                $sql_feth = "SELECT name FROM " . TABLE_PREFIX . "discount WHERE pk_id =?";
                $params = array();
                $params[] = 's';
                $params[] = &$rows[$j]['discount_id'];
                $row = $this->executePrepareBindQuery($sql_feth, $params);
                for ($i = 0; $i < sizeof($row); $i++) {
                    $result = $row[$i]['name'];
                }
                $resultData[$j]['discount_name'] = $result;
            }
        } catch (Exception $e) {
            $resultData = array('Caught exception:' => $e->getMessage());
        }
        return (array_values($resultData));
    }

    /**
     *
     *date created (dd-mm-yy)
     *date modified 15-4-2016 (dd-mm-yy)
     *Set size variant additionalprice
     *
     *@param (String)apikey
     *@param (Int)product_id
     *@param (Array)print_detail
     *@return json data
     *
     */
    public function setSizeVariantAdditionalPrice() {
        try {
            $status = 0;
            $insert_success = array('status' => 'success');
            if (!empty($this->_request) && isset($this->_request['product_id']) && !empty($this->_request['print_detail'])) {
                extract($this->_request);
                $str = '';
                foreach ($print_detail as $k => $v) {
                    foreach ($v['sizePrice'] as $v1) {
                        if ($v1['pk_id']) {
                            $usql = "UPDATE " . TABLE_PREFIX . "size_variant_additional_price SET percentage=? WHERE pk_id=?";
                            $params = array();
                            $params[] = 'ss';
                            $params[] = &$v1['percentage'];
                            $params[] = &$v1['pk_id'];
                            $status = $this->executePrepareBindQuery($usql, $params, 'dml');
                        } else {
                            $v1['print_method_id'] = (isset($v1['print_method_id']) && $v1['print_method_id']) ? $v1['print_method_id'] : 0;
                            $sql = "INSERT INTO " . TABLE_PREFIX . "size_variant_additional_price(product_id,print_method_id,xe_size_id,percentage) VALUES(?,?,?,?)";
                            $params = array();
                            $params[] = 'ssss';
                            $params[] = &$product_id;
                            $params[] = &$v1['print_method_id'];
                            $params[] = &$v['xe_size_id'];
                            $params[] = &$v1['percentage'];
                            $status = $this->executePrepareBindQuery($sql, $params, 'dml');
                        }
                    }
                }
                $insert_success = $this->getSizeVariantAdditionalPrice($product_id);
            }
            if ($status) {
                $msg = $insert_success;
            } else {
                $msg['status'] = 'failure';
            }
        } catch (Exception $e) {
            $msg = array('Caught exception:' => $e->getMessage());
        }
        $this->response($this->json($msg), 200);
    }

    /**
     *
     *date created 3-6-2016(dd-mm-yy)
     *date modified (dd-mm-yy)
     *get 3d view is enabled or no
     *
     *@param (int)productId
     *@return true | false
     *
     */
    public function is3dViewEnabled($product_id){
         $sql = "SELECT pt.is_3d_preview FROM " . TABLE_PREFIX . "product_template pt LEFT JOIN  " . TABLE_PREFIX . "product_temp_rel ptr ON pt.pk_id = ptr.temp_id WHERE ptr.product_id = ". $product_id;

        $row = $this->executeFetchAssocQuery($sql);
        $is3dEnable = $row[0]['is_3d_preview'];
        if($is3dEnable == 1){
            $is3dEnable = $is3dEnable;
        } else {
            $is3dEnable = 0;
        }
        return $is3dEnable;
    }
    /**
     *
     *date created (dd-mm-yy)
     *date modified 19-07-2017 (dd-mm-yy)
     *Get additional printing prices of each color variants for Client
     *
     *@param (String)apikey
     *@param (Int)productid
     *@param (Int)variantid
     *@return json data
     *
     */
    public function getAdditionalPrintingPriceOfVariantsClient($productid, $variantid) {
        if ($productid) {
            try {
                $sql = "SELECT distinct pk_id, print_method_id,price,is_whitebase
                            FROM   " . TABLE_PREFIX . "product_additional_prices
                            WHERE  product_id =? AND variant_id =? ORDER BY pk_id";
                $params = array();
                $params[] = 'ii';
                $params[] = &$productid;
                $params[] = &$variantid;
                $rows = $this->executePrepareBindQuery($sql, $params, 'assoc');
                        $priceDetails = array();
                        //$num = sizeof($rows);
                        if (!empty($rows)) {
                            foreach ($rows as $k => $v) {
                                $priceDetails[$k]['prntMthdId'] = $v['print_method_id'];
                                $priceDetails[$k]['prntMthdPrice'] = $v['price'];
                                $priceDetails[$k]['is_whitebase'] = intval($v['is_whitebase']);
                            }
                        }
                return $priceDetails;
            } catch (Exception $e) {
                $result = array('Caught exception:' => $e->getMessage());
                $this->response($this->json($result), 200);
            }
        } else {
            $msg = array("inputData" => "invalid");
            return $msg;
        }
    }

    /**
     *
     *date created (dd-mm-yy)
     *date modified 28-07-2017 (dd-mm-yy)
     *Get additional printing prices of each color variants for Client
     *
     *@param (String)apikey
     *@param (Int)productid
     *@param (Int)variantid
     *@return json data
     *
     */
    public function getTemplateData($product_id) {
        if ($productid) {
            try {
                $templates = array();
                if (isset($product_id) && $product_id) {
                    $sql = "SELECT template_id FROM " . TABLE_PREFIX . "template_product_rel WHERE product_id = ?";
                    $params = array();
                    $params[] = 'i';
                    $params[] = &$product_id;
                    $res = $this->executePrepareBindQuery($sql, $params, 'assoc');
                    foreach ($res as $k => $v) {
                        $templates[$k] = $v['template_id'];
                    }
                }
                return $templates;
            } catch (Exception $e) {
                $result = array('Caught exception:' => $e->getMessage());
                $this->response($this->json($result), 200);
            }
        } else {
            $msg = array("inputData" => "invalid");
            return $msg;
        }
    }

    //get printdetails
    public function getPrintDetails($result) {
        $sql = "SELECT distinct pm.pk_id as printid,pm.name as printName
        FROM " . TABLE_PREFIX . "print_method pm
        JOIN " . TABLE_PREFIX . "print_setting  pst ON pm.pk_id = pst.pk_id
        LEFT JOIN " . TABLE_PREFIX . "print_method_setting_rel pmsr ON pst.pk_id = pmsr.print_setting_id where pst.is_default = 1";
        $params = array();
        $default_id = $this->executePrepareBindQuery($sql, $params, 'assoc');
        $product1 = array();
        $i = 0;
        foreach ($result['product'] as $k => $product) {
            $productPrintTypeSql = "SELECT distinct pm.pk_id, pm.name FROM " . TABLE_PREFIX . "print_method pm
            INNER JOIN " . TABLE_PREFIX . "product_printmethod_rel ppr ON ppr.print_method_id = pm.pk_id
            WHERE ppr.product_id=?";
            $params = array();
            $params[] = 's';
            $params[] = &$product['id'];
            $productPrintType = $this->executePrepareBindQuery($productPrintTypeSql, $params);
            if (!empty($productPrintType)) {
                $this->log('productPrintTypeSql: ' . $productPrintTypeSql, true, 'Zsql.log');
                foreach ($productPrintType as $k2 => $v2) {
                    $product1[$i]['print_details'][$k2]['prntMthdId'] = (string) $v2['pk_id'];
                    $product1[$i]['print_details'][$k2]['prntMthdName'] = $v2['name'];
                }
            } else {
                $catIdArr = $product['category'];
                $catIds = !empty($catIdArr) ? implode(',', array_fill(0, count($catIdArr), '?')) : 0;
                $in_pattern = !empty($catIdArr) ? implode('', array_fill(0, count($catIdArr), 's')) : 0;
                $catSql = 'SELECT DISTINCT pm.pk_id, pm.name
                FROM ' . TABLE_PREFIX . 'product_category_printmethod_rel AS pcpml
                JOIN ' . TABLE_PREFIX . 'print_method AS pm ON pm.pk_id = pcpml.print_method_id WHERE pcpml.product_category_id IN(' . $catIds . ')';
                $params = array();
                $params[] = $in_pattern;
                for ($j = 0; $j < count($catIdArr); $j++) {
                    $params[] = &$catIdArr[$j];
                }
                $rows = $this->executePrepareBindQuery($catSql, $params, 'assoc');
                if (empty($rows)) {
                    $default_print_type = "SELECT pmsr.print_method_id,pm.name FROM " . TABLE_PREFIX . "print_method_setting_rel AS pmsr JOIN " . TABLE_PREFIX . "print_setting ps ON pmsr.print_setting_id = ps.pk_id JOIN " . TABLE_PREFIX . "print_method AS pm ON pmsr.print_method_id = pm.pk_id WHERE ps.is_default='1' AND pm.is_enable='1' LIMIT 1";
                    $res = $this->executeFetchAssocQuery($default_print_type);
                    $product1[$i]['print_details'][0]['prntMthdId'] = (string) $res[0]['print_method_id'];
                    $product1[$i]['print_details'][0]['prntMthdName'] = $res[0]['name'];
                } else {
                    foreach ($rows as $k1 => $v1) {
                        $product1[$i]['print_details'][$k1]['prntMthdId'] = (string) $v1['pk_id'];
                        $product1[$i]['print_details'][$k1]['prntMthdName'] = $v1['name'];
                    }
                }
            }
            $i++;
        }
        return $product1;
    }

    public function getColorUrl($variants, $confId) {
        $resultArr['variants'] = $variants;
        $colorarray = array();
        $i = 0;
        foreach ($resultArr['variants'] as $key => $value) {
            $surplusPrice = $resultArr['variants'][$key]['price'];
            $sql = "SELECT ref_id,parent_id FROM " . TABLE_PREFIX . "template_state_rel WHERE temp_id = ?";
            $params = array();
            $params[] = 'i';
            $params[] = &$confId;
            $parentId = $this->executePrepareBindQuery($sql, $params, 'assoc');
            if (!empty($parentId)) {
                $sql = "SELECT custom_price FROM " . TABLE_PREFIX . "decorated_product WHERE product_id =? and refid =?";
                $params = array();
                $params[] = 'ii';
                $params[] = &$parentId[0]['parent_id'];
                $params[] = &$parentId[0]['ref_id'];
                $res = $this->executePrepareBindQuery($sql, $params, 'assoc');
                $customPrice = $res[0]['custom_price'];
                $resultArr['variants'][$key]['price'] = $surplusPrice - $customPrice;
                $resultArr['variants'][$key]['finalPrice'] = $surplusPrice;
            }
            $colorId = $resultArr['variants'][$key]['xe_color_id'];
            $colorSwatch = $this->getColorSwatch($colorId);
            // $sqlSwatch = "SELECT  hex_code,image_name FROM " . TABLE_PREFIX . "swatches WHERE attribute_id=?";
            // $params = array();
            // $params[] = 's';
            // $params[] = &$colorId;
            // $res = $this->executePrepareBindQuery($sqlSwatch, $params, 'assoc');
            // if ($res) {
            //     if ($res[0]['hex_code']) {
            //         $colorSwatch = $res[0]['hex_code'];
            //     } else {
            //         $imageName = $res[0]['image_name'];
            //         $swatchWidth = '45';
            //         $swatchDir = $this->getSwatchURL();
            //         $colorSwatch = $swatchDir . $swatchWidth . 'x' . $swatchWidth . '/' . $imageName;
            //     }
            // } else {
            //     $colorSwatch = '';
            // }
            $colorarray[$i]['colorUrl'] = $colorSwatch;
            $i++;
        }
        return $colorarray;
    }

    public function getVariantPrice($variantsArr, $product_id) {
        $i = 0;
        $priceArray = array();
        foreach ($variantsArr as $key => $value) {
            $surplusPrice = $variantsArr[$key]['price'];
            $sql = "SELECT ref_id,parent_id FROM " . TABLE_PREFIX . "template_state_rel WHERE temp_id = ?";
            $params = array();
            $params[] = 's';
            $params[] = &$product_id;
            $parentId = $this->executePrepareBindQuery($sql, $params, 'assoc');
            if (!empty($parentId)) {
                $sql = "SELECT custom_price FROM " . TABLE_PREFIX . "decorated_product WHERE product_id = ? and refid = ?";
                $params = array();
                $params[] = 'ii';
                $params[] = &$parentId[0]['parent_id'];
                $params[] = &$parentId[0]['ref_id'];
                $res = $this->executePrepareBindQuery($sql, $params, 'assoc');
                $customPrice = $res[0]['custom_price'];
                $priceArray[$i]['price'] = $surplusPrice - $customPrice;
                $priceArray[$i]['finalPrice'] = $surplusPrice;
            }
            $i++;
        }
        return $priceArray;
    }

    public function getProductPrintMethodDetails($confProductId, $isAdmin, $catIds, $refid, $resValue) {
        $MultiBoundQry = "SELECT * FROM " . TABLE_PREFIX . "multi_bound_print_profile_rel WHERE product_id = ?";
        $params = array();
        $params[] = 's';
        $params[] = &$confProductId;
        $records = $this->executePrepareBindQuery($MultiBoundQry, $params, 'assoc');
        if (!empty($records) && !$isAdmin) {
            $result_arr[0]['print_method_id'] = 0;
            $result_arr[0]['name'] = "multiple";
            $result_arr[0]['fetched_from'] = 'DB';
            $result_arr[0]['refid'] = $refid;
        } else {
            $fieldSql = 'SELECT distinct pm.pk_id AS print_method_id, pm.name';
            if ($additional_price) {
                $fieldSql .= ', pst.additional_price';
            }
            // Check whether product has specific print method assigned //
            $productPrintTypeSql = $fieldSql . ' FROM ' . TABLE_PREFIX . "print_method pm
        INNER JOIN " . TABLE_PREFIX . "product_printmethod_rel ppr ON ppr.print_method_id = pm.pk_id
        JOIN " . TABLE_PREFIX . "print_setting AS pst ON pm.pk_id = pst.pk_id
        WHERE ppr.product_id=? order by pm.pk_id ASC";
            $params = array();
            $params[] = 's';
            $params[] = &$confProductId;
            $res = $this->executePrepareBindQuery($productPrintTypeSql, $params, 'assoc');
            $result_arr = array();
            if (empty($res)) {
                try {
                    if (!empty($catIds)) {
                        $id_str = implode(',', array_fill(0, count($catIds), '?'));
                        $in_pattern = implode('', array_fill(0, count($catIds), 's'));
                        $catSql = $fieldSql . ' FROM ' . TABLE_PREFIX . 'product_category_printmethod_rel AS pcpml
                    JOIN ' . TABLE_PREFIX . 'print_method AS pm ON pm.pk_id = pcpml.print_method_id
                    JOIN ' . TABLE_PREFIX . 'print_setting AS pst ON pm.pk_id = pst.pk_id
                    LEFT JOIN ' . TABLE_PREFIX . 'print_method_setting_rel pmsr ON pst.pk_id = pmsr.print_setting_id
                    WHERE pcpml.product_category_id IN(' . $id_str . ') order by pm.pk_id ASC';
                        $params = array();
                        $params[] = $in_pattern;
                        for ($i = 0; $i < count($catIds); $i++) {
                            $params[] = &$catIds[$i];
                        }
                        $res = $this->executePrepareBindQuery($catSql, $params, 'assoc');
                        foreach ($res as $k => $v) {
                            $result_arr[$k]['print_method_id'] = $v['print_method_id'];
                            $result_arr[$k]['name'] = $v['name'];
                            $result_arr[$k]['fetched_from'] = 'category';
                            if ($refid != '') {
                                $result_arr[$k]['refid'] = $refid;
                            }
                        }
                        if (empty($res)) {
                            $res = $resValue;
                            foreach ($res as $k => $v) {
                                $result_arr[$k]['print_method_id'] = $v['print_method_id'];
                                $result_arr[$k]['name'] = $v['name'];
                                $result_arr[$k]['fetched_from'] = 'default';
                                if ($refid != '') {
                                    $result_arr[$k]['refid'] = $refid;
                                }
                            }
                        }
                    } else {
                        $res = $resValue;
                        foreach ($res as $k => $v) {
                            $result_arr[$k]['print_method_id'] = $v['print_method_id'];
                            $result_arr[$k]['name'] = $v['name'];
                            $result_arr[$k]['fetched_from'] = 'default';
                            if ($refid != '') {
                                $result_arr[$k]['refid'] = $refid;
                            }
                        }
                    }
                } catch (Exception $e) {
                    $result_arr = json_encode(array('isFault' => 1, 'faultMessage' => $e->getMessage()));
                }
            } else {
                foreach ($res as $k => $v) {
                    $result_arr[$k]['print_method_id'] = $v['print_method_id'];
                    $result_arr[$k]['name'] = $v['name'];
                    $result_arr[$k]['fetched_from'] = 'product';
                    if ($refid != '') {
                        $result_arr[$k]['refid'] = $refid;
                    }
                }
            }
        }
        return $result_arr;
}

    public function getProductPrintMethodType($productId) {
        $productPrintTypeSql = "SELECT distinct pm.pk_id, pm.name FROM " . TABLE_PREFIX . "print_method pm
        INNER JOIN " . TABLE_PREFIX . "product_printmethod_rel ppr ON ppr.print_method_id = pm.pk_id
        JOIN " . TABLE_PREFIX . "print_setting AS pst ON pm.pk_id = pst.pk_id
        WHERE ppr.product_id=?";
        $params = array();
        $params[] = 's';
        $params[] = &$productId;
        $productPrintType = $this->executePrepareBindQuery($productPrintTypeSql, $params);
        return $productPrintType;
    }

    public function getPrintMethodDetailsByCategory($catIds) {
        $id_str = implode(',', array_fill(0, count($catIds), '?'));
        $in_pattern = implode('', array_fill(0, count($catIds), 's'));
        $catSql = 'SELECT DISTINCT pm.pk_id, pm.name
    FROM ' . TABLE_PREFIX . 'product_category_printmethod_rel AS pcpml
    JOIN ' . TABLE_PREFIX . 'print_method AS pm ON pm.pk_id = pcpml.print_method_id
    JOIN ' . TABLE_PREFIX . 'print_setting AS pst ON pm.pk_id = pst.pk_id
    LEFT JOIN ' . TABLE_PREFIX . 'print_method_setting_rel pmsr ON pst.pk_id = pmsr.print_setting_id
    WHERE pcpml.product_category_id IN(' . $id_str . ')';
        $params = array();
        $params[] = $in_pattern;
        for ($i = 0; $i < count($catIds); $i++) {
            $params[] = &$catIds[$i];
        }
        $rows = $this->executePrepareBindQuery($catSql, $params, 'assoc');
        $printDetails = array();
        if (empty($rows)) {
            $default_print_type = "SELECT pm.pk_id,pm.name
        from " . TABLE_PREFIX . "print_method AS pm
        JOIN " . TABLE_PREFIX . "print_setting ps ON pm.pk_id = ps.pk_id
        LEFT JOIN " . TABLE_PREFIX . "print_method_setting_rel pmsr ON ps.pk_id = pmsr.print_setting_id
        WHERE ps.is_default='1' AND pm.is_enable ='1' AND ps.is_default='1'";
            $params = array();
            $res = $this->executePrepareBindQuery($default_print_type, $params, 'assoc');
            $printDetails[0]['print_method_id'] = $res[0]['pk_id'];
            $printDetails[0]['name'] = $res[0]['name'];
        } else {
            foreach ($rows as $k1 => $v1) {
                $printDetails[$k1]['print_method_id'] = $v1['pk_id'];
                $printDetails[$k1]['name'] = $v1['name'];
            }
        }
        return $printDetails;
}

    public function saveProductTemplateStateRel($printMethodId, $refId, $oldId, $newId) {
        $sql = "DELETE from " . TABLE_PREFIX . "template_state_rel WHERE temp_id=?";
        $params = array();
        $params[] = 's';
        $params[] = &$newId;
        $result = $this->executePrepareBindQuery($sql, $params, 'dml');
        $sql = "DELETE from " . TABLE_PREFIX . "product_printmethod_rel WHERE product_id=?";
        $params = array();
        $params[] = 's';
        $params[] = &$newId;
        $result = $this->executePrepareBindQuery($sql, $params, 'dml');
        $values = '';
        $pValues = '';
        $status = 0;
        $values .= ",(?,?,?)";
        $pValues .= ",(?,?)";
        if (strlen($values)) {
            $sql = "INSERT INTO " . TABLE_PREFIX . "template_state_rel (ref_id,temp_id,parent_id) VALUES" . substr($values, 1);
            $params = array();
            $params[] = 'iii';
            $params[] = &$refId;
            $params[] = &$newId;
            $params[] = &$oldId;
            $status = $this->executePrepareBindQuery($sql, $params, 'dml');
        }
        if (strlen($pValues)) {
            $sql = "INSERT INTO " . TABLE_PREFIX . "product_printmethod_rel (product_id,print_method_id) VALUES" . substr($pValues, 1);
            $params = array();
            $params[] = 'ii';
            $params[] = &$newId;
            $params[] = &$printMethodId;
            $status = $this->executePrepareBindQuery($sql, $params, 'dml');
        }
        if ($status) {
            $msg = array("status" => "success");
        } else {
            $msg = array("status" => "failed");
        }
        return $msg;
    }

    //for getsimpleproduct
    public function getColorSwatchProduct($colorId) {
        $sqlSwatch = "SELECT  hex_code,image_name FROM " . TABLE_PREFIX . "swatches WHERE attribute_id =?";
        $params = array();
        $params[] = 's';
        $params[] = &$colorId;
        $res = $this->executePrepareBindQuery($sqlSwatch, $params, 'assoc');
        if ($res) {
            if ($res[0]['hex_code']) {
                $colorSwatch = $res[0]['hex_code'];
            } else {
                $imageName = $res[0]['image_name'];
                $swatchWidth = '45';
                $swatchDir = $this->getSwatchURL();
                $colorSwatch = $swatchDir . $swatchWidth . 'x' . $swatchWidth . '/' . $imageName;
            }
        } else {
            $colorSwatch = '';
        }
        return $colorSwatch;
    }

    public function getTemplateId($pro_id) {
        $sql = "SELECT template_id FROM " . TABLE_PREFIX . "template_product_rel WHERE product_id = ?";
        $params = array();
        $params[] = 's';
        $params[] = &$pro_id;
        $res = $this->executePrepareBindQuery($sql, $params, 'assoc');
        $templates = array();
        foreach ($res as $k => $v) {
            $templates[$k] = $v['template_id'];
        }
        return $templates;
    }

    public function updateOrderList($ref_id, $itemID) {
        $sql = "update order_list set item_printed = 1 where refid = ? and  itemid = ?";
        $params = array();
        $params[] = 'is';
        $params[] = &$ref_id;
        $params[] = &$itemID;
        $stmt = $this->db->prepare($sql);
        call_user_func_array([$stmt, 'bind_param'], $params);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result;
    }

    public function getPrintMethodName($catIdStr) {
        $id_str = implode(',', array_fill(0, count($catIdStr), '?'));
        $in_pattern = implode('', array_fill(0, count($catIdStr), 's'));
        $sql = 'SELECT DISTINCT pm.pk_id AS print_method_id,pm.name FROM ' . TABLE_PREFIX . 'print_method AS pm INNER JOIN ' . TABLE_PREFIX . 'product_category_printmethod_rel AS pcpmr ON pm.pk_id = pcpmr.print_method_id WHERE pcpmr.product_category_id IN(' . $id_str . ')';
        $params = array();
        $params[] = $in_pattern;
        for ($i = 0; $i < count($catIdStr); $i++) {
            $params[] = &$catIdStr[$i];
        }
        $res = $this->executePrepareBindQuery($sql, $params, 'assoc');
        return $res;
    }

    public function getCategoryName($print_id, $all_categories) {
        $category_result = array();
        if (isset($print_id) && $print_id != 0) {
            $sql = "SELECT product_category_id FROM " . TABLE_PREFIX . "product_category_printmethod_rel WHERE print_method_id=?";
            $params = array();
            $params[] = 's';
            $params[] = &$print_id;
            $category = array();
            $rows = $this->executePrepareBindQuery($sql, $params);
            $category = $rows;
            foreach ($all_categories as $categories) {
                $categories = (array) $categories;
                for ($j = 0; $j < sizeof($category); $j++) {
                    if ($categories['id'] == $category[$j]['product_category_id']) {
                        $category_result[$j]['id'] = $categories['id'];
                        $category_result[$j]['name'] = $categories['name'];
                    }
                }
            }
            $result_arr = array();
            $result_arr['categories'] = array_values($category_result);
            $responsedata = $result_arr;
        } else {
            foreach ($all_categories as $categories) {
                $categories = (array) $categories;
                $category_result[] = array('id' => "" . $categories['id'] . "", 'name' => $categories['name']);
            }
            $result_arr = array();
            $result_arr['categories'] = array_values($category_result);
            $responsedata = $result_arr;
        }
        return $responsedata;
    }

    public function getPredecoRefId($product_id) {
        $sql = "SELECT  ref_id FROM " . TABLE_PREFIX . "template_state_rel WHERE  temp_id =?";
        $params = array();
        $params[] = 's';
        $params[] = &$product_id;
        $rows = $this->executePrepareBindQuery($sql, $params, 'assoc');
        return $rows;
    }

    public function checkCustomProductTemplateType($pid) {
        $productQry = "SELECT template_type FROM " . TABLE_PREFIX . "predeco_cat_rel WHERE product_id = ?";
        $params = array();
        $params[] = 's';
        $params[] = &$pid;
        $productInfo = $this->executePrepareBindQuery($productQry, $params, 'assoc');
        $productState = ($productInfo[0]['template_type'] == 0 ? "with_image" : "without_image");
        $productInfo = array('product_type' => $productState);
        return $productInfo;
    }

    /**
     * Used to get all products which are eligible to customize
     *
     * @param (Int)categoryid,
     * @param (String)searchstring,
     * @param (Int)start,
     * @param (Int)limit,
     * @param (boolean)loadVariants (To filter the product list)
     * @return  list of products which are eligible to customize
     */
    public function getAllProducts() {
        $error = false;
        $result = $this->productList();
        if (!empty($result) && !isset($result['isFault'])) {
            try {
                $printMethod = $this->getPrintDetails($result);
                $productsArr = array_replace_recursive($result['product'],$printMethod);
                $result['product'] = $productsArr;
                $result['count'] = $result['count'];
            } catch (Exception $e) {
                $result = json_encode(array('isFault' => 1, 'faultMessage' => $e->getMessage()));
            }
            //$this->response($this->json($result, 1), 200);
            if (!$error) {
                $this->response(json_encode($result,JSON_UNESCAPED_SLASHES), 200);
            } else {
                $msg = array('status' => 'failed', 'error' => $this->formatJSONToArray($result));
                $this->response($this->json($msg), 200);
            }
        } else if (isset($result['isFault']) && $result['isFault'] == 1) {
            $msg = json_encode(array('status' => 'failed', 'error' => $result));
            $this->response($this->json($msg), 200);
        } else if (isset($result['isFault']) && $result['isFault'] == 2) {
            $result = $result['error'];
            $msg = array('status' => 'apiLoginFailed', 'error' => $result);
            $this->response($this->json($msg), 200);
        }
    }

    /**
     * get all categories from store
     *
     * @param  (Int)printId
     * @return  json array
     */
    public function getCategories() {
        $error = false;
        $printId = isset($this->_request['printId']) ? $this->_request['printId'] : 0;
        $result = $this->allCategories();
        if (!empty($result) && !isset($result['isFault'])) {
            try {
                $categories = $result;
                $result = $this->getCategoryName($printId,$categories);
                $this->response($this->json($result), 200);
            } catch (Exception $e) {
                $result = json_encode(array('isFault' => 1, 'faultMessage' => $e->getMessage()));
                $error = true;
            }
            if (!$error) {
                $this->response($result, 200);
            } else {
                $this->response($this->json($result), 200);
            }
        } else if (isset($result['isFault']) && $result['isFault'] == 1) {
            $msg = json_encode(array('status' => 'failed', 'error' => $result));
            $this->response($this->json($msg), 200);
        } else if (isset($result['isFault']) && $result['isFault'] == 2) {
            $result = $result['error'];
            $msg = array('status' => 'apiLoginFailed', 'error' => $result);
            $this->response($this->json($msg), 200);
        }else{
            $msg = array('status' => 'success', 'categories' => $result);
            $this->response($this->json($msg), 200);
        }
    }

    /**
     * get simple product by product id from store
     *
     * @param   (int)id
     * @param   (int)confId
     * @param   (int)size
     * @return  json array
     */
    public function getSimpleProduct() {
        $error = false;
        if (!isset($this->_request['id']) || trim($this->_request['id']) == '') {
            $msg = array('status' => 'invalid id', 'id' => $this->_request['id']);
            $this->response($this->json($msg), 204); //terminate
        } else {
            $productId = trim($this->_request['id']);
        }
        $result = $this->productDetails();
        $result = (is_array($result))?$result:(array) $result;

        if (!empty($result) && !isset($result['isFault'])) {
            if (!$error) {
                try {
                        $resultArr = (object) $result;
                        $confProductId = $resultArr->pid;
                        $simpleProductId = $resultArr->pvid;
                        $colorId = $resultArr->xe_color_id;
                    if (empty($resultArr->colorSwatch)) {
                            $colorSwatch = $this->getColorSwatchProduct($colorId);
                            $resultArr->colorSwatch = $colorSwatch;
                        }
                        $this->_request['productid'] = $confProductId; //Mask Info
                        $this->_request['returns'] = true; //Mask Info
                        $maskInfo = $this->getMaskData(sizeof($resultArr->sides));
                        $resultArr->maskInfo = json_decode($maskInfo);
                        $printsize = $this->getDtgPrintSizesOfProductSides($confProductId);
                        $resultArr->printsize = $printsize;
                        $printareatype = $this->getPrintareaType($confProductId);
                        $resultArr->printareatype = $printareatype;
                        // insert multiple boundary data; if available
                        $settingsObj = Flight::multipleBoundary();
                        $multiBoundData = $settingsObj->getMultiBoundMaskData($confProductId);
                        if (!empty($multiBoundData)) {
                            $resultArr->printareatype['multipleBoundary'] = "true";
                            $resultArr->multiple_boundary = $multiBoundData;
                        }
                        $additionalprices = $this->getAdditionalPrintingPriceOfVariants($confProductId, $simpleProductId);
                        $resultArr->additionalprices = $additionalprices;
                        $resultArr->sizeAdditionalprices = $this->getSizeVariantAdditionalPrice($confProductId);
                        $pCategories = $resultArr->category;
                        $features = $this->fetchProductFeatures($confProductId, $pCategories);
                        $resultArr->features = $features;
                        $templates = array();
                        $resultArr->is_product_template = false;
                        if (isset($productId) && $productId) {
                            $templates = $this->getTemplateId($productId);
                        }
                        $resultArr->templates = $templates;
                        $resultArr->discountData = $this->getDiscountToProduct($productId);
                        $templateArr = $this->getProductTemplateByProductId($confProductId);
                        if (!empty($templateArr) && $templateArr['tepmlate_id'] != '') {
                            $resultArr->is_product_template = true;
                            $resultArr->tepmlate_id = $templateArr['tepmlate_id'];
                            if (!empty($templateArr['thumbsides']) && !empty($templateArr['sides'])) {
                                $resultArr->thumbsides = $templateArr['thumbsides'];
                                $resultArr->sides = $templateArr['sides'];
                            } else {
                                $resultArr->thumbsides = [];
                                $resultArr->sides = [];
                            }
                        }
                        if (empty($resultArr->maskInfo)) {
                            $maskInfo = $this->getMaskData(sizeof($templateArr['side_id']));
                            $resultArr->maskInfo = json_decode($maskInfo);
                        }
                        $result = json_encode($resultArr);
                } catch (Exception $e) {
                    $result = json_encode(array('isFault' => 1, 'faultMessage' => $e->getMessage()));
                    $error = true;
                }
            }
            $this->closeConnection();
            $this->response($result, 200);
        } else if (isset($result['isFault']) && $result['isFault'] == 1) {
            $msg = json_encode(array('status' => 'failed', 'error' => $result));
            $this->response($this->json($msg), 200);
        } else if (isset($result['isFault']) && $result['isFault'] == 2) {
            $result = $result['error'];
            $msg = array('status' => 'apiLoginFailed', 'error' => $result);
            $this->response($this->json($msg), 200);
        }
    }

    /**
     * fetch product by print method id
     *
     * @param (Int)pid
     * @return json array
     */
    public function getPrintMethodByProduct($additional_price = false) {
        $error = false;
        $result = $this->printMethodParameters();
        if (!empty($result) && !isset($result['isFault'])) {
            $resultArr = array();
            $confProductId  = $result['confProductId'];
            $isAdmin        = $result['isAdmin'];
            $catIds         = $result['catIds'];
            $refid          = $result['refid'];
            $printPObj = Flight::printProfile();
            $res = $printPObj->getDefaultPrintMethodId();
            $resultArr = $this->getProductPrintMethodDetails($confProductId,$isAdmin,$catIds,$refid,$res);
            if (STORE_TYPE =='shopify'){
                echo json_encode($resultArr);
                exit();
            }else{
                $this->response($this->json($resultArr), 200);
            }
        } else if (isset($result['isFault']) && $result['isFault'] == 1) {
            $msg = json_encode(array('status' => 'failed', 'error' => $result));
            $this->response($this->json($msg), 200);
        } else if (isset($result['isFault']) && $result['isFault'] == 2) {
            $result = $result['error'];
            $msg = array('status' => 'apiLoginFailed', 'error' => $result);
            $this->response($this->json($msg), 200);
        }
    }

    /**
     * Get all size and quantity  by product id from store
     *
     * @param (Int)productId
     * @param (Int)simplePdctId
     * @return json array
     */
    public function getSizeAndQuantity() {
        if (!isset($this->_request['productId']) || trim($this->_request['productId']) == '') {
            //$product_id = '';
            $msg = array('status' => 'invalid productId', 'productId' => $this->_request['productId']);
            $this->response($this->json($msg), 204);
        } else {
            $product_id = trim($this->_request['productId']);
        }
        if (!isset($this->_request['simplePdctId']) || trim($this->_request['simplePdctId']) == '') {
            //$varient_id = '';
            $msg = array('status' => 'invalid simplePdctId', 'simplePdctId' => $this->_request['simplePdctId']);
            $this->response($this->json($msg), 204);
        } else {
            $varient_id = trim($this->_request['simplePdctId']);
        }
        $error = false;
        $result = $this->sizeAndQuantityDetails();
        if (!empty($result) && !isset($result['isFault'])) {
            if (!$error) {
                try {
                    $resultArr = $result;
                    $variantprice = $this->getVariantPrice($resultArr,$product_id);
                    $resultArr1['quantities'] = array_replace_recursive($resultArr,$variantprice);
                    $result = json_encode($resultArr1);
                } catch (Exception $e) {
                    $result = json_encode(array('isFault' => 1, 'faultMessage' => $e->getMessage()));
                    $error = true;
                }
            }
            $this->closeConnection();
            $this->response($result, 200);
        } else if (isset($result['isFault']) && $result['isFault'] == 1) {
            $msg = json_encode(array('status' => 'failed', 'error' => $result));
            $this->response($this->json($msg), 200);
        } else if (isset($result['isFault']) && $result['isFault'] == 2) {
            $result = $result['error'];
            $msg = array('status' => 'apiLoginFailed', 'error' => $result);
            $this->response($this->json($msg), 200);
        }
    }

    /**
     * getVariants - Get Product Variant List
     *
     * @param int start
     * @param int range
     * @param int offset
     * @param int store
     * @param int conf_pid
     * @return json of Product variants
     *
     */
    public function getVariants() {
        $error = false;
        $confId = $this->_request['conf_pid'];
        $result = $this->variantDetails();
        if (!empty($result) && !isset($result['isFault'])) {
            try {
                $variants = $result['variants'];
                $pCount = $result['count'];
                $productId = $confId;
                if (STORE_TYPE != 'prestashop' && STORE_TYPE != 'bigcommerce') {
                    $colorurl = $this->getColorUrl($variants, $productId);
                    $variantDtls = array_replace_recursive($variants, $colorurl);
                    $datalist['variants'] = $variantDtls;
                } else {
                    $datalist['variants'] = $variants;
                }
                $datalist['count'] = $pCount;
                $resultArr = json_encode($datalist);
            } catch (Exception $e) {
                $resultArr = json_encode(array('isFault' => 1, 'faultMessage' => $e->getMessage()));
                $error = true;
            }
            if (!$error) {
                $this->response($resultArr, 200);
            } else {
                $this->response($resultArr, 200);
            }
        } else if (isset($result['isFault']) && $result['isFault'] == 1) {
            $msg = json_encode(array('status' => 'failed', 'error' => $result));
            $this->response($this->json($msg), 200);
        } else if (isset($result['isFault']) && $result['isFault'] == 2) {
            $result = $result['error'];
            $msg = array('status' => 'apiLoginFailed', 'error' => $result);
            $this->response($this->json($msg), 200);
        }
    }

    /**
     * get simple product from store
     *
     * @param (int)id
     * @param (int)confId
     * @return  json array
     */
    public function getSimpleProductClient() {
        $error = false;
        $result = $this->productDetails(1);
        $result = (is_array($result))?$result:(array) $result;
        if (!empty($result) && !isset($result['isFault'])) {
            if (!$error) {
                try {
                        $resultArr = $result;
                        $confProductId = $resultArr['pid'];
                        $simpleProductId = $resultArr['pvid'];
                        $this->_request['productid'] = $confProductId;
                        $this->_request['returns'] = true;

                    if (STORE_TYPE != 'prestashop') {
                            $predecorateObj = Flight::predecorate();
                            $templatInfo = $predecorateObj->getTemplateInfo($confProductId);
                            $resultArr['defautlCategoryId'] = $templatInfo['cat_id'];
                            $resultArr['productStatus'] = $templatInfo['status'];
                        }

                        $printareatype = $this->getPrintareaTypeClient($confProductId);
                        $resultArr['printareatype'] = $printareatype;
                            if($resultArr['printareatype']['bounds'] == "true"){
                                $maskfilter = 'bounds';
                            }else if($resultArr['printareatype']['mask'] == "true"){
                                $maskfilter = 'mask';
                            }else if($resultArr['printareatype']['custom_size'] == "true"){
                                $maskfilter = 'custom_size';
                            }else if($resultArr['printareatype']['customMask'] == "true"){
                                $maskfilter = 'customMask';
                            }
                        // insert multiple boundary data; if available
                        $multiBoundObj = Flight::multipleBoundary();
                        $multiBoundData = $multiBoundObj->getMultiBoundMaskData($confProductId);
                        if (!empty($multiBoundData)) {
                            $resultArr['printareatype']['multipleBoundary'] = "true";
                            $resultArr['multiple_boundary'] = $multiBoundData;
                        }
                        $pCategories = $resultArr['category'];
                        //$features = $this->fetchProductFeatures($confProductId, $pCategories); //get features details
                        //$resultArr['features'] = $features;

                    if (STORE_TYPE == 'prestashop' || STORE_TYPE == 'woocommerce') {
                            $resultArr['sizeAdditionalprices'] = $this->getSizeVariantAdditionalPriceClient($confProductId, $this->_request['print_method_id']); // get variant additional price
                        }

                        $surplusPrice = $resultArr['price'];
                    $pid = $resultArr['pid'];
                        if (isset($pid) && $pid) {
                        $sql = "SELECT ref_id,parent_id FROM " . TABLE_PREFIX . "template_state_rel WHERE temp_id = ?";
                        $params = array();
                        $params[] = 'i';
                        $params[] = &$pid;
                        $parentId = $this->executePrepareBindQuery($sql, $params, 'assoc');
                            if (!empty($parentId)) {
                            $sql = "SELECT custom_price FROM " . TABLE_PREFIX . "decorated_product WHERE product_id = ? and refid = ?";
                            $params = array();
                            $params[] = 'ii';
                            $params[] = &$parentId[0]['parent_id'];
                            $params[] = &$parentId[0]['ref_id'];
                            $res = $this->executePrepareBindQuery($sql, $params, 'assoc');
                                $customPrice = $res[0]['custom_price'];
                                $resultArr['price'] = $surplusPrice - $customPrice;
                                $resultArr['finalPrice'] = $surplusPrice;
                            }
                        }
                        if ($simpleProductId) {
                            $priceDetails = $this->getAdditionalPrintingPriceOfVariantsClient($confProductId, $simpleProductId);
                            if (empty($priceDetails) && STORE_TYPE =='prestashop') {
                                $sql_fetch = "SELECT distinct product_id
                                FROM   product_additional_prices
                                WHERE  product_id =?";
                            $params = array();
                            $params[] = 'i';
                            $params[] = &$confProductId;
                            $rowsData = $this->executePrepareBindQuery($sql_fetch, $params, 'assoc');
                                if(!empty($rowsData[0]['product_id'])){
                                    $results = $this->getSimpleProductsVariants($confProductId, $simpleProductId, $color,$resultArr['xe_color_id']);
                                    foreach($results as $attrId){
                                        $sql = "SELECT distinct pk_id, print_method_id,price,is_whitebase
                                        FROM   product_additional_prices WHERE  product_id =? AND variant_id =? ORDER BY pk_id";
                                    $params = array();
                                    $params[] = 'ii';
                                    $params[] = &$confProductId;
                                    $params[] = &$attrId;
                                    $rows = $this->executePrepareBindQuery($sql, $params, 'assoc');
                                        if(!empty($rows)){
                                            foreach ($rows as $k => $v) {
                                                $priceDetails[$k]['prntMthdId'] = $v['print_method_id'];
                                                $priceDetails[$k]['prntMthdPrice'] = $v['price'];
                                                $priceDetails[$k]['is_whitebase'] = intval($v['is_whitebase']);
                                            }
                                        }
                                    }
                                }
                            }
                            $resultArr['additionalprices'] = $priceDetails;
                        } else {
                            $resultArr['additionalprices'] = [];
                        }
                        $dimentionDetails = $this->getDimensionAttributeDetails();
                        if(!empty($dimentionDetails)){
                            $resultArr['dimension'] = $dimentionDetails['attr_value'];
                            $resultArr['dimensionOrder'] = $dimentionDetails['attr_sort_order'];
                        }
                        $resultArr['is_product_template'] = false;
                        $resultArr['is_3d_preview'] = $this->is3dViewEnabled($confProductId);
                        $templateArr = $this->getProductTemplateByProductId($confProductId);
                        if (!empty($templateArr) && $templateArr['tepmlate_id'] != '') {
                            $resultArr['is_product_template'] = true;
                            $resultArr['tepmlate_id'] = $templateArr['tepmlate_id'];
                            if (!empty($templateArr['thumbsides']) && !empty($templateArr['sides'])) {
                                $resultArr['thumbsides'] = $templateArr['thumbsides'];
                                $resultArr['sides'] = $templateArr['sides'];
                            } else {
                                $resultArr['thumbsides'] = [];
                                $resultArr['sides'] = [];
                            }

                            $maskInfo = $this->getMaskDataClient(sizeof($templateArr['side_id']),$maskfilter);
                            $resultArr['maskInfo'] = json_decode($maskInfo);
                        } else {
                            $maskInfo = $this->getMaskDataClient(sizeof($resultArr['sides']),$maskfilter);
                            $resultArr['maskInfo'] = json_decode($maskInfo);
                        }
                        if($resultArr->is_product_template){
                            $resultArr['templates'] = $this->getTemplateData($pid);
                        }
                        if($maskfilter == 'bounds'){
                            $printsize = $this->getDtgPrintSizesOfProductSides($confProductId); //dtg print size info
                            $resultArr['printsize'] = $printsize;
                        }
                    if (STORE_TYPE == 'opencart') {
                            $resultArr['labels'] = array();
                        }

                    if (STORE_TYPE == 'magento' || STORE_TYPE == 'woocommerce') {
                            $colorId = $resultArr['xe_color_id'];
                            $resultArr['colorSwatch'] = $this->getColorSwatch($colorId);
                        }
                        //$resultArr['colorSwatch']
                        $result = json_encode($resultArr);
                } catch (Exception $e) {
                    $result = json_encode(array('isFault' => 1, 'faultMessage' => $e->getMessage()));
                    $error = true;
                }
            }
            $this->closeConnection();
            if (STORE_TYPE =='shopify'){
                print_r($result);
                exit;
            }else{
                $this->response($result, 200);
            }
        } else if (isset($result['isFault']) && $result['isFault'] == 1) {
            $msg = json_encode(array('status' => 'failed', 'error' => $result));
            $this->response($this->json($msg), 200);
        } else if (isset($result['isFault']) && $result['isFault'] == 2) {
            $result = $result['error'];
            $msg = array('status' => 'apiLoginFailed', 'error' => $result);
            $this->response($this->json($msg), 200);
        }
    }

}
