<?php

/* Check Un-authorize Access */
if (!defined('accessUser')) {
    die("Error");
}

class ManagePlugin extends UTIL {

    /**
     *
     * Install Plugin
     *
     * @param string $api_key API_KEY
     * @param string $master_key  MASTER KEY
     * @param string $zip  FILES- zip to extract and execute
     * @return json status message in json
     */
    public function installPlugin() {
        $msg = '';
        $installStatus = 0;
        // initialize all variables
        $pluginZipFileName = basename($_FILES['zip_file']['name']);
        $pluginZipFileNameTemp = $_FILES['zip_file']['tmp_name'];
        $storeType = trim($_REQUEST['store_type']);

        // Check extension of zip file //
        $extn = explode(".", $pluginZipFileName);
        $extn = $extn[count($extn) - 1];

        try {
            if ($extn != 'zip') {
                $msg = 'Please upload a zip file';
            } else {
                $tempUploadDir = $this->getBasePath() . "/tempPluginInstall";
                if (!file_exists($tempUploadDir)) {
                    mkdir($tempUploadDir, 0777, true);
                }

                $status = move_uploaded_file($pluginZipFileNameTemp, $tempUploadDir . '/' . $pluginZipFileName);
                $zipUploadStatus = ($status) ? 1 : 0;
                // check zip file uploaded or not //
                $uplaodError = $_FILES['zip_file']['error'];
                if ($uplaodError != 0) {
                    // Uplaod Error //
                    switch ($uplaodError) {
                        case 1:
                            $msg = "Error Code:" . $uplaodError . " The uploaded file exceeds the upload_max_filesize directive in php.ini";
                            break;
                        case 2:
                            $msg = "Error Code:" . $uplaodError . " The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form.";
                            break;
                        case 3:
                            $msg = "Error Code:" . $uplaodError . " The uploaded file was only partially uploaded.";
                            break;
                        case 4:
                            $msg = "Error Code:" . $uplaodError . " No file was uploaded.";
                            break;
                        case 6:
                            $msg = "Error Code:" . $uplaodError . " Missing a temporary folder.";
                            break;
                        case 7:
                            $msg = "Error Code:" . $uplaodError . " Failed to write file to disk.";
                            break;
                        case 8:
                            $msg = "Error Code:" . $uplaodError . " A PHP extension stopped the file upload.";
                            break;
                    }
                } else if ($uplaodError == 0) {
                    // Upload Success //
                    //create a ZipArchive instance
                    $zip = new \ZipArchive;
                    //open the archive
                    if ($zip->open($tempUploadDir . '/' . $pluginZipFileName) === true) {
                        $checkCounter = 0;
                        $sts = '';
                        //iterate the archive files array and display the filename or each one
                        $checkValidSqlFiles = $this->validatePluginZip($zip);
                        if ($checkValidSqlFiles == true) {

                            // check the meta_json plugin file details and extract all info //
                            $metaJsonPath = 'plugin_data/meta_plugin.json';
                            $metaJsonContents = $zip->getFromName($metaJsonPath);
                            if ($metaJsonContents != '') {
                                $result = json_decode($metaJsonContents);
                                if (json_last_error() === 0 && json_last_error() == JSON_ERROR_NONE) {
                                    // JSON is valid
                                    $pluginInfo = json_decode($metaJsonContents, true);
                                    $pluginActualName = addslashes($pluginInfo['name']);
                                    $pluginName = str_replace(" ", "_", $pluginInfo['name']);
                                    $pluginDesc = addslashes($pluginInfo['desc']);
                                    $pluginID = $pluginInfo['id'];
                                    $pluginKeywords = $pluginInfo['keywords'];
                                    $pluginHost = $pluginInfo['host'];
                                    $pluginDateCreated = $pluginInfo['date_created'];
                                    $pluginCompatible = $pluginInfo['compatible_with'];
                                    $pluginChanges = $pluginInfo['changes'];

                                    if ($pluginName == '' || $pluginDesc == '' || $pluginChanges == '' || $pluginID == '') {
                                        $msg = 'Few parameters missing either name, description, changes or id in meta json file.';
                                    } else {

                                        /* Check if thbe plugin name and id already installed */
                                        $checkSql = "SELECT count( * ) AS nos FROM " . TABLE_PREFIX . "plugins WHERE name = ? AND version = ?";
                                        $params = array();
                                        $params[] = 'ss';
                                        $params[] = &$pluginActualName;
                                        $params[] = &$pluginID;
                                        $res = $this->executePrepareBindQuery($checkSql, $params, 'assoc');
                                        if ($res[0]['nos'] == 0) {
                                            //check sql file available or not //
                                            $changes = $pluginFolderNames = $pluginFolderNamesCopy = explode(" | ", $pluginChanges);
                                            if (count($changes) > 0) {
                                                if (in_array('sql', $changes)) {
                                                    // check if install.sql and uninstall.sql files exists or not //
                                                    $sqlFileCheckStatus = $this->sqlFileCheck($zip);
                                                    if ($sqlFileCheckStatus != 1) {
                                                        $msg = $sqlFileCheckStatus;
                                                    }
                                                }

                                                // Create a plugin backup folder //
                                                $backupFolder = $this->createPluginBackUp($pluginID, $pluginName);

                                                // Compare files from plugin and Copy all files from xetool to plugin backup folder //

                                                /* Create folders inside plugin backup folder */
                                                $pluginBackUpPath = $this->getPluginBackUpPath($pluginID, $pluginName);
                                                foreach ($changes as $folderName) {
                                                    if ($folderName != 'sql') {
                                                        if (!file_exists($pluginBackUpPath . "/" . $folderName)) {
                                                            mkdir($pluginBackUpPath . "/" . $folderName, 0777, true);
                                                        }
                                                    }
                                                }

                                                // Extract folder to the temp folder //
                                                $tempPluginExtractDir = $this->getBasePath() . "/tempPluginInstall/" . $pluginName . $pluginID;
                                                $zip->extractTo($tempPluginExtractDir);
                                                $zip->close();
                                                $pluginTempDir = $tempPluginExtractDir;

                                                //copy files from xetool to plugin backup folder //
                                                foreach ($pluginFolderNames as $currentFolder) {

                                                    if ($currentFolder == 'api') {
                                                        $flagdir = $pluginTempDir . '/' . 'api';
                                                        $getFileDirList = $getFileDirList2 = $this->getDirContents($flagdir);
                                                        foreach ($getFileDirList as $apiFiles) {
                                                            $apiFilePath = str_replace($flagdir . '/', '', $apiFiles);
                                                            if (is_dir($this->getBasePath() . "/api/" . $apiFilePath)) {
                                                                mkdir($pluginBackUpPath . "/api/" . $apiFilePath, 0777, true);
                                                            }
                                                        }

                                                        foreach ($getFileDirList2 as $apiFiles) {
                                                            $apiFilePath = str_replace($flagdir . '/', '', $apiFiles);
                                                            if (is_file($this->getBasePath() . "/api/" . $apiFilePath)) {
                                                                copy($this->getBasePath() . "/api/" . $apiFilePath, $pluginBackUpPath . "/api/" . $apiFilePath);
                                                            }
                                                        }
                                                    }

                                                    if ($currentFolder == 'admin') {
                                                        $flagdir = $pluginTempDir . '/' . 'admin';
                                                        $getFileDirList = $getFileDirList2 = $this->getDirContents($flagdir);
                                                        foreach ($getFileDirList as $apiFiles) {
                                                            $apiFilePath = str_replace($flagdir . '/', '', $apiFiles);
                                                            if (is_dir($this->getBasePath() . "/admin/" . $apiFilePath)) {
                                                                mkdir($pluginBackUpPath . "/admin/" . $apiFilePath, 0777, true);
                                                            }
                                                        }

                                                        foreach ($getFileDirList2 as $apiFiles) {
                                                            $apiFilePath = str_replace($flagdir . '/', '', $apiFiles);
                                                            if (is_file($this->getBasePath() . "/admin/" . $apiFilePath)) {
                                                                copy($this->getBasePath() . "/admin/" . $apiFilePath, $pluginBackUpPath . "/admin/" . $apiFilePath);
                                                            }
                                                        }
                                                    }

                                                    if ($currentFolder == 'tool') {
                                                        $flagdir = $pluginTempDir . '/' . 'tool';
                                                        $getFileDirList = $getFileDirList2 = $this->getDirContents($flagdir);
                                                        foreach ($getFileDirList as $apiFiles) {
                                                            $apiFilePath = str_replace($flagdir . '/', '', $apiFiles);
                                                            if (is_dir($this->getBasePath() . "/" . $apiFilePath)) {
                                                                mkdir($pluginBackUpPath . "/tool/" . $apiFilePath, 0777, true);
                                                            }
                                                        }

                                                        foreach ($getFileDirList2 as $apiFiles) {
                                                            $apiFilePath = str_replace($flagdir . '/', '', $apiFiles);
                                                            if (is_file($this->getBasePath() . "/" . $apiFilePath)) {
                                                                copy($this->getBasePath() . "/" . $apiFilePath, $pluginBackUpPath . "/tool/" . $apiFilePath);
                                                            }
                                                        }
                                                    }

                                                    if ($currentFolder == 'store') {
                                                        $flagdir = $pluginTempDir . '/' . 'store';
                                                        $xetoolStorePath = substr($this->getBasePath(), 0, -7);
                                                        $getFileDirList = $getFileDirList2 = $this->getDirContents($flagdir);

                                                        if ($storeType == 'magento') {
                                                            $xetoolStorePath = substr($this->getBasePath(), 0, -7) . "/app";
                                                        } else if ($storeType == 'shopify') {
                                                            $xetoolStorePath = substr($this->getBasePath(), 0, -7) . "/shopify";
                                                        } else if ($storeType == 'opencart') {
                                                            $xetoolStorePath = substr($this->getBasePath(), 0, -7) . "/vqmod/xml";
                                                        }

                                                        foreach ($getFileDirList as $apiFiles) {
                                                            $apiFilePath = str_replace($flagdir . '/', '', $apiFiles);
                                                            if (is_dir($xetoolStorePath . "/" . $apiFilePath)) {
                                                                mkdir($pluginBackUpPath . "/store/" . $apiFilePath, 0777, true);
                                                            }
                                                        }

                                                        foreach ($getFileDirList2 as $apiFiles) {
                                                            $apiFilePath = str_replace($flagdir . '/', '', $apiFiles);
                                                            if (is_file($xetoolStorePath . "/" . $apiFilePath)) {
                                                                copy($xetoolStorePath . "/" . $apiFilePath, $pluginBackUpPath . "/store/" . $apiFilePath);
                                                            }
                                                        }
                                                    }
                                                }

                                                /* Copy SQL files from plugin to backup plugin folder */
                                                $flagdir = $pluginTempDir . '/' . 'sql';
                                                if (file_exists($flagdir)) {
                                                    if (!file_exists($pluginBackUpPath . "/sql")) {
                                                        mkdir($pluginBackUpPath . "/sql", 0777, true);
                                                    }
                                                    //copy install.sql //
                                                    if (file_exists($flagdir . '/install.sql')) {
                                                        copy($flagdir . '/install.sql', $pluginBackUpPath . "/sql/install.sql");
                                                    }
                                                    //copy uninstall.sql //
                                                    if (file_exists($flagdir . '/uninstall.sql')) {
                                                        copy($flagdir . '/uninstall.sql', $pluginBackUpPath . "/sql/uninstall.sql");
                                                    }
                                                }

                                                /* Copy files from plugin to xetool */
                                                foreach ($pluginFolderNamesCopy as $currentFolder) {
                                                    if ($currentFolder == 'api') {
                                                        $flagdir = $pluginTempDir . '/' . 'api';

                                                        $getFileDirList = $getFileDirList2 = $this->getDirContents($flagdir);
                                                        foreach ($getFileDirList as $apiFiles) {
                                                            $apiFilePath = str_replace($flagdir . '/', '', $apiFiles);
                                                            if (is_dir($apiFiles) && !file_exists($this->getBasePath() . "/api/" . $apiFilePath)) {
                                                                mkdir($this->getBasePath() . "/api/" . $apiFilePath, 0777, true);
                                                            }
                                                        }

                                                        foreach ($getFileDirList2 as $apiFiles) {
                                                            $apiFilePath = str_replace($flagdir . '/', '', $apiFiles);
                                                            if (is_file($apiFiles)) {
                                                                copy($pluginTempDir . "/api/" . $apiFilePath, $this->getBasePath() . "/api/" . $apiFilePath);
                                                            }
                                                        }
                                                    }
                                                    if ($currentFolder == 'admin') {
                                                        $flagdir = $pluginTempDir . '/' . 'admin';
                                                        $getFileDirList = $getFileDirList2 = $this->getDirContents($flagdir);
                                                        foreach ($getFileDirList as $apiFiles) {
                                                            $apiFilePath = str_replace($flagdir . '/', '', $apiFiles);
                                                            if (is_dir($apiFiles) && !file_exists($this->getBasePath() . "/admin/" . $apiFilePath)) {
                                                                mkdir($this->getBasePath() . "/admin/" . $apiFilePath, 0777, true);
                                                            }
                                                        }

                                                        foreach ($getFileDirList2 as $apiFiles) {
                                                            $apiFilePath = str_replace($flagdir . '/', '', $apiFiles);
                                                            if (is_file($apiFiles)) {
                                                                copy($pluginTempDir . "/admin/" . $apiFilePath, $this->getBasePath() . "/admin/" . $apiFilePath);
                                                            }
                                                        }
                                                    }
                                                    if ($currentFolder == 'tool') {
                                                        $flagdir = $pluginTempDir . '/' . 'tool';
                                                        $getFileDirList = $getFileDirList2 = $this->getDirContents($flagdir);

                                                        foreach ($getFileDirList as $apiFiles) {
                                                            $apiFilePath = str_replace($flagdir . '/', '', $apiFiles);
                                                            if (is_dir($apiFiles) && !file_exists($this->getBasePath() . "/" . $apiFilePath)) {
                                                                mkdir($this->getBasePath() . "/" . $apiFilePath, 0777, true);
                                                            }
                                                        }

                                                        foreach ($getFileDirList2 as $apiFiles) {
                                                            $apiFilePath = str_replace($flagdir . '/', '', $apiFiles);
                                                            if (is_file($apiFiles)) {
                                                                copy($pluginTempDir . "/tool/" . $apiFilePath, $this->getBasePath() . "/" . $apiFilePath);
                                                            }
                                                        }
                                                    }

                                                    if ($currentFolder == 'store') {

                                                        $flagdir = $pluginTempDir . '/' . 'store';
                                                        $xetoolStorePath = substr($this->getBasePath(), 0, -7);
                                                        $getFileDirList = $getFileDirList2 = $this->getDirContents($flagdir);

                                                        if ($storeType == 'magento') {
                                                            $xetoolStorePath = substr($this->getBasePath(), 0, -7) . "/app";
                                                        } else if ($storeType == 'shopify') {
                                                            $xetoolStorePath = substr($this->getBasePath(), 0, -7) . "/shopify";
                                                        } else if ($storeType == 'opencart') {
                                                            $xetoolStorePath = substr($this->getBasePath(), 0, -7) . "/vqmod/xml";
                                                        }

                                                        foreach ($getFileDirList as $apiFiles) {
                                                            $apiFilePath = str_replace($flagdir . '/', '', $apiFiles);
                                                            if (is_dir($apiFiles) && !file_exists($xetoolStorePath . "/" . $apiFilePath)) {
                                                                mkdir($xetoolStorePath . "/" . $apiFilePath, 0777, true);
                                                            }
                                                        }

                                                        foreach ($getFileDirList2 as $apiFiles) {
                                                            $apiFilePath = str_replace($flagdir . '/', '', $apiFiles);
                                                            if (is_file($apiFiles)) {
                                                                copy($pluginTempDir . "/store/" . $apiFilePath, $xetoolStorePath . "/" . $apiFilePath);
                                                            }
                                                        }
                                                    }
                                                }
                                                /* Execute install.sql file */
                                                $installSqlfilePath = $pluginTempDir . "/sql/install.sql";
                                                $sqlStatus = $this->run_sql_file($installSqlfilePath);
                                                if ($sqlStatus['0'] == 1) {
                                                    // Sql Success //
                                                    /* Insert to xetool DB */
                                                    $current_date = date('Y-m-d h:i:s');
                                                    $pluginSql = "INSERT INTO " . TABLE_PREFIX . "plugins SET name = ?, description = ?, version = ?, installed_on = ?, settings = ?, status = '1'";
                                                    $params = array();
                                                    $params[] = 'sssss';
                                                    $params[] = &$pluginActualName;
                                                    $params[] = &$pluginDesc;
                                                    $params[] = &$pluginID;
                                                    $params[] = &$current_date;
                                                    $params[] = &$metaJsonContents;
                                                    $insertStatus = $this->executePrepareBindQuery($pluginSql, $params, 'insert');
                                                    if ($insertStatus > 0) {
                                                        $msg = "Plugin installed successfully.";
                                                        $installStatus = 1;
                                                        /* Delete zip file and extracted folder */
                                                        unlink($tempUploadDir . '/' . $pluginZipFileName);
                                                        $this->deleteZipFileFolder($tempPluginExtractDir);
                                                    }
                                                } else {
                                                    // Sql Error //
                                                    $msg = $sqlStatus['1'];
                                                }
                                            } else {
                                                $msg = 'Invalid values for changes parameter';
                                            }
                                        } else {
                                            $msg = 'Plugin with name already installed, Try another';
                                        }
                                    }
                                } else {
                                    $msg = 'contents of meta plugin json file is not a valid json file, Please try again';
                                }
                            } else {
                                $msg = 'Invalid Plugin, meta_plugin json is blank';
                            }
                        } else {
                            $msg = 'Invalid Plugin, meta plugin json not found.';
                        }
                    } else {
                        $msg = 'Failed to open the archive!';
                    }
                }
            }
        } catch (Exception $e) {
            $msg = $e->getMessage();
        }
        $response['status'] = ($installStatus == 1) ? 'Success' : 'Failure';
        $response['response'] = $msg;
        $this->response($this->json($response), 200);
    }

    /**
     *
     * Get list of Plugin
     *
     * @return list of plugin in json
     */
    public function getPluginList() {
        try {
            $sql = "SELECT id,name,description,version, DATE(installed_on) AS installed_on,updated_on,status FROM " . TABLE_PREFIX . "plugins";
            $pluginList = $this->executePrepareBindQuery($sql);
            if (sizeof($pluginList) > 0) {
                $this->response($this->json($pluginList), 200);
            } else {
                $result = array('Error: ' => 'No plugin installed.');
                $this->response($this->json($result), 200);
            }
        } catch (Exception $e) {
            $result = array('Caught exception:' => $e->getMessage());
            $this->response($this->json($result), 200);
        }
    }

    /**
     *
     * Uninstall Plugin
     * @param int $plugin id
     * @return json status message in json
     */
    public function uninstallPlugin() {
        $uninstallStatus = 0;
        $pluginId = $this->_request['plugin_id'];
        $storeType = $this->_request['store_type'];
        $apiKey = $this->_request['api_key'];
        try {
            if (isset($this->_request['plugin_id']) && ($this->_request['plugin_id']) != '') {
                $pluginBackupDir = $this->getBasePath() . "/plugin_backup";
                if (file_exists($pluginBackupDir)) {
                    $sql = "SELECT id,name,version,settings FROM " . TABLE_PREFIX . "plugins WHERE id=?";
                    $params = array();
                    $params[] = 'i';
                    $params[] = &$pluginId;
                    $rows = $this->executePrepareBindQuery($sql, $params);
                    $name = $rows[0]['name'];
                    $name = str_replace(" ", "_", $name);
                    $version = $rows[0]['version'];
                    $settings = $rows[0]['settings'];
                    $pluginInfo = json_decode($settings, true);
                    $pluginChanges = $pluginInfo['changes'];
                    $pluginFolderNames = explode(" | ", $pluginChanges);
                    $pluginBackupPath = $pluginBackupDir . "/plugin_" . $name . "_" . $version;
                    if (count($pluginFolderNames) > 0) {
                        //copy files from plugin backup directory to xetool working directory //
                        foreach ($pluginFolderNames as $currentFolder) {
                            if ($currentFolder == 'api') {
                                $flagdir = $pluginBackupPath . '/' . 'api';
                                $getFileDirList = $this->getDirContents($flagdir);
                                foreach ($getFileDirList as $apiFiles) {
                                    $apiFilePath = str_replace($flagdir . '/', '', $apiFiles);
                                    if (is_file($this->getBasePath() . "/api/" . $apiFilePath)) {
                                        if (unlink($this->getBasePath() . "/api/" . $apiFilePath)) {
                                            if (copy($pluginBackUpPath . "/api/" . $apiFilePath, $this->getBasePath() . "/api/" . $apiFilePath)) {
                                                $uninstallStatus = 1;
                                                $msg = "Plugin removed successfully.";
                                            }
                                        }
                                    }
                                }
                            }

                            if ($currentFolder == 'admin') {
                                $flagdir = $pluginBackupPath . '/' . 'admin';
                                $getFileDirList = $this->getDirContents($flagdir);
                                foreach ($getFileDirList as $adminFiles) {
                                    $adminFilePath = str_replace($flagdir . '/', '', $adminFiles);
                                    if (is_file($this->getBasePath() . "/admin/" . $adminFilePath)) {
                                        if (unlink($this->getBasePath() . "/admin/" . $adminFilePath)) {
                                            if (copy($pluginBackUpPath . "/admin/" . $adminFilePath, $this->getBasePath() . "/admin/" . $adminFilePath)) {
                                                $uninstallStatus = 1;
                                                $msg = "Plugin removed successfully.";
                                            }
                                        }
                                    }
                                }
                            }

                            if ($currentFolder == 'tool') {
                                $flagdir = $pluginBackupPath . '/' . 'tool';
                                $getFileDirList = $this->getDirContents($flagdir);
                                foreach ($getFileDirList as $appFiles) {
                                    $appFilePath = str_replace($flagdir . '/', '', $appFiles);
                                    if (is_file($this->getBasePath() . "/" . $appFilePath)) {
                                        if (unlink($this->getBasePath() . "/api/" . $appFilePath)) {
                                            if (copy($pluginBackUpPath . "/tool/" . $appFilePath, $this->getBasePath() . "/" . $appFilePath)) {
                                                $uninstallStatus = 1;
                                                $msg = "Plugin removed successfully.";
                                            }
                                        }
                                    }
                                }
                            }

                            if ($currentFolder == 'store') {
                                $flagdir = $pluginBackupPath . '/' . 'store';
                                $xetoolStorePath = substr($this->getBasePath(), 0, -7);
                                $getFileDirList = $getFileDirList2 = $this->getDirContents($flagdir);

                                if ($storeType == 'magento') {
                                    $xetoolStorePath = substr($this->getBasePath(), 0, -7) . "/app";
                                } else if ($storeType == 'shopify') {
                                    $xetoolStorePath = substr($this->getBasePath(), 0, -7) . "/shopify";
                                } else if ($storeType == 'opencart') {
                                    $xetoolStorePath = substr($this->getBasePath(), 0, -7) . "/vqmod/xml";
                                }

                                foreach ($getFileDirList as $storeFiles) {
                                    $storeFilePath = str_replace($flagdir . '/', '', $storeFiles);
                                    if (is_file($xetoolStorePath . "/" . $storeFilePath)) {
                                        if (unlink($xetoolStorePath . "/" . $storeFilePath)) {
                                            if (copy($pluginBackUpPath . "/store/" . $storeFilePath, $xetoolStorePath . "/" . $storeFilePath)) {
                                                $uninstallStatus = 1;
                                                $msg = "Plugin removed successfully.";
                                            }
                                        }
                                    }
                                }
                            }
                            if ($currentFolder == 'sql') {
                                $uninstallSqlfilePath = $pluginBackupPath . "/sql/uninstall.sql";
                                $sqlStatus = $this->run_sql_file($installSqlfilePath);
                                if ($sqlStatus['0'] != 1) {
                                    // Sql Error //
                                    $msg = $sqlStatus['1'];
                                }
                            }

                            // Sql Success //
                            $sql = "DELETE FROM " . TABLE_PREFIX . "plugins WHERE id=?";
                            $params = array();
                            $params[] = 'i';
                            $params[] = &$pluginId;
                            $this->executePrepareBindQuery($sql, $params, 'dml');
                            $uninstallStatus = 1;
                            $msg = "Plugin removed successfully.";
                        }
                    } else {
                        $msg = 'Invalid Plugin, meta_plugin.json is blank';
                    }
                } else {
                    $msg = 'No backup files found! Could not able to Uninstall';
                }
            } else {
                $msg = 'Invalid parameters';
            }
        } catch (Exception $e) {
            $msg = $e->getMessage();
        }
        $response['status'] = ($uninstallStatus == 1) ? 'Success' : 'Failure';
        $response['response'] = $msg;
        $this->response($this->json($response), 200);
    }

    /**
     *
     * check api key is valid or not
     * @param varchar $apiKey
     */
    public function isValidCall($apiKey) {
        $flag = false;
        //$apiKey = mysqli_real_escape_string($apiKey);

        $sql = "SELECT api_key FROM api_data WHERE api_key =?";
        $params = array();
        $params[] = 's';
        $params[] = &$apiKey;
        $result = $this->executePrepareBindQuery($sql, $params);
        if (count($result) > 0) {
            $flag = true;
        }
        return $flag;
    }

    /**
     * check meta_plugin.json is valid or not
     * @param varchar $zip
     */
    public function validatePluginZip($zip) {
        $flag = false;
        for ($i = 1; $i < $zip->numFiles; $i++) {
            $fileName = $zip->getNameIndex($i);
            if ($fileName == "plugin_data/meta_plugin.json") {
                $flag = true;
            }
        }
        return $flag;
    }

    /**
     * check sql file available or not
     * @param varchar $zip
     */
    public function sqlFileCheck($zip) {
        $flag = false;
        $flag1 = false;
        $msg = '';
        for ($i = 1; $i < $zip->numFiles; $i++) {
            $fileName = $zip->getNameIndex($i);
            if ($fileName == "sql/install.sql") {
                $flag = true;
            }
            if ($fileName == "sql/uninstall.sql") {
                $flag1 = true;
            }
        }

        if ($flag == false && $flag1 == false) {
            $msg = "install.sql and uninstall.sql files are missing";
        } else if ($flag == false && $flag1 == true) {
            $msg = "install.sql file is missing";
        } else if ($flag == true && $flag1 == false) {
            $msg = "uninstall.sql file is missing";
        } else if ($flag == true && $flag1 == true) {
            $msg = 1;
        }
        return $msg;
    }

    /**
     * create plugin backup 
     * @param varchar $pluginID, $pluginName
     */
    public function createPluginBackUp($pluginID, $pluginName) {
        $pluginBackupRootFolder = $this->getBasePath() . "/plugin_backup";
        if (!file_exists($pluginBackupRootFolder)) {
            mkdir($pluginBackupRootFolder, 0777, true);
        }

        $pluginBackFolderName = $this->getPluginName($pluginID, $pluginName);
        $pluginBackupPath = $this->getBasePath() . "/plugin_backup/" . $pluginBackFolderName;
        if (!file_exists($pluginBackupPath)) {
            mkdir($pluginBackupPath, 0777, true);
        }
    }

    /**
     * get Plugin name 
     * @param varchar $pluginID, $pluginName
     */
    public function getPluginName($pluginID, $pluginName) {
        return $pluginBackFolderName = "plugin_" . $pluginName . "_" . $pluginID;
    }

    /**
     * get Plugin backup path 
     * @param varchar $pluginID, $pluginName
     */
    public function getPluginBackUpPath($pluginID, $pluginName) {
        $pluginBackFolderName = $this->getPluginName($pluginID, $pluginName);
        return $pluginBackupPath = $this->getBasePath() . "/plugin_backup/" . $pluginBackFolderName;
    }

    /**
     * get directory files and folders of a directory 
     * @param varchar $dir directory path
     * return array $results
     */
    public function getDirContents($dir, &$results = array()) {
        $files = scandir($dir);
        foreach ($files as $key => $value) {
            $path = $dir . DIRECTORY_SEPARATOR . $value;
            if (!is_dir($path)) {
                $results[] = str_replace(DIRECTORY_SEPARATOR, '/', $path);
            } else if ($value != "." && $value != "..") {
                $this->getDirContents($path, $results);
                $results[] = str_replace(DIRECTORY_SEPARATOR, '/', $path);
            }
        }
        return $results;
    }

    /**
     * get folder real path 
     * @param varchar $dir directory path
     */
    public function getDirRealPath($dir, &$results = array()) {
        $files = scandir($dir);

        foreach ($files as $key => $value) {
            $path = realpath($dir . DIRECTORY_SEPARATOR . $value);
            if (!is_dir($path)) {
                $results[] = $path;
            } else if ($value != "." && $value != "..") {
                $this->getDirContents($path, $results);
                $results[] = $path;
            }
        }

        return $results;
    }

    /**
     * execute sql file 
     * @param varchar $filename file path
     */
    public function run_sql_file($filename) {
        $errorMsg = "";
        $status = 1;
        $errorSql = '';

        $commands = @file_get_contents($filename); //load file
        //delete comments
        $lines = explode("\n", $commands);
        $commands = '';
        foreach ($lines as $line) {
            $line = trim($line);
            if ($line && !$this->startsWith($line, '--')) {
                $commands .= $line . "\n";
            }
        }
        $commands = explode(";", $commands); //convert to array
        //run commands
        $total = $success = 0;
        foreach ($commands as $command) {
            if (trim($command)) {
                $params = array();
                $sqlStatus = $this->executePrepareBindQuery($command, $params, 'dml');
                if ($sqlStatus == false) {
                    $errorSql .= $command . "\n";
                }
            }
        }
        if ($errorSql !== '' && strlen($errorSql) > 0) {
            $errorMsg = "\n" . 'Following SQL queries failed to run:' . "\n" . $errorSql;
            //xe_log($errorMsg);
            $status = 0;
        }
        return array($status, $errorMsg);
    }

    /*
      @ This is a helper function to the above function
     */

    public function startsWith($haystack, $needle) {
        $length = strlen($needle);
        return (substr($haystack, 0, $length) === $needle);
    }

    /* Delete files and folders of a seleted Path */

    public function deleteZipFileFolder($path) {
        if (is_dir($path) === true) {
            $files = array_diff(scandir($path), array('.', '..'));
            foreach ($files as $file) {
                $this->deleteZipFileFolder(realpath($path) . '/' . $file);
            }
            return rmdir($path);
        } else if (is_file($path) === true) {
            return unlink($path);
        }
        return false;
    }

}
