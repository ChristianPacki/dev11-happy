<?php

/* Check Un-authorize Access */
if (!defined('accessUser')) {
    die("Error");
}

class WordCloud extends UTIL {

    /**
     *
     * date created (dd-mm-yy)
     * date modified 15-4-2016 (dd-mm-yy)
     * Get word cloud details data
     *
     * @param (String)apikey
     * @param (Int)srtIndex
     * @param (Int)range
     * @param (String)fileExtensions
     * @param (String)data
     * @return json data
     *
     */
    public function getWordcloudDetails() {
        $apiKey = $this->_request['apikey'];
        if ($this->isValidCall($apiKey)) {
            try {
                $srtIndex = $this->_request['srtIndex'];
                $range = $this->_request['range'];
                $sql = "SELECT * FROM " . TABLE_PREFIX . "wordcloud LIMIT ?,?";
                $params = array();
                $params[] = 'ii';
                $params[] = &$srtIndex;
                $params[] = &$range;
                $dataFromValue = $this->executePrepareBindQuery($sql, $params);
                $dataArray['wordcloud'] = array();
                foreach ($dataFromValue as $i => $row) {
                    $dataArray['wordcloud'][$i]['id'] = $row['id'];
                    $dataArray['wordcloud'][$i]['name'] = $row['name'];
                    $dataArray['wordcloud'][$i]['price'] = $row['price'];
                    $dataArray['wordcloud'][$i]['filename'] = $row['file_name'];
                }
                $this->closeConnection();
                $this->response($this->json($dataArray, 1), 200);
            } catch (Exception $e) {
                $result = array('Caught exception:' => $e->getMessage());
                $this->response($this->json($result), 200);
            }
        } else {
            $msg = array("status" => "invalid");
            $this->response($this->json($msg), 200);
        }
    }

    /**
     *
     * date created (dd-mm-yy)
     * date modified 15-4-2016(dd-mm-yy)
     * Update World Cloud by id
     *
     * @param (String)apikey
     * @param (String)name
     * @param (Float)price
     * @param (int)id
     * @return json data
     *
     */
    public function updateWordcloudData() {
        try {
            $status = 0;
            if (!empty($this->_request) && isset($this->_request['name'])) {
                extract($this->_request);
                $id_str = implode(',', array_fill(0, count($id), '?'));
                $in_pattern = implode('', array_fill(0, count($id), 's'));
                $price = (isset($price) & $price) ? $price : 0.00;
                $name = addslashes($name);
                $sql = "UPDATE " . TABLE_PREFIX . "wordcloud SET name = ?, price = ? WHERE id IN(" . $id_str . ")";
                $params = array();
                $params[0] = 'ss';
                $params[0] .= $in_pattern;
                $params[] = &$name;
                $params[] = &$price;
                for ($i = 0; $i < count($id); $i++) {
                    $params[] = &$id[$i];
                }
                $status = $this->executePrepareBindQuery($sql, $params, 'dml');
            }
            $msg['status'] = ($status) ? 'Success' : 'Failure';
            $this->response($this->json($msg), 200);
        } catch (Exception $e) {
            $result = array('Caught exception:' => $e->getMessage());
            $this->response($this->json($result), 200);
        }
    }

    /**
     *
     * date created (dd-mm-yy)
     * date modified 15-4-2016(dd-mm-yy)
     * Add World Cloud
     *
     * @param (String)apikey
     * @param (String)name
     * @param (Float)price
     * @param (Array)files
     * @return json data
     *
     */
    //TRUNCATE wordcloud;
    public function addBulkWordcloud() {
        try {
            $status = 0;
            if (!empty($this->_request) && isset($this->_request['name'])) {
                if (!empty($_FILES['files'])) {
                    $sql = array();
                    $usql = '';
                    $rsql = '';
                    $shape_id = array();
                    $fname = array();
                    $thumbBase64Data = array();
                    $isql = "INSERT INTO " . TABLE_PREFIX . "wordcloud (name, price) VALUES";
                    $usql = "UPDATE " . TABLE_PREFIX . "wordcloud SET file_name = CASE id";
                    $usql1 = '';
                    $usql2 = '';
                    $params1 = array();
                    $params2 = array();
                    $params_pattern = array();
                    $params_pattern[1] = '';
                    $params_pattern[2] = '';
                    $dir = $this->getWordcloudSvgPath();
                    if (!$dir) {
                        $this->response('', 204);
                    }
                    //204 - immediately termiante this request
                    if (!file_exists($dir)) {
                        mkdir($dir, 0777, true);
                    }
                    $this->_request['price'] = (isset($this->_request['price']) && $this->_request['price'] != '') ? $this->_request['price'] : 0.00;
                    foreach ($_FILES['files']['tmp_name'] as $k => $v) {
                        $type =  array_pop(explode($_FILES['files']['type'][$k]));
                        $sql[$k] = $isql . "(?,?)";
                        $name = addslashes($this->_request['name']);
                        $params = array();
                        $params[0] = 'ss';
                        $params[] = &$name;
                        $params[] = &$this->_request['price'];
                        $shape_id[$k] = $this->executePrepareBindQuery($sql[$k], $params, 'insert');
                        $fname[$k] = 'w_' . $shape_id[$k] . '.' . $type;
                        // $thumbBase64Data[$k] = base64_decode($v['base64']);
                        // file_put_contents($dir . $fname[$k], $thumbBase64Data[$k]);
                        move_uploaded_file($v, $dir . $fname[$k]);
                        $usql1 .= " WHEN ? THEN ?";
                        $usql2 .= ',?';
                        $params_pattern[1] .= 'ss';
                        $params_pattern[2] .= 's';
                        $params1[] = &$shape_id[$k];
                        $params1[] = &$fname[$k];
                        $params2[] = &$shape_id[$k];
                    }

                    $usql = $usql . $usql1 . ' END WHERE id IN(' . substr($usql2, 1) . ')';
                    $params = array();
                    $params[0] = $params_pattern[1];
                    $params[0] .= $params_pattern[2];
                    $params = array_merge($params, $params1);
                    $params = array_merge($params, $params2);
                    $status = $this->executePrepareBindQuery($usql, $params, 'dml');
                }
            }
        } catch (Exception $e) {
            $msg = array('Caught exception:' => $e->getMessage());
        }
        $msg['status'] = ($status) ? 'Success' : 'Failure';
        $this->response($this->json($msg), 200);
    }

    /**
     *
     * date created (dd-mm-yy)
     * date modified 15-4-2016 (dd-mm-yy)
     * remove word cloud
     *
     * @param (String)apikey
     * @param (String)fileNames
     * @param (Array)fileIds
     * @return json data
     *
     */
    public function removeWordcloud() {
        $apiKey = $this->_request['apikey'];
        $fileNamesArray = $this->_request['fileNames'];
        $fileIdsArray = $this->_request['fileIds'];
        $status = 0;
        if ($this->isValidCall($apiKey)) {
            try {
//                $ids = implode("','", $fileIdsArray);
                $id_str = implode(',', array_fill(0, count($fileIdsArray), '?'));
                $in_pattern = implode('', array_fill(0, count($fileIdsArray), 's'));
                $sql = "DELETE FROM " . TABLE_PREFIX . "wordcloud WHERE id in (" . $id_str . ")";
                $params = array();
                $params[] = $in_pattern;
                for ($i = 0; $i < count($fileIdsArray); $i++) {
                    $params[] = &$fileIdsArray[$i];
                }
                $status = $this->executePrepareBindQuery($sql, $params, 'dml'); //executeGenericDMLQuery($sql)'".$ids."'
                if ($status) {
                    $dir = '';
                    $dir = $this->getWordcloudImagePath();
                    if (!$dir) {
                        $this->response('', 204);
                    }
                    //204 - immediately termiante this request
                    for ($j = 0; $j < sizeof($fileNamesArray); $j++) {
                        $filePath = $dir . $fileNamesArray[$j];
                        $msg = '';
                        if (file_exists($filePath) && is_file($filePath)) {
                            @chmod($filePath, 0777);
                            @unlink($filePath);
                        }
                    }
                }

                $this->closeConnection();
            } catch (Exception $e) {
                $msg = array('Caught exception:' => $e->getMessage());
            }
            $msg['status'] = ($status) ? 'success' : 'failed';
        } else {
            $msg['status'] = 'invalid';
        }
        $this->response($this->json($msg), 200);
    }

    /**
     *
     * date created 7-07-2016(dd-mm-yy)
     * date modified (dd-mm-yy)
     * save word cloud image data
     *
     * @param  (String)svgData
     *
     * return svg url array
     */
    public function saveWordcloudImage() {
        $result = array();
        if (!empty($this->_request) && !empty($_FILES['svgData'])) {
            /* && $this->isValidCall($this->_request['apiKey'])){ */
            $dir = $this->setUserWordCloudSvgPath();
            if (!file_exists($dir)) {
                mkdir($dir, 0777, true);
            }

            $fileUrl = $this->getUserWordCloudSvgUrl();
            $type = 'svg';
            extract($this->_request);
            if (!file_exists($dir)) {
                mkdir($dir, 0777, true);
            }
            $svgData = urldecode($svgData);
            $fname = uniqid('ci_', true) . '.' . $type;
            $file = $dir . $fname;
            $status = move_uploaded_file($_FILES['svgData']['tmp_name'], $file);
            // $status = file_put_contents($file, stripslashes($svgData));
            if ($status) {
                $result = $fileUrl . $fname;
            }
        }
        $msg['status'] = ($status) ? 'success' : 'failed';
        $msg['result'] = $result;
        $this->response($this->json($msg), 200);
    }

}
