<?php

/* Check Un-authorize Access */
if (!defined('accessUser')) {
    die("Error");
}

class Shape extends UTIL {

    /**
     *
     * date created (dd-mm-yy)
     * date modified 15-4-2016(dd-mm-yy)
     * remove Shape category
     *
     * @param (String)apikey
     * @return array
     *
     */
    public function allShapeCatagory() {
        $catagoryArray = array();
        try {
            $sql = "SELECT distinct id,category_name FROM " . TABLE_PREFIX . "shape_cat ORDER BY category_name DESC";
            $categoryDetail = array();
            $rows = $this->executePrepareBindQuery($sql); 
            for ($i = 0; $i < sizeof($rows); $i++) {
                $categoryDetail[$i]['id'] = $rows[$i]['id'];
                $categoryDetail[$i]['category_name'] = $rows[$i]['category_name'];
            }
            $this->response($this->json($categoryDetail, 1), 200);
        } catch (Exception $e) {
            $result = array('Caught exception:' => $e->getMessage());
            $this->response($this->json($result), 200);
        }
    }

    /**
     *
     * date created (dd-mm-yy)
     * date modified 15-4-2016(dd-mm-yy)
     * fetch shapes by search in admin
     *
     * @param (String)apikey
     * @param (String)categoryValue
     * @param (String)searchval
     * @param (Int)lastLoaded
     * @param (Int)loadCount
     * @return json data
     *
     */
    public function fetchShapesBySearchForAdmin() {
        try {
            $category = $this->_request['categoryValue'];
            //$subCategoryValue = $this->_request['subCategoryValue'];
            $searchval = addslashes($this->_request['searchval']);
            $shapeLastLoaded = $this->_request['lastLoaded'];
            $shapeLimit = $this->_request['loadCount'];
            $category = addslashes($category);
            $categoryValue = ($category != '') ? " and sc.category_name=?" : "";
            $joinText = '';
            $params = array('');
            if ($category != '' && $searchval == '') {
                $sql = "SELECT DISTINCT s.id, s.shape_name, s.file_name, s.price, s.status
                FROM " . TABLE_PREFIX . "shapes s, " . TABLE_PREFIX . "shape_cat_rel scr, " . TABLE_PREFIX . "shape_cat sc
                WHERE s.id = scr.shape_id
                AND scr.category_id = sc.id$categoryValue";
                $params[0] .= 's';
                $params[] = &$category;
            } else if ($category != '' && $searchval != '') {
                $sql = "select distinct s.id,s.shape_name ,s.file_name,s.price,s.status from " . TABLE_PREFIX . "shapes s,
                " . TABLE_PREFIX . "shape_tags st," . TABLE_PREFIX . "shape_tag_rel str," . TABLE_PREFIX . "shape_cat_rel scr," . TABLE_PREFIX . "shape_cat sc
                WHERE s.id= scr.shape_id AND scr.category_id = sc.id$categoryValue and (( s.shape_name LIKE ? ESCAPE '|') OR (s.id=str.shape_id and str.tag_id = st.id and st.name like ? ESCAPE '|'))";
                $params[0] .= 'sss';
                $params[] = &$category;
                $searchval = '%' . $searchval . '%';
                $params[] = &$searchval;
                $params[] = &$searchval;
            } else if ($category == '' && $searchval != '') {
                $sql = "select distinct s.id,s.shape_name ,s.file_name,s.price,s.status from " . TABLE_PREFIX . "shapes s,
                " . TABLE_PREFIX . "shape_tags st," . TABLE_PREFIX . "shape_tag_rel str," . TABLE_PREFIX . "shape_cat_rel scr," . TABLE_PREFIX . "shape_cat sc
                WHERE 1 and (( s.shape_name LIKE ? ESCAPE '|') OR (s.id=str.shape_id and str.tag_id = st.id and st.name like ? ESCAPE '|'))";
                $params[0] .= 'ss';
                $searchval = '%' . $searchval . '%';
                $params[] = &$searchval;
                $params[] = &$searchval;
            } else {
                $sql = "select distinct s.id,s.shape_name ,s.file_name,s.price,s.status from " . TABLE_PREFIX . "shapes s ";
            }
            $sql .= " ORDER BY s.id DESC "; //LIMIT $designLastLoaded, $designLimit
            // gettting total number of records present based on condition
            $rows = $this->executePrepareBindQuery($sql,$params,'assoc');
            $count = sizeof($rows);
            $sql .= " LIMIT $shapeLastLoaded, $shapeLimit";
            //getting deatiles of records by limitations
            $rows = $this->clearArray($rows);
            $rows = $this->executePrepareBindQuery($sql,$params,'assoc');
            //$this->log($sql) ;
            $shapeArray = array();
            for ($i = 0; $i < sizeof($rows); $i++) {
                $shapeArray[$i]['id'] = $rows[$i]['id'];
                $shapeArray[$i]['name'] = $rows[$i]['shape_name'];
                // $designArray[$i]['url'] = $this->getDesignImageURL().$rows[$i]['file_name'].'.svg';
                $shapeArray[$i]['url'] = $this->getShapeImageURL() . $rows[$i]['file_name'] . '.svg';
                $shapeArray[$i]['file_name'] = $rows[$i]['file_name'] . '.svg';
                $shapeArray[$i]['price'] = $rows[$i]['price'];
                $shapeArray[$i]['count'] = $count;
            }
            $sql = "SELECT COUNT(id) as total FROM " . TABLE_PREFIX . "shapes";
            $countShape = $this->executeFetchAssocQuery($sql);
            $x = array();
            $x['count'] = $count;
            $x['total_count'] = $countShape[0]['total'];
            $x['shapes'] = $shapeArray;
            $this->closeConnection();
            $this->response($this->json($x, 1), 200);
        } catch (Exception $e) {
            $result = array('Caught exception:' => $e->getMessage());
            $this->response($this->json($result), 200);
        }
    }

    /**
     *
     * date created (dd-mm-yy)
     * date modified 15-4-2016(dd-mm-yy)
     * Add shape and shape category
     *
     * @param (String)apikey
     * @param (String)shape_name
     * @param (Array)id
     * @param (Array)tags
     * @param (Array)files
     * @return json data
     *
     */
    //TRUNCATE shapes;Truncate shape_cat_rel;
    public function addBulkShape() {
        $status = 0;
        try {
            if (!empty($this->_request) && isset($this->_request['shape_name'])) {
                if (!empty($_FILES['files'])) {
                    $sql = array();
                    $rsql = '';
                    $shape_id = array();
                    $fname = array();
                    $thumbBase64Data = array();
                    $isql = "INSERT INTO " . TABLE_PREFIX . "shapes (shape_name, price) VALUES";
                    $usql = "UPDATE " . TABLE_PREFIX . "shapes SET file_name = CASE id";
                    $usql1 = '';
                    $usql2 = '';
                    $params1 = array();
                    $params2 = array();
                    $params_pattern = array();
                    $params_pattern[1] = '';
                    $params_pattern[2] = '';
                    $shape_tag_rel_sql = '';
                    $shape_cat_rel_sql = '';
                    $shape_tag_rel_sql_params = array();
                    $shape_cat_rel_sql_params = array();
                    $shape_tag_rel_sql_params[0] = '';
                    $shape_cat_rel_sql_params[0] = '';

                    $dir = $this->getShapeSvgPath();
                    if (!$dir) {
                        $this->response('', 204);
                    }
                    //204 - immediately termiante this request
                    if (!file_exists($dir)) {
                        mkdir($dir, 0777, true);
                    }

                    if (!empty($this->_request['tags'])) {
                        foreach ($this->_request['tags'] as $k => $v) {
                            $v = addslashes($v);
                            $tag_sql = "SELECT id,count( * ) AS nos FROM " . TABLE_PREFIX . "shape_tags WHERE name = ?";
                            $params = array();
                            $params[] = 's';
                            $params[] = &$v;
                            $res = $this->executePrepareBindQuery($tag_sql, $params, 'assoc'); 
                            if (!$res[0]['nos']) {
                                $tag_sql1 = "INSERT INTO " . TABLE_PREFIX . "shape_tags(name) VALUES(?)";
                                $tag_arr[] = $this->executePrepareBindQuery($tag_sql1, $params, 'insert'); 
                            } else {
                                $tag_arr[] = $res[0]['id'];
                            }
                        }
                    }

                    $this->_request['price'] = (isset($this->_request['price']) && $this->_request['price']) ? $this->_request['price'] : 0.00;
                    foreach ($_FILES['files']['tmp_name'] as $k => $v) {
                        $sql[$k] = $isql . "(?,?)";
                        $params = array();
                        $params[] = 'ss';
                        $this->_request['shape_name'] = addslashes($this->_request['shape_name']);
                        $params[] = &$this->_request['shape_name'];
                        $params[] = &$this->_request['price'];
                        $shape_id[$k] = $this->executePrepareBindQuery($sql[$k], $params, 'insert'); 
                        $fname[$k] = 's_' . $shape_id[$k];

                        // $thumbBase64Data[$k] = base64_decode($v);
                        // file_put_contents($dir . $fname[$k] . '.svg', $thumbBase64Data[$k]);
                        move_uploaded_file($v, $dir . $fname[$k] . '.svg');

                        $usql1 .= " WHEN ? THEN ?";
                        $usql2 .= ',?';
                        $params_pattern[1] .= 'ss';
                        $params_pattern[2] .= 's';
                        $params1[] = &$shape_id[$k];
                        $params1[] = &$fname[$k];
                        $params2[] = &$shape_id[$k];
                        if (!empty($tag_arr)) {
                            foreach ($tag_arr as $k1 => $v1) {
                                $shape_tag_rel_sql .= ",(?,?)";
                                $shape_tag_rel_sql_params[0] .= 'ss';
                                $shape_tag_rel_sql_params[] = &$shape_id[$k];
                                $shape_tag_rel_sql_params[] = &$tag_arr[$k1];
                            }
                        }
                        if (!empty($this->_request['category_id'])) {
                            foreach ($this->_request['category_id'] as $k2 => $v2) {
                                $shape_cat_rel_sql .= ",(?,?)";
                                $shape_cat_rel_sql_params[0] .= 'ss';
                                $shape_cat_rel_sql_params[] = &$shape_id[$k];
                                $shape_cat_rel_sql_params[] = &$this->_request['category_id'][$k2];
                            }
                        }
                    }
                    if (strlen($usql2)) {
                        $usql = $usql . $usql1 . ' END WHERE id IN(' . substr($usql2, 1) . ')';
                        $params = array();
                        $params[0] = $params_pattern[1];
                        $params[0] .= $params_pattern[2];
                        $params = array_merge($params, $params1);
                        $params = array_merge($params, $params2);
                        $status = $this->executePrepareBindQuery($usql, $params, 'dml'); 
                    }
                    if (strlen($shape_tag_rel_sql)) {
                        $shape_tag_rel_sql = "INSERT INTO " . TABLE_PREFIX . "shape_tag_rel (shape_id,tag_id) VALUES " . substr($shape_tag_rel_sql, 1);
                        $status = $this->executePrepareBindQuery($shape_tag_rel_sql, $shape_tag_rel_sql_params, 'dml'); 
                    }
                    if (strlen($shape_cat_rel_sql)) {
                        $shape_cat_rel_sql = "INSERT INTO " . TABLE_PREFIX . "shape_cat_rel (shape_id,category_id) VALUES " . substr($shape_cat_rel_sql, 1);
                        $status = $this->executePrepareBindQuery($shape_cat_rel_sql, $shape_cat_rel_sql_params, 'dml'); 
                    }
                }
            }
            $msg['status'] = ($status) ? 'Success' : 'Failure';
            $categoryId = $this->_request['category_id'][0];
            $this->updateJSONFile("shape", $categoryId, false);
            $this->updateJSONFile("shape", $categoryId, true);
            $this->response($this->json($msg), 200);
        } catch (Exception $e) {
            $result = array('Caught exception:' => $e->getMessage());
            $this->response($this->json($result), 200);
        }
    }

    /**
     *
     * date created (dd-mm-yy)
     * date modified 15-4-2016(dd-mm-yy)
     * getting shape details
     *
     * @param (String)apikey
     * @param (int)shape_id
     * @return array
     *
     */
    public function getShapeDetails() {
        $shapeId = $this->_request['shape_id'];
        $shapeData = array();
        try {
            $sql = "select distinct s.id as shape_id ,s.file_name,s.shape_name,s.price,s.status , c.id as category_id, c.category_name as category_name ,st.name as tag_name from " . TABLE_PREFIX . "shapes s left join  " . TABLE_PREFIX . "shape_cat_rel scr  on  s.id =scr.shape_id  left join  " . TABLE_PREFIX . "shape_cat c on scr.category_id = c.id
                left join " . TABLE_PREFIX . "shape_tag_rel str  on s.id = str.shape_id  left join " . TABLE_PREFIX . "shape_tags st on str.tag_id = st.id  where s.id = ?";
            $params = array();
            $params[] = 'i';
            $params[] = &$shapeId;
            $rows = $this->executePrepareBindQuery($sql, $params); 
            //fetching design detailes
            $shapeData['shape_details'] = array();
            $shapeData['shape_details']['shape_id'] = $rows[0]['shape_id'];
            $shapeData['shape_details']['file_name'] = $rows[0]['file_name'];
            $shapeData['shape_details']['shape_name'] = $rows[0]['shape_name'];
            $shapeData['shape_details']['price'] = $rows[0]['price'];
            $shapeData['shape_details']['status'] = $rows[0]['status'];

            //fetching categories and sub categories
            //$sql = "select * from ".TABLE_PREFIX."design_category_sub_category_rel dcsr where shape_id = 9";
            $shapeData['categories'] = array();
            for ($i = 0; $i < sizeof($rows); $i++) {
                $shapeData['categories'][$i]['category_id'] = $rows[$i]['category_id'];
                $shapeData['categories'][$i]['category_name'] = $rows[$i]['category_name'];
            }
            //$shapeData['categories'] = array_unique($shapeData['categories']);
            $shapeData['categories'] = $this->uniqueObjArray($shapeData['categories'], "category_name");
            // fetching tags
            $shapeData['tags'] = array();
            for ($i = 0; $i < sizeof($rows) && $rows[$i]['tag_name'] != null; $i++) {
                //$designData['tags'][$i]['category_id'] = $rows[$i]['category_id'];
                $shapeData['tags'][$i]['tag_name'] = $rows[$i]['tag_name'];
            }
            // $shapeData['tags'] = array_unique($shapeData['tags']);
            $shapeData['tags'] = $this->uniqueObjArray($shapeData['tags'], "tag_name");
            $this->response($this->json($shapeData, 1), 200);
        } catch (Exception $e) {
            $result = array('Caught exception:' => $e->getMessage());
            $this->response($this->json($result), 200);
        }
    }

    /**
     *
     * date created (dd-mm-yy)
     * date modified 15-4-2016(dd-mm-yy)
     * Add shape data by id
     *
     * @param (String)apikey
     * @param (String)shape_name
     * @param (Array)id
     * @param (Array)tags
     * @return json data
     *
     */
    public function updateShapeData() {
        $status = 0;
        try {
            if (!empty($this->_request) && isset($this->_request['id']) && isset($this->_request['shape_name'])) {
                extract($this->_request);
//                $id_str = implode(',', $id);
                $id_str = implode(',', array_fill(0, count($id), '?'));
                $in_pattern = implode('', array_fill(0, count($id), 's'));
                $price = (isset($price) & $price) ? $price : 0.00;
                $shape_name = addslashes($shape_name);
                $sql = "UPDATE " . TABLE_PREFIX . "shapes SET shape_name = ?, price = ? WHERE id IN(" . $id_str . ")";
                $params = array();
                $params[0] = 'ss';
                $params[] = &$shape_name;
                $params[] = &$price;
                $params[0] .= $in_pattern;
                for ($i = 0; $i < count($id); $i++) {
                    $params[] = &$id[$i];
                }
                $status = $this->executePrepareBindQuery($sql, $params, 'dml'); 
                $sql = "DELETE FROM " . TABLE_PREFIX . "shape_tag_rel WHERE shape_id IN(" . $id_str . ")";
                $params = array();
                $params[] = $in_pattern;
                for ($i = 0; $i < count($id); $i++) {
                    $params[] = &$id[$i];
                }
                $status = $this->executePrepareBindQuery($sql, $params, 'dml'); 

                $sql = "DELETE FROM " . TABLE_PREFIX . "shape_cat_rel WHERE shape_id IN(" . $id_str . ")";
                $params = array();
                $params[] = $in_pattern;
                for ($i = 0; $i < count($id); $i++) {
                    $params[] = &$id[$i];
                }
                $status = $this->executePrepareBindQuery($sql, $params, 'dml');

                $shape_tag_rel_sql = '';
                $shape_cat_rel_sql = '';
                $shape_tag_rel_sql_params = array();
                $shape_cat_rel_sql_params = array();
                $shape_tag_rel_sql_params[0] = '';
                $shape_cat_rel_sql_params[0] = '';
                $tag_arr = array();

                if (!empty($this->_request['tags'])) {
                    foreach ($this->_request['tags'] as $k => $v) {
                        $v = addslashes($v);
                        $tag_sql = "SELECT id,count( * ) AS nos FROM " . TABLE_PREFIX . "shape_tags WHERE name = ?";
                        $params = array();
                        $params[] = 's';
                        $params[] = &$v;
                        $res = $this->executePrepareBindQuery($tag_sql, $params, 'assoc'); 
                        if (!$res[0]['nos']) {
                            $tag_sql1 = "INSERT INTO " . TABLE_PREFIX . "shape_tags(name) VALUES(?)";
                            $tag_arr[] = $this->executePrepareBindQuery($tag_sql1, $params, 'insert'); 
                        } else {
                            $tag_arr[] = $res[0]['id'];
                        }
                    }
                }
                foreach ($this->_request['id'] as $k => $v) {
                    $shape_id[$k] = $v;
                    if (!empty($tag_arr)) {
                        foreach ($tag_arr as $k1 => $v1) {
                            $shape_tag_rel_sql .= ",(?,?)";
                            $shape_tag_rel_sql_params[0] .= 'ss';
                            $shape_tag_rel_sql_params[] = &$shape_id[$k];
                            $shape_tag_rel_sql_params[] = &$tag_arr[$k1];
                        }
                    }
                    if (!empty($this->_request['category_id'])) {
                        foreach ($this->_request['category_id'] as $k2 => $v2) {
                            $shape_cat_rel_sql .= ",(?,?)";
                            $shape_cat_rel_sql_params[0] .= 'ss';
                            $shape_cat_rel_sql_params[] = &$shape_id[$k];
                            $shape_cat_rel_sql_params[] = &$this->_request['category_id'][$k2];
                        }
                    }
                }
                if (strlen($shape_tag_rel_sql)) {
                    $shape_tag_rel_sql = "INSERT INTO " . TABLE_PREFIX . "shape_tag_rel (shape_id,tag_id) VALUES " . substr($shape_tag_rel_sql, 1);
                    $status = $this->executePrepareBindQuery($shape_tag_rel_sql, $shape_tag_rel_sql_params, 'dml'); 
                }
                if (strlen($shape_cat_rel_sql)) {
                    $shape_cat_rel_sql = "INSERT INTO " . TABLE_PREFIX . "shape_cat_rel (shape_id,category_id) VALUES " . substr($shape_cat_rel_sql, 1);
                    $status = $this->executePrepareBindQuery($shape_cat_rel_sql, $shape_cat_rel_sql_params, 'dml'); 
                }
            }
            $msg['status'] = ($status) ? 'Success' : 'Failure';
            $shapeCatId = $this->_request['category_id'];
            $this->updateJSONFile("shape", $shapeCatId[0], false);
            $this->updateJSONFile("shape", $shapeCatId[0], true);
            $this->response($this->json($msg), 200);
        } catch (Exception $e) {
            $result = array('Caught exception:' => $e->getMessage());
            $this->response($this->json($result), 200);
        }
    }

    /**
     *
     * date created (dd-mm-yy)
     * date modified 15-4-2016(dd-mm-yy)
     * Add shape category
     *
     * @param (String)apikey
     * @param (String)category_name
     * @return json data
     *
     */
    public function addShapeCategory() {
        $status = 0;
        try {
            if (!empty($this->_request) && isset($this->_request['category_name']) && $this->_request['category_name']) {
                $categoryName = $this->_request['category_name'];
                $query = "select count(*) count from " . TABLE_PREFIX . "shape_cat where category_name = ?";
                $params = array('s');
                $params[] = &$categoryName;
                $rows = $this->executePrepareBindQuery($query, $params); 
                $response = array();
                if ($rows[0]['count'] == "0") {
                    $sql = "INSERT INTO " . TABLE_PREFIX . "shape_cat (category_name) VALUES (?)";
                    $status = $this->executePrepareBindQuery($sql, $params, 'insert'); 
                    $msg['status'] = ($status) ? 'Success' : 'Failure';
                } else {
                    $msg['status'] = "Failure";
                }
            } else {
                $msg['status'] = "nodata";
            }
            $this->generateJSONFile('shape', $status);
            $this->response($this->json($msg), 200);
        } catch (Exception $e) {
            $result = array('Caught exception:' => $e->getMessage());
            $this->response($this->json($result), 200);
        }
    }

    /**
     *
     * date created (dd-mm-yy)
     * date modified 15-4-2016(dd-mm-yy)
     * upadte Shape category name
     *
     * @param (String)apikey
     * @param (int)id
     * @param (String)name
     * @return json data
     *
     */
    public function updateShapeCategory() {
        $status = 0;
        if (!empty($this->_request) && $this->_request['id'] && isset($this->_request['name'])) {
            extract($this->_request);
            try {
                $name = $name;
                $chk_duplicate = "SELECT COUNT(*) AS duplicate FROM " . TABLE_PREFIX . "shape_cat WHERE category_name=? AND id !=?";
                $params = array();
                $params[] = 'ss';
                $params[] = &$name;
                $params[] = &$id;
                $res = $this->executePrepareBindQuery($chk_duplicate, $params, 'assoc'); 
                if ($res[0]['duplicate']) {
                    $msg['msg'] = 'Duplicate Entry';
                } else {
                    $sql = "UPDATE " . TABLE_PREFIX . "shape_cat SET category_name=? WHERE id=?";
                    $status = $this->executePrepareBindQuery($sql, $params, 'dml'); 
                }
                $msg['status'] = ($status) ? 'success' : 'failed';
            } catch (Exception $e) {
                $msg = array('Caught exception:' => $e->getMessage());
            }
        } else {
            $msg['status'] = 'nodata';
        }

        $this->response($this->json($msg), 200);
    }

    /**
     *
     * date created (dd-mm-yy)
     * date modified 15-4-2016(dd-mm-yy)
     * remove Shape category
     *
     * @param (String)apikey
     * @param (int)removeCategory
     * @return array
     *
     */
    public function removeShapeCategory() {
        $pCategory = $this->_request['removeCategory'];
        try {
            $sql = "select count(*) count,id from " . TABLE_PREFIX . "shape_cat where category_name = ?";
            $params = array();
            $params[] = 's';
            $params[] = &$pCategory;
            $row = $this->executePrepareBindQuery($sql, $params); 
        } catch (Exception $e) {
            $result = array('Caught exception:' => $e->getMessage());
            $this->response($this->json($result), 200);
        }
        $response = array();
        if ($row[0]['count'] == "0") {
            // category not present error
            $response['status'] = false;
            $response['message'] = 'ERROR cateory not present';
        } else {
            try {
                $thisCatID = $row[0]['id'];
                $sql = "DELETE FROM " . TABLE_PREFIX . "shape_cat WHERE category_name= ?";
                $params = array();
                $params[] = 's';
                $params[] = &$pCategory;
                $this->executePrepareBindQuery($sql, $params, 'dml');
                $this->deleteJSONFile('shape', $thisCatID);
            } catch (Exception $e) {
                $result = array('Caught exception:' => $e->getMessage());
                $this->response($this->json($result), 200);
            }
            $response['status'] = true;
            $response['message'] = "'$pCategory' cateory delete successful !!";
        }
        $this->response($this->json($response, 1), 200);
    }

    /**
     *
     * date created (dd-mm-yy)
     * date modified 15-4-2016 (dd-mm-yy)
     * Get Sahapes
     *
     * @param (String)apikey
     * @param (String)categoryValue
     * @param (int)tabType
     * @param (int)sides
     * @return json data
     *
     */
    public function loadShapes() {
        try {
            $shapeArray = array();
            $categoryValue = $this->_request['categoryValue'];
            $searchedDesign = $this->_request['searchval'];
            $tabType = $this->_request['tabType'];
            $sides = $this->_request['sides'];
            $searchedDesign .= "%";
            if ($tabType == "N Shape") {
                $tabType = "nShape";
            } else {
                $tabType = "starShape";
            }
            $searchedDesign .= "_" . $tabType . "%";
            if ($sides != '') {
                $searchedDesign .= "_" . $sides;
            }
            $sql = "SELECT * FROM " . TABLE_PREFIX . "designs d inner join des_cat c on (d.category_id=c.id) ";
            $sql .= " WHERE lower(design_name) like lower(?) "; //%_nShape%_0
            $params = array();
            $params[0] = 's';
            $params[] = &$searchedDesign;
            if ($categoryValue && $categoryValue != 'All' && $categoryValue != '') {
                $sql .= " and c.category_name=?";
                $params[0] .= 's';
                $params[] = &$categoryValue;
            }
            $sql .= " and d.is_shape=1 and c.is_shape=1";

            $designsFromCatagory = $this->executePrepareBindQuery($sql, $params); 
            foreach ($designsFromCatagory as $i => $row) {
                $shapeArray[$i]['id'] = $row['id'];
                $shapeArray[$i]['name'] = $row['design_name'];
                $shapeArray[$i]['url'] = $this->getDesignImageURL() . $row['file_name'] . '.svg';
            }
            $this->closeConnection();
            $this->response($this->json($shapeArray), 200);
        } catch (Exception $e) {
            $result = array('Caught exception:' => $e->getMessage());
            $this->response($this->json($result), 200);
        }
    }

    /**
     *
     * date created (dd-mm-yy)
     * date modified 15-4-2016 (dd-mm-yy)
     * Get Sahape category
     *
     * @param (String)apikey
     * @param (Int)isShape
     * @return json data
     *
     */
    public function getShapeCatagories() {
        try {
            $isShape = $this->_request["isShape"];
            $catagoryArray = array();
            $sql = ($isShape == "true" || $isShape == undefined) ? "SELECT * FROM " . TABLE_PREFIX . "des_cat where is_shape=1" : "SELECT * FROM " . TABLE_PREFIX . "des_cat where is_shape=0";
            //$allCatagory = mysqli_query($con,$sql);
            $allCatagory = mysqli_query($this->db, $sql);
            while ($row = mysqli_fetch_array($allCatagory)) {
                array_push($catagoryArray, $row['category_name']);
            }
            $this->closeConnection();
            $this->response($this->json($catagoryArray), 200);
        } catch (Exception $e) {
            $result = array('Caught exception:' => $e->getMessage());
            $this->response($this->json($result), 200);
        }
    }

    /**
     *
     * date created (dd-mm-yy)
     * date modified 15-4-2016 (dd-mm-yy)
     * Fetch shape tags
     *
     * @param (String)apikey
     * @param (String)category
     * @return json data
     *
     */
    public function fetchShapeTags() {
        try {
            $sql = "select distinct t.name from " . TABLE_PREFIX . "shape_tags t ";
            $rows = $this->clearArray($rows);
            $rows = $this->executeGenericDQLQuery($sql);
            $tagArr = array();
            for ($i = 0; $i < sizeof($rows); $i++) {
                array_push($tagArr, $rows[$i]['name']);
            }
            $detailsArr['tags'] = $tagArr;
            $this->closeConnection();
            $this->response($this->json($detailsArr), 200);
        } catch (Exception $e) {
            $result = array('Caught exception:' => $e->getMessage());
            $this->response($this->json($result), 200);
        }
    }

    /**
     *
     * date created (dd-mm-yy)
     * date modified 15-4-2016 (dd-mm-yy)
     * delete shape by shape id
     *
     * @param (String)apikey
     * @param (int)shape_id
     * @return json data
     *
     */
    public function deleteShapeById() {
        $pShapeId = $this->_request['shape_id'];
        try {
            $sql = 'SELECT file_name FROM ' . TABLE_PREFIX . 'shapes WHERE id=?';
            $params = array();
            $params[] = 'i';
            $params[] = &$pShapeId;
            $res = $this->executePrepareBindQuery($sql, $params, 'assoc'); 
            $file = $res[0]['file_name'];
            $ds = DIRECTORY_SEPARATOR;
            $file = $this->getShapeImagePath() . $ds . $file;
            if (file_exists($file)) {
                @chmod($file, 07777);
                @unlink($file);
            }
            $sql = "delete from " . TABLE_PREFIX . "shapes where id=?";
            $this->executePrepareBindQuery($sql, $params, 'dml'); 
        } catch (Exception $e) {
            $result = array('Caught exception:' => $e->getMessage());
            $this->response($this->json($result), 200);
        }
        /* $response['status'] = "success";
          $response['message'] = "design deleted for design id ".$pDesignId;
          $this->response($this->json($response), 200); */
    }

    /**
     *
     * date created (dd-mm-yy)
     * date modified 15-4-2016(dd-mm-yy)
     * saving shapes detials
     *
     * @param (String)apikey
     * @param (String)fileName
     * @param (String)shapeName
     * @param (Flopat)price
     * @param (String)categoryIdTxt
     * @param (String)tagsText
     * @return json data
     *
     */
    public function saveShapeDetails() {
        try {
            $fileName = $this->_request['fileName'];
            $shapeName = $this->_request['shapeName'];
            // to be parse category sub category objects
            $category_txt = $this->_request['categoryIdTxt'];
            $tagsText = $this->_request['tagsText'];
            $price = floatval($this->_request['price']);
            $shapeId = $this->getShapeId($fileName, $shapeName, $price);
            $tagIdArr = $this->getShapeTagIdArr($tagsText);
            $this->mapShapeTagRel($shapeId, $tagIdArr);
            $this->mapShapeCategoryRel($shapeId, $category_txt);
            $msg['status'] = ($shapeId) ? 'success' : 'failed';
        } catch (Exception $e) {
            $msg = array('Caught exception:' => $e->getMessage());
        }
        $this->response($this->json($msg), 200);
        // $this->mapDesign_category_subCategory_rel($shapeId,$category_subCategory_txt);
    }

    /**
     *
     * date created (dd-mm-yy)
     * date modified 15-4-2016(dd-mm-yy)
     * Get shape id
     *
     * @param (String)apikey
     * @param (String)fileName
     * @param (String)shapeName
     * @param (Flopat)price
     * @return json data
     *
     */
    public function getShapeId($fileName, $shapeName, $price) {
        /* trimming the data */
        try {
            $fileName = trim($fileName);
            $shapeName = trim($shapeName);
            $price = trim($price);
            //$isShape = trim($isShape);
            $sql = "select id from " . TABLE_PREFIX . "shapes  where file_name = ?";
            $params = array();
            $params[] = 's';
            $params[] = &$fileName;
            $row = $this->executePrepareBindQuery($sql, $params, 'assoc'); 
            $shapeId;
            if (sizeof($row) == 0) {
                // inser the detile of desings , get the id
                $sql = "insert into " . TABLE_PREFIX . "shapes(file_name,shape_name,price) values(?,?,?)";
                $params = array();
                $params[] = 'sss';
                $params[] = &$fileName;
                $params[] = &$shapeName;
                $params[] = &$price;
                $shapeId = $this->executePrepareBindQuery($sql, $params, 'insert'); 
            } else {
                // get the id
                $shapeId = $row[0]['id'];
                $sql = "UPDATE " . TABLE_PREFIX . "shapes set shape_name = ? , price = ? where id = ?";
                $params = array();
                $params[] = 'ssi';
                $params[] = &$shapeName;
                $params[] = &$price;
                $params[] = &$shapeId;
                $this->executePrepareBindQuery($sql, $params, 'dml'); 
            }
            return $shapeId;
        } catch (Exception $e) {
            $result = array('Caught exception:' => $e->getMessage());
            $this->response($this->json($result), 200);
        }
    }

    /**
     *
     * date created (dd-mm-yy)
     * date modified 15-4-2016(dd-mm-yy)
     * Get shape and tags
     *
     * @param (String)apikey
     * @param (String)tagsText
     * @return array
     *
     */
    public function getShapeTagIdArr($tagsText) {
        $reqTag = explode(",", $tagsText);
        try {
            $sql = "select id,name from " . TABLE_PREFIX . "shape_tags";
            $dbTagArr = $this->executePrepareBindQuery($sql);
            $resTagArr = array();
            for ($i = 0; $i < sizeof($reqTag); $i++) {
                $found = false;
                $reqTag[$i] = trim($reqTag[$i]);
                for ($j = 0; $j < sizeof($dbTagArr); $j++) {
                    if ($dbTagArr[$j]['name'] == $reqTag[$i]) {
                        $found = true;
                        array_push($resTagArr, $dbTagArr[$j]['id']);
                        break;
                    }
                }
                if ($found == false && $reqTag[$i] != '') {
                    // insert category and push the id
                    $sql = "insert into " . TABLE_PREFIX . "shape_tags(name) values(?)";
                    $params = array();
                    $params[] = 's';
                    $params[] = &$reqTag[$i];
                    $tagId = $this->executePrepareBindQuery($sql, $params, 'insert'); 
                    array_push($resTagArr, $tagId);
                }
            }
            return $resTagArr;
        } catch (Exception $e) {
            $result = array('Caught exception:' => $e->getMessage());
            $this->response($this->json($result), 200);
        }
    }

    /**
     *
     * date created (dd-mm-yy)
     * date modified 15-4-2016(dd-mm-yy)
     * mapping shape and tags
     *
     * @param (String)apikey
     * @param (Array)tagIdArr
     * @param (int)shapeId
     * @return array
     *
     */
    public function mapShapeTagRel($shapeId, $tagIdArr) {
        try {
            $sql = "select shape_id , tag_id from " . TABLE_PREFIX . "shape_tag_rel";
            $rows = $this->executePrepareBindQuery($sql);
            for ($j = 0; $j < sizeof($tagIdArr); $j++) {
                $found = false;
                for ($k = 0; $k < sizeof($rows); $k++) {
                    if ($rows[$k]['shape_id'] == $shapeId && $rows[$k]['tag_id'] == $tagIdArr[$j]) {
                        $found = true;
                        break;
                    }
                }
                if (!$found) {
                    $sql = "insert into " . TABLE_PREFIX . "shape_tag_rel(shape_id,tag_id) values(? , ?)";
                    $params = array();
                    $params[] = 'ii';
                    $params[] = &$shapeId;
                    $params[] = &$tagIdArr[$j];
                    $this->executePrepareBindQuery($sql, $params, 'insert'); 
                }
            }
        } catch (Exception $e) {
            $result = array('Caught exception:' => $e->getMessage());
            $this->response($this->json($result), 200);
        }
    }

    /**
     *
     * date created (dd-mm-yy)
     * date modified 15-4-2016(dd-mm-yy)
     * map shape,category rel
     *
     * @param (String)apikey
     * @param (String)category_txt
     * @param (int)shapeId
     * @return array
     *
     */
    public function mapShapeCategoryRel($shapeId, $category_txt) {
        $category_arr = explode("*", $category_txt);
        try {
            $sql = "select shape_id , category_id from " . TABLE_PREFIX . "shape_cat_rel";
            $rows = $this->executePrepareBindQuery($sql);
            for ($i = 0; $i < sizeof($category_arr); $i++) {
                $found = false;
                for ($j = 0; $j < sizeof($rows); $j++) {
                    if ($rows[$j]['shape_id'] == $shapeId && $rows[$j]['category_id'] == $category_arr[$i]) {
                        $found = true;
                        break;
                    }
                }
                if (!$found) {
                    $sql = "insert into  " . TABLE_PREFIX . "shape_cat_rel (shape_id , category_id) values(?,?)";
                    $params = array();
                    $params[] = 'ii';
                    $params[] = &$shapeId;
                    $params[] = &$category_arr[$i];
                    $this->executePrepareBindQuery($sql, $params, 'dml'); 
                }
            }
        } catch (Exception $e) {
            $result = array('Caught exception:' => $e->getMessage());
            $this->response($this->json($result), 200);
        }
    }

    /**
     *
     * date created (dd-mm-yy)
     * date modified 15-4-2016(dd-mm-yy)
     * un map shape from rel table
     *
     * @param (String)apikey
     * @param (String)relTableName
     * @param (int)pId
     * @return array
     *
     */
    public function unMapShapeFromRelTable($pId, $relTableName) {
        try {
            $sql = "delete from ? where ?.shape_id = ?";
            $params = array();
            $params[] = 'ssi';
            $params[] = &$relTableName;
            $params[] = &$relTableName;
            $params[] = &$pId;
            $this->executePrepareBindQuery($sql, $params, 'dml'); 
        } catch (Exception $e) {
            $result = array('Caught exception:' => $e->getMessage());
            $this->response($this->json($result), 200);
        }
    }

    /**
     *
     * date created (dd-mm-yy)
     * date modified 15-4-2016(dd-mm-yy)
     * Edit shape details
     *
     * @param (String)apikey
     * @param (int)categoryId
     * @param (String)categoryName
     * @return array
     *
     */
    public function editShapeCategory() {
        $categoryId = $this->_request['categoryId'];
        $categoryName = $this->_request['categoryName'];
        try {
            $sql = "update " . TABLE_PREFIX . "shape_cat set category_name =? where shape_cat.id = ?";
            $params = array();
            $params[] = 'si';
            $params[] = &$categoryName;
            $params[] = &$categoryId;
            $row = $this->executePrepareBindQuery($sql, $params, 'dml'); 
        } catch (Exception $e) {
            $result = array('Caught exception:' => $e->getMessage());
            $this->response($this->json($result), 200);
        }
        $response = array();
        $response['status'] = true;
        $response['message'] = "category name changed to $categoryName";
        $this->response($this->json($response), 200);
    }

}
