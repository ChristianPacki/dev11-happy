<?php

/* Check Un-authorize Access */
if (!defined('accessUser')) {
    die("Error");
}

class ProductImage extends UTIL {

    /**
     *
     *date created 3-6-2016(dd-mm-yy)
     *date modified (dd-mm-yy)
     *add template side
     *
     *@param (String)apikey
     *@param (Array)product_templist
     *@return json data
     *
     */
    public function setProductTempList() {
        $status = 0;
        if (!empty($this->_request) && !empty($_POST['product_templist'])) {
            if(strpos($this->lsStore_type, "woo") === 0){
                $this->_request['product_templist'] = stripslashes($_POST['product_templist']);
            } else {
                $this->_request['product_templist'] = json_decode($_POST['product_templist'],true);
            }
            extract($this->_request);
            $dir = $this->setProductTemplatePath();
            $dir_3d = $this->get3dProductPath();
            if (!file_exists($dir_3d)) {
                mkdir($dir_3d, 0777, true);
            }
            if (!file_exists($dir)) {
                mkdir($dir, 0777, true);
            }
            if (!is_array($product_templist))
                $product_templist = json_decode($product_templist,true);
            //for obj file
            $obj_file_data = $this->_request['Object_fileFor_3D'];
            $obj_file = base64_decode(str_replace('data:application/json;base64,', '', $obj_file_data));
            $json_value = $product_templist['object3DData'];
            $check_3d_value = $this->_request['check_3d_value'];
            
            $type = 'png';
            $obj_type = 'json';
            $type = 'png';
			$insertSql = "INSERT INTO " . TABLE_PREFIX . "product_template (name,date_created,date_modified,is_default) VALUES(?,NOW(),NULL,?);";
            $params = array();
            $params[] = 'ss';
            $params[] = &$product_templist['name'];
            $params[] = &$product_templist['is_default'];
            $temp_id = $this->executePrepareBindQuery($insertSql, $params, 'insert'); 

            foreach ($product_templist['temp_side_list'] as $k => $v) {
                $sql_side = "INSERT INTO " . TABLE_PREFIX . "product_temp_side (product_temp_id,side_name,sort_order,image,date_created,date_modified)
                VALUES(?,?,?,?,NOW(),NULL);";
                $params = array();
                $params[] = 'ssss';
                $params[] = &$temp_id;
                $params[] = &$v['side_name'];
                $params[] = &$v['sort_order'];
                $params[] = &$type;
                $side_id[$k] = $this->executePrepareBindQuery($sql_side, $params, 'insert'); 

                if (strrpos($v['url'],'http') == false) {
                    if (!file_exists($dir . $temp_id)) {
                        mkdir($dir . $temp_id, 0777, true);
                    }

		    $fname = $dir . $temp_id . '/' . $side_id[$k] . '.' . $type;
                    $status = move_uploaded_file($_FILES[$v['url']]['tmp_name'],$fname);
                }
            }
             // for obj file
            if($check_3d_value == 1){
                $Sql_3d = "UPDATE ".TABLE_PREFIX."product_template set is_3d_preview=$check_3d_value where pk_id=$temp_id";
                $status_3d = $this->executeGenericDMLQuery($Sql_3d);
                if($status_3d){
                    if (!file_exists($dir_3d . $temp_id)) {
                        mkdir($dir_3d . $temp_id, 0777);
                    }
                    $obj_name = $dir_3d.$temp_id. '/model.'. $obj_type;
                    $json_name = $dir_3d.$temp_id. '/settings.json';
                    $obj_status = file_put_contents($obj_name,$obj_file);
                    $json_status = file_put_contents($json_name,$json_value);
                }  
            }
        }
        $msg['status'] = ($status) ? $this->getProductTempList() : 'failed';
        $this->response($this->json($msg), 200);
    }

    /**
     *
     *date created 3-6-2016(dd-mm-yy)
     *date modified (dd-mm-yy)
     *upadte template side
     *
     *@param (String)apikey
     *@param (Array)product_templist
     *@return json data
     *
     */
    public function updateProductTemp() {
        $status = 0;
        if (!empty($this->_request) && (!empty($this->_request['product_templist']) || !empty($_POST['product_templist']))) {
            if(strpos($this->lsStore_type, "woo") == 0){
                $string = str_replace('\"','"',$_POST['product_templist']);
                extract(json_decode($string,true));
            } else {
                extract(json_decode($this->_request['product_templist'], true));
            }

            $dir_3d = $this->get3dProductPath();
            if (!file_exists($dir_3d)) {
                mkdir($dir_3d, 0777, true);
            }

            //for obj file
            $obj_file_data = $this->_request['Object_fileFor_3D'];
            $obj_file = base64_decode(str_replace('data:application/json;base64,', '', $obj_file_data));
            $json_value = $this->_request['object3D_Data'];
            $check_3d_value = $this->_request['check_3d_value'];

            $name = addslashes($name);
			$checkExistSql = "SELECT count(*) AS nos FROM " . TABLE_PREFIX . "product_template WHERE name = ? AND pk_id != ?";
            $params = array();
            $params[] = 'ss';
            $params[] = &$name;
            $params[] = &$product_temp_id;
            $exist = $this->executePrepareBindQuery($checkExistSql, $params, 'assoc');
            if (!empty($exist) && $exist['nos']) {
                $msg['msg'] = 'Duplicate template name.';
            } else {
                $dir = $this->setProductTemplatePath();
                $type = 'png';

                $updateSql = "UPDATE " . TABLE_PREFIX . "product_template SET name = ?,date_modified = NOW() WHERE pk_id=?;";
                $params = array();
                $params[] = 'ss';
                $params[] = &$name;
                $params[] = &$product_temp_id;
                $status = $this->executePrepareBindQuery($updateSql, $params, 'dml'); 

                $Sql = "DELETE FROM " . TABLE_PREFIX . "product_temp_side WHERE product_temp_id=?;";
                $params = array();
                $params[] = 's';
                $params[] = &$product_temp_id;
                $status = $this->executePrepareBindQuery($Sql, $params, 'dml'); 
                if (!empty($temp_side_list)) {
                    if (!file_exists($dir)) {
                        mkdir($dir, 0777, true);
                    }

                    foreach ($temp_side_list as $k => $v) {
                        $sql_side = "INSERT INTO " . TABLE_PREFIX . "product_temp_side (product_temp_id,side_name,sort_order,image,date_modified)
                        VALUES(?,?,?,?,NOW());";
                        $params = array();
                        $params[] = 'ssss';
                        $params[] = &$product_temp_id;
                        $params[] = &$v['side_name'];
                        $params[] = &$v['sort_order'];
                        $params[] = &$type;
                        $side_id[$k] = $this->executePrepareBindQuery($sql_side, $params, 'insert');
                        $uploadedFile = false;

                        if ($v['side_id']) {
                            if (!file_exists($dir . $product_temp_id)) {
                                mkdir($dir . $product_temp_id, 0777, true);
                            }
                            $fnam = $dir . $product_temp_id . '/' . $side_id[$k] . '.' . $type;

                            if(strrpos($v['url'],'http') == false && isset($_FILES[$v['url']])) {
                              $status = move_uploaded_file($_FILES[$v['url']]['tmp_name'],$fnam);
                              $uploadedFile = $fnam;
                            } else {
                              $imageData = file_get_contents($v['url']);
                              $status = file_put_contents($fnam, $imageData);
                            }
                            unlink($dir . $product_temp_id . '/' . $v['side_id'] . '.' . $type);
                        }
                        if (strrpos($v['url'],'http') == false) {
                            if (!file_exists($dir . $product_temp_id)) {
                                mkdir($dir . $product_temp_id, 0777, true);
                            }
                            $fname = $dir . $product_temp_id . '/' . $side_id[$k] . '.' . $type;
                            if($uploadedFile)
                              copy($uploadedFile,$fname);
                            else
                              move_uploaded_file($_FILES[$v['url']]['tmp_name'],$fname);
                        }
                    }
                }

            // for obj file
            if($check_3d_value == 1){
                $Sql_3d = "UPDATE ".TABLE_PREFIX."product_template set is_3d_preview=$check_3d_value where pk_id=$product_temp_id";
                $status_3d = $this->executeGenericDMLQuery($Sql_3d);
                if (!file_exists($dir_3d . $product_temp_id)) {
                    mkdir($dir_3d . $product_temp_id, 0777);
                }

                $obj_file_data = $this->_request['Object_fileFor_3D'];
                if(isset($obj_file_data) && $obj_file_data != ''){
                    $obj_file = base64_decode(str_replace('data:application/json;base64,', '', $obj_file_data));
                    $obj_name = $dir_3d.$product_temp_id. '/model.json';
                    $obj_status = file_put_contents($obj_name,$obj_file);
                }
                if(isset($json_value) && $json_value != ''){
                    $json_name = $dir_3d.$product_temp_id. '/settings.json';
                    $json_status = file_put_contents($json_name,$json_value);
                } 
            }else{
                $Sql_3d = "UPDATE ".TABLE_PREFIX."product_template set is_3d_preview = 0 where pk_id = $product_temp_id";
                $status_3d = $this->executeGenericDMLQuery($Sql_3d);
            }

            }
        }
        $msg['status'] = ($status) ? $this->getProductTempList() : 'failed';
        $this->response($this->json($msg), 200);
    }

    /**
     *
     *date created 3-6-2016(dd-mm-yy)
     *date modified (dd-mm-yy)
     *Remove product template by template id
     *
     *@param (String)apikey
     *@param (int)templateId
     *@return json data
     *
     */
    public function removeProductTemplate() {
        $templateId = $this->_request['templateId'];
        if (isset($templateId) && $templateId != '') {
            $status = 0;
            $imageurl = $this->setProductTemplatePath();
            $sql_temp = "DELETE FROM " . TABLE_PREFIX . "product_template WHERE  pk_id = ?";
            $params = array();
            $params[] = 's';
            $params[] = &$templateId;
            $status = $this->executePrepareBindQuery($sql_temp, $params, 'dml'); 
            $sql_data = "DELETE FROM " . TABLE_PREFIX . "product_temp_rel WHERE  temp_id = ?";
            $params = array();
            $params[] = 's';
            $params[] = &$templateId;
            $status = $this->executePrepareBindQuery($sql_data, $params, 'dml');
            $sql_pro_temp = "DELETE FROM " . TABLE_PREFIX . "product_temp_side WHERE  product_temp_id = ?";
            $params = array();
            $params[] = 's';
            $params[] = &$templateId;
            $status = $this->executePrepareBindQuery($sql_pro_temp, $params, 'dml'); 
            if ($status) {
                $this->deleteZipFileFolder($imageurl . $templateId);
            }

            $msg['status'] = ($status) ? $this->getProductTempList() : 'failed';
        } else {
            $msg['status'] = 'no templateid';
        }
        $this->response($this->json($msg,1), 200);
    }

    /**
     *
     *date created 3-6-2016(dd-mm-yy)
     *date modified (dd-mm-yy)
     *Get product template list
     *
     *@param (String)apikey
     *@param (int)start
     *@param (int)count
     *@return json data
     *
     */
    public function getProductTempList() {
        $start = (isset($this->_request['start'])) ? $this->_request['start'] : 0;
        $range = (isset($this->_request['count'])) ? $this->_request['count'] : 0;
        $objDir = $this->get3dProductPath();
        
        $params = array();
        if ($range == 0) {
                $sql="SELECT pk_id,name,is_default,is_3d_preview FROM ".TABLE_PREFIX."product_template";
            }else{
            $sql = "SELECT pk_id,name,is_default, is_3d_preview FROM " . TABLE_PREFIX . "product_template LIMIT ?, ?";
            $params[] = 'ii';
            $params[] = &$start;
            $params[] = &$range;
            }
        $rows = $this->executePrepareBindQuery($sql, $params, 'assoc'); 
        $result = array();
        $i = 0;
        foreach ($rows as $value) {
            $result[$i]['product_temp_id'] = $value['pk_id'];
            $result[$i]['name'] = $value['name'];
            $result[$i]['is_default'] = $value['is_default'];
            $result[$i]['is_3d_preview'] = ($value['is_3d_preview'] == 1)?1:0;

            if($result[$i]['is_3d_preview'] == 1){
                $jsonFile = json_decode(file_get_contents($objDir.$value['pk_id']. '/settings.json'), true);
                $result[$i]['json_3d'] = $jsonFile;
            }else{
                $result[$i]['json_3d'] = array();
            }
            $side_arr = array();
            $side_sql = "SELECT distinct pts.pk_id,pts.sort_order,pts.side_name,pts.image
                        FROM " . TABLE_PREFIX . "product_template AS pt ," . TABLE_PREFIX . "product_temp_side AS pts
                        WHERE pts.product_temp_id =? ORDER BY sort_order";
            $params = array();
            $params[] = 's';
            $params[] = &$value['pk_id'];
            $row = $this->executePrepareBindQuery($side_sql, $params, 'assoc'); 
            if (!empty($row)) {
                $imageurl = $this->getProductTemplatePath();
                foreach ($row as $key => $v) {
                    $side_arr[$key]['side_id'] = $v['pk_id'];
                    $side_arr[$key]['side_name'] = $v['side_name'];
                    $side_arr[$key]['sort_order'] = $v['sort_order'];
                    $side_arr[$key]['url'] = $imageurl . $value['pk_id'] . '/' . $v['pk_id'] . '.' . $v['image'];
                }
                $result[$i]['temp_side_list'] = $side_arr;
            } else {
                $result[$i]['temp_side_list'] = [];
            }
            $i++;
        }
        $resultArr['product_templist'] = $result;
        $response = (empty($result)) ? array() : $resultArr['product_templist'];
        $this->response($this->json($resultArr,1), 200);
    }

    /**
     *
     *date created 3-6-2016(dd-mm-yy)
     *date modified (dd-mm-yy)
     *set default template to product
     *
     *@param (String)apikey
     *@param (int)templateId
     *@return json data
     *
     */
    public function setDefaultProductTemp() {
        $status = 0;
        if (isset($this->_request['templateId']) && $this->_request['templateId']) {
            $sql = "UPDATE " . TABLE_PREFIX . "product_template SET is_default='0'";
            $params = array();
            $status = $this->executePrepareBindQuery($sql, $params, 'dml'); 
            $sql = "UPDATE " . TABLE_PREFIX . "product_template SET is_default='1' WHERE pk_id=? LIMIT 1";
            $params = array();
            $params[] = 's';
            $params[] = &$this->_request['templateId'];
            $status = $this->executePrepareBindQuery($sql, $params, 'dml'); 
        }
        $msg['status'] = ($status) ? $this->getProductTempList() : 'failed';
        $this->response($this->json($msg), 200);
    }
    
    /**
     *
     *date created 3-6-2016(dd-mm-yy)
     *date modified (dd-mm-yy)
     *get object json for 3d object
     *     
     *@param (int)productId
     *@return json data
     *
     */
    public function getObjectJsonFor3DPreview() {
        $product_id = $this->_request['product_id'];
        $output = array();
        if(isset($product_id) && $product_id != '') {
            $sql = "SELECT temp_id FROM " . TABLE_PREFIX . "product_temp_rel WHERE product_id = ". $product_id;
            $row = $this->executeFetchAssocQuery($sql);
            $templateId = $row[0]['temp_id'];
            $objDir = $this->get3dProductPath().$templateId;
            $objPath = $objDir. '/model.json';
            $jsonFile = $objDir. '/settings.json';
            $json_data = file_get_contents($jsonFile);
            $json_data = json_decode($json_data,true);
            $output['json'] = $json_data;
            $fileURL = substr(XEPATH, 0, -1) .'/xetool'. HTML5_3D_DIR.$templateId;
            $output['objFilePath'] = $fileURL;
        }
        $this->response($this->json($output), 200);
    }   
    
}
