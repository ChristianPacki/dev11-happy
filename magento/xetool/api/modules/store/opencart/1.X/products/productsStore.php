<?php

/* Check Un-authorize Access */
if (!defined('accessUser')) {
    die("Error");
}

class ProductsStore extends UTIL {

    public function __construct() {
        parent::__construct();
        $this->datalayer = new Datalayer();
        $this->helper = new Helper();
    }

    /**
     * Used to get all the xe_size inside magento
     *
     * @param   nothing
     * @return  array contains all the xe_size inside store
     */
    public function getSizeArr() {
        $error = '';
        $result = $this->storeApiLogin();
        $size = $this->getStoreAttributes("xe_size");
        $filter = array("filter_name" => $size);
        if ($this->storeApiLogin == true) {
            $key = $GLOBALS['params']['apisessId'];
            try {
                $result = $this->datalayer->getOptions($filter);
                //$result = $proxy->call($key, 'catalog_category.tree');
            } catch (Exception $e) {
                $result = json_encode(array('isFault' => 1, 'faultMessage' => $e->getMessage()));
                $error = true;
            }
            if (!$error) {
                $categories = array();
                $this->response($result, 200);
            } else {
                $this->response(json_decode($result), 200);
            }
        } else {
            $msg = array('status' => 'apiLoginFailed', 'error' => json_decode($result));
            $this->response($this->json($msg), 200);
        }
    }

    /**
     * Used to get all the xe_color inside magento
     *
     * @param   nothing
     * @return  array contains all the xe_color inside store
     */
    public function getColorArr($isSameClass = false) {
        $error = '';
        $result = $this->storeApiLogin();
        $productId = 0;
        if (!empty($this->_request['productId'])) {
            $productId = $this->_request['productId'];
        }
        $color = $this->getStoreAttributes("xe_color");
        $lastLoaded = ($this->_request['lastLoaded']) ? $this->_request['lastLoaded'] : 0;
        $loadCount = ($this->_request['loadCount']) ? $this->_request['loadCount'] : 0;
        $filter = array("filter_name" => $color, "lastLoaded" => $lastLoaded, "loadCount" => $loadCount, 'oldConfId' => $productId);
        if ($this->storeApiLogin == true) {
            $key = $GLOBALS['params']['apisessId'];
            try {
                $result = $this->datalayer->getOptions($filter);
            } catch (Exception $e) {
                $result = json_encode(array('isFault' => 1, 'faultMessage' => $e->getMessage()));
                $error = true;
            }
            if (!$error) {
                if ($isSameClass) {
                    return $result;
                } else {
                    $this->response($result, 200);
                }
            } else {
                $this->response(json_decode($result), 200);
            }
        } else {
            $msg = array('status' => 'apiLoginFailed', 'error' => json_decode($result));
            $this->response($this->json($msg), 200);
        }
    }

    /**
     * Check whether the given sku exists or doesn't
     *
     * @param   $sku_arr
     * @return  true/false
     */
    public function checkDuplicateSku() {
// chk for storeid
        $error = false;
        $result = $this->storeApiLogin();
        if (!empty($this->_request) && $this->storeApiLogin == true) {
            $key = $GLOBALS['params']['apisessId'];
            if (!$error) {
                $filters = array(
                    'sku_arr' => $this->_request['sku_arr'],
                );

                try {
                    $result = $this->json(array()); //array("status"=>"failed");//$this->proxy->call($key, 'cedapi_product.checkDuplicateSku', $filters);
                } catch (Exception $e) {
                    $result = json_encode(array('isFault inside apiv4: ' => 1, 'faultMessage' => $e->getMessage()));
                    $error = true;
                }
            }
            $this->closeConnection();
            $this->response($result, 200);
        } else {
            $msg = array('status' => 'apiLoginFailed', 'error' => json_decode($result));
            $this->response($this->json($msg), 200);
        }
    }

    /**
     * Check whether xetool is enabled or disabled
     *
     * @param   nothing
     * @return  true/false
     */
    public function checkDesignerTool($t = 0) {
        $result = $this->storeApiLogin();
        if ($this->storeApiLogin == true) {
            $key = $GLOBALS['params']['apisessId'];
            try {
                $url = $this->getCurrentUrl() . '/qvcheck.php';
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                //  curl_setopt($ch,CURLOPT_HEADER, false);

                $version = curl_exec($ch);
                curl_close($ch);
                $path = $this->getCurrentUrl() . '/vqmod/xml/Riaxe_Product_Designer.xml';
                $status = is_array(@get_headers($path));
                if ($version == 'VQMOD ALREADY INSTALLED!' && $status) {
                    $result = 'Enabled';
                } else {
                    $result = 'Disabled';
                }
            } catch (Exception $e) {
                $result = json_encode(array('isFault' => 1, 'faultMessage' => $e->getMessage()));
            }
            if ($t) {
                return $result;
            } else {
                $this->response($result, 200);
            }
        } else {
            $msg = array('status' => 'apiLoginFailed', 'error' => json_decode($result));
            $this->response($this->json($msg), 200);
        }
    }

    /**
     * Get the list of variants available for a product
     *
     * @param   nothing
     * @return  json list of variants
     */
    public function getVariantList() {
        $error = false;
        $resultArr = $this->storeApiLogin();
        if ($this->storeApiLogin == true) {
            $key = $GLOBALS['params']['apisessId'];

            try {
                $confId = $this->_request['conf_pid'];
                $filters = array('confId' => $confId);
                //$resultArr = $this->proxy->call($key, 'cedapi_product.getVariantList',$filters);
                $pvariant = $this->datalayer->getProductInfo($confId);
                $resultArr = array("conf_id" => $confId, "variants" => $pvariant);
            } catch (Exception $e) {
                $resultArr = json_encode(array('isFault' => 1, 'faultMessage' => $e->getMessage()));
                $error = true;
            }

            if (!$error) {
                $this->response($this->json($resultArr), 200);
            } else {
                $this->response(json_decode($resultArr), 200);
            }
        } else {
            $msg = array('status' => 'apiLoginFailed', 'error' => json_decode($resultArr));
            $this->response($this->json($msg), 200);
        }
    }

    /**
     * Update item print status
     *
     * @param   orderID, productID, orderItemId, refid
     * @return  true/false
     */
    public function updateItemPrintStatus() {
        $error = false;

        $result = $this->storeApiLogin();
        if ($this->storeApiLogin == true) {
            $order_product_id = trim($this->_request['order_product_id']);
            $url = $this->getCurrentUrl() . '/?route=' . $this->extensionPath() . 'feed/web_api/updatePrintStatus&order_product_id=' . $order_product_id;
            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            $output = curl_exec($ch);

            curl_close($ch);
            $this->response($output, 200);
        } else {
            $msg = array('status' => 'apiLoginFailed', 'error' => json_decode($result));
            $this->response($this->json($msg), 200);
        }
    }

    /**
     * Add Product to store
     *
     * @param   product information
     * @return  product id,name in json format
     */
    public function addProducts() {
        $error = false;
        $result = $this->storeApiLogin();
        if (!empty($this->_request) && $this->storeApiLogin == true) {
            $key = $GLOBALS['params']['apisessId'];
            if (!$error) {
                try {
                    $result = $this->datalayer->addProduct(json_encode($this->_request, true));
                } catch (Exception $e) {
                    $result = json_encode(array('isFault' => 1, 'faultMessage' => $e->getMessage()));
                    $error = true;
                }
            }
            if ($result['status'] == 'success') {
                $this->response($this->json($result), 200);
            } else {
                $this->response(json_decode($result), 200);
            }
        } else {
            $msg = array('status' => 'apiLoginFailed', 'error' => json_decode($result));
            $this->response($this->json($msg), 200);
        }
    }

    /**
     *
     * date created 31-05-2016(dd-mm-yy)
     * date modified (dd-mm-yy)
     * Add template as product
     *
     *
     */
    public function addTemplateProducts() {
        $error = false;
        if (!empty($this->_request['data'])) {
            $data = json_decode(urldecode($this->_request['data']), true);
            $apikey = $this->_request['apikey'];
            $result = $this->storeApiLogin();
            if ($this->storeApiLogin == true) {
                $key = $GLOBALS['params']['apisessId'];
                if (!$error) {
                    try {
                        $predecorateObj = Flight::predecorate();
                        $arr = array('store' => $this->getDefaultStoreId(), 'data' => $data, 'configFile' => $data['images'], 'oldConfId' => $data['simpleproduct_id'], 'varColor' => $data['color_id'], 'varSize' => $data['sizes'], 'productStatus' => $data['captureType']);
                        $result = $this->datalayer->addTemplateProducts($arr);
                        $resultData = json_decode($result, true);
                        $templateData = $predecorateObj->saveTemplateInfo($resultData['conf_id'], $data['assignedCategory'], $data['captureType']);
                        $this->_request['productid'] = $data['simpleproduct_id'];
                        $this->_request['isTemplate'] = 1;
                        $sides = sizeof($data['images']);
                        $productTemplate = $this->getProductTemplateByProductId($data['simpleproduct_id']);
                        if ($data['boundary_type'] == "single") {
                            $maskData = $this->getMaskData($sides);
                            $maskDatas = json_decode($maskData, true);
                            $printArea = array();
                            $printArea = $this->getPrintareaType($data['simpleproduct_id']);
                            foreach ($maskDatas as $key => $maskData) {
                                $maskScalewidth[$key] = $maskData['mask_width'];
                                $maskScaleHeight[$key] = $maskData['mask_height'];
                                $maskPrice[$key] = $maskData['mask_price'];
                                $scaleRatio[$key] = $maskData['scale_ratio'];
                                $scaleRatio_unit[$key] = $maskData['scaleRatio_unit'];
                            }
                            $this->customRequest(array('maskScalewidth' => $maskScalewidth, 'maskScaleHeight' => $maskScaleHeight, 'maskPrice' => $maskPrice, 'scaleRatio' => $scaleRatio, 'scaleRatio_unit' => $scaleRatio_unit, 'maskstatus' => $printArea['mask'], 'unitid' => $printArea['unit_id'], 'pricePerUnit' => $printArea['pricePerUnit'], 'maxWidth' => $printArea['maxWidth'], 'maxHeight' => $printArea['maxHeight'], 'boundsstatus' => $printArea['bounds'], 'customsizestatus' => $printArea['custom_size'], 'customMask' => $printArea['customMask']));
                            $printSizes = $this->getDtgPrintSizesOfProductSides($data['simpleproduct_id']);
                            $printSizes = $this->getDtgPrintSizesOfProductSides($data['simpleproduct_id']);
                            $this->customRequest(array('productid' => $resultData['conf_id'], 'jsondata' => json_encode($maskDatas), 'printsizes' => $printSizes));
                            $this->saveMaskData();
                            if ($printSizes['status'] != 'nodata') {
                                $this->setDtgPrintSizesOfProductSides();
                            }
                        }else{
                             // multiple boundary set up
                            $multipleObj = Flight::multipleBoundary();
                            $resultData['old_conf_id'] = $data['simpleproduct_id'];
                            $multiBoundData = $multipleObj->getMultiBoundMaskData($resultData['old_conf_id']);
                            $multiBoundData[0]['id'] = 0;
                            $unitArr = array($multiBoundData[0]['scaleRatio_unit']);
                            $saveStatus = $multipleObj->saveMultipleBoundary($resultData['conf_id'], json_encode($multiBoundData), $unitArr, true);
                        }
                        
                        $this->saveProductTemplateData($data['print_method_id'], $data['ref_id'], $data['simpleproduct_id'], $resultData['conf_id']);
                        if (!empty($productTemplate['tepmlate_id'])) {
                            $this->customRequest(array('pid' => $resultData['conf_id'], 'productTempId' => $productTemplate['tepmlate_id']));
                            $this->addTemplateToProduct();
                        }
                    } catch (Exception $e) {
                        $result = json_encode(array('isFault' => 1, 'faultMessage' => $e->getMessage()));
                        $error = true;
                    }
                }
                $this->response($result, 200);
            } else {
                $msg = array('status' => 'apiLoginFailed', 'error' => json_decode($result));
                $this->response($this->json($msg), 200);
            }
        }
    }

    /**
     * Get Category list by product id
     *
     * @param   pid
     * @return  category list in json format
     */
    public function getCategoriesByProduct() {
        //$error='';
        $result = $this->storeApiLogin();
        $printProfile = Flight::printProfile();
        if ($this->storeApiLogin == true && isset($this->_request['pid']) && $this->_request['pid']) {
            $key = $GLOBALS['params']['apisessId'];
            $res = array();
            try {
                $catIdArr = $this->datalayer->getProductCategoryList($this->_request['pid']);
                if (empty($catIdArr)) {
                    $res = $printProfile->getDefaultPrintMethodId();
                } else {
                    $catIdStr = implode(',', array_fill(0, count($catIdArr), '?'));
                    $in_pattern = implode('', array_fill(0, count($catIdArr), 's'));
                    $sql = 'SELECT DISTINCT pm.pk_id AS print_method_id,pm.name FROM ' . TABLE_PREFIX . 'print_method AS pm INNER JOIN ' . TABLE_PREFIX . 'product_category_printmethod_rel AS pcpmr ON pm.pk_id=pcpmr.print_method_id WHERE pcpmr.product_category_id IN(' . $catIdStr . ')';
                    $params = array();
                    $params[] = $in_pattern;
                    for ($i = 0; $i < count($catIdArr); $i++) {
                        $params[] = &$catIdArr[$i];
                    }
                    $res = $this->executePrepareBindQuery($sql, $params, 'assoc');
                    if (empty($res)) {
                        $res = $printProfile->getDefaultPrintMethodId();
                    }
                }
            } catch (Exception $e) {
                $res = json_encode(array('isFault' => 1, 'faultMessage' => $e->getMessage()));
            }
            $this->response(json_encode($res), 200);
        } else {
            $msg = array('status' => 'apiLoginFailed', 'error' => json_decode($result));
            $this->response($this->json($msg), 200);
        }
    }

    /**
     * Get Sub-Category list
     *
     * @param   selectedCategory
     * @return  sub-category list in json format
     */
    public function getsubCategories() {
        $error = '';
        $result = $this->storeApiLogin();
        $cat_id = $this->_request['selectedCategory'];
        if ($this->storeApiLogin == true) {
            $key = $GLOBALS['params']['apisessId'];
            try {
                //$filters=array('catid'=>$this->_request['selectedCategory'], 'store'=>1 );
                $result = array('subcategories' => $this->datalayer->getSubCategory($cat_id));
            } catch (Exception $e) {
                $result = json_encode(array('isFault' => 1, 'faultMessage' => $e->getMessage()));
                $error = true;
            }

            if (!$error) {
                $categories = array();
                $this->response($this->json($result), 200);
            } else {
                $this->response(json_decode($result), 200);
            }
        } else {
            $msg = array('status' => 'apiLoginFailed', 'error' => json_decode($result));
            $this->response($this->json($msg), 200);
        }
    }

    /**
     * Get product count
     *
     * @param   orderIncrementId
     * @return  integer number of product
     */
    public function getProductCount() {
        $error = false;
        $result = $this->wcApi->get_products_count();
        if (!isset($result->errors)) {
            try {
                $result = array('size' => $result->count);
            } catch (Exception $e) {
                $result = json_encode(array('isFault' => 1, 'faultMessage' => $e->getMessage()));
                $error = true;
            }
            if (!$error) {
                $this->response($this->json($result), 200);
            } else {
                $msg = array('status' => 'failed', 'error' => $this->de_json($result));
                $this->response($this->json($msg), 200);
            }
        } else {

            $msg = array('status' => 'apiLoginFailed', 'error' => $result);
            $this->response($this->json($msg), 200);
        }
    }

    /**
     *
     * date created (dd-mm-yy)
     * date modified 15-4-2016(dd-mm-yy)
     * fetch print method id and name
     *
     * @param (String)apikey
     * @param (int)productid
     * @return json data
     *
     */
    public function getProductPrintMethod() {
        $productId = $this->_request['productid'];
        $key = $this->_request['apikey'];
        if (!empty($productId)) {
            // &&  !empty($key) && $this->isValidCall($key)){
            $error = false;
            //get print type
            $productPrintType = $this->getProductPrintMethodType($productId);
            if (!empty($productPrintType)) {
                //$this->log('productPrintTypeSql: '.$productPrintTypeSql, true, 'Zsql.log');
                foreach ($productPrintType as $k2 => $v2) {
                    $printDetails[$k2]['print_method_id'] = $v2['pk_id'];
                    $printDetails[$k2]['name'] = $v2['name'];
                }
            } else {
                $result = $this->storeApiLogin();
                if ($this->storeApiLogin == true) {
                    try {
                        $catIds = $this->datalayer->getProductCategoryList($productId);
                        //$lencatid = count(trim($catIds));
                        $catIds = implode(',', (array) $catIds);
                        //get print method 
                        $printDetails = $this->getPrintMethodDetailsByCategory($catIds);
                    } catch (Exception $e) {
                        $result = json_encode(array('isFault' => 1, 'faultMessage' => $e->getMessage()));
                        $error = true;
                    }
                } else {
                    $msg = array('status' => 'apiLoginFailed', 'error' => json_decode($result));
                    $this->response($this->json($msg), 200);
                }
            }
            if (!$error) {

                $resultArr = $printDetails;
                $result = json_encode($resultArr);
                $this->response($this->json($resultArr), 200);
            } else {
                $this->response($result, 200);
            }
        } else {
            $msg = array("status" => "invalidkey");
            $this->response($this->json($msg), 200);
        }
    }

    /**
     * Check magento version
     *
     * @param   nothing
     * @return  string $version
     */
    public function storeVersion() {
        $result = $this->storeApiLogin();
        if ($this->storeApiLogin == true) {
            //$key = $GLOBALS['params']['apisessId'];

            try {
                //$result = $this->proxy->call($key, 'cedapi_product.storeVersion');
                //return $version = (!empty($result))?strchr($result,'.',true):1;
                return $version = 1;
            } catch (Exception $e) {
                $result = json_encode(array('isFault' => 1, 'faultMessage' => $e->getMessage()));
                $error = true;
            }
        } else {
            $msg = array('status' => 'apiLoginFailed', 'error' => json_decode($result));
            $this->response($this->json($msg), 200);
        }
    }

    /**
     *
     * date created (dd-mm-yy)
     * date modified 15-4-2016 (dd-mm-yy)
     * Build Product Array
     *
     * @param (String)apikey
     * @param (Array)cartArr
     * @param (Int)refid
     * @return Array or boolean value
     *
     */
    public function buildProductArray($cartArr, $refid) {
        try {
            $configProductId = $cartArr['id'];
            $custom_price = $cartArr['addedprice'];
            //$cutom_design_refId = $cartArr['refid'];
            $cutom_design_refId = $refid;
            $quantity = $cartArr['qty'];
            $simpleProductId = $cartArr['simple_product']['simpleProductId'];
            //$color1 = $cartArr['simple_product']['color1'];
            $xeColor = $cartArr['simple_product']['xe_color'];
            $xeSize = $cartArr['simple_product']['xe_size'];
            $product = array(
                "product_id" => $configProductId,
                "qty" => $quantity,
                "simpleproduct_id" => $simpleProductId,
                "options" => array('xe_color' => $xeColor, 'xe_size' => $xeSize),
                "custom_price" => $custom_price,
                "custom_design" => $cutom_design_refId,
            );
            if ($quantity > 0) {
                return $product;
            } else {
                return false;
            }
        } catch (Exception $e) {
            $result = array('Caught exception:' => $e->getMessage());
            return $result;
        }
    }

    /**
     *
     * date created 07-06-2016(dd-mm-yy)
     * date modified (dd-mm-yy)
     * Save product template data
     *
     * @param (Int)old productid
     * @param (Int)new productid
     * @param (Int)refId
     *
     */
    public function saveProductTemplateData($printMethodId, $refId, $oldId, $newId) {
        $apiKey = $this->_request['apikey'];
        if ($this->isValidCall($apiKey)) {
            try {
                $msg = $this->saveProductTemplateStateRel($printMethodId, $refId, $oldId, $newId);
                return $this->json($msg);
            } catch (Exception $e) {
                $result = array('Caught exception:' => $e->getMessage());
                $this->response($this->json($result), 200);
            }
        }
    }

    /**
     * Used to get all attributes value details for simple product
     * @return list of attributes
     */
    public function getCustomOption() {
        $error = 0;
        $result = $this->storeApiLogin();
        if ($this->storeApiLogin == true) {
            if (!isset($this->_request['id']) || trim($this->_request['id']) == '') {
                $msg = array('status' => 'invalid productId', 'productId' => $this->_request['id']);
                $this->response($this->json($msg), 204);
            } else {
                $product_id = trim($this->_request['id']);
            }
            try {
                $result = $this->datalayer->getProductOptions($product_id);
            } catch (Exception $e) {
                $result = json_encode(array('isFault' => 1, 'faultMessage' => $e->getMessage()));
                $error = true;
            }
            if (!$error) {
                $categories = array();
                $this->response($this->json($result), 200);
            } else {
                $this->response(json_decode($result), 200);
            }
        } else {
            $msg = array('status' => 'apiLoginFailed', 'error' => json_decode($result));
            $this->response($this->json($msg), 200);
        }
    }

    /**
     *
     * date created 13-05-2017(dd-mm-yy)
     * date modified (dd-mm-yy)
     * Get Attributes
     *
     */
    public function getAttributes() {
        $error = false;
        $result = $this->storeApiLogin();
        if ($this->storeApiLogin == true) {
            $filters = array(
                'store' => 0,
            );
            try {
                $result = $this->datalayer->getAllOptions($filters);
                if (empty($result)) {
                    $result = json_encode(array('No Records Found'));
                    $error = true;
                }
            } catch (Exception $e) {
                $result = json_encode(array('isFault' => 1, 'faultMessage' => $e->getMessage()));
                $error = true;
            }
            if (!$error) {
                $this->response($this->json($result), 200);
            } else {
                $this->response($this->json($result), 200);
            }
        } else {
            $msg = array('status' => 'apiLoginFailed', 'error' => json_decode($result));
            $this->response($this->json($msg), 200);
        }
    }

    /**
     *
     * Created By : Ramasankar
     * date created 12-10-2017(dd-mm-yy)
     * date modified (dd-mm-yy)
     * Returning List of Products from store product to getAllProducts() in products module
     * Scope : this page 
     *
     * @param 
     * @return Array data
     *
     */
    public function productList() {
        $limit = 10;
        $start = 0;
        $categoryid = 0;
        $searchstring = '';
        $productsCat = '';
        $loadVariants = false;
        $startvar = 0;
        $limitvar = 50;
        if (isset($this->_request['categoryid']) && trim($this->_request['categoryid']) != '') {
            $categoryid = trim($this->_request['categoryid']);
        }
        if (isset($this->_request['searchstring']) && trim($this->_request['searchstring']) != '') {
            $searchstring = trim($this->_request['searchstring']);
        }

        if (isset($this->_request['start']) && trim($this->_request['start']) != '') {
            $start = trim($this->_request['start']);
        }
        if (isset($this->_request['range']) && trim($this->_request['range']) != '') {
            $limit = trim($this->_request['range']);
        }
        if (isset($this->_request['loadVariants']) && trim($this->_request['loadVariants']) == true) {
            $loadVariants = true;
        }
        $result = $this->storeApiLogin();
        if ($this->storeApiLogin == true) {
            try {
                $result = $this->datalayer->getAllProducts((object) $this->_request, $categoryid);
                return $result;
            } catch (Exception $e) {
                $result = array('isFault' => 1, 'faultMessage' => $e->getMessage());
                return $result;
            }
        } else {
            $res = array('isFault' => 2, 'error' => json_decode($result));
            return $res;
        }
    }

    /**
     *
     * date created 10-10-2017(dd-mm-yy)
     * date modified (dd-mm-yy)
     * Returning category details from store to getCategories()
     * Scope : this page 
     *
     * @param 
     * @return Array data
     *
     */
    public function allCategories() {
        $result = $this->storeApiLogin();
        $printId = $this->_request['printId'];
        if ($this->storeApiLogin == true) {
            try {
                $categories = $this->datalayer->getCategories();
                return $categories;
            } catch (Exception $e) {
                $result = json_encode(array('isFault' => 1, 'faultMessage' => $e->getMessage()));
                $error = true;
            }
        } else {
            $res = array('isFault' => 2, 'error' => json_decode($result));
            return $res;
        }
    }

    /**
     *
     * date created 27-09-2017(dd-mm-yy)
     * date modified (dd-mm-yy)
     * Returning details of a product
     * Scope : this page 
     *
     * @param 
     * @return Array data
     *
     */
    public function productDetails($client = '') {
        $result = $this->storeApiLogin();
        if ($this->storeApiLogin == true) {
            try {
                $result = $this->datalayer->getProductById((object) $this->_request);
                if ($client != '') {
                    $simpleProductId = trim($result['pvid']);
                    $tier = array();
                    $tierPrice = $this->datalayer->getTierPrice($simpleProductId);
                    if (!empty($tierPrice)) {
                        foreach ($tierPrice as $k => $value) {
                            $tier[$k]['tierQty'] = (int) $value['quantity'];
                            $tier[$k]['percentage'] = round(100 - $value['price'] / $surplusPrice * 100);
                            $tier[$k]['tierPrice'] = number_format($value['price'], 2);
                        }
                    }
                    // For Color Swatch
                    $colorId = $result['xe_color_id'];
                    $result['colorSwatch'] = $this->getColorSwatchProduct($colorId);
                    $result['tierPrices'] = $tier;
                }
                return $result;
            } catch (Exception $e) {
                $result = array('isFault' => 1, 'faultMessage' => $e->getMessage());
                return $result;
            }
        } else {
            $res = array('isFault' => 2, 'error' => json_encode($result));
            return $res;
        }
    }

    /**
     *
     * date created 10-10-2017(dd-mm-yy)
     * date modified (dd-mm-yy)
     * Returning product print method parameter details to getPrintMethodByProduct()
     * Scope : this page 
     *
     * @param 
     * @return Array data
     *
     */
    public function printMethodParameters() {
        $result = $this->storeApiLogin();
        if ($this->storeApiLogin == true && isset($this->_request['pid']) && $this->_request['pid']) {
            $key = $GLOBALS['params']['apisessId'];
            $filters = array('store' => $this->getDefaultStoreId());
            $confProductId = $this->_request['pid'];
            $isAdmin = (isset($this->_request['isAdmin']) && trim($this->_request['isAdmin']) == true) ? true : false;
            //  Do not send any print method ID for multiple boundary product
            //inkxe
            $catIds = $this->datalayer->getProductCategoryList($confProductId);
            $refid = '';
            $result = array('confProductId' => $confProductId, 'isAdmin' => $isAdmin, 'catIds' => $catIds, 'refid' => $refid);
            return $result;
        } else {
            $res = array('isFault' => 2, 'error' => json_encode($result));
            return $res;
        }
    }

    /**
     *
     * date created 27-09-2017(dd-mm-yy)
     * date modified (dd-mm-yy)
     * Returning size and quantity of a product
     *
     * @param 
     * @return Array data
     *
     */
    public function sizeAndQuantityDetails() {
        $result = $this->storeApiLogin();
        if ($this->storeApiLogin == true) {
            $productId = trim($this->_request['productId']);
            if (isset($this->_request['byAdmin'])) {
                $byAdmin = true;
            } else {
                $byAdmin = false;
            }
            try {
                $resultArr = $this->datalayer->getSizeAndQuantity((object) $this->_request);
                foreach ($resultArr['quantities'] as $key => $value) {
                    $tier = array();
                    $tierPrice = $this->datalayer->getTierPrice($resultArr['quantities'][$key]['simpleProductId']);
                    if (!empty($tierPrice)) {
                        foreach ($tierPrice as $k => $value) {
                            $tier[$k]['tierQty'] = (int) $value['quantity'];
                            $tier[$k]['percentage'] = round(100 - $value['price'] / $surplusPrice * 100);
                            $tier[$k]['tierPrice'] = number_format($value['price'], 2);
                        }
                    }
                    $resultArr['quantities'][$key]['tierPrices'] = $tier;
                    $result = $resultArr['quantities'][$key]['tierPrices'];
                }
                return $resultArr['quantities'];
            } catch (Exception $e) {
                $result = array('isFault' => 1, 'faultMessage' => $e->getMessage());
                return $result;
            }
        } else {
            $res = array('isFault' => 2, 'error' => json_encode($result));
            return $res;
        }
    }

    /**
     *
     * date created 27-09-2017(dd-mm-yy)
     * date modified (dd-mm-yy)
     * Returning Veriant Details
     * Scope : this page 
     *
     * @param 
     * @return Array data
     *
     */
    public function variantDetails() {
        $result = $this->storeApiLogin();
        if ($this->storeApiLogin == true) {
            try {
                $confId = $this->_request['conf_pid'];
                $resultArr = $this->datalayer->getVariants((object) $this->_request);
                $pCount = $resultArr['count'];
                $resultArr = $resultArr['variants'];

                $result = array('variants' => $resultArr, 'count' => $pCount);
                return $result;
            } catch (Exception $e) {
                $result = array('isFault' => 1, 'faultMessage' => $e->getMessage());
                return $result;
            }
        } else {
            $res = array('isFault' => 2, 'error' => json_encode($result));
            return $res;
        }
    }

}
