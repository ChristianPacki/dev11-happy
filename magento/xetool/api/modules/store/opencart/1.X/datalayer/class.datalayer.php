<?php

require_once dirname(__FILE__) . '/../../../../../../../config.php';

class Datalayer {

    public $con = '';
    private $directory;

    public function __construct() {
        $this->con = mysqli_connect(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
        if (mysqli_connect_errno()) {
            $this->con = '';
        }
        mysqli_set_charset($this->con, "utf8");
        $this->util = new UTIL();
        $this->color = $this->util->getStoreAttributes("xe_color");
        $this->size = $this->util->getStoreAttributes("xe_size");
        $this->store = $this->util->getDefaultStoreId();
        //get session path for opencart 3
        $pos = strrpos(session_save_path(), ';');
        if ($pos === false) {
            $this->directory = session_save_path();
        } else {
            $this->directory = substr(session_save_path(), $pos + 1);
        }
    }

    public function userAuthenticate($username = '', $key = '') {
        $key = md5($key);
        $sql = "SELECT * FROM `onj_api_user` WHERE username='" . API_USERNAME . "' AND `key` = '" . API_KEY . "'";
        /*
          $query = mysqli_query($this->con,$sql);
          if(mysqli_num_rows($query)){
          return true;
          } */
        //return false;
        return true;
    }

    public function getCategories() {
        $store = $this->store;
        $categories = array();
        $sql = "SELECT c.category_id,cd.name FROM " . DB_PREFIX . "category c INNER JOIN " . DB_PREFIX . "category_description cd ON (cd.category_id = c.category_id) INNER JOIN `" . DB_PREFIX . "category_to_store` cts ON (c.category_id = cts.category_id) WHERE c.parent_id=0 AND cts.store_id = ? ORDER BY cd.name ASC";
        $params = array();
        $params[] = 's';
        $params[] = &$store;
        $stmt = $this->con->prepare($sql);
        call_user_func_array([$stmt, 'bind_param'], $params);
        $stmt->execute();
        $query = $stmt->get_result();
        $i = 0;
        while ($row = mysqli_fetch_array($query, MYSQL_ASSOC)) {
            $categories[$i]['id'] = $row['category_id'];
            $categories[$i]['name'] = $row['name'];
            $i++;
        }
        return $categories;
    }

    public function getCategoryId($category) {
        $this->log('category:' . $category);
        $sql = "SELECT c.category_id FROM " . DB_PREFIX . "category c INNER JOIN " . DB_PREFIX . "category_description cd ON (cd.category_id = c.category_id) WHERE cd.name=?";
        $params = array();
        $params[] = 's';
        $params[] = &$category;
        $stmt = $this->con->prepare($sql);
        call_user_func_array([$stmt, 'bind_param'], $params);
        $stmt->execute();
        $query = $stmt->get_result();
        $row = mysqli_fetch_array($query, MYSQL_ASSOC);

        return $row['category_id'];
    }

    public function getCategoryList($categoryid) {
        $categories[] = $categoryid;
        $sql = "SELECT c.category_id FROM " . DB_PREFIX . "category c WHERE c.parent_id=?";
        $params = array();
        $params[] = 'i';
        $params[] = &$categoryid;
        $stmt = $this->con->prepare($sql);
        call_user_func_array([$stmt, 'bind_param'], $params);
        $stmt->execute();
        $query = $stmt->get_result();
        while ($row = mysqli_fetch_array($query, MYSQL_ASSOC)) {
            $categories[] = $row['category_id'];
        }
        return $categories;
    }

    public function getProductCategoryList($productid) {
        $sql = "SELECT c.category_id FROM " . DB_PREFIX . "product_to_category c WHERE c.product_id=?";
        $params = array();
        $params[] = 'i';
        $params[] = &$productid;
        $stmt = $this->con->prepare($sql);
        call_user_func_array([$stmt, 'bind_param'], $params);
        $stmt->execute();
        $query = $stmt->get_result();
        while ($row = mysqli_fetch_array($query, MYSQL_ASSOC)) {
            $categories[] = $row['category_id'];
        }
        return $categories;
    }

    public function getSubCategory($categoryid) {
        $subcategory = array();
        $sql = "SELECT c.category_id,cd.name FROM " . DB_PREFIX . "category c INNER JOIN " . DB_PREFIX . "category_description cd ON (cd.category_id = c.category_id) WHERE c.parent_id=? ORDER BY cd.name ASC";
        $params = array();
        $params[] = 'i';
        $params[] = &$categoryid;
        $stmt = $this->con->prepare($sql);
        call_user_func_array([$stmt, 'bind_param'], $params);
        $stmt->execute();
        $query = $stmt->get_result();
        $i = 0;
        while ($row = mysqli_fetch_array($query, MYSQL_ASSOC)) {
            $subcategory[$i]['id'] = $row['category_id'];
            $subcategory[$i]['name'] = $row['name'];
            $i++;
        }
        return $subcategory;
    }

    public function getParentCategory($categoryid) {
        $sql = "SELECT parent_id FROM " . DB_PREFIX . "category WHERE category_id=?";
        $params = array();
        $params[] = 'i';
        $params[] = &$categoryid;
        $stmt = $this->con->prepare($sql);
        call_user_func_array([$stmt, 'bind_param'], $params);
        $stmt->execute();
        $query = $stmt->get_result();
        $row = mysqli_fetch_assoc($query);
        return $row['parent_id'];
    }

    public function getProductCount($catid = '') {
        $categoryList = ($catid != '') ? $this->getCategoryList($catid) : array();
        $sql = "SELECT status FROM " . DB_PREFIX . "product WHERE status='1' and is_variant='0'";
        if (!empty($categoryList)) {
            $sql .= "  AND product_id IN(SELECT p2c.product_id FROM " . DB_PREFIX . "product_to_category p2c WHERE p2c.category_id IN (" . implode(',', $categoryList) . "))";
        }
        $result = mysqli_query($this->con, $sql);
        $products_count = mysqli_num_rows($result);
        return $products_count;
    }

    public function getAllProducts($data, $catid = '') {
        $products = array();
        $query = mysqli_query($this->con, "SELECT option_id FROM `" . DB_PREFIX . "option_description` WHERE name='xe_is_design'");
        $row = mysqli_fetch_array($query, MYSQL_ASSOC);
        $option_id = $row['option_id'];

        //$categoryList = ($catid!='')?$this->getCategoryList($catid):array();
        $sql = "SELECT p.product_id,p.image as thumbnail,pd.name, pd.description, p.price FROM " . DB_PREFIX . "product p INNER JOIN " . DB_PREFIX . "product_description pd ON(p.product_id = pd.product_id) WHERE p.product_id != '' and p.is_variant='0'";
        $sql = "SELECT p.product_id,p.image as thumbnail,pd.name, pd.description, p.price FROM " . DB_PREFIX . "product p ";
        $sql .= "INNER JOIN " . DB_PREFIX . "product_description pd ON(p.product_id = pd.product_id) ";
        $sql .= "INNER JOIN " . DB_PREFIX . "product_to_store pts ON (p.product_id = pts.product_id) WHERE p.product_id != '' AND p.is_variant='0' AND pts.store_id = ? ";
        $params = array();
        $params[0] = 's';
        $params[] = &$this->store;
        $sql .= "  AND p.product_id IN(SELECT po.product_id FROM " . DB_PREFIX . "product_option po WHERE po.option_id = ? and po.value=1)";
        $params[0] .= 's';
        $params[] = &$option_id;
        if (isset($data->preDecoStatus) && $data->preDecoStatus == 'true') {
            $sql .= " AND p.product_id IN (SELECT pv.variant_id FROM oc_product_variant pv)";
        }
        if (!isset($data->preDecorated) || $data->preDecorated == 'false') {
            $refid_query = mysqli_query($this->con, "SELECT option_id FROM `" . DB_PREFIX . "option_description` WHERE name='refid'");
            $refid_query_row = mysqli_fetch_array($refid_query, MYSQL_ASSOC);
            $refid_option_id = $refid_query_row['option_id'];
            $sql .= "  AND p.product_id NOT IN(SELECT po.product_id FROM " . DB_PREFIX . "product_option po WHERE po.option_id = ? and po.value!='')";
            $params[0] .= 's';
            $params[] = &$refid_option_id;
        }
        if ((isset($data->categoryid) && $data->categoryid != "") && (isset($data->subcategoryid) && $data->subcategoryid != "")) {
            $cat_array = array($data->categoryid, $data->subcategoryid);
            $sql .= " AND p.product_id IN(SELECT p2c.product_id FROM " . DB_PREFIX . "product_to_category p2c WHERE p2c.category_id IN (" . implode(',', $cat_array) . "))";
        }
        if (isset($data->categoryid) && $data->categoryid != "") {
            $cat_ids = $this->getCategoryList($data->categoryid);
            $sql .= " AND p.product_id IN(SELECT p2c.product_id FROM " . DB_PREFIX . "product_to_category p2c INNER JOIN `" . DB_PREFIX . "category_to_store` c2s ON p2c.category_id = c2s.category_id WHERE p2c.category_id IN (" . implode(',', $cat_ids) . "))";
        }
        if (isset($data->searchstring) && $data->searchstring != "") {
            $sql .= " AND pd.name like ? ";
            $datasearchstring = "%" . $data->searchstring . "%";
            $params[0] .= 's';
            $params[] = &$datasearchstring;
        }
        /* if(!empty($categoryList))
          {
          $sql .= " AND p.product_id IN(SELECT p2c.product_id FROM ".DB_PREFIX."product_to_category p2c WHERE p2c.category_id IN (".implode(',', $categoryList)."))";
          } */
        $sql .= " order by product_id DESC ";

        $limit = '';
        $start = (int) $data->range * ((int) $data->offset - 1);
        if (isset($data->offset) && isset($data->range)) {
            $limit = ' limit ? , ?';
            $params[0] .= 'ss';
            $params[] = &$start;
            $params[] = &$data->range;
        }
        $sql .= $limit;
        $stmt = $this->con->prepare($sql);
        call_user_func_array([$stmt, 'bind_param'], $params);
        $stmt->execute();
        $query = $stmt->get_result();
        $count = mysqli_num_rows($query);
        $i = 0;
        while ($row = mysqli_fetch_array($query, MYSQL_ASSOC)) {
            $products[$i]['id'] = $row['product_id'];
            $products[$i]['name'] = $row['name'];
            $products[$i]['price'] = $row['price'];
            $products[$i]['description'] = strip_tags(htmlspecialchars_decode($row['description']));
            $products[$i]['category'] = $this->getCategoriesByProduct($row['product_id']);
            $thumb = $this->resize($row['thumbnail'], 140, 140);
            $products[$i]['thumbnail'] = HTTP_SERVER . 'image/' . $thumb;
            $i++;
        }
        return array('product' => $products, 'count' => $count);
    }

    public function getVariants($data) {
        $sql = "SELECT * FROM " . DB_PREFIX . "product p WHERE p.product_id = ? and p.is_variant='0'";
        $params = array();
        $params[] = 'i';
        $params[] = &$data->conf_pid;
        $stmt = $this->con->prepare($sql);
        call_user_func_array([$stmt, 'bind_param'], $params);
        $stmt->execute();
        $query = $stmt->get_result();
        if (mysqli_num_rows($query) > 0) {
            $config_id = (int) $data->conf_pid;
        } else {
            $sql = "SELECT variant_id FROM " . DB_PREFIX . "product_variant WHERE product_id = ?";
            $params = array();
            $params[] = 'i';
            $params[] = &$data->conf_pid;
            $stmt = $this->con->prepare($sql);
            call_user_func_array([$stmt, 'bind_param'], $params);
            $stmt->execute();
            $query = $stmt->get_result();
            $row = mysqli_fetch_array($query, MYSQL_ASSOC);
            $config_id = $row['variant_id'];
        }

        $sql = "SELECT * FROM " . DB_PREFIX . "product_variant pv LEFT JOIN " . DB_PREFIX . "product p ON (p.product_id = pv.product_id) LEFT JOIN " . DB_PREFIX . "product_description pd ON (pd.product_id = p.product_id)WHERE pv.variant_id= ?";
        $params = array();
        $params[0] = 's';
        $params[] = &$config_id;
        $sql1 = "SELECT * FROM " . DB_PREFIX . "product_variant pv LEFT JOIN " . DB_PREFIX . "product p ON (p.product_id = pv.product_id) LEFT JOIN " . DB_PREFIX . "product_description pd ON (pd.product_id = p.product_id)WHERE pv.variant_id= ?";
        $params1 = array();
        $params1[] = 's';
        $params1[] = &$config_id;
        $limit = '';
        $count = 0;
        $sql1 = $sql;
        $limit = '';
        $start = (int) $data->range * ((int) $data->offset - 1);
        if (isset($data->offset) && isset($data->range)) {
            $limit = ' limit ? , ?';
            $params[0] .= 'is';
            $params[] = &$start;
            $params[] = &$data->range;
        }
        /*             if(isset($data->start) && $data->start!='undefined'){
          $limit = ' limit '.(int)$data->start;
          }
          if(isset($data->start) && $data->start!='undefined' && $data->start<$data->range)
          {
          $limit = ' limit 0';
          }
          if(isset($data->range) && $limit!=''){
          if($data->range == 'undefined')
          $range = 100;
          else
          $range = (int)$data->range;
          $limit .= ', '.$range;
          } */
        $sql .= $limit;
        $stmt = $this->con->prepare($sql);
        call_user_func_array([$stmt, 'bind_param'], $params);
        $stmt->execute();
        $query = $stmt->get_result();
        $stmt = $this->con->prepare($sql1);
        call_user_func_array([$stmt, 'bind_param'], $params1);
        $stmt->execute();
        $query1 = $stmt->get_result();
        $i = 0;
        if (mysqli_num_rows($query) > 0) {
            while ($row = mysqli_fetch_array($query, MYSQL_ASSOC)) {
                $color = $this->getProductOptionsModified($row['product_id'], 'color');
                $variants[$i]['id'] = $row['product_id'];
                $variants[$i]['name'] = $row['name'];
                $variants[$i]['xeColor'] = isset($color[$this->color]) ? $color[$this->color] : '';
                $variants[$i]['price'] = $row['price'];
                $variants[$i]['colorUrl'] = isset($color['option_value_id']) ? $color['option_value_id'] . ".png" : '';
                $thumb = $this->resize($row['image'], 140, 140);
                $variants[$i]['thumbnail'] = HTTP_SERVER . 'image/' . $thumb;
                $variants[$i]['xe_color_id'] = $color['option_value_id'];
                $i++;
            }
            $count = mysqli_num_rows($query1);
        } else if ($start == 0) {
            $sql = "SELECT * FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON (pd.product_id = p.product_id)WHERE p.product_id= ?";
            $params = array();
            $params[] = 'i';
            $params[] = &$data->conf_pid;
            $stmt = $this->con->prepare($sql);
            call_user_func_array([$stmt, 'bind_param'], $params);
            $stmt->execute();
            $query = $stmt->get_result();
            $row = mysqli_fetch_array($query, MYSQL_ASSOC);
            $sizes = array();
            $color = $this->getProductOptionsModified($row['product_id'], 'color');
            $variants[0]['id'] = $row['product_id'];
            $variants[0]['name'] = $row['name'];
            $variants[0]['xeColor'] = isset($color[$this->color]) ? $color[$this->color] : '';
            $variants[0]['price'] = $row['price'];
            $variants[0]['colorUrl'] = isset($color['option_value_id']) ? $color['option_value_id'] . ".png" : '';
            $thumb = $this->resize($row['image'], 140, 140);
            $variants[0]['thumbnail'] = HTTP_SERVER . 'image/' . $thumb;
            $variants[$i]['xe_color_id'] = $color['option_value_id'];
        } else {
            $variants = array();
        }
        return array('variants' => $variants, 'count' => $count);
    }

    public function getProductById($data) {
        $product_info = array();
        $sql = "SELECT * FROM " . DB_PREFIX . "product p WHERE p.product_id = ? and p.is_variant='0'";
        $params = array();
        $params[] = 'i';
        $params[] = &$data->id;
        $stmt = $this->con->prepare($sql);
        call_user_func_array([$stmt, 'bind_param'], $params);
        $stmt->execute();
        $query = $stmt->get_result();
        if (mysqli_num_rows($query) > 0) {
            $config_id = (int) $data->id;
        } else {
            $sql = "SELECT variant_id FROM " . DB_PREFIX . "product_variant WHERE product_id = ?";
            $params = array();
            $params[] = 'i';
            $params[] = &$data->id;
            $stmt = $this->con->prepare($sql);
            call_user_func_array([$stmt, 'bind_param'], $params);
            $stmt->execute();
            $query = $stmt->get_result();
            $row = mysqli_fetch_array($query, MYSQL_ASSOC);
            $config_id = $row['variant_id'];
        }
        $variant_id = ($config_id != (int) $data->id) ? (int) $data->id : '';
        $sql = "SELECT p.product_id,p.price,p.quantity,p.image,pd.name,pd.description FROM " . DB_PREFIX . "product p INNER JOIN " . DB_PREFIX . "product_description pd ON(p.product_id = pd.product_id) WHERE p.product_id=?";
        $params = array();
        $params[] = 'i';
        $params[] = &$config_id;
        $stmt = $this->con->prepare($sql);
        call_user_func_array([$stmt, 'bind_param'], $params);
        $stmt->execute();
        $query = $stmt->get_result();
        if ($row = mysqli_fetch_array($query, MYSQL_ASSOC)) {
            $variants = $this->getProductVariants($row['product_id'], $variant_id);
            $product_info['pid'] = $row['product_id'];
            $product_info['pidtype'] = (isset($variants['type'])) ? $variants['type'] : 'configurable';
            $product_info['pname'] = $row['name'];
            $product_info['shortdescription'] = strip_tags(html_entity_decode($row['description']));
            $product_info['category'] = $this->getCategoriesByProduct($row['product_id']);
            $product_info['minQuantity'] = $variants['minQuantity'];
            $product_info['pvid'] = $variants['id'];
            $product_info['pvname'] = $variants['name'];
            $product_info['xecolor'] = $variants['xe_color'];
            $product_info['xe_color_id'] = $variants['xe_color_id'];
            if ($product_info['pidtype'] == 'custom')
                $product_info['xesize'] = "";
            else
                $product_info['xesize'] = $variants['size'];
            $product_info['xe_size_id'] = $variants['xe_size_id'];
            $product_info['quanntity'] = $variants['quantity'];
            $product_info['price'] = $variants['price'];
            $product_info['taxrate'] = $variants['tax'];
            $product_info['attributes'] = $variants['attributes'];
            $product_info['thumbsides'] = $variants['thumbsides'];
            $product_info['sides'] = $variants['sides'];
            $product_info['isPreDecorated'] = ($variants['refid'] != '') ? true : false;
        }
        return $product_info;
    }

    public function getSizeAndQuantity($data) {
        if ((int) $data->productId != (int) $data->simplePdctId) {
            $sql = "SELECT * FROM " . DB_PREFIX . "product_variant pv LEFT JOIN " . DB_PREFIX . "product p ON (p.product_id = pv.product_id) LEFT JOIN " . DB_PREFIX . "product_description pd ON (pd.product_id = p.product_id)WHERE pv.variant_id= ? AND pv.product_id= ?";
            $params = array();
            $params[] = 'ii';
            $params[] = &$data->productId;
            $params[] = &$data->simplePdctId;
        } else {
            $sql = "SELECT * FROM " . DB_PREFIX . "product WHERE product_id=?";
            $params = array();
            $params[] = 'i';
            $params[] = &$data->productId;
        }
        $stmt = $this->con->prepare($sql);
        call_user_func_array([$stmt, 'bind_param'], $params);
        $stmt->execute();
        $query = $stmt->get_result();
        if (mysqli_num_rows($query) > 0) {
            while ($row = mysqli_fetch_array($query, MYSQL_ASSOC)) {
                $sizes = $this->getProductOptionsModified($row['product_id'], 'size');
                $color = $this->getProductOptionsModified($row['product_id'], 'color');
                $attributes = $this->getProductOptionsModified($row['product_id'], 'all');
                $attributes['xe_color'] = isset($color[$this->color]) ? $color[$this->color] : '';
                $attributes['xe_color_id'] = isset($color['option_value_id']) ? $color['option_value_id'] : '';
                if (!empty($sizes)) {
                    $j = 0;
                    foreach ($sizes as $value) {
                        $attributes['xe_size'] = $value['name'];
                        $attributes['xe_size_id'] = $value['option_value_id'];
                        $price = ($value['price_prefix'] == '+') ? $row['price'] + $value['price'] : $row['price'] - $value['price'];
                        $quantities[$j]['simpleProductId'] = $row['product_id'];
                        $quantities[$j]['xe_size_id'] = $value['option_value_id'];
                        $quantities[$j]['xe_color_id'] = isset($color['option_value_id']) ? $color['option_value_id'] : '';
                        $quantities[$j]['xe_size'] = $value['name'];
                        $quantities[$j]['xe_color'] = isset($color[$this->color]) ? $color[$this->color] : '';
                        $quantities[$j]['quantity'] = (int) $this->getProductQuantity($row['product_id'], $value['option_value_id']);
                        $quantities[$j]['price'] = $price;
                        $quantities[$j]['minQuantity'] = (int) $row['minimum'];
                        $quantities[$j]['attributes'] = $attributes;
                        $j++;
                    }
                } else {
                    $attributes['xe_size'] = '';
                    $attributes['xe_size_id'] = '';
                    $quantities['simpleProductId'] = $row['product_id'];
                    $quantities['xe_size_id'] = '';
                    $quantities['xe_color_id'] = isset($color['option_value_id']) ? $color['option_value_id'] : '';
                    $quantities['xe_size'] = '';
                    $quantities['xe_color'] = isset($color[$this->color]) ? $color[$this->color] : '';
                    $quantities['quantity'] = (int) $this->getProductQuantity($row['product_id'], $value['option_value_id']);
                    $quantities['price'] = $row['price'];
                    $quantities['minQuantity'] = (int) $row['minimum'];
                    $quantities['attributes'] = $attributes;
                }
            }
        } else {
            $sql = "SELECT * FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON (pd.product_id = p.product_id)WHERE p.product_id= ?";
            $params = array();
            $params[] = 'i';
            $params[] = &$product_id;
            $stmt = $this->con->prepare($sql);
            call_user_func_array([$stmt, 'bind_param'], $params);
            $stmt->execute();
            $query = $stmt->get_result();
            $row = mysqli_fetch_array($query, MYSQL_ASSOC);
            $sizes = $this->getProductOptionsModified($row['product_id'], 'size');
            $color = $this->getProductOptionsModified($row['product_id'], 'color');
            $attributes = $this->getProductOptionsModified($row['product_id'], 'all');
            $attributes['xe_color'] = isset($color[$this->color]) ? $color[$this->color] : '';
            $attributes['xe_color_id'] = isset($color['option_value_id']) ? $color['option_value_id'] : '';
            if (!empty($sizes)) {
                $j = 0;
                foreach ($sizes as $value) {
                    $attributes['xe_size'] = $value['name'];
                    $attributes['xe_size_id'] = $value['option_value_id'];
                    $price = ($value['price_prefix'] == '+') ? $row['price'] + $value['price'] : $row['price'] - $value['price'];
                    $quantities[$j]['simpleProductId'] = $row['product_id'];
                    $quantities[$j]['xe_size'] = $value['name'];
                    $quantities[$j]['xe_size_id'] = $value['option_value_id'];
                    $quantities[$j]['xe_color'] = isset($color[$this->color]) ? $color[$this->color] : '';
                    $quantities[$j]['xe_color_id'] = isset($color['option_value_id']) ? $color['option_value_id'] : '';
                    $quantities[$j]['quantity'] = $this->getProductQuantity($row['product_id'], $value['option_value_id']);
                    $quantities[$j]['price'] = $price;
                    $quantities[$j]['minQuantity'] = (int) $row['minimum'];
                    $quantities[$j]['attributes'] = $attributes;
                }
            } else {
                $attributes['xe_size'] = '';
                $attributes['xe_size_id'] = '';
                $quantities['simpleProductId'] = $row['product_id'];
                $quantities['xe_size'] = '';
                $quantities['xe_size_id'] = '';
                $quantities['xe_color'] = isset($color[$this->color]) ? $color[$this->color] : '';
                $quantities['xe_color_id'] = isset($color['option_value_id']) ? $color['option_value_id'] : '';
                $quantities['quantity'] = $this->getProductQuantity($row['product_id'], $value['option_value_id']);
                $quantities['price'] = $price;
                $quantities['minQuantity'] = (int) $row['minimum'];
                $quantities['attributes'] = $attributes;
            }
        }
        return array('quantities' => $quantities);
    }

    public function getSizeVariants($data) {
        if ((int) $data->productId != (int) $data->simplePdctId) {
            $sql = "SELECT * FROM " . DB_PREFIX . "product_variant pv LEFT JOIN " . DB_PREFIX . "product p ON (p.product_id = pv.product_id) LEFT JOIN " . DB_PREFIX . "product_description pd ON (pd.product_id = p.product_id)WHERE pv.variant_id= ?";
            $params = array();
            $params[] = 'i';
            $params[] = &$data->productId;
        } else {
            $sql = "SELECT * FROM " . DB_PREFIX . "product WHERE product_id=?";
            $params = array();
            $params[] = 'i';
            $params[] = &$data->productId;
        }
        $stmt = $this->con->prepare($sql);
        call_user_func_array([$stmt, 'bind_param'], $params);
        $stmt->execute();
        $query = $stmt->get_result();
        if (mysqli_num_rows($query) > 0) {
            $size_array = array();
            $j = 0;
            while ($row = mysqli_fetch_array($query, MYSQL_ASSOC)) {
                $sizes = $this->getProductOptionsModified($row['product_id'], 'size');
                $color = $this->getProductOptionsModified($row['product_id'], 'color');
                if (!empty($sizes)) {
                    foreach ($sizes as $value) {
                        if (empty($size_array) || !in_array($value['name'], $size_array)) {
                            $price = ($value['price_prefix'] == '+') ? $row['price'] + $value['price'] : $row['price'] - $value['price'];
                            $quantities[$j]['simpleProductId'] = $row['product_id'];
                            $quantities[$j]['xe_size_id'] = $value['option_value_id'];
                            $quantities[$j]['xe_color_id'] = isset($color['option_value_id']) ? $color['option_value_id'] : '';
                            $quantities[$j]['xe_size'] = $value['name'];
                            $quantities[$j]['xe_color'] = isset($color[$this->color]) ? $color[$this->color] : '';
                            $quantities[$j]['quantity'] = (int) $this->getProductQuantity($row['product_id'], $value['option_value_id']);
                            $quantities[$j]['price'] = $price;
                            $size_array[] = $value['name'];
                            $j++;
                        }
                    }
                } else {
                    $quantities['simpleProductId'] = $row['product_id'];
                    $quantities['xe_size_id'] = '';
                    $quantities['xe_color_id'] = '';
                    $quantities['xe_size'] = '';
                    $quantities['xe_color'] = isset($color[$this->color]) ? $color[$this->color] : '';
                    $quantities['quantity'] = (int) $this->getProductQuantity($row['product_id'], $value['option_value_id']);
                    $quantities['price'] = $row['price'];
                }
            }
        } else {
            $sql = "SELECT * FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON (pd.product_id = p.product_id)WHERE p.product_id= ?";
            $params = array();
            $params[] = 'i';
            $params[] = &$product_id;
            $stmt = $this->con->prepare($sql);
            call_user_func_array([$stmt, 'bind_param'], $params);
            $stmt->execute();
            $query = $stmt->get_result();
            $row = mysqli_fetch_array($query, MYSQL_ASSOC);
            $sizes = $this->getProductOptionsModified($row['product_id'], 'size');
            $color = $this->getProductOptionsModified($row['product_id'], 'color');
            if (!empty($sizes)) {
                $j = 0;
                foreach ($sizes as $value) {
                    $price = ($value['price_prefix'] == '+') ? $row['price'] + $value['price'] : $row['price'] - $value['price'];
                    $quantities[$j]['simpleProductId'] = $row['product_id'];
                    $quantities[$j]['xe_size'] = $value['name'];
                    $quantities[$j]['xe_color'] = isset($color[$this->color]) ? $color[$this->color] : '';
                    $quantities[$j]['quantity'] = $this->getProductQuantity($row['product_id'], $value['option_value_id']);
                    $quantities[$j]['price'] = $price;
                }
            } else {
                $quantities['simpleProductId'] = $row['product_id'];
                $quantities['xe_size'] = '';
                $quantities['xe_color'] = isset($color[$this->color]) ? $color[$this->color] : '';
                $quantities['quantity'] = $this->getProductQuantity($row['product_id'], $value['option_value_id']);
                $quantities['price'] = $price;
            }
        }
        return array('quantities' => $quantities);
    }

    public function getCategoriesByProduct($product_id) {
        $categories = array();
        $sql = "SELECT category_id FROM " . DB_PREFIX . "product_to_category WHERE product_id = ?";
        $params = array();
        $params[] = 'i';
        $params[] = &$product_id;
        $stmt = $this->con->prepare($sql);
        call_user_func_array([$stmt, 'bind_param'], $params);
        $stmt->execute();
        $query = $stmt->get_result();
        while ($row = mysqli_fetch_array($query, MYSQL_ASSOC)) {
            $categories[] = $row['category_id'];
        }
        return $categories;
    }

    public function getProductOptionsModified($product_id, $type, $extra = 0) {
        $sql = "SELECT * FROM " . DB_PREFIX . "product_option po LEFT JOIN `" . DB_PREFIX . "option` o ON (po.option_id = o.option_id) LEFT JOIN " . DB_PREFIX . "option_description od ON (o.option_id = od.option_id) WHERE po.product_id = ? ORDER BY o.sort_order";
        $params = array();
        $params[] = 'i';
        $params[] = &$product_id;
        $stmt = $this->con->prepare($sql);
        call_user_func_array([$stmt, 'bind_param'], $params);
        $stmt->execute();
        $query = $stmt->get_result();
        $color = array();
        $size = array();
        $refid = array();
        $attributeArray = array();
        while ($row = mysqli_fetch_array($query, MYSQL_ASSOC)) {
            if ($row['type'] == 'select' || $row['type'] == 'radio' || $row['type'] == 'checkbox' || $row['type'] == 'image') {
                $sql = "SELECT pov.option_value_id, pov.price, pov.price_prefix, ovd.name FROM " . DB_PREFIX . "product_option_value pov LEFT JOIN " . DB_PREFIX . "option_value ov ON (pov.option_value_id = ov.option_value_id) LEFT JOIN " . DB_PREFIX . "option_value_description ovd ON (ov.option_value_id = ovd.option_value_id) WHERE pov.product_id = ? AND pov.product_option_id = ? ORDER BY ov.sort_order";
                $params = array();
                $params[] = 'ii';
                $params[] = &$product_id;
                $params[] = &$row['product_option_id'];
                $stmt = $this->con->prepare($sql);
                call_user_func_array([$stmt, 'bind_param'], $params);
                $stmt->execute();
                $query2 = $stmt->get_result();
                $temp_color = array();
                $i = 0;
                while ($row2 = mysqli_fetch_array($query2, MYSQL_ASSOC)) {
                    if ($row['name'] == $this->color) {
                        $color[$row['name']] = $row2['name'];
                        $color['option_value_id'] = $row2['option_value_id'];
                    } elseif ($row['name'] == $this->size) {
                        $size[$i]['name'] = $row2['name'];
                        $size[$i]['price'] = $row2['price'];
                        $size[$i]['price_prefix'] = $row2['price_prefix'];
                        $size[$i]['option_value_id'] = $row2['option_value_id'];
                        $i++;
                    } else if ($extra == 0) {
                        $attributeArray[$row['name']] = $row2['name'];
                        $attributeArray[$row['name'] . '_id'] = $row2['option_value_id'];
                    }
                }
            } elseif ($row['type'] == 'text') {
                if (strtolower($row['name']) == $this->color) {
                    $color[$row['name']] = $row['value'];
                } elseif (strtolower($row['name']) == 'refid') {
                    $refid[$row['name']] = $row['value'];
                } elseif (strtolower($row['name'] != 'xe_is_design') && strtolower($row['name'] != 'disable_addtocart')) {
                    $attributeArray[$row['name']] = $row['value'];
                    $attributeArray[$row['name'] . '_id'] = $row['option_id'];
                }
            }
        }
        if ($type == 'color') {
            return $color;
        } elseif ($type == 'size') {
            return $size;
        } elseif ($type == 'refid') {
            return $refid;
        } elseif ($type == 'all') {
            return $attributeArray;
        }
    }

    public function getProductAttributes($product_id) {
        $product_attribute_group_data = array();

        $product_attribute_group_query = array();

        $sql = "SELECT ag.attribute_group_id, agd.name FROM " . DB_PREFIX . "product_attribute pa LEFT JOIN " . DB_PREFIX . "attribute a ON (pa.attribute_id = a.attribute_id) LEFT JOIN " . DB_PREFIX . "attribute_group ag ON (a.attribute_group_id = ag.attribute_group_id) LEFT JOIN " . DB_PREFIX . "attribute_group_description agd ON (ag.attribute_group_id = agd.attribute_group_id) WHERE pa.product_id = ?  GROUP BY ag.attribute_group_id ORDER BY ag.sort_order, agd.name";
        $params = array();
        $params[] = 'i';
        $params[] = &$product_id;
        $stmt = $this->con->prepare($sql);
        call_user_func_array([$stmt, 'bind_param'], $params);
        $stmt->execute();
        $query = $stmt->get_result();
        while ($row = mysqli_fetch_array($query, MYSQL_ASSOC)) {
            $product_attribute_group_query[] = $row;
        }

        foreach ($product_attribute_group_query as $product_attribute_group) {
            $product_attribute_data = array();
            $product_attribute_query = array();
            $sql = "SELECT a.attribute_id, ad.name, pa.text FROM " . DB_PREFIX . "product_attribute pa LEFT JOIN " . DB_PREFIX . "attribute a ON (pa.attribute_id = a.attribute_id) LEFT JOIN " . DB_PREFIX . "attribute_description ad ON (a.attribute_id = ad.attribute_id) WHERE pa.product_id = ? AND a.attribute_group_id = ?  ORDER BY a.sort_order, ad.name";
            $params = array();
            $params[] = 'ii';
            $params[] = &$product_id;
            $params[] = &$product_attribute_group['attribute_group_id'];
            $stmt = $this->con->prepare($sql);
            call_user_func_array([$stmt, 'bind_param'], $params);
            $stmt->execute();
            $query = $stmt->get_result();
            while ($row = mysqli_fetch_array($query, MYSQL_ASSOC)) {
                $product_attribute_query[] = $row;
            }

            foreach ($product_attribute_query as $product_attribute) {
                $product_attribute_group_data[$product_attribute_group['name']] = array(
                    $product_attribute['name'] => $product_attribute['text'],
                );
            }
        }
        return $product_attribute_group_data;
    }

    /* public function getProductOptions($product_id)
      {

      $product_option_data = array();

      $sql = "SELECT * FROM " . DB_PREFIX . "product_option po LEFT JOIN `" . DB_PREFIX . "option` o ON (po.option_id = o.option_id) LEFT JOIN " . DB_PREFIX . "option_description od ON (o.option_id = od.option_id) WHERE po.product_id = '" . (int) $product_id . "'  ORDER BY o.sort_order";

      $query = mysqli_query($this->con, $sql);

      while ($row = mysqli_fetch_array($query, MYSQL_ASSOC)) {

      if ($row['type'] == 'select' || $row['type'] == 'radio' || $row['type'] == 'checkbox' || $row['type'] == 'image') {

      $query2 = mysqli_query($this->con, "SELECT * FROM " . DB_PREFIX . "product_option_value pov LEFT JOIN " . DB_PREFIX . "option_value ov ON (pov.option_value_id = ov.option_value_id) LEFT JOIN " . DB_PREFIX . "option_value_description ovd ON (ov.option_value_id = ovd.option_value_id) WHERE pov.product_id = '" . (int) $product_id . "' AND pov.product_option_id = '" . (int) $row['product_option_id'] . "'  ORDER BY ov.sort_order");

      while ($row2 = mysqli_fetch_array($query2, MYSQL_ASSOC)) {
      $product_option_data[$row['name']][] = array($row2['name'] => $row2['option_value_id']);
      }
      }
      }
      return $product_option_data;
      } */

    public function add_to_cart($data) {
        require_once DIR_SYSTEM . 'library/session.php';
        $this->log(DIR_SYSTEM . 'library/session.php', true, 'logctest.log');
        $session = new Session();
        /* $product_data = (array)$data;
          foreach($product_data as $data)
          {
          $data = (object)$data; */
        if (isset($data->product_id)) {
            $product = array();
            $product['product_id'] = $data->product_id;
            if (isset($data->option)) {
                $product['option'] = $data->option;
            }
            $key = base64_encode(serialize($product));
            $_SESSION['cart'][$key] = $data->quantity;
        }
        //}
    }

    public function isProductExists($product_id) {
        $sql = "select product_id from " . DB_PREFIX . "product WHERE product_id =?";
        $params = array();
        $params[] = 'i';
        $params[] = &$product_id;
        $stmt = $this->con->prepare($sql);
        call_user_func_array([$stmt, 'bind_param'], $params);
        $stmt->execute();
        $query = $stmt->get_result();
        return mysqli_num_rows($result);
    }

    public function log($text, $append = true, $fileName = '') {
        $file = 'log_datalayer.log';
        if ($fileName) {
            $file = $fileName;
        }

        $file = dirname(__FILE__) . '/../' . $file;

        // Write the contents to the file,
        // using the FILE_APPEND flag to append the content to the end of the file
        // and the LOCK_EX flag to prevent anyone else writing to the file at the same time
        //file_put_contents($file, $text, FILE_APPEND | LOCK_EX);

        if ($append) {
            file_put_contents($file, $text . PHP_EOL, FILE_APPEND | LOCK_EX);
        } else {
            file_put_contents($file, $text);
        }
    }

    public function getProductRelatedOptions($product_id) {
        $options = array();
        $sql = "SELECT po.product_option_id, od.name FROM " . DB_PREFIX . "product_option po LEFT JOIN " . DB_PREFIX . "option_description od ON (po.option_id = od.option_id) WHERE po.product_id = ?";
        $params = array();
        $params[] = 'i';
        $params[] = &$product_id;
        $stmt = $this->con->prepare($sql);
        call_user_func_array([$stmt, 'bind_param'], $params);
        $stmt->execute();
        $query = $stmt->get_result();
        $i = 0;
        while ($row = mysqli_fetch_array($query, MYSQL_ASSOC)) {
            $options[$i]['id'] = $row['product_option_id'];
            $options[$i]['name'] = $row['name'];
            $i++;
        }
        return $options;
    }

    public function getProductVariants($product_id, $variant_id) {
        $variant = array();
        $sql = "SELECT * FROM " . DB_PREFIX . "product_variant pv LEFT JOIN " . DB_PREFIX . "product p ON (p.product_id = pv.product_id) INNER JOIN `" . DB_PREFIX . "product_to_store` pts ON p.product_id = pts.product_id LEFT JOIN " . DB_PREFIX . "product_description pd ON (pd.product_id = p.product_id) WHERE pv.variant_id= ? AND pts.store_id=?";
        $params = array();
        $params[0] = 'ii';
        $params[] = &$product_id;
        $params[] = &$this->store;
        if ($variant_id != '') {
            $sql .= " AND pv.product_id=?";
            $params[0] .= 'i';
            $params[] = &$variant_id;
        }
        $sql .= " LIMIT 1";
        $stmt = $this->con->prepare($sql);
        call_user_func_array([$stmt, 'bind_param'], $params);
        $stmt->execute();
        $query = $stmt->get_result();
        $res = mysqli_query($this->con, "SELECT * FROM " . DB_PREFIX . "setting s WHERE s.key='config_tax'");
        $exe = mysqli_fetch_array($res, MYSQL_ASSOC);
        if (mysqli_num_rows($query) > 0) {
            while ($row = mysqli_fetch_array($query, MYSQL_ASSOC)) {
                if ($row['tax_class_id'] != 0 && $exe['value'] == 1) {
                    $sql = "SELECT * FROM " . DB_PREFIX . "tax_rate trt LEFT JOIN " . DB_PREFIX . "tax_rule trl ON (trt.tax_rate_id = trl.tax_rate_id) WHERE trl.tax_rate_id=?";
                    $params = array();
                    $params[] = 's';
                    $params[] = &$row['tax_class_id'];
                    $stmt = $this->con->prepare($sql);
                    call_user_func_array([$stmt, 'bind_param'], $params);
                    $stmt->execute();
                    $que = $stmt->get_result();
                    $tax_row = mysqli_fetch_array($que, MYSQL_ASSOC);
                    $tax_price = 0;
                    foreach ($tax_row as $tax_rate) {
                        if ($tax_rate['type'] == 'F') {
                            //$tax_price += $tax_rate['rate'];
                            $tax_price = $tax_price;
                        } elseif ($tax_rate['type'] == 'P') {
                            $tax_price += $tax_rate['rate']; //($row['price'] / 100 * $tax_rate['rate']);
                        }
                    }
                } else {
                    $tax_price = 0;
                }
                $customOption = $this->getProductOptions($row['product_id']);
                foreach ($customOption as $key => $value) {
                    $attributes[$value['option_title']] = $value['option_values'][0]['title'];
                    $attributes[$value['option_title'] . "_id"] = $value['option_values'][0]['option_type_id'];
                }
                // $attributes = $this->getProductOptionsModified($row['product_id'], 'all');
                $color = $this->getProductOptionsModified($row['product_id'], 'color');
                // $attributes['xe_color'] = isset($color[$this->color]) ? $color[$this->color] : '';
                // $attributes['xe_color_id'] = isset($color['option_value_id']) ? $color['option_value_id'] : '';
                $variants['type'] = 'configurable';
                $variants['id'] = $row['product_id'];
                $variants['name'] = $row['name'];
                $variants['xe_color'] = isset($color[$this->color]) ? $color[$this->color] : '';
                $variants['xe_color_id'] = isset($color['option_value_id']) ? $color['option_value_id'] : '';
                $variants['quantity'] = $row['quantity'];
                $variants['price'] = $row['price'];
                $variants['tax'] = $tax_price;
                $variants['minQuantity'] = $row['minimum'];
                $refid = $this->getProductOptionsModified($row['product_id'], 'refid');
                $variants['refid'] = isset($refid['refid']) ? $refid['refid'] : '';
                $sizes = $this->getProductOptionsModified($row['product_id'], 'size');
                if (!empty($sizes)) {
                    $i = 0;
                    foreach ($sizes as $value) {
                        $variants['size'] = $value['name'];
                        $variants['xe_size_id'] = $value['option_value_id'];
                        // $attributes['xe_size'] = $value['name'];
                        // $attributes['xe_size_id'] = $value['option_value_id'];
                        $i++;
                        if ($i == 1) {
                            break;
                        }
                    }
                } else {
                    $variants['size'] = '';
                    $variants['xe_size_id'] = '';
                }
                $images = $this->getProductImages($row['product_id']);
                $thumb = $this->getProductThumbImages($row['product_id']);
                $variants['thumbsides'] = $thumb;
                $variants['sides'] = $images;
                $variants['attributes'] = $attributes;
            }
        } else {
            $sql = "SELECT * FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON (pd.product_id = p.product_id) INNER JOIN `" . DB_PREFIX . "product_to_store` pts ON p.product_id = pts.product_id WHERE p.product_id= ? AND pts.store_id=?";
            $params = array();
            $params[] = 'is';
            $params[] = &$product_id;
            $params[] = &$this->store;
            $stmt = $this->con->prepare($sql);
            call_user_func_array([$stmt, 'bind_param'], $params);
            $stmt->execute();
            $query = $stmt->get_result();
            $row = mysqli_fetch_array($query, MYSQL_ASSOC);
            if ($row['tax_class_id'] != 0 && $exe['value'] == 1) {
                $sql = "SELECT * FROM " . DB_PREFIX . "tax_rate trt LEFT JOIN " . DB_PREFIX . "tax_rule trl ON (trt.tax_rate_id = trl.tax_rate_id) WHERE trl.tax_rate_id=?";
                $params = array();
                $params[] = 's';
                $params[] = &$row['tax_class_id'];
                $stmt = $this->con->prepare($sql);
                call_user_func_array([$stmt, 'bind_param'], $params);
                $stmt->execute();
                $que = $stmt->get_result();
                $tax_row = mysqli_fetch_array($que, MYSQL_ASSOC);
                $tax_price = 0;
                foreach ($tax_row as $tax_rate) {
                    if ($tax_rate['type'] == 'F') {
                        $tax_price += $tax_rate['rate'];
                    } elseif ($tax_rate['type'] == 'P') {
                        $tax_price += ($row['price'] / 100 * $tax_rate['rate']);
                    }
                }
            } else {
                $tax_price = 0;
            }
            $customOption = $this->getProductOptions($row['product_id']);
            $extraAttr = (!empty($customOption)) ? 1 : 0;
            $color = $this->getProductOptionsModified($row['product_id'], 'color');
            $customOption = $this->getProductOptions($row['product_id']);
            $variants['type'] = (!empty($customOption)) ? 'custom' : 'simple';
            $variants['id'] = $row['product_id'];
            $variants['name'] = $row['name'];
            $variants['xe_color'] = isset($color[$this->color]) ? $color[$this->color] : '';
            $variants['xe_color_id'] = isset($color['option_value_id']) ? $color['option_value_id'] : '';
            $variants['quantity'] = $row['quantity'];
            $variants['price'] = $row['price'];
            $variants['tax'] = $tax_price;
            $refid = $this->getProductOptionsModified($row['product_id'], 'refid');
            $variants['refid'] = isset($refid['refid']) ? $refid['refid'] : '';
            $sizes = $this->getProductOptionsModified($row['product_id'], 'size');
            if (!empty($sizes)) {
                $i = 0;
                foreach ($sizes as $value) {
                    $variants['size'] = $value['name'];
                    $variants['xe_size_id'] = $value['option_value_id'];
                    $i++;
                    if ($i == 1) {
                        break;
                    }
                }
            } else {
                $variants['size'] = '';
                $variants['xe_size_id'] = '';
            }
            $images = $this->getProductImages($row['product_id']);
            $thumb = $this->getProductThumbImages($row['product_id']);
            $variants['thumbsides'] = $thumb;
            $variants['sides'] = $images;
            $variants['attributes'] = array();
        }
        return $variants;
    }

    public function getProductSize($product_id) {
        $size = array();
        $sql = "SELECT * FROM " . DB_PREFIX . "product_variant pv LEFT JOIN " . DB_PREFIX . "product p ON (p.product_id = pv.product_id) LEFT JOIN " . DB_PREFIX . "product_description pd ON (pd.product_id = p.product_id)WHERE pv.variant_id= ?";
        $params = array();
        $params[] = 'i';
        $params[] = &$product_id;
        $stmt = $this->con->prepare($sql);
        call_user_func_array([$stmt, 'bind_param'], $params);
        $stmt->execute();
        $query = $stmt->get_result();
        $i = 0;
        if (mysqli_num_rows($query) > 0) {
            while ($row = mysqli_fetch_array($query, MYSQL_ASSOC)) {
                $sizes = $this->getProductOptionsModified($row['product_id'], 'size');
                $size[] = $sizes;
            }
        } else {
            $sql = "SELECT * FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON (pd.product_id = p.product_id)WHERE p.product_id= ?";
            $params = array();
            $params[] = 'i';
            $params[] = &$product_id;
            $stmt = $this->con->prepare($sql);
            call_user_func_array([$stmt, 'bind_param'], $params);
            $stmt->execute();
            $query = $stmt->get_result();
            $row = mysqli_fetch_array($query, MYSQL_ASSOC);
            $sizes = $this->getProductOptionsModified($row['product_id'], 'size');
            $size[] = $sizes;
        }
        return $size;
    }

    public function getProductColors($product_id) {
        $colors = array();
        $sql = "SELECT * FROM " . DB_PREFIX . "product_variant pv LEFT JOIN " . DB_PREFIX . "product p ON (p.product_id = pv.product_id) LEFT JOIN " . DB_PREFIX . "product_description pd ON (pd.product_id = p.product_id)WHERE pv.variant_id= ?";
        $params = array();
        $params[] = 'i';
        $params[] = &$product_id;
        $stmt = $this->con->prepare($sql);
        call_user_func_array([$stmt, 'bind_param'], $params);
        $stmt->execute();
        $query = $stmt->get_result();
        $i = 0;
        if (mysqli_num_rows($query) > 0) {
            while ($row = mysqli_fetch_array($query, MYSQL_ASSOC)) {
                $color = $this->getProductOptionsModified($row['product_id'], 'color');
                $colors[] = $color;
            }
        } else {
            $sql = "SELECT * FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON (pd.product_id = p.product_id)WHERE p.product_id= ?";
            $params = array();
            $params[] = 'i';
            $params[] = &$product_id;
            $stmt = $this->con->prepare($sql);
            call_user_func_array([$stmt, 'bind_param'], $params);
            $stmt->execute();
            $query = $stmt->get_result();
            $row = mysqli_fetch_array($query, MYSQL_ASSOC);
            $sizes = array();
            $color = $this->getProductOptionsModified($row['product_id'], 'color');
            $colors[] = $color;
        }
        return $colors;
    }

    public function getProductImages($product_id) {
        $images = array();
        $sql = "SELECT * FROM " . DB_PREFIX . "product_image WHERE product_id=? ORDER BY sort_order ASC";
        $params = array();
        $params[] = 'i';
        $params[] = &$product_id;
        $stmt = $this->con->prepare($sql);
        call_user_func_array([$stmt, 'bind_param'], $params);
        $stmt->execute();
        $additional_images = $stmt->get_result();
        //$z =2;
        $z = 1;
        while ($row = mysqli_fetch_array($additional_images)) {
            $images[] = XEPATH . 'image/' . $row['image'];
            $z++;
        }
        return $images;
    }

    public function getProductQuantity($product_id, $option_value_id) {
        $sql = "SELECT * FROM " . DB_PREFIX . "product_option_value WHERE product_id=? AND option_value_id=?";
        $params = array();
        $params[] = 'is';
        $params[] = &$product_id;
        $params[] = &$option_value_id;
        $stmt = $this->con->prepare($sql);
        call_user_func_array([$stmt, 'bind_param'], $params);
        $stmt->execute();
        $query = $stmt->get_result();
        $row = mysqli_fetch_array($query, MYSQL_ASSOC);
        return $row['quantity'];
    }

    /**
     *
     * date created (dd-mm-yy)
     * date modified 24-05-2017(dd-mm-yy)
     * Get value of product option
     *
     * @param (string)option name
     * @param (int)option id
     * @return array data
     *
     */
    public function getProductOptionValue($option, $id) {
        $sql = "SELECT p.product_option_value_id FROM " . DB_PREFIX . "option_value_description o, " . DB_PREFIX . "product_option_value p  WHERE p.option_value_id = o.option_value_id AND o.name=? AND p.product_option_id=?";
        $params = array();
        $params[] = 'ss';
        $params[] = &$option;
        $params[] = &$id;
        $stmt = $this->con->prepare($sql);
        call_user_func_array([$stmt, 'bind_param'], $params);
        $stmt->execute();
        $query = $stmt->get_result();
        $sql = "SELECT value FROM " . DB_PREFIX . "product_option WHERE product_option_id=?";
        $params = array();
        $params[] = 's';
        $params[] = &$id;
        $stmt = $this->con->prepare($sql);
        call_user_func_array([$stmt, 'bind_param'], $params);
        $stmt->execute();
        $queryForTextOption = $stmt->get_result();
        if (mysqli_num_rows($query) > 0) {
            $row = mysqli_fetch_array($query, MYSQL_ASSOC);
            $value_id = $row['product_option_value_id'];
        } else {
            $row = mysqli_fetch_array($queryForTextOption, MYSQL_ASSOC);
            $value_id = $row['value'];
        }

        return $value_id;
    }

    public function getProductThumbImages($product_id) {
        $images = array();
        $sql = "SELECT * FROM " . DB_PREFIX . "product_image WHERE product_id=? ORDER BY sort_order ASC";
        $params = array();
        $params[] = 's';
        $params[] = &$product_id;
        $stmt = $this->con->prepare($sql);
        call_user_func_array([$stmt, 'bind_param'], $params);
        $stmt->execute();
        $additional_images = $stmt->get_result();
//$z =2;
        $z = 1;
        while ($row = mysqli_fetch_array($additional_images)) {
            $images[] = XEPATH . 'image/' . $this->resize($row['image'], 140, 140);
            $z++;
        }
        return $images;
    }

    public function getOptions($data = array()) {
        $options = array();
        $i = 0;
        if ($data['filter_name'] == $this->color && isset($data['oldConfId']) && $data['oldConfId'] != 0) {
            $productvariant = $this->getProductInfo($data['oldConfId']);
            $query = mysqli_query($this->con, "SELECT language_id FROM `" . DB_PREFIX . "language` WHERE status=1");
            $row = mysqli_fetch_array($query, MYSQL_ASSOC);
            foreach ($productvariant as $variant) {
                $sql = "SELECT name FROM `" . DB_PREFIX . "option_value_description` WHERE language_id=? and option_value_id=?";
                $params = array();
                $params[] = 'ss';
                $params[] = &$row['language_id'];
                $params[] = &$variant['color_id'];
                $stmt = $this->con->prepare($sql);
                call_user_func_array([$stmt, 'bind_param'], $params);
                $stmt->execute();
                $query = $stmt->get_result();
                $color = mysqli_fetch_array($query, MYSQL_ASSOC);

                $options[$i]['value'] = $variant['color_id'];
                $options[$i]['label'] = $color['name'];
                $options[$i]['swatchImage'] = '';
                $i++;
            }
        } else {
            $query = mysqli_query($this->con, "SELECT language_id FROM `" . DB_PREFIX . "language` WHERE status=1");
            $row = mysqli_fetch_array($query, MYSQL_ASSOC);

            $sql = "SELECT o.option_id FROM `" . DB_PREFIX . "option` o LEFT JOIN " . DB_PREFIX . "option_description od ON (o.option_id = od.option_id) WHERE od.language_id = ?";
            $params = array();
            $params[0] = 's';
            $params[] = &$row['language_id'];
//$sql = "SELECT o.option_id FROM `" . DB_PREFIX . "option` o LEFT JOIN " . DB_PREFIX . "option_description od ON (o.option_id = od.option_id) WHERE od.language_id = 1"

            if (!empty($data['filter_name'])) {
                $sql .= " AND od.name = ?";
                $params[0] .= 's';
                $params[] = &$data['filter_name'];
            }
            $limit = '';
            if (!empty($data['loadCount']) && $data['loadCount'] != 0) {
                $limit = "LIMIT " . $data['lastLoaded'] . "," . $data['loadCount'];
            }
            $stmt = $this->con->prepare($sql);
            call_user_func_array([$stmt, 'bind_param'], $params);
            $stmt->execute();
            $query = $stmt->get_result();
            $row = mysqli_fetch_assoc($query);
            $option_id = $row['option_id'];

            $sql1 = mysqli_query($this->con, "SELECT * FROM " . DB_PREFIX . "option_value ov LEFT JOIN  " . DB_PREFIX . "option_value_description ovd ON (ov.option_value_id = ovd.option_value_id) WHERE ov.option_id='" . $option_id . "' $limit");
            while ($result = mysqli_fetch_array($sql1)) {
                $options[$i]['value'] = $result['option_value_id'];
                $options[$i]['label'] = $result['name'];
                if ($data['filter_name'] == $this->color) {
                    $options[$i]['swatchImage'] = '';
                }
                $i++;
            }
        }
        return json_encode($options);
    }

    public function addProduct($data) {
        $config_id = 0;
        $productData = json_decode($data);
        $productData = json_decode($productData->data);
        $productData = (array) $productData->productData;
        $config_id = (isset($productData['conf_id']) && $productData['conf_id'] != 0) ? $productData['conf_id'] : $this->addConfigProduct($data, $_FILES);
        $data = $productData;
        $productVariant = array();
        if ($config_id != 0) {
            $pv = $this->getProductInfo($config_id);
            foreach ($data['variants'] as $variants) {
                $variants = (array) $variants;
                $simpleProduct = (array) $variants['simpleProducts'];
                $qty = $simpleProduct[0]->qty;
                $product_price = $simpleProduct[0]->price;
                if (empty($pv) || !in_array($variants['color_id'], $pv)) {
                    $sql = "INSERT INTO " . DB_PREFIX . "product SET model = '',sku = ?, upc = '', ean = '', jan = '', isbn = '', mpn = '', location = '', quantity = ?, minimum = '1', subtract = '1', stock_status_id = '7', date_available = NOW(), manufacturer_id = '', shipping = '', price = ?, points = '', weight = ?, weight_class_id = '1', length = '', width = '', height = '', length_class_id = '1', status = '1', tax_class_id = '0', sort_order = '1', date_added = NOW(), is_variant= '1'";
                    $params = array();
                    $params[] = 'ssss';
                    $params[] = &$data['sku'];
                    $params[] = &$qty;
                    $params[] = &$product_price;
                    $params[] = &$data['weight'];
                    $stmt = $this->con->prepare($sql);
                    call_user_func_array([$stmt, 'bind_param'], $params);
                    $stmt->execute();
                    $stmt->get_result();
                    $product_id = mysqli_insert_id($this->con);
                    $sql = "INSERT INTO `" . DB_PREFIX . "product_variant` SET `product_id` = ?, variant_id = ?";
                    $params = array();
                    $params[] = 'ii';
                    $params[] = &$product_id;
                    $params[] = &$config_id;
                    $stmt = $this->con->prepare($sql);
                    call_user_func_array([$stmt, 'bind_param'], $params);
                    $stmt->execute();
                    $query = $stmt->get_result();
                    if (isset($_FILES['configFile']['tmp_name'])) {
                        $filename = str_replace(" ", "_", $_FILES['configFile']['name']);
                        $file = "../../../image/catalog/" . $filename;
                        if (file_exists($file)) {
                            $status = 1;
                        } else {
                            $status = move_uploaded_file($_FILES['configFile']['tmp_name'], $file);
                        }

                        if ($status)
                            ;
                        {
                            $sql = "UPDATE " . DB_PREFIX . "product SET image = ? WHERE product_id = ?";
                            $catalog_filename = "catalog/" . $filename;
                            $params = array();
                            $params[] = 'si';
                            $params[] = &$catalog_filename;
                            $params[] = &$product_id;
                            $stmt = $this->con->prepare($sql);
                            call_user_func_array([$stmt, 'bind_param'], $params);
                            $stmt->execute();
                            $stmt->get_result();
                        }
                    }
                    $query = mysqli_query($this->con, "SELECT language_id FROM `" . DB_PREFIX . "language` WHERE status=1");
                    $row = mysqli_fetch_array($query, MYSQL_ASSOC);

                    $name = $simpleProduct[0]->product_name;
                    $description = $simpleProduct[0]->description;

                    $sql = "INSERT INTO " . DB_PREFIX . "product_description SET product_id = ?, language_id = ?, name = ?, description = ?, tag = '', meta_title = '', meta_description = '', meta_keyword = ''";
                    $params = array();
                    $params[] = 'iiss';
                    $params[] = &$product_id;
                    $params[] = &$row['language_id'];
                    $params[] = &$name;
                    $params[] = &$description;
                    $stmt = $this->con->prepare($sql);
                    call_user_func_array([$stmt, 'bind_param'], $params);
                    $stmt->execute();
                    $stmt->get_result();
//Insert Store Product
                    $sql = "INSERT INTO " . DB_PREFIX . "product_to_store SET product_id = ?, store_id = 0";
                    $params = array();
                    $params[] = 'i';
                    $params[] = &$product_id;
                    $stmt = $this->con->prepare($sql);
                    call_user_func_array([$stmt, 'bind_param'], $params);
                    $stmt->execute();
                    $stmt->get_result();
                    // Insert Size Options
                    if (isset($simpleProduct)) {
                        $sql = "SELECT option_id FROM `" . DB_PREFIX . "option_description` WHERE name=?";
                        $params = array();
                        $params[] = 's';
                        $params[] = &$this->size;
                        $stmt = $this->con->prepare($sql);
                        call_user_func_array([$stmt, 'bind_param'], $params);
                        $stmt->execute();
                        $query = $stmt->get_result();
                        $row = mysqli_fetch_array($query, MYSQL_ASSOC);
                        $option_id = $row['option_id'];
                        $sql = "INSERT INTO " . DB_PREFIX . "product_option SET product_id = ?, option_id = ?, required = '1'";
                        $params = array();
                        $params[] = 'ii';
                        $params[] = &$product_id;
                        $params[] = &$option_id;
                        $stmt = $this->con->prepare($sql);
                        call_user_func_array([$stmt, 'bind_param'], $params);
                        $stmt->execute();
                        $stmt->get_result();
                        $product_option_id = mysqli_insert_id($this->con);
                        $product_qty = 0;
                        foreach ($simpleProduct as $product_option) {
                            $product_option = (array) $product_option;

                            if (isset($product_option['sizeId'])) {
                                if ($product_option_id != '') {
                                    $price_prefix = ($product_option['price'] > $data['price']) ? '+' : '-';
                                    $weight_prefix = ($product_option['weight'] > $data['weight']) ? '+' : '-';
                                    if ($product_option['price'] == $data['price']) {
                                        $price = 0;
                                    } else if ($product_option['price'] > $data['price']) {
                                        $price = (int) $product_option['price'] - (int) $data['price'];
                                    } else {
                                        $price = (int) $data['price'] - (int) $product_option['price'];
                                    }

                                    if ($product_option['weight'] == $data['weight']) {
                                        $weight = 0;
                                    } else if ($product_option['weight'] > $data['weight']) {
                                        $weight = (int) $product_option['weight'] - (int) $data['weight'];
                                    } else {
                                        $weight = (int) $data['weight'] - (int) $product_option['weight'];
                                    }

                                    $sql = "INSERT INTO " . DB_PREFIX . "product_option_value SET product_option_id = ?, product_id = ?, option_id = ?, option_value_id = ?, quantity = ?, subtract = '1', price = ?, price_prefix = ?, points = '', points_prefix = '+', weight = ?, weight_prefix = ?";
                                    $params = array();
                                    $params[] = 'sssssssss';
                                    $params[] = &$product_option_id;
                                    $params[] = &$product_id;
                                    $params[] = &$option_id;
                                    $params[] = &$product_option['sizeId'];
                                    $params[] = &$product_option['qty'];
                                    $params[] = &$price;
                                    $params[] = &$price_prefix;
                                    $params[] = &$weight;
                                    $params[] = &$weight_prefix;
                                    $stmt = $this->con->prepare($sql);
                                    call_user_func_array([$stmt, 'bind_param'], $params);
                                    $stmt->execute();
                                    $query = $stmt->get_result();
                                    $product_qty += (int) $product_option['qty'];
                                }
                            }
                        }
                    }
                    // Insert Color Option
                    if (isset($variants['color_id'])) {
                        $sql = "SELECT option_id FROM `" . DB_PREFIX . "option_description` WHERE name=?";
                        $params = array();
                        $params[] = 's';
                        $params[] = &$this->color;
                        $stmt = $this->con->prepare($sql);
                        call_user_func_array([$stmt, 'bind_param'], $params);
                        $stmt->execute();
                        $query = $stmt->get_result();
                        $row = mysqli_fetch_array($query, MYSQL_ASSOC);

                        $sql = "INSERT INTO " . DB_PREFIX . "product_option SET product_id = ?, option_id = ?, required = '1'";
                        $params = array();
                        $params[] = 'ii';
                        $params[] = &$product_id;
                        $params[] = &$row['option_id'];
                        $stmt = $this->con->prepare($sql);
                        call_user_func_array([$stmt, 'bind_param'], $params);
                        $stmt->execute();
                        $stmt->get_result();
                        $product_option_id = mysqli_insert_id($this->con);
                        $value = 0;
                        $product_qty = ($data['qty'] != '' && $data['qty'] != 0) ? $data['qty'] : $product_qty;
                        if ($product_option_id != '') {
                            $sql = "INSERT INTO " . DB_PREFIX . "product_option_value SET product_option_id = ?, product_id = ?, option_id = ?, option_value_id = ?, quantity = ?, subtract = '1', price = ?, price_prefix = '+', points = '', points_prefix = '+', weight = ?, weight_prefix = '+'";
                            $params = array();
                            $params[] = 'iiiiiss';
                            $params[] = &$product_option_id;
                            $params[] = &$product_id;
                            $params[] = &$row['option_id'];
                            $params[] = &$variants['color_id'];
                            $params[] = &$product_qty;
                            $params[] = &$value;
                            $params[] = &$value;
                            $stmt = $this->con->prepare($sql);
                            call_user_func_array([$stmt, 'bind_param'], $params);
                            $stmt->execute();
                            $stmt->get_result();
                        }
                    }
                    // Insert Refid Option
                    $query = mysqli_query($this->con, "SELECT option_id FROM `" . DB_PREFIX . "option_description` WHERE name='refid'");
                    $row = mysqli_fetch_array($query, MYSQL_ASSOC);

                    $sql = "INSERT INTO " . DB_PREFIX . "product_option SET product_id = ?, option_id = ?, value = '', required = '0'";
                    $params = array();
                    $params[] = 'ii';
                    $params[] = &$product_id;
                    $params[] = &$row['option_id'];
                    $stmt = $this->con->prepare($sql);
                    call_user_func_array([$stmt, 'bind_param'], $params);
                    $stmt->execute();
                    $stmt->get_result();
                    // Insert xe_is_design Option
                    $query = mysqli_query($this->con, "SELECT option_id FROM `" . DB_PREFIX . "option_description` WHERE name='xe_is_design'");
                    $row = mysqli_fetch_array($query, MYSQL_ASSOC);

                    $sql = "INSERT INTO " . DB_PREFIX . "product_option SET product_id = ?, option_id = ?, value = '1', required = '0'";
                    $params = array();
                    $params[] = 'ii';
                    $params[] = &$product_id;
                    $params[] = &$row['option_id'];
                    $stmt = $this->con->prepare($sql);
                    call_user_func_array([$stmt, 'bind_param'], $params);
                    $stmt->execute();
                    $stmt->get_result();
                    if (isset($data['cat_id'])) {
                        foreach ($data['cat_id'] as $category_id) {
                            $sql = "INSERT INTO " . DB_PREFIX . "product_to_category SET product_id = ?, category_id = ?";
                            $params = array();
                            $params[] = 'ii';
                            $params[] = &$product_id;
                            $params[] = &$category_id;
                            $stmt = $this->con->prepare($sql);
                            call_user_func_array([$stmt, 'bind_param'], $params);
                            $stmt->execute();
                            $stmt->get_result();
                        }
                    }
                    $varintFile = '';
                    //Insert Product Images
                    if (isset($_FILES['simpleFile'])) {
                        $count = 0;
                        foreach ($_FILES['simpleFile']['tmp_name'] as $key => $product_image) {
                            $filename = str_replace(" ", "_", $_FILES['simpleFile']['name'][$key]);
                            $file = "../../../image/catalog/" . $filename;

                            if (file_exists($file)) {
                                $status = 1;
                            } else {
                                $status = move_uploaded_file($product_image, $file);
                            }

                            if ($status) {
                                $sql = "INSERT INTO " . DB_PREFIX . "product_image SET product_id = ?, image = ?, sort_order = ?";
                                $catalog_filename = "catalog/" . $filename;
                                $params = array();
                                $params[] = 'iss';
                                $params[] = &$product_id;
                                $params[] = &$catalog_filename;
                                $params[] = &$count;
                                $stmt = $this->con->prepare($sql);
                                call_user_func_array([$stmt, 'bind_param'], $params);
                                $stmt->execute();
                                $stmt->get_result();
                                if ($count == 0) {
                                    $varintFile = $filename;
                                }
                            }
                            $count++;
                        }
                    }
                    if (!isset($_FILES['configFile']['tmp_name']) && $varintFile != '') {
                        $sql = "UPDATE " . DB_PREFIX . "product SET image = ? WHERE product_id = ?";
                        $catalog_varintFile = "catalog/" . $varintFile;
                        $params = array();
                        $params[] = 'ss';
                        $params[] = &$catalog_varintFile;
                        $params[] = &$product_id;
                        $stmt = $this->con->prepare($sql);
                        call_user_func_array([$stmt, 'bind_param'], $params);
                        $stmt->execute();
                        $stmt->get_result();
                    }
                } else {
                    $product_id = $this->getProductId($config_id, $variants['color_id']);
                    // Insert Size Options
                    if (isset($simpleProduct)) {
                        $sql = "SELECT option_id FROM `" . DB_PREFIX . "option_description` WHERE name=?";
                        $params = array();
                        $params[] = 's';
                        $params[] = &$this->size;
                        $stmt = $this->con->prepare($sql);
                        call_user_func_array([$stmt, 'bind_param'], $params);
                        $stmt->execute();
                        $query = $stmt->get_result();
                        $row = mysqli_fetch_array($query, MYSQL_ASSOC);
                        $sql = "SELECT product_option_id FROM `" . DB_PREFIX . "product_option` WHERE option_id = ? AND product_id = ?";
                        $params = array();
                        $params[] = 'ii';
                        $params[] = &$row['option_id'];
                        $params[] = &$product_id;
                        $stmt = $this->con->prepare($sql);
                        call_user_func_array([$stmt, 'bind_param'], $params);
                        $stmt->execute();
                        $query1 = $stmt->get_result();
                        if (mysqli_num_rows($query1) > 0) {
                            $row1 = mysqli_fetch_array($query, MYSQL_ASSOC);
                            $product_option_id = $row1['product_option_id'];
                        } else {
                            $sql = "INSERT INTO " . DB_PREFIX . "product_option SET product_id = ?, option_id = ?, required = '1'";
                            $params = array();
                            $params[] = 'ii';
                            $params[] = &$product_id;
                            $params[] = &$row['option_id'];
                            $stmt = $this->con->prepare($sql);
                            call_user_func_array([$stmt, 'bind_param'], $params);
                            $stmt->execute();
                            $stmt->get_result();
                            $product_option_id = mysqli_insert_id($this->con);
                        }
                        foreach ($simpleProduct as $product_option) {
                            $product_option = (array) $product_option;
                            if (isset($product_option['sizeId'])) {
                                if ($product_option_id != '') {
                                    $price_prefix = ($product_option['price'] > $data['price']) ? '+' : '-';
                                    $weight_prefix = ($product_option['weight'] > $data['weight']) ? '+' : '-';
                                    if ($product_option['price'] == $data['price']) {
                                        $price = 0;
                                    } else if ($product_option['price'] > $data['price']) {
                                        $price = (int) $product_option['price'] - (int) $data['price'];
                                    } else {
                                        $price = (int) $data['price'] - (int) $product_option['price'];
                                    }

                                    if ($product_option['weight'] == $data['weight']) {
                                        $weight = 0;
                                    } else if ($product_option['weight'] > $data['weight']) {
                                        $weight = (int) $product_option['weight'] - (int) $data['weight'];
                                    } else {
                                        $weight = (int) $data['weight'] - (int) $product_option['weight'];
                                    }

                                    $sql = "INSERT INTO " . DB_PREFIX . "product_option_value SET product_option_id = ?, product_id = ?, option_id = ?, option_value_id = ?, quantity = ?, subtract = '1', price = ?, price_prefix = ?, points = '', points_prefix = '+', weight = ?, weight_prefix = ?";
                                    $params = array();
                                    $params[] = 'iiiiissss';
                                    $params[] = &$product_option_id;
                                    $params[] = &$product_id;
                                    $params[] = &$row['option_id'];
                                    $params[] = &$product_option['sizeId'];
                                    $params[] = &$product_option['qty'];
                                    $params[] = &$price;
                                    $params[] = &$price_prefix;
                                    $params[] = &$weight;
                                    $params[] = &$weight_prefix;
                                    $stmt = $this->con->prepare($sql);
                                    call_user_func_array([$stmt, 'bind_param'], $params);
                                    $stmt->execute();
                                    $stmt->get_result();
                                }
                            }
                        }
                    }
                }
            }
            $pvariant = $this->getProductInfo($config_id);
            return array("status" => "success", "conf_id" => $config_id, "variants" => $pvariant);
        } else {
            return json_encode(array("status" => "failed"));
        }
    }

    public function addConfigProduct($data) {
        $data = json_decode($data);
        $data = json_decode($data->data);
        $data = (array) $data->productData;
        $sql = "INSERT INTO " . DB_PREFIX . "product SET model = '',sku = ?, upc = '', ean = '', jan = '', isbn = '', mpn = '', location = '', quantity = ?, minimum = '1', subtract = '1', stock_status_id = '7', date_available = NOW(), manufacturer_id = '', shipping = '', price = ?, points = '', weight = ?, weight_class_id = '1', length = '', width = '', height = '', length_class_id = '1', status = '1', tax_class_id = '0', sort_order = '1', date_added = NOW(), is_variant= '0'";
        $params = array();
        $params[] = 'ssss';
        $params[] = &$data['sku'];
        $params[] = &$data['qty'];
        $params[] = &$data['price'];
        $params[] = &$data['weight'];
        $stmt = $this->con->prepare($sql);
        call_user_func_array([$stmt, 'bind_param'], $params);
        $stmt->execute();
        $stmt->get_result();
        $product_id = mysqli_insert_id($this->con);
        if (isset($_FILES['configFile']['tmp_name'])) {
            $filename = str_replace(" ", "_", $_FILES['configFile']['name']);
            $file = "../../../image/catalog/" . $filename;
            if (file_exists($file)) {
                $status = 1;
            } else {
                $status = move_uploaded_file($_FILES['configFile']['tmp_name'], $file);
            }

            if ($status)
                ;
            {
                $sql = "UPDATE " . DB_PREFIX . "product SET image = ? WHERE product_id = ?";
                $catalog_filename = "catalog/" . $filename;
                $params = array();
                $params[] = 'si';
                $params[] = &$catalog_filename;
                $params[] = &$product_id;
                $stmt = $this->con->prepare($sql);
                call_user_func_array([$stmt, 'bind_param'], $params);
                $stmt->execute();
                $stmt->get_result();
            }
        }
        $query = mysqli_query($this->con, "SELECT language_id FROM `" . DB_PREFIX . "language` WHERE status=1");
        $row = mysqli_fetch_array($query, MYSQL_ASSOC);
        $name = $data['product_name'];
        $description = $data['description'];

        $sql = "INSERT INTO " . DB_PREFIX . "product_description SET product_id = ?, language_id = ?, name = ?, description = ?, tag = '', meta_title = '', meta_description = '', meta_keyword = ''";
        $params = array();
        $params[] = 'iiss';
        $params[] = &$product_id;
        $params[] = &$row['language_id'];
        $params[] = &$name;
        $params[] = &$description;
        $stmt = $this->con->prepare($sql);
        call_user_func_array([$stmt, 'bind_param'], $params);
        $stmt->execute();
        $stmt->get_result();

        $variants = $data['variants'];
        $variants = (array) $variants[0];
        //Insert Store Product
        $sql = "INSERT INTO " . DB_PREFIX . "product_to_store SET product_id = ?, store_id = 0";
        $params = array();
        $params[] = 'i';
        $params[] = &$product_id;
        $stmt = $this->con->prepare($sql);
        call_user_func_array([$stmt, 'bind_param'], $params);
        $stmt->execute();
        $stmt->get_result();
        // Insert Size Options
        $simpleProduct = (array) $variants['simpleProducts'];
        if (isset($simpleProduct)) {
            $sql = "SELECT option_id FROM `" . DB_PREFIX . "option_description` WHERE name=?";
            $params = array();
            $params[] = 's';
            $params[] = &$this->size;
            $stmt = $this->con->prepare($sql);
            call_user_func_array([$stmt, 'bind_param'], $params);
            $stmt->execute();
            $query = $stmt->get_result();
            $row = mysqli_fetch_array($query, MYSQL_ASSOC);
            $option_id = $row['option_id'];

            $sql = "INSERT INTO " . DB_PREFIX . "product_option SET product_id = ?, option_id = ?, required = '1'";
            $params = array();
            $params[] = 'ii';
            $params[] = &$product_id;
            $params[] = &$option_id;
            $stmt = $this->con->prepare($sql);
            call_user_func_array([$stmt, 'bind_param'], $params);
            $stmt->execute();
            $stmt->get_result();
            $product_option_id = mysqli_insert_id($this->con);
            foreach ($simpleProduct as $product_option) {
                $product_option = (array) $product_option;

                if (isset($product_option['sizeId'])) {
                    if ($product_option_id != '') {
                        $price_prefix = ($product_option['price'] > $data['price']) ? '+' : '-';
                        $weight_prefix = ($product_option['weight'] > $data['weight']) ? '+' : '-';
                        if ($product_option['price'] == $data['price']) {
                            $price = 0;
                        } else if ($product_option['price'] > $data['price']) {
                            $price = (int) $product_option['price'] - (int) $data['price'];
                        } else {
                            $price = (int) $data['price'] - (int) $product_option['price'];
                        }

                        if ($product_option['weight'] == $data['weight']) {
                            $weight = 0;
                        } else if ($product_option['weight'] > $data['weight']) {
                            $weight = (int) $product_option['weight'] - (int) $data['weight'];
                        } else {
                            $weight = (int) $data['weight'] - (int) $product_option['weight'];
                        }

                        $sql = "INSERT INTO " . DB_PREFIX . "product_option_value SET product_option_id = ?, product_id = ?, option_id = ?, option_value_id = ?, quantity = ?, subtract = '1', price = ?, price_prefix = ?, points = '', points_prefix = '+', weight = ?, weight_prefix = ?";
                        $params = array();
                        $params[] = 'iiiiissss';
                        $params[] = &$product_option_id;
                        $params[] = &$product_id;
                        $params[] = &$option_id;
                        $params[] = &$product_option['sizeId'];
                        $params[] = &$product_option['qty'];
                        $params[] = &$price;
                        $params[] = &$price_prefix;
                        $params[] = &$weight;
                        $params[] = &$weight_prefix;
                        $stmt = $this->con->prepare($sql);
                        call_user_func_array([$stmt, 'bind_param'], $params);
                        $stmt->execute();
                        $stmt->get_result();
                    }
                }
            }
        }

        // Insert Color Option
        if (isset($variants['color_id'])) {
            $query = "SELECT option_id FROM `" . DB_PREFIX . "option_description` WHERE name=?";
            $params = array();
            $params[] = 's';
            $params[] = &$this->color;
            $stmt = $this->con->prepare($sql);
            call_user_func_array([$stmt, 'bind_param'], $params);
            $stmt->execute();
            $query = $stmt->get_result();
            $row = mysqli_fetch_array($query, MYSQL_ASSOC);
            $sql = "INSERT INTO " . DB_PREFIX . "product_option SET product_id = ?, option_id = ?, required = '1'";
            $params = array();
            $params[] = 'ii';
            $params[] = &$product_id;
            $params[] = &$row['option_id'];
            $stmt = $this->con->prepare($sql);
            call_user_func_array([$stmt, 'bind_param'], $params);
            $stmt->execute();
            $stmt->get_result();
            $product_option_id = mysqli_insert_id($this->con);
            $value = 0;
            if ($product_option_id != '') {
                $sql = "INSERT INTO " . DB_PREFIX . "product_option_value SET product_option_id = ?, product_id = ?, option_id = ?, option_value_id = ?, quantity = ?, subtract = '1', price =?, price_prefix = '+', points = '', points_prefix = '+', weight = ?, weight_prefix = '+'";
                $params = array();
                $params[] = 'iiiiiss';
                $params[] = &$product_option_id;
                $params[] = &$product_id;
                $params[] = &$row['option_id'];
                $params[] = &$variants['color_id'];
                $params[] = &$data['qty'];
                $params[] = &$value;
                $params[] = &$value;
                $stmt = $this->con->prepare($sql);
                call_user_func_array([$stmt, 'bind_param'], $params);
                $stmt->execute();
                $stmt->get_result();
            }
        }
        // Insert Refid Option
        $query = mysqli_query($this->con, "SELECT option_id FROM `" . DB_PREFIX . "option_description` WHERE name='refid'");
        $row = mysqli_fetch_array($query, MYSQL_ASSOC);

        $sql = "INSERT INTO " . DB_PREFIX . "product_option SET product_id = ?, option_id = ?, value = '', required = '0'";
        $params = array();
        $params[] = 'ii';
        $params[] = &$product_id;
        $params[] = &$row['option_id'];
        $stmt = $this->con->prepare($sql);
        call_user_func_array([$stmt, 'bind_param'], $params);
        $stmt->execute();
        $stmt->get_result();
        // Insert xe_is_design Option
        $query = mysqli_query($this->con, "SELECT option_id FROM `" . DB_PREFIX . "option_description` WHERE name='xe_is_design'");
        $row = mysqli_fetch_array($query, MYSQL_ASSOC);

        $sql = "INSERT INTO " . DB_PREFIX . "product_option SET product_id = ?, option_id = ?, value = '1', required = '0'";
        $params = array();
        $params[] = 'ii';
        $params[] = &$product_id;
        $params[] = &$row['option_id'];
        $stmt = $this->con->prepare($sql);
        call_user_func_array([$stmt, 'bind_param'], $params);
        $stmt->execute();
        $stmt->get_result();

        if (isset($data['cat_id'])) {
            foreach ($data['cat_id'] as $category_id) {
                $sql = "INSERT INTO " . DB_PREFIX . "product_to_category SET product_id = ?, category_id = ?";
                $params = array();
                $params[] = 'ii';
                $params[] = &$product_id;
                $params[] = &$category_id;
                $stmt = $this->con->prepare($sql);
                call_user_func_array([$stmt, 'bind_param'], $params);
                $stmt->execute();
                $stmt->get_result();
            }
        }
        //Insert Product Images
        if (isset($_FILES['simpleFile'])) {
            $count = 0;
            foreach ($_FILES['simpleFile']['tmp_name'] as $key => $product_image) {
                $filename = str_replace(" ", "_", $_FILES['simpleFile']['name'][$key]);
                $file = "../../../image/catalog/" . $filename;
                if (file_exists($file)) {
                    $status = 1;
                } else {
                    $status = move_uploaded_file($product_image, $file);
                }

                if ($status)
                    ;
                {
                    $sql = "INSERT INTO " . DB_PREFIX . "product_image SET product_id = ?, image = ?, sort_order = ?";
                    $catalog_filename = "catalog/" . $filename;
                    $params = array();
                    $params[] = 'isi';
                    $params[] = &$product_id;
                    $params[] = &$catalog_filename;
                    $params[] = &$count;
                    $stmt = $this->con->prepare($sql);
                    call_user_func_array([$stmt, 'bind_param'], $params);
                    $stmt->execute();
                    $stmt->get_result();
                }
                $count++;
            }
        }
        return $product_id;
    }

    public function getProductInfo($conf_id) {
        $sql = "SELECT * FROM " . DB_PREFIX . "product p WHERE p.product_id = ? and p.is_variant='0'";
        $params = array();
        $params[] = 'i';
        $params[] = &$conf_id;
        $stmt = $this->con->prepare($sql);
        call_user_func_array([$stmt, 'bind_param'], $params);
        $stmt->execute();
        $query = $stmt->get_result();
        if (mysqli_num_rows($query) > 0) {
            $config_id = (int) $conf_id;
        } else {
            $sql = "SELECT variant_id FROM " . DB_PREFIX . "product_variant WHERE product_id = ?";
            $params = array();
            $params[] = 'i';
            $params[] = &$conf_id;
            $stmt = $this->con->prepare($sql);
            call_user_func_array([$stmt, 'bind_param'], $params);
            $stmt->execute();
            $query = $stmt->get_result();
            $row = mysqli_fetch_array($query, MYSQL_ASSOC);
            $config_id = $row['variant_id'];
        }
        $sql = "SELECT * FROM " . DB_PREFIX . "product_variant pv LEFT JOIN " . DB_PREFIX . "product p ON (p.product_id = pv.product_id) LEFT JOIN " . DB_PREFIX . "product_description pd ON (pd.product_id = p.product_id)WHERE pv.variant_id= ?";
        $params = array();
        $params[] = 'i';
        $params[] = &$config_id;
        $stmt = $this->con->prepare($sql);
        call_user_func_array([$stmt, 'bind_param'], $params);
        $stmt->execute();
        $query = $stmt->get_result();
        $i = 0;
        $variants = array();
        if ((mysqli_num_rows($query) > 0)) {
            while ($row = mysqli_fetch_array($query, MYSQL_ASSOC)) {
                $color = $this->getProductOptionsModified($row['product_id'], 'color');
                $size = $this->getProductOptionsModified($row['product_id'], 'size');
                $variants[$i]['color_id'] = $color['option_value_id'];
                $variants[$i]['var_id'] = $row['product_id'];
                $variants[$i]['sizeid'] = array();
                foreach ($size as $siz) {
                    $variants[$i]['sizeid'][] = $siz['option_value_id'];
                }
                $i++;
            }
        }
        return $variants;
    }

    public function getProductId($conf_id, $color_id) {
        $sql = "SELECT * FROM " . DB_PREFIX . "product_variant pv LEFT JOIN " . DB_PREFIX . "product p ON (p.product_id = pv.product_id) LEFT JOIN " . DB_PREFIX . "product_description pd ON (pd.product_id = p.product_id)WHERE pv.variant_id= ?";
        $params = array();
        $params[] = 'i';
        $params[] = &$conf_id;
        $stmt = $this->con->prepare($sql);
        call_user_func_array([$stmt, 'bind_param'], $params);
        $stmt->execute();
        $query = $stmt->get_result();
        while ($row = mysqli_fetch_array($query, MYSQL_ASSOC)) {
            $color = $this->getProductOptionsModified($row['product_id'], 'color');
            if ($color_id == $color['option_value_id']) {
                $product_id = $row['product_id'];
            }
        }
        return $product_id;
    }

    public function resize($filename, $width, $height) {
        if (!is_file(DIR_IMAGE . $filename)) {
            return;
        }

        $extension = pathinfo($filename, PATHINFO_EXTENSION);

        $old_image = $filename;
        $new_image = 'cache/' . substr($filename, 0, strrpos($filename, '.')) . '-' . $width . 'x' . $height . '.' . $extension;

        if (!is_file(DIR_IMAGE . $new_image) || (filectime(DIR_IMAGE . $old_image) > filectime(DIR_IMAGE . $new_image))) {
            $path = '';

            $directories = explode('/', dirname(str_replace('../', '', $new_image)));

            foreach ($directories as $directory) {
                $path = $path . '/' . $directory;

                if (!is_dir(DIR_IMAGE . $path)) {
                    @mkdir(DIR_IMAGE . $path, 0777);
                }
            }

            list($width_orig, $height_orig) = getimagesize(DIR_IMAGE . $old_image);

            if ($width_orig != $width || $height_orig != $height) {
                $image = new ImageResize(DIR_IMAGE . $old_image);
                $image->resize($width, $height);
                $image->save(DIR_IMAGE . $new_image);
            } else {
                copy(DIR_IMAGE . $old_image, DIR_IMAGE . $new_image);
            }
        }

        if ($this->request->server['HTTPS']) {
            return $new_image;
        } else {
            return $new_image;
        }
    }

    public function addColor($color) {
        $result = array();
        $query = mysqli_query($this->con, "SELECT language_id FROM `" . DB_PREFIX . "language` WHERE status=1");
        $row2 = mysqli_fetch_array($query, MYSQL_ASSOC);
        $language_id = $row2['language_id'];
        $sql = "SELECT option_id FROM `" . DB_PREFIX . "option_description` WHERE name=?";
        $params = array();
        $params[] = 's';
        $params[] = &$this->color;
        $stmt = $this->con->prepare($sql);
        call_user_func_array([$stmt, 'bind_param'], $params);
        $stmt->execute();
        $query = $stmt->get_result();
        $row = mysqli_fetch_array($query, MYSQL_ASSOC);
        $option_id = $row['option_id'];
        $sort_order = 0;
        $sql = "INSERT INTO " . DB_PREFIX . "option_value SET option_id = ?, image = '', sort_order = ?";
        $params = array();
        $params[] = 'ii';
        $params[] = &$option_id;
        $params[] = &$sort_order;
        $stmt = $this->con->prepare($sql);
        call_user_func_array([$stmt, 'bind_param'], $params);
        $stmt->execute();
        $stmt->get_result();
        $option_value_id = mysqli_insert_id($this->con);
        $sql = "INSERT INTO " . DB_PREFIX . "option_value_description SET option_value_id = ?, language_id = ?, option_id = ?, name = ?";
        $params = array();
        $params[] = 'iiis';
        $params[] = &$option_value_id;
        $params[] = &$language_id;
        $params[] = &$option_id;
        $params[] = &$color;
        $stmt = $this->con->prepare($sql);
        call_user_func_array([$stmt, 'bind_param'], $params);
        $status = $stmt->execute();
        $query = $stmt->get_result();
        if ($status) {
            $result['attribute_id'] = $option_value_id;
            $result['attribute_value'] = $color;
            $result['status'] = 'success';
            $result['swatchImage'] = '';
        }
        return json_encode($result);
    }

    public function editColor($data) {
        $result = array();
        $sql = "SELECT option_id FROM `" . DB_PREFIX . "option_description` WHERE name=?";
        $params = array();
        $params[] = 's';
        $params[] = &$this->color;
        $stmt = $this->con->prepare($sql);
        call_user_func_array([$stmt, 'bind_param'], $params);
        $stmt->execute();
        $query = $stmt->get_result();
        $row = mysqli_fetch_array($query, MYSQL_ASSOC);
        $option_id = $row['option_id'];
        $sort_order = 0;
        $option_value_id = $data['option_id'];
        $color = $data['colorname'];
        $sql = "UPDATE " . DB_PREFIX . "option_value_description SET name = ? WHERE option_id = ? AND option_value_id = ?";
        $params = array();
        $params[] = 'sii';
        $params[] = &$color;
        $params[] = &$option_id;
        $params[] = &$option_value_id;
        $stmt = $this->con->prepare($sql);
        call_user_func_array([$stmt, 'bind_param'], $params);
        $status = $stmt->execute();
        $query = $stmt->get_result();
        if ($status) {
            $result['attribute_id'] = $option_value_id;
            $result['attribute_value'] = $color;
            $result['status'] = 'success';
            $result['swatchImage'] = '';
        }
        return json_encode($result);
    }

    public function getProductParent($product_id) {
        $sql = "SELECT variant_id FROM " . DB_PREFIX . "product_variant WHERE product_id = ?";
        $params = array();
        $params[] = 'i';
        $params[] = &$product_id;
        $stmt = $this->con->prepare($sql);
        call_user_func_array([$stmt, 'bind_param'], $params);
        $stmt->execute();
        $query = $stmt->get_result();
        if (mysqli_num_rows($query) > 0) {
            $row = mysqli_fetch_array($query, MYSQL_ASSOC);
            $parent_id = $row['variant_id'];
        } else {
            $parent_id = $product_id;
        }
        return $parent_id;
    }

    public function addColorImage($option_value_id, $filename) {
        $filename = 'catalog/swatchImage/' . $filename;
        $sql = "UPDATE " . DB_PREFIX . "option_value SET image = ? WHERE option_value_id = ?";
        $params = array();
        $params[] = 'si';
        $params[] = &$filename;
        $params[] = &$option_value_id;
        $stmt = $this->con->prepare($sql);
        call_user_func_array([$stmt, 'bind_param'], $params);
        $stmt->execute();
        $query = $stmt->get_result();
        if ($query) {
            return json_encode(array("status" => "success"));
        }

        return true;
    }

    public function addTemplateProducts($data) {
        $config_id = 0;
        $product_data = $data['data'];
        $config_id = (isset($product_data['conf_id']) && $product_data['conf_id'] != 0) ? $product_data['conf_id'] : $this->addConfigTemplateProduct($data);
        if (isset($product_data['conf_id']) && $product_data['conf_id'] != 0) {
            $sql = "SELECT price FROM `" . DB_PREFIX . "product` WHERE product_id=?";
            $params = array();
            $params[] = 'i';
            $params[] = &$config_id;
            $stmt = $this->con->prepare($sql);
            call_user_func_array([$stmt, 'bind_param'], $params);
            $stmt->execute();
            $query = $stmt->get_result();
            $row = mysqli_fetch_array($query, MYSQL_ASSOC);
            $config_price = $row['price'];
            $price_prefix = ($product_data['price'] > $config_price) ? '+' : '-';
            if ($product_data['price'] == $config_price) {
                $price = 0;
            } else if ($product_data['price'] > $config_price) {
                $price = (int) $product_data['price'] - (int) $config_price;
            } else {
                $price = (int) $config_price - (int) $product_data['price'];
            }

            $sql = "SELECT option_id FROM `" . DB_PREFIX . "option_description` WHERE name=?";
            $params = array();
            $params[] = 'i';
            $params[] = &$this->color;
            $stmt = $this->con->prepare($sql);
            call_user_func_array([$stmt, 'bind_param'], $params);
            $stmt->execute();
            $query = $stmt->get_result();
            $row = mysqli_fetch_array($query, MYSQL_ASSOC);
            $sql = "SELECT product_option_id FROM `" . DB_PREFIX . "product_option` WHERE product_id = ? AND option_id = ?";
            $params = array();
            $params[] = 'ii';
            $params[] = &$config_id;
            $params[] = &$row['option_id'];
            $stmt = $this->con->prepare($sql);
            call_user_func_array([$stmt, 'bind_param'], $params);
            $stmt->execute();
            $query1 = $stmt->get_result();
            $row1 = mysqli_fetch_array($query1, MYSQL_ASSOC);
            $config_product_color_option_id = $row1['product_option_id'];
            $value = 0;
            if ($config_product_color_option_id != '') {
                $sql = "INSERT INTO " . DB_PREFIX . "product_option_value SET product_option_id = ?, product_id = ?, option_id = ?, option_value_id = ?, quantity = ?, subtract = '1', price = ?, price_prefix = ?, points = '', points_prefix = '+', weight = ?, weight_prefix = '+'";
                $params = array();
                $params[] = 'iiiiisss';
                $params[] = &$config_product_color_option_id;
                $params[] = &$config_id;
                $params[] = &$row['option_id'];
                $params[] = &$data['varColor'];
                $params[] = &$product_data['qty'];
                $params[] = &$price;
                $params[] = &$price_prefix;
                $params[] = &$value;
                $stmt = $this->con->prepare($sql);
                call_user_func_array([$stmt, 'bind_param'], $params);
                $stmt->execute();
                $stmt->get_result();
            }
            $sql = "SELECT option_id FROM `" . DB_PREFIX . "option_description` WHERE name=?";
            $params = array();
            $params[] = 's';
            $params[] = &$this->size;
            $stmt = $this->con->prepare($sql);
            call_user_func_array([$stmt, 'bind_param'], $params);
            $stmt->execute();
            $query = $stmt->get_result();
            $row = mysqli_fetch_array($query, MYSQL_ASSOC);
            $sql = "SELECT product_option_id FROM `" . DB_PREFIX . "product_option` WHERE product_id = ? AND option_id = ?";
            $params = array();
            $params[] = 'ii';
            $params[] = &$config_id;
            $params[] = &$row['option_id'];
            $stmt = $this->con->prepare($sql);
            call_user_func_array([$stmt, 'bind_param'], $params);
            $stmt->execute();
            $query1 = $stmt->get_result();
            $row1 = mysqli_fetch_array($query1, MYSQL_ASSOC);
            $config_product_color_option_id = $row1['product_option_id'];
            if ($config_product_color_option_id != '') {
                foreach ($data['varSize'] as $size) {
                    $sql = "SELECT * FROM `" . DB_PREFIX . "product_option_value` WHERE product_id = ? AND option_id = ? AND product_option_id = ? AND option_value_id = ?";
                    $params = array();
                    $params[] = 'iiii';
                    $params[] = &$config_id;
                    $params[] = &$row['option_id'];
                    $params[] = &$config_product_color_option_id;
                    $params[] = &$size;
                    $stmt = $this->con->prepare($sql);
                    call_user_func_array([$stmt, 'bind_param'], $params);
                    $stmt->execute();
                    $query2 = $stmt->get_result();
                    if (!mysqli_num_rows($query2) > 0) {
                        $sql = "INSERT INTO " . DB_PREFIX . "product_option_value SET product_option_id = ?, product_id = ?, option_id = ?, option_value_id = ?, quantity = ?, subtract = '1', price = ?, price_prefix = ?, points = '', points_prefix = '+', weight = ?, weight_prefix = '+'";
                        $params = array();
                        $params[] = 'iiiiisss';
                        $params[] = &$config_product_color_option_id;
                        $params[] = &$config_id;
                        $params[] = &$row['option_id'];
                        $params[] = &$size;
                        $params[] = &$product_data['qty'];
                        $params[] = &$price;
                        $params[] = &$price_prefix;
                        $params[] = &$value;
                        $stmt = $this->con->prepare($sql);
                        call_user_func_array([$stmt, 'bind_param'], $params);
                        $stmt->execute();
                        $stmt->get_result();
                    }
                }
            }
        }
        if ($config_id != 0) {
            $sql = "INSERT INTO " . DB_PREFIX . "product SET model = 'Customized',sku = ?, upc = '', ean = '', jan = '', isbn = '', mpn = '', location = '', quantity = ?, minimum = ?, subtract = '1', stock_status_id = '7', date_available = NOW(), manufacturer_id = '', shipping = '', price = ?, points = '', weight = '', weight_class_id = '1', length = '', width = '', height = '', length_class_id = '1', status = '1', tax_class_id = '0', sort_order = '1', date_added = NOW(), is_variant= '1'";
            $product_data['mini_qty'] = empty($product_data['mini_qty']) ? '' : $product_data['mini_qty'];
            $params = array();
            $params[] = 'ssss';
            $params[] = &$product_data['sku'];
            $params[] = &$product_data['qty'];
            $params[] = &$product_data['mini_qty'];
            $params[] = &$product_data['price'];
            $stmt = $this->con->prepare($sql);
            call_user_func_array([$stmt, 'bind_param'], $params);
            $stmt->execute();
            $stmt->get_result();
            $product_id = mysqli_insert_id($this->con);
            $sql = "INSERT INTO `" . DB_PREFIX . "product_variant` SET `product_id` = ?, variant_id = ?";
            $params = array();
            $params[] = 'ii';
            $params[] = &$product_id;
            $params[] = &$config_id;
            $stmt = $this->con->prepare($sql);
            call_user_func_array([$stmt, 'bind_param'], $params);
            $stmt->execute();
            $query = $stmt->get_result();
            $query = mysqli_query($this->con, "SELECT language_id FROM `" . DB_PREFIX . "language` WHERE status=1");
            $row = mysqli_fetch_array($query, MYSQL_ASSOC);
            $sql = "SELECT name FROM `" . DB_PREFIX . "option_value_description` WHERE language_id=? and option_value_id=?";
            $params = array();
            $params[] = 'ss';
            $params[] = &$row['language_id'];
            $params[] = &$data['varColor'];
            $stmt = $this->con->prepare($sql);
            call_user_func_array([$stmt, 'bind_param'], $params);
            $stmt->execute();
            $query = $stmt->get_result();
            $color = mysqli_fetch_array($query, MYSQL_ASSOC);

            $name = $product_data['product_name'] . "-" . $color['name'];
            $description = $product_data['description'];
            $short_description = $product_data['short_description'];

            $sql = "INSERT INTO " . DB_PREFIX . "product_description SET product_id = ?, language_id = ?, name = ?, description = ?, tag = '', meta_title = '', meta_description = '', meta_keyword = ''";
            $params = array();
            $params[] = 'iiss';
            $params[] = &$product_id;
            $params[] = &$row['language_id'];
            $params[] = &$name;
            $params[] = &$description;
            $stmt = $this->con->prepare($sql);
            call_user_func_array([$stmt, 'bind_param'], $params);
            $stmt->execute();
            $stmt->get_result();
//Insert Store Product
            $sql = "INSERT INTO " . DB_PREFIX . "product_to_store SET product_id = ?, store_id = ?";
            $params = array();
            $params[] = 'ii';
            $params[] = &$product_id;
            $params[] = &$data['store'];
            $stmt = $this->con->prepare($sql);
            call_user_func_array([$stmt, 'bind_param'], $params);
            $stmt->execute();
            $stmt->get_result();
            $oldConfId = $data['oldConfId'];
            $pv = $this->getProductInfo($oldConfId);
            $variant_id = 0;
            foreach ($pv as $var) {
                if ($var['color_id'] == $data['varColor']) {
                    $variant_id = $var['var_id'];
                }
            }
            $sql = "SELECT image FROM `" . DB_PREFIX . "product` WHERE  product_id = ?";
            $params = array();
            $params[] = 'i';
            $params[] = &$variant_id;
            $stmt = $this->con->prepare($sql);
            call_user_func_array([$stmt, 'bind_param'], $params);
            $stmt->execute();
            $query = $stmt->get_result();
            $row = mysqli_fetch_array($query, MYSQL_ASSOC);

            $sql = "UPDATE " . DB_PREFIX . "product SET image = ? WHERE product_id = ?";
            $params = array();
            $params[] = 'si';
            $params[] = &$row['image'];
            $params[] = &$product_id;
            $stmt = $this->con->prepare($sql);
            call_user_func_array([$stmt, 'bind_param'], $params);
            $stmt->execute();
            $stmt->get_result();
            $sql = "SELECT option_id FROM `" . DB_PREFIX . "option_description` WHERE name=?";
            $params = array();
            $params[] = 's';
            $params[] = &$this->size;
            $stmt = $this->con->prepare($sql);
            call_user_func_array([$stmt, 'bind_param'], $params);
            $stmt->execute();
            $query = $stmt->get_result();
            $row = mysqli_fetch_array($query, MYSQL_ASSOC);
            $option_id = $row['option_id'];

            $sql = "INSERT INTO " . DB_PREFIX . "product_option SET product_id = ?, option_id = ?, required = '1'";
            $params = array();
            $params[] = 'ii';
            $params[] = &$product_id;
            $params[] = &$option_id;
            $stmt = $this->con->prepare($sql);
            call_user_func_array([$stmt, 'bind_param'], $params);
            $stmt->execute();
            $stmt->get_result();
            $product_size_option_id = mysqli_insert_id($this->con);
            foreach ($data['varSize'] as $size) {
                $sql = "INSERT INTO " . DB_PREFIX . "product_option_value SET product_option_id = ?, product_id = ?, option_id = ?, option_value_id = ?, quantity = ?, subtract = '1', price = '" . (float) 0 . "', price_prefix = '+', points = '', points_prefix = '+', weight = '" . (float) 0 . "', weight_prefix = '+'";
                $params = array();
                $params[] = 'iisss';
                $params[] = &$product_size_option_id;
                $params[] = &$product_id;
                $params[] = &$option_id;
                $params[] = &$size;
                $params[] = &$product_data['qty'];
                $stmt = $this->con->prepare($sql);
                call_user_func_array([$stmt, 'bind_param'], $params);
                $stmt->execute();
                $stmt->get_result();
            }
            if (isset($data['varColor'])) {
                $sql = "SELECT option_id FROM `" . DB_PREFIX . "option_description` WHERE name=?";
                $params = array();
                $params[] = 's';
                $params[] = &$this->color;
                $stmt = $this->con->prepare($sql);
                call_user_func_array([$stmt, 'bind_param'], $params);
                $stmt->execute();
                $query = $stmt->get_result();
                $row = mysqli_fetch_array($query, MYSQL_ASSOC);
                $sql = "INSERT INTO " . DB_PREFIX . "product_option SET product_id = ?, option_id = ?, required = '1'";
                $params = array();
                $params[] = 'ii';
                $params[] = &$product_id;
                $params[] = &$row['option_id'];
                $stmt = $this->con->prepare($sql);
                call_user_func_array([$stmt, 'bind_param'], $params);
                $stmt->execute();
                $stmt->get_result();
                $product_color_option_id = mysqli_insert_id($this->con);
                $value = 0;
                if ($product_color_option_id != '') {
                    $sql = "INSERT INTO " . DB_PREFIX . "product_option_value SET product_option_id = ?, product_id = ?, option_id = ?, option_value_id = ?, quantity = ?, subtract = '1', price = ?, price_prefix = '+', points = '', points_prefix = '+', weight = ?, weight_prefix = '+'";
                    $params = array();
                    $params[] = 'iiiiiss';
                    $params[] = &$product_color_option_id;
                    $params[] = &$product_id;
                    $params[] = &$row['option_id'];
                    $params[] = &$data['varColor'];
                    $params[] = &$product_data['qty'];
                    $params[] = &$value;
                    $params[] = &$value;
                    $stmt = $this->con->prepare($sql);
                    call_user_func_array([$stmt, 'bind_param'], $params);
                    $stmt->execute();
                    $stmt->get_result();
                }
            }
            // Insert Refid Option
            $query = mysqli_query($this->con, "SELECT option_id FROM `" . DB_PREFIX . "option_description` WHERE name='refid'");
            $row = mysqli_fetch_array($query, MYSQL_ASSOC);

            $sql = "INSERT INTO " . DB_PREFIX . "product_option SET product_id = ?, option_id = ?, value = ?, required = '0'";
            $params = array();
            $params[] = 'iis';
            $params[] = &$product_id;
            $params[] = &$row['option_id'];
            $params[] = &$product_data['ref_id'];
            $stmt = $this->con->prepare($sql);
            call_user_func_array([$stmt, 'bind_param'], $params);
            $stmt->execute();
            $stmt->get_result();
            // Insert xe_is_design Option
            $query = mysqli_query($this->con, "SELECT option_id FROM `" . DB_PREFIX . "option_description` WHERE name='xe_is_design'");
            $row = mysqli_fetch_array($query, MYSQL_ASSOC);

            $sql = "INSERT INTO " . DB_PREFIX . "product_option SET product_id = ?, option_id = ?, value = ?, required = '0'";
            $params = array();
            $params[] = 'iis';
            $params[] = &$product_id;
            $params[] = &$row['option_id'];
            $params[] = &$product_data['is_customized'];
            $stmt = $this->con->prepare($sql);
            call_user_func_array([$stmt, 'bind_param'], $params);
            $stmt->execute();
            $stmt->get_result();
// Insert disable_addtocart Option
            if (isset($data['productStatus'])) {
                $query = mysqli_query($this->con, "SELECT option_id FROM `" . DB_PREFIX . "option_description` WHERE name='disable_addtocart'");
                $row = mysqli_fetch_array($query, MYSQL_ASSOC);
                $value = ( $data['productStatus'] == 'without_image' ? 1 : 0);
                $sql = "INSERT INTO " . DB_PREFIX . "product_option SET product_id = ?, option_id = ?, value = ?, required = '0'";
                $params = array();
                $params[] = 'iis';
                $params[] = &$product_id;
                $params[] = &$row['option_id'];
                $params[] = &$value;
                $stmt = $this->con->prepare($sql);
                call_user_func_array([$stmt, 'bind_param'], $params);
                $stmt->execute();
                $stmt->get_result();
            }
            if (isset($product_data['cat_id'])) {
                foreach ($product_data['cat_id'] as $category_id) {
                    $sql = "INSERT INTO " . DB_PREFIX . "product_to_category SET product_id = ?, category_id = ?";
                    $params = array();
                    $params[] = 'ii';
                    $params[] = &$product_id;
                    $params[] = &$category_id;
                    $stmt = $this->con->prepare($sql);
                    call_user_func_array([$stmt, 'bind_param'], $params);
                    $stmt->execute();
                    $stmt->get_result();
                }
            }
            //Insert Product Images
            $sql = "SELECT * FROM `" . DB_PREFIX . "product_image` WHERE  product_id = ?";
            $params = array();
            $params[] = 'i';
            $params[] = &$variant_id;
            $stmt = $this->con->prepare($sql);
            call_user_func_array([$stmt, 'bind_param'], $params);
            $stmt->execute();
            $query = $stmt->get_result();
            if ((mysqli_num_rows($query) > 0)) {
                while ($row = mysqli_fetch_array($query, MYSQL_ASSOC)) {
                    $sql = "INSERT INTO " . DB_PREFIX . "product_image SET product_id = ?, image = ?, sort_order = ?";
                    $params = array();
                    $params[] = 'iss';
                    $params[] = &$product_id;
                    $params[] = &$row['image'];
                    $params[] = &$row['sort_order'];
                    $stmt = $this->con->prepare($sql);
                    call_user_func_array([$stmt, 'bind_param'], $params);
                    $stmt->execute();
                    $stmt->get_result();
                }
            }
            $pvariant = $this->getProductInfo($config_id);
            return json_encode(array("status" => "success", "conf_id" => $config_id, "variants" => $pvariant));
        } else {
            return json_encode(array("status" => "failed"));
        }
    }

    /*     * *** Addition of configurable product ***** */

    public function addConfigTemplateProduct($data) {
        $product_data = $data['data'];
        $sql = "INSERT INTO " . DB_PREFIX . "product SET model = 'Customized',sku = ?, upc = '', ean = '', jan = '', isbn = '', mpn = '', location = '', quantity = ?, minimum = ?, subtract = '1', stock_status_id = '7', date_available = NOW(), manufacturer_id = '', shipping = '', price = ?, points = '', weight = '', weight_class_id = '1', length = '', width = '', height = '', length_class_id = '1', status = '1', tax_class_id = '0', sort_order = '1', date_added = NOW(), is_variant= '0'";
        $product_data['mini_qty'] = empty($product_data['mini_qty']) ? '' : $product_data['mini_qty'];
        $params = array();
        $params[] = 'ssss';
        $params[] = &$product_data['sku'];
        $params[] = &$product_data['qty'];
        $params[] = &$product_data['mini_qty'];
        $params[] = &$product_data['price'];
        $stmt = $this->con->prepare($sql);
        call_user_func_array([$stmt, 'bind_param'], $params);
        $stmt->execute();
        $stmt->get_result();
        $product_id = mysqli_insert_id($this->con);
        $imageArr = array();
        if (isset($data['configFile'])) {
            foreach ($data['configFile'] as $imageFile) {
                if (strpos(stripslashes($imageFile), 'capturedImage/') !== false) {
                    $filename = explode("capturedImage/preDecoProduct/", $imageFile);
                    $filename = $filename[1];
                    $destinationImagePath = DIR_IMAGE . 'catalog/preDecoProduct/' . $product_id;
                    $destImageFile = $destinationImagePath . '/' . $filename;
                    $contents = file_get_contents($imageFile);
                    if (!file_exists($mkDirPreviw)) {
                        $exp = explode('/', $destinationImagePath);
                        foreach ($exp as $dir) {
                            $mkDirPreviw .= $dir . "/";
                            $dira[] = $mkDirPreviw;
                            if (!file_exists($mkDirPreviw)) {
                                mkdir($mkDirPreviw, 0755, true);
                            }
                        }
                    }
                    file_put_contents($destImageFile, $contents);
                    $imageArr[] = 'catalog/preDecoProduct/' . $product_id . '/' . $filename;
                } else {
                    $filename = explode("catalog/", $imageFile);
                    $filename = $filename[1];
                    $imageArr[] = 'catalog/' . $filename;
                }
            }
            $sql = "UPDATE " . DB_PREFIX . "product SET image = ? WHERE product_id = ?";
            $params = array();
            $params[] = 'ss';
            $params[] = &$imageArr[0];
            $params[] = &$product_id;
            $stmt = $this->con->prepare($sql);
            call_user_func_array([$stmt, 'bind_param'], $params);
            $stmt->execute();
            $stmt->get_result();
        }
        $query = mysqli_query($this->con, "SELECT language_id FROM `" . DB_PREFIX . "language` WHERE status=1");
        $row = mysqli_fetch_array($query, MYSQL_ASSOC);
        $name = $product_data['product_name'];
        $description = $product_data['description'];
        $short_description = $product_data['short_description'];

        $sql = "INSERT INTO " . DB_PREFIX . "product_description SET product_id = ?, language_id = ?, name = ?, description = ?, tag = '', meta_title = '', meta_description = ?, meta_keyword = ''";
        $params = array();
        $params[] = 'sssss';
        $params[] = &$product_id;
        $params[] = &$row['language_id'];
        $params[] = &$name;
        $params[] = &$description;
        $params[] = &$short_description;
        $stmt = $this->con->prepare($sql);
        call_user_func_array([$stmt, 'bind_param'], $params);
        $stmt->execute();
        $stmt->get_result();
//Insert Store Product
        $sql = "INSERT INTO " . DB_PREFIX . "product_to_store SET product_id = ?, store_id = ?";
        $params = array();
        $params[] = 'ii';
        $params[] = &$product_id;
        $params[] = &$data['store'];
        $stmt = $this->con->prepare($sql);
        call_user_func_array([$stmt, 'bind_param'], $params);
        $stmt->execute();
        $stmt->get_result();
        $sql = "SELECT option_id FROM `" . DB_PREFIX . "option_description` WHERE name=?";
        $params = array();
        $params[] = 's';
        $params[] = &$this->size;
        $stmt = $this->con->prepare($sql);
        call_user_func_array([$stmt, 'bind_param'], $params);
        $stmt->execute();
        $query = $stmt->get_result();
        $row = mysqli_fetch_array($query, MYSQL_ASSOC);
        $option_id = $row['option_id'];

        $sql = "INSERT INTO " . DB_PREFIX . "product_option SET product_id = ?, option_id = ?, required = '1'";
        $params = array();
        $params[] = 'ii';
        $params[] = &$product_id;
        $params[] = &$option_id;
        $stmt = $this->con->prepare($sql);
        call_user_func_array([$stmt, 'bind_param'], $params);
        $stmt->execute();
        $stmt->get_result();
        $product_size_option_id = mysqli_insert_id($this->con);
        foreach ($data['varSize'] as $size) {
            $sql = "INSERT INTO " . DB_PREFIX . "product_option_value SET product_option_id = ?, product_id = ?, option_id = ?, option_value_id = ?, quantity = ?, subtract = '1', price = '" . (float) 0 . "', price_prefix = '+', points = '', points_prefix = '+', weight = '" . (float) 0 . "', weight_prefix = '+'";
            $params = array();
            $params[] = 'iiiii';
            $params[] = &$product_size_option_id;
            $params[] = &$product_id;
            $params[] = &$option_id;
            $params[] = &$size;
            $params[] = &$product_data['qty'];
            $stmt = $this->con->prepare($sql);
            call_user_func_array([$stmt, 'bind_param'], $params);
            $stmt->execute();
            $stmt->get_result();
        }
        if (isset($data['varColor'])) {
            $sql = "SELECT option_id FROM `" . DB_PREFIX . "option_description` WHERE name=?";
            $params = array();
            $params[] = 's';
            $params[] = &$this->color;
            $stmt = $this->con->prepare($sql);
            call_user_func_array([$stmt, 'bind_param'], $params);
            $stmt->execute();
            $query = $stmt->get_result();
            $row = mysqli_fetch_array($query, MYSQL_ASSOC);
            $sql = "INSERT INTO " . DB_PREFIX . "product_option SET product_id = ?, option_id = ?, required = '1'";
            $params = array();
            $params[] = 'ii';
            $params[] = &$product_id;
            $params[] = &$row['option_id'];
            $stmt = $this->con->prepare($sql);
            call_user_func_array([$stmt, 'bind_param'], $params);
            $stmt->execute();
            $stmt->get_result();
            $product_color_option_id = mysqli_insert_id($this->con);
            $value = 0;
            if ($product_color_option_id != '') {
                $sql = "INSERT INTO " . DB_PREFIX . "product_option_value SET product_option_id = ?, product_id = ?, option_id = ?, option_value_id = ?, quantity = ?, subtract = '1', price = ?, price_prefix = '+', points = '', points_prefix = '+', weight = ?, weight_prefix = '+'";
                $params = array();
                $params[] = 'iiiiiss';
                $params[] = &$product_color_option_id;
                $params[] = &$product_id;
                $params[] = &$row['option_id'];
                $params[] = &$data['varColor'];
                $params[] = &$product_data['qty'];
                $params[] = &$value;
                $params[] = &$value;
                $stmt = $this->con->prepare($sql);
                call_user_func_array([$stmt, 'bind_param'], $params);
                $stmt->execute();
                $stmt->get_result();
            }
        }

        // Insert Refid Option
        $sql = mysqli_query($this->con, "SELECT option_id FROM `" . DB_PREFIX . "option_description` WHERE name='refid'");
        $row = mysqli_fetch_array($sql, MYSQL_ASSOC);

        $sql = "INSERT INTO " . DB_PREFIX . "product_option SET product_id = ?, option_id = ?, value = ?, required = '0'";
        $params = array();
        $params[] = 'iis';
        $params[] = &$product_id;
        $params[] = &$row['option_id'];
        $params[] = &$product_data['ref_id'];
        $stmt = $this->con->prepare($sql);
        call_user_func_array([$stmt, 'bind_param'], $params);
        $stmt->execute();
        $stmt->get_result();
        // Insert xe_is_design Option
        $query = mysqli_query($this->con, "SELECT option_id FROM `" . DB_PREFIX . "option_description` WHERE name='xe_is_design'");
        $row = mysqli_fetch_array($query, MYSQL_ASSOC);

        $sql = "INSERT INTO " . DB_PREFIX . "product_option SET product_id = ?, option_id = ?, value = ?, required = '0'";
        $params = array();
        $params[] = 'iis';
        $params[] = &$product_id;
        $params[] = &$row['option_id'];
        $params[] = &$product_data['is_customized'];
        $stmt = $this->con->prepare($sql);
        call_user_func_array([$stmt, 'bind_param'], $params);
        $stmt->execute();
        $stmt->get_result();
// Insert disable_addtocart Option
        if (isset($data['productStatus'])) {
            $query = mysqli_query($this->con, "SELECT option_id FROM `" . DB_PREFIX . "option_description` WHERE name='disable_addtocart'");
            $row = mysqli_fetch_array($query, MYSQL_ASSOC);
            $value = ( $data['productStatus'] == 'without_image' ? 1 : 0);
            $sql = "INSERT INTO " . DB_PREFIX . "product_option SET product_id = ?, option_id = ?, value = ?, required = '0'";
            $params = array();
            $params[] = 'iis';
            $params[] = &$product_id;
            $params[] = &$row['option_id'];
            $params[] = &$value;
            $stmt = $this->con->prepare($sql);
            call_user_func_array([$stmt, 'bind_param'], $params);
            $stmt->execute();
            $stmt->get_result();
        }
        if (isset($product_data['cat_id'])) {
            foreach ($product_data['cat_id'] as $category_id) {
                $sql = "INSERT INTO " . DB_PREFIX . "product_to_category SET product_id = ?, category_id = ?";
                $params = array();
                $params[] = 'ii';
                $params[] = &$product_id;
                $params[] = &$category_id;
                $stmt = $this->con->prepare($sql);
                call_user_func_array([$stmt, 'bind_param'], $params);
                $stmt->execute();
                $stmt->get_result();
            }
        }
        //Insert Product Images
        if (!empty($imageArr)) {
            $count = 0;
            foreach ($imageArr as $product_image) {
                $sql = "INSERT INTO " . DB_PREFIX . "product_image SET product_id = ?, image = ?, sort_order = ?";
                $params = array();
                $params[] = 'iss';
                $params[] = &$product_id;
                $params[] = &$product_image;
                $params[] = &$count;
                $stmt = $this->con->prepare($sql);
                call_user_func_array([$stmt, 'bind_param'], $params);
                $stmt->execute();
                $stmt->get_result();
                $count++;
            }
        }
        return $product_id;
    }

    /*
     * Method to get the tier discount
     * return array
     */

    public function getTierPrice($productId) {
        $tier = array();
        $todayDate = date("Y-m-d");
        $sql = "SELECT price, quantity FROM " . DB_PREFIX . "product_discount WHERE product_id = ? AND (date_start IS NULL  OR date_start >= ?) AND (date_end IS NULL OR date_end <= ?)";
        $params = array();
        $params[] = 'sss';
        $params[] = &$productId;
        $params[] = &$todayDate;
        $params[] = &$todayDate;
        $stmt = $this->con->prepare($sql);
        call_user_func_array([$stmt, 'bind_param'], $params);
        $stmt->execute();
        $tierExecute = $stmt->get_result();
        if (mysqli_num_rows($tierExecute) > 0) {
            $k = 0;
            while ($tierRow = mysqli_fetch_array($tierExecute, MYSQL_ASSOC)) {
                $tier[$k]['price'] = $tierRow['price'];
                $tier[$k]['quantity'] = $tierRow['quantity'];
                $k++;
            }
        }
        return $tier;
    }

    /*
     * Method to get the number of cart items
     * return string
     */

    public function getTotalCartItem() {
        $version = $this->getVersion();
        if ($version >= '3.0.0.0') {
            $cookieObj = $_COOKIE;
            $session_id = $cookieObj['OCSESSID'];
            $sessionArr = $this->sessionRead($session_id);
            if (isset($sessionArr['customer_id']) && $sessionArr['customer_id']) {
                $customer_id = $sessionArr['customer_id'];
            } else {
                $customer_id = 0;
            }
            $count = 0;
            if (isset($sessionArr['cart'])) {
                foreach ($sessionArr['cart'] as $key => $quantity) {
                    $count += $quantity;
                }
            } else {
                $sql = "SELECT quantity FROM " . DB_PREFIX . "cart WHERE customer_id = ? AND session_id = ?";
                $params = array();
                $params[] = 'ss';
                $params[] = &$customer_id;
                $params[] = &$session_id;
                $stmt = $this->con->prepare($sql);
                call_user_func_array([$stmt, 'bind_param'], $params);
                $stmt->execute();
                $cartQuery = $stmt->get_result();
                while ($cartTotal = mysqli_fetch_array($cartQuery, MYSQL_ASSOC)) {
                    $count += $cartTotal['quantity'];
                }
            }
        } else {
            session_start();
            $session_key = '';
            if (isset($_SESSION)) {
                $session_id = '';
                $i = 0;
                foreach ($_SESSION as $key => $value) {
                    if ($i > 0) {
                        break;
                    }

                    $session_key = $key;
                    if ($session_id == '' && $key != 'default') {
                        $session_id = $key;
                    } else {
                        $session_id = session_id();
                    }
                    $i++;
                }
            }
            $count = 0;
            if (isset($_SESSION['cart'])) {
                foreach ($_SESSION['cart'] as $key => $quantity) {
                    $count += $quantity;
                }
            } else {
                $sql = "SELECT quantity FROM " . DB_PREFIX . "cart WHERE customer_id = ? AND session_id = ?";
                $customer_id = isset($_SESSION[$session_key]['customer_id']) ? (int) $_SESSION[$session_key]['customer_id'] : 0;
                $params = array();
                $params[] = 'ss';
                $params[] = &$customer_id;
                $params[] = &$session_id;
                $stmt = $this->con->prepare($sql);
                call_user_func_array([$stmt, 'bind_param'], $params);
                $stmt->execute();
                $cartQuery = $stmt->get_result();
                while ($cartTotal = mysqli_fetch_array($cartQuery, MYSQL_ASSOC)) {
                    $count += $cartTotal['quantity'];
                }
            }
        }
        return $count;
    }

    /* Modification 
     * fetching product options from the store
     * 27-03-17
     */

    public function getStoreOptions() {

        $product_option_data = array();

        $sql = "SELECT * FROM `" . DB_PREFIX . "option` o LEFT JOIN `" . DB_PREFIX . "option_description` od ON (o.option_id = od.option_id) ORDER BY o.option_id";
        $query = mysqli_query($this->con, $sql);
        $i = 0;
        while ($row = mysqli_fetch_array($query, MYSQL_ASSOC)) {
            $product_option_data[$i][option_id] = $row['option_id'];
            $product_option_data[$i]['name'] = $row['name'];
            $i++;
        }
        return $product_option_data;
    }

    /**
     *
     * date created (dd-mm-yy)
     * date modified 24-05-2017(dd-mm-yy)
     * Get all product options
     *
     * @param (int)product_id
     * @return array data
     *
     */
    public function getProductOptions($product_id) {
        $productOptionData = array();
        $priceSql = "SELECT price FROM " . DB_PREFIX . "product WHERE product_id=?";
        $params = array();
        $params[] = 'i';
        $params[] = &$product_id;
        $stmt = $this->con->prepare($priceSql);
        call_user_func_array([$stmt, 'bind_param'], $params);
        $stmt->execute();
        $priceQuery = $stmt->get_result();
        $priceResult = mysqli_fetch_array($priceQuery, MYSQL_ASSOC);
        $sql = "SELECT * FROM " . DB_PREFIX . "product_option po LEFT JOIN `" . DB_PREFIX . "option` o ON (po.option_id = o.option_id) LEFT JOIN " . DB_PREFIX . "option_description od ON (o.option_id = od.option_id) WHERE po.product_id = ? ORDER BY o.sort_order";
        $params = array();
        $params[] = 'i';
        $params[] = &$product_id;
        $stmt = $this->con->prepare($sql);
        call_user_func_array([$stmt, 'bind_param'], $params);
        $stmt->execute();
        $productOptionQuery = $stmt->get_result();
        $i = 0;
        while ($row = mysqli_fetch_array($productOptionQuery, MYSQL_ASSOC)) {
            if ($row['name'] != 'refid' && $row['name'] != 'xe_is_design' && $row['name'] != 'disable_addtocart') {
                if ($row['type'] == 'select' || $row['type'] == 'radio' || $row['type'] == 'checkbox' || $row['type'] == 'image') {
                    $productOptionData[$i]['option_title'] = $row['name'];
                    $productOptionData[$i]['option_id'] = $row['product_option_id'];
                    $productOptionData[$i]['type'] = $row['type'];
                    $productOptionData[$i]['is_require'] = $row['required'];
                    $sql = "SELECT pov.option_value_id, pov.price, pov.price_prefix, ov.sort_order, ovd.name FROM " . DB_PREFIX . "product_option_value pov LEFT JOIN " . DB_PREFIX . "option_value ov ON (pov.option_value_id = ov.option_value_id) LEFT JOIN " . DB_PREFIX . "option_value_description ovd ON (ov.option_value_id = ovd.option_value_id) WHERE pov.product_id = ? AND pov.product_option_id = ?  ORDER BY ov.sort_order";
                    $params = array();
                    $params[] = 'ii';
                    $params[] = &$product_id;
                    $params[] = &$row['product_option_id'];
                    $stmt = $this->con->prepare($sql);
                    call_user_func_array([$stmt, 'bind_param'], $params);
                    $stmt->execute();
                    $optionQuery = $stmt->get_result();
                    $j = 0;
                    while ($optionResult = mysqli_fetch_array($optionQuery, MYSQL_ASSOC)) {
                        $price = ($optionResult['price_prefix'] == '+') ? $priceResult['price'] + $optionResult['price'] : $priceResult['price'] - $optionResult['price'];
                        $productOptionData[$i]['option_values'][$j]['option_type_id'] = $optionResult['option_value_id'];
                        $productOptionData[$i]['option_values'][$j]['sku'] = '';
                        $productOptionData[$i]['option_values'][$j]['sort_order'] = $optionResult['sort_order'];
                        $productOptionData[$i]['option_values'][$j]['title'] = $optionResult['name'];
                        $productOptionData[$i]['option_values'][$j]['price'] = $price;
                        $j++;
                    }
                }
                $i++;
            }
        }
        $option = array();
        foreach ($productOptionData as $data) {
            $option[] = $data;
        }
        return $option;
    }

    /**
     *
     * date of created 06-06-2017(dd-mm-yy)
     * date of Modified (dd-mm-yy)
     * get all Product options
     *
     * @return Array  data
     *
     */
    public function getAllOptions($filter) {
        $product_option_data = array();
        $query = mysqli_query($this->con, "SELECT language_id FROM `" . DB_PREFIX . "language` WHERE status=1");
        $row = mysqli_fetch_array($query, MYSQL_ASSOC);
        $language_id = $row['language_id'];
        $sql = "SELECT o.option_id, od.name FROM `" . DB_PREFIX . "option` o LEFT JOIN `" . DB_PREFIX . "option_description` od ON (o.option_id = od.option_id) WHERE language_id = ? ORDER BY o.option_id";
        $params = array();
        $params[] = 'i';
        $params[] = &$language_id;
        $stmt = $this->con->prepare($sql);
        call_user_func_array([$stmt, 'bind_param'], $params);
        $stmt->execute();
        $query = $stmt->get_result();
        $i = 0;
        while ($row = mysqli_fetch_array($query, MYSQL_ASSOC)) {
            $product_option_data[$i]['id'] = $row['option_id'];
            $product_option_data[$i]['attribute_code'] = $row['name'];
            $i++;
        }
        return $product_option_data;
    }

    /**
     *
     * date of created 09-09-2017(dd-mm-yy)
     * date of Modified (dd-mm-yy)
     * get latest version of opencart
     *
     * @return String
     *
     */
    public function getVersion() {
        $opencartPath = XEPATH;
        $url = $opencartPath . 'vcheck.php';
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        return $version = curl_exec($ch);
    }

    /**
     * date of created 09-09-2017(dd-mm-yy)
     * date of Modified (dd-mm-yy)
     * Get all session value
     *
     * @param (int)session_id
     * @return array session details
     */
    public function sessionRead($session_id) {
        $file = $this->directory . '/sess_' . basename($session_id);
        if (is_file($file)) {
            $handle = fopen($file, 'r');
            flock($handle, LOCK_SH);
            $data = fread($handle, filesize($file));
            flock($handle, LOCK_UN);
            fclose($handle);
            $session_values = unserialize($data);
            return $session_values;
        } else {
            return array();
        }
    }

    /**
     * date of created 09-09-2017(dd-mm-yy)
     * date of Modified (dd-mm-yy)
     * Write all session value
     *
     * @param (int)session_id
     * @param (String)data
     * @return boolean
     */
    public function sessionWrite($session_id, $data) {
        $file = $this->directory . '/sess_' . basename($session_id);
        $handle = fopen($file, 'wr');
        flock($handle, LOCK_EX);
        fwrite($handle, serialize($data));
        fflush($handle);
        flock($handle, LOCK_UN);
        fclose($handle);
        return true;
    }

    /**
     * date of created 09-09-2017(dd-mm-yy)
     * date of Modified (dd-mm-yy)
     * Add to cart for opencart 3
     *
     * @param (Array)cartData
     * @return boolean
     */
    public function customAddToCart($cartData) {
        $cartData = (array) $cartData;
        $status = false;
        try {
            foreach ($cartData as $cartArray) {
                $data = (object) $cartArray;
                $sessionArr = $this->sessionRead($data->session_id);
                if (isset($sessionArr['customer_id'])) {
                    $customer = $sessionArr['customer_id'];
                } else {
                    $customer = 0;
                }
                $recurring_id = 0;
                $product_id = (int) $data->id;
                if ($data->options) {
                    $option = $data->options;
                }
                $conn = mysqli_connect(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);

                $sql = "SELECT cart_id, COUNT(*) AS total FROM " . DB_PREFIX . "cart WHERE api_id = ? AND customer_id = ? AND session_id = ? AND product_id = ? AND recurring_id = ? AND `option` = ?";
                $sessionArr['data']['api_id'] = isset($sessionArr['data']['api_id']) ? (int) $sessionArr['data']['api_id'] : 0;
                $option = json_encode($option);
                $params = array();
                $params[] = 'ssssss';
                $params[] = &$sessionArr['data']['api_id'];
                $params[] = &$customer;
                $params[] = &$data->session_id;
                $params[] = &$product_id;
                $params[] = &$recurring_id;
                $params[] = &$option;
                $stmt = $conn->prepare($sql);
                call_user_func_array([$stmt, 'bind_param'], $params);
                $stmt->execute();
                $query = $stmt->get_result();
                $row = mysqli_fetch_assoc($query);

                if (!$row['total']) {
                    $sql = "INSERT " . DB_PREFIX . "cart SET api_id = ?, customer_id = ?, session_id = ?, product_id = ?, recurring_id = ?, `option` = ?, quantity = ?, date_added = NOW()";
                    $sessionArr['data']['api_id'] = isset($sessionArr['data']['api_id']) ? (int) $sessionArr['data']['api_id'] : 0;
                    $params = array();
                    $params[] = 'ssssssi';
                    $params[] = &$sessionArr['data']['api_id'];
                    $params[] = &$customer;
                    $params[] = &$data->session_id;
                    $params[] = &$product_id;
                    $params[] = &$recurring_id;
                    $params[] = &$option;
                    $params[] = &$data->qty;
                    $stmt = $conn->prepare($sql);
                    call_user_func_array([$stmt, 'bind_param'], $params);
                    $status = $stmt->execute();
                    $stmt->get_result();
                    $cart_id = mysqli_insert_id($conn);
                } else {
                    $sql = "UPDATE " . DB_PREFIX . "cart SET quantity = (quantity + " . (int) $data->qty . ") WHERE api_id = ? AND customer_id = ? AND session_id = ? AND product_id = ? AND recurring_id = ? AND `option` = ?";
                    $sessionArr['data']['api_id'] = isset($sessionArr['data']['api_id']) ? (int) $sessionArr['data']['api_id'] : 0;
                    $params = array();
                    $params[] = 'issssss';
                    $params[] = &$data->qty;
                    $params[] = &$sessionArr['data']['api_id'];
                    $params[] = &$customer;
                    $params[] = &$data->session_id;
                    $params[] = &$product_id;
                    $params[] = &$recurring_id;
                    $params[] = &$option;
                    $stmt = $conn->prepare($sql);
                    call_user_func_array([$stmt, 'bind_param'], $params);
                    $stmt->execute();
                    $status = $stmt->get_result();
                    $cart_id = $row['cart_id'];
                }
                if (isset($data->extra_price)) {
                    $sessionArr['cart-design'][$cart_id]['extra-price'] = $data->extra_price;
                }
                if (isset($data->refid)) {
                    $sessionArr['cart-design'][$cart_id]['refid'] = $data->refid;
                }
                $this->sessionWrite($data->session_id, $sessionArr);
            }
            if ($status) {
                return true;
            }
        } catch (Exception $e) {
            return $msg = $e->getMessage();
        }
    }

}
