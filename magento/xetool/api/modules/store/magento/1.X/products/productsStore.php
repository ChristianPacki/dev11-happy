<?php
/* Check Un-authorize Access */
if (!defined('accessUser')) {
    die("Error");
}

class ProductsStore extends UTIL
{
    public $_request = array();

    ####################################################
    ############## syncOrdersZip ###################
    ####################################################
    public function getsubCategories()
    {
        $error = '';
        $result = $this->storeApiLogin();
        if ($this->storeApiLogin == true) {
            $key = $GLOBALS['params']['apisessId'];
            try {
                $filters = array('catid' => $this->_request['selectedCategory'], 'store' => $this->getDefaultStoreId());
                $result = $this->proxy->call($key, 'cedapi_product.getsubCategories', $filters);
            } catch (Exception $e) {
                $result = json_encode(array('isFault' => 1, 'faultMessage' => $e->getMessage()));
                $error = true;
            }
            if (!$error) {
                $categories = array();
                $this->response($result, 200);
            } else {
                $this->response($this->json($result), 200);
            }
        } else {
            $msg = array('status' => 'apiLoginFailed', 'error' => json_decode($result));
            $this->response($this->json($msg), 200);
        }
    }


    ####################################################
    ############## syncOrdersZip ###################
    ####################################################
    public function getProductCount()
    {
        $error = false;

        $result = $this->storeApiLogin();
        if ($this->storeApiLogin == true) {
            $key = $GLOBALS['params']['apisessId'];
            $filterArray = array('type' => array('eq' => 'configurable'));
            try {
                $filters = array(
                    'filters' => $filterArray,
                    'store' => $this->getDefaultStoreId()
                );
                $result = $this->proxy->call($key, 'cedapi_product.getProductCount', $filters);
            } catch (Exception $e) {
                $result = json_encode(array('isFault' => 1, 'faultMessage' => $e->getMessage()));
                $error = true;
            }
            if (!$error) {
                $this->response($result, 200);
            } else {
                $this->response($this->json($result), 200);
            }
        } else {
            $msg = array('status' => 'apiLoginFailed', 'error' => json_decode($result));
            $this->response($this->json($msg), 200);
        }
    }

    /**
     * Used to get all the xe_size inside magento
     *
     * @param   nothing
     * @return  array contains all the xe_size inside store
     */
    public function getSizeArr()
    {
        $error = '';
        $result = $this->storeApiLogin();
        if ($this->storeApiLogin == true) {
            $key = $GLOBALS['params']['apisessId'];
            $filters = array(
                'size' => $this->getStoreAttributes("xe_size"),
                'store' => $this->getDefaultStoreId()
            );
            try {
                $result = $this->proxy->call($key, 'cedapi_product.getSizeArr', $filters);
            } catch (Exception $e) {
                $result = json_encode(array('isFault' => 1, 'faultMessage' => $e->getMessage()));
                $error = true;
            }
            if (!$error) {
                $categories = array();
                $this->response($result, 200);
            } else {
                $this->response($result, 200);
            }
        } else {
            $msg = array('status' => 'apiLoginFailed', 'error' => json_decode($result));
            $this->response($this->json($msg), 200);
        }
    }
    /**
     * Used to get all the xe_color inside magento
     *
     * @param   nothing
     * @return  array contains all the xe_color inside store
     */
    public function getColorArr($isSameClass = false)
    {
        $error = '';
        $result = $this->storeApiLogin();
        $productId = 0;
        if (!empty($this->_request['productId'])) {
            $productId = $this->_request['productId'];
        }
        if ($this->storeApiLogin == true) {
            $key = $GLOBALS['params']['apisessId'];
            try {
                $filter = array('lastLoaded' => $this->_request['lastLoaded'], 'loadCount' => $this->_request['loadCount'], 'oldConfId' => $productId, 'color' => $this->getStoreAttributes("xe_color"), 'store' => $this->getDefaultStoreId());
                $result = $this->proxy->call($key, 'cedapi_product.getColorArr', $filter);
            } catch (Exception $e) {
                $result = json_encode(array('isFault' => 1, 'faultMessage' => $e->getMessage()));
                $error = true;
            }
            if (!$error) {
                if ($isSameClass) {
                    return $result;
                } else {
                    $this->response($result, 200);
                }
            } else {
                $this->response($result, 200);
            }
        } else {
            $msg = array('status' => 'apiLoginFailed', 'error' => json_decode($result));
            $this->response($this->json($msg), 200);
        }
    }
    /**
     * Check whether the given sku exists or doesn't
     *
     * @param   $sku_arr
     * @return  true/false
     */
    public function checkDuplicateSku()
    {
        // chk for storeid
        $error = false;
        $result = $this->storeApiLogin();
        if (!empty($this->_request) && $this->storeApiLogin == true) {
            $key = $GLOBALS['params']['apisessId'];
            if (!$error) {
                $filters = array(
                    'sku_arr' => $this->_request['sku_arr'],
                    'store' => $this->getDefaultStoreId()
                );
                try {
                    $result = $this->proxy->call($key, 'cedapi_product.checkDuplicateSku', $filters);
                } catch (Exception $e) {
                    $result = json_encode(array('isFault: ' => 1, 'faultMessage' => $e->getMessage()));
                    $error = true;
                }
            }
            $this->closeConnection();
            $this->response($result, 200);
        } else {
            $msg = array('status' => 'apiLoginFailed', 'error' => json_decode($result));
            $this->response($this->json($msg), 200);
        }
    }
    ####################################################
    ############## syncOrdersZip ###################
    ####################################################
    public function getVariantList()
    {
        $error = false;
        $resultArr = $this->storeApiLogin();
        if ($this->storeApiLogin == true) {
            $key = $GLOBALS['params']['apisessId'];
            try {
                $confId = $this->_request['conf_pid'];
                $filters = array(
                    'confId' => $confId,
                    'color' => $this->getStoreAttributes("xe_color"),
                    'size' => $this->getStoreAttributes("xe_size"),
                    'store' => $this->getDefaultStoreId()
                );
                $resultArr = $this->proxy->call($key, 'cedapi_product.getVariantList', $filters);
            } catch (Exception $e) {
                $resultArr = json_encode(array('isFault' => 1, 'faultMessage' => $e->getMessage()));
                $error = true;
            }
            if (!$error) {
                $this->response($resultArr, 200);
            } else {
                $this->response($resultArr, 200);
            }
        } else {
            $msg = array('status' => 'apiLoginFailed', 'error' => json_decode($resultArr));
            $this->response($this->json($msg), 200);
        }
    }
    ####################################################
    ############## syncOrdersZip ###################
    ####################################################
    public function addProducts()
    {
        $error = false;
        if (!empty($this->_request['data']) && !empty($_FILES['simpleFile'])) {
            $data = json_decode($this->_request['data'], true);
            $this->_request['apikey'] = $data['apikey'];
            $result = $this->storeApiLogin();
            if ($this->storeApiLogin == true) {
                $key = $GLOBALS['params']['apisessId'];
                if (!$error) {
                    try {
                        $arr = array('store' => $this->getDefaultStoreId(), 'data' => $data, 'configFile' => $_FILES['configFile'], 'simpleFile' => $_FILES['simpleFile'], 'color' => $this->getStoreAttributes("xe_color"), 'size' => $this->getStoreAttributes("xe_size"), 'attrSet' => $this->getStoreAttributes("inkXE"));
                        $result = $this->proxy->call($key, 'cedapi_product.addProducts', $arr);
                    } catch (Exception $e) {
                        $result = json_encode(array('isFault' => 1, 'faultMessage' => $e->getMessage()));
                        $error = true;
                    }
                }
                $this->response($result, 200);
            } else {
                $msg = array('status' => 'apiLoginFailed', 'error' => json_decode($result));
                $this->response($this->json($msg), 200);
            }
        }
    }
    /**
     *
     *date created (dd-mm-yy)
     *date modified 15-4-2016(dd-mm-yy)
     *fetch print method id and name
     *
     *@param (String)apikey
     *@param (int)productid
     *@return json data
     *
     */
    public function getProductPrintMethod()
    {
        $productId = $this->_request['productid'];
        $key = $this->_request['apikey'];
        if (!empty($productId)) {
// &&  !empty($key) && $this->isValidCall($key)){
            $error = false;
            $productPrintType = $this->getProductPrintMethodType($productId);
            if (!empty($productPrintType)) {
                foreach ($productPrintType as $k2 => $v2) {
                    $printDetails[$k2]['print_method_id'] = $v2['pk_id'];
                    $printDetails[$k2]['name'] = $v2['name'];
                }
            } else {
                $result = $this->storeApiLogin();
                if ($this->storeApiLogin == true) {
                    $key = $GLOBALS['params']['apisessId'];
                    try {
                        $filters = array(
                            'productid' => $productId,
                            'store' => $this->getDefaultStoreId()
                        );
                        $result = $this->proxy->call($key, 'cedapi_product.getCategoriesByProduct', $filters);
                        $catIds = json_decode($result);
                        $catIds = implode(',', (array) $catIds);
                        $printDetails = $this->getPrintMethodDetailsByCategory($catIds);
                    } catch (Exception $e) {
                        $result = json_encode(array('isFault' => 1, 'faultMessage' => $e->getMessage()));
                        $error = true;
                    }
                } else {
                    $msg = array('status' => 'apiLoginFailed', 'error' => json_decode($result));
                    $this->response($this->json($msg), 200);
                }
            }
            if (!$error) {
                $resultArr = $printDetails;
                $result = json_encode($resultArr);
                $this->response($this->json($resultArr), 200);
                //print_r($result);
            } else {
                $this->response($result, 200);
            }
        } else {
            $msg = array("status" => "invalid Product Id");
            $this->response($this->json($msg), 200);
        }
    }
    /**
     *
     *date created 31-05-2016(dd-mm-yy)
     *date modified 20-03-2018(dd-mm-yy)
     *Add template as product
     *
     *
     */
     public function addTemplateProducts()
    {
        $error = false;
        if (!empty($this->_request['data'])) {
            $data = $this->_request['data'];
            $apikey = $this->_request['apikey'];
            $result = $this->storeApiLogin();
            if ($this->storeApiLogin == true) {
                $key = $GLOBALS['params']['apisessId'];
                if (!$error) {
                    try {
                        // $productsObj = Flight::products();
                        // $predecorateObj = Flight::predecorate();
                        // $templateObj = Flight::template();
                        $data = json_decode(urldecode($data), true);
                        $arr = array('store' => $this->getDefaultStoreId(), 'data' => $data, 'configFile' => $data['images'], 'oldConfId' => $data['simpleproduct_id'], 'varColor' => $data['color_id'], 'varSize' => $data['sizes'], 'color' => $this->getStoreAttributes("xe_color"), 'size' => $this->getStoreAttributes("xe_size"), 'attrSet' => $this->getStoreAttributes("inkXE"), 'productStatus' => $data['captureType']);
                        $result = $this->proxy->call($key, 'cedapi_product.addTemplateProducts', $arr);
                        $resultData = json_decode($result, true);
                        $this->customRequest(array('productid' => $data['simpleproduct_id'], 'isTemplate' => 1));
                        $sides = sizeof($data['images']);
                        $productTemplate = $this->getProductTemplateByProductId($data['simpleproduct_id']);
                        if ($data['boundary_type'] == "single") {
                             $maskDatas = $this->getMaskData($sides);
                            $maskDatas = json_decode($maskDatas, true);
                            $printArea = array();
                            $printArea = $this->getPrintareaType($data['simpleproduct_id']);
                            foreach ($maskDatas as $key => $maskData) {
                                $maskScalewidth[$key] = $maskData['mask_width'];
                                $maskScaleHeight[$key] = $maskData['mask_height'];
                                $maskPrice[$key] = $maskData['mask_price'];
                                $scaleRatio[$key] = $maskData['scale_ratio'];
                                $scaleRatio_unit[$key] = $maskData['scaleRatio_unit'];
                            }
                            $this->customRequest(array('maskScalewidth' => $maskScalewidth, 'maskScaleHeight' => $maskScaleHeight, 'maskPrice' => $maskPrice, 'scaleRatio' => $scaleRatio, 'scaleRatio_unit' => $scaleRatio_unit, 'maskstatus' => $printArea['mask'], 'unitid' => $printArea['unit_id'], 'pricePerUnit' => $printArea['pricePerUnit'], 'maxWidth' => $printArea['maxWidth'], 'maxHeight' => $printArea['maxHeight'], 'boundsstatus' => $printArea['bounds'], 'customsizestatus' => $printArea['custom_size'], 'customMask' => $printArea['customMask']));
                            $printSizes = $this->getDtgPrintSizesOfProductSides($data['simpleproduct_id']);
                            $this->customRequest(array('productid' => $resultData['conf_id'], 'jsondata' => json_encode($maskDatas), 'printsizes' => $printSizes));
                            $this->saveMaskData();
                            if ($printSizes['status'] != 'nodata') {
                                $this->setDtgPrintSizesOfProductSides();
                            }
                        }else{
                             // multiple boundary set up
                            $multipleObj = Flight::multipleBoundary();
                            $multiBoundData = $multipleObj->getMultiBoundMaskData($resultData['old_conf_id']);
                            $multiBoundData[0]['id'] = 0;
                            $unitArr = array($multiBoundData[0]['scaleRatio_unit']);
                            $saveStatus = $multipleObj->saveMultipleBoundary($resultData['conf_id'], json_encode($multiBoundData), $unitArr, true);
                        }

                        $this->saveProductTemplateData($data['print_method_id'], $data['ref_id'], $data['simpleproduct_id'], $resultData['conf_id']);

                        if (!empty($productTemplate['tepmlate_id'])) {
                            $this->customRequest(array('pid' => $resultData['conf_id'], 'productTempId' => $productTemplate['tepmlate_id']));
                            $test = $this->addTemplateToProduct();

                        }
                    } catch (Exception $e) {
                        $result = json_encode(array('isFault' => 1, 'faultMessage' => $e->getMessage()));
                        $error = true;
                    }
                }
                echo $result;exit;
            } else {
                $msg = array('status' => 'apiLoginFailed', 'error' => json_decode($result));
                $this->response($this->json($msg), 200);
            }
        }
    }
    /**
     *
     *date created 07-06-2016(dd-mm-yy)
     *date modified (dd-mm-yy)
     *Save product template data
     *
     *@param (Int)old productid
     *@param (Int)new productid
     *@param (Int)refId
     *
     */
    public function saveProductTemplateData($printMethodId, $refId, $oldId, $newId)
    {
        $apiKey = $this->_request['apikey'];
        if ($this->isValidCall($apiKey)) {
            try {
                $msg = $this->saveProductTemplateStateRel($printMethodId,$refId,$oldId,$newId);
                return $this->json($msg);
            } catch (Exception $e) {
                $result = array('Caught exception:' => $e->getMessage());
                $this->response($this->json($result), 200);
            }
        }
    }
    /**
     * Check whether xetool is enabled or disabled
     *
     * @param   nothing
     * @return  true/false
     */
    public function checkDesignerTool($t = 0)
    {
        $result = $this->storeApiLogin();
        if ($this->storeApiLogin == true) {
            $key = $GLOBALS['params']['apisessId'];
            $filters = array(
                'store' => $this->getDefaultStoreId()
            );
            try {
                $result = $this->proxy->call($key, 'cedapi_product.checkDesignerTool', $filters);
            } catch (Exception $e) {
                $result = json_encode(array('isFault' => 1, 'faultMessage' => $e->getMessage()));
            }
            if ($t) {
                return $result;
            } else {
                return $result;
            }
        } else {
            $msg = array('status' => 'apiLoginFailed', 'error' => json_decode($result));
            $this->response($this->json($msg), 200);
        }
    }

    /**
     *
     *date created 16-05-2017(dd-mm-yy)
     *date modified (dd-mm-yy)
     *fetch all attribute
     *
     *
     */
    public function getAttributes()
    {
        $error = false;
        $result = $this->storeApiLogin();
        if ($this->storeApiLogin == true) {
            $key = $GLOBALS['params']['apisessId'];
            $filters = array(
                'store' => $this->getDefaultStoreId()
            );
            try {
                $result = $this->proxy->call($key, 'cedapi_product.getAttributes', $filters);
                if (empty($result)) {
                    $result = json_encode(array('No Records Found'));
                    $error = true;
                }
            } catch (Exception $e) {
                $result = json_encode(array('isFault' => 1, 'faultMessage' => $e->getMessage()));
                $error = true;
            }
            if (!$error) {
                $this->response($result, 200);
            } else {
                $this->response($this->json($result), 200);
            }
        } else {
            $msg = array('status' => 'apiLoginFailed', 'error' => json_decode($result));
            $this->response($this->json($msg), 200);
        }

    }
    /**
     * get custom option of a product
     *
     * @param   product id
     * @param   store id
     * @return  string of custom options
     */
    public function getCustomOption()
    {
        $error = false;
        $result = $this->storeApiLogin();
        if ($this->storeApiLogin == true) {
            $key = $GLOBALS['params']['apisessId'];
            if (!isset($this->_request['id']) || trim($this->_request['id']) == '') {
                $msg = array('status' => 'invalid id', 'id' => $this->_request['id']);
                $this->response($this->json($msg), 204); //terminate
            } else {
                $product_id = trim($this->_request['id']);
            }
            $productInfo = array(
                'productId' => $product_id,
                'store' => $this->getDefaultStoreId()
            );
            try {
                $result = $this->proxy->call($key, 'cedapi_product.getCustomOption', $productInfo);
                if (empty($result)) {
                    $result = json_encode(array('No Records Found'));
                    $error = true;
                } else {
                    $this->response($result, 200);
                }
            } catch (Exception $e) {
                $result = json_encode(array('isFault' => 1, 'faultMessage' => $e->getMessage()));
                $error = true;
            }
        } else {
            $msg = array('status' => 'apiLoginFailed', 'error' => json_decode($result));
            $this->response($this->json($msg), 200);
        }
    }

    /**
     *
     *Created By : Ramasankar
     *date created 12-10-2017(dd-mm-yy)
     *date modified (dd-mm-yy)
     *Returning List of Products from store product to getAllProducts() in products module
     *Scope : this page
     *
     *@param
     *@return Array data
     *
     */
    public function productList()
    {
        $result = $this->storeApiLogin();
        if ($this->storeApiLogin == true) {
            $key = $GLOBALS['params']['apisessId'];
            $categoryid = (isset($this->_request['categoryid']) && trim($this->_request['categoryid']) != '') ? trim($this->_request['categoryid']) : 0;
            $searchstring = (isset($this->_request['searchstring']) && trim($this->_request['searchstring']) != '') ? trim($this->_request['searchstring']) : '';
            $start = (isset($this->_request['start']) && trim($this->_request['start']) != '') ? trim($this->_request['start']) : 0;
            $limit = (isset($this->_request['range']) && trim($this->_request['range']) != '') ? trim($this->_request['range']) : 10;
            $offset = (isset($this->_request['offset']) && trim($this->_request['offset']) != '') ? trim($this->_request['offset']) : 1;
            $store = (isset($this->_request['store']) && trim($this->_request['store']) != '') ? trim($this->_request['store']) : $this->getDefaultStoreId();
            $loadVariants = (isset($this->_request['loadVariants']) && trim($this->_request['loadVariants']) == true) ? true : false;
            $preDecorated = (isset($this->_request['preDecorated']) && trim($this->_request['preDecorated']) == 'true') ? true : false;
            $preDecoStatus = (isset($this->_request['preDecoStatus']) && trim($this->_request['preDecoStatus']) == 'true') ? true : false;
            $filterArray = array('type' => array('eq' => 'configurable'));
            try {
                $filters = array(
                    'filters' => $filterArray,
                    'categoryid' => $categoryid,
                    'searchstring' => $searchstring,
                    'store' => $store,
                    'range' => array('start' => $start, 'range' => $limit),
                    'loadVariants' => $loadVariants,
                    'offset' => $offset,
                    'limit' => $limit,
                    'preDecorated' => $preDecorated,
                    'color' => $this->getStoreAttributes("xe_color"),
                    'size' => $this->getStoreAttributes("xe_size"),
                    'preDecoStatus' => $preDecoStatus
                );
                $result = $this->proxy->call($key, 'cedapi_product.getAllProducts', $filters);
                $result = json_decode($result, true);
                return $result;
            } catch (Exception $e) {
                $result = array('isFault' => 1, 'faultMessage' => $e->getMessage());
                return $result;
            }
        } else {
            $res=array('isFault' => 2, 'error' => json_encode($result));
            return $res;
        }
    }

    /**
     *
     *date created 10-10-2017(dd-mm-yy)
     *date modified (dd-mm-yy)
     *Returning category details from store to getCategories()
     *Scope : this page
     *
     *@param
     *@return Array data
     *
     */
    public function allCategories()
    {
        $result = $this->storeApiLogin();
        if ($this->storeApiLogin == true) {
            $key = $GLOBALS['params']['apisessId'];
            $filters = array(
                'store' => $this->getDefaultStoreId()
            );
            try {
                $result = $this->proxy->call($key, 'cedapi_product.getCategories', $filters); //,$filters);
                $categories = json_decode($result, true);
                return $categories['categories'];
            } catch (Exception $e) {
                $result = array('isFault' => 1, 'faultMessage' => $e->getMessage());
                return $result;
            }
        } else {
            $res=array('isFault' => 2, 'error' => json_encode($result));
            return $res;
        }
    }

    /**
     *
     *date created 27-09-2017(dd-mm-yy)
     *date modified (dd-mm-yy)
     *Returning details of a product
     *Scope : this page
     *
     *@param
     *@return Array data
     *
     */
    public function productDetails($client = '')
    {
        $result = $this->storeApiLogin();
        if ($this->storeApiLogin == true) {
            $key = $GLOBALS['params']['apisessId'];
            $productId = trim($this->_request['id']);

            if (!isset($this->_request['confId']) || trim($this->_request['confId']) == '') {
                $configProductId = 0;
            } else {
                $configProductId = trim($this->_request['confId']);
            }
            if (!isset($this->_request['size']) || trim($this->_request['size']) == '') {
                $size = '';
            } else {
                $size = trim($this->_request['size']);
            }
            $attributes = array();
            if ($size != '') {
                $attributes['size'] = $size;
            }
            $productInfo = array(
                'productId' => $productId,
                'store' => $this->getDefaultStoreId(),
                'attributes' => $attributes,
                'configId' => $configProductId,
                'color' => $this->getStoreAttributes("xe_color"),
                'size' => $this->getStoreAttributes("xe_size"),
            );

            try {
                $result = $this->proxy->call($key, 'cedapi_product.getSimpleProduct', $productInfo);
                if (empty($result)) {
                    $result = array('No Records Found');
                    return $result;
                } else {
                    $resultArr = json_decode($result);
                    return $resultArr;
                }
            } catch (Exception $e) {
                $result = array('isFault' => 1, 'faultMessage' => $e->getMessage());
                return $result;
            }
        } else {
            $res=array('isFault' => 2, 'error' => json_encode($result));
            return $res;
        }
    }

    /**
     *
     *date created 10-10-2017(dd-mm-yy)
     *date modified (dd-mm-yy)
     *Returning product print method parameter details to getPrintMethodByProduct()
     *Scope : this page
     *
     *@param
     *@return Array data
     *
     */
    public function printMethodParameters()
    {
        $result = $this->storeApiLogin();
        if ($this->storeApiLogin == true && isset($this->_request['pid']) && $this->_request['pid']) {
            $key = $GLOBALS['params']['apisessId'];
            $filters = array('store' => $this->getDefaultStoreId());
            $confProductId = $this->_request['pid'];
            $isAdmin = (isset($this->_request['isAdmin']) && trim($this->_request['isAdmin']) == true) ? true : false;
            $result = $this->proxy->call($key, 'cedapi_product.getCategoriesByProduct', $this->_request['pid'], $filters);
            $catIds = json_decode($result, true);
            $refid = '';
            $result=array('confProductId' => $confProductId, 'isAdmin' => $isAdmin, 'catIds' => $catIds, 'refid' => $refid);
            return $result;
        } else {
            $res=array('isFault' => 2, 'error' => $result);
            return $res;
        }
    }

     /**
     *
     *date created 27-09-2017(dd-mm-yy)
     *date modified (dd-mm-yy)
     *Returning size and quantity of a product
     *
     *@param
     *@return Array data
     *
     */
    public function sizeAndQuantityDetails()
    {
        $result = $this->storeApiLogin();
        if ($this->storeApiLogin == true) {
            $key = $GLOBALS['params']['apisessId'];
            $productId = trim($this->_request['productId']);
            $varientId = trim($this->_request['simplePdctId']);

            if (isset($this->_request['byAdmin'])) {
                $byAdmin = true;
            } else {
                $byAdmin = false;
            }
            $productInfo = array(
                'productId' => $productId,
                'store' => $this->getDefaultStoreId(),
                'simpleProductId' => $varientId,
                'color' => $this->getStoreAttributes("xe_color"),
                'size' => $this->getStoreAttributes("xe_size")
            );

            try {
                if (!$byAdmin) {
                    $result = $this->proxy->call($key, 'cedapi_product.getSizeAndQuantity', $productInfo);
                } else {
                    $result = $this->proxy->call($key, 'cedapi_product.getSizeVariants', $productInfo);
                }
                $resultArr = json_decode($result,true);
                $resultArr = $resultArr['quantities'];
                return $resultArr;

            } catch (Exception $e) {
                $result = array('isFault' => 1, 'faultMessage' => $e->getMessage());
                return $result;
            }

        } else {
            $res=array('isFault' => 2, 'error' => json_encode($result));
            return $res;
        }
    }

    /**
     *
     *date created 27-09-2017(dd-mm-yy)
     *date modified (dd-mm-yy)
     *Returning Veriant Details
     *Scope : this page
     *
     *@param
     *@return Array data
     *
     */
    public function variantDetails()
    {
        $start = 0; // default values
        if (isset($this->_request['start']) && trim($this->_request['start']) != '') {
            $start = trim($this->_request['start']);
        }
        if (isset($this->_request['range']) && trim($this->_request['range']) != '' && trim($this->_request['range']) != 0) {
            $limit = trim($this->_request['range']);
        } else {
            $limit = 0;
        }
        $offset = (isset($this->_request['offset']) && trim($this->_request['offset']) != '') ? trim($this->_request['offset']) : 1;
        $store = (isset($this->_request['store']) && trim($this->_request['store']) != '') ? trim($this->_request['store']) : $this->getDefaultStoreId();
        $result = $this->storeApiLogin();
        $confId = $this->_request['conf_pid'];
        if ($this->storeApiLogin == true) {
            $key = $GLOBALS['params']['apisessId'];
            try {
                $filters = array(
                    'confId' => $confId,
                    'start' => $start,
                    'limit' => $limit,
                    'store' => $store,
                    'offset' => $offset,
                    'color' => $this->getStoreAttributes("xe_color"),
                    'size' => $this->getStoreAttributes("xe_size"),
                );
                $result = $this->proxy->call($key, 'cedapi_product.getVariants', $filters);
                $catIds = '';
                //convert json value into inkxe array
                $resultArr = json_decode($result,true);
                $pCount =  $resultArr['count'];
                $resultArr = $resultArr['variants'];

                $result = array('variants' => $resultArr, 'count' => $pCount);
                return $result;
            } catch (Exception $e) {
                $resultArr = array('isFault' => 1, 'faultMessage' => $e->getMessage());
                return $resultArr;
            }
        } else {
            $res=array('isFault' => 2, 'error' => json_encode($result));
            return $res;
        }
    }
}