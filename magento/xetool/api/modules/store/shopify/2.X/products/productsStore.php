<?php

/* Check Un-authorize Access */
if (!defined('accessUser')) {
    die("Error");
}

class ProductsStore extends UTIL {
    ####################################################
    ############## syncOrdersZip ###################
    ####################################################

    public function editSwachColor() {
        $error = '';
        $swatches = array();
        $result = $this->storeApiLogin();
        if ($this->storeApiLogin == true) {
            $key = $GLOBALS['params']['apisessId'];
            if (isset($this->_request['option_id']) && trim($this->_request['option_id']) != '') {
                $option_id = $this->_request['option_id'];
            }
            if (isset($this->_request['colorname']) && trim($this->_request['colorname']) != '') {
                $colorname = $this->_request['colorname'];
            }
            if (isset($this->_request['imagename']) && trim($this->_request['imagename']) != '') {
                $imagename = trim($this->_request['imagename']);
            }
            if (isset($this->_request['imagetype']) && trim($this->_request['imagetype']) != '') {
                $imagetype = trim($this->_request['imagetype']);
            }
            try {
                if (isset($imagename) && $imagename != '') {
                    $swatchObj = Flight::colorSwatch();
                    $swatchObj->customRequest(array('value' => str_replace(' ', '_', $colorname), 'imgData' => $imagename, 'imagetype' => $imagetype, 'swatchWidth' => 45, 'swatchHeight' => 45, 'base64Data' => base64_decode($imagename)));
                    $saveSucss = $swatchObj->saveColorSwatch('add');
                    $rsultrsponse['attribute_id'] = str_replace(' ', '_', $colorname);
                    $rsultrsponse['attribute_value'] = str_replace(' ', '_', $colorname);
                    $rsultrsponse['status'] = 'success';
                    $rsultrsponse['swatchImage'] = $saveSucss['swatchImage'];
                    $rsultrsponse['hexCode'] = $saveSucss['hexCode'];
                    // update swatch image details
                }
                $result = json_encode($rsultrsponse);
            } catch (Exception $e) {
                $result = json_encode(array('isFault' => 1, 'faultMessage' => $e->getMessage()));
                $error = true;
            }
            if (!$error) {
                print_r($result);
                exit();
            } else {
                print_r(json_decode($result));
                exit();
            }
        } else {
            $msg = array('status' => 'apiLoginFailed', 'error' => json_decode($result));
            $this->response($this->json($msg), 200);
        }
    }

    ####################################################
    ############## syncOrdersZip ###################
    ####################################################

    public function getProductById() {
        $error = false;
        $result = $this->storeApiLogin();
        if ($this->storeApiLogin == true) {
            $key = $GLOBALS['params']['apisessId'];
            if (!isset($this->_request['id']) || trim($this->_request['id']) == '') {
                // $product_id = 1;
                $msg = array('status' => 'invalid id', 'id' => $this->_request['id']);
                $this->response($this->json($msg), 204); //terminate
            } else {
                $product_id = trim($this->_request['id']);
            }
            if (!isset($this->_request['size']) || trim($this->_request['size']) == '') {
                $size = '';
            } else {
                $size = trim($this->_request['size']);
            }
            if (!isset($this->_request['simplePdctId']) || trim($this->_request['simplePdctId']) == '') {
                $varient_id = '';
            } else {
                $varient_id = trim($this->_request['simplePdctId']);
            }
            if (!isset($this->_request['start']) || trim($this->_request['start']) == '') {
                $start = 0;
            } else {
                $start = trim($this->_request['start']);
            }
            if (!isset($this->_request['limit']) || trim($this->_request['limit']) == '') {
                $limit = 50;
            } else {
                $limit = trim($this->_request['limit']);
            }
            $attributes = array();
            $simpleProductId = '';
            if ($size != '') {
                $attributes['size'] = $size;
            }

            if ($varient_id != '') {
                $simpleProductId = $varient_id;
            }

            $productInfo = array(
                'productId' => $product_id,
                'store' => $this->getDefaultStoreId(),
                'attributes' => $attributes,
                'simpleProductId' => $simpleProductId,
                'start' => $start,
                'limit' => $limit,
            );
            if (!$error) {
                try {
                    $result = $this->proxy->call($key, 'cedapi_product.getProductById', $productInfo);
                    $resultArr = json_decode($result);
                    $this->_request['productid'] = $product_id; //Mask Info
                    $this->_request['returns'] = true; //Mask Info
                    $maskInfo = $this->getMaskData();
                    $resultArr->product->maskInfo = json_decode($maskInfo);
                    $printsize = $this->getDtgPrintSizesOfProductSides($product_id);
                    $resultArr->product->printsize = $printsize;
                    $printareatype = $this->getPrintareaType($product_id);
                    $resultArr->product->printareatype = $printareatype;
                    $cVariants = $resultArr->product->variants;
                    $cVariantsIds = array();
                    for ($i = 0; $i < sizeof($cVariants); $i++) {
                        array_push($cVariantsIds, $cVariants[$i]->data->id);
                    }
                    $additionalprices = $this->getAdditionalPrintingPriceOfVariants($product_id, $cVariantsIds);
                    $resultArr->product->additionalprices = $additionalprices;
                    $pCategories = $resultArr->product->category;
                    $pCategoryIds = array();
                    for ($i = 0; $i < sizeof($pCategories); $i++) {
                        array_push($pCategoryIds, $pCategories[$i]);
                    }
                    $features = $this->fetchProductFeatures($product_id, $pCategoryIds);
                    $resultArr->product->features = $features;
                    $result = json_encode($resultArr);
                } catch (Exception $e) {
                    $result = json_encode(array('isFault' => 1, 'faultMessage' => $e->getMessage()));
                    $error = true;
                }
            }
            $this->closeConnection();
            print_r($result);
            exit();
        } else {
            $msg = array('status' => 'apiLoginFailed', 'error' => json_decode($result));
            $this->response($this->json($msg), 200);
        }
    }

    ####################################################
    ############## syncOrdersZip ###################
    ####################################################

    private function fetchProductFeatures($productId, $productCategoryIdArray) {
        $apiKey = $this->_request['apikey'];
        if ($this->isValidCall($apiKey)) {
            $featureIdsArray = array();
            $featureNamesArray = array();
            $featureTypesArray = array();
            $result = array();
            $sql = "SELECT id,name,type FROM " . TABLE_PREFIX . "features WHERE mandatory_status=0 && status=1 && product_level_status=1";
            $featuresFromValue = mysqli_query($this->db, $sql);
            while ($row = mysqli_fetch_array($featuresFromValue)) {
                array_push($featureIdsArray, $row['id']);
                array_push($featureNamesArray, $row['name']);
                array_push($featureTypesArray, $row['type']);
            }
            $productFeaturesStatusData = array();
            $sql = "SELECT feature_id FROM " . TABLE_PREFIX . "product_feature_rel WHERE product_id= ? && status = 1";
            $params = array();
            $params[0] = 's';
            $params[] = &$productId;
            $stmt = $this->db->prepare($sql);
            call_user_func_array([$stmt, 'bind_param'], $params);
            $stmt->execute();
            $productFeatures = $stmt->get_result();
            if (count($productFeatures) > 0) {
                foreach ($productFeatures as $row) {
                    $productFeatureId = $row['feature_id'];
                    $key = array_search($productFeatureId, $featureIdsArray);
                    $productFeature = $featureTypesArray[$key];
                    array_push($result, $productFeature);
                }
            } else {
                $categoryFeaturesList = array();
                for ($j = 0; $j < sizeof($productCategoryIdArray); $j++) {
                    $sql = "SELECT feature_id FROM " . TABLE_PREFIX . "productcategory_feature_rel WHERE product_category_id= ? && status = 1";
                    $params = array();
                    $params[0] = 's';
                    $params[] = &$productCategoryIdArray[$j];
                    $stmt = $this->db->prepare($sql);
                    call_user_func_array([$stmt, 'bind_param'], $params);
                    $stmt->execute();
                    $categoryFeatures = $stmt->get_result();
                    if (count($categoryFeatures) > 0) {
                        foreach ($categoryFeatures as $row) {
                            $categoryFeatureId = $row['feature_id'];
                            $key = array_search($categoryFeatureId, $featureIdsArray);
                            $categoryFeature = $featureTypesArray[$key];
                            if (!in_array($categoryFeature, $categoryFeaturesList)) {
                                array_push($result, $productFeature);
                            }
                            if (sizeof($categoryFeaturesList) == sizeof($featureTypesArray)) {
                                break;
                            }
                        }
                    }
                }
                $result = $categoryFeaturesList;
            }
            return $result;
        } else {
            $msg = array("status" => "invalid");
            return $this->json($msg);
        }
    }

    /**
     *
     * date created (dd-mm-yy)
     * date modified 15-4-2016(dd-mm-yy)
     * fetch print method id and name
     *
     * @param (String)apikey
     * @param (int)productid
     * @return json data
     *
     */
    public function getProductPrintMethod() {
        $productId = $this->_request['productid'];
        $key = $this->_request['apikey'];
        if (!empty($productId)) {
// &&  !empty($key) && $this->isValidCall($key)){
            $error = false;
            $productPrintTypeSql = "SELECT distinct pm.pk_id, pm.name FROM " . TABLE_PREFIX . "print_method pm
            INNER JOIN " . TABLE_PREFIX . "product_printmethod_rel ppr ON ppr.print_method_id=pm.pk_id
            JOIN " . TABLE_PREFIX . "print_setting AS pst ON pm.pk_id=pst.pk_id WHERE ppr.product_id=?";
            $params = array();
            $params[] = 's';
            $params[] = &$productId;
            $productPrintType = $this->executePrepareBindQuery($productPrintTypeSql, $params);
            if (!empty($productPrintType)) {
                //$this->log('productPrintTypeSql: '.$productPrintTypeSql, true, 'Zsql.log');
                foreach ($productPrintType as $k2 => $v2) {
                    $printDetails[$k2]['print_method_id'] = $v2['pk_id'];
                    $printDetails[$k2]['name'] = $v2['name'];
                }
            } else {
                $result = $this->storeApiLogin();
                if ($this->storeApiLogin == true) {
                    $key = $GLOBALS['params']['apisessId'];
                    try {
                        $filters = array(
                            'productid' => $productId,
                            'store' => $this->getDefaultStoreId(),
                        );
                        $result = $this->proxy->call($key, 'cedapi_product.getCategoriesByProduct', $filters);
                        $catIds = json_decode($result);
                        //$lencatid = count(trim($catIds));
                        $catIds = implode(',', (array) $catIds);
                        $catSql = 'SELECT DISTINCT pm.pk_id, pm.name
                            FROM ' . TABLE_PREFIX . 'product_category_printmethod_rel AS pcpml
                            JOIN ' . TABLE_PREFIX . 'print_method AS pm ON pm.pk_id = pcpml.print_method_id
                            JOIN ' . TABLE_PREFIX . 'print_setting AS pst ON pm.pk_id=pst.pk_id
                            LEFT JOIN ' . TABLE_PREFIX . 'print_method_setting_rel pmsr ON pst.pk_id=pmsr.print_setting_id
                            WHERE pcpml.product_category_id IN (' . $catIds . ')';
                        $params = array();
                        $rows = $this->executePrepareBindQuery($catSql, $params, 'assoc');
                        $printDetails = array();
                        if (empty($rows)) {
                            $default_print_type = "SELECT pm.pk_id,pm.name
                            from " . TABLE_PREFIX . "print_method AS pm
                            JOIN " . TABLE_PREFIX . "print_setting ps ON pm.pk_id=ps.pk_id
                            LEFT JOIN " . TABLE_PREFIX . "print_method_setting_rel pmsr ON ps.pk_id=pmsr.print_setting_id
                            WHERE ps.is_default='1' AND pm.is_enable='1' AND ps.is_default='1'";
                            $params = array();
                            $res = $this->executePrepareBindQuery($default_print_type, $params, 'assoc');
                            $printDetails[0]['print_method_id'] = $res[0]['pk_id'];
                            $printDetails[0]['name'] = $res[0]['name'];
                        } else {
                            foreach ($rows as $k1 => $v1) {
                                $printDetails[$k1]['print_method_id'] = $v1['pk_id'];
                                $printDetails[$k1]['name'] = $v1['name'];
                            }
                        }
                    } catch (Exception $e) {
                        $result = json_encode(array('isFault' => 1, 'faultMessage' => $e->getMessage()));
                        $error = true;
                    }
                } else {
                    $msg = array('status' => 'apiLoginFailed', 'error' => json_decode($result));
                    $this->response($this->json($msg), 200);
                }
            }
            if (!$error) {
                $resultArr = $printDetails;
                $result = json_encode($resultArr);
                $this->response($this->json($resultArr), 200);
                //print_r($result);
            } else {
                print_r($result);
                exit();
            }
        } else {
            $msg = array("status" => "invalid Product Id");
            $this->response($this->json($msg), 200);
        }
    }

    /**
     * Check whether the given sku exists or doesn't
     *
     * @param   $sku_arr
     * @return  true/false
     */
    public function checkDuplicateSku() {
// chk for storeid
        $error = false;
        $result = $this->storeApiLogin();
        if (!empty($this->_request) && $this->storeApiLogin == true) {
            $key = $GLOBALS['params']['apisessId'];
            if (!$error) {
                $filters = array(
                    'sku_arr' => $this->_request['sku_arr'],
                    'store' => $this->getDefaultStoreId(),
                );
                try {
                    $result = $this->proxy->call($key, 'cedapi_product.checkDuplicateSku', $filters);
                } catch (Exception $e) {
                    $result = json_encode(array('isFault: ' => 1, 'faultMessage' => $e->getMessage()));
                    $error = true;
                }
            }
            $this->closeConnection();
            print($result);
            exit();
        } else {
            $msg = array('status' => 'apiLoginFailed', 'error' => json_decode($result));
            $this->response($this->json($msg), 200);
        }
    }

    /**
     * Used to get all the xe_size inside shopify
     *
     * @param   nothing
     * @return  array contains all the xe_size inside store
     */
    public function getSizeArr() {
        $error = '';
        $result = $this->storeApiLogin();
        if ($this->storeApiLogin == true) {
            $key = $GLOBALS['params']['apisessId'];
            $filters = array(
                'store' => $this->getDefaultStoreId(),
            );
            try {
                $result = $this->proxy->call($key, 'cedapi_product.getSizeArr', $filters);
                //$result = $proxy->call($key, 'catalog_category.tree');
            } catch (Exception $e) {
                $result = json_encode(array('isFault' => 1, 'faultMessage' => $e->getMessage()));
                $error = true;
            }
            if (!$error) {
                $categories = array();
                print_r($result);
                exit();
                exit();
            } else {
                print_r(json_decode($result));
                exit();
                exit();
            }
        } else {
            $msg = array('status' => 'apiLoginFailed', 'error' => json_decode($result));
            $this->response($this->json($msg), 200);
        }
    }

    /**
     * Used to get all the xe_color inside shopify
     *
     * @param   nothing
     * @return  array contains all the xe_color inside store
     */
    public function getColorArr($isSameClass = false) {
        $error = '';
        $result = $this->storeApiLogin();
        if ($this->storeApiLogin == true) {
            $key = $GLOBALS['params']['apisessId'];
            try {
                $filter = array('lastLoaded' => $this->_request['lastLoaded'], 'loadCount' => $this->_request['loadCount'], 'store' => $this->getDefaultStoreId(), 'productId' => $this->_request['productId']);
                $result = $this->proxy->call($key, 'cedapi_product.getColorArr', $filter);
            } catch (Exception $e) {
                $result = json_encode(array('isFault' => 1, 'faultMessage' => $e->getMessage()));
                $error = true;
            }
            if (!$error) {
                if ($isSameClass) {
                    return $result;
                } else {
                    print_r($result);
                    exit();
                }
            } else {
                print_r(json_decode($result));
                exit();
            }
        } else {
            $msg = array('status' => 'apiLoginFailed', 'error' => json_decode($result));
            $this->response($this->json($msg), 200);
        }
    }

    /**
     *
     * @return type json
     */
    public function checkCustomProduct() {
        try {
            $error = false;
            $result = $this->storeApiLogin();
            if ($this->storeApiLogin == true) {
                $key = $GLOBALS['params']['apisessId'];
                $pid = $this->_request['pid'];
                $result = $this->proxy->call($key, 'cedapi_product.checkCustomProduct', $pid);
                if (!$result) {
                    $productQry = "SELECT template_type FROM " . TABLE_PREFIX . "predeco_cat_rel WHERE product_id = ?";
                    $params = array();
                    $params[] = 's';
                    $params[] = &$pid;
                    $productInfo = $this->executePrepareBindQuery($productQry, $params, 'assoc');
                    $productState = ($productInfo[0]['template_type'] == 0 ? "with_image" : "without_image");
                    $productInfo = array('product_type' => $productState);
                    print_r(json_encode($productInfo));
                    exit();
                }
                print_r($result);
                exit();
            } else {
                $msg = array('status' => 'apiLoginFailed', 'error' => json_decode($result));
            }
            $this->response($this->json($msg), 200);
        } catch (Exception $e) {
            $result = array('Caught exception:' => $e->getMessage());
            $this->response($result, 200);
        }
    }

    /**
     *
     * @return type json
     */
    public function editCustomProduct() {
        try {
            $error = false;
            $result = $this->storeApiLogin();
            if ($this->storeApiLogin == true) {
                $key = $GLOBALS['params']['apisessId'];
                $pid = $this->_request['pid'];
                $isDelete = ($this->_request['delete'] ? $this->_request['delete'] : 0);
                $product = array(
                    "product_id" => $pid,
                    "isDelete" => $isDelete,
                );
                $result = $this->proxy->call($key, 'cedapi_product.editCustomProduct', $product);
            } else {
                $msg = array('status' => 'apiLoginFailed', 'error' => json_decode($result));
            }
            $this->response($this->json($msg), 200);
        } catch (Exception $e) {
            $result = array('Caught exception:' => $e->getMessage());
            $this->response($this->json($result), 200);
        }
    }

    /**
     *
     * @return type json
     */
    public function clearCustomProducts() {
        try {
            $error = false;
            $result = $this->storeApiLogin();
            if ($this->storeApiLogin == true) {
                $key = $GLOBALS['params']['apisessId'];
                $interval = $this->_request['interval'];
                $result = $this->proxy->call($key, 'cedapi_product.clearCustomProducts', $interval);
            } else {
                $msg = array('status' => 'apiLoginFailed', 'error' => json_decode($result));
            }
            $this->response($this->json($msg), 200);
        } catch (Exception $e) {
            $result = array('Caught exception:' => $e->getMessage());
            $this->response($this->json($result), 200);
        }
    }

    /**
     *
     * date created (dd-mm-yy)
     * date modified 15-4-2016(dd-mm-yy)
     * Fetch color swatches
     *
     * @param (String)apikey
     * @return json data
     *
     */
    public function fetchColorSwatch() {
        $isSameClass = true;
        $colorOptionsArr = $this->getColorArr($isSameClass);
        if (!is_array($colorOptionsArr)) {
            $colorOptionsArr = json_decode($colorOptions);
        }
        $dir = $this->getSwatchURL();
        $filePath = $this->getSwatchesPath();
        $colorOptions = array();
        $cii = 0;
        foreach ($colorOptionsArr as $ci => $value) {
            $key = $value['label'];
            $colorOptions[$cii] = new stdClass();
            $colorOptions[$cii]->value = $value['label'];
            $colorOptions[$cii]->label = $value['label'];
            // $swatchImageFile = $filePath.'/'.$value->value.'.png';
            $swatchFilePath = $filePath . '/45x45/' . $key . '.png';
            $swatchFileDir = $dir . '45x45/' . $key . '.png';
            if (file_exists($swatchFilePath)) {
                //$colorOptions[$key]->swatchImage = $dir.$value->value.'.png';
                $colorOptions[$cii]->swatchImage = $swatchFileDir;
                $colorOptions[$cii]->hexCode = '';
            } else {
                $colorOptions[$cii]->swatchImage = '';
                $sql = "select hex_code from " . TABLE_PREFIX . "swatches where attribute_id =?";
                $params = array();
                $params[] = 's';
                $params[] = &$value['label'];
                $row = $this->executePrepareBindQuery($sql, $params);
                $colorOptions[$cii]->hexCode = $row[0]['hex_code'];
            }
            $colorOptions[$cii]->width = 45;
            $colorOptions[$cii]->height = 45;
            $cii++;
        }
        $this->response(json_encode($colorOptions), 200);
    }

    /**
     *
     * date created 31-05-2016(dd-mm-yy)
     * date modified (dd-mm-yy)
     * Add template as product
     *
     *
     */
    public function addTemplateProducts() {
        $error = false;
        if (!empty($this->_request['data'])) {
            $data = json_decode(urldecode($this->_request['data']), true);
            $apikey = $this->_request['apikey'];
            $result = $this->storeApiLogin();
            if ($this->storeApiLogin == true) {
                $key = $GLOBALS['params']['apisessId'];
                if (!$error) {
                    try {
                        // $productsObj = Flight::products();
                        // $predecorateObj = Flight::predecorate();
                        // $templateObj = Flight::template();
                        $arr = array('store' => $this->getDefaultStoreId(), 'data' => $data, 'configFile' => $data['images'], 'oldConfId' => $data['simpleproduct_id'], 'varColor' => $data['color_id'], 'varSize' => $data['sizes']);
                        $result = $this->proxy->call($key, 'cedapi_product.addTemplateProducts', $arr);
                        $resultData = json_decode($result, true);
                        $predecorateObj = Flight::predecorate();
                        $templateData = $predecorateObj->saveTemplateInfo($resultData['conf_id'], $data['assignedCategory'], $data['captureType']);
                        $this->customRequest(array('productid' => $data['simpleproduct_id'], 'isTemplate' => 1));
                        $sides = sizeof($data['images']);
                        $productTemplate = $this->getProductTemplateByProductId($data['simpleproduct_id']);
                        if ($data['boundary_type'] == "single") {
                            $maskData = $this->getMaskData($sides);
                            $maskDatas = json_decode($maskData, true);
                            $printArea = array();
                            $printArea = $this->getPrintareaType($data['simpleproduct_id']);
                            foreach ($maskDatas as $key => $maskData) {
                                $maskScalewidth[$key] = $maskData['mask_width'];
                                $maskScaleHeight[$key] = $maskData['mask_height'];
                                $maskPrice[$key] = $maskData['mask_price'];
                                $scaleRatio[$key] = $maskData['scale_ratio'];
                                $scaleRatio_unit[$key] = $maskData['scaleRatio_unit'];
                            }
                            $this->customRequest(array('maskScalewidth' => $maskScalewidth, 'maskScaleHeight' => $maskScaleHeight, 'maskPrice' => $maskPrice, 'scaleRatio' => $scaleRatio, 'scaleRatio_unit' => $scaleRatio_unit, 'maskstatus' => $printArea['mask'], 'unitid' => $printArea['unit_id'], 'pricePerUnit' => $printArea['pricePerUnit'], 'maxWidth' => $printArea['maxWidth'], 'maxHeight' => $printArea['maxHeight'], 'boundsstatus' => $printArea['bounds'], 'customsizestatus' => $printArea['custom_size'], 'customMask' => $printArea['customMask']));
                            $printSizes = $this->getDtgPrintSizesOfProductSides($data['simpleproduct_id']);

                            $printSizes = $this->getDtgPrintSizesOfProductSides($data['simpleproduct_id']);
                            $this->customRequest(array('productid' => $resultData['conf_id'], 'jsondata' => json_encode($maskDatas), 'printsizes' => $printSizes));

                            $this->saveMaskData();
                            if ($printSizes['status'] != 'nodata') {
                                $this->setDtgPrintSizesOfProductSides();
                            }
                        } else {
                            // multiple boundary set up
                            $multipleObj = Flight::multipleBoundary();
                            $multiBoundData = $multipleObj->getMultiBoundMaskData($resultData['old_conf_id']);
                            $multiBoundData[0]['id'] = 0;
                            $unitArr = array($multiBoundData[0]['scaleRatio_unit']);
                            $saveStatus = $multipleObj->saveMultipleBoundary($resultData['conf_id'], json_encode($multiBoundData), $unitArr, true);
                        }

                        $this->saveProductTemplateData($data['print_method_id'], $data['ref_id'], $data['simpleproduct_id'], $resultData['conf_id']);

                        if (!empty($productTemplate['tepmlate_id'])) {
                            $this->customRequest(array('pid' => $resultData['conf_id'], 'productTempId' => $productTemplate['tepmlate_id']));
                            $test = $this->addTemplateToProduct();
                        }
                    } catch (Exception $e) {
                        $result = json_encode(array('isFault' => 1, 'faultMessage' => $e->getMessage()));
                        $error = true;
                    }
                }
                echo $result;
                exit;
            } else {
                $msg = array('status' => 'apiLoginFailed', 'error' => json_decode($result));
                $this->response($this->json($msg), 200);
            }
        }
    }

    ####################################################
    ########## Not used till now (future use) ##########
    ####################################################

    public function addProducts() {
        $error = false;
        if (!empty($this->_request['data']) && !empty($_FILES['simpleFile'])) {
            $data = json_decode($this->_request['data'], true);
            $this->_request['apikey'] = $data['apikey'];
            $result = $this->storeApiLogin();
            if ($this->storeApiLogin == true) {
                $key = $GLOBALS['params']['apisessId'];
                if (!$error) {
                    try {
                        $arr = array('store' => $this->getDefaultStoreId(), 'data' => $data, 'configFile' => $_FILES['configFile'], 'simpleFile' => $_FILES['simpleFile']);
                        $result = $this->proxy->call($key, 'cedapi_product.addProducts', $arr);
                        //$resultArr = json_decode($result,true);
                    } catch (Exception $e) {
                        $result = json_encode(array('isFault' => 1, 'faultMessage' => $e->getMessage()));
                        $error = true;
                    }
                }
                echo $result;
                exit();
                exit();
            } else {
                $msg = array('status' => 'apiLoginFailed', 'error' => json_decode($result));
                $this->response($this->json($msg), 200);
            }
        }
    }

    ####################################################
    ############## syncOrdersZip ###################
    ####################################################

    public function getsubCategories() {
        $error = '';
        $result = $this->storeApiLogin();
        if ($this->storeApiLogin == true) {
            $key = $GLOBALS['params']['apisessId'];
            try {
                $filters = array('catid' => $this->_request['selectedCategory'], 'store' => $this->getDefaultStoreId());
                $result = $this->proxy->call($key, 'cedapi_product.getsubCategories', $filters);
            } catch (Exception $e) {
                $result = json_encode(array('isFault' => 1, 'faultMessage' => $e->getMessage()));
                $error = true;
            }
            if (!$error) {
                $categories = array();
                print_r($result);
                exit();
            } else {
                print_r(json_decode($result));
                exit();
            }
        } else {
            $msg = array('status' => 'apiLoginFailed', 'error' => json_decode($result));
            $this->response($this->json($msg), 200);
        }
    }

    /**
     * Check whether xetool is enabled or disabled
     *
     * @param   nothing
     * @return  true/false
     */
    public function checkDesignerTool($t = 0) {
        $result = $this->storeApiLogin();
        if ($this->storeApiLogin == true) {
            $key = $GLOBALS['params']['apisessId'];
            $filters = array(
                'store' => $this->getDefaultStoreId(),
            );
            try {
                $result = $this->proxy->call($key, 'cedapi_product.checkDesignerTool', $filters);
            } catch (Exception $e) {
                $result = json_encode(array('isFault' => 1, 'faultMessage' => $e->getMessage()));
            }
            if ($t) {
                return $result;
            } else {
                print_r($result);
            }
            exit();
        } else {
            $msg = array('status' => 'apiLoginFailed', 'error' => json_decode($result));
            $this->response($this->json($msg), 200);
        }
    }

    ####################################################
    ############## syncOrdersZip ###################
    ####################################################

    public function getVariantList() {
        $error = false;
        $resultArr = $this->storeApiLogin();
        if ($this->storeApiLogin == true) {
            $key = $GLOBALS['params']['apisessId'];
            try {
                $confId = $this->_request['conf_pid'];
                $filters = array(
                    'confId' => $confId,
                    'store' => $this->getDefaultStoreId(),
                );
                $resultArr = $this->proxy->call($key, 'cedapi_product.getVariantList', $filters);
            } catch (Exception $e) {
                $resultArr = json_encode(array('isFault' => 1, 'faultMessage' => $e->getMessage()));
                $error = true;
            }
            if (!$error) {
                print_r($resultArr);
                exit();
            } else {
                print_r(json_decode($resultArr));
                exit();
            }
        } else {
            $msg = array('status' => 'apiLoginFailed', 'error' => json_decode($resultArr));
            $this->response($this->json($msg), 200);
        }
    }

    ####################################################
    ############## syncOrdersZip ###################
    ####################################################

    public function getProductCount() {
        $error = false;
        $result = $this->storeApiLogin();
        if ($this->storeApiLogin == true) {
            $key = $GLOBALS['params']['apisessId'];
            $filterArray = array('type' => array('eq' => 'configurable'));
            try {
                $filters = array(
                    'filters' => $filterArray,
                    'store' => $this->getDefaultStoreId(),
                );
                $result = $this->proxy->call($key, 'cedapi_product.getProductCount', $filters);
            } catch (Exception $e) {
                $result = json_encode(array('isFault' => 1, 'faultMessage' => $e->getMessage()));
                $error = true;
            }
            if (!$error) {
                print_r($result);
                exit();
            } else {
                print_r(json_decode($result));
                exit();
            }
        } else {
            $msg = array('status' => 'apiLoginFailed', 'error' => json_decode($result));
            $this->response($this->json($msg), 200);
        }
    }

    /**
     *
     * date created 07-06-2016(dd-mm-yy)
     * date modified (dd-mm-yy)
     * Save product template data
     *
     * @param (Int)old productid
     * @param (Int)new productid
     * @param (Int)refId
     *
     */
    public function saveProductTemplateData($printMethodId, $refId, $oldId, $newId) {
        $apiKey = $this->_request['apikey'];
        if ($this->isValidCall($apiKey)) {
            try {
                $sql = "delete from " . TABLE_PREFIX . "template_state_rel where temp_id=?";
                $params = array();
                $params[] = 's';
                $params[] = &$newId;
                $result = $this->executePrepareBindQuery($sql, $params, 'dml');
                $sql = "delete from " . TABLE_PREFIX . "product_printmethod_rel where product_id=?";
                $params = array();
                $params[] = 's';
                $params[] = &$newId;
                $result = $this->executePrepareBindQuery($sql, $params, 'dml');
                $values = '';
                $pValues = '';
                $status = 0;
                $values .= ",(?,?,?)";
                $params1 = array('sss', &$refId, &$newId, &$oldId);
                $pValues .= ",(?,?)";
                $params2 = array('ss', &$newId, &$printMethodId);
                if (strlen($values)) {
                    $sql = "INSERT INTO " . TABLE_PREFIX . "template_state_rel (ref_id,temp_id,parent_id) VALUES" . substr($values, 1);
                    $status = $this->executePrepareBindQuery($sql, $params1, 'dml');
                }
                if (strlen($pValues)) {
                    $sql = "INSERT INTO " . TABLE_PREFIX . "product_printmethod_rel (product_id,print_method_id) VALUES" . substr($pValues, 1);
                    $status = $this->executePrepareBindQuery($sql, $params2, 'dml');
                }
                if ($status) {
                    $msg = array("status" => "success");
                } else {
                    $msg = array("status" => "failed");
                }
                return $this->json($msg);
            } catch (Exception $e) {
                $result = array('Caught exception:' => $e->getMessage());
                $this->response($this->json($result), 200);
            }
        }
    }

    /**
     *
     * date created 4-1-2016(dd-mm-yy)
     * date modified 14-4-2016(dd-mm-yy)
     * separaetd svg created for name and number by group element
     *
     * @param (String)refids
     * @param (String)svgUrl
     * @param (String)resultSvg
     * @param (String)index
     * @return json data
     *
     */
    public function CheckPreDecoProduct($pid) {
        if (!isset($this->_request['pid']) || trim($this->_request['pid']) == '') {
            $configProduct_id = $pid;
        } else {
            $configProduct_id = trim($this->_request['pid']);
        }
    }

    /**
     *
     * date created 23-05-2017(dd-mm-yy)
     * date modified (dd-mm-yy)
     * fetch all attribute
     *
     *
     */
    public function getAttributes() {
        $error = false;
        $result = $this->storeApiLogin();
        if ($this->storeApiLogin == true) {
            try {
                $dimensionQuery = "SELECT * FROM " . TABLE_PREFIX . "store_attributes WHERE attr_key ='dimension' ";
                $params = array();
                $dimension = $this->executePrepareBindQuery($dimensionQuery, $params, 'assoc');
                if (!empty($dimension)) {
                    $result['is_custom_height_width'] = true;
                    $result['attr_sort_order'] = $dimension[0]['attr_sort_order'];
                } else {
                    $result['is_custom_height_width'] = false;
                    $result['attr_sort_order'] = "";
                }
            } catch (Exception $e) {
                $result = json_encode(array('isFault' => 1, 'faultMessage' => $e->getMessage()));
                $error = true;
            }
            if (!$error) {
                print_r(json_encode($result));
                exit;
            } else {
                $this->response($this->json($result), 200);
            }
        } else {
            $msg = array('status' => 'apiLoginFailed', 'error' => json_decode($result));
            $this->response($this->json($msg), 200);
        }
    }

    /**
     *
     * date created 23-05-2017(dd-mm-yy)
     * date modified (dd-mm-yy)
     * Save attribute data (the setting to use size attribute as dimension)
     *
     *
     */
    public function saveAttributes() {
        $error = false;
        $result = $this->storeApiLogin();
        if ($this->storeApiLogin == true) {
            try {
                $isEnabled = $this->_request['s_custom_height_width'];
                $format = $this->_request['sort_order'];
                $removeDimenQry = "DELETE FROM " . TABLE_PREFIX . "store_attributes WHERE `attr_key` = 'dimension'";
                $params = array();
                $removeDimension = $this->executePrepareBindQuery($removeDimenQry, $params, 'dml');
                if ($isEnabled) {
                    $dimensionQuery = "INSERT INTO `store_attributes` SET `attr_key` = 'dimension', `attr_value` = 'size', `attr_sort_order` = ?";
                    $params = array();
                    $params[] = 's';
                    $params[] = &$format;
                    $newDimension = $this->executePrepareBindQuery($dimensionQuery, $params, 'insert');
                }
            } catch (Exception $e) {
                $result = json_encode(array('isFault' => 1, 'faultMessage' => $e->getMessage()));
                $error = true;
            }
            if (!$error) {
                $this->getAttributes();
            } else {
                $this->response($this->json($result), 200);
            }
        } else {
            $msg = array('status' => 'apiLoginFailed', 'error' => json_decode($result));
            $this->response($this->json($msg), 200);
        }
    }

    /**
     *
     * Created By : Ramasankar
     * date created 12-10-2017(dd-mm-yy)
     * date modified (dd-mm-yy)
     * Returning List of Products from store product to getAllProducts() in products module
     * Scope : this page 
     *
     * @param 
     * @return Array data
     *
     */
    public function productList() {
        $result = $this->storeApiLogin();
        if ($this->storeApiLogin == true) {
            $key = $GLOBALS['params']['apisessId'];
            $categoryid = (isset($this->_request['categoryid']) && trim($this->_request['categoryid']) != '') ? trim($this->_request['categoryid']) : 0;
            $searchstring = (isset($this->_request['searchstring']) && trim($this->_request['searchstring']) != '') ? trim($this->_request['searchstring']) : '';
            $start = (isset($this->_request['start']) && trim($this->_request['start']) != '') ? trim($this->_request['start']) : 0;
            $limit = (isset($this->_request['range']) && trim($this->_request['range']) != '') ? trim($this->_request['range']) : 10;
            $offset = (isset($this->_request['offset']) && trim($this->_request['offset']) != '') ? trim($this->_request['offset']) : 1;
            $store = (isset($this->_request['store']) && trim($this->_request['store']) != '') ? trim($this->_request['store']) : $this->getDefaultStoreId();
            $loadVariants = (isset($this->_request['loadVariants']) && trim($this->_request['loadVariants']) == true) ? true : false;
            $preDecorated = (isset($this->_request['preDecorated']) && trim($this->_request['preDecorated']) == 'true') ? true : false;
            $filterArray = array('type' => array('eq' => 'configurable'));
            try {
                $filters = array(
                    'filters' => $filterArray,
                    'categoryid' => $categoryid,
                    'searchstring' => $searchstring,
                    'store' => $store,
                    'range' => array('start' => $start, 'range' => $limit),
                    'loadVariants' => $loadVariants,
                    'offset' => $offset,
                    'limit' => $limit,
                    'preDecorated' => $preDecorated,
                );
                $result = $this->proxy->call($key, 'cedapi_product.getAllProducts', $filters);
                $result = json_decode($result, true);
                return $result;
            } catch (Exception $e) {
                $result = array('isFault' => 1, 'faultMessage' => $e->getMessage());
                return $result;
            }
        } else {
            $res = array('isFault' => 2, 'error' => json_decode($result));
            return $res;
        }
    }

    /**
     *
     * date created 10-10-2017(dd-mm-yy)
     * date modified (dd-mm-yy)
     * Returning category details from store to getCategories()
     * Scope : this page 
     *
     * @param 
     * @return Array data
     *
     */
    public function allCategories() {
        $result = $this->storeApiLogin();
        $printId = $this->_request['printId'];
        if ($this->storeApiLogin == true) {
            $key = $GLOBALS['params']['apisessId'];
            $filters = array(
                'store' => $this->getDefaultStoreId(),
            );
            try {
                $result = $this->proxy->call($key, 'cedapi_product.getCategories', $filters); //,$filters);
                $categories = json_decode($result, true);
                return $categories['categories'];
            } catch (Exception $e) {
                $result = array('isFault' => 1, 'faultMessage' => $e->getMessage());
                return $result;
            }
        } else {
            $res = array('isFault' => 2, 'error' => json_decode($result));
            return $res;
        }
    }

    /**
     *
     * date created 27-09-2017(dd-mm-yy)
     * date modified (dd-mm-yy)
     * Returning details of a product
     * Scope : this page 
     *
     * @param 
     * @return Array data
     *
     */
    public function productDetails($client = '') {
        $result = $this->storeApiLogin();
        if ($this->storeApiLogin == true) {
            $key = $GLOBALS['params']['apisessId'];
            $productId = trim($this->_request['id']);

            if (!isset($this->_request['confId']) || trim($this->_request['confId']) == '') {
                $configProductId = 0;
            } else {
                $configProductId = trim($this->_request['confId']);
            }
            if (!isset($this->_request['size']) || trim($this->_request['size']) == '') {
                $size = '';
            } else {
                $size = trim($this->_request['size']);
            }
            $attributes = array();
            if ($size != '') {
                $attributes['size'] = $size;
            }
            $productInfo = array(
                'productId' => $productId,
                'store' => $this->getDefaultStoreId(),
                'attributes' => $attributes,
                'configId' => $configProductId,
            );
            if (!$error) {
                try {
                    $result = $this->proxy->call($key, 'cedapi_product.getSimpleProduct', $productInfo);
                    if (empty($result)) {
                        $result = array('isFault' => 1, 'faultMessage' => 'No Records Found');
                        return $result;
                    } else {
                        $resultArr = json_decode($result);
                        if ($client != '') {
                            $confProductId = $resultArr->pid;
                            // pre decorate info
                            $predecorateObj = Flight::predecorate();
                            $templatInfo = $predecorateObj->getTemplateInfo($confProductId);
                            $resultArr->defautlCategoryId = $templatInfo['cat_id'];
                            $resultArr->productStatus = $templatInfo['status'];
                        }
                        return $resultArr;
                    }
                } catch (Exception $e) {
                    $result = array('isFault' => 1, 'faultMessage' => $e->getMessage());
                    return $result;
                }
            }
        } else {
            $res = array('isFault' => 2, 'error' => json_decode($result));
            return $res;
        }
    }

    /**
     *
     * date created 10-10-2017(dd-mm-yy)
     * date modified (dd-mm-yy)
     * Returning product print method parameter details to getPrintMethodByProduct()
     * Scope : this page 
     *
     * @param 
     * @return Array data
     *
     */
    public function printMethodParameters() {
        $result = $this->storeApiLogin();
        if ($this->storeApiLogin == true && isset($this->_request['pid']) && $this->_request['pid']) {
            $key = $GLOBALS['params']['apisessId'];
            $confProductId = $this->_request['pid'];
            $isAdmin = (isset($this->_request['isAdmin']) && trim($this->_request['isAdmin']) == true) ? true : false;

            $filters = array('productid' => $this->_request['pid'], 'store' => $this->getDefaultStoreId());
            $result = $this->proxy->call($key, 'cedapi_product.getCategoriesByProduct', $filters);
            $catIds = json_decode($result, true);
            $refid = '';
            $result = array('confProductId' => $confProductId, 'isAdmin' => $isAdmin, 'catIds' => $catIds, 'refid' => $refid);
            return $result;
        } else {
            $res = array('isFault' => 2, 'error' => json_encode($result));
            return $res;
        }
    }

    /**
     *
     * date created 27-09-2017(dd-mm-yy)
     * date modified (dd-mm-yy)
     * Returning size and quantity of a product
     *
     * @param 
     * @return Array data
     *
     */
    public function sizeAndQuantityDetails() {
        $result = $this->storeApiLogin();
        if ($this->storeApiLogin == true) {
            $key = $GLOBALS['params']['apisessId'];
            if (!isset($this->_request['productId']) || trim($this->_request['productId']) == '') {
                $msg = array('status' => 'invalid productId', 'productId' => $this->_request['productId']);
                $this->response($this->json($msg), 204);
            } else {
                $productId = trim($this->_request['productId']);
            }
            if (!isset($this->_request['simplePdctId']) || trim($this->_request['simplePdctId']) == '') {
                //$varient_id = '';
                $msg = array('status' => 'invalid simplePdctId', 'simplePdctId' => $this->_request['simplePdctId']);
                $this->response($this->json($msg), 204);
            } else {
                $varientId = trim($this->_request['simplePdctId']);
            }
            if (isset($this->_request['byAdmin'])) {
                $byAdmin = true;
            } else {
                $byAdmin = false;
            }
            $productInfo = array(
                'productId' => $productId,
                'store' => $this->getDefaultStoreId(),
                'simpleProductId' => $varientId,
            );
            if (!$error) {
                try {
                    if ($byAdmin) {
                        $result = $this->proxy->call($key, 'cedapi_product.getSizeVariants', $productInfo);
                    } else {
                        $result = $this->proxy->call($key, 'cedapi_product.getSizeAndQuantity', $productInfo);
                    }
                    $resultArr = json_decode($result, true);
                    $resultArr = $resultArr['quantities'];
                    return $resultArr;
                } catch (Exception $e) {
                    $result = array('isFault' => 1, 'faultMessage' => $e->getMessage());
                    return $result;
                }
            }
        } else {
            $res = array('isFault' => 2, 'error' => json_encode($result));
            return $res;
        }
    }

    /**
     *
     * date created 27-09-2017(dd-mm-yy)
     * date modified (dd-mm-yy)
     * Returning Veriant Details
     * Scope : this page 
     *
     * @param 
     * @return Array data
     *
     */
    public function variantDetails() {
        $start = 0; // default values
        if (isset($this->_request['start']) && trim($this->_request['start']) != '') {
            $start = trim($this->_request['start']);
        }
        if (isset($this->_request['range']) && trim($this->_request['range']) != '' && trim($this->_request['range']) != 0) {
            $limit = trim($this->_request['range']);
        } else {
            $limit = 0;
        }
        $offset = (isset($this->_request['offset']) && trim($this->_request['offset']) != '') ? trim($this->_request['offset']) : '';
        $store = (isset($this->_request['store']) && trim($this->_request['store']) != '') ? trim($this->_request['store']) : $this->getDefaultStoreId();
        $result = $this->storeApiLogin();
        $confId = $this->_request['conf_pid'];
        if ($this->storeApiLogin == true) {
            $key = $GLOBALS['params']['apisessId'];
            try {
                $filters = array(
                    'confId' => $confId,
                    'start' => $start,
                    'limit' => $limit,
                    'store' => $store,
                    'offset' => $offset,
                );
                $result = $this->proxy->call($key, 'cedapi_product.getVariants', $filters);
                $catIds = '';
                $resultArr = json_decode($result, 'true');
                $pCount = $resultArr['count'];
                $variants = $resultArr['variants'];
                $result = array('variants' => $variants, 'count' => $pCount);
                return $result;
            } catch (Exception $e) {
                $resultArr = array('isFault' => 1, 'faultMessage' => $e->getMessage());
                return $resultArr;
            }
        } else {
            $res = array('isFault' => 2, 'error' => json_encode($result));
            return $res;
        }
    }

}
