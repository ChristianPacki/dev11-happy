<?php
	//echo "testt"; exit();
	$cedapi_data = array();
	$cedapi_data['cedapi_cart'] = array();
	$cedapi_data['cedapi_product'] = array();


	$cedapi_data['cedapi_product']['storeVersion'] = '1.9';
	$cedapi_data['cedapi_product']['getSizeArr'] = '[{"value":"3","label":"M"},{"value":"4","label":"L"},{"value":"5","label":"XL"}]';

	$cedapi_data['cedapi_product']['getColorArr'] = '[{"value":"8","label":"Blue"},{"value":"7","label":"Green"},{"value":"6","label":"Red"}]';

	$cedapi_data['cedapi_product']['getAllProducts'] = '{"product":[{"id":"2","name":"InkXE Tshirt","description":"InkXE tshirt Description","price":"123.0000","thumbnail":"../mock-assets/products/2/thumbnail.png","category":["2"],"store":["1"],"print_details":[{"prntMthdId":"1","prntMthdName":"DTG"}]}],"count":1}';

	$cedapi_data['cedapi_product']['checkDuplicateSku'] = array();
	$cedapi_data['cedapi_product']['checkDesignerTool'] = 'Enabled';
	$cedapi_data['cedapi_product']['getVariantList'] = '{"conf_id":"2","variants":[{"color_id":6,"size_id":[3]}]}';

	$cedapi_data['cedapi_product']['getVariants'] = '{"variants":[{"id":"1","name":"Simple InkXE Tshirt","description":"simple InkXE tshirt description","thumbnail":"../mock-assets/products/2/thumbnail.png","price":100,"tax":0,"xeColor":"Red","xe_color_id":"6","xe_size_id":"3","colorUrl":"","ConfcatIds":["2"],"attributes":{"xe_color":"Red","xe_color_id":"6","xe_size":"M","xe_size_id":"3"}}],"count":1}';

	$cedapi_data['cedapi_product']['getSizeAndQuantity'] = '{"quantities":[{"simpleProductId":"1","xe_color":"Red","xe_size":"M","xe_color_id":"6","xe_size_id":"3","quantity":23121,"price":100,"attributes":{"xe_color":"Red","xe_color_id":"6","xe_size":"M","xe_size_id":"3"}}]}';

	$cedapi_data['cedapi_product']['updateItemStatus'] = '{"is_Fault":"1",status":"1"}';
	$cedapi_data['cedapi_product']['addProducts'] = '{"is_Fault":"0",status":"ok","message":"Could not connect to store with mock data"}';
	$cedapi_data['cedapi_product']['addTemplateProducts'] = '{"is_Fault":"0",status":"ok","message":"Could not connect to store with mock data"}';
	$cedapi_data['cedapi_product']['getCategoriesByProduct'] = '["2"]';
	$cedapi_data['cedapi_product']['getCategories'] = '{"categories":[{"id":"2","name":"inkXE"}]}';
	$cedapi_data['cedapi_product']['getsubCategories'] = '{"subcategories":[{"id":"3","name":"TSHIRT"}]}';
	$cedapi_data['cedapi_cart']['getOrders'] = '{"is_Fault":0,"order_list":[{"order_id":"2","order_incremental_id":"100000002","order_status":"Pending","order_date":"2016-10-01 03:40:04","customer_name":"d m","print_status":"1"},{"order_id":"1","order_incremental_id":"100000001","order_status":"Pending","order_date":"2016-08-11 05:14:21","customer_name":"dev m","print_status":null}]}';

	$cedapi_data['cedapi_cart']['getOrdersGraph'] = '[{"date":"2016-10-01","sales":1}]';
	$cedapi_data['cedapi_cart']['getOrderDetails'] = '{"is_Fault":0,"orderIncrementId":null,"order_details":{"shipping_address":{"first_name":"dev","last_name":"m","fax":null,"region":"Alabama","postcode":"75","telephone":"99","city":"b","address_1":"aa","address_2":"","state":"Alabama","company":null,"email":"e@m.com","country":"US"},"billing_address":{"first_name":"dev","last_name":"m","fax":null,"region":"Alabama","postcode":"75","telephone":"99","state":"Alabama","city":"b","address_1":"aa","address_2":"","company":null,"email":"e@m.com","country":"US"},"order_id":"1","order_incremental_id":"100000001","order_status":"Pending","order_date":"2016-08-11 05:14:20","customer_name":"dev m","customer_email":"e@m.com","shipping_method":"flatrate_flatrate","order_items":[{"itemStatus":"Ordered","ref_id":"1","item_id":"1","print_status":null,"product_price":"104.0000","config_product_id":"2","product_id":"1","product_sku":"tshirt_S","product_name":"Simple InkXE Tshirt","quantity":"1.0000","xe_color":"Red","xe_size":"M"}]}}';

	$cedapi_data['cedapi_product']['getProductCount'] = '{"size":1}';
	$cedapi_data['cedapi_product']['addAttributeColorOptionValue'] = '{"is_Fault":"0",status":"ok","message":"Could not connect to store with mock data"}';
	$cedapi_data['cedapi_product']['editAttributeColorOptionValue'] = '{"is_Fault":"0",status":"ok","message":"Could not connect to store with mock data"}';
	$cedapi_data['cedapi_product']['getProductById'] = '{"is_Fault":"0",status":"ok","message":"nodata"}';
	$cedapi_data['cedapi_product']['getSimpleProduct'] = '{"pid":"2","pidtype":"simple","pname":"InkXE Tshirt","shortdescription":"simple InkXE tshirt short description","category":["2"],"pvid":"1","pvname":"Simple InkXE Tshirt","xecolor":"Red","xesize":"M","xeColorCode":"Red","xe_color_id":"6","xe_size_id":"3","quanntity":23121,"price":100,"taxrate":0,"thumbsides":["../mock-assets/products/1/thumbnail.png"],"sides":["../mock-assets/products/1/1.png"],"isPreDecorated":false,"labels":[""],"attributes":{"xe_color":"Red","xe_color_id":"6","xe_size":"M","xe_size_id":"3"}}';

	$cedapi_data['cedapi_cart']['addToCart'] = '{"is_Fault":"0",status":"ok","message":"Could not connect to store with mock data"}';
	$cedapi_data['cedapi_product']['getProductInfo'] = '{"is_Fault":"0",status":"ok","message":"Could not connect to store with mock data"}';

	$cedapi_data['cedapi_cart']['getOrderDetails'] = '{"is_Fault":0,"orderIncrementId":null,"order_details":{"shipping_address":{"first_name":"dev","last_name":"m","fax":null,"region":"Alabama","postcode":"75","telephone":"99","city":"b","address_1":"aa","address_2":"","state":"Alabama","company":null,"email":"e@m.com","country":"US"},"billing_address":{"first_name":"dev","last_name":"m","fax":null,"region":"Alabama","postcode":"75","telephone":"99","state":"Alabama","city":"b","address_1":"aa","address_2":"","company":null,"email":"e@m.com","country":"US"},"order_id":"1","order_incremental_id":"100000001","order_status":"Pending","order_date":"2016-08-11 05:14:20","customer_name":"dev m","customer_email":"e@m.com","shipping_method":"flatrate_flatrate","order_items":[{"itemStatus":"Ordered","ref_id":"1","item_id":"1","print_status":null,"product_price":"104.0000","config_product_id":"2","product_id":"1","product_sku":"tshirt_S","product_name":"Simple InkXE Tshirt","quantity":"1.0000","xe_color":"Red","xe_size":"M"}]}}';

	$cedapi_data['cedapi_product']['getCategoriesByProduct'] = '["2"]';

	$cedapi_data['cedapi_cart']['orderIdFromStore'] = '{"is_Fault":0,"order_list":[{"order_id":"1","order_incremental_id":"100000001"},{"order_id":"2","order_incremental_id":"100000002"}]}';

	$cedapi_data['cedapi_product']['getLiveQuoteRefIds'] = '{"is_Fault":"0",status":"ok","message":"Could not connect to store with mock data"}';

	define('CEDAPI_DATA', json_encode($cedapi_data));
?>