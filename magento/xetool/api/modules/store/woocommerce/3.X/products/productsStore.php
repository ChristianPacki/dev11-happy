<?php
/* Check Un-authorize Access */
if (!defined('accessUser')) {
	die("Error");
}
use Automattic\WooCommerce\Client;

class ProductsStore extends UTIL
{
	public function __construct()
	{
		parent::__construct();
		$this->wcNewApi = new Client(XEPATH, C_KEY, C_SECRET, ['wp_api' => true,'version' => 'wc/v1','query_string_auth' => true]);
	}

	/**
	 * Used to get all the xe_size inside magento
	 *
	 * @param   nothing
	 * @return  array contains all the xe_size inside store
	 */
	public function getSizeArr()
	{
		header('HTTP/1.1 200 OK');
		$error = '';
		global $wpdb;
		$sizeAttr = $this->getStoreAttributes("xe_size");
		$tableAttrTaxonomy = $wpdb->prefix . "woocommerce_attribute_taxonomies";
		$sizeAttrSlug = $wpdb->get_var("SELECT attribute_name FROM $tableAttrTaxonomy WHERE attribute_label = '$sizeAttr'");
		$sizeTexonomy = "pa_".$sizeAttrSlug;
		$result = $this->storeApiLogin();
		if ($this->storeApiLogin == true) {
			$key = $GLOBALS['params']['apisessId'];
			try {
				$sizes = get_terms($sizeTexonomy, 'hide_empty = 0');
				$sizeArray = array();
				$i = 0;
				foreach ($sizes as $size) {
					$size = (array) $size;
					$sizeArray[$i]['value'] = $size['name'];
					$sizeArray[$i]['label'] = $size['name'];
					$i++;
				}
			} catch (Exception $e) {
				$result = json_encode(array('isFault' => 1, 'faultMessage' => $e->getMessage()));
				$error = true;
			}
			if (!$error) {
				$this->response(json_encode($sizeArray), 200);
			} else {
				$this->response($this->formatJSONToArray($result), 200);
			}
		} else {
			$msg = array('status' => 'apiLoginFailed', 'error' => $this->formatJSONToArray($result));
			$this->response($this->json($msg), 200);
		}
	}

	/**
	 * Used to get all the xe_color inside magento
	 *
	 * @param   nothing
	 * @return  array contains all the xe_color inside store
	 */
	public function getColorArr($isSameClass = false)
	{
		header('HTTP/1.1 200 OK');
		global $wpdb;
		$error = '';
		$result = $this->storeApiLogin();
		if ($this->storeApiLogin == true) {
			// get color attribute.
			$tableAttrTaxonomy = $wpdb->prefix . "woocommerce_attribute_taxonomies";
			$colorAttr = $this->getStoreAttributes("xe_color");
			$colorAttrSlug = $wpdb->get_var("SELECT attribute_name FROM $tableAttrTaxonomy WHERE attribute_label = '$colorAttr'");
			$lastLoaded = ($this->_request['lastLoaded']) ? $this->_request['lastLoaded'] : 0;
			$loadCount = ($this->_request['loadCount']) ? $this->_request['loadCount'] : 0;
			$productId = ($this->_request['productId']) ? $this->_request['productId'] : 0;
			$productColors = array();
			try {
				if ($productId != 0) {
					$result = $this->wcNewApi->get('products/'. $productId);
					foreach ($result['attributes'] as $attribute) {
						$attribute = (object) $attribute;
						if (isset($attribute->name) && $attribute->name == $colorAttr) {
							$productColors = $attribute->options;
						}
					}
				}
				$num = ($lastLoaded == 0) ? $loadCount : $loadCount + $lastLoaded;
				$limit = '';
				if ($loadCount != 0) {
					$limit = "LIMIT " . $lastLoaded . "," . $loadCount;
				}
				$tableTerm = $wpdb->prefix . "terms";
				$tableTaxo = $wpdb->prefix . "term_taxonomy";
				$key = $GLOBALS['params']['apisessId'];
				$colorTaxonomy = "pa_".$colorAttrSlug;
				$sql = "SELECT t.term_id, t.name, t.slug FROM $tableTerm t LEFT JOIN $tableTaxo tt ON (t.term_id = tt.term_id)  WHERE tt.taxonomy='".$colorTaxonomy."' ORDER BY t.term_id ASC $limit";
				$colors = $wpdb->get_results($sql) or die(mysql_error());
				$colorArray = array();
				$i = 0;
				foreach ($colors as $color) {
					$color = (array) $color;
					if ($productId != 0 && !empty($productColors)) {
						if (in_array($color['slug'], $productColors) || in_array($color['name'], $productColors)) {
							$colorArray[$i]['value'] = $color['term_id'];
							$colorArray[$i]['label'] = $color['name'];
							$colorArray[$i]['swatchImage'] = $color['slug'] . '.png';
							$i++;
						}
					} else {
						$colorArray[$i]['value'] = $color['term_id'];
						$colorArray[$i]['label'] = $color['name'];
						$colorArray[$i]['swatchImage'] = $color['slug'] . '.png';
						$i++;
					}
				}
			} catch (Exception $e) {
				$result = json_encode(array('isFault' => 1, 'faultMessage' => $e->getMessage()));
				$error = true;
			}
			if (!$error) {
				if ($isSameClass) {
					return $colorArray;
				} else {
					$this->response(json_encode($colorArray), 200);
				}
			} else {
				$this->response($this->formatJSONToArray($result), 200);
			}
		} else {
			$msg = array('status' => 'apiLoginFailed', 'error' => $this->formatJSONToArray($result));
			$this->response($this->json($msg), 200);
		}
	}

	/**
	 * Used to get Term Id.
	 * @param $tname (attribute name) 
	 * @return term id.
	*/
	public function getTermId($tname)
	{
		global $wpdb;
		$tableName = $wpdb->prefix . "term_taxonomy";
		$termId = $wpdb->get_var("SELECT term_id FROM $tableName WHERE taxonomy = '$tname'");
		return $termId;
	}

	
	
	/**
	 * Check whether the given sku exists or doesn't
	 *
	 * @param   $sku_arr
	 * @return  true/false
	 */
	public function checkDuplicateSku()
	{
		// chk for storeid
		header('HTTP/1.1 200 OK');
		$error = false;
		$result = $this->storeApiLogin();
		if (!empty($this->_request) && $this->storeApiLogin == true) {
			$key = $GLOBALS['params']['apisessId'];
			if (!$error) {
				$filters = array(
					'sku_arr' => $this->_request['sku_arr'],
				);
				try {
					$result = $this->json(array()); //array("status"=>"failed");//$this->proxy->call($key, 'cedapi_product.checkDuplicateSku', $filters);
				} catch (Exception $e) {
					$result = json_encode(array('isFault inside apiv4: ' => 1, 'faultMessage' => $e->getMessage()));
					$error = true;
				}
			}
			$this->closeConnection();
			$this->response($result, 200);
		} else {
			$msg = array('status' => 'apiLoginFailed', 'error' => $this->formatJSONToArray($result));
			$this->response($this->json($msg), 200);
		}
	}

	/**
	 * Check whether xetool is enabled or disabled
	 *
	 * @param   nothing
	 * @return  true/false
	 */
	public function checkDesignerTool($t = 0)
	{
		require_once dirname(__FILE__) . '/../../../../../../../wp-admin/includes/plugin.php';
		$result = $this->storeApiLogin();
		if ($this->storeApiLogin == true) {
			$key = $GLOBALS['params']['apisessId'];
			try {
				$currentPlugin = get_option("active_plugins");
				$myPlugin = 'jck_woothumbs/jck_woothumbs.php';
				if (!in_array($myPlugin, $currentPlugin)) {
					$result = 'Disabled';
				} else {
					$result = 'Enabled';
				}

			} catch (Exception $e) {
				$result = json_encode(array('isFault' => 1, 'faultMessage' => $e->getMessage()));
			}
			if ($t) {
				return $result;
			} else {
				$this->response($result, 200);
			}

		} else {
			$msg = array('status' => 'apiLoginFailed', 'error' => $this->formatJSONToArray($result));
			$this->response($this->json($msg), 200);
		}
	}

	/**
	 * Get the list of variants available for a product
	 *
	 * @param   nothing
	 * @return  json list of variants
	 */
	public function getVariantList()
	{
		$error = false;
		// get color attribute and size attribute.
		$colorAttr = $this->getStoreAttributes("xe_color");
		$sizeAttr = $this->getStoreAttributes("xe_size");
		$resultArr = $this->storeApiLogin();
		if ($this->storeApiLogin == true) {
			$key = $GLOBALS['params']['apisessId'];
			try {
				$confId = $this->_request['conf_pid'];
				$filters = array('confId' => $confId);
				$product = $this->wcNewApi->get('products/'. $confId);
				$attrArray = array();
				$colorArr = array();
				$j = 0;
				foreach ($product['variations'] as $variation) {
					foreach ($variation['attributes'] as $attributes) {
						if ($attributes['name'] == $colorAttr) {
							$attrArray[$j]['color_id'] = $attributes['option'];
							$colorArr[] = $attributes['option'];
						} else if ($attributes['name'] == $sizeAttr) {
							$attrArray[$j]['size'] = $attributes['option'];
						}

					}
					$j++;
				}
				$pvariants = array();
				$k = 0;
				$resultArr = array();
				$resultArr['conf_id'] = $confId;
				$colorArr = array_unique($colorArr);
				$colArr = array();
				foreach ($attrArray as $attribute) {
					if (empty($colArr) || !in_array($attribute['color_id'], $colArr)) {
						$colArr[] = $attribute['color_id'];
						$pvariants[$k]['sizeid'] = array();
						$pvariants[$k]['color_id'] = $attribute['color_id'];
						$pvariants[$k]['sizeid'][] = $attribute['size'];
						$k++;
					} else {
						$key = array_search($attribute['color_id'], $colArr);
						$pvariants[$key]['sizeid'][] = $attribute['size'];
					}

				}
				$resultArr['variants'] = $pvariants;
			} catch (Exception $e) {
				$resultArr = json_encode(array('isFault' => 1, 'faultMessage' => $e->getMessage()));
				$error = true;
			}
			if (!$error) {
				$this->response($this->json($resultArr), 200);
			} else {
				$this->response($resultArr, 200);
			}
		} else {
			$msg = array('status' => 'apiLoginFailed', 'error' => json_decode($resultArr));
			$this->response($this->json($msg), 200);
		}
	}

	
	

	/**
	 *
	 *date created 31-05-2016(dd-mm-yy)
	 *date modified (dd-mm-yy)
	 *Add template as product
	 *
	 *
	 */
	public function addTemplateProducts()
	{
		header('HTTP/1.1 200 OK');
		$error = false;
		global $wpdb;
		if (!empty($this->_request['data'])) {
			$data = json_decode(urldecode($this->_request['data']), true);
			$apikey = $this->_request['apikey'];
			$result = $this->storeApiLogin();
			// get color and size attribute.
			$colorAttr = $this->getStoreAttributes("xe_color");
			$sizeAttr = $this->getStoreAttributes("xe_size");
			if ($this->storeApiLogin == true) {
				$key = $GLOBALS['params']['apisessId'];
				$var_color = $data['color_id'];
				$tableName = $wpdb->prefix . "terms";
				$colorData = $wpdb->get_row("SELECT * FROM $tableName WHERE term_id='$var_color'", ARRAY_A);
				$color = $colorData['slug'];
				$colorName = $colorData['name'];
				$colorId = 0;
				$sizeId = 0;
				$xeIsDesId = 0;
				$image_array = array();
				$productId = $data['simpleproduct_id'];
				$oldConfigId = $data['simpleproduct_id'];
				$oldProductData = $this->wcNewApi->get('products/'. $productId);
				$oldVariantId = 0;
				foreach ($oldProductData['attributes'] as $attribute) {
					$attribute = (object) $attribute;
					if ($attribute->name == "xe_is_designer")
					{
						$xeIsDesId = $attribute->id;
					}
				}
				foreach ($oldProductData['variations'] as $variants) {
					foreach ($variants['attributes'] as $attribute) {
						$attribute = (object) $attribute;
						if ($attribute->name == $colorAttr && ($colorName == $attribute->option || $color == $attribute->option)) {
							$colorId = $attribute->id;
							$oldVariantId = $variants['id'];
							foreach ($variants['image'] as $img) {
								$img = (object) $img;
								if ($img->id != 0) 
									$image_array[] = $img->src;
							}
						}
						if ($attribute->name == $sizeAttr)
						{
							$sizeId = $attribute->id;
						}
					}
					if ($oldVariantId != 0) {
						break;
					}
				}
				$attachments = get_post_meta($oldVariantId, 'variation_image_gallery', true);
				$attachmentsExp = array_filter(explode(',', $attachments));
				foreach ($attachmentsExp as $id) {
					$imageSrc = wp_get_attachment_image_src($id, 'full');
					$image_array[] = $imageSrc[0];
				}
				if ($data['conf_id'] == 0) {
					$productArray = array();
					$productArray['name'] = $data['product_name'];
					$productArray['type'] = 'variable';
					$productArray['sku'] = $data['sku'];
					$productArray['virtual'] = false;
					$productArray['price'] = $data['price'];
					$productArray['manage_stock'] = true;
					$productArray['stock_quantity'] = $data['qty'];
					$productArray['in_stock'] = true;
					$productArray['visible'] = true;
					$productArray['catalog_visibility'] = "visible";
					$productArray['weight'] = '';
					$productArray['description'] = $data['description'];
					$productArray['short_description'] = $data['short_description'];
					foreach ($data['cat_id'] as $key => $value) {
						$allCat[$key]['id'] = $value;
					}
					$productArray['categories'] = $allCat;
					$productArray['attributes'][0]['id'] = $colorId;
					$productArray['attributes'][0]['position'] = 0;
					$productArray['attributes'][0]['visible'] = true;
					$productArray['attributes'][0]['variation'] = true;
					$productArray['attributes'][0]['options'] = array($color);
					$productArray['attributes'][1]['id'] = $sizeId;
					$productArray['attributes'][1]['position'] = 0;
					$productArray['attributes'][1]['visible'] = true;
					$productArray['attributes'][1]['variation'] = true;
					if ($data['is_customized'] == 1) {
						$productArray['attributes'][2]['id'] = $xeIsDesId;
						$productArray['attributes'][2]['position'] = 0;
						$productArray['attributes'][2]['visible'] = false;
						$productArray['attributes'][2]['variation'] = false;
						$productArray['attributes'][2]['options'] = array($data['is_customized']);
					}
					// default_attributes
					$count = 0;
					foreach ($data['images'] as $productImage) {
						$productArray['images'][$count]['src'] = $productImage;
						$productArray['images'][$count]['position'] = $count;
						$count++;
					}
				} else {
					$conf_id = $data['conf_id'];
					$product_info = $this->wcNewApi->get('products/'. $conf_id);
					$product_info = (object) $product_info;
					$colorArray = array();
					$sizeArr = array();
					foreach ($product_info->attributes as $attribute) {
						$attribute = (object) $attribute;
						if ($attribute->name == $colorAttr) {
							foreach ($attribute->options as $option) {
								$colorArray[] = $option;
							}
						} else if ($attribute->name == $sizeAttr) {
							foreach ($attribute->options as $option) {
								$sizeArr[] = $option;
							}
						}
					}
					$a = 0;
					foreach ($product_info->variations as $variations) {
						$variations = (object) $variations;
						foreach ($variations->attributes as $attributes) {
							if ($attributes['name'] == $colorAttr) {
								$attrArray[$a]['color_id'] = $attributes['option'];
								$colorArr[] = $attributes['option'];
							} else if ($attributes['name'] == $sizeAttr) {
								$attrArray[$a]['size'] = $attributes['option'];
							}
						}
						$a++;
					}
					$varCount = count($product_info->variations);
					$colorArray[] = $color;
					$productArray = array();
					$productArray['type'] = 'variable';
					$productArray['attributes'][0]['id'] = $colorId;
					$productArray['attributes'][0]['position'] = 0;
					$productArray['attributes'][0]['visible'] = true;
					$productArray['attributes'][0]['variation'] = true;
					$productArray['attributes'][0]['options'] = array($color);
					$productArray['attributes'][1]['id'] = $sizeId;
					$productArray['attributes'][1]['position'] = 0;
					$productArray['attributes'][1]['visible'] = true;
					$productArray['attributes'][1]['variation'] = true;
					if ($data['is_customized'] == 1) {
						$productArray['attributes'][2]['id'] = $xeIsDesId;
						$productArray['attributes'][2]['position'] = 0;
						$productArray['attributes'][2]['visible'] = false;
						$productArray['attributes'][2]['variation'] = false;
						$productArray['attributes'][2]['options'] = array($data['is_customized']);
					}
					$img_count = count($product_info->images);
					foreach ($product_info->variations as $pvar) {
						$pvar = (object) $pvar;
						$var_ids[] = $pvar->id;
					}
				}
				$i = 0;
				foreach ($data['sizes'] as $size) {
					$productArray['variations'][$i]['sku'] = '';
					$productArray['variations'][$i]['regular_price'] = $data['price'];
					$productArray['variations'][$i]['manage_stock'] = true;
					$productArray['variations'][$i]['stock_quantity'] = $data['qty'];
					$productArray['variations'][$i]['in_stock'] = true;
					$productArray['variations'][$i]['weight'] = '';
					if (!empty($image_array)) {
						$j = 0;
						foreach ($image_array as $image) {
							$productArray['variations'][$i]['image'][$j]['src'] = $image;
							$productArray['variations'][$i]['image'][$j]['position'] = $j;
							$j++;
						}
					}
					$productArray['variations'][$i]['attributes'][0]['id'] = $colorId;
					$productArray['variations'][$i]['attributes'][0]['option'] = $color;
					$productArray['variations'][$i]['attributes'][1]['id'] = $sizeId;
					$productArray['variations'][$i]['attributes'][1]['option'] = $size;
					$sizeArray[] = $size;
					$i++;
				}
				try {
					if ($data['conf_id'] == 0) {
						$productArray['attributes'][1]['options'] = $sizeArray;
						$result = $this->wcNewApi->post("products", $productArray);
						add_post_meta($result['id'], 'refid', $data['ref_id']);
					} else {
						$productArray['attributes'][1]['options'] = array_merge($sizeArr, $sizeArray);
						$productArray['attributes'][0]['options'] = $colorArray;
						$result = $this->wcNewApi->put("products/".$data['conf_id'], $productArray);
					}
					if (!empty($result)) {
						$product = $result;
						$j = $varCount;
						foreach ($product['variations'] as $variation) {
							$i = 0;
							$attach_id = array();
							if (empty($var_ids) || !in_array($variation['id'], $var_ids)) {
								foreach ($image_array as $image) {
									if ($i > 0) {
										$finfo = getimagesize($image);
										$type = $finfo['mime'];
										$filename = basename($image);
										$dirPath = explode("wp-content/uploads", $image);
										$subDir = explode($filename, $dirPath[1]);
										$attachment = array(
											'guid' => $image,
											'post_mime_type' => $type,
											'post_title' => preg_replace('/\.[^.]+$/', '', basename($filename)),
											'post_content' => '',
											'post_status' => 'inherit',
										);
										$attach_id[] = wp_insert_attachment($attachment, $subDir[0] . basename($filename), $product['id']);
									}
									$i++;

								}
								if (!empty($attach_id)) {
									$var_image = implode(",", $attach_id);
									update_post_meta($variation['id'], 'variation_image_gallery', $var_image);
								}

								foreach ($variation['attributes'] as $attributes) {
									$attributes = (array) $attributes;
									if ($attributes['name'] == $colorAttr) {
										$attrArray[$j]['color_id'] = $attributes['option'];
										$colorArr[] = $attributes['option'];
									} else if ($attributes['name'] == $sizeAttr) {
										$attrArray[$j]['size'] = $attributes['option'];
									}
								}
								$j++;
							}
						}
					}
					$productId = ($productData['conf_id'] == 0) ? $result['id'] : $productData['conf_id'];
					$pvariants = array();
					$k = 0;
					$productArr = array();
					$productArr['conf_id'] = $productId;
					$colorArr = array_unique($colorArr);
					$colArr = array();
					foreach ($attrArray as $attribute) {
						if (empty($colArr) || !in_array($attribute['color_id'], $colArr)) {
							$colorId = $wpdb->get_var("SELECT term_id FROM $tableName WHERE slug = '" . $attribute['color_id'] . "'");
							$colArr[] = $attribute['color_id'];
							$pvariants[$k]['sizeid'] = array();
							$pvariants[$k]['color_id'] = $colorId;
							$pvariants[$k]['sizeid'][] = $attribute['size'];
							$k++;
						} else {
							$key = array_search($attribute['color_id'], $colArr);
							$pvariants[$key]['sizeid'][] = $attribute['size'];
						}
					}
					$productArr['variants'] = $pvariants;
					$this->customRequest(array('productid' => $data['simpleproduct_id'], 'isTemplate' => 1));
					$sides = sizeof($data['images']);
					$productTemplate = $this->getProductTemplateByProductId($data['simpleproduct_id']);

					if ($data['boundary_type'] == "single") {
                        $maskDatas = $this->getMaskData($sides);
                        $maskDatas = json_decode($maskDatas, true);
                        $printArea = array();
                        $printArea = $this->getPrintareaType($data['simpleproduct_id']);
                        foreach ($maskDatas as $key => $maskData) {
                            $maskScalewidth[$key] = $maskData['mask_width'];
                            $maskScaleHeight[$key] = $maskData['mask_height'];
                            $maskPrice[$key] = $maskData['mask_price'];
                            $scaleRatio[$key] = $maskData['scale_ratio'];
                            $scaleRatio_unit[$key] = $maskData['scaleRatio_unit'];
                        }
                        $this->customRequest(array('maskScalewidth' => $maskScalewidth, 'maskScaleHeight' => $maskScaleHeight, 'maskPrice' => $maskPrice, 'scaleRatio' => $scaleRatio, 'scaleRatio_unit' => $scaleRatio_unit, 'maskstatus' => $printArea['mask'], 'unitid' => $printArea['unit_id'], 'pricePerUnit' => $printArea['pricePerUnit'], 'maxWidth' => $printArea['maxWidth'], 'maxHeight' => $printArea['maxHeight'], 'boundsstatus' => $printArea['bounds'], 'customsizestatus' => $printArea['custom_size'], 'customMask' => $printArea['customMask']));
                        $printSizes = $this->getDtgPrintSizesOfProductSides($data['simpleproduct_id']);
                        $this->customRequest(array('productid' => $productArr['conf_id'], 'jsondata' => json_encode($maskDatas), 'printsizes' => $printSizes));
                        $this->saveMaskData();
                        if ($printSizes['status'] != 'nodata') {
                            $this->setDtgPrintSizesOfProductSides();
                        }
                    }else{
                         // multiple boundary set up
                        $multipleObj = Flight::multipleBoundary();
                        $productArr['old_conf_id'] = $oldConfigId;
                        $multiBoundData = $multipleObj->getMultiBoundMaskData($productArr['old_conf_id']);
                        $multiBoundData[0]['id'] = 0;
                        $unitArr = array($multiBoundData[0]['scaleRatio_unit']);
                        $saveStatus = $multipleObj->saveMultipleBoundary($productArr['conf_id'], json_encode($multiBoundData), $unitArr, true);
                    }
					$this->saveProductTemplateData($data['print_method_id'], $data['ref_id'], $data['simpleproduct_id'], $productArr['conf_id']);
					if (!empty($productTemplate['tepmlate_id'])) {
						$this->customRequest(array('pid' => $productArr['conf_id'], 'productTempId' => $productTemplate['tepmlate_id']));
						$this->addTemplateToProduct();
					}
				} catch (Exception $e) {
					$result = json_encode(array('isFault' => 1, 'faultMessage' => $e->getMessage()));
					$error = true;
				}
				if (!$error) {
					$this->response($this->json($productArr), 200);
				} else {
					$this->response($this->formatJSONToArray($result), 200);
				}
			} else {
				$msg = array('status' => 'apiLoginFailed', 'error' => json_decode($result));
				$this->response($this->json($msg), 200);
			}
		}
	}

	/**
	 * Get Category list by product id
	 *
	 * @param   pid
	 * @return  category list in json format
	 */
	public function getCategoriesByProduct()
	{
		header('HTTP/1.1 200 OK');
		//$error='';
		$printProfile = Flight::printProfile();
		$result = $this->storeApiLogin();
		if ($this->storeApiLogin == true && isset($this->_request['pid']) && $this->_request['pid']) {
			$key = $GLOBALS['params']['apisessId'];
			$res = array();
			try {
				$catIdArr = wp_get_post_terms($productId, 'product_cat', array('fields' => 'ids'));
				if (empty($catIdArr)) {
					$res = $printProfile->getDefaultPrintMethodId();
				} else {
					$catIdStr = implode(',', $catIdArr);
					//funn call inkxe db str
					$res = $this->getPrintMethodName($catIdStr);
					if (empty($res)) {
						$res = $printProfile->getDefaultPrintMethodId();
					}
				}
			} catch (Exception $e) {
				$res = json_encode(array('isFault' => 1, 'faultMessage' => $e->getMessage()));
			}
			$this->response(json_encode($res), 200);
		} else {
			$msg = array('status' => 'apiLoginFailed', 'error' => json_decode($result));
			$this->response($this->json($msg), 200);
		}
	}

	

	/**
	 * Get Sub-Category list
	 *
	 * @param   selectedCategory
	 * @return  sub-category list in json format
	 */
	public function getsubCategories()
	{
		$error = '';
		$result = $this->storeApiLogin();
		if ($this->storeApiLogin == true) {
			$key = $GLOBALS['params']['apisessId'];
			try {
				$taxonomy = 'product_cat';
				$orderby = 'name';
				$showCount = 0; // 1 for yes, 0 for no
				$padCounts = 0; // 1 for yes, 0 for no
				$hierarchical = 1; // 1 for yes, 0 for no
				$title = '';
				$empty = 0;
				$args = array(
					'taxonomy' => $taxonomy,
					'child_of' => 0,
					'parent' => $this->_request['selectedCategory'],
					'orderby' => $orderby,
					'show_count' => $showCount,
					'pad_counts' => $padCounts,
					'hierarchical' => $hierarchical,
					'title_li' => $title,
					'hide_empty' => $empty,
				);
				$sub_cats = get_categories($args);
				usort($sub_cats, function($a, $b) { //Sort the array using a user defined function
					return $a->name < $b->name ? -1 : 1; //Compare the scores
				});
				foreach ($sub_cats as $cat) {
					$catArr[] = array('id' => "" . $cat->term_id . "", 'name' => $cat->name);
				}
				$result = array('subcategories' => $catArr);
			} catch (Exception $e) {
				$result = json_encode(array('isFault' => 1, 'faultMessage' => $e->getMessage()));
				$error = true;
			}
			if (!$error) {
				$categories = array();
				$this->response($this->json($result), 200);
			} else {
				$this->response($this->formatJSONToArray($result), 200);
			}
		} else {
			$msg = array('status' => 'apiLoginFailed', 'error' => $this->formatJSONToArray($result));
			$this->response($this->json($msg), 200);
		}
	}

	/**
	 * Get product count
	 *
	 * @param   orderIncrementId
	 * @return  integer number of product
	 */
	public function getProductCount()
	{
		$error = false;
		$filter = array();
		$filter['attribute'] = 'pa_xe_is_designer';
		$filter['attribute_term'] = $this->getTermId('pa_xe_is_designer');
		try {
			$result = $this->wcNewApi->get('products',$parameters = $filter);
			$result = array('size' => count($result));
		} catch (Exception $e) {
			$result = json_encode(array('isFault' => 1, 'faultMessage' => $e->getMessage()));
			$error = true;
		}
		if (!$error) {
			$this->response($this->json($result), 200);
		} else {
			$msg = array('status' => 'failed', 'error' => $this->formatJSONToArray($result));
			$this->response($this->json($msg), 200);
		}
	}

	

	

	/**
	 *
	 *date created (dd-mm-yy)
	 *date modified 15-4-2016(dd-mm-yy)
	 *fetch print method id and name
	 *
	 *@param (String)apikey
	 *@param (int)productid
	 *@return json data
	 *
	 */
	public function getProductPrintMethod()
	{
		$productId = $this->_request['productid'];
		$key = $this->_request['apikey'];
		if (!empty($productId)) {
			// &&  !empty($key) && $this->isValidCall($key)){
			$error = false;
			$productPrintType = $this->getProductPrintMethodType($productId);
			if (!empty($productPrintType)) {
				foreach ($productPrintType as $k2 => $v2) {
					$printDetails[$k2]['print_method_id'] = $v2['pk_id'];
					$printDetails[$k2]['name'] = $v2['name'];
				}
			} else {
				$result = $this->storeApiLogin();
				if ($this->storeApiLogin == true) {
					$key = $GLOBALS['params']['apisessId'];
					try {
						$catIds = wp_get_post_terms($productId, 'product_cat', array('fields' => 'ids'));
						$catIds = implode(',', (array) $catIds);
						$printDetails = $this->getPrintMethodDetailsByCategory($catIds);
					} catch (Exception $e) {
						$result = json_encode(array('isFault' => 1, 'faultMessage' => $e->getMessage()));
						$error = true;
					}

				} else {
					$msg = array('status' => 'apiLoginFailed', 'error' => json_decode($result));
					$this->response($this->json($msg), 200);
				}
			}
			if (!$error) {

				$resultArr = $printDetails;
				$result = json_encode($resultArr);
				$this->response($this->json($resultArr), 200);
			} else {
				$this->response($result, 200);
			}
		} else {
			$msg = array("status" => "invalid Product Id");
			$this->response($this->json($msg), 200);
		}
	}

	
		/**
	 *
	 *date created 15-4-2016(dd-mm-yy)
	 *date modified 08-1-2018(dd-mm-yy)
	 *product is custmizable or not by product id
	 *
	 *@param (int)pid
	 *@return json data
	 *
	 */
	public function isCustomizable()
	{
		$result = $this->storeApiLogin();
		if ($this->storeApiLogin == true && isset($this->_request['pid']) && $this->_request['pid']) {
			$confProductId = $this->_request['pid'];
			try {
				$product = wc_get_product($confProductId);
				foreach ($product->attributes as $key => $value) {
					$attrTaxoName = $value->get_name();
					if($attrTaxoName == "pa_xe_is_designer") {
						if (get_term_by('id', $value->get_options()[0], $attrTaxoName)->name == 1) {
							$customizable = 1;
						} else {
							$customizable = 0;
						}
					}
				}
			} catch (Exception $e) {
				$result = json_encode(array('isFault' => 1, 'faultMessage' => $e->getMessage()));
				$error = true;
			}
			if (!$error) {
				$this->response($customizable, 200);
			} else {
				$msg = array('status' => 'failed', 'error' => $this->formatJSONToArray($result));
				$this->response($this->json($msg), 200);
			}			
		} else {
			$msg = array('status' => 'apiLoginFailed', 'error' => json_decode($result));
			$this->response($this->json($msg), 200);
		}
	}

	/**
	 * Check magento version
	 *
	 * @param   nothing
	 * @return  string $version
	 */
	public function storeVersion()
	{
		$result = $this->storeApiLogin();
		if ($this->storeApiLogin == true) {
			try {
				return $version = 1;
			} catch (Exception $e) {
				$result = json_encode(array('isFault' => 1, 'faultMessage' => $e->getMessage()));
				$error = true;
			}
		} else {
			$msg = array('status' => 'apiLoginFailed', 'error' => json_decode($result));
			$this->response($this->json($msg), 200);
		}
	}

	/**
	 *
	 *date created 07-06-2016(dd-mm-yy)
	 *date modified (dd-mm-yy)
	 *Save product template data
	 *
	 *@param (Int)old productid
	 *@param (Int)new productid
	 *@param (Int)refId
	 *
	 */
	public function saveProductTemplateData($printMethodId, $refId, $oldId, $newId)
	{
		$apiKey = $this->_request['apikey'];
		if ($this->isValidCall($apiKey)) {
			try {
				$msg = $this->saveProductTemplateStateRel($printMethodId,$refId,$oldId,$newId);
				return $this->json($msg);
			} catch (Exception $e) {
				$result = array('Caught exception:' => $e->getMessage());
				$this->response($this->json($result), 200);
			}
		}
	}
	/**
	 * Used to get all attributes value details for custom product
	 * date created 10-06-2017(dd-mm-yy)
	 * date modified 08-09-2017(dd-mm-yy)
	 * @return list of attributes
	*/
	public function getCustomOption()
	{
		$error = false;
		global $wpdb;
		$result = $this->storeApiLogin();
		if ($this->storeApiLogin == true) {

			if (!isset($this->_request['id']) || trim($this->_request['id']) == '') {
				$msg = array('status' => 'invalid productId', 'productId' => $this->_request['id']);
				$this->response($this->json($msg), 204);
			} else {
				$productId = trim($this->_request['id']);
			}
			$result = $this->wcNewApi->get('products/'. $productId);
			$result = (object) $result;
			try {
				$variantsArr = array();
				$i = 0;
				$attributes = array();
				$attrId = 0;
				$attrType = "";
				$attrSlug = "";
				$tableName = $wpdb->prefix . "terms";
				$tableTaxonomy = $wpdb->prefix . "term_taxonomy";
				$tableAttrTaxonomy = $wpdb->prefix . "woocommerce_attribute_taxonomies";
				foreach ($result->attributes as $attribute) {
					$attribute = (object) $attribute;
					if ($attribute->name != "xe_is_designer")
					{
						$attrDetails = $wpdb->get_results("SELECT attribute_id, attribute_name, attribute_type FROM $tableAttrTaxonomy WHERE attribute_label = '$attribute->name'");
						if (!empty($attrDetails['0']))
						{
							$attrId = $attrDetails['0']->attribute_id;
							$attrType = $attrDetails['0']->attribute_type;
							$attrSlug = $attrDetails['0']->attribute_name;
						}
						$attributes[$i]['option_title'] = $attribute->name;
						$attributes[$i]['option_id'] = $attrId;
						$attributes[$i]['type'] = $attrType;
						$attributes[$i]['is_require'] = 1;
						$attributes[$i]['option_values'] = array();
						foreach ($attribute->options as $key => $value) {
							$option_type_id = $wpdb->get_var("SELECT ts.term_id FROM $tableTaxonomy as ts join $tableName as tm on ts.term_id = tm.term_id WHERE ts.taxonomy = 'pa_$attrSlug' and tm.name = '$value'");
							$attributes[$i]['option_values'][$key]["option_type_id"] = $option_type_id;
							$attributes[$i]['option_values'][$key]["sku"] = null;
							$attributes[$i]['option_values'][$key]["sort_order"] = null;
							$attributes[$i]['option_values'][$key]["title"] = $value;
							$attributes[$i]['option_values'][$key]["price"] = null;
						}
						$i++;
					}
				}
				$result = $attributes;
			} catch (Exception $e) {
				$result = json_encode(array('isFault' => 1, 'faultMessage' => $e->getMessage()));
				$error = true;
			}
			$this->closeConnection();
			$this->response($this->json($result), 200);
		} else {
			$msg = array('status' => 'apiLoginFailed', 'error' => $this->formatJSONToArray($result));
			$this->response($this->json($msg), 200);
		}
	}

	/**
	 * date created 07-06-2017(dd-mm-yy)
	 * date modified (dd-mm-yy)
	 * Used to get all attributes
	 * @return list of attributes
	*/
	public function getAttributes()
	{
		$result = $this->storeApiLogin();
		if ($this->storeApiLogin == true) {
			$error = false;
			try {
				$result = $this->wcNewApi->get('products/attributes');
				$attributeArr = array();
				foreach ($result as $key => $value) {
					$value = (object) $value;
					$attributeArr[] = $arrayName = array('id' => $value->id,'attribute_code' => $value->name);
				}
			} catch (Exception $e) {
				$result = json_encode(array('isFault' => 1, 'faultMessage' => $e->getMessage()));
				$error = true;
			}
			if (!$error) {
				$this->response($this->json($attributeArr), 200);
			} else {
				$msg = array('status' => 'failed', 'error' => $this->formatJSONToArray($result));
				$this->response($this->json($msg), 200);
			}
		} else {
			$msg = array('status' => 'apiLoginFailed', 'error' => json_decode($result));
			$this->response($this->json($msg), 200);
		}
	}
	/**
     *
     *date created 10-06-2017(dd-mm-yy)
     *date modified (dd-mm-yy)
     *product is disabled add to cart or not by product id
     *
     *@param (int)pid
     *@return json data
     *
     */
    public function isDisabledAddToCart()
    {
        $result = $this->storeApiLogin();
        if ($this->storeApiLogin == true && isset($this->_request['pid']) && $this->_request['pid']) {
        	$error = false;
            try {
            	$confProductId = $this->_request['pid'];
				$result = $this->wcNewApi->get('products/'. $confProductId);
				$disableAddToCartArr = array();
				foreach ($result['attributes'] as $key => $value) {
					$value = (object) $value;
					if ($value->name == "disabled_addtocart") {
						$disableAddToCartArr = $value->options;
					}
				}
				if (!empty($disableAddToCartArr) && in_array(1, $disableAddToCartArr)) {
					$isDisabled = 1;
				} else {
					$isDisabled = 0;
				}
			} catch (Exception $e) {
				$result = json_encode(array('isFault' => 1, 'faultMessage' => $e->getMessage()));
				$error = true;
			}
			if (!$error) {
				$this->response($isDisabled, 200);
			} else {
				$msg = array('status' => 'failed', 'error' => $this->formatJSONToArray($result));
				$this->response($this->json($msg), 200);
			}
        } else {
            $msg = array('status' => 'apiLoginFailed', 'error' => json_decode($result));
            $this->response($this->json($msg), 200);
        }
    }

    /**
     *
     *Created By : Ramasankar
     *Modified By : Mukesh
     *date created 12-10-2017(dd-mm-yy)
     *date modified 16-04-2018(dd-mm-yy)
     *Returning List of Products from store product to getAllProducts() in products module
     *Scope : this page 
     *
     *@param 
     *@return Array data
     *
     */
    public function productList()
    {
    	global $wpdb;
        global $woocommerce;
        $start = $this->_request['start'];
        $range = $this->_request['range'];
        $page = $this->_request['offset'];
        $filter = array();
        $args = array();
        $error = false;
		function title_filter( $where, &$wp_query )
		{
		    global $wpdb;
		    if ( $search_term = $wp_query->get( 'search_prod_title' ) ) {
		        $where .= ' AND ' . $wpdb->posts . '.post_title LIKE \'%' . esc_sql( like_escape( $search_term ) ) . '%\'';
		    }
		    return $where;
		}

		try {
			$args = array(
			    'post_type' => 'product',
			    'posts_per_page' => $range,
			    'paged' => $page,
			    'search_prod_title' => $this->_request['searchstring'],
			    'post_status' => 'publish',
			    'orderby'     => 'title', 
			    'order'       => 'ASC',
			    'meta_query'  => array(),
			    'tax_query'   => array(
			        array(
			            'taxonomy'      => 'product_visibility',
			            'field'         => 'slug',
			            'terms'         => 'exclude-from-catalog', // Possibly 'exclude-from-search' too
			            'operator'      => 'NOT IN'
			        ),
				    array(
				      	'taxonomy'        => 'pa_xe_is_designer',
				      	'field'           => 'slug',
				      	'terms'           => '1',
				      	'operator'        => 'IN'
				    )
			    )
			);
			// Category Search
			if ($this->_request['categoryid'] != '' && $this->_request['categoryid'] != 0) {
				array_push($args['tax_query'],array(
		            'taxonomy'      => 'product_cat',
		            'field' => 'term_id', //This is optional, as it defaults to 'term_id'
		            'terms'         => $this->_request['categoryid'],
		            'operator'      => 'IN' // Possible values are 'IN', 'NOT IN', 'AND'.
		        ));
			}
			// For predeco product
			if ((isset($this->_request['preDecorated']) && $this->_request['preDecorated'] == 'false')){
				$meta_query = 
					array(
						'key' => 'refid',
						'value' => '1',
						'compare' => 'NOT EXISTS'
				);
				array_push($args['meta_query'], $meta_query);
	        }
			add_filter( 'posts_where', 'title_filter', 10, 2 );
			$wp_query = new WP_Query($args);
			remove_filter( 'posts_where', 'title_filter', 10, 2 );
			foreach ($wp_query->posts as $key => $value) {
				$image =  wp_get_attachment_image_src( get_post_thumbnail_id( $value->ID ), 'single-post-thumbnail' );
				$cat_list = wp_get_post_terms($value->ID,'product_cat',array('fields'=>'ids'));
				$price = get_post_meta($value->ID, '_regular_price', true);
				$productsArr[] = array('id' => $value->ID, 'name' => $value->post_title, 'description' => wp_strip_all_tags($value->post_content), 'price' => $price, 'thumbnail' => $image['0'], 'image' => $image['0'], 'category' => $cat_list);
				$count++;
			}
			$result = array('product' => $productsArr, 'count' => $count);
            return $result;
        } catch (Exception $e) {
            $result = array('isFault' => 1, 'faultMessage' => $e->getMessage());
            return $result;
        }
    }

    /**
     *
     *date created 10-10-2017(dd-mm-yy)
     *date modified (dd-mm-yy)
     *Returning category details from store to getCategories()
     *Scope : this page 
     *
     *@param 
     *@return Array data
     *
     */
    public function allCategories()
    {
    	$catArr = array();
        if (!isset($result->errors)) {
            try {
                $taxonomy = 'product_cat';
                $orderby = 'name';
                $showCount = 0; // 1 for yes, 0 for no
                $padCounts = 0; // 1 for yes, 0 for no
                $hierarchical = 1; // 1 for yes, 0 for no
                $title = '';
                $empty = 0;
                $args = array(
                    'taxonomy' => $taxonomy,
                    'orderby' => $orderby,
                    'parent' => 0,
                    'show_count' => $showCount,
                    'pad_counts' => $padCounts,
                    'hierarchical' => $hierarchical,
                    'title_li' => $title,
                    'hide_empty' => $empty,
                );
                $all_categories = get_categories($args);
                foreach($all_categories as $k=>$v){
                     $all_categories[$k]->id = $v->term_id;
                }
                usort($all_categories, function($a, $b) { //Sort the array using a user defined function
                    return $a->name < $b->name ? -1 : 1; //Compare the scores
                });
                $all_categories = (array) $all_categories;
                return $all_categories;
            } catch (Exception $e) {
                $result = array('isFault' => 1, 'faultMessage' => $e->getMessage());
                return $result;
            }
        } else {
            $res=array('isFault' => 2, 'error' => $result);
            return $res;
        }
    }

    /**
     *
     *date created 10-10-2017(dd-mm-yy)
     *date modified (dd-mm-yy)
     *Returning product print method parameter details to getPrintMethodByProduct()
     *Scope : this page 
     *
     *@param 
     *@return Array data
     *
     */
    public function printMethodParameters()
    {
    	$result = $this->storeApiLogin();
        if ($this->storeApiLogin == true && isset($this->_request['pid']) && $this->_request['pid']) {
            $key = $GLOBALS['params']['apisessId'];
            $resultArr = array();
            $filters = array('store' => $this->getDefaultStoreId());
            $confProductId = $this->_request['pid'];
            $refid = get_post_meta($confProductId, 'refid', true);
            $isAdmin = (isset($this->_request['isAdmin']) && trim($this->_request['isAdmin']) == true) ? true : false;
            $catIds = wp_get_post_terms($confProductId, 'product_cat', array('fields' => 'ids'));
            $result=array('confProductId' => $confProductId, 'isAdmin' => $isAdmin, 'catIds' => $catIds, 'refid' => $refid);
            return $result;
        } else {
            $res=array('isFault' => 2, 'error' => json_encode($result));
            return $res;
        }
    }

    /**
     *
     *date created 27-09-2017(dd-mm-yy)
     *date modified (dd-mm-yy)
     *Returning details of a product
     *Scope : this page 
     *
     *@param 
     *@return Array data
     *
     */
    public function productDetails($client = '')
    {
    	global $wpdb;
        $_tax = new WC_Tax();
        $pid = trim($this->_request['id']);
        if (!isset($this->_request['size']) || trim($this->_request['size']) == '') {
            $size = '';
        } else {
            $size = trim($this->_request['size']);
        }
        $configId = ($this->_request['confId']) ? $this->_request['confId'] : '';
        $proId = ($configId != '' && $configId != $pid) ? $configId : $pid;
        $attributes = array();
        $simpleProductId = '';
        if ($size != '') {
            $attributes['size'] = $size;
        }
        // get color attribute and size attribute.
        $colorAttr = $this->getStoreAttributes("xe_color");
        $sizeAttr = $this->getStoreAttributes("xe_size");
        try {
			$product = wc_get_product($proId);
			$productId = $product->id;
			$colorArr = array();
			$iscustom = false;
			// Check if multiple values exist or not in a attribute
			foreach ($product->attributes as $key => $value) {
				if($client != ''){
					if (!$iscustom && count($value->get_options()) > 1)
					{
						$iscustom = true;
					}
				}
			}

			// Product Type
			if ($product->product_type == "simple") {
				if($client != ''){
					if ($iscustom)
						$ptype = "custom";
					else 
						$ptype = $product->product_type;
				}
				else{
					$ptype = $product->product_type;
				}
			} else {
				$ptype = "configurable";
			} 
			$productsArr = array('pid' => $productId, 'pidtype' => $ptype, 'pname' => $product->name, 'category' => $product->category_ids);
			$productsArr['xesize'] = "";
			$productsArr['xecolor'] = "";
			$productsArr['xe_size_id'] = 0;
			$productsArr['xe_color_id'] = 0;
			$taxTable = $wpdb->prefix . "options";
			$taxEnabled = $wpdb->get_var("SELECT option_value FROM $taxTable WHERE option_name = 'woocommerce_calc_taxes'");
			$isInclusiveTax = $wpdb->get_var("SELECT option_value FROM $taxTable WHERE option_name = 'woocommerce_prices_include_tax'");
            $image = array();
			if ($configId != '' && $configId != $pid) {
				// For Variant Product
				$pvariant = new WC_Product_Variation($pid);
				$taxRate = $_tax->get_rates($pvariant->tax_class);
				$tax = 0;
				if ($taxEnabled == 'yes') {
					if ($isInclusiveTax == 'no') {
						foreach ($taxRate as $value) {
							$tax += $value['rate'];
						}
					}
				}
				$productsArr['pvid'] = $pid;
				$productsArr['pvname'] = $pvariant->name;
				$productsArr['quanntity'] = $pvariant->stock_quantity;
				$productsArr['price'] = $pvariant->price;
				$productsArr['taxrate'] = $tax;
				foreach ($pvariant->attributes as $key => $value) {
					$attrName = wc_attribute_label( $key );
					// Get Attribute slug details
					$attrTermDetails = get_term_by('slug', $value, $key);
					$attrValId = $attrTermDetails->term_id;
					$attrValName = $attrTermDetails->name;
					if ($attrName == $sizeAttr) {
						$productsArr['xesize'] = $attrValName;
						$productsArr['xe_size_id'] = $attrValId;
						if($client != ''){
							$productsArr['attributes']['xe_size'] = $value;
							$productsArr['attributes']['xe_size_id'] = $attrValId;
						}
					} elseif ($attrName == $colorAttr) {
						$productsArr['xecolor'] = $attrValName;
						$productsArr['xe_color_id'] = $attrValId;
						if($client != ''){
							$productsArr['attributes'][$attrName] = $value;
							$productsArr['attributes'][$attrName . '_id'] = $attrValId;
						}
					} else {
						if($client != ''){
							$productsArr['attributes'][$attrName] = $value;
							$productsArr['attributes'][$attrName . '_id'] = $attrValId;
						}

					}

				}
				
				if ($pvariant->image_id != 0) {
					$imageSrc = wp_get_attachment_image_src($pvariant->image_id, 'full');
					$image[] = $imageSrc[0];
				}
				$attachments = get_post_meta($pid, 'variation_image_gallery', true);
				$attachmentsExp = array_filter(explode(',', $attachments));
				foreach ($attachmentsExp as $id) {
					$imageSrc = wp_get_attachment_image_src($id, 'full');
					$image[] = $imageSrc[0];
				}
			} else {
				// For Configure Product
				if ($product->product_type == "variable" && !empty($product->get_children())){
					$variation_id = $product->get_children()[0];
					$pvariant = new WC_Product_Variation($variation_id);

					$taxRate = $_tax->get_rates($product->tax_class);
					$tax = 0;
					if($client != ''){
						if ($taxEnabled == 'yes') {
							if ($isInclusiveTax == 'no') {
								foreach ($taxRate as $value) {
									$tax += $value['rate'];
								}
							}
						}
					}
					else{
						foreach ($taxRate as $value) {
							$tax += $value['rate'];
						}
					}
					$productsArr['pvid'] = $variation_id;
					$productsArr['pvname'] = $pvariant->name;
					$productsArr['quanntity'] = $pvariant->stock_quantity;
					$productsArr['price'] = $pvariant->price;
					$productsArr['taxrate'] = $tax;
					foreach ($pvariant->attributes as $key => $value) {
						// Attributes slug details
						$attrTermDetails = get_term_by('slug', $value, $key);
						$attrName = wc_attribute_label( $key );
						$attrValId = $attrTermDetails->term_id;
						$attrValName = $attrTermDetails->name;
						if ($attrName == $sizeAttr) {
							$productsArr['xesize'] = $attrValName;
							$productsArr['xe_size_id'] = $attrValId;
							if($client != ''){
								$productsArr['attributes']['xe_size'] = $attrValName;
								$productsArr['attributes']['xe_size_id'] = $attrValId;
							}
						} elseif ($attrName == $colorAttr) {
							$productsArr['xecolor'] = $attrValName;
							$productsArr['xe_color_id'] = $attrValId;
							if($client != ''){
								$productsArr['attributes']['xe_color'] = $attrValName;
								$productsArr['attributes']['xe_color_id'] = $attrValId;
							}
						} else {
							if($client != ''){
								$productsArr['attributes'][$attrName] = $attrValName;
								$productsArr['attributes'][$attrName . '_id'] = $attrValId;
							}
						}
						
					}
					if ($pvariant->image_id != 0) {
						$imageSrc = wp_get_attachment_image_src($pvariant->image_id, 'full');
						$image[] = $imageSrc[0];
					}
					$attachments = get_post_meta($variation_id, 'variation_image_gallery', true);
					$attachmentsExp = array_filter(explode(',', $attachments));
					foreach ($attachmentsExp as $id) {
						$imageSrc = wp_get_attachment_image_src($id, 'full');
						$image[] = $imageSrc[0];
					}
				} else {
					// For Simple Product
					$pvariant = $product;
					$taxRate = $_tax->get_rates($pvariant->tax_class);
					$tax = 0;
					if($client != ''){
						if ($taxEnabled == 'yes') {
							if ($isInclusiveTax == 'no') {
								foreach ($taxRate as $value) {
									$tax += $value['rate'];
								}
							}
						}
					}
					else{
						foreach ($taxRate as $value) {
							$tax += $value['rate'];
						}
					}
					$productsArr['pvid'] = $pvariant->id;
					$productsArr['pvname'] = $pvariant->name;
					$productsArr['quanntity'] = $pvariant->stock_quantity;
					$productsArr['price'] = $pvariant->price;
					$productsArr['taxrate'] = $tax;
					foreach ($pvariant->attributes as $key => $value) {
						$attrName = wc_attribute_label( $key );
						$attrValId = $value->get_options()['0'];
						$attrTermDetails = get_term_by('term_id', $attrValId, $key);
						$attrValName = $attrTermDetails->name;
						
						if ($attrName == $sizeAttr) {
							$productsArr['xesize'] = $attrValName;
							$productsArr['xe_size_id'] = $attrValId;
						}
						if ($attrName == $colorAttr) {
							$productsArr['xecolor'] = $attrValName;
							$productsArr['xe_color_id'] = $attrValId;
						}
					}
					// For Color Swatch
					if($client == ''){
						$colorId = $productsArr['xe_color_id'];
						$productsArr['colorSwatch'] = $this->getColorSwatchProduct($colorId);
					}
                    // End
					
					if ($pvariant->image_id != 0) {
						$imageSrc = wp_get_attachment_image_src($pvariant->image_id, 'full');
						$image[] = $imageSrc[0];
					}
					foreach ($pvariant->gallery_image_ids as $id) {
						$imageSrc = wp_get_attachment_image_src($id, 'full');
						$image[] = $imageSrc[0];
					}
				}
			}
			
			$productsArr['thumbsides'] = $image;
			$productsArr['sides'] = $image;
			$refid = get_post_meta($productId, 'refid', true);
			$productsArr['isPreDecorated'] = ($refid != '') ? true : false;
			$productsArr['minQuantity'] = 1;
			if($client != ''){
				$productsArr['labels'] = array();
			}
			if($client == ''){
				$this->customRequest(array('productid' => $proId, 'returns' => true));
			}
			return $productsArr;
		} catch (Exception $e) {
			$result = json_encode(array('isFault' => 1, 'faultMessage' => $e->getMessage()));
			return $result;
		}
    }
    /**
     *
     *date created 27-09-2017(dd-mm-yy)
     *date modified (dd-mm-yy)
     *Returning size and quantity of a product
     *
     *@param 
     *@return Array data
     *
     */
    public function sizeAndQuantityDetails()
    {
    	global $wpdb;
        $result = $this->storeApiLogin();
        if ($this->storeApiLogin == true) {
            $key = $GLOBALS['params']['apisessId'];
            $productId = trim($this->_request['productId']);
            $varientId = trim($this->_request['simplePdctId']);
            // get color attribute and size attribute.
            $colorAttr = $this->getStoreAttributes("xe_color");
            $sizeAttr = $this->getStoreAttributes("xe_size");
			try {
				$product = wc_get_product($productId);
				$variantsArr = array();
				$i = 0;
				$variant = array();
				$colorArr = array();
				foreach ($product->attributes as $key => $value) {
					$attrName = wc_attribute_label( $key );
					if (isset($attrName) && $attrName == $colorAttr) {
						$colorArr = array_map('trim',explode(',', $product->get_attribute( $key )));
					}
				}
				if ($product->product_type == "variable" && !empty($product->get_children())) {
					foreach ($product->get_children() as $key => $value) {
						$variant[$i]['id'] = $value;
						$pvariant = new WC_Product_Variation($variant[$i]['id']);
						foreach ($pvariant->attributes as $attrKey => $attrVal) {
							$attrName = wc_attribute_label( $attrKey );
							$attrVal = get_term_by('slug', $attrVal, $attrKey)->name;
							$attrValId = get_term_by('slug', $attrVal, $attrKey)->term_id;
							if ($attrName == $colorAttr) {
								$variant[$i]['xe_color_id'] = $attrValId;
								if ($variant[$i]['id'] == $varientId) {
									if (!empty($colorArr) && in_array('#' . $attrName, $colorArr)) {
										$color = '#' . $attrVal;
									} else {
										$color = $attrVal;
									}
								}
								if (!empty($colorArr) && in_array('#' . $attrVal, $colorArr)) {
									$variant[$i]['xe_color'] = '#' . $attrVal;
									$variant[$i]['attributes']['xe_color'] = $attrVal;
								} else {
									$variant[$i]['xe_color'] = $attrVal;
									$variant[$i]['attributes']['xe_color'] = $attrVal;
								}
								$variant[$i]['attributes']['xe_color_id'] = $attrValId;
							} elseif ($attrName == $sizeAttr){
								$variant[$i]['xe_size'] = $attrVal;
								$variant[$i]['xe_size_id'] = $attrValId;
								$variant[$i]['attributes']['xe_size'] = $attrVal;
								$variant[$i]['attributes']['xe_size_id'] = $attrValId;
							} else {
								if ($attrVal != "") {
									$variant[$i]['attributes'][$attrName] = $attrVal;
									$variant[$i]['attributes'][$attrName . '_id'] = $attrValId;
								}
							}
						}
						$variant[$i]['price'] = $pvariant->price;
						$variant[$i]['quantity'] = $pvariant->stock_quantity;
						$i++;
					}
				}
				$j = 0;
				$sizeArray = array();
				foreach ($variant as $var) {
					if (isset($this->_request['byAdmin'])) {
						if (empty($sizeArray) || !in_array($var['attributes']['xe_size'], $sizeArray)) {
							$variantsArr[$j]['simpleProductId'] = $var['id'];
							$variantsArr[$j]['xe_size_id'] = $var['xe_size_id'];
							$variantsArr[$j]['xe_color_id'] = $var['xe_color_id'];
							$variantsArr[$j]['xe_color'] = $var['xe_color'];
							$variantsArr[$j]['xe_size'] = $var['xe_size'];
							$variantsArr[$j]['attributes'] = $var['attributes'];
							$variantsArr[$j]['quantity'] = $var['quantity'];
							$variantsArr[$j]['price'] = $var['price'];
							$variantsArr[$j]['minQuantity'] = 1;
							$sizeArray[] = $var['xe_size'];
							$j++;
						}
					} else {
						if ($var['xe_color'] == $color && (empty($sizeArray) || !in_array($var['attributes']['xe_size'], $sizeArray))) {
							$variantsArr[$j]['simpleProductId'] = $var['id'];
							$variantsArr[$j]['xe_size_id'] = $var['xe_size_id'];
							$variantsArr[$j]['xe_color_id'] = $var['xe_color_id'];
							$variantsArr[$j]['xe_color'] = $var['xe_color'];
							$variantsArr[$j]['xe_size'] = $var['xe_size'];
							$variantsArr[$j]['attributes'] = $var['attributes'];
							$variantsArr[$j]['quantity'] = (float) $var['quantity'];
							$variantsArr[$j]['price'] = $var['price'];
							$variantsArr[$j]['minQuantity'] = 1;
							$sizeArray[] = $var['attributes']['xe_size'];
							$j++;
						}
					}
				}
				return $variantsArr;
			} catch (Exception $e) {
				$result = json_encode(array('isFault' => 1, 'faultMessage' => $e->getMessage()));
				return $result;
			}
        } else {
            $res=array('isFault' => 2, 'error' => json_encode($result));
            return $res;
        }
    }

    /**
     *
     *date created 27-09-2017(dd-mm-yy)
     *date modified (dd-mm-yy)
     *Returning Veriant Details
     *Scope : this page 
     *
     *@param 
     *@return Array data
     *
     */
    public function variantDetails()
    {
    	global $wpdb;
        header('HTTP/1.1 200 OK');
        $error = false;
        $limit = 1; // default values
        $start = 0; // default values
        if (isset($this->_request['start']) && trim($this->_request['start']) != '') {
            $start = trim($this->_request['start']);
        }
        if (isset($this->_request['range']) && trim($this->_request['range']) != '') {
            $limit = trim($this->_request['range']);
        }
        $startIndex = (isset($this->_request['offset'])) ? (int) $this->_request['range'] * ((int) $this->_request['offset'] - 1) : 0;
        $result = $this->storeApiLogin();
        $confId = $this->_request['conf_pid'];
        // get color attributes.
        $colorAttr = $this->getStoreAttributes("xe_color");
        if ($this->storeApiLogin == true) {
            $key = $GLOBALS['params']['apisessId'];
            try {
				$product = wc_get_product($confId);
				$i = 0;
				$variant = array();
				if ($product->product_type == "variable" && !empty($product->get_children())) {
					foreach ($product->get_children() as $key => $value) {
						$variationAttrDetails = $product->get_available_variation($value);
						$tableName = $wpdb->prefix . "terms";
						$tableTaxonomy = $wpdb->prefix . "term_taxonomy";
						$tableAttrTaxonomy = $wpdb->prefix . "woocommerce_attribute_taxonomies";
						$color_taxonomy = $wpdb->get_var("SELECT attribute_name FROM $tableAttrTaxonomy WHERE attribute_label = '$colorAttr'");
						if (get_post_meta($value,'attribute_pa_'.$color_taxonomy,true) != "") {
							$attrvalue = get_post_meta($value,'attribute_pa_'.$color_taxonomy,true);
							$color_option = $attrvalue;
							if (empty($variant) || (!empty($variant) && !in_array($attrvalue, $colorArray))) {
								$colorArray[] = $attrvalue;
								$variant[$i]['xeColor'] = $attrvalue;
								$termId = get_term_by('slug', $attrvalue, 'pa_'.$color_taxonomy)->term_id;
								$variant[$i]['colorUrl'] = $termId . '.png';
								$variant[$i]['id'] = $value;
								if (get_post_meta($value,'_thumbnail_id',true) != "")
									$imageId = get_post_meta($value,'_thumbnail_id',true);
								else 
									$imageId = get_post_meta($confId,'_thumbnail_id',true);
								$imageSrc = wp_get_attachment_image_src($imageId, 'full');
								$variant[$i]['name'] = $product->name;
								$variant[$i]['price'] = get_post_meta($confId,'_price',true);
								$variant[$i]['image'] = $imageSrc[0];
								$variant[$i]['xe_color_id'] = $termId;
								$i++;
							}
						}
					}
				}
				$variants = array();
				$j = 0;
				$length = sizeof($variant);
				$range = (isset($this->_request['range'])) ? $this->_request['range'] : $length;
				while ($startIndex < $length) {
					if ($j < $range) {
						$variants[$j]['id'] = $variant[$startIndex]['id'];
						$variants[$j]['name'] = $variant[$startIndex]['name'];
						$variants[$j]['price'] = $variant[$startIndex]['price'];
						$variants[$j]['xeColor'] = $variant[$startIndex]['xeColor'];
						$variants[$j]['xe_color_id'] = $variant[$startIndex]['xe_color_id'];
						$variants[$j]['colorUrl'] = $variant[$startIndex]['colorUrl'];
						$variants[$j]['thumbnail'] = $variant[$startIndex]['image'];
						$j++;
						$startIndex++;
					} else {
						break;
					}
				}
				$result = array('variants' => $variants, 'count' => $length);
                return $result;
			} catch (Exception $e) {
				$resultArr = json_encode(array('isFault' => 1, 'faultMessage' => $e->getMessage()));
				return $resultArr;
			}
        } else {
            $res=array('isFault' => 2, 'error' => json_encode($result));
            return $res;
        }
    }
}
