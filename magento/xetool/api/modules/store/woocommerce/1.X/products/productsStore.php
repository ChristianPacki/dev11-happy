<?php
/* Check Un-authorize Access */
if (!defined('accessUser')) {
	die("Error");
}

class ProductsStore extends UTIL
{
	public function __construct()
	{
		parent::__construct();
		$this->wcApi = new WC_API_Client(C_KEY, C_SECRET, XEPATH);
	}

	/**
	 * Used to get all the xe_size inside magento
	 *
	 * @param   nothing
	 * @return  array contains all the xe_size inside store
	 */
	public function getSizeArr()
	{
		header('HTTP/1.1 200 OK');
		$error = '';
		$sizetexonomy = "pa_".$this->getStoreAttributes("xe_size");
		$result = $this->storeApiLogin();
		if ($this->storeApiLogin == true) {
			$key = $GLOBALS['params']['apisessId'];
			try {
				$sizes = get_terms($sizetexonomy, 'hide_empty = 0');
				$size_array = array();
				$i = 0;
				foreach ($sizes as $size) {
					$size = (array) $size;
					$size_array[$i]['value'] = $size['slug'];
					$size_array[$i]['label'] = $size['name'];
					$i++;
				}
			} catch (Exception $e) {
				$result = json_encode(array('isFault' => 1, 'faultMessage' => $e->getMessage()));
				$error = true;
			}
			if (!$error) {
				$this->response(json_encode($size_array), 200);
			} else {
				$this->response($this->formatJSONToArray($result), 200);
			}
		} else {
			$msg = array('status' => 'apiLoginFailed', 'error' => $this->formatJSONToArray($result));
			$this->response($this->json($msg), 200);
		}
	}

	/**
	 * Used to get all the xe_color inside magento
	 *
	 * @param   nothing
	 * @return  array contains all the xe_color inside store
	 */
	public function getColorArr($isSameClass = false)
	{
		header('HTTP/1.1 200 OK');
		global $wpdb;
		$error = '';
		$result = $this->storeApiLogin();
		if ($this->storeApiLogin == true) {
			$lastLoaded = ($this->_request['lastLoaded']) ? $this->_request['lastLoaded'] : 0;
			$loadCount = ($this->_request['loadCount']) ? $this->_request['loadCount'] : 0;
			$product_id = ($this->_request['productId']) ? $this->_request['productId'] : 0;
			$productColors = array();
			if ($product_id != 0) {
				$result = $this->wcApi->get_product($product_id);
				foreach ($result->product->attributes as $attribute) {
					if (isset($attribute->name) && $attribute->name == $this->getStoreAttributes("xe_color")) {
						$productColors = $attribute->options;
					}
				}
			}
			$num = ($lastLoaded == 0) ? $loadCount : $loadCount + $lastLoaded;
			$limit = '';
			if ($loadCount != 0) {
				$limit = "LIMIT " . $lastLoaded . "," . $loadCount;
			}

			$table_term = $wpdb->prefix . "terms";
			$table_taxo = $wpdb->prefix . "term_taxonomy";

			$key = $GLOBALS['params']['apisessId'];
			$colortexonomy = "pa_".$this->getStoreAttributes("xe_color");
			try {
				$sql = "SELECT t.term_id, t.name, t.slug FROM $table_term t LEFT JOIN $table_taxo tt ON (t.term_id = tt.term_id)  WHERE tt.taxonomy='".$colortexonomy."' ORDER BY t.term_id ASC $limit";
				$colors = $wpdb->get_results($sql) or die(mysql_error());
				$color_array = array();
				$i = 0;
				foreach ($colors as $color) {
					$color = (array) $color;
					if ($product_id != 0 && !empty($productColors)) {
						if (in_array($color['slug'], $productColors) || in_array($color['name'], $productColors)) {
							$color_array[$i]['value'] = $color['term_id'];
							$color_array[$i]['label'] = $color['name'];
							$color_array[$i]['swatchImage'] = $color['slug'] . '.png';
							$i++;
						}
					} else {
						$color_array[$i]['value'] = $color['term_id'];
						$color_array[$i]['label'] = $color['name'];
						$color_array[$i]['swatchImage'] = $color['slug'] . '.png';
						$i++;
					}

				}
			} catch (Exception $e) {
				$result = json_encode(array('isFault' => 1, 'faultMessage' => $e->getMessage()));
				$error = true;
			}
			if (!$error) {
				if ($isSameClass) {
					return $color_array;
				} else {
					$this->response(json_encode($color_array), 200);
				}

			} else {
				$this->response($this->formatJSONToArray($result), 200);
			}
		} else {
			$msg = array('status' => 'apiLoginFailed', 'error' => $this->formatJSONToArray($result));
			$this->response($this->json($msg), 200);
		}
	}
	/**
	 * Used to get all attributes details for simple product
	 * @return list of attributes
	*/
	public function getAttributeDetailsByName($attr_name){

		$attrDetails = array();
		try {
			$attributeList = $this->wcApi->get_products_attribute();
			foreach ($attributeList->product_attributes as $attribute) {
				if ($attribute->name == $attr_name)
				{
					$attrDetails['id'] = $attribute->id;
					$attrDetails['name'] = $attribute->name;
					$attrDetails['type'] = $attribute->type;
				}
			}
			return $attrDetails;
		} catch (Exception $e) {
			return $attrDetails;
		}
	}

	/**
	 * Check whether the given sku exists or doesn't
	 *
	 * @param   $sku_arr
	 * @return  true/false
	 */
	public function checkDuplicateSku()
	{
		// chk for storeid
		header('HTTP/1.1 200 OK');
		$error = false;
		$result = $this->storeApiLogin();
		if (!empty($this->_request) && $this->storeApiLogin == true) {
			$key = $GLOBALS['params']['apisessId'];
			if (!$error) {
				$filters = array(
					'sku_arr' => $this->_request['sku_arr'],
				);

				try {
					$result = $this->json(array()); //array("status"=>"failed");//$this->proxy->call($key, 'cedapi_product.checkDuplicateSku', $filters);
				} catch (Exception $e) {
					$result = json_encode(array('isFault inside apiv4: ' => 1, 'faultMessage' => $e->getMessage()));
					$error = true;
				}
			}
			$this->closeConnection();
			$this->response($result, 200);
		} else {
			$msg = array('status' => 'apiLoginFailed', 'error' => $this->formatJSONToArray($result));
			$this->response($this->json($msg), 200);
		}
	}

	/**
	 * Check whether xetool is enabled or disabled
	 *
	 * @param   nothing
	 * @return  true/false
	 */
	public function checkDesignerTool($t = 0)
	{
		require_once dirname(__FILE__) . '/../../../../../../../wp-admin/includes/plugin.php';
		$result = $this->storeApiLogin();
		if ($this->storeApiLogin == true) {
			$key = $GLOBALS['params']['apisessId'];
			try {
				$current_plugin = get_option("active_plugins");
				$myplugin = 'jck_woothumbs/jck_woothumbs.php';
				if (!in_array($myplugin, $current_plugin)) {
					$result = 'Disabled';
				} else {
					$result = 'Enabled';
				}

			} catch (Exception $e) {
				$result = json_encode(array('isFault' => 1, 'faultMessage' => $e->getMessage()));
			}
			if ($t) {
				return $result;
			} else {
				$this->response($result, 200);
			}

		} else {
			$msg = array('status' => 'apiLoginFailed', 'error' => $this->formatJSONToArray($result));
			$this->response($this->json($msg), 200);
		}
	}

	/**
	 * Get the list of variants available for a product
	 *
	 * @param   nothing
	 * @return  json list of variants
	 */
	public function getVariantList()
	{
		$error = false;
		// get color attribute and size attribute.
		$colorAttr = $this->getStoreAttributes("xe_color");
		$sizeAttr = $this->getStoreAttributes("xe_size");
		$resultArr = $this->storeApiLogin();
		if ($this->storeApiLogin == true) {
			$key = $GLOBALS['params']['apisessId'];

			try {
				$confId = $this->_request['conf_pid'];
				$filters = array('confId' => $confId);
				$product_info = $this->wcApi->get_product($confId);
				$product = (array) $product_info->product;
				$attr_array = array();
				$colorArr = array();
				$j = 0;
				foreach ($product['variations'] as $variation) {
					$variation = (array) $variation;
					foreach ($variation['attributes'] as $attributes) {

						$attributes = (array) $attributes;
						if ($attributes['name'] == $colorAttr) {
							$attr_array[$j]['color_id'] = $attributes['option'];
							$colorArr[] = $attributes['option'];
						} else if ($attributes['name'] == $sizeAttr) {
							$attr_array[$j]['size'] = $attributes['option'];
						}

					}
					$j++;
				}
				$pvariants = array();
				$k = 0;
				$resultArr = array();
				$resultArr['conf_id'] = $confId;
				$colorArr = array_unique($colorArr);
				$colArr = array();
				foreach ($attr_array as $attribute) {
					if (empty($colArr) || !in_array($attribute['color_id'], $colArr)) {
						$colArr[] = $attribute['color_id'];
						$pvariants[$k]['sizeid'] = array();
						$pvariants[$k]['color_id'] = $attribute['color_id'];
						$pvariants[$k]['sizeid'][] = $attribute['size'];
						$k++;
					} else {
						$key = array_search($attribute['color_id'], $colArr);
						$pvariants[$key]['sizeid'][] = $attribute['size'];
					}

				}
				$resultArr['variants'] = $pvariants;
			} catch (Exception $e) {
				$resultArr = json_encode(array('isFault' => 1, 'faultMessage' => $e->getMessage()));
				$error = true;
			}

			if (!$error) {
				$this->response($this->json($resultArr), 200);
			} else {
				$this->response(json_decode($resultArr), 200);
			}
		} else {
			$msg = array('status' => 'apiLoginFailed', 'error' => json_decode($resultArr));
			$this->response($this->json($msg), 200);
		}
	}

	/**
	 * Update item print status
	 *
	 * @param   orderID, productID, orderItemId, refid
	 * @return  true/false
	 */
	public function updateItemPrintStatus()
	{
		$error = false;

		$result = $this->storeApiLogin();
		if ($this->storeApiLogin == true) {
			$key = $GLOBALS['params']['apisessId'];
			try {
				$orderID = $this->_request['orderID'];
				$productID = $this->_request['productID'];
				$itemID = trim($this->_request['orderItemId']);
				$ref_id = trim($this->_request['refid']);

				$data['order']['id'] = $orderID;
				$data['order']['line_items'][] = array(
					'id' => $itemID,
					'product_id' => $productID,
					'variations' => array(
						'print_status' => 1,
					),
				);

				$data['order']['order_meta'][] = array(
					'key' => "print_status",
					'label' => "Print_status",
					'value' => 1,

				);
				$result = $this->wcApi->update_order($orderID, $data);
				if (!isset($result->errors)) {                   
					$stat = $this->updateOrderList($ref_id,$itemID);
				}
			} catch (Exception $e) {
				$result = json_encode(array('isFault' => 1, 'faultMessage' => $e->getMessage()));
				$error = true;
			}
			if (!$error) {
				$this->response($result, 200);
			} else {
				$this->response($this->formatJSONToArray($result), 200);
			}

		} else {
			$msg = array('status' => 'apiLoginFailed', 'error' => $this->formatJSONToArray($result));
			$this->response($this->json($msg), 200);
		}
	}

	/**
	 * Add Product to store
	 *
	 * @param   product information
	 * @return  product id,name in json format
	 */
	public function addProducts()
	{
		header('HTTP/1.1 200 OK');
		$error = false;
		$result = $this->storeApiLogin();
		$t = time();
		if (!empty($this->_request) && $this->storeApiLogin == true) {
			$key = $GLOBALS['params']['apisessId'];
			$product_array = array();
			global $wpdb;
			$upload_dir = wp_upload_dir();
			if (isset($_FILES['configFile']['tmp_name'])) {
				$filename = str_replace(" ", "_", $_FILES['configFile']['name']);
				$filename = $t . "_" . $filename;
				$file = $upload_dir['path'] . "/" . trim($filename);
				if (file_exists($file)) {
					$status = 1;
				} else {
					$status = move_uploaded_file($_FILES['configFile']['tmp_name'], $file);
				}

				if ($status);
				{
					$imageFile = $upload_dir['url'] . "/" . $filename;
				}
			}
			$image_array = array();
			$image_file = array();
			if (isset($_FILES['simpleFile'])) {
				foreach ($_FILES['simpleFile']['tmp_name'] as $key => $product_image) {
					$t = time();
					$filename = str_replace(" ", "_", $_FILES['simpleFile']['name'][$key]);
					$filename = $t . "_" . $filename;
					$file = $upload_dir['path'] . "/" . trim($filename);
					if (file_exists($file)) {
						$status = 1;
					} else {
						$status = move_uploaded_file($product_image, $file);
					}

					if ($status);
					{
						$image_array[] = $upload_dir['url'] . "/" . trim($filename);
						$image_file[] = trim($filename);
					}
				}
			}
			$data = $this->formatJSONToArray($this->_request['data']);
			$productData = $data->productData;
			$productVariants = $productData['variants'];
			$var_ids = array();
			$attr_array = array();
			$colorArr = array();
			$varCount = 0;
			if ($productData['conf_id'] == 0) {
				$var_color = $productVariants[0]->color_id;
				$table_name = $wpdb->prefix . "terms";
				$color = $wpdb->get_var("SELECT slug FROM $table_name WHERE term_id = '$var_color'");

				$product_array['product']['title'] = $productData['product_name'];
				$product_array['product']['type'] = 'variable';
				$product_array['product']['sku'] = $productData['sku'];
				$product_array['product']['virtual'] = false;
				$product_array['product']['price'] = $productData['price'];
				$product_array['product']['regular_price'] = $productData['price'];
				$product_array['product']['managing_stock'] = true;
				$product_array['product']['stock_quantity'] = $productData['qty'];
				$product_array['product']['in_stock'] = true;
				$product_array['product']['visible'] = true;
				$product_array['product']['catalog_visibility'] = "visible";
				$product_array['product']['weight'] = $productData['weight'];
				$product_array['product']['description'] = $productData['description'];
				$product_array['product']['short_description'] = $productData['short_description'];
				$product_array['product']['categories'] = $productData['cat_id'];
				$product_array['product']['images'][0]['src'] = $imageFile;
				$product_array['product']['images'][0]['position'] = 0;
				$product_array['product']['featured_src'] = $imageFile;
				$product_array['product']['attributes'][0]['name'] = 'xe_color';
				$product_array['product']['attributes'][0]['slug'] = 'xe_color';
				$product_array['product']['attributes'][0]['position'] = 0;
				$product_array['product']['attributes'][0]['visible'] = true;
				$product_array['product']['attributes'][0]['variation'] = true;
				$product_array['product']['attributes'][0]['options'] = array($color);
				$product_array['product']['attributes'][1]['name'] = 'xe_size';
				$product_array['product']['attributes'][1]['slug'] = 'xe_size';
				$product_array['product']['attributes'][1]['position'] = 0;
				$product_array['product']['attributes'][1]['visible'] = true;
				$product_array['product']['attributes'][1]['variation'] = true;
				$product_array['product']['attributes'][2]['name'] = 'xe_is_designer';
				$product_array['product']['attributes'][2]['slug'] = 'xe_is_designer';
				$product_array['product']['attributes'][2]['position'] = 0;
				$product_array['product']['attributes'][2]['visible'] = false;
				$product_array['product']['attributes'][2]['variation'] = false;
				$product_array['product']['attributes'][2]['options'] = array("1");
				$size_array = array();
				foreach ($productVariants as $variants) {
					$i = 0;
					$simpleProduct = (array) $variants->simpleProducts;
					foreach ($simpleProduct as $variant) {
						$variant = (array) $variant;
						$product_array['product']['variations'][$i]['sku'] = $variant['sku'];
						$product_array['product']['variations'][$i]['regular_price'] = $variant['price'];
						$product_array['product']['variations'][$i]['managing_stock'] = true;
						$product_array['product']['variations'][$i]['stock_quantity'] = $variant['qty'];
						$product_array['product']['variations'][$i]['in_stock'] = true;
						$product_array['product']['variations'][$i]['weight'] = $variant['weight'];
						if (!empty($image_array)) {
							$j = 0;
							foreach ($image_array as $image) {
								if ($imageFile != $image) {
									$product_array['product']['images'][$j]['src'] = $image;
									$product_array['product']['images'][$j]['position'] = $j;
								}
								$product_array['product']['variations'][$i]['image'][$j]['src'] = $image;
								$product_array['product']['variations'][$i]['image'][$j]['position'] = $j;
								$j++;
							}
						}
						$product_array['product']['variations'][$i]['attributes'][0]['name'] = 'xe_color';
						$product_array['product']['variations'][$i]['attributes'][0]['slug'] = 'xe_color';
						$product_array['product']['variations'][$i]['attributes'][0]['option'] = $color;
						$product_array['product']['variations'][$i]['attributes'][1]['name'] = 'xe_size';
						$product_array['product']['variations'][$i]['attributes'][1]['slug'] = 'xe_size';
						$product_array['product']['variations'][$i]['attributes'][1]['option'] = $variant['sizeId'];
						$size_array[] = $variant['sizeId'];
						$i++;
					}
				}
				$product_array['product']['attributes'][1]['options'] = $size_array;

			} else {
				$product_info = $this->wcApi->get_product($productData['conf_id']);
				$colorArray = array();
				$sizeArr = array();

				foreach ($product_info->product->attributes as $attribute) {
					if ($attribute->name == 'xe_color') {
						foreach ($attribute->options as $option) {
							$colorArray[] = $option;
						}
					} else if ($attribute->name == 'xe_size') {
						foreach ($attribute->options as $option) {
							$sizeArr[] = $option;
						}
					}

				}
				$a = 0;
				foreach ($product_info->product->variations as $variations) {
					foreach ($variations->attributes as $attributes) {
						$attributes = (array) $attributes;
						if ($attributes['name'] == 'xe_color') {
							$attr_array[$a]['color_id'] = $attributes['option'];
							$colorArr[] = $attributes['option'];
						} else if ($attributes['name'] == 'xe_size') {
							$attr_array[$a]['size'] = $attributes['option'];
						}

					}
					$a++;
				}
				$varCount = count($product_info->product->variations);
				$var_color = $productVariants[0]->color_id;
				$table_name = $wpdb->prefix . "terms";
				$color = $wpdb->get_var("SELECT slug FROM $table_name WHERE term_id='$var_color'");
				$colorArray[] = $color;
				$product_array = array();
				$product_array['product']['type'] = 'variable';
				$product_array['product']['attributes'][0]['name'] = 'xe_color';
				$product_array['product']['attributes'][0]['slug'] = 'xe_color';
				$product_array['product']['attributes'][0]['position'] = 0;
				$product_array['product']['attributes'][0]['visible'] = true;
				$product_array['product']['attributes'][0]['variation'] = true;
				$product_array['product']['attributes'][0]['options'] = $colorArray;
				$product_array['product']['attributes'][1]['name'] = 'xe_size';
				$product_array['product']['attributes'][1]['slug'] = 'xe_size';
				$product_array['product']['attributes'][1]['position'] = 0;
				$product_array['product']['attributes'][1]['visible'] = true;
				$product_array['product']['attributes'][1]['variation'] = true;
				$product_array['product']['attributes'][2]['name'] = 'xe_is_designer';
				$product_array['product']['attributes'][2]['slug'] = 'xe_is_designer';
				$product_array['product']['attributes'][2]['position'] = 0;
				$product_array['product']['attributes'][2]['visible'] = false;
				$product_array['product']['attributes'][2]['variation'] = false;
				$product_array['product']['attributes'][2]['options'] = array("1");
				$img_count = count($product_info->product->images);

				foreach ($product_info->product->variations as $pvar) {
					$var_ids[] = $pvar->id;
				}
				foreach ($productVariants as $variants) {
					$i = 0;
					$simpleProduct = (array) $variants->simpleProducts;
					foreach ($simpleProduct as $variant) {
						$variant = (array) $variant;
						$product_array['product']['variations'][$i]['sku'] = $variant['sku'];
						$product_array['product']['variations'][$i]['regular_price'] = $variant['price'];
						$product_array['product']['variations'][$i]['managing_stock'] = true;
						$product_array['product']['variations'][$i]['stock_quantity'] = $variant['qty'];
						$product_array['product']['variations'][$i]['in_stock'] = true;
						$product_array['product']['variations'][$i]['weight'] = $variant['weight'];
						if (!empty($image_array)) {
							$j = 0;
							$m = 0;
							foreach ($image_array as $image) {
								$product_array['product']['images'][$j]['src'] = $image;
								$product_array['product']['images'][$j]['position'] = $j + $img_count;
								$product_array['product']['variations'][$i]['image'][$m]['src'] = $image;
								$product_array['product']['variations'][$i]['image'][$m]['position'] = $m;
								$j++;
								$m++;
							}
						}
						$product_array['product']['variations'][$i]['attributes'][0]['name'] = 'xe_color';
						$product_array['product']['variations'][$i]['attributes'][0]['slug'] = 'xe_color';
						$product_array['product']['variations'][$i]['attributes'][0]['option'] = $color;
						$product_array['product']['variations'][$i]['attributes'][1]['name'] = 'xe_size';
						$product_array['product']['variations'][$i]['attributes'][1]['slug'] = 'xe_size';
						$product_array['product']['variations'][$i]['attributes'][1]['option'] = $variant['sizeId'];
						$sizeArr[] = $variant['sizeId'];
						$i++;
					}
				}
				$product_array['product']['attributes'][1]['options'] = $sizeArr;
			}

			if (!$error) {
				try {
					if ($productData['conf_id'] == 0) {
						$result = $this->wcApi->create_product($product_array);
					} else {
						$result = $this->wcApi->edit_product($productData['conf_id'], $product_array);
					}
					if (!empty($result)) {
						$product = (array) $result->product;
						$j = $varCount;

						foreach ($product['variations'] as $variation) {
							$variation = (array) $variation;
							$i = 0;
							$attach_id = array();
							if (empty($var_ids) || !in_array($variation['id'], $var_ids)) {
								foreach ($image_file as $filename) {
									if ($i > 0) {
										$finfo = getimagesize($upload_dir['path'] . '/' . basename($filename));
										$type = $finfo['mime'];
										$attachment = array(
											'guid' => $upload_dir['url'] . '/' . basename($filename),
											'post_mime_type' => $type,
											'post_title' => preg_replace('/\.[^.]+$/', '', basename($filename)),
											'post_content' => '',
											'post_status' => 'inherit',
										);
										$attach_id[] = wp_insert_attachment($attachment, $upload_dir['subdir'] . '/' . basename($filename), $product['id']);
									}
									$i++;

								}
								if (!empty($attach_id)) {
									$var_image = implode(",", $attach_id);
									update_post_meta($variation['id'], 'variation_image_gallery', $var_image);
								}

								foreach ($variation['attributes'] as $attributes) {

									$attributes = (array) $attributes;
									if ($attributes['name'] == 'xe_color') {
										$attr_array[$j]['color_id'] = $attributes['option'];
										$colorArr[] = $attributes['option'];
									} else if ($attributes['name'] == 'xe_size') {
										$attr_array[$j]['size'] = $attributes['option'];
									}

								}
								$j++;

							}

						}
					}
					$product_id = ($productData['conf_id'] == 0) ? $result->product->id : $productData['conf_id'];

					$pvariants = array();
					$k = 0;
					$productArr = array();
					$productArr['conf_id'] = $product_id;
					$colorArr = array_unique($colorArr);
					$colArr = array();
					foreach ($attr_array as $attribute) {
						if (empty($colArr) || !in_array($attribute['color_id'], $colArr)) {
							$colArr[] = $attribute['color_id'];
							$pvariants[$k]['sizeid'] = array();
							$pvariants[$k]['color_id'] = $attribute['color_id'];
							$pvariants[$k]['sizeid'][] = $attribute['size'];
							$k++;
						} else {
							$key = array_search($attribute['color_id'], $colArr);
							$pvariants[$key]['sizeid'][] = $attribute['size'];
						}

					}
					$productArr['variants'] = $pvariants;

				} catch (Exception $e) {
					$result = json_encode(array('isFault' => 1, 'faultMessage' => $e->getMessage()));
					$error = true;
				}
			}
			if (!$error) {
				$this->response($this->json($productArr), 200);
			} else {
				$this->response($this->formatJSONToArray($result), 200);
			}
		} else {
			$msg = array('status' => 'apiLoginFailed', 'error' => $this->formatJSONToArray($result));
			$this->response($this->json($msg), 200);
		}
	}
	/**
	 *
	 *date created 31-05-2016(dd-mm-yy)
	 *date modified (dd-mm-yy)
	 *Add template as product
	 *
	 *
	 */
	public function addTemplateProducts()
	{
		header('HTTP/1.1 200 OK');
		$error = false;
		global $wpdb;
		if (!empty($this->_request['data'])) {
			$data = json_decode(urldecode($this->_request['data']), true);
			$apikey = $this->_request['apikey'];
			$result = $this->storeApiLogin();
			// get color and size attribute.
			$colorAttr = $this->getStoreAttributes("xe_color");
			$sizeAttr = $this->getStoreAttributes("xe_size");
			if ($this->storeApiLogin == true) {
				$key = $GLOBALS['params']['apisessId'];
				$var_color = $data['color_id'];
				$table_name = $wpdb->prefix . "terms";
				$colorData = $wpdb->get_row("SELECT * FROM $table_name WHERE term_id='$var_color'", ARRAY_A);
				$color = $colorData['slug'];
				$colorName = $colorData['name'];
				$oldProductData = $this->wcApi->get_product($data['simpleproduct_id']);
				$oldVariantId = 0;
				foreach ($oldProductData->product->variations as $variants) {
					foreach ($variants->attributes as $attribute) {
						if ($attribute->name == $colorAttr && ($colorName == $attribute->option || $color == $attribute->option)) {
							$oldVariantId = $variants->id;
							foreach ($variants->image as $img) {
								$image_array[] = $img->src;
							}
						}
					}
					if ($oldVariantId != 0) {
						break;
					}

				}
				$attachments = get_post_meta($oldVariantId, 'variation_image_gallery', true);
				$attachmentsExp = array_filter(explode(',', $attachments));
				foreach ($attachmentsExp as $id) {
					$imageSrc = wp_get_attachment_image_src($id, 'full');
					$image_array[] = $imageSrc[0];
				}
				if ($data['conf_id'] == 0) {
					$product_array['product']['title'] = $data['product_name'];
					$product_array['product']['type'] = 'variable';
					$product_array['product']['sku'] = $data['sku'];
					$product_array['product']['virtual'] = false;
					$product_array['product']['price'] = $data['price'];
					$product_array['product']['regular_price'] = $data['price'];
					$product_array['product']['managing_stock'] = true;
					$product_array['product']['stock_quantity'] = $data['qty'];
					$product_array['product']['in_stock'] = true;
					$product_array['product']['visible'] = true;
					$product_array['product']['catalog_visibility'] = "visible";
					$product_array['product']['weight'] = '';
					$product_array['product']['description'] = $data['description'];
					$product_array['product']['short_description'] = $data['short_description'];
					$product_array['product']['categories'] = $data['cat_id'];
					$product_array['product']['attributes'][0]['name'] = $colorAttr;
					$product_array['product']['attributes'][0]['slug'] = $colorAttr;
					$product_array['product']['attributes'][0]['position'] = 0;
					$product_array['product']['attributes'][0]['visible'] = true;
					$product_array['product']['attributes'][0]['variation'] = true;
					$product_array['product']['attributes'][0]['options'] = array($color);
					$product_array['product']['attributes'][1]['name'] = $sizeAttr;
					$product_array['product']['attributes'][1]['slug'] = $sizeAttr;
					$product_array['product']['attributes'][1]['position'] = 0;
					$product_array['product']['attributes'][1]['visible'] = true;
					$product_array['product']['attributes'][1]['variation'] = true;
					if ($data['is_customized'] == 1) {
						$product_array['product']['attributes'][2]['name'] = 'xe_is_designer';
						$product_array['product']['attributes'][2]['slug'] = 'xe_is_designer';
						$product_array['product']['attributes'][2]['position'] = 0;
						$product_array['product']['attributes'][2]['visible'] = false;
						$product_array['product']['attributes'][2]['variation'] = false;
						$product_array['product']['attributes'][2]['options'] = array($data['is_customized']);
					}
					if ($data['captureType'] == "without_image") {
						$product_array['product']['attributes'][3]['name'] = 'disabled_addtocart';
						$product_array['product']['attributes'][3]['slug'] = 'disabled_addtocart';
						$product_array['product']['attributes'][3]['position'] = 0;
						$product_array['product']['attributes'][3]['visible'] = false;
						$product_array['product']['attributes'][3]['variation'] = false;
						$product_array['product']['attributes'][3]['options'] = array(1);
					}
					$count = 0;
					foreach ($data['images'] as $productImage) {
						$product_array['product']['images'][$count]['src'] = $productImage;
						$product_array['product']['images'][$count]['position'] = $count;
						if ($count == 0) {
							$product_array['product']['featured_src'] = $productImage;
						}

						$count++;
					}
				} else {
					$product_info = $this->wcApi->get_product($data['conf_id']);
					$colorArray = array();
					$sizeArr = array();
					foreach ($product_info->product->attributes as $attribute) {
						if ($attribute->name == $colorAttr) {
							foreach ($attribute->options as $option) {
								$colorArray[] = $option;
							}
						} else if ($attribute->name == $sizeAttr) {
							foreach ($attribute->options as $option) {
								$sizeArr[] = $option;
							}
						}

					}
					$a = 0;
					foreach ($product_info->product->variations as $variations) {
						foreach ($variations->attributes as $attributes) {
							$attributes = (array) $attributes;
							if ($attributes['name'] == $colorAttr) {
								$attr_array[$a]['color_id'] = $attributes['option'];
								$colorArr[] = $attributes['option'];
							} else if ($attributes['name'] == $sizeAttr) {
								$attr_array[$a]['size'] = $attributes['option'];
							}
						}
						$a++;
					}
					$varCount = count($product_info->product->variations);
					$colorArray[] = $color;
					$product_array = array();
					$product_array['product']['type'] = 'variable';
					$product_array['product']['attributes'][0]['name'] = $colorAttr;
					$product_array['product']['attributes'][0]['slug'] = $colorAttr;
					$product_array['product']['attributes'][0]['position'] = 0;
					$product_array['product']['attributes'][0]['visible'] = true;
					$product_array['product']['attributes'][0]['variation'] = true;
					$product_array['product']['attributes'][0]['options'] = $colorArray;
					$product_array['product']['attributes'][1]['name'] = $sizeAttr;
					$product_array['product']['attributes'][1]['slug'] = $sizeAttr;
					$product_array['product']['attributes'][1]['position'] = 0;
					$product_array['product']['attributes'][1]['visible'] = true;
					$product_array['product']['attributes'][1]['variation'] = true;
					if ($data['is_customized'] == 1) {
						$product_array['product']['attributes'][2]['name'] = 'xe_is_designer';
						$product_array['product']['attributes'][2]['slug'] = 'xe_is_designer';
						$product_array['product']['attributes'][2]['position'] = 0;
						$product_array['product']['attributes'][2]['visible'] = false;
						$product_array['product']['attributes'][2]['variation'] = false;
						$product_array['product']['attributes'][2]['options'] = array($data['is_customized']);
					}
					if ($data['captureType'] == "without_image") {
						$product_array['product']['attributes'][3]['name'] = 'disabled_addtocart';
						$product_array['product']['attributes'][3]['slug'] = 'disabled_addtocart';
						$product_array['product']['attributes'][3]['position'] = 0;
						$product_array['product']['attributes'][3]['visible'] = false;
						$product_array['product']['attributes'][3]['variation'] = false;
						$product_array['product']['attributes'][3]['options'] = array(1);
					}
					$img_count = count($product_info->product->images);
					foreach ($product_info->product->variations as $pvar) {
						$var_ids[] = $pvar->id;
					}
				}
				$i = 0;
				foreach ($data['sizes'] as $size) {
					$product_array['product']['variations'][$i]['sku'] = '';
					$product_array['product']['variations'][$i]['regular_price'] = $data['price'];
					$product_array['product']['variations'][$i]['managing_stock'] = true;
					$product_array['product']['variations'][$i]['stock_quantity'] = $data['qty'];
					$product_array['product']['variations'][$i]['in_stock'] = true;
					$product_array['product']['variations'][$i]['weight'] = '';
					if (!empty($image_array)) {
						$j = 0;
						foreach ($image_array as $image) {
							$product_array['product']['variations'][$i]['image'][$j]['src'] = $image;
							$product_array['product']['variations'][$i]['image'][$j]['position'] = $j;
							$j++;
						}
					}
					$product_array['product']['variations'][$i]['attributes'][0]['name'] = $colorAttr;
					$product_array['product']['variations'][$i]['attributes'][0]['slug'] = $colorAttr;
					$product_array['product']['variations'][$i]['attributes'][0]['option'] = $color;
					$product_array['product']['variations'][$i]['attributes'][1]['name'] = $sizeAttr;
					$product_array['product']['variations'][$i]['attributes'][1]['slug'] = $sizeAttr;
					$product_array['product']['variations'][$i]['attributes'][1]['option'] = $size;
					$size_array[] = $size;
					$i++;

				}
				$product_array['product']['attributes'][1]['options'] = $size_array;
				try {
					if ($data['conf_id'] == 0) {
						$result = $this->wcApi->create_product($product_array);
						add_post_meta($result->product->id, 'refid', $data['ref_id']);
					} else {
						$result = $this->wcApi->edit_product($data['conf_id'], $product_array);
					}
					if (!empty($result)) {
						$product = (array) $result->product;
						$j = $varCount;

						foreach ($product['variations'] as $variation) {
							$variation = (array) $variation;
							$i = 0;
							$attach_id = array();
							if (empty($var_ids) || !in_array($variation['id'], $var_ids)) {
								foreach ($image_array as $image) {
									if ($i > 0) {
										$finfo = getimagesize($image);
										$type = $finfo['mime'];
										$filename = basename($image);
										$dirPath = explode("wp-content/uploads", $image);
										$subDir = explode($filename, $dirPath[1]);
										$attachment = array(
											'guid' => $image,
											'post_mime_type' => $type,
											'post_title' => preg_replace('/\.[^.]+$/', '', basename($filename)),
											'post_content' => '',
											'post_status' => 'inherit',
										);
										$attach_id[] = wp_insert_attachment($attachment, $subDir[0] . basename($filename), $product['id']);
									}
									$i++;

								}
								if (!empty($attach_id)) {
									$var_image = implode(",", $attach_id);
									update_post_meta($variation['id'], 'variation_image_gallery', $var_image);
								}

								foreach ($variation['attributes'] as $attributes) {
									$attributes = (array) $attributes;
									if ($attributes['name'] == $colorAttr) {
										$attr_array[$j]['color_id'] = $attributes['option'];
										$colorArr[] = $attributes['option'];
									} else if ($attributes['name'] == $sizeAttr) {
										$attr_array[$j]['size'] = $attributes['option'];
									}

								}
								$j++;
							}
						}
					}
					$product_id = ($productData['conf_id'] == 0) ? $result->product->id : $productData['conf_id'];
					$pvariants = array();
					$k = 0;
					$productArr = array();
					$productArr['conf_id'] = $product_id;
					$colorArr = array_unique($colorArr);
					$colArr = array();
					foreach ($attr_array as $attribute) {
						if (empty($colArr) || !in_array($attribute['color_id'], $colArr)) {
							$color_id = $wpdb->get_var("SELECT term_id FROM $table_name WHERE slug='" . $attribute['color_id'] . "'");
							$colArr[] = $attribute['color_id'];
							$pvariants[$k]['sizeid'] = array();
							$pvariants[$k]['color_id'] = $color_id;
							$pvariants[$k]['sizeid'][] = $attribute['size'];
							$k++;
						} else {
							$key = array_search($attribute['color_id'], $colArr);
							$pvariants[$key]['sizeid'][] = $attribute['size'];
						}

					}
					$productArr['variants'] = $pvariants;
					$predecorateObj = Flight::predecorate();
					$templateData = $predecorateObj->saveTemplateInfo($productArr['conf_id'], $data['assignedCategory'], $data['captureType']);
					$this->customRequest(array('productid' => $data['simpleproduct_id'], 'isTemplate' => 1));
					$sides = sizeof($data['images']);
					$productTemplate = $this->getProductTemplateByProductId($data['simpleproduct_id']);

					if ($data['boundary_type'] == "single") {
						$maskData = $this->getMaskData($sides);
						$maskDatas = json_decode($maskData, true);
						$printArea = array();
						$printArea = $this->getPrintareaType($data['simpleproduct_id']);
						foreach ($maskDatas as $key => $maskData) {
	                        $maskScalewidth[$key] = $maskData['mask_width'];
	                        $maskScaleHeight[$key] = $maskData['mask_height'];
	                        $maskPrice[$key] = $maskData['mask_price'];
	                        $scaleRatio[$key] = $maskData['scale_ratio'];
	                        $scaleRatio_unit[$key] = $maskData['scaleRatio_unit'];
	                     }
	                    $this->customRequest(array('maskScalewidth' => $maskScalewidth, 'maskScaleHeight' => $maskScaleHeight, 'maskPrice' => $maskPrice, 'scaleRatio' => $scaleRatio, 'scaleRatio_unit' => $scaleRatio_unit, 'maskstatus' => $printArea['mask'], 'unitid' => $printArea['unit_id'], 'pricePerUnit' => $printArea['pricePerUnit'], 'maxWidth' => $printArea['maxWidth'], 'maxHeight' => $printArea['maxHeight'], 'boundsstatus' => $printArea['bounds'], 'customsizestatus' => $printArea['custom_size'], 'customMask' => $printArea['customMask']));
	                    $printSizes = $this->getDtgPrintSizesOfProductSides($data['simpleproduct_id']);
						$printSizes = $this->getDtgPrintSizesOfProductSides($data['simpleproduct_id']);
						$this->customRequest(array('productid' => $productArr['conf_id'], 'jsondata' => json_encode($maskDatas), 'printsizes' => $printSizes));
						$this->saveMaskData();
						if ($printSizes['status'] != 'nodata') {
							$this->setDtgPrintSizesOfProductSides();
						}
					}else{
                         // multiple boundary set up
                        $multipleObj = Flight::multipleBoundary();
                        $productArr['old_conf_id'] = $data['simpleproduct_id'];
                        $multiBoundData = $multipleObj->getMultiBoundMaskData($productArr['old_conf_id']);
                        $multiBoundData[0]['id'] = 0;
                        $unitArr = array($multiBoundData[0]['scaleRatio_unit']);
                        $saveStatus = $multipleObj->saveMultipleBoundary($productArr['conf_id'], json_encode($multiBoundData), $unitArr, true);
                    }
					
					$this->saveProductTemplateData($data['print_method_id'], $data['ref_id'], $data['simpleproduct_id'], $productArr['conf_id']);
					if (!empty($productTemplate['tepmlate_id'])) {
						$this->customRequest(array('pid' => $productArr['conf_id'], 'productTempId' => $productTemplate['tepmlate_id']));
						$this->addTemplateToProduct();
					}
				} catch (Exception $e) {
					$result = json_encode(array('isFault' => 1, 'faultMessage' => $e->getMessage()));
					$error = true;
				}
				if (!$error) {
					$this->response($this->json($productArr), 200);
				} else {
					$this->response($this->formatJSONToArray($result), 200);
				}
			} else {
				$msg = array('status' => 'apiLoginFailed', 'error' => json_decode($result));
				$this->response($this->json($msg), 200);
			}
		}
	}

	/**
	 * Get Category list by product id
	 *
	 * @param   pid
	 * @return  category list in json format
	 */
	public function getCategoriesByProduct()
	{
		header('HTTP/1.1 200 OK');
		//$error='';
		$printProfile = Flight::printProfile();
		$result = $this->storeApiLogin();
		if ($this->storeApiLogin == true && isset($this->_request['pid']) && $this->_request['pid']) {
			$key = $GLOBALS['params']['apisessId'];
			$res = array();
			try {
				$catIdArr = wp_get_post_terms($productId, 'product_cat', array('fields' => 'ids'));

				if (empty($catIdArr)) {
					$res = $printProfile->getDefaultPrintMethodId();
				} else {
					$catIdStr = implode(',', $catIdArr);
					//funn call inkxe db str
					$res = $this->getPrintMethodName($catIdStr);
					if (empty($res)) {
						$res = $printProfile->getDefaultPrintMethodId();
					}
				}
			} catch (Exception $e) {
				$res = json_encode(array('isFault' => 1, 'faultMessage' => $e->getMessage()));
			}

			$this->response(json_encode($res), 200);
		} else {
			$msg = array('status' => 'apiLoginFailed', 'error' => json_decode($result));
			$this->response($this->json($msg), 200);
		}

	}

	/**
	 * Get Sub-Category list
	 *
	 * @param   selectedCategory
	 * @return  sub-category list in json format
	 */
	public function getsubCategories()
	{
		$error = '';
		$result = $this->storeApiLogin();
		if ($this->storeApiLogin == true) {
			$key = $GLOBALS['params']['apisessId'];
			try {
				$taxonomy = 'product_cat';
				$orderby = 'name';
				$show_count = 0; // 1 for yes, 0 for no
				$pad_counts = 0; // 1 for yes, 0 for no
				$hierarchical = 1; // 1 for yes, 0 for no
				$title = '';
				$empty = 0;

				$args = array(
					'taxonomy' => $taxonomy,
					'child_of' => 0,
					'parent' => $this->_request['selectedCategory'],
					'orderby' => $orderby,
					'show_count' => $show_count,
					'pad_counts' => $pad_counts,
					'hierarchical' => $hierarchical,
					'title_li' => $title,
					'hide_empty' => $empty,
				);
				$sub_cats = get_categories($args);

				usort($sub_cats, function($a, $b) { //Sort the array using a user defined function
					return $a->name < $b->name ? -1 : 1; //Compare the scores
				});
				foreach ($sub_cats as $cat) {
					$catArr[] = array('id' => "" . $cat->term_id . "", 'name' => $cat->name);
				}
				$result = array('subcategories' => $catArr);
			} catch (Exception $e) {
				$result = json_encode(array('isFault' => 1, 'faultMessage' => $e->getMessage()));
				$error = true;
			}

			if (!$error) {
				$categories = array();
				$this->response($this->json($result), 200);
			} else {
				$this->response($this->formatJSONToArray($result), 200);
			}
		} else {
			$msg = array('status' => 'apiLoginFailed', 'error' => $this->formatJSONToArray($result));
			$this->response($this->json($msg), 200);
		}

	}

	/**
	 * Get product count
	 *
	 * @param   orderIncrementId
	 * @return  integer number of product
	 */
	public function getProductCount()
	{
		$error = false;
		$result = $this->wcApi->get_products_count();
		if (!isset($result->errors)) {
			try {
				$result = array('size' => $result->count);
			} catch (Exception $e) {
				$result = json_encode(array('isFault' => 1, 'faultMessage' => $e->getMessage()));
				$error = true;
			}
			if (!$error) {
				$this->response($this->json($result), 200);
			} else {
				$msg = array('status' => 'failed', 'error' => $this->formatJSONToArray($result));
				$this->response($this->json($msg), 200);
			}

		} else {

			$msg = array('status' => 'apiLoginFailed', 'error' => $result);
			$this->response($this->json($msg), 200);
		}
	}

	
	

	/**
	 *
	 *date created (dd-mm-yy)
	 *date modified 15-4-2016(dd-mm-yy)
	 *fetch print method id and name
	 *
	 *@param (String)apikey
	 *@param (int)productid
	 *@return json data
	 *
	 */
	public function getProductPrintMethod()
	{
		$productId = $this->_request['productid'];
		$key = $this->_request['apikey'];
		if (!empty($productId)) {
			// &&  !empty($key) && $this->isValidCall($key)){
			$error = false;
			$productPrintType = $this->getProductPrintMethodType($productId);
			if (!empty($productPrintType)) {
				foreach ($productPrintType as $k2 => $v2) {
					$printDetails[$k2]['print_method_id'] = $v2['pk_id'];
					$printDetails[$k2]['name'] = $v2['name'];
				}
			} else {
				$result = $this->storeApiLogin();
				if ($this->storeApiLogin == true) {
					$key = $GLOBALS['params']['apisessId'];
					try {
						$catIds = wp_get_post_terms($productId, 'product_cat', array('fields' => 'ids'));
						$catIds = implode(',', (array) $catIds);
						$printDetails = $this->getPrintMethodDetailsByCategory($catIds);
					} catch (Exception $e) {
						$result = json_encode(array('isFault' => 1, 'faultMessage' => $e->getMessage()));
						$error = true;
					}

				} else {
					$msg = array('status' => 'apiLoginFailed', 'error' => json_decode($result));
					$this->response($this->json($msg), 200);
				}
			}
			if (!$error) {

				$resultArr = $printDetails;
				$result = json_encode($resultArr);
				$this->response($this->json($resultArr), 200);
			} else {
				$this->response($result, 200);
			}
		} else {
			$msg = array("status" => "invalid Product Id");
			$this->response($this->json($msg), 200);
		}
	}

	/**
	 *
	 *date created (dd-mm-yy)
	 *date modified 15-4-2016(dd-mm-yy)
	 *product is custmizable or not by product id
	 *
	 *@param (int)pid
	 *@return json data
	 *
	 */
	public function isCustomizable()
	{
		$result = $this->storeApiLogin();
		if ($this->storeApiLogin == true && isset($this->_request['pid']) && $this->_request['pid']) {
			$confProductId = $this->_request['pid'];
			$result = $this->wcApi->get_product($confProductId);
			$colorArr = array();
			foreach ($result->product->attributes as $key => $value) {
				if ($value->name == "xe_is_designer") {
					$colorArr = $value->options;
				}

			}
			if (!empty($colorArr) && in_array(1, $colorArr)) {
				$customizable = 1;
			} else {
				$customizable = 0;
			}

			$this->response($customizable, 200);
		} else {
			$msg = array('status' => 'apiLoginFailed', 'error' => json_decode($result));
			$this->response($this->json($msg), 200);
		}
	}
	/**
	 * Check magento version
	 *
	 * @param   nothing
	 * @return  string $version
	 */
	public function storeVersion()
	{
		$result = $this->storeApiLogin();
		if ($this->storeApiLogin == true) {
			try {
				return $version = 1;
			} catch (Exception $e) {
				$result = json_encode(array('isFault' => 1, 'faultMessage' => $e->getMessage()));
				$error = true;
			}
		} else {
			$msg = array('status' => 'apiLoginFailed', 'error' => json_decode($result));
			$this->response($this->json($msg), 200);
		}
	}

	/**
	 *
	 *date created 07-06-2016(dd-mm-yy)
	 *date modified (dd-mm-yy)
	 *Save product template data
	 *
	 *@param (Int)old productid
	 *@param (Int)new productid
	 *@param (Int)refId
	 *
	 */
	public function saveProductTemplateData($printMethodId, $refId, $oldId, $newId)
	{
		$apiKey = $this->_request['apikey'];
		if ($this->isValidCall($apiKey)) {
			try {
				$msg = $this->saveProductTemplateStateRel($printMethodId,$refId,$oldId,$newId);
				return $this->json($msg);
			} catch (Exception $e) {
				$result = array('Caught exception:' => $e->getMessage());
				$this->response($this->json($result), 200);
			}
		}
	}

	/**
	 * Used to get all attributes value details for simple product
	 * @return list of attributes
	*/
	public function getCustomOption()
	{
		$error = false;
		global $wpdb;
		$result = $this->storeApiLogin();
		$table_taxonomy = $wpdb->prefix . "term_taxonomy";
		if ($this->storeApiLogin == true) {

			if (!isset($this->_request['id']) || trim($this->_request['id']) == '') {
				$msg = array('status' => 'invalid productId', 'productId' => $this->_request['id']);
				$this->response($this->json($msg), 204);
			} else {
				$product_id = trim($this->_request['id']);
			}
			
			$result = $this->wcApi->get_product($product_id);

			if (!isset($result->errors)) {
				try {
					$variantsArr = array();
					$i = 0;
					$attributes = array();
					$attrId = 0;
					$attrType = "";
					$table_name = $wpdb->prefix . "terms";
					foreach ($result->product->attributes as $attribute) {
						if ($attribute->name != "xe_is_designer")
						{
							$attrDetails = $this->getAttributeDetailsByName($attribute->name);
							if (array_key_exists("id", $attrDetails))
							{
								$attrId = $attrDetails['id'];
								$attrType = $attrDetails['type'];
							}
							$attributes[$i]['option_title'] = $attribute->name;
							$attributes[$i]['option_id'] = $attrId;
							$attributes[$i]['type'] = $attrType;
							$attributes[$i]['is_require'] = 1;
							$attributes[$i]['option_values'] = array();
							foreach ($attribute->options as $key => $value) {
								$taxonomy_name = 'pa_' . $attribute->name;
								$option_type_id = $wpdb->get_var("SELECT ts.term_id FROM $table_taxonomy as ts join $table_name as tm on ts.term_id = tm.term_id WHERE ts.taxonomy = '$taxonomy_name' and tm.name = '$value'");
								$attributes[$i]['option_values'][$key]["option_type_id"] = $option_type_id;
								$attributes[$i]['option_values'][$key]["sku"] = null;
								$attributes[$i]['option_values'][$key]["sort_order"] = null;
								$attributes[$i]['option_values'][$key]["title"] = $value;
								$attributes[$i]['option_values'][$key]["price"] = null;
							}
							$i++;
						}
					}
					$result = $attributes;
				} catch (Exception $e) {
					$result = json_encode(array('isFault' => 1, 'faultMessage' => $e->getMessage()));
					$error = true;
				}
				$this->closeConnection();
				$this->response($this->json($result), 200);
			} else {
				$msg = array('status' => 'apiLoginFailed', 'error' => $result);
				$this->response($this->json($msg), 200);
			}

		} else {
			$msg = array('status' => 'apiLoginFailed', 'error' => $this->formatJSONToArray($result));
			$this->response($this->json($msg), 200);
		}
	}
	/**
	 * date created 23-05-2017(dd-mm-yy)
	 * date modified 06-06-2017(dd-mm-yy)
	 * Used to get all attributes
	 * @return list of attributes
	*/
	public function getAttributes()
	{
		$result = $this->storeApiLogin();
		if ($this->storeApiLogin == true) {
			$error = false;
			try {
				$result = $this->wcApi->get_products_attribute();
				$attributeArr = array();
				if (!isset($result->errors)) {
					foreach ($result->product_attributes as $key => $value) {
						$attributeArr[] = $arrayName = array('id' => $value->id,'attribute_code' => $value->name);
					}
				} else {
					$msg = array('status' => 'apiLoginFailed', 'error' => $result);
					$this->response($this->json($msg), 200);
				}
			} catch (Exception $e) {
				$result = json_encode(array('isFault' => 1, 'faultMessage' => $e->getMessage()));
				$error = true;
			}
			if (!$error) {
				$this->response($this->json($attributeArr), 200);
			} else {
				$msg = array('status' => 'failed', 'error' => $this->formatJSONToArray($result));
				$this->response($this->json($msg), 200);
			}
		} else {
			$msg = array('status' => 'apiLoginFailed', 'error' => json_decode($result));
			$this->response($this->json($msg), 200);
		}
	}

	/**
	 *
	 *date created (dd-mm-yy)
	 *date modified 15-4-2016(dd-mm-yy)
	 *product is disabled add to cart or not by product id
	 *
	 *@param (int)pid
	 *@return json data
	 *
	 */
	public function isDisabledAddToCart()
	{
		$result = $this->storeApiLogin();
		if ($this->storeApiLogin == true && isset($this->_request['pid']) && $this->_request['pid']) {
			$confProductId = $this->_request['pid'];
			$result = $this->wcApi->get_product($confProductId);
			$disableAddToCartArr = array();
			foreach ($result->product->attributes as $key => $value) {
				if ($value->name == "disabled_addtocart") {
					$disableAddToCartArr = $value->options;
				}
			}
			if (!empty($disableAddToCartArr) && in_array(1, $disableAddToCartArr)) {
				$isDisabled = 1;
			} else {
				$isDisabled = 0;
			}

			$this->response($isDisabled, 200);
		} else {
			$msg = array('status' => 'apiLoginFailed', 'error' => json_decode($result));
			$this->response($this->json($msg), 200);
		}
	}
	
	/**
	 *
	 *Created By : Ramasankar
	 *date created 26-09-2017(dd-mm-yy)
	 *date modified (dd-mm-yy)
	 *Returning List of Products from store products to getAllProducts() in products module
	 *Scope : this page 
	 *
	 *@param 
	 *@return Array data
	 *
	 */
	public function productList()
	{
		global $wpdb;
		global $woocommerce;
		$start = $this->_request['start'];
		$range = $this->_request['range'];
		/* if($start!=0 && $start!='')
		$page = ($start/$range)+1;
		else
		$page = 1; */
		$page = $this->_request['offset'];
		$filter = array();
		if ($range != '' && $range != 0) {
			$filter['filter[limit]'] = $range;
		}

		$filter['filter[page]'] = $page;
		$filter['per_page'] = $range;
		$filter['page'] = $page;
		$filter['attribute'] = 'xe_is_designer';
		$filter['attribute_term'] = 1;

		if ($this->_request['categoryid'] != '' && $this->_request['categoryid'] != 0) {
			$cat = $this->_request['categoryid'];
			$tableName = $wpdb->prefix . "terms";
			$slug = $wpdb->get_var("SELECT slug FROM $tableName WHERE term_id = '$cat'");
			$filter['filter[category]'] = $slug;
		}
		$error = false;

		$result = $this->wcApi->get_products($filter);

		$productsArr = array();
		if (!isset($result->errors)) {
			try {
				foreach ($result->products as $key => $value) {
					$refid = get_post_meta($value->id, 'refid', true);
					$variant = array();
					$catArr = $value->categories;
					$catArr = wp_get_post_terms($value->id, 'product_cat', array('fields' => 'ids'));
					foreach($value->attributes as $attribute)
					{
						if (isset($attribute->name) && $attribute->name == 'xe_is_designer' && in_array(1, $attribute->options))
						{
							if ((isset($this->_request['preDecorated']) && $this->_request['preDecorated'] == 'false')) {
								if ($refid == '') {
									if ($this->_request['searchstring'] != '') {
										$search = $this->_request['searchstring'];
										$search = stripslashes($search);
										if (($value->title == $search) || strlen(stristr($value->title, $search)) > 0 || in_array($search, $value->tags)) {
											$productsArr[] = array('id' => $value->id, 'name' => $value->title, 'description' => wp_strip_all_tags($value->description), 'price' => $value->price, 'thumbnail' => $value->images[0]->src, 'image' => $value->images[0]->src, 'category' => $catArr);
											//$count++;
										}
									} else {
										$productsArr[] = array('id' => $value->id, 'name' => $value->title, 'description' => wp_strip_all_tags($value->description), 'price' => $value->price, 'thumbnail' => $value->images[0]->src, 'image' => $value->images[0]->src, 'category' => $catArr);
										//$count++;
									}
								}
							} else {
								if ($this->_request['searchstring'] != '') {
									$search = $this->_request['searchstring'];
									$search = stripslashes($search);
									if (($value->title == $search) || strlen(stristr($value->title, $search)) > 0 || in_array($search, $value->tags)) {
										$productsArr[] = array('id' => $value->id, 'name' => $value->title, 'description' => wp_strip_all_tags($value->description), 'price' => $value->price, 'thumbnail' => $value->images[0]->src, 'image' => $value->images[0]->src, 'category' => $catArr);
										//$count++;
									}
								} else {
									$productsArr[] = array('id' => $value->id, 'name' => $value->title, 'description' => wp_strip_all_tags($value->description), 'price' => $value->price, 'thumbnail' => $value->images[0]->src, 'image' => $value->images[0]->src, 'category' => $catArr);
									//$count++;
								}
							}
						}
					}
				}
				$res = array('product' => $productsArr);
			}catch (Exception $e) {
				$res = array('isFault' => 1, 'faultMessage' => $e->getMessage());
			}
			return $res;
		}
		else
		{
			$res=array('isFault' => 2, 'error' => $result);
			return $res;
		}
	}



	/**
	 *
	 *date created 27-09-2017(dd-mm-yy)
	 *date modified (dd-mm-yy)
	 *Returning category details from store to getCategories()
	 *Scope : this page 
	 *
	 *@param 
	 *@return Array data
	 *
	 */
	public function allCategories()
	{
		try{
			$taxonomy = 'product_cat';
			$orderby = 'name';
			$showCount = 0; // 1 for yes, 0 for no
			$padCounts = 0; // 1 for yes, 0 for no
			$hierarchical = 1; // 1 for yes, 0 for no
			$title = '';
			$empty = 0;

			$args = array(
				'taxonomy' => $taxonomy,
				'orderby' => $orderby,
				'parent' => 0,
				'show_count' => $showCount,
				'pad_counts' => $padCounts,
				'hierarchical' => $hierarchical,
				'title_li' => $title,
				'hide_empty' => $empty,
			);
			$allCategories = get_categories($args);
			//inkxe
			foreach($allCategories as $k=>$v){
				$allCategories[$k]->id = $v->term_id;
			}
			//inkxe
			usort($allCategories, function($a, $b) { //Sort the array using a user defined function
				return $a->name < $b->name ? -1 : 1; //Compare the scores
			});
			
			$allCategories = (array) $allCategories;
			return $allCategories;
		}catch (Exception $e) {
			$result = array('isFault' => 1, 'faultMessage' => $e->getMessage());
			return $result;
		}
	}

	/**
	 *
	 *date created 27-09-2017(dd-mm-yy)
	 *date modified (dd-mm-yy)
	 *Returning product print method parameter details to getPrintMethodByProduct()
	 *Scope : this page 
	 *
	 *@param 
	 *@return Array data
	 *
	 */
	public function printMethodParameters()
	{
		$result = $this->storeApiLogin();
		if ($this->storeApiLogin == true && isset($this->_request['pid']) && $this->_request['pid']) {
			$key = $GLOBALS['params']['apisessId'];
			$filters = array('store' => $this->getDefaultStoreId());
			$confProductId = $this->_request['pid'];
			$refid = get_post_meta($confProductId, 'refid', true);
			$isAdmin = (isset($this->_request['isAdmin']) && trim($this->_request['isAdmin']) == true) ? true : false;
			
			$catIds = wp_get_post_terms($confProductId, 'product_cat', array('fields' => 'ids'));
			$result=array('confProductId' => $confProductId, 'isAdmin' => $isAdmin, 'catIds' => $catIds, 'refid' => $refid);
			return $result;
		} else {
			$res=array('isFault' => 2, 'error' => json_decode($result));
            return $res;
		}
	}

	/**
	 *
	 *date created 27-09-2017(dd-mm-yy)
	 *date modified (dd-mm-yy)
	 *Returning details of a product
	 *Scope : this page 
	 *
	 *@param 
	 *@return Array data
	 *
	 */
	public function productDetails($client = '')
	{
		global $wpdb;
		$_tax = new WC_Tax();
		$pid = trim($this->_request['id']);
		if (!isset($this->_request['size']) || trim($this->_request['size']) == '') {
			$size = '';
		} else {
			$size = trim($this->_request['size']);
		}
		$configId = ($this->_request['confId']) ? $this->_request['confId'] : '';
		$proId = ($configId != '' && $configId != $pid) ? $configId : $pid;
		$attributes = array();
		$simpleProductId = '';
		if ($size != '') {
			$attributes['size'] = $size;
		}
		$colorAttr = strtolower($this->getStoreAttributes("xe_color"));
        $sizeAttr = strtolower($this->getStoreAttributes("xe_size"));
		$result = $this->wcApi->get_product($proId);
		if (!isset($result->errors)) {
			try {
				$productId = $result->product->id;
				$catArr = $result->product->categories;
				$catArray = array();
				$tableName = $wpdb->prefix . "terms";
				$tableName1 = $wpdb->prefix . "term_taxonomy";
				foreach ($catArr as $cat) {
					$ID = $wpdb->get_var("SELECT term_id FROM $tableName WHERE name='$cat'");
					$parent = $wpdb->get_var("SELECT parent FROM $tableName1 WHERE term_id=$ID");
					$catArray[] = ($parent) ? $parent : $ID;
				}
				$typesArr = array();
				$colorArr = array();
				if($client != ''){
					$iscustom = false;
				}
				foreach ($result->product->attributes as $key => $value) {
					if($client != ''){
						if (!$iscustom && count($value->options) > 1)
						{
							$iscustom = true;
						}
					}
					if ($value->name == "size") {
						$typesArr = $value->options;
					}
					if ($value->name == $colorAttr && $colorAttr != '') {
						$colorArr = $value->options;
					}
				}
				// Product Type
				if ($result->product->type == "simple") {
					if($client != ''){
						if ($iscustom)
							$ptype = "custom";
						else 
							$ptype = $result->product->type;
					}
					else{
						$ptype = $result->product->type;
					}
				} else {
					$ptype = "configurable";
				}   
				$productsArr = array('pid' => $result->product->id, 'pidtype' => $ptype, 'pname' => $result->product->title, 'category' => $catArray);
				$tableName = $wpdb->prefix . "terms";
				if($client != ''){
					$tableTaxonomy = $wpdb->prefix . "term_taxonomy";
					$taxTable = $wpdb->prefix . "options";
					$taxEnabled = $wpdb->get_var("SELECT option_value FROM $taxTable WHERE option_name='woocommerce_calc_taxes'");
				}
				$image = array();
				if ($configId != '' && $configId != $pid) {
					foreach ($result->product->variations as $variants) {
						if ($variants->id == $pid) {
							$pvariant = $variants;
							$taxRate = $_tax->get_rates($pvariant->tax_class);
							$tax = 0;
							if($client != ''){
								if ($taxEnabled == 'yes') {
									foreach ($taxRate as $value) {
										$tax += $value['rate'];
									}
								}
							}
							else{
								foreach ($taxRate as $value) {
									$tax += $value['rate'];
								}
							}
							$productsArr['pvid'] = $pvariant->id;
							$productsArr['pvname'] = $result->product->title;
							$productsArr['quanntity'] = $pvariant->stock_quantity;
							$productsArr['price'] = $pvariant->price;
							$productsArr['taxrate'] = $tax;
							$productsArr['xecolor'] = "";
							$productsArr['xesize'] = "";
							$productsArr['xe_color_id'] = 0;
							$productsArr['xe_size_id'] = 0;
							
							if($client != ''){
								foreach ($pvariant->attributes as $attribute) {
									$taxonomyName = 'pa_' . $attribute->name;
									$attrId = $wpdb->get_var("SELECT ts.term_id FROM $tableTaxonomy as ts join $tableName as tm on ts.term_id = tm.term_id WHERE ts.taxonomy = '$taxonomyName' and tm.slug = '$attribute->option'");
									$attrName = $wpdb->get_var("SELECT name FROM $tableName WHERE term_id=$attrId");
									
									if ($attribute->name == $colorAttr) {
										if (!empty($colorArr) && in_array('#' . $attribute->option, $colorArr)) {
											$productsArr['xecolor'] = '#' . $attribute->option;
										} else {
											$productsArr['xecolor'] = $attrName;
										}
									} else {
										if ($attribute->name == $sizeAttr) {
											$productsArr['xesize'] = $attrName;
										}
									}
									if ($attribute->name == $colorAttr)
										$productsArr['xe_color_id'] = $attrId;
									else if ($attribute->name == $sizeAttr)
										$productsArr['xe_size_id'] = $attrId;
									if ($attribute->option != "") {
										$productsArr['attributes'][$attribute->name] = $attrName;
										$productsArr['attributes'][$attribute->name . '_id'] = $attrId;
									}
								}

									// for color attribute
								if ($productsArr['xecolor'] == "") {
									$productsArr['attributes']['xe_color'] = "";
									$productsArr['attributes']['xe_color_id'] = 0;
								}
								// for size attribute
								if ($productsArr['xesize'] == "") {
									$productsArr['attributes']['xe_size'] = "";
									$productsArr['attributes']['xe_size_id'] = 0;
								}
							}
							else{
								foreach ($pvariant->attributes as $attribute) {
									if ($attribute->name == $sizeAttr && $sizeAttr != '') {
										$productsArr['xesize'] = strtoupper($attribute->option);
										$sizeId = $wpdb->get_var("SELECT term_id FROM $tableName WHERE slug='$attribute->option'");
										$productsArr['xe_size_id'] = $sizeId;
									}
									if ($attribute->name == $colorAttr && $colorAttr != '') {
										if (!empty($colorArr) && in_array('#' . $attribute->option, $colorArr)) {
											$productsArr['xecolor'] = '#' . $attribute->option;
										} else {
											$productsArr['xecolor'] = $attribute->option;
										}

										$color_id = $wpdb->get_var("SELECT term_id FROM $tableName WHERE slug='$attribute->option'");
										$productsArr['xe_color_id'] = $color_id;
									}
								}
							}
							$attachments = get_post_meta($pvariant->id, 'variation_image_gallery', true);
							$attachmentsExp = array_filter(explode(',', $attachments));
							$variantImg = get_post_meta($pvariant->id, '_thumbnail_id', true);
							if (!empty($pvariant->image) && $variantImg != 0) {
								foreach ($pvariant->image as $img) {
									$image[] = $img->src;
								}
							}
							foreach ($attachmentsExp as $id) {
								$imageSrc = wp_get_attachment_image_src($id, 'full');
								$image[] = $imageSrc[0];
							}
						}
					}
				}else {
					// For Configure Product
					if (!empty($result->product->variations)){
						$pvariant = $result->product->variations[0];
						$taxRate = $_tax->get_rates($pvariant->tax_class);
						$tax = 0;
						foreach ($taxRate as $value) {
							$tax += $value['rate'];
						}
						$productsArr['pvid'] = $pvariant->id;
						$productsArr['pvname'] = $result->product->title;
						$productsArr['quanntity'] = $pvariant->stock_quantity;
						$productsArr['price'] = $pvariant->price;
						$productsArr['taxrate'] = $tax;
						$productsArr['xecolor'] = "";
						$productsArr['xesize'] = "";
						$productsArr['xe_color_id'] = 0;
						$productsArr['xe_size_id'] = 0;
						if($client != ''){
							foreach ($pvariant->attributes as $attribute) {
								$taxonomyName = 'pa_' . $attribute->name;
								$attrId = $wpdb->get_var("SELECT ts.term_id FROM $tableTaxonomy as ts join $tableName as tm on ts.term_id = tm.term_id WHERE ts.taxonomy = '$taxonomyName' and tm.slug = '$attribute->option'");
								$attrName = $wpdb->get_var("SELECT name FROM $tableName WHERE term_id=$attrId");
								if ($attribute->name == $colorAttr) {
									$productsArr['xe_color_id'] = $attrId;
									if (!empty($colorArr) && in_array('#' . $attribute->option, $colorArr)) {
										$productsArr['xecolor'] = '#' . $attribute->option;
									} else {
										$productsArr['xecolor'] = $attrName;
									}
								} else {
									if ($attribute->name == $sizeAttr) {
										$productsArr['xesize'] = $attrName;
										$productsArr['xe_size_id'] = $attrId;
									}
								}
								if ($attribute->option != "") {
									$productsArr['attributes'][$attribute->name] = $attrName;
									$productsArr['attributes'][$attribute->name . '_id'] = $attrId;
								}
							}

							// for color attribute
							if ($productsArr['xecolor'] == "") {
								$productsArr['attributes']['xe_color'] = "";
								$productsArr['attributes']['xe_color_id'] = 0;
							}
							// for size attribute
							if ($productsArr['xesize'] == "") {
								$productsArr['attributes']['xe_size'] = "";
								$productsArr['attributes']['xe_size_id'] = 0;
							}
						}
						else{
							foreach ($pvariant->attributes as $attribute) {
								if ($attribute->name == $sizeAttr && $sizeAttr != '') {
									$productsArr['xesize'] = strtoupper($attribute->option);
									$sizeId = $wpdb->get_var("SELECT term_id FROM $tableName WHERE slug='$attribute->option'");
									$productsArr['xe_size_id'] = $sizeId;
								}
								if ($attribute->name == $colorAttr && $colorAttr != '') {
									if (!empty($colorArr) && in_array('#' . $attribute->option, $colorArr)) {
										$productsArr['xecolor'] = '#' . $attribute->option;
									} else {
										$productsArr['xecolor'] = $attribute->option;
									}
									$color_id = $wpdb->get_var("SELECT term_id FROM $tableName WHERE slug='$attribute->option'");
									$productsArr['xe_color_id'] = $color_id;
								}
							}
						}
						$attachments = get_post_meta($pvariant->id, 'variation_image_gallery', true);
						$attachmentsExp = array_filter(explode(',', $attachments));
						$variantImg = get_post_meta($pvariant->id, '_thumbnail_id', true);
						if (!empty($pvariant->image) && $variantImg != 0) {
							foreach ($pvariant->image as $img) {
								$image[] = $img->src;
							}
						}
						foreach ($attachmentsExp as $id) {
							$imageSrc = wp_get_attachment_image_src($id, 'full');
							$image[] = $imageSrc[0];
						}
					} else {
						// For Simple Product
						$pvariant = $result->product;
						$taxRate = $_tax->get_rates($pvariant->tax_class);
						$tax = 0;
						if($client != ''){
							if ($taxEnabled == 'yes') {
								foreach ($taxRate as $value) {
									$tax += $value['rate'];
								}
							}
						}
						else{
							foreach ($taxRate as $value) {
								$tax += $value['rate'];
							}
						}
						$productsArr['pvid'] = $pvariant->id;
						$productsArr['pvname'] = $result->product->title;
						$productsArr['quanntity'] = $pvariant->stock_quantity;
						$productsArr['price'] = $pvariant->price;
						$productsArr['taxrate'] = $tax;
						$productsArr['xesize'] = "";
						$productsArr['xecolor'] = "";
						$productsArr['xe_size_id'] = 0;
						$productsArr['xe_color_id'] = 0;
						if($client != ''){
							foreach ($pvariant->attributes as $attribute) {
								$taxonomyName = 'pa_' . $attribute->name;
								$attrnamedata = $attribute->options['0'];
								$attrId = $wpdb->get_var("SELECT ts.term_id FROM $tableTaxonomy as ts join $tableName as tm on ts.term_id = tm.term_id WHERE ts.taxonomy = '$taxonomyName' and tm.name ='$attrnamedata'");
								$attrName = $wpdb->get_var("SELECT name FROM $tableName WHERE term_id=$attrId");
								if ($ptype != "custom") {
									if (strtolower($attribute->name) == $colorAttr) {
										if (!empty($colorArr) && in_array('#' . $attribute->options, $colorArr)) {
											$productsArr['xecolor'] = '#' . $attribute->options['0'];
										} else {
											$productsArr['xecolor'] = $attrName;
										}
									}
									if ($attribute->name == $sizeAttr) {
										$productsArr['xesize'] = $attrName;
										$productsArr['xe_size_id'] = $attrId;
									}
									if ( strtolower($attribute->name) == strtolower($colorAttr )) {
										$productsArr['xe_color_id'] = $attrId;
									}
								}
								if ($attribute->options != "" && $attribute->name != "xe_is_designer") {
									$productsArr['attributes'][$attribute->name] = $attrName;
									$productsArr['attributes'][$attribute->name . '_id'] = $attrId;
								}
							}
						}
						else{
							foreach ($pvariant->attributes as $attribute) {
								if ($attribute->name == $sizeAttr && $sizeAttr != '') {
									$productsArr['xesize'] = strtoupper($attribute->options['0']);
									$sizeSlug = $attribute->options['0'];
									$sizeId = $wpdb->get_var("SELECT term_id FROM $tableName WHERE slug = '$sizeSlug'");
									$productsArr['xe_size_id'] = $sizeId;
								}
								if ($attribute->name == $colorAttr && $colorAttr != '') {
									if (!empty($colorArr) && in_array('#' . $attribute->options['0'], $colorArr)) {
										$productsArr['xecolor'] = '#' . $attribute->options['0'];
									} else {
										$productsArr['xecolor'] = $attribute->options['0'];
									}
									$colorSlug = $attribute->options['0'];
									$color_id = $wpdb->get_var("SELECT term_id FROM $tableName WHERE slug = '$colorSlug'");
									$productsArr['xe_color_id'] = $color_id;
								}
							}
						}
						// For Color Swatch
						if($client == ''){
							$colorId = $productsArr['xe_color_id'];						
							$productsArr['colorSwatch'] = $this->getColorSwatchProduct($colorId);
						}
						$attachments = get_post_meta($pvariant->id, 'variation_image_gallery', true);
						$attachmentsExp = array_filter(explode(',', $attachments));
						$variantImg = get_post_meta($pvariant->id, '_thumbnail_id', true);
						if (!empty($pvariant->images) && $variantImg != 0) {
							foreach ($pvariant->images as $img) {
								$image[] = $img->src;
							}
						}
						foreach ($attachmentsExp as $id) {
							$imageSrc = wp_get_attachment_image_src($id, 'full');
							$image[] = $imageSrc[0];
						}
					}
				}
				$productsArr['thumbsides'] = $image;
				$productsArr['sides'] = $image;
				$refid = get_post_meta($productId, 'refid', true);
				$productsArr['isPreDecorated'] = ($refid != '') ? true : false;
				if($client != ''){
					$productsArr['labels'] = array();
				}
				else{
					$cVariants = $variant;
					$cVariantsIds = array();
					for ($i = 0; $i < sizeof($cVariants); $i++) {
						array_push($cVariantsIds, $cVariants[$i]['data']['id']);
					}
					$features = array();
					$productsArr['features'] = $features;					
					if ($result->product->type == "simple")
						$productsArr['sizeAdditionalprices'] = [];
					else 
						$productsArr['sizeAdditionalprices'] = $this->getSizeVariantAdditionalPrice($proId);
				}
				$productsArr['minQuantity'] = 1;
				return $productsArr;
			}
			catch (Exception $e) {
				$result = array('isFault' => 1, 'faultMessage' => $e->getMessage());
				return $result;
			}
		}
		else
		{
			$res=array('isFault' => 2, 'error' => json_decode($result));
            return $res;
		}
	}
	
	/**
	 *
	 *date created 27-09-2017(dd-mm-yy)
	 *date modified (dd-mm-yy)
	 *Returning size and quantity of a product
	 *
	 *@param 
	 *@return Array data
	 *
	 */
	public function sizeAndQuantityDetails()
	{
		global $wpdb;
		$result = $this->storeApiLogin();
		if ($this->storeApiLogin == true) {
			$key = $GLOBALS['params']['apisessId'];
			$productId = trim($this->_request['productId']);
			$varientId = trim($this->_request['simplePdctId']);
			// get color attribute and size attribute.
			$colorAttr = $this->getStoreAttributes("xe_color");
			$sizeAttr = $this->getStoreAttributes("xe_size");
			$result = $this->wcApi->get_product($productId);

			if (!isset($result->errors)) {
				try {
					$variantsArr = array();
					$i = 0;
					$variant = array();
					$colorArr = array();
					$sizeArr = array();
					foreach ($result->product->attributes as $value) {
						if ($value->name == $colorAttr) {
							$colorArr = $value->options;
						}
						if ($value->name == $sizeAttr) {
							$sizeArr = $value->options;
						}

					}

					foreach ($result->product->variations as $variations) {
						$variant[$i]['id'] = $variations->id;
						foreach ($variations->attributes as $attribute) {
							if ($attribute->name == $colorAttr) {
								if ($variations->id == $varientId) {
									if (!empty($colorArr) && in_array('#' . $attribute->option, $colorArr)) {
										$color = '#' . $attribute->option;
									} else {
										$color = $attribute->option;
									}

								}
								if (!empty($colorArr) && in_array('#' . $attribute->option, $colorArr)) {
									$variant[$i][$attribute->name] = '#' . $attribute->option;
								} else {
									$variant[$i][$attribute->name] = $attribute->option;
								}
							} else {
								$variant[$i][$attribute->name] = $attribute->option;
							}
							$tableName = $wpdb->prefix . "terms";
							$attr_val = $variant[$i][$attribute->name];

							if ($attr_val != "") {
								$variant[$i]['attributes'][$attribute->name] = $attr_val;
							}
						}

						$variant[$i]['price'] = $variations->price;
						$variant[$i]['quantity'] = $variations->stock_quantity;
						$i++;
					}
					$j = 0;
					$size_array = array();
					// for sorting
					usort($variant, function($a, $b) use ($sizeArr){ //Sort the array using a user defined function
						$sizes = array();
						$sizeAttr1 = $this->getStoreAttributes("xe_size");
						foreach ($sizeArr as $key => $value) {
							$sizes[$value] = $key;
						}
						if ($sizes[$a[$sizeAttr1]] == $sizes[$b[$sizeAttr1]]) {
							return 0;
						}
						return ($sizes[$a[$sizeAttr1]] < $sizes[$b[$sizeAttr1]]) ? -1 : 1;
					});
					// end

					foreach ($variant as $var) {
						
						if (isset($this->_request['byAdmin'])) {
							if (empty($size_array) || !in_array($var['attributes'][$sizeAttr], $size_array)) {
								$size = $var[$sizeAttr];
								$xecolor = $var[$colorAttr];
								$tableName = $wpdb->prefix . "terms";
								$tableTaxonomy = $wpdb->prefix . "term_taxonomy";
								$colorTaxonomy = 'pa_' . $colorAttr;
								$sizeTaxonomy = 'pa_' . $sizeAttr;
								$colorId = $wpdb->get_var("SELECT ts.term_id FROM $tableTaxonomy as ts join $tableName as tm on ts.term_id = tm.term_id WHERE ts.taxonomy = '$colorTaxonomy' and tm.name = '$color'");
								$size_id = $wpdb->get_var("SELECT ts.term_id FROM $tableTaxonomy as ts join $tableName as tm on ts.term_id = tm.term_id WHERE ts.taxonomy = '$sizeTaxonomy' and tm.name = '$size'");
								$variantsArr[$j]['simpleProductId'] = $var['id'];
								$variantsArr[$j]['xe_size_id'] = $size_id;
								$variantsArr[$j]['xe_color_id'] = $colorId;
								$variantsArr[$j]['xe_color'] = $var[$colorAttr];
								$variantsArr[$j]['xe_size'] = $var[$sizeAttr];
								foreach ($var['attributes'] as $key => $value) {
									$attrId = $wpdb->get_var("SELECT ts.term_id FROM $tableTaxonomy as ts join $tableName as tm on ts.term_id = tm.term_id WHERE ts.taxonomy = 'pa_$key' and tm.name = '$value'");
									$attrName = $wpdb->get_var("SELECT name FROM $tableName WHERE term_id = $attrId");
									if ($key == $colorAttr) {
										$variantsArr[$j]['attributes']['xe_color'] = $attrName;
										$variantsArr[$j]['attributes']['xe_color_id'] = $attrId;
									} else if ($key == $sizeAttr) {
										$variantsArr[$j]['attributes']['xe_size'] = $attrName;
										$variantsArr[$j]['attributes']['xe_size_id'] = $attrId;
									} else {
										$variantsArr[$j]['attributes'][$key] = $attrName;
										$variantsArr[$j]['attributes'][$key . '_id'] = $attrId;
									}
								}
								$variantsArr[$j]['quantity'] = $var['quantity'];
								$variantsArr[$j]['price'] = $var['price'];
								$variantsArr[$j]['minQuantity'] = 1;
								$size_array[] = $var['attributes'][$sizeAttr];
								$j++;
							}
						} else {
							if ($var[$colorAttr] == $color) {

								$size = $var[$sizeAttr];
								$tableName = $wpdb->prefix . "terms";
								$tableTaxonomy = $wpdb->prefix . "term_taxonomy";
								$colorTaxonomy = 'pa_' . $colorAttr;
								$sizeTaxonomy = 'pa_' . $sizeAttr;

								$colorId = $wpdb->get_var("SELECT ts.term_id FROM $tableTaxonomy as ts join $tableName as tm on ts.term_id = tm.term_id WHERE ts.taxonomy = '$colorTaxonomy' and tm.name = '$color'");

								$size_id = $wpdb->get_var("SELECT ts.term_id FROM $tableTaxonomy as ts join $tableName as tm on ts.term_id = tm.term_id WHERE ts.taxonomy = '$sizeTaxonomy' and tm.slug = '$size'");
								$variantsArr[$j]['simpleProductId'] = $var['id'];
								$variantsArr[$j]['xe_size_id'] = $size_id;
								$variantsArr[$j]['xe_color_id'] = $colorId;
								$variantsArr[$j]['xe_color'] = $var[$colorAttr];

								$sizeAttrName = $wpdb->get_var("SELECT name FROM $tableName WHERE term_id = '$size_id'");

								$variantsArr[$j]['xe_size'] = $sizeAttrName;
								foreach ($var['attributes'] as $key => $value) {
									$attrId = $wpdb->get_var("SELECT ts.term_id FROM $tableTaxonomy as ts join $tableName as tm on ts.term_id = tm.term_id WHERE ts.taxonomy = 'pa_$key' and tm.slug = '$value'");
									$attrName = $wpdb->get_var("SELECT name FROM $tableName WHERE term_id = '$attrId'");
									if ($key == $colorAttr) {
										$variantsArr[$j]['attributes']['xe_color'] = $attrName;
										$variantsArr[$j]['attributes']['xe_color_id'] = $attrId;
									} else if ($key == $sizeAttr) {
										$variantsArr[$j]['attributes']['xe_size'] = $attrName;
										$variantsArr[$j]['attributes']['xe_size_id'] = $attrId;
									} else {
										$variantsArr[$j]['attributes'][$key] = $attrName;
										$variantsArr[$j]['attributes'][$key . '_id'] = $attrId;
									}
								}
								$variantsArr[$j]['quantity'] = $var['quantity'];
								$variantsArr[$j]['price'] = $var['price'];
								$variantsArr[$j]['minQuantity'] = 1;

								$j++;
							}
						}
					}
					//get variant price	

					return $variantsArr;
					
				} catch (Exception $e) {
					$result = array('isFault' => 1, 'faultMessage' => $e->getMessage());
					return $result;
				}
				//$this->closeConnection();
				//$this->response($this->json($result), 200);
			} else {
				$res=array('isFault' => 2, 'error' => $result);
            	return $res;
			}

		} else {
			$res=array('isFault' => 2, 'error' => $this->formatJSONToArray($result));
            return $res;
		}

	}

	/**
	 *
	 *date created 27-09-2017(dd-mm-yy)
	 *date modified (dd-mm-yy)
	 *Returning Veriant Details 
	 *Scope : this page 
	 *
	 *@param 
	 *@return Array data
	 *
	 */
	public function variantDetails()
	{
		global $wpdb;
		$limit = 1; // default values
		$start = 0; // default values
		if (isset($this->_request['start']) && trim($this->_request['start']) != '') {
			$start = trim($this->_request['start']);
		}
		if (isset($this->_request['range']) && trim($this->_request['range']) != '') {
			$limit = trim($this->_request['range']);
		}
		$startIndex = (isset($this->_request['offset'])) ? (int) $this->_request['range'] * ((int) $this->_request['offset'] - 1) : 0;
		$result = $this->storeApiLogin();
		$confId = $this->_request['conf_pid'];
		// get color attributes.
		$colorAttr = $this->getStoreAttributes("xe_color");
		if ($this->storeApiLogin == true) {
			$key = $GLOBALS['params']['apisessId'];
			try {
				$filters = array(
					'confId' => $confId,
					'range' => array('start' => $start, 'range' => $limit),
				);
				$result = $this->wcApi->get_product($confId);
				if (!isset($result->errors)) {
					$i = 0;
					$variant = array();
					$color = array();
					foreach ($result->product->attributes as $attribute) {
						if ($attribute->name == $colorAttr) {
							$color = $attribute->option;
						}

					}
					$colorArray = array();
					foreach ($result->product->variations as $variations) {
						foreach ($variations->attributes as $attribute) {
							if ($attribute->name == $colorAttr) {
								if (empty($variant) || (!empty($variant) && !in_array($attribute->option, $colorArray))) {

									if (!empty($color) && in_array('#' . $attribute->option, $color)) {
										$variant[$i]['xeColor'] = '#' . $attribute->option;
									} else {
										$variant[$i]['xeColor'] = $attribute->option;
									}

									$colorArray[] = $attribute->option;

									$tableName = $wpdb->prefix . "terms";
									$tableName1 = $wpdb->prefix . "term_taxonomy";
									$colorOption = $attribute->option;
									$colorTaxonomy = "pa_" . $colorAttr;
									$termId = $wpdb->get_var("SELECT t.term_id FROM $tableName t LEFT JOIN $tableName1 tt ON (t.term_id = tt.term_id) WHERE tt.taxonomy = '".$colorTaxonomy."' AND (t.slug='".$colorOption."' OR lower(t.name) = '".strtolower($colorOption)."')");
									$variant[$i]['colorUrl'] = $termId . '.png';
									$variant[$i]['id'] = $variations->id;
									$variant[$i]['name'] = $result->product->title;
									$variant[$i]['price'] = $variations->price;
									$variant[$i]['image'] = $variations->image[0]->src;
									$variant[$i]['xe_color_id'] = $termId;
									$i++;
								}
							}
						}
					}
					$variants = array();
					$j = 0;
					$length = sizeof($variant);
					$range = (isset($this->_request['range'])) ? $this->_request['range'] : $length;
					while ($startIndex < $length) {
						if ($j < $range) {
							$variants[$j]['id'] = $variant[$startIndex]['id'];
							$variants[$j]['name'] = $variant[$startIndex]['name'];
							$variants[$j]['price'] = $variant[$startIndex]['price'];
							$variants[$j]['xeColor'] = $variant[$startIndex]['xeColor'];
							$variants[$j]['xe_color_id'] = $variant[$startIndex]['xe_color_id'];
							$variants[$j]['colorUrl'] = $variant[$startIndex]['colorUrl'];
							$variants[$j]['thumbnail'] = $variant[$startIndex]['image'];
							$j++;
							$startIndex++;
						} else {
							break;
						}
					}
					$result = array('variants' => $variants, 'count' => $length);
					return $result;
				}
			} catch (Exception $e) {
				$resultArr = array('isFault' => 1, 'faultMessage' => $e->getMessage());
				return $resultArr;
			}
		} else {
			$res=array('isFault' => 2, 'error' => $this->formatJSONToArray($result));
            return $res;
		}
	}
}