<?php
/* Check Un-authorize Access */
if (!defined('accessUser')) {
    die("Error");
}

class CartsStore extends UTIL
{
    public function __construct()
    {
        parent::__construct();
        $this->storeApi = new storeApi();
    }
    /**
     * Add customized product to cart
     *
     * @param   product information
     * @return  boolean true/false
     *
     */
    public function addToCart()
    {
        $original_mem = ini_get('memory_limit');
        $mem = substr($original_mem, 0, -1);
        if ($original_mem <= $mem) {
            $mem = $mem + 1024;
            ini_set('memory_limit', $mem . 'M');
            set_time_limit(0);
        }
        $error = false;
		$cartId  = '';
        $result = $this->storeApiLogin();
        if ($this->storeApiLogin == true) {
            $key = $GLOBALS['params']['apisessId'];
            $isTemplate = 0;
            if (isset($this->_request['isTemplate'])) {
                $refid = $this->_request['refid'];
                $isTemplate = $this->_request['isTemplate'];
            }
            if (isset($this->_request['cartEncData'])) {
                $cartData = json_decode(stripslashes($this->rcEncDec5(self::POST_KEY, $this->_request['cartEncData'])));
                $msg = array('status' => 'checking', 'cartData' => $this->_request['cartEncData']);
                $this->response($this->json($msg), 200);
                $apikey = $cartData['apikey'];
                $designData = $cartData['designData'];
                $productDataJSON = $cartData['productData'];
				$cartId = $cartData['cartId']?$cartData['cartId']:'';
            } else {
                $apikey = $this->_request['apikey'];
                $designData = $this->_request['designData'];
                $productDataJSON = $this->_request['productData'];
				$cartId = $this->_request['cartId']?$this->_request['cartId']:'';
            }
            $designData = urldecode($designData);
            $productDataJSON = urldecode($productDataJSON);
            $cartArr = json_decode($productDataJSON, true);
            if ($isTemplate == 0) {
                $refid = $this->saveDesignStateCart($apikey, $refid, $designData); // private
                if ($refid > 0) {
                    $dbstat = $this->saveProductPreviewSvgImagesOnAddToCart($apikey, $refid, $designData);
                }
            }
            $arrProducts = array();
            foreach ($cartArr as $arkey => $value) {
                $product = $this->buildProductArray($value, $refid);
                if ($product) {
                    array_push($arrProducts, $product);
                }
            }
            ini_set('memory_limit', $original_mem);
            if (!$error) {
                $designUrl = $this->getCustomPreviewImages($refid, 1);
                $customImageUrl = $designUrl[$refid][0]['customImageUrl'];
                $lineItems = array();
                $colorAttribute = $this->getStoreAttributes("xe_color");
                $sizeAttribute = $this->getStoreAttributes("xe_size");
                foreach ($arrProducts as $ap) {
                    if($ap['qty']>0){
                        $lineItems[] = array(
                            "quantity" => $ap['qty'],
                            "total_quantity" => $ap['total_qty'],
                            "product_id" => $ap['product_id'],
                            "variant_id" => $ap['simpleproduct_id'],
                            "design_url" => $customImageUrl,
                            "added_price" => $ap['custom_price'],
                            "colorAttribute" => $colorAttribute,
                            "sizeAttribute" => $sizeAttribute,
                            "options" => $ap['options']
                         );
                    }
                }
                $newProductItems = $this->storeApi->addCartProduct($lineItems);
                $cartDetails = $this->storeApi->addToCart($newProductItems,$cartId);
                $cartDetails['sku'];
                $refIdDetails = array();
				$i =0;
                foreach ($cartDetails['item_id'] as $itemId) {
                    $refIdDetails[] = array(
                        'cart_id' => $cartDetails["cart_id"],
                        'ref_id' => $refid,
                        'item_id' => $itemId,
                        'custom_price' => 0,
						'sku'=>$cartDetails['sku'][$i],
                        'bulk_price_status'=>$cartDetails['bulk_price_status'][$i],
						'old_variant_id'=>$cartDetails['old_variant_id'][$i],
						'product_name'=>$cartDetails['old_product_name'][$i]
                    );
					$i++;
                }
                $addCartData = $this->putCartData($refIdDetails);
                if($addCartData){
                    $url = $cartDetails['cart_url'];
                    $msg = array('status' => 'success', 'url' => $url, 'refid' => 0, 'productData' => ''); 
                }else{
                    $msg = array('status' => 'failed', 'error' => 'Database Error');
                } 
                $this->response($this->json($msg), 200);
            } else {
                $msg = array('status' => 'failed', 'error' => json_decode($result));
                $this->response($this->json($msg), 200);
            }
        } else {
            $msg = array('status' => 'apiLoginFailed', 'error' => json_decode($result));
            $this->response($this->json($msg), 200);
        }
    }
    /**
     * Add predeco product in to store
     *
     * @param   (Int)vid
     * @param   (Int)pid
     * @param   (Int)refid
     * @param   (Int)orderQty
     * @return  String/Array
     */
    public function addTemplateToCart()
    {
        $error = false;
        $result = $this->storeApiLogin();
        if ($this->storeApiLogin == true) {
            try {
                $productInfo = array(
                    'configId' => $this->_request['pid'],
                    'smplProdID' => $this->_request['vid'],
                    'refid' => $this->_request['refid'],
                    'qty' => $this->_request['orderQty'],
                    'color' => $this->getStoreAttributes("xe_color"),
                    'size' => $this->getStoreAttributes("xe_size"),
                );
                $result = $this->storeApi->getProductInfo($productInfo);
            } catch (Exception $e) {
                $result = json_encode(array('isFault' => 1, 'faultMessage' => $e->getMessage()));
                $error = true;
            }
            $tarray = array(" ", "\n", "\r");
            if (file_exists($this->getBasePath() . "/localsettings.js")) {
                $contents = file_get_contents($this->getBasePath() . "/localsettings.js");
                $contents = trim(str_replace($tarray, "", $contents));
                $contents = substr($contents, 0, -1);
                $contents = explode("localSettings=", $contents);
                $contents = json_decode($contents['1'], true);
            }
            $this->_request['productData'] = $result;
            $this->_request['apikey'] = $contents['api_key'];
            $this->_request['isTemplate'] = 1;
            $this->_request['refid'] = $this->_request['refid'];
			$this->_request['cartId'] = $this->_request['cartId'];
            return $resultArr = $this->addToCart();
        } else {
            echo 'Add to cart failed';
        }
    }
    /**
     * Get no of cart item from store
     *
     * @param   nothing
     * @return  Array
     *
     */
    public function getTotalCartItem()
    {
        $store_api_result = $this->storeApiLogin();
        if ($this->storeApiLogin == true) {
            try {
                $cart_result = $this->storeApi->getTotalCartItem();
                $this->response($this->json($cart_result), 200);
            } catch (Exception $e) {
                $cart_result = json_encode(array('isFault' => 1, 'faultMessage' => $e->getMessage()));
                $this->response($cart_result, 200);
            }
        }
    }

    /**
     *
     *date created (dd-mm-yy)
     *date modified 15-4-2016 (dd-mm-yy)
     *Build Product Array
     *
     *@param (String)apikey
     *@param (Array)cartArr
     *@param (Int)refid
     *@return Array or boolean value
     *
     */
    public function buildProductArray($cartArr, $refid)
    {
        try {
            $configProductId = $cartArr['id'];
            $custom_price = $cartArr['addedprice'];
            //$cutom_design_refId = $cartArr['refid'];
            $cutom_design_refId = $refid;
            $quantity = $cartArr['qty'];
            $totalQuantity = $cartArr['totalQty'];
            $simpleProductId = $cartArr['simple_product']['simpleProductId'];
            //$color1 = $cartArr['simple_product']['color1'];
            $xeColor = ($cartArr['simple_product']['xe_color'])?$cartArr['simple_product']['xe_color']:'';
            $xeSize = ($cartArr['simple_product']['xe_size'])?$cartArr['simple_product']['xe_size']:'';
            $product = array(
                "product_id" => $configProductId,
                "qty" => $quantity,
                "total_qty" => $totalQuantity,
                "simpleproduct_id" => $simpleProductId,
                "options" => array('xe_color' => $xeColor, 'xe_size' => $xeSize),
                "custom_price" => $custom_price,
                "custom_design" => $cutom_design_refId,
            );
            if ($quantity > 0) {
                return $product;
            } else {
                return false;
            }

        } catch (Exception $e) {
            $result = array('Caught exception:' => $e->getMessage());
            return $result;
        }
    }


    /**
     *
     *date created 16-11-2017(dd-mm-yy)
     *date modified (dd-mm-yy)
     *Put refid to cart_data
     *
     *@param (Varchar)cart_id
     *@param (Varchar)item_id
     *@param (Int)ref_id
     *@param (float)custom_price
     *@return true/false
     *
     */
    public function putCartData($refIdDetails)
    {
        try {
            $values = "";
            foreach ($refIdDetails as $key => $value) {
                $cartId = $this->executeEscapeStringQuery($value['cart_id']);
                $refId = $value['ref_id'];
                $sku = $value['sku'];
                $itemId = $this->executeEscapeStringQuery($value['item_id']);
                $productName = $this->executeEscapeStringQuery($value['product_name']);
                $customPrice = $value['custom_price'];
                $oldVariantId = $value['old_variant_id'];
                $bulkPriceStatus = $value['bulk_price_status'];
                $sql0 = "Select * from  " . TABLE_PREFIX . "cart_data where cart_id='".$cartId."' and item_id='".$itemId."'";
                $result0 = $this->executeGenericCountQuery($sql0);
                if($result0 == 0 && $productName != ''){
                    $values .= "('".$cartId."','".$itemId."','".$refId."','".$customPrice."','".$sku."','".$productName."','".$oldVariantId."','".$bulkPriceStatus."'),";
                }
            }
            $values = substr(trim($values), 0, -1); 
            $sql = "INSERT INTO " . TABLE_PREFIX . "cart_data (cart_id, item_id, ref_id, custom_price, sku, product_name, old_variant_id, bulk_price_status) VALUES".$values;
            $status = $this->executeGenericDMLQuery($sql);
            if($status){
                return true;
            }else{
                return false;
            }
        }catch(Exception $e){
            $result = array('Caught exception:' => $e->getMessage());
            return false;
        }
    }

    /**
     *
     *date created 9-2-2017(dd-mm-yy)
     *date modified (dd-mm-yy)
     *Get name and number list by refid
     *
     *@param (Int)pid
     *@param (Int)refId
     *@return json array
     *
     */
    public function getRefIdByItemId()
    {
        try {
            $itemId = $this->_request['ItemId'];
            $itemId = $this->executeEscapeStringQuery($itemId);
            if(isset($itemId)){
                $sql = "Select ref_id,product_name,old_variant_id,bulk_price_status from  " . TABLE_PREFIX . "cart_data where item_id='".$itemId."'";
                $result = $this->executeGenericDQLQuery($sql);
                if (!empty($result)) {
                    $refId = $result[0]['ref_id'];
                    $results['refid'] =$refId;
                    $results['product_name'] =$result[0]['product_name'];
                    $results['old_variant_id'] =$result[0]['old_variant_id'];
                    $results['bulk_price_status'] =$result[0]['bulk_price_status'];
                }else{
                    $results['refid'] ='';
                    $results['product_name'] = '';
                    $results['old_variant_id'] =$result[0]['old_variant_id'];
                    $results['bulk_price_status'] =$result[0]['bulk_price_status'];
                }
            }else{
                $results = array('error:' => 'Cart Id And Item Id is not Set');
            }
            $this->response($this->json($results), 200);
        } catch (Exception $e) {
            $result = array('Caught exception:' => $e->getMessage());
            $this->response($this->json($result), 200);
        }
    }

    public function getCartIdByItemId()
    {
       try {
            $itemId = $this->_request['ItemId'];
            $itemId = $this->executeEscapeStringQuery($itemId);
            if(isset($itemId)){
                $sql = "Select cart_id from  " . TABLE_PREFIX . "cart_data where item_id='".$itemId."'";
                $result = $this->executeGenericDQLQuery($sql);
                if (!empty($result)) {
                    $refId = $result[0]['cart_id'];
                    $results['cart_id'] =$refId;
                }else{
                    $results['cart_id'] ='';
                }
            }else{
                $results = array('error:' => 'Cart Id And Item Id is not Set');
            }
            $this->response($this->json($results), 200);
        } catch (Exception $e) {
            $result = array('Caught exception:' => $e->getMessage());
            $this->response($this->json($result), 200);
        }
    }

    public function getRefidByOrderIdAndSku($cartId,$sku){
        $sql = "Select ref_id,product_name,sku from " . TABLE_PREFIX . "cart_data where cart_id='".$cartId."' and sku='".$sku."'";
        $result = $this->executeGenericDQLQuery($sql);
        return $result;
    }
}
