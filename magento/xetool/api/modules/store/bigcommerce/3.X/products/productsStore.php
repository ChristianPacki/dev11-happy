<?php
/* Check Un-authorize Access */
if (!defined('accessUser')) {
    die("Error");
}

class ProductsStore extends UTIL
{
    public function __construct()
    {
        parent::__construct();
        $this->storeApi = new storeApi();
    }
       
    /**
     * get all subcategories from store
     *
     * @param (int)selectedCategory
     * @return  json array
     */
    public function getsubCategories()
    {
        $error = '';
        $result = $this->storeApiLogin();
        if ($this->storeApiLogin == true) {
            $key = $GLOBALS['params']['apisessId'];
            try {
                $result = $this->storeApi->getsubCategories($this->_request['selectedCategory']);
            } catch (Exception $e) {
                $result = json_encode(array('isFault' => 1, 'faultMessage' => $e->getMessage()));
                $error = true;
            }
            if (!$error) {
                $categories = array();
                print_r($result);exit;
            } else {
                print_r(json_decode($result));exit;
            }
        } else {
            $msg = array('status' => 'apiLoginFailed', 'error' => json_decode($result));
            $this->response($this->json($msg), 200);
        }
    }
    
    /**
     * Check whether the given sku exists or doesn't
     *
     * @param   $sku_arr
     * @return  true/false
     */
    public function checkDuplicateSku()
    {
        // chk for storeid
        $error = false;
        $result = $this->storeApiLogin();
        if (!empty($this->_request) && $this->storeApiLogin == true) {
            if (!$error) {
                $filters = array(
                    'sku_arr' => $this->_request['sku_arr'],
                );
                try {
                    $result = $this->json(array());
                } catch (Exception $e) {
                    $result = json_encode(array('isFault inside apiv4: ' => 1, 'faultMessage' => $e->getMessage()));
                    $error = true;
                }
            }
            $this->closeConnection();
            print_r($result);exit;
        } else {
            $msg = array('status' => 'apiLoginFailed', 'error' => json_decode($result));
            $this->response($this->json($msg), 200);
        }
    }
    /**
     * Fetch all size attribute from store
     *
     * @param   nothing
     * @return  array contains all the xe_size inside store
     */

    public function getSizeArr()
    {
        $error = '';
        $result = $this->storeApiLogin();
        if ($this->storeApiLogin == true) {
            try {
                $size = $this->getStoreAttributes("xe_size");
                //Store Api
                $result = $this->storeApi->getSizeArr($size);
            } catch (Exception $e) {
                $result = json_encode(array('isFault' => 1, 'faultMessage' => $e->getMessage()));
                $error = true;
            }
            if (!$error) {
                $categories = array();
                print_r($result);exit;
            } else {
                print_r(json_decode($result));exit;
            }
        } else {
            $msg = array('status' => 'apiLoginFailed', 'error' => json_decode($result));
            $this->response($this->json($msg), 200);
        }
    }
    /**
     * Check whether xetool is enabled or disabled
     *
     * @param   nothing
     * @return  true/false
     */
    public function checkDesignerTool($t = 0)
    {
        if ($t) {
            return $result = 'Enabled';
        } else {
            return $result = 'Disabled';
        }

    }
    /**
     * Check magento version
     *
     * @param   nothing
     * @return  string $version
     */
    public function storeVersion()
    {
        $result = $this->storeApiLogin();
        if ($this->storeApiLogin == true) {
            try {
                $result = $this->storeApi->storeVersion();
                return $version = (!empty($result)) ? strchr($result, '.', true) : 1;
            } catch (Exception $e) {
                $result = json_encode(array('isFault' => 1, 'faultMessage' => $e->getMessage()));
                $error = true;
            }
        } else {
            $msg = array('status' => 'apiLoginFailed', 'error' => json_decode($result));
            $this->response($this->json($msg), 200);
        }
    }
    
    /**
     *
     *date created (dd-mm-yy)
     *date modified 15-4-2016(dd-mm-yy)
     *Fetch color swatches
     *
     * @param (String)apikey
     * @return json data
     *
     */
    public function fetchColorSwatch()
    {
        $isSameClass = true;
        $colorOptions = $this->getColorArr($isSameClass);
        if (!is_array($colorOptions)) {
            $colorOptions = $this->formatJSONToArray($colorOptions);
        }
        $dir = $this->getSwatchURL();
        $filePath = $this->getSwatchesPath();
        try {
            foreach ($colorOptions as $key => $value) {
                $swatchFilePath = $filePath . '/45x45/' . $value->value . '.png';
                $swatchFileDir = $dir . '45x45/' . $value->value . '.png';
                if (file_exists($swatchFilePath)) {
                    $colorOptions[$key]->swatchImage = $swatchFileDir;
                } else {
                    $colorOptions[$key]->swatchImage = '';
                }
                $colorOptions[$key]->width = 45;
                $colorOptions[$key]->height = 45;
            }
        } catch (Exception $e) {
            $result = array('Caught exception:' => $e->getMessage());
            $this->response($this->json($result), 200);
        }
        $this->response(json_encode($colorOptions), 200);
    }
    /**
     * Used to get all the color inside prestashop
     *
     * @param (Int)lastLoaded
     * @param (Int)loadCount
     * @return  array contains all the color inside store
     */
    public function getColorArr($isSameClass = false)
    {
        $productid = $this->_request['productId'] ? $this->_request['productId'] : 0;
        $error = '';
        $result = $this->storeApiLogin();
        if ($this->storeApiLogin == true) {
            try {
                $color = $this->getStoreAttributes("xe_color");
                $size = $this->getStoreAttributes("xe_size");
                $result = $this->storeApi->getColorArr($this->_request['lastLoaded'], $this->_request['loadCount'], $productid, $color, $size);
                print_r($result);exit;
            } catch (Exception $e) {
                $result = json_encode(array('isFault' => 1, 'faultMessage' => $e->getMessage()));
                $error = true;
            }
            if (!$error) {
                if ($isSameClass) {
                    return $result;
                } else {
                    print_r($result);exit;
                }
            } else {
                print_r(json_decode($result));exit;
            }
        } else {
            $msg = array('status' => 'apiLoginFailed', 'error' => json_decode($result));
            $this->response($this->json($msg), 200);
        }
    }
    /**
     * fetch product by print method id
     *
     * @param (Int)productid
     * @return json array
     */
    public function getProductPrintMethod()
    {
        $productId = $this->_request['productid'];
        $key = $this->_request['apikey'];
        if (!empty($productId)) {
            $error = false;
           //funn call for inkxe db
            $productPrintType = $this->getProductPrintMethodType($productId);
            if (!empty($productPrintType)) {
                foreach ($productPrintType as $k2 => $v2) {
                    $printDetails[$k2]['print_method_id'] = $v2['pk_id'];
                    $printDetails[$k2]['name'] = $v2['name'];
                }
            } else {
                try {
                    //fetch  category_id by product id from store
                    $result = $this->storeApi->getCategoriesByProduct($productId);
                    $catIds = json_decode($result);
                    $catIds = implode(',', (array) $catIds);
                    //get print details
                    $printDetails = $this->getPrintMethodDetailsByCategory($catIds);
                } catch (Exception $e) {
                    $result = json_encode(array('isFault' => 1, 'faultMessage' => $e->getMessage()));
                    $error = true;
                }
            }
            if (!$error) {
                $resultArr = $printDetails;
                $result = json_encode($resultArr);
                $this->response($this->json($resultArr), 200);
            } else {
                print_r($result);exit;
            }
        } else {
            $msg = array("status" => "invalid Product Id");
            $this->response($this->json($msg), 200);
        }
    }
    
    /**
     *
     *date created 31-05-2016(dd-mm-yy)
     *date modified (dd-mm-yy)
     *Add template as product
     *
     *
     */
    
    public function addTemplateProducts()
    {
        $error = false;
        if (!empty($this->_request['data'])) {
            $data = json_decode(urldecode($this->_request['data']), true);
            $apikey = $this->_request['apikey'];
            $result = $this->storeApiLogin();
            if ($this->storeApiLogin == true) {
                if (!$error) {
                    try {
                        $arr = array('data' => $data, 'configFile' => $data['images'], 'oldConfId' => $data['simpleproduct_id'], 'varColor' => $data['color_id'], 'varSize' => $data['sizes'], 'color' => $this->getStoreAttributes("xe_color"), 'size' => $this->getStoreAttributes("xe_size"));
                        $result = $this->storeApi->addTemplateProducts($arr);
                        if(isset($result['status'])){
                            throw new Exception($result['status']);
                        }
                        $resultData = json_decode($result, true);
                        $this->customRequest(array('productid' => $data['simpleproduct_id'], 'isTemplate' => 1));
                        $sides = sizeof($data['images']);
                        $productTemplate = $this->getProductTemplateByProductId($data['simpleproduct_id']);
                        $maskData = $this->getMaskData($sides);
                        $maskDatas = json_decode($maskData, true);
                        $printArea = array();
                        $printArea = $this->getPrintareaType($data['simpleproduct_id']);
                         foreach ($maskDatas as $key => $maskData) {
                            $maskScalewidth[$key] = $maskData['mask_width'];
                            $maskScaleHeight[$key] = $maskData['mask_height'];
                            $maskPrice[$key] = $maskData['mask_price'];
                            $scaleRatio[$key] = $maskData['scale_ratio'];
                            $scaleRatio_unit[$key] = $maskData['scaleRatio_unit'];
                        }
                        $this->customRequest(array('maskScalewidth' => $maskScalewidth, 'maskScaleHeight' => $maskScaleHeight, 'maskPrice' => $maskPrice, 'scaleRatio' => $scaleRatio, 'scaleRatio_unit' => $scaleRatio_unit, 'maskstatus' => $printArea['mask'], 'unitid' => $printArea['unit_id'], 'pricePerUnit' => $printArea['pricePerUnit'], 'maxWidth' => $printArea['maxWidth'], 'maxHeight' => $printArea['maxHeight'], 'boundsstatus' => $printArea['bounds'], 'customsizestatus' => $printArea['custom_size'], 'customMask' => $printArea['customMask']));
                        $printSizes = $this->getDtgPrintSizesOfProductSides($data['simpleproduct_id']);

                        $printSizes = $this->getDtgPrintSizesOfProductSides($data['simpleproduct_id']);
                        $this->customRequest(array('productid' => $resultData['conf_id'], 'jsondata' => json_encode($maskDatas), 'printsizes' => $printSizes));

                        $this->saveMaskData();
                        if ($printSizes['status'] != 'nodata') {
                            $this->setDtgPrintSizesOfProductSides();
                        }

                        $this->saveProductTemplateData($data['print_method_id'], $data['ref_id'], $data['simpleproduct_id'], $resultData['conf_id']);

                        if (!empty($productTemplate['tepmlate_id'])) {
                            $this->customRequest(array('pid' => $resultData['conf_id'], 'productTempId' => $productTemplate['tepmlate_id']));
                            $test = $this->addTemplateToProduct();

                        }
                    } catch (Exception $e) {
                        $result = json_encode(array('isFault' => 1, 'faultMessage' => $e->getMessage()));
                        $error = true;
                    }
                }
                echo $result;exit;
            } else {
                $msg = array('status' => 'apiLoginFailed', 'error' => json_decode($result));
                $this->response($this->json($msg), 200);
            }
        }
    }
    /*
     *date created 07-06-2016(dd-mm-yy)
     *date modified (dd-mm-yy)
     *Save product template data
     *
     *@param (Int)old productid
     *@param (Int)new productid
     *@param (Int)refId
     *
     */
    public function saveProductTemplateData($printMethodId, $refId, $oldId, $newId)
    {
        $apiKey = $this->_request['apikey'];
        if ($this->isValidCall($apiKey)) {
            try {
               //save product template 
                $msg = $this->saveProductTemplateStateRel($printMethodId,$refId,$oldId,$newId);
                return $this->json($msg);
            } catch (Exception $e) {
                $result = array('Caught exception:' => $e->getMessage());
                $this->response($this->json($result), 200);
            }
        }
    }
	/*
     *date created 25-05-2017(dd-mm-yy)
     *date modified (dd-mm-yy)
     *Get all attribute group from store
     *
     *@param (Nothing)
     *return json array
     */
	public function getAttributes(){
		try {
			$result = $this->storeApi->getAttributes();
			$this->response($this->json($result), 200);
		} catch (Exception $e) {
			$result = array('Caught exception:' => $e->getMessage());
			$this->response($this->json($result), 200);
        }
    }
    

    public function deleteDummyProduct(){
        $orderId = $this->_request['order_id'];
        $orderItems = $this->storeApi->getOrderItems($orderId);
        $productIds = array();
        foreach ($orderItems as $key => $value) {
            $productIds[] = $value['product_id'];
        }
        $productIds = array_unique($productIds);
        $result = $this->storeApi->deleteDummyProduct($productIds);
        $this->response($this->json($result), 200);
    }


    /**
     *
     *date created 27-09-2017(dd-mm-yy)
     *date modified (dd-mm-yy)
     *Returning details of a product
     *Scope : this page 
     *
     *@param 
     *@return Array data
     *
     */
    public function productDetails($client = '')
    {
        $result = $this->storeApiLogin();
        if ($this->storeApiLogin == true) {
            $colorValue = $this->getStoreAttributes("xe_color");
            $sizeValue = $this->getStoreAttributes("xe_size");
            $confId     = $this->_request['id'];
            $pid        = $this->_request['confId'];
            if($client!=''){
                $dimension  = $this->getStoreAttributes("dimension");
            }
            if (!isset($this->_request['size']) || trim($this->_request['size']) == '') {
                $size = '';
            } else {
                $size = trim($this->_request['size']);
            }
            $attributes = array();
            if ($size != '') {
                $attributes['size'] = $size;
            }
            try {
                //fetch simple product by product id from store//
                if($client!=''){
                    $result = $this->storeApi->getSimpleProducts($confId, $pid, $colorValue, $sizeValue, $dimension);
                }
                else{
                    $result = $this->storeApi->getSimpleProducts($confId, $pid, $colorValue, $sizeValue);
                }
                
                if (empty($result)) {
                    $result = array('isFault' => 1, 'faultMessage' => 'No Records Found');
                    return $result;
                } else {
                    $resultArr = json_decode($result, true);
                    return $resultArr;
                }
            } catch (Exception $e) {
                $result = array('isFault' => 1, 'faultMessage' => $e->getMessage());
                return $result;
            }
        } else {
            $res=array('isFault' => 2, 'error' => json_encode($result));
            return $res;
        }
    }

    /**
     *
     *Created By : Ramasankar
     *date created 12-10-2017(dd-mm-yy)
     *date modified (dd-mm-yy)
     *Returning List of Products from store product to getAllProducts() in products module
     *Scope : this page 
     *
     *@param 
     *@return Array data
     *
     */
    public function productList()
    {
        $categoryid = (isset($this->_request['categoryid']) && trim($this->_request['categoryid']) != '') ? trim($this->_request['categoryid']) : 0;
        $searchstring = (isset($this->_request['searchstring']) && trim($this->_request['searchstring']) != '') ? trim($this->_request['searchstring']) : '';
        $start = (isset($this->_request['start']) && trim($this->_request['start']) != '') ? trim($this->_request['start']) : 0;
        $limit = (isset($this->_request['range']) && trim($this->_request['range']) != '') ? trim($this->_request['range']) : 10;
        $offset = (isset($this->_request['offset']) && trim($this->_request['offset']) != '') ? trim($this->_request['offset']) : 1;
        $loadVariants = (isset($this->_request['loadVariants']) && trim($this->_request['loadVariants']) == true) ? true : false;
        $preDecorated = (isset($this->_request['preDecorated']) && trim($this->_request['preDecorated']) == 'true') ? true : false;
        $start = (int) $limit * ((int) $offset - 1);
        try {
            $result = $this->storeApi->getAllProducts($offset, $limit, $searchstring, $categoryid, $loadVariants, $preDecorated);
            return $result;
        } catch (Exception $e) {
            $result = array('isFault' => 1, 'faultMessage' => $e->getMessage());
            return $result;
        }
    }

    /**
     *
     *date created 10-10-2017(dd-mm-yy)
     *date modified (dd-mm-yy)
     *Returning category details from store to getCategories()
     *Scope : this page 
     *
     *@param 
     *@return Array data
     *
     */
    public function allCategories()
    {
        $result = $this->storeApiLogin();
        if ($this->storeApiLogin == true) {
            ;
            try {
                $result = $this->storeApi->getCategories();
                $categories = json_decode($result, true);
                return $categories['categories'];
            } catch (Exception $e) {
                $result = array('isFault' => 1, 'faultMessage' => $e->getMessage());
                return $result;
            }
        } else {
            $res=array('isFault' => 2, 'error' => json_decode($result));
            return $res;
        }
    }

    /**
     *
     * date created 10-10-2017(dd-mm-yy)
     * date modified (dd-mm-yy)
     * Returning product print method parameter details to getPrintMethodByProduct()
     * Scope : this page 
     *
     * @param 
     * @return Array data
     *
     */
    public function printMethodParameters() {
        $result = $this->storeApiLogin();
        if ($this->storeApiLogin == true && isset($this->_request['pid']) && $this->_request['pid']) {
            $confProductId = $this->_request['pid'];
            $isAdmin = (isset($this->_request['isAdmin']) && trim($this->_request['isAdmin']) == true) ? true : false;
            $result = $this->storeApi->getCategoriesByProduct($this->_request['pid']);
            $catIds = json_decode($result, true);
            $refid = '';
            $result = array('confProductId' => $confProductId, 'isAdmin' => $isAdmin, 'catIds' => $catIds, 'refid' => $refid);
            return $result;
        } else {
            $res = array('isFault' => 2, 'error' => json_encode($result));
            return $res;
        }
    }

    /**
     *
     * date created 27-09-2017(dd-mm-yy)
     * date modified (dd-mm-yy)
     * Returning Veriant Details
     * Scope : this page 
     *
     * @param 
     * @return Array data
     *
     */
    public function variantDetails() {
        $start = 0; // default values
        if (isset($this->_request['start']) && trim($this->_request['start']) != '') {
            $start = trim($this->_request['start']);
        }
        if (isset($this->_request['range']) && trim($this->_request['range']) != '' && trim($this->_request['range']) != 0) {
            $limit = trim($this->_request['range']);
        } else {
            $limit = 0;
        }
        $offset = (isset($this->_request['offset']) && trim($this->_request['offset']) != '') ? trim($this->_request['offset']) : '';
        $store = (isset($this->_request['store']) && trim($this->_request['store']) != '') ? trim($this->_request['store']) : $this->getDefaultStoreId();
        $result = $this->storeApiLogin();
        $confId = $this->_request['conf_pid'];
        if ($this->storeApiLogin == true) {
            try {
                $filters = array("id_product" => $confId, 'color' => $this->getStoreAttributes("xe_color"), 'size' => $this->getStoreAttributes("xe_size"));
                $result = $this->storeApi->getVariants($filters);
                $pCount = count($result);
                $resultArr = array('variants' => $result, 'count' => $pCount);
                return $resultArr;
            } catch (Exception $e) {
                $resultArr = array('isFault' => 1, 'faultMessage' => $e->getMessage());
                return $resultArr;
            }
        } else {
            $res = array('isFault' => 2, 'error' => json_encode($result));
            return $res;
        }
    }

    /**
     *
     * date created 27-09-2017(dd-mm-yy)
     * date modified (dd-mm-yy)
     * Returning size and quantity of a product
     *
     * @param 
     * @return Array data
     *
     */
    public function sizeAndQuantityDetails() {
        $result = $this->storeApiLogin();
        if ($this->storeApiLogin == true) {
            $color = $this->getStoreAttributes("xe_color");
            $size = $this->getStoreAttributes("xe_size");
            //$key = $GLOBALS['params']['apisessId'];
            if (!isset($this->_request['productId']) || trim($this->_request['productId']) == '') {
                $msg = array('status' => 'invalid productId', 'productId' => $this->_request['productId']);
                $this->response($this->json($msg), 204);
            } else {
                $productId = trim($this->_request['productId']);
            }
            if (!isset($this->_request['simplePdctId']) || trim($this->_request['simplePdctId']) == '') {
                //$varient_id = '';
                $msg = array('status' => 'invalid simplePdctId', 'simplePdctId' => $this->_request['simplePdctId']);
                $this->response($this->json($msg), 204);
            } else {
                $varientId = trim($this->_request['simplePdctId']);
            }
            if (isset($this->_request['byAdmin'])) {
                $byAdmin = true;
            } else {
                $byAdmin = false;
            }
            $productInfo = array(
                'productId' => $productId,
                'store' => $this->getDefaultStoreId(),
                'simpleProductId' => $varientId,
            );
            if (!$error) {
                try {
                    if (!$byAdmin) {
                        $result = $this->storeApi->getSizeAndQuantity($productId, $varientId, $color, $size);
                    } else {
                        $result = $this->storeApi->getSizeVariants($productId, $varientId, $color, $size);
                    }
                    $resultArr = $result['quantities'];
                    return $resultArr;
                } catch (Exception $e) {
                    $result = array('isFault' => 1, 'faultMessage' => $e->getMessage());
                    return $result;
                }
            }
        } else {
            $res = array('isFault' => 2, 'error' => json_encode($result));
            return $res;
        }
    }
}
