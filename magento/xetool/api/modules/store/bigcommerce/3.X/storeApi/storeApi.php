<?php
ini_set('display_errors', 'on');
require_once dirname(__FILE__) . '/../../../../../../../xetool/xeconfig.php';
class storeApi
{
 	public $_clientid = CLIENTID;
    public $_accessToken = ACCESSTOKEN;
    public $_headers;
    public $_store_url = 'https://api.bigcommerce.com/stores/'.STOREHASH.'/';
    
    private  function arrayUtf8Encoder($data)
    {
        if (is_string($data))
            return utf8_encode($data);
        if (!is_array($data))
            return $data;
        $result = array();
        foreach ($data as $i => $d)
            $result[$i] = self::arrayUtf8Encoder($d);
        return $result;
    }
    /**
     * Check Product is customizable or not
     *
     * @param (int)pid Product id
     * @return Boolean (True/False)
     */
	public function checkIsCustomize($pid){
		$resource = 'v3/catalog/products/'.$pid.'/custom-fields';
	 	$apiUrl = $this->_store_url.$resource;
	 	$result = $this->get($apiUrl);
	 	$resultArr = json_decode($result, true);
        $resultArr = $this->arrayUtf8Encoder($resultArr);
	 	if(!empty($resultArr['data'])){
			foreach($resultArr['data'] as $k){
				if($k['name'] == 'inkxe' && $k['value'] == 'customize'){
					return true;
                }else{
                    return false;
                }
			}
		}else{
			return false;
		}
	}
    /**
     * Check Product is Predeco or not
     *
     * @param (int)pid Product id
     * @return Boolean (True/False)
     */
    public function checkIsPredeco($pid){
        $isePred = true ;
        $resource = 'v3/catalog/products/'.$pid.'/custom-fields';
        $apiUrl = $this->_store_url.$resource;
        $result = $this->get($apiUrl);
        $resultArr = json_decode($result, true);
        $resultArr = $this->arrayUtf8Encoder($resultArr);
        if(!empty($resultArr['data'])){
            foreach($resultArr['data'] as $k){
                if($k['value'] == 'predeco'){
                   $isePred = false ;
                }else{
                   $isePred = true ;
                }
            }
        }
        return $isePred;
    }
    /**
     * Get all available products
     *
     * @param (int)id_lang Language id
     * @param (int)start Start number
     * @param (int) limit Number of products to return
     * @param (string) order_by Field for ordering
     * @param (string)order_way Way for ordering (ASC or DESC)
     * @return array Products details
     */
	public function getAllProducts($offset, $limit, $searchstring, $categoryid, $loadVariants, $preDecorated)
    {
        try {
			//$isCustomize = false;
            $result = $this->getProducts($offset, $limit, $searchstring);
            $showInDesignerCategoryId = $this->getCategoryByName();
            $result = $result['data']; 
			$k = 0;
            foreach ($result as $v) {
                $description = trim(strip_tags($v['description']));
                $description = html_entity_decode($description);
            	$categories = $v['categories'];
            	if (($key = array_search($showInDesignerCategoryId, $categories)) !== false) {
                    unset($categories[$key]);
                }
                $categories = array_values($categories);
                $pid = $v['id'];
                if((count($v['variants']) == 1) && (empty($v['variants'][0]['option_values'])) && ($v['variants'][0]['sku_id']=='')){
                        $ptype = 'simple';
                    }else{
                        $ptype = 'configurable';
                    }
                if($preDecorated){
					$images = $this->getAllProductImages($pid, 'getAllProducts');
					$resultArr[$k]['ptype'] = $ptype;
					$resultArr[$k]['id'] = (string)$v['id'];
					$resultArr[$k]['name'] = $v['name'];
					$resultArr[$k]['description'] = $description;
					$resultArr[$k]['price'] = (string)$v['price'];
					$resultArr[$k]['thumbnail'] = $images['url_standard'][0];
					$resultArr[$k]['category'] = $categories;
					$resultArr[$k]['store'][] = 1;
                }else{
                    $isPredeco = $this->checkIsPredeco($pid);
                    if($isPredeco){
                        $images = $this->getAllProductImages($pid, 'getAllProducts');
                        $resultArr[$k]['ptype'] = $ptype;
                        $resultArr[$k]['id'] = (string)$v['id'];
                        $resultArr[$k]['name'] = $v['name'];
                        $resultArr[$k]['description'] = $description;
                        $resultArr[$k]['price'] = (string)$v['price'];
                        $resultArr[$k]['thumbnail'] = $images['url_standard'][0];
                        $resultArr[$k]['category'] = $categories;
                        $resultArr[$k]['store'][] = 1;
                    }
                }
                $k++;
            }
            $resultData = array();
            $resultData['product'] = array_values($resultArr);
            $resultData['count'] = count($resultArr);
            return $resultData;
        } catch (Exception $e) {
            $msg['status'] = $e->getMessage();
            return json_encode($msg);
        }
    }

	public function getSimpleProducts($pid, $confId, $color, $size, $dimension)
    {
        try {
			$colorId = '';
            $optionId = '';
            $isInkxeProduct = false;
            $resultArr = $this->getProductById($confId);
            $variants = $this->getVariantsByProductId($confId);
            if((count($resultArr['data']['variants']) == 1) && (empty($resultArr['data']['variants'][0]['option_values'])) && ($resultArr['data']['variants'][0]['sku_id']=='')){
                $ptype = 'simple';
                
            }else{
                $ptype = 'configurable';
            }
            $isPredeco = $this->checkIsPredeco($confId);
            $productImages = $this->getAllProductImages($confId); 
            $pname = $resultArr['data']['name'];
            if(strtolower($pname)==strtolower('inkxe-tshirt')){
                $isInkxeProduct = true;
            }

            $shortDescription = strip_tags($resultArr['data']['description']);
            $shortDescription = html_entity_decode($shortDescription);
            $categories = $resultArr['data']['categories'];
            $productsArr = array('pid' => $confId, 'pidtype' => $ptype, 'pname' => $pname, 'minQuantity' => 1, 'shortdescription' => $shortDescription, 'category' => $categories);

            $showInDesignerCategoryId = $this->getCategoryByName();
            if(in_array($showInDesignerCategoryId, $categories)){
                $productsArr['customize'] = 1;
            }else{
                $productsArr['customize'] = 0;
            }
            
            $variants = $variants['data'];
            $productsArr['xecolor'] = "";
            $productsArr['xe_color_id'] = "";
            $productsArr['xesize'] = "";
            $productsArr['xe_size_id'] = "";
            $productsArr['colorSwatch'] = "";
            foreach ($variants as $variant) {
                if ($variant['sku_id'] == $pid) {
                    $images = ($variant['image_url'])?array($variant['image_url']):array();
                    $productsArr['pvid'] = $variant['sku_id'];
                    $productsArr['pvname'] = $resultArr['data']['name'];
                    $optionValues = $variant['option_values'];
                    $price = $variant['calculated_price'];
                    if(!empty($optionValues)){
                        foreach ($optionValues as $key => $value) {
                            $optionDetails = $this->getOptionById($confId, $value['option_id']);
                            $optionName = strtolower($optionDetails['data']['name']);
                            if(!$isPredeco || $isInkxeProduct){//Predeco is true
                                if($optionDetails['data']['type']=='swatch'){
                                    $opNameDetails = explode('coloroption', $optionName); 
                                }
                                if($optionDetails['data']['type']=='rectangles'){
                                    $opNameDetails = explode('sizeoption', $optionName); 
                                }
                                $optionName = $opNameDetails[0];
                            }
                            if($optionName == strtolower($color)){
                                $productsArr['xecolor'] = $value['label'];
                                $productsArr['xe_color_id'] = (string)$value['id'];
                                $colorId = $value['id'];
                                $optionId = $value['option_id'];
                                $productsArr['colorSwatch'] = $this->getColorSwatchProduct($confId, $optionId, $colorId);
                            }
                            if($optionName == strtolower($size)){
                                $productsArr['xesize'] = $value['label'];
                                $productsArr['xe_size_id'] = (string)$value['id'];
                            }
                        }
                    }
                }
            }
			if(empty($colorId) && $colorId == ''){
				$optionValues = $variants[0]['option_values'];
                $price = $variants[0]['calculated_price'];
                $images = ($variants[0]['image_url'])?array($variants[0]['image_url']):array();
                if(!empty($optionValues)){
    				foreach ($optionValues as $key => $value) {
                        $optionDetails = $this->getOptionById($confId, $value['option_id']);
                        $optionName = strtolower($optionDetails['data']['name']);
                        if(!$isPredeco || $isInkxeProduct){//Predeco is true
                            if($optionDetails['data']['type']=="swatch"){
                                $opNameDetails = explode('coloroption', $optionName);
                            }
                            if($optionDetails['data']['type']=="rectangles"){
                                $opNameDetails = explode('sizeoption', $optionName);
                            }
                            $optionName = $opNameDetails[0];
                        }
    					if($optionName == strtolower($color)){
    						$productsArr['xecolor'] = $value['label'];
    						$productsArr['xe_color_id'] = (string)$value['id'];
    						$colorId = $value['id'];
    						$optionId = $value['option_id'];
    						$productsArr['colorSwatch'] = $this->getColorSwatchProduct($confId, $optionId, $colorId);
    					}
    					if($optionName == strtolower($size)){
    						$productsArr['xesize'] = $value['label'];
    						$productsArr['xe_size_id'] = (string)$value['id'];
    					}
    				}
                }
                if($ptype == 'configurable'){
				    $productsArr['pvid'] = (string)$variants[0]['sku_id'];
                }else{
                    $productsArr['pvid'] = (string)$confId;
                }
				$productsArr['pvname'] = $resultArr['data']['name'];
			}
            /*Multiple Image Sides Start*/
            $unSortedSides = $unSortedThumbsides = array();
            foreach ($productImages['url_thumbnail'] as $ut) {
                $color_look = "_" . str_replace(" ", "_", strtolower($productsArr['xecolor'])) . "_";
                $color_lookup = ($productsArr['pidtype'] == 'simple' ? '_' : $color_look);
                if (strpos(strtolower($ut), $color_lookup . "front") !== false) {
                    $unSortedThumbsides['front'] = $ut;
                } else if (strpos(strtolower($ut), $color_lookup . "back") !== false) {
                    $unSortedThumbsides['back'] = $ut;
                } else if (strpos(strtolower($ut), $color_lookup . "left") !== false) {
                    $unSortedThumbsides['left'] = $ut;
                } else if (strpos(strtolower($ut), $color_lookup . "right") !== false) {
                    $unSortedThumbsides['right'] = $ut;
                }
            }
            $thumbsides = $sides = array();
            
            if(!empty($unSortedThumbsides)){
                if(isset($unSortedThumbsides['front'])){
                    array_push($thumbsides, $unSortedThumbsides['front']);
                }
                if(isset($unSortedThumbsides['back'])){
                    array_push($thumbsides, $unSortedThumbsides['back']);
                }
                if(isset($unSortedThumbsides['left'])){
                    array_push($thumbsides, $unSortedThumbsides['left']);
                }
                if(isset($unSortedThumbsides['right'])){
                    array_push($thumbsides, $unSortedThumbsides['right']);
                }
            }
            foreach ($productImages['url_standard'] as $us) {
                $color_look = "_" . str_replace(" ", "_", strtolower($productsArr['xecolor'])) . "_";
                $color_lookup = ($productsArr['pidtype'] == 'simple' ? '_' : $color_look);
                if (strpos(strtolower($us), $color_lookup . "front") !== false) {
                    $unSortedSides['front'] = $us;
                } else if (strpos(strtolower($us), $color_lookup . "back") !== false) {
                    $unSortedSides['back'] = $us;
                } else if (strpos(strtolower($us), $color_lookup . "left") !== false) {
                    $unSortedSides['left'] = $us;
                } else if (strpos(strtolower($us), $color_lookup . "right") !== false) {
                    $unSortedSides['right'] = $us;
                }
            }
            if(!empty($unSortedSides)){
                if(isset($unSortedSides['front'])){
                    array_push($sides, $unSortedSides['front']);
                }
                if(isset($unSortedSides['back'])){
                    array_push($sides, $unSortedSides['back']);
                }
                if(isset($unSortedSides['left'])){
                    array_push($sides, $unSortedSides['left']);
                }
                if(isset($unSortedSides['right'])){
                    array_push($sides, $unSortedSides['right']);
                }
            }
			
            /*Multiple Image Sides End*/
            $productsArr['quanntity'] = $resultArr['data']['inventory_level'];
            $productsArr['price'] = (string)$price;
			$productsArr['thumbsides'] = (!empty($thumbsides) && count($thumbsides)>1)?$thumbsides:$images;
            $productsArr['sides'] = (!empty($sides) && count($sides)>1)?$sides:$images;
            $productsArr['labels'] = "";
            $productsArr['taxrate'] = "0";

            /*Tier Price Start*/
            $tiersPrice = $this->getTierPrice($confId, $price);
            $productsArr['tierPrices'] = $tiersPrice;
            /*Tier Price End*/
            /*Attributes Start*/
            $attribe = array();
            foreach ($optionValues as $k => $v) {
                $optionDetails = $this->getOptionById($confId, $v['option_id']);
                $optionName = strtolower($optionDetails['data']['name']);
                if(!$isPredeco || $isInkxeProduct){//Predeco is true
                    if($optionDetails['data']['type']=="swatch"){
                        $opNameDetails = explode('coloroption', $optionName);
                    }
                    if($optionDetails['data']['type']=="rectangles"){
                        $opNameDetails = explode('sizeoption', $optionName);
                    }
                    $optionName = $opNameDetails[0];
                }
                if ($optionName == strtolower($color)) {
                    $attribe['xe_color'] = $v['label'];
                    $attribe['xe_color_id'] = $v['id'];
                }
                if ($optionName == strtolower($size)) {
                    $attribe['xe_size'] = $v['label'];
                    $attribe['xe_size_id'] = $v['id'];
                }
                if ($optionName == strtolower($dimension)) {
                    $attribe[$dimension] = $v['label'];
                }
                if ($optionName != strtolower($size) && $optionName != strtolower($color) && $optionName != strtolower($dimension)) {
                    $attribe[$v['option_display_name']] = $v['label'];
                }
            }
            $productsArr['attributes'] = $attribe;
            /*Attributes End*/
            if(!$isPredeco)
                $productsArr['isPreDecorated'] = true;
            else
                $productsArr['isPreDecorated'] = false;
            return json_encode($productsArr);
        } catch (Exception $e) {
            $msg['status'] = $e->getMessage();
            return json_encode($msg);
        }
	}

	public function getVariants($filters){
        try {
            $isInkxeProduct = false;
            $pid = $filters['id_product'];
            $productDetails = $this->getProductById($filters['id_product']);
            $productDetails = $productDetails['data'];
            if(strtolower($productDetails['name'])==strtolower('inkxe-tshirt')){
                $isInkxeProduct = true;
            }
            $variants = $this->getVariantsByProductId($filters['id_product']);
            $isPredeco = $this->checkIsPredeco($filters['id_product']);
            $variants = $variants['data'];
            $variantsArr = array();
            $colorArray = array();
            $id = 0;
            foreach ($variants as $k => $v) {
                $optionValues = $v['option_values'];
                $productId = $v['sku_id'];
                $productPrice = $v['calculated_price'];
                $productImageUrl = $v['image_url'];
                $sizeId = $colorId = $colorName = $optionId = $colorOptionName ='';
                if(!empty($optionValues)){
                    foreach ($optionValues as $key => $value) {
                        $optionDetails = $this->getOptionById($filters['id_product'], $value['option_id']);
                        $optionName = strtolower($optionDetails['data']['name']);
                        if(!$isPredeco || $isInkxeProduct){//Predeco is true
                            if($optionDetails['data']['type']=='swatch'){
                                $opNameDetails = explode('coloroption', $optionName); 
                            }
                            if($optionDetails['data']['type']=='rectangles'){
                                $opNameDetails = explode('sizeoption', $optionName); 
                            }
                            $optionName = $opNameDetails[0];
                        }
                        if($optionName == strtolower($filters['color'])){
                            $colorId = $value['id'];
                            $colorName = $value['label'];
                            $optionId = $value['option_id'];
                            $colorOptionName = $optionName;
                        }
                        if($optionName == strtolower($filters['size'])){
                            $sizeId = $value['id'];
                        }
                    }
                }
                
                if(($colorOptionName == strtolower($filters['color'])) && (!in_array($colorName, $colorArray))){
                    array_push($colorArray, $colorName);
                    $colors = $this->getColorSwatchProduct($pid, $optionId, $colorId);
                    $variantsArr[$id]['id'] = (string)$productId;
                    $variantsArr[$id]['name'] = $productDetails['name'];
                    $variantsArr[$id]['description'] = $productDetails['description'];
                    $variantsArr[$id]['thumbnail'] = $productImageUrl;
                    $variantsArr[$id]['price'] = (string)$productDetails['price'];
                    $variantsArr[$id]['tax'] = "0";
                    $variantsArr[$id]['ConfcatIds'] = $productDetails['categories'];
                    $pid = $filters['id_product']; 
                    $variantsArr[$id]['xeColor'] = $colorName;
                    $variantsArr[$id]['xe_color_id'] = (string)$colorId;
                    $variantsArr[$id]['colorUrl'] = $colors;
                    $variantsArr[$id]['xe_size_id'] = (string)$sizeId;
                    $id++;
                }
            }
            return $variantsArr;
        } catch (Exception $e) {
            $msg['status'] = $e->getMessage();
            return json_encode($msg);
        }
	}

    /**
     * Get category id by product id id from store
     *
     * @param (int)orderId
     * @return json array
     */
    public function getCategoriesByProduct($productId)
    {
        try {
            $productDetails = $this->getProductById($productId);
            $productDetails = $productDetails['data'];
            $result = $productDetails['categories'];
            return json_encode($result);
        } catch (Exception $e) {
            $msg['status'] = $e->getMessage();
            return json_encode($msg);
        }
    }

    /**
     * Get all size and quanties by product id and combination id from store
     *
     * @param (int)productId
     * @param (int)combinationsId
     * @return json array
     */
    public function getSizeAndQuantity($productId, $variantId, $color, $size)
    {
        try {
            $isInkxeProduct = false;
            $productDetails = $this->getProductById($productId);
            $productDetails = $productDetails['data'];
            if(strtolower($productDetails['name'])==strtolower('inkxe-tshirt')){
                $isInkxeProduct = true;
            }
            $variants = $this->getVariantsByProductId($productId);
            $isPredeco = $this->checkIsPredeco($productId);
            $variants = $variants['data'];
            $refColorName = '';
            foreach ($variants  as $k => $v) {
                if($v['sku_id'] == $variantId){
                    foreach($v['option_values'] as $v2){
                        $optionDetails = $this->getOptionById($productId, $v2['option_id']);
                        $optionName = strtolower($optionDetails['data']['name']);
                        if(!$isPredeco || $isInkxeProduct){//Predeco is true
                            if($optionDetails['data']['type']=='swatch'){
                                $opNameDetails = explode('coloroption', $optionName); 
                            }
                            if($optionDetails['data']['type']=='rectangles'){
                                $opNameDetails = explode('sizeoption', $optionName); 
                            }
                            $optionName = $opNameDetails[0];
                        }
                        if ($optionName == strtolower($color)) {
                            $refColorName = $v2['label'];
                        }
                    }
                }
            }
            $quantities = array();
            $id = 0;
            $inventory_tracking = $productDetails['inventory_tracking'];
            foreach ($variants as $variant) {
                $simpleProductId = $variant['sku_id'];
                $price = $variant['calculated_price'];
                $tiersPrice = $this->getTierPrice($productId, $price);
                if($variant['inventory_level']==0){
                    if($inventory_tracking=='variant'){
                        $quantity = $variant['inventory_level'];
                        $minQuantity = $variant['inventory_warning_level'];
                    }else{
                        $quantity = $productDetails['inventory_level'];
                        $minQuantity = $productDetails['inventory_warning_level'];
                    }
                }else{
                    if($inventory_tracking=='variant'){
                        $quantity = $variant['inventory_level'];
                        $minQuantity = $variant['inventory_warning_level'];
                    }
                }
                $optionValues = $variant['option_values'];
                $sizeId = $sizeName = $colorId = $colorName = $colorOptionName = '';
                $attributes = array();
                if(!empty($optionValues)){
                    foreach ($optionValues as $key => $value) {
                        $optionDetails = $this->getOptionById($productId, $value['option_id']);
                        $optionName = strtolower($optionDetails['data']['name']);
                        if(!$isPredeco || $isInkxeProduct){//Predeco is true
                            if($optionDetails['data']['type']=='swatch'){
                                $opNameDetails = explode('coloroption', $optionName); 
                            }
                            if($optionDetails['data']['type']=='rectangles'){
                                $opNameDetails = explode('sizeoption', $optionName); 
                            }
                            $optionName = $opNameDetails[0];
                        }
                        if($optionName == strtolower($color)){
                            $colorId = $value['id'];
                            $colorName = $value['label'];
                            $colorOptionName = $optionName;
                        }
                        if($optionName == strtolower($size)){
                            $sizeId             = $value['id'];
                            $sizeName           = $value['label'];
                        }

                        if ($optionName != strtolower($size) && $optionName != strtolower($color)) {
		                    $attributes[$value['option_display_name']] = $value['label'];
		                }
                    }
                }
                 
                if(($colorOptionName == strtolower($color)) && (strtolower($refColorName) == strtolower($colorName))){
                    $attributes['xe_color'] = $colorName;
                    $attributes['xe_color_id'] = (string)$colorId;
                    $attributes['xe_size'] = $sizeName;
                    $attributes['xe_size_id'] = (string)$sizeId;

                    $quantities[$id]['simpleProductId'] = (string)$simpleProductId;
                    $quantities[$id]['xe_size'] = $sizeName;
                    $quantities[$id]['xe_size_id'] = (string)$sizeId;
                    $quantities[$id]['quantity'] = $quantity;
                    $quantities[$id]['minQuantity'] = $minQuantity;
                    $quantities[$id]['xe_color'] = $colorName;
                    $quantities[$id]['xe_color_id'] = (string)$colorId;
                    $quantities[$id]['price'] = (string)$price;
                    $quantities[$id]['tierPrices'] = $tiersPrice;
                    $quantities[$id]['attributes'] = $attributes;
                    $id++;
                }
            }
			$result['quantities'] = array_values($quantities);
            return $result;
        } catch (Exception $e) {
            $msg['status'] = $e->getMessage();
            return json_encode($msg);
        }
    }

    /**
     * get tier price details for a product
     *
     * @param string $productId
     * @return Array
     */
    private function getTierPrice($productId, $price)
    {
        $bulkPrices = $this->getBulkPricingRules($productId);
        $tiersPrice = array();
        if (!empty($bulkPrices)) {
            $bulkPrices = $bulkPrices['data'];
            foreach ($bulkPrices as $k => $v) {
                $tiersPrice[$k]['tierQty'] = intval($v['quantity_min']);
                if($v['type'] == 'price'){
                    $reduce_price = $v['amount'];
                    $tiersPrice[$k]['percentage'] = number_format(floatval(100/$price*$reduce_price),2);
                }else if($v['type'] == 'percent'){
                    $reduce_price = $price *($v['amount']/100);
                    $tiersPrice[$k]['percentage'] = number_format(floatval($v['amount']),2);
                }else if($v['type'] == 'fixed'){
                    $reduce_price = $price-$v['amount'];
                    $tiersPrice[$k]['percentage'] = number_format(floatval(100/$price*$reduce_price),2);
                }
                $tiersPrice[$k]['tierPrice'] = number_format($price - $reduce_price, 2);
            }
        }
        return $tiersPrice;
    }

    /**
     * Get all size and quanties by product id and combination id from store
     *
     * @param (int)productId
     * @param (int)combinationsId
     * @return json array
     */
    public function getSizeVariants($productId, $variantId, $color, $size)
    {
        try {
            $isInkxeProduct = false;
            $productDetails = $this->getProductById($productId);
            $productDetails = $productDetails['data'];
            if(strtolower($productDetails['name'])==strtolower('inkxe-tshirt')){
                $isInkxeProduct = true;
            }
            $variants = $this->getVariantsByProductId($productId);
            $isPredeco = $this->checkIsPredeco($productId);
            $variants = $variants['data'];
            $quantities = array();
            $sizeArray = array();
            $id = 0;
            $refColorName = '';
            $inventory_tracking = $productDetails['inventory_tracking'];
            $price = $productDetails['price'];
            foreach ($variants as $variant) {
                $simpleProductId = $variant['id'];
                if($variant['inventory_level'] == 0){
                    if($inventory_tracking == 'variant'){
                        $quantity = 0;
                        $minQuantity = 1;
                    }else{
                        $quantity = $productDetails['inventory_level'];
                        $minQuantity = $productDetails['inventory_warning_level'];
                    }
                }
                else{
                    if($inventory_tracking=='variant'){
                        $quantity = $variant['inventory_level'];
                        $minQuantity = $variant['inventory_warning_level'];
                    }
                }
                $optionValues = $variant['option_values'];
                $sizeId = $sizeName = $colorId = $colorName = $colorOptionName = '';
                if(!empty($optionValues)){
                    foreach ($optionValues as $key => $value) {
                        $optionDetails = $this->getOptionById($productId, $value['option_id']);
                        $optionName = strtolower($optionDetails['data']['name']);
                        if(!$isPredeco || $isInkxeProduct){//Predeco is true
                            if($optionDetails['data']['type']=='swatch'){
                                $opNameDetails = explode('coloroption', $optionName); 
                            }
                            if($optionDetails['data']['type']=='rectangles'){
                                $opNameDetails = explode('sizeoption', $optionName); 
                            }
                            $optionName = $opNameDetails[0];
                        }
                        if($optionName == strtolower($color)){
                            $colorId = $value['id'];
                            $colorName = $value['label'];
                            $colorOptionName = $optionName;
                        }
                        
                        if($optionName == strtolower($size)){
                            $sizeId = $value['id'];
                            $sizeName = $value['label'];
                        }
                    }
                }

                $bulkPrices = $this->getBulkPricingRules($pid);
                $tiersPrice = array();
                if (!empty($bulkPrices)) {
                    $bulkPrices = $bulkPrices['data'];
                    foreach ($bulkPrices as $k => $v) {
                        $tiersPrice[$k]['tierQty'] = intval($v['quantity_min']);
                        if($v['type'] == 'price'){
                            $reduce_price = $v['amount'];
                            $tiersPrice[$k]['amount'] = floatval($v['amount']);
                        }
                        else if($v['type'] == 'percent'){
                            $reduce_price = $resultArr['data']['price'] *($v['amount']/100);
                            $tiersPrice[$k]['percent'] = floatval($v['amount']);
                        }
                        else if($v['type'] == 'fixed'){
                            $reduce_price = $v['amount'];
                            $tiersPrice[$k]['fixed'] = floatval($v['amount']);
                        }
                        $tiersPrice[$k]['tierPrice'] = number_format($resultArr['data']['price'] - $reduce_price, 5);
                    }
                } 

                if(($colorOptionName == strtolower($color)) && (!in_array($sizeName, $sizeArray))){
                    array_push($sizeArray, $sizeName);
                    $attributes = array('xe_color'=> $colorName, 'xe_color_id' => (string)$colorId, 'xe_size' => $sizeName, 'xe_size_id' => (string)$sizeId); 
                    $quantities[$id]['simpleProductId'] = (string)$simpleProductId;
                    $quantities[$id]['xe_size'] = $sizeName;
                    $quantities[$id]['xe_size_id'] = (string)$sizeId;
                    $quantities[$id]['quantity'] = $quantity;
                    $quantities[$id]['minQuantity'] = $minQuantity;
                    $quantities[$id]['xe_color'] = $colorName;
                    $quantities[$id]['xe_color_id'] = (string)$colorId;
                    $quantities[$id]['price'] = (string)$price;
                    $quantities[$id]['tierPrices'] = $tiersPrice;
                    $quantities[$id]['attributes'] = $attributes;
                    $id++;
                }
            }

            $result['quantities'] = array_values($quantities);
            return $result;
        } catch (Exception $e) {
            $msg['status'] = $e->getMessage();
            return json_encode($msg);
        }
    }

    /**
     *Get all available category
     *
     *@param nothing
     *@return array category details
     */
    public function getCategories()
    {
        try {
            $catagories = $this->getAllCategories();
            $catagories = $catagories['data'];
            $result = array();
            if (!empty($catagories)) {
                foreach ($catagories as $key => $value) {
                    if($value['parent_id']==0 && $value['name']!="Show_in_Designer"){
                        $result[$key]['id'] = $value['id'];
                        $result[$key]['name'] = $value['name'];                        
                    }
                }
            }
            return json_encode(array('categories' => array_values($result)));
        }catch (Exception $e) {
            $msg['status'] = $e->getMessage();
            return json_encode($msg);
        }

    }

    /**
     *Get all Subcategory of a category
     *
     *@param nothing
     *@return array category details
     */
    public function getsubCategories($category)
    {
        try {
            $catagories = $this->getAllCategories();
            $catagories = $catagories['data'];
            $result = array();
            if (!empty($catagories)) {
                foreach ($catagories as $key => $value) {
                    if($value['parent_id'] == $category){
                        $result[$key]['id'] = $value['id'];
                        $result[$key]['name'] = $value['name'];                        
                    }
                }
            }
            return json_encode(array('subcategories' => array_values($result)));
        } catch (Exception $e) {
            $msg['status'] = $e->getMessage();
            return json_encode($msg);
        }
    }

    /**
     *fetch only getSizeArr
     *
     * @param nothing
     * @return array
     *
     */
    public function getSizeArr($size)
    {
        try {
            $options = $this->getOptions();
            $sizeArray = array();
            foreach ($options as $key => $value) {
                if(strtolower($value['name'])==strtolower($size)){
                    $url = $value['values']['url'];
                    $optionValues = $this->getOptionValues($url);
                    foreach ($optionValues as $k => $v) {
                        $sizeArray[$k]['value'] = $v['id'];
                        $sizeArray[$k]['label'] = $v['label'];
                    }
                }
            }
            return json_encode($sizeArray);
        } catch (Exception $e) {
            $msg['status'] = $e->getMessage();
            return json_encode($msg);
        }
    }

    /**
     *get color name from the prestashop store
     *
     * @param (Int)lastLoaded
     * @param (Int)loadCount
     * @return color array
     *
     */
    public function getColorArr($lastLoaded, $loadCount, $productId, $color, $size)
    {
        try{
            if (isset($lastLoaded) && isset($loadCount)) {
                
            } else {
                $filters = array("id_product" => $productId, 'color' => $color, 'size' => $size);
                $options = $this->getVariants($filters);
                $resultArr = array();
                $k = 0;
                foreach ($options as $key => $value) {
                    $resultArr[$k]['value'] = $value['xe_color_id'];
                    $resultArr[$k]['label'] = $value['xeColor'];
                    $resultArr[$k]['swatchImage'] = '';
                    $k++;
                }
                $resultArr = array_unique($resultArr, SORT_REGULAR);
                $result = array_values($resultArr);
            }
            return json_encode($result);
        } catch (Exception $e) {
            $msg['status'] = $e->getMessage();
            return json_encode($msg);
        }
    }
    public function getColorHexValue($smplProdID,$colorOptionId,$colorId){
    	$resource = 'v3/catalog/products/'.$smplProdID.'/options/'.$colorOptionId.'/values/'.$colorId;
        $apiUrl = $this->_store_url.$resource;
        $result = $this->get($apiUrl);
        $resultArr = json_decode($result, true);
        $resultArr =$this->arrayUtf8Encoder($resultArr);
        return $resultArr;
    }
    /**
     * Retrieve a single product's category(not used currently)
     *
     * @param array|object $callParams
     * @return json
     */
    public function addTemplateProducts($callParams)
    {
        try{
            $confProdID = $callParams['data']['conf_id'];
            $smplProdID = $callParams['data']['simpleproduct_id'];
            $prod_sku = $callParams['data']['sku'];
            $prod_price = $callParams['data']['price'];
            $prod_qty = $callParams['data']['qty'];
            $prod_color = str_replace(' ', '_', strtolower($callParams['varColor']));
            $colorId = $callParams['data']['color_id'];
            $categories = $callParams['data']['cat_id'];

            $isInkxeProduct = false;
            $productDetails = $this->getProductById($smplProdID);
            $productDetails = $productDetails['data'];
            if(strtolower($productDetails['name'])==strtolower('inkxe-tshirt')){
                $isInkxeProduct = true;
            }

            $variants = $this->getVariantsByProductId($smplProdID);
            $sizesArr =  $callParams['data']['sizes'];
            $variants = $variants['data'];
            $mainParentProductId = $variants[0]['product_id'];
            $parentProductImages = $this->getAllProductImages($mainParentProductId);
            foreach ($parentProductImages['url_standard'] as $us) {
                $color_lookup = "_" . str_replace(" ", "_", strtolower($callParams['data']['color_label'])) . "_";
                if (strpos(strtolower($us), $color_lookup . "front") !== false) {
                    $unSortedSides['front'] = $us;
                } else if (strpos(strtolower($us), $color_lookup . "back") !== false) {
                    $unSortedSides['back'] = $us;
                } else if (strpos(strtolower($us), $color_lookup . "left") !== false) {
                    $unSortedSides['left'] = $us;
                } else if (strpos(strtolower($us), $color_lookup . "right") !== false) {
                    $unSortedSides['right'] = $us;
                }
            }
            $sides = array();
            if(!empty($unSortedSides)){
                if(isset($unSortedSides['front'])){
                    array_push($sides, $unSortedSides['front']);
                }
                if(isset($unSortedSides['back'])){
                    array_push($sides, $unSortedSides['back']);
                }
                if(isset($unSortedSides['left'])){
                    array_push($sides, $unSortedSides['left']);
                }
                if(isset($unSortedSides['right'])){
                    array_push($sides, $unSortedSides['right']);
                }
            }
            $images = '';
            foreach($variants as $variant) {
                $optionValues = $variant['option_values'];
                $colorName = '';
                if(!empty($optionValues)){
                    foreach ($optionValues as $key => $value) {
                        $optionDetails = $this->getOptionById($smplProdID, $value['option_id']);
                        $optionName = strtolower($optionDetails['data']['name']);
                        if($isInkxeProduct){
                            if($optionDetails['data']['type']=='swatch'){
                                $opNameDetails = explode('coloroption', $optionName); 
                            }
                            if($optionDetails['data']['type']=='rectangles'){
                                $opNameDetails = explode('sizeoption', $optionName); 
                            }
                            $optionName = $opNameDetails[0];
                        }
                        if($optionName == strtolower($callParams['color']) && $colorId == $value['id']){
                            $colorOptionId = $value['option_id'];
                            $colorName = $value['label'];
                        }
                        if($optionName == strtolower($callParams['size'])){
                            $sizeOptionId = $value['option_id'];
                        }
                    }
                }
                if($colorName == $callParams['data']['color_label']){
                    $images = $variant['image_url'];
                }
            }
            $colorHexValueArr = $this->getColorHexValue($smplProdID,$colorOptionId,$colorId);
            $coloeHexCode = $colorHexValueArr['data']['value_data']['colors'][0];
            $variantArr = array();
            $sizeLabel = array_unique($callParams['data']['size_label']);
            if($confProdID > 0){
                $productId = $confProdID;
                $options = $this->getOptionsByProductId($confProdID);
                $options = $options['data'];
                $sizeOptionId = $colorOptionId = '';
                $availableSizes = array();
                $newSizeDetails = array();
                foreach ($options as $key => $value) {
                    $optionName = strtolower($value['name']);
                    if($value['type']=='swatch'){
                        $opNameDetails = explode('coloroption', $optionName); 
                    }
                    if($value['type']=='rectangles'){
                        $opNameDetails = explode('sizeoption', $optionName); 
                    }
                    $optionName = $opNameDetails[0];
                    if($optionName==strtolower($callParams['size'])){
                        $optionValues = $value['option_values'];
                        foreach ($optionValues as $ov) {
                            $availableSizes[] = $ov['label'];
                            if(in_array($ov['label'], $sizeLabel)){
                                $newSizeDetails[] = array('id' => $ov["id"], 'label' => $ov["label"]);
                            }
                        }
                        $sizeOptionId = $value['id'];
                    }
                    if($optionName==strtolower($callParams['color'])){
                        $optionValues = $value['option_values'];
                        $colorOptionId = $value['id'];
                    }
                }
                $sizeValuesInStore = array_diff($availableSizes, $sizeLabel);
                $newSizeValues = array_diff($sizeLabel, $availableSizes);
                $remainingSizes = array_merge($sizeValuesInStore, $newSizeValues);
                $sizes  = array_unique($callParams['data']['sizes']);
                foreach ($remainingSizes as $rs) {
                    $sizeParams = array(
                        "is_default" => false,
                        "label" => $rs,
                        "sort_order" => 1,
                    );
                    $addedSizes = $this->createOptionValues($confProdID, $sizeOptionId, $sizeParams);
                    $newSizeDetails[] = array('id' => $addedSizes["data"]["id"], 'label' => $addedSizes["data"]["label"]);
                }
                $colorParams = array(
                    "is_default" => false,
                    "label" => $callParams['data']['color_label'],
                    "sort_order" => 1,
                    "value_data"=>array("colors"=>array($coloeHexCode))
                );
                $addedColors = $this->createOptionValues($confProdID, $colorOptionId, $colorParams);
                $colorId = $addedColors["data"]["id"];
                foreach($sizeLabel as $sl) {
                    foreach($newSizeDetails as $nsd) {
                        if($sl == $nsd['label']){
                            $sizeId = $nsd['id'];
                        }
                    }
                    $optionValues = array(
                        array(
                            "id" => $colorId,
                            "option_id" => $colorOptionId
                        ),
                        array(
                            "id" => $sizeId,
                            "option_id" => $sizeOptionId
                        )
                    );
                    $variantSku = str_replace(" ", "_", $callParams['data']['product_name']).mt_rand(0,100000);
                    $variantArr[] = array(
                        "weight" => 0,
                        "purchasing_disabled" => false,
                        "purchasing_disabled_message" => "",
                        "image_url" => $images,
                        "product_id" => $productId,
                        "sku" => $variantSku,
                        "option_values" => $optionValues
                    );
                }

                foreach ($variantArr as $va) {
                    $newVariants = $this->createVariants($va, $confProdID);
                }

                if(!empty($sides)){
                    foreach ($sides as $side) {
                        $param = array(
                          "sort_order"=> 1,
                          "description"=> "",
                          "image_url"=> $side,
                          "image_file"=> $side
                        );
                        $this->addImageByPid($param,$confProdID);
                    }
                }
                $confId = $confProdID;
            }

            $showInDesignerCategoryId = $this->getCategoryByName();
            if(!in_array($showInDesignerCategoryId, $categories)){
                $categories[]=$showInDesignerCategoryId;
            }
            $customField = array(
                                array(
                                  "name" => "inkxe",
                                  "value" => "predeco"
                                )
                            );
            
            if($callParams['data']['is_customized']!=1){
                if (($key = array_search($showInDesignerCategoryId, $categories)) !== false) {
                    unset($categories[$key]);
                }
            }
            
            if ($confProdID == 0) {
                $productArray = array(
                    "name" => addslashes($callParams['data']['product_name']),
                    "type" => "physical",
                    "description" => addslashes($callParams['data']['description']),
                    "weight" => 0,
                    "price" => $prod_price,
                    "tax_class_id" => 0,
                    "product_tax_code" => "",
                    "categories" => $categories,
                    "inventory_level" => $prod_qty,
                    "inventory_warning_level" => 0,
                    "inventory_tracking" => "product",
                    "fixed_cost_shipping_price" => 0,
                    "is_free_shipping" => true,
                    "is_visible"=> true,
                    "is_featured"=> false,
                    "related_products"=> array(),
                    "availability"=> "available",
                    "availability_description"=> "",
                    "gift_wrapping_options_list"=> array(),
                    "condition"=> "New",
                    "is_condition_shown"=> false,
                    "order_quantity_minimum"=> 0,
                    "order_quantity_maximum"=> 0,
                    "view_count"=> 0,
                    "preorder_message"=> "",
                    "is_preorder_only"=> false,
                    "is_price_hidden"=> false,
                    "price_hidden_label"=> "",            
                    "custom_fields" => $customField, 
                );  
                $newProduct = $this->createProduct($productArray);
                if(isset($newProduct['errors'])){
                    throw new Exception($newProduct['title']);
                }
                if($newProduct['data']['id']){
                    $param = array("is_thumbnail"=> true,
                              "sort_order"=> 1,
                              "description"=> "string",
                              "image_url"=> $callParams['configFile'][0],
                              "image_file"=> $callParams['configFile'][0]);
                    $this->addImageByPid($param,$newProduct['data']['id']);

                    if(!empty($sides)){
                        foreach ($sides as $side) {
                            $param = array(
                              "sort_order"=> 1,
                              "description"=> "",
                              "image_url"=> $side,
                              "image_file"=> $side
                            );
                            $this->addImageByPid($param,$newProduct['data']['id']);
                        }
                    }
                }
                if ($newProduct['data']['id'] > 0) {
              		$colorOptionParams = array("product_id"=> $newProduct['data']['id'],
    				  "display_name"=> $callParams['color']."coloroption",
    				  "type"=> "swatch",
    				  "option_values"=> array(array(
    				      "is_default"=>false,
    				      "label"=>$callParams['data']['color_label'],
    				      "sort_order"=> 1,
    				      "value_data"=>array("colors"=>array($coloeHexCode))
    				    ))
    				);
    				$sizeParams = array();
              		foreach ($sizeLabel as $rs) {
    	                $sizeParams[] = array(
    	                    "is_default" => false,
    	                    "label" => $rs,
    	                    "sort_order" => 1,
    	                );
               		}
    				$sizeOptionParams = array("product_id"=> $newProduct['data']['id'],
    				  "display_name"=> $callParams['size']."sizeoption",
    				  "type"=> "rectangles",
    				  "option_values"=> $sizeParams
    				);
                	$addedOptionColor = $this->createOption($newProduct['data']['id'], $colorOptionParams);
                	$newColorOptionId = $addedOptionColor['data']['id'];
                	$newColorId = $addedOptionColor['data']['option_values'][0]['id'];
                	$addedOptionSize = $this->createOption($newProduct['data']['id'], $sizeOptionParams);
                	$newSizeOptionId = $addedOptionSize['data']['id'];
                	$sizeOptionArr = array();
                	foreach ($addedOptionSize['data']['option_values'] as $k => $op) {
                		$sizeOptionArr[] = array("id"=>$op['id'],"value"=>$op['label']);
                	}
    		    	foreach($sizeLabel as $sl) {
    		    		foreach ($sizeOptionArr as $soa) {
    		    			if($sl == $soa['value']){
    		    				$sizeId = $soa['id'];
    		    			}
    		    		}
    		            $optionValues = array(
    		                array(
    		                    "id" => $newColorId,
    		                    "option_id" => $newColorOptionId
    		                ),
    		                array(
    		                    "id" => $sizeId,
    		                    "option_id" => $newSizeOptionId
    		                )
    		            );
    		            $variantSku = str_replace(" ", "_", $callParams['data']['product_name']).mt_rand(0,100000);
    		            $variantArrs[] = array(
    		                "weight" => 0,
    		                "purchasing_disabled" => false,
    		                "purchasing_disabled_message" => "",
    		                "image_url" => $images,
    		                "product_id" => $newProduct['data']['id'],
    		                "sku" => $variantSku,
    		                "option_values" => $optionValues
    		            );
    		        }
    	            foreach ($variantArrs as $va) {
    	                $newVariants = $this->createVariants($va, $newProduct['data']['id']);
    	            }
    	            $confId = $newProduct['data']['id'];
           		}
            }
                  
            $response['conf_id'] = $confId;
            $response['old_conf_id'] = $smplProdID;
            $response['variants'] = array(array("color_id" => $callParams['varColor'], "size_id" => $callParams['data']['sizes']));
            return json_encode($response);
        } catch (Exception $e) {
            $msg['status'] = $e->getMessage();
            return json_encode($msg);
        }
    }
    public function addImageByPid($param,$pid){
        $resource = 'v3/catalog/products/'.$pid.'/images';
        $apiUrl = $this->_store_url.$resource;
        $result = $this->post($apiUrl, $param);
        $resultArr = json_decode($result, true);
        $resultArrs = $this->arrayUtf8Encoder($resultArr);
        if($resultArrs['data']['id'])
            return true;
        else
            return false;
    }
    public function createOption($newProductId, $colorOptionParams){
    	$resource = 'v3/catalog/products/'.$newProductId.'/options';
        $apiUrl = $this->_store_url.$resource;
        $result = $this->post($apiUrl, $colorOptionParams);
        $resultArr = json_decode($result, true);
        $resultArrs = $this->arrayUtf8Encoder($resultArr);
        return $resultArrs;

    }
    /**
     * Add Product for cart
     *
     * @param array|object $lineItems
     * @return array
     */
    public function addCartProduct($lineItems)
    {
        try{
            $variantArray = array();
            $catIds = '';
            $productId = $lineItems[0]['product_id'];
            $addedPrice = $lineItems[0]['added_price'];
            $productDetails = $this->getProductById($productId);
            if((count($productDetails['data']['variants']) == 1) && (empty($productDetails['data']['variants'][0]['option_values'])) && ($productDetails['data']['variants'][0]['sku_id']=='')){
                $ptype = 'simple';
                
            }else{
                $ptype = 'configurable';
            }
            $productName = addslashes($productDetails['data']['name']);
            $productPrice = $productDetails['data']['price'];
            $productCategories = $productDetails['data']['categories'];
            $productQty = $productDetails['data']['inventory_level'];
            $variants = $productDetails['data']['variants'];
            $skuWithPrice = array();
            foreach ($variants as $variant) {
            	if($ptype == 'configurable'){
            		$skuWithPrice[$variant['sku_id']] = $variant['calculated_price'];
            	}else{
            		$skuWithPrice[$variant['product_id']] = $variant['calculated_price'];
            	}
            }
            $bulkPricingRules = $productDetails['data']['bulk_pricing_rules']; 
            $newProductPrice = $productPrice+$addedPrice;
            foreach ($lineItems as $key => $value) {
                $isAppliedBulkPrice = 0;
                if(!empty($bulkPricingRules)){
                    $status = 0;
                    foreach($bulkPricingRules as $bpr) {
                        if(($value['total_quantity'] >= $bpr['quantity_min']) && (($value['total_quantity'] <= $bpr['quantity_max']) || ($bpr['quantity_max'] == 0))){
                            if($bpr['type']=='fixed'){
                                $price = $bpr['amount']+$value['added_price'];
                            }else if($bpr['type']=='percent'){
                            	if($ptype == 'configurable'){
                            		$price = ($skuWithPrice[$value['variant_id']]-($skuWithPrice[$value['variant_id']]*$bpr['amount']/100))+$value['added_price'];
				            	}else{
				            		$price = ($skuWithPrice[$value['product_id']]-($skuWithPrice[$value['product_id']]*$bpr['amount']/100))+$value['added_price'];
				            	}
                            }else{
                            	if($ptype == 'configurable'){
                            		$price = ($skuWithPrice[$value['variant_id']]-$bpr['amount'])+$value['added_price'];
				            	}else{
				            		$price = ($skuWithPrice[$value['product_id']]-$bpr['amount'])+$value['added_price'];
				            	}
                            }
                            $status = 1;
                            $isAppliedBulkPrice = 1;
                        }
                    }
                    if($status == 0){
                    	if($ptype == 'configurable'){
                    		$price = $skuWithPrice[$value['variant_id']]+$value['added_price'];
                    	}else{
                    		$price = $skuWithPrice[$value['product_id']]+$value['added_price'];
                    	}
                        
                    }
                }else{
                	if($ptype == 'configurable'){
                		$price = $skuWithPrice[$value['variant_id']]+$value['added_price'];
                	}else{
                		$price = $skuWithPrice[$value['product_id']]+$value['added_price'];
                	}
                    
                }

                if($ptype == 'configurable'){
                    if($isAppliedBulkPrice == 1){
                        $variantSku = $value['variant_id']."_sku_bulk_".$productId."_".mt_rand(0,100000);
                    }else{
                        $variantSku = $value['variant_id']."_sku_nobulk_".$productId."_".mt_rand(0,100000);
                    }
                    $newProductName = $productName."_".$value['options']['xe_color']."_".$value['options']['xe_size'].mt_rand(0,1000);
                    $newProductSku = "dummy_".mt_rand(0,10000);

                    $optionValues = array();

                    if($value['options']['xe_color'] !=''){
                        $colorOption = array(
                            "option_display_name" => "Color",
                            "label" => $value['options']['xe_color']
                        );
                        array_push($optionValues, $colorOption);
                    }

                    if($value['options']['xe_size'] !=''){
                        $sizeOption = array(
                            "option_display_name" => "Size",
                            "label" => $value['options']['xe_size']
                        );
                        array_push($optionValues, $sizeOption);
                    }
                    foreach ($variants as $variant) {
                		if($variant['sku_id']==$value['variant_id']){
                			$attributrOptionValues = $variant['option_values'];
                			foreach ($attributrOptionValues as $k => $v) {
                				$optionDetails = $this->getOptionById($variant['product_id'], $v['option_id']);
                        		$optionName = strtolower($optionDetails['data']['name']);
				                if ($optionName != strtolower($value['sizeAttribute']) && $optionName != strtolower($value['colorAttribute'])) {
				                    $extraOption = array(
			                            "option_display_name" => $v['option_display_name'],
			                            "label" => $v['label']
			                        );
			                        array_push($optionValues, $extraOption);
				                }
				            }
                		}
		            }
                    $variantArray[] = array(
                        "weight" => 0,
                        "image_url" => $value['design_url'],
                        "sku" => $variantSku,
                        "price" => $price,
                        "option_values" => $optionValues
                    );
                }else{
                    if($isAppliedBulkPrice == 1){
                        $newProductSku = $productId."_sku_bulk_".$productId."_".mt_rand(0,100000);
                    }else{
                        $newProductSku = $productId."_sku_nobulk_".$productId."_".mt_rand(0,100000);
                    }
                    $newProductName = $productName."_".mt_rand(0,1000);
                    $newProductPrice = $price;
                    $variantArray = array();
                }
            }
            $productArray = array(
                "name" => $newProductName,
                "type" => "physical",
                "sku" => $newProductSku,
                "weight" => 0,
                "price" => $newProductPrice,
                "categories" => $productCategories,
                "inventory_level" => $productQty,
                "inventory_tracking" => "product",
                "is_free_shipping" => true,
                "is_visible"=> false,
                "availability"=> "available",
                "condition"=> "New",
                "is_condition_shown"=> false,
                "is_price_hidden"=> false,
                "price_hidden_label"=> "",
                "variants" => $variantArray
            );

            $newProduct = $this->createProduct($productArray);
            $newLineItems = array();
            $newProductId = $newProduct['data']['id'];
            if($newProduct['data']['id']){
                $param = array(
                    "is_thumbnail"=> true,
                    "sort_order"=> 1,
                    "description"=> "string",
                    "image_url"=> $lineItems[0]['design_url'],
                    "image_file"=> $lineItems[0]['design_url']
                );
                $this->addImageByPid($param,$newProduct['data']['id']);
            }
            $l = 0;
            foreach ($newProduct['data']['variants'] as $npv) {
                $newLineItems[] = array(
                    "quantity" => $lineItems[$l]['quantity'],
                    "product_id" => $newProductId,
                    "variant_id" => $npv['id']
                );
                $l++;
            }
            return $newLineItems;
        } catch (Exception $e) {
            $msg['status'] = $e->getMessage();
            return json_encode($msg);
        }
    }

    /**
     * Add Producr to cart
     *
     * @param array|object $lineItems
     * @return array
     */
    public function addToCart($lineItems,$cartId)
    {
        try{
            $postValues = array('line_items'=>$lineItems, "gift_certificates"=>array());
    		if($cartId !='' && $cartId){
    			$resource = 'v3/carts/'.$cartId.'/items';
    		}else{
    			$resource = 'v3/carts';
    		}
            $apiUrl = $this->_store_url.$resource;
            $result = $this->post($apiUrl, $postValues);
            $resultArr = json_decode($result, true);
            $resultArrs = $this->arrayUtf8Encoder($resultArr);
            $cartId = $resultArrs['data']['id'];
    		$lineItemsId = $skuArr = $oldVariantId = $appliedBulkPrice = $oldProductname = array();
            $responseLineItems = $resultArrs['data']['line_items']['physical_items'];
            foreach ($responseLineItems as $rli) {
                $lineItemsId[] = $rli['id'];
    			$sku = $this->getVariantById($rli['product_id'],$rli['variant_id']);
    			$skuArr[] = $sku['data']['sku'];
                $getOldSku = explode('_', $sku['data']['sku']);
                $oldVariantId[] = $getOldSku[0];
                if($getOldSku[2] == 'bulk'){
                    $appliedBulkPrice[] = 1;
                }else if($getOldSku[2] == 'nobulk'){
                    $appliedBulkPrice[] = 0;
                }
                $oldProductId = $getOldSku[3];
                $productName  = $this->getProductById($oldProductId);
                $oldProductname[] = $productName['data']['name'];
            }
            $urls = $this->getUrls($cartId);
            $responseArray = array(
                "cart_id" => $cartId,
                "cart_url" => $urls['data']['cart_url'],
                "item_id" => $lineItemsId,
                "sku" => $skuArr,
    			"old_variant_id" => $oldVariantId,
                "bulk_price_status" => $appliedBulkPrice,
                "old_product_name" => $oldProductname
            );
            return $responseArray;
        } catch (Exception $e) {
            $msg['status'] = $e->getMessage();
            return json_encode($msg);
        }
    }
    /**
     * Get AddToCart Urls
     *
     * @param integer|String $cartId
     * @return array
     */
    public function getUrls($cartId)
    {
        $resource = 'v3/carts/'.$cartId.'/redirect_urls';
        $apiUrl = $this->_store_url.$resource;
        $result = $this->post($apiUrl, '');
        $resultArr = json_decode($result, true);
        $resultArrs = $this->arrayUtf8Encoder($resultArr);
        return $resultArrs;
    }

    /**
     * Get product details of preDeco Product
     *
     * @param array|object $callParams
     * @return json
     */
    public function getProductInfo($callParams)
    {
        try{
            $sizePos = "";
            $colorPos = "";
            $pid = (isset($callParams['configId']) && trim($callParams['configId']) != '') ? trim($callParams['configId']) : '';
            $vid = (isset($callParams['smplProdID']) && trim($callParams['smplProdID']) != '' && trim($callParams['smplProdID']) > 0) ? trim($callParams['smplProdID']) : '';
            $qty = (isset($callParams['qty']) && trim($callParams['qty']) != '') ? trim($callParams['qty']) : '';
            $refid = (isset($callParams['refid']) && trim($callParams['refid']) != '') ? trim($callParams['refid']) : '';

            $isInkxeProduct = false;
            $productDetails = $this->getProductById($pid);
            $productDetails = $productDetails['data'];
            if(strtolower($productDetails['name'])==strtolower('inkxe-tshirt')){
                $isInkxeProduct = true;
            }

            $isPredeco = $this->checkIsPredeco($pid);

            $product = $this->getVariantsByProductId($pid);
            if (!($vid > 0) || $vid == '') {
                $vid = $product['data'][0]['sku_id'];
                foreach ($product['data'][0]['option_values'] as $key => $value) {
                    $optionDetails = $this->getOptionById($pid, $value['option_id']);
                    $optionName = strtolower($optionDetails['data']['name']);
                    if(!$isPredeco || $isInkxeProduct){//Predeco is true
                        if($optionDetails['data']['type']=='swatch'){
                            $opNameDetails = explode('coloroption', $optionName); 
                        }
                        if($optionDetails['data']['type']=='rectangles'){
                            $opNameDetails = explode('sizeoption', $optionName); 
                        }
                        $optionName = $opNameDetails[0];
                    }
                    if ($optionName == strtolower($callParams['color'])) {
                        $colorId = $value['id'];
                        $colorName = $value['label'];
                    }
                    if ($optionName == strtolower($callParams['size'])) {
                        $sizeId = $value['id'];
                        $sizeName = $value['label'];
                    }
                }
            }else{
                foreach ($product['data']  as $k => $v) {
    				if($v['sku_id'] == $vid){
    					foreach($v['option_values'] as $v2){
                            $optionDetails = $this->getOptionById($pid, $v2['option_id']);
                            $optionName = strtolower($optionDetails['data']['name']);
                            if(!$isPredeco || $isInkxeProduct){//Predeco is true
                                if($optionDetails['data']['type']=='swatch'){
                                    $opNameDetails = explode('coloroption', $optionName); 
                                }
                                if($optionDetails['data']['type']=='rectangles'){
                                    $opNameDetails = explode('sizeoption', $optionName); 
                                }
                                $optionName = $opNameDetails[0];
                            }
    						if ($optionName == strtolower($callParams['color'])) {
    							$colorId = $v2['id'];
    							$colorName = $v2['label'];
    						}
    						if ($optionName == strtolower($callParams['size'])) {
    							$sizeId = $v2['id'];
    							$sizeName = $v2['label'];
    						}
    					}
    				}
                }
            }
    			
            $prodData = array();
            $prodData['is_predeco_product_template'] = true;
            $prodData['qty'] = $qty;
            $prodData['id'] = $pid;
            $prodData['refid'] = $refid;
            $prodData['addedprice'] = "0.00";
            $prodData['simple_product']['xe_color'] = $colorName;
            $prodData['simple_product']['xe_size'] = $sizeName;
            $prodData['simple_product']['xe_size_id'] = $sizeId;
            $prodData['simple_product']['simpleProductId'] = $vid;
            return json_encode(array($prodData), JSON_NUMERIC_CHECK);
        } catch (Exception $e) {
            $msg['status'] = $e->getMessage();
            return json_encode($msg);
        }
    }

    public function deleteDummyProduct($productIds)
    {
       foreach ($productIds as $productId) {
            $productDetails  = $this->getProductById($productId);
            $sku = explode('_', $productDetails['data']['sku']);
            if($sku[0]=='dummy'){
                $this->deleteProduct($productId);
            }
        } 
    }
    
    public function getProducts($offset, $limit, $searchstring){
        $categoryId = $this->getCategoryByName();
        if($searchstring!=''){
            $resource = 'v3/catalog/products?keyword='.$searchstring.'&include=bulk_pricing_rules,variants';
        }else{
            $resource = 'v3/catalog/products?page='.$offset.'&limit='.$limit.'&include=bulk_pricing_rules,variants&is_visible=1&categories:in='.$categoryId;
        }
        $apiUrl = $this->_store_url.$resource;
        $result = $this->get($apiUrl);
        $resultArr = json_decode($result, true);
        $resultArrs = $this->arrayUtf8Encoder($resultArr);
        return $resultArrs;
    } 

    public function createProduct($productArray){
        $resource = 'v3/catalog/products';
        $apiUrl = $this->_store_url.$resource;
        $result = $this->post($apiUrl, $productArray);
        $resultArr = json_decode($result, true);
        $resultArrs = $this->arrayUtf8Encoder($resultArr);
        return $resultArrs;
    } 

    public function deleteProduct($productId){
        $resource = 'v3/catalog/products/'.$productId;
        $apiUrl = $this->_store_url.$resource;
        $result = $this->delete($apiUrl);
        $resultArr = json_decode($result, true);
        $resultArrs = $this->arrayUtf8Encoder($resultArr);
        return $resultArrs;
    }

    public function createVariants($variantArray, $productId){
        $resource = 'v3/catalog/products/'.$productId.'/variants';
        $apiUrl = $this->_store_url.$resource;
        $result = $this->post($apiUrl, $variantArray);
        $resultArr = json_decode($result, true);
        $resultArrs = $this->arrayUtf8Encoder($resultArr);
        return $resultArrs;
    } 

    public function createOptionValues($confProdID, $sizeOptionId, $sizeParams){
        $resource = 'v3/catalog/products/'.$confProdID.'/options/'.$sizeOptionId.'/values';
        $apiUrl = $this->_store_url.$resource;
        $result = $this->post($apiUrl, $sizeParams);
        $resultArr = json_decode($result, true);
        $resultArrs = $this->arrayUtf8Encoder($resultArr);
        return $resultArrs;
    } 

    public function getProductById($pid){
        $resource = 'v3/catalog/products/'.$pid.'?include=bulk_pricing_rules,variants,custom_fields';//variants
        $apiUrl = $this->_store_url.$resource;
        $result = $this->get($apiUrl);
        $resultArr = json_decode($result, true);
        $resultArr =$this->arrayUtf8Encoder($resultArr);
        return $resultArr;
    }

	public function getAllCategories(){
		$resource = 'v3/catalog/categories';
	 	$apiUrl = $this->_store_url.$resource;
	 	$result = $this->get($apiUrl);
	 	$resultArr = json_decode($result, true);
        $resultArr =$this->arrayUtf8Encoder($resultArr);
	 	return $resultArr;
	}

    public function getProductImages($productId)
    {
        $resource = 'v3/catalog/products/'.$productId.'/images';
        $apiUrl = $this->_store_url.$resource;
        $result = $this->get($apiUrl);
        $resultArr = json_decode($result, true);
        $resultArr =$this->arrayUtf8Encoder($resultArr);
        $imgArray = $resultArr['data'];
        foreach ($imgArray as $img) {
            if($img['is_thumbnail'] == true)
            {
                $images['url_standard'] = $img['url_standard']; 
                $images['url_thumbnail'] = $img['url_thumbnail']; 
            }
        }
        return $images;
    }

    public function getAllProductImages($productId, $callFrom = '')
    {
        $resource = 'v3/catalog/products/'.$productId.'/images';
        $apiUrl = $this->_store_url.$resource;
        $result = $this->get($apiUrl);
        $resultArr = json_decode($result, true);
        $resultArr =$this->arrayUtf8Encoder($resultArr);
        $imgArray = $resultArr['data'];
        $images = $urlThumbnail = $urlStandard = array();
        foreach ($imgArray as $img) {
            if($callFrom =='getAllProducts'){
                if($img['is_thumbnail']==1){
                    $urlThumbnail[] = $img['url_thumbnail']; 
                    $urlStandard[] = $img['url_standard']; 
                }
            }else{
                $urlThumbnail[] = $img['url_thumbnail']; 
                $urlStandard[] = $img['url_standard']; 
            }
        }
        $images['url_thumbnail'] = $urlThumbnail;
        $images['url_standard'] = $urlStandard;
        return $images;
    }

	public function getCategoryById($categoryId)
	{
		$resource = 'v3/catalog/products/categories/'.$categoryId;
	 	$apiUrl = $this->_store_url.$resource;
	 	$result = $this->get($apiUrl);
	 	$resultArr = json_decode($result, true);
        $resultArr =$this->arrayUtf8Encoder($resultArr);
	 	return $resultArr;
	}

    public function getCategoryByName()
    {
        $categoryId = 0;
        $resource = "v3/catalog/categories?name=Show_in_Designer";
        $apiUrl = $this->_store_url.$resource;
        $result = $this->get($apiUrl);
        $resultArr = json_decode($result, true);
        $resultArr =$this->arrayUtf8Encoder($resultArr);
        foreach ($resultArr['data'] as $key => $value) {
            if($value['name']=='Show_in_Designer'){
                $categoryId = $value['id'];
            }
        }
        return $categoryId;
    }

	public function getVariantsByProductId($productId)
	{
		$resource = 'v3/catalog/products/'.$productId.'/variants';
	 	$apiUrl = $this->_store_url.$resource;
	 	$result = $this->get($apiUrl);
	 	$resultArr = json_decode($result, true);
        $resultArr =$this->arrayUtf8Encoder($resultArr);
	 	return $resultArr;
	}

	public function getVariantById($productId, $variantId)
	{
		$resource = 'v3/catalog/products/'.$productId.'/variants/'.$variantId;
	 	$apiUrl = $this->_store_url.$resource;
	 	$result = $this->get($apiUrl);
	 	$resultArr = json_decode($result, true);
        $resultArr =$this->arrayUtf8Encoder($resultArr);
	 	return $resultArr;
	}

	public function getColorSwatchProduct($productId, $optionId, $colorId)
	{
		$resource = 'v3/catalog/products/'.$productId.'/options/'.$optionId.'/values/'.$colorId;
	 	$apiUrl = $this->_store_url.$resource;
	 	$result = $this->get($apiUrl);
	 	$resultArr = json_decode($result, true);
        $resultArr =$this->arrayUtf8Encoder($resultArr);
        if($resultArr['status']==404)
        {
        	$resultArr = '';
        	return $resultArr;
        }
        else
        {
	 		return $resultArr['data']['value_data']['colors'][0];
        }
	}

	public function getBulkPricingRules($productId)
	{
		$resource = 'v3/catalog/products/'.$productId.'/bulk-pricing-rules';
	 	$apiUrl = $this->_store_url.$resource;
	 	$result = $this->get($apiUrl);
	 	$resultArr = json_decode($result, true);
        $resultArr =$this->arrayUtf8Encoder($resultArr);
	 	return $resultArr;
	}

    public function getOptionsByProductId($productId)
    {
        $resource = 'v3/catalog/products/'.$productId.'/options';
        $apiUrl = $this->_store_url.$resource;
        $result = $this->get($apiUrl);
        $resultArr = json_decode($result, true);
        $resultArr =$this->arrayUtf8Encoder($resultArr);
        return $resultArr;
    }

    public function getOptions()
    {
        $resource = 'v2/options';
        $apiUrl = $this->_store_url.$resource;
        $result = $this->get($apiUrl);
        $resultArr = json_decode($result, true);
        $resultArr =$this->arrayUtf8Encoder($resultArr);
        return $resultArr;
    }
    public function getProductOptions($productId)
    {
        $resource = 'v3/catalog/products/'.$productId.'/options';
        $apiUrl = $this->_store_url.$resource;
        $result = $this->get($apiUrl);
        $resultArr = json_decode($result, true);
        $resultArr =$this->arrayUtf8Encoder($resultArr);
        return $resultArr;
    }
    public function getOptionValues($url)
    {
        $urlResult = $this->get($url);
        $urlResults = json_decode($urlResult, true);
        $urlResults =$this->arrayUtf8Encoder($urlResults);
        return $urlResults;
    }

    public function getOrder($orderId)
    {
        $resource = 'v2/orders/'.$orderId;
        $apiUrl = $this->_store_url.$resource;
        $result = $this->get($apiUrl);
        $resultArr = json_decode($result, true);
        $resultArr =$this->arrayUtf8Encoder($resultArr);
        return $resultArr;
    }

    public function getAllOrderWithRange($minOrderId, $maxOrderId)
    {
        $resource = 'v2/orders?min_id='.$minOrderId.'&max_id='.$maxOrderId;
        $apiUrl = $this->_store_url.$resource;
        $result = $this->get($apiUrl);
        $resultArr = json_decode($result, true);
        $resultArr =$this->arrayUtf8Encoder($resultArr);
        return $resultArr;
    }

    public function getAllOrder()
    {
        $resource = 'v2/orders?page=1&limit=200';
        $apiUrl = $this->_store_url.$resource;
        $result = $this->get($apiUrl);
        $resultArr = json_decode($result, true);
        $resultArr =$this->arrayUtf8Encoder($resultArr);
        return $resultArr;
    }

    public function getCart($cartId)
    {
        $resource = 'v3/carts/'.$cartId;
        $apiUrl = $this->_store_url.$resource;
        $result = $this->get($apiUrl);
        $resultArr = json_decode($result, true);
        $resultArr =$this->arrayUtf8Encoder($resultArr);
        return $resultArr;
    }

    public function getOrderItems($orderId)
    {
        $resource = 'v2/orders/'.$orderId.'/products';
        $apiUrl = $this->_store_url.$resource;
        $result = $this->get($apiUrl);
        $resultArr = json_decode($result, true);
        $resultArr =$this->arrayUtf8Encoder($resultArr);
        return $resultArr;
    }

    public function getShippingAddress($shippingAddressesUrl)
    {
        $result = $this->get($shippingAddressesUrl);
        $resultArr = json_decode($result, true);
        $resultArr =$this->arrayUtf8Encoder($resultArr);
        return $resultArr;
    }

    public function getOptionById($productId, $optionId)
    {
        $resource = 'v3/catalog/products/'.$productId.'/options/'.$optionId;
        $apiUrl = $this->_store_url.$resource;
        $result = $this->get($apiUrl);
        $resultArr = json_decode($result, true);
        $resultArr =$this->arrayUtf8Encoder($resultArr);
        return $resultArr;
    }
	/**
	 * Performs a get request to the instantiated class
	 * 
	 * Accepts the apiUrl to perform the request on
	 * 
	 * @param $apiUrl string $apiUrl a string to perform get on
	 * @return results or var_dump error
	 */
	public function get($apiUrl){
 		$headers = array(
            'X-Auth-Client: '.$this->_clientid.'',
            'X-Auth-Token: '.$this->_accessToken.'',
            'Accept: application/json',
            'Content-Type: application/json'
        );
	    $ch = curl_init();
	    curl_setopt( $ch, CURLOPT_URL, $apiUrl); 
	    curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers );                                   
	    curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, 'GET'); 
	    curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, 0 ); 
	    curl_setopt( $ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
	    curl_setopt( $ch, CURLOPT_SSL_VERIFYHOST, 0 );
	    curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );   
	    $response = curl_exec( $ch );   
	    curl_close($ch); 
	    return $response;
 	}
	/**
	 * Performs a post request to the instantiated class
	 * 
	 * Accepts the url to perform the request on, and fields to be sent
	 * 
	 * @param string $url a string to perform get on
	 * @param array $fields an array to be sent in the request
	 * @return results or var_dump error
	 */
	public function post($url, $fields) {
        $headers = array(
            'X-Auth-Client: '.$this->_clientid.'',
            'X-Auth-Token: '.$this->_accessToken.'',
            'Accept: application/json',
            'Content-Type: application/json'
        );
		global $error;
		$json = json_encode($fields);
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_VERBOSE, 1);
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($curl, CURLOPT_POSTFIELDS, $json);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		$response = curl_exec ($curl);
		$http_status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
		$header_size = curl_getinfo($curl, CURLINFO_HEADER_SIZE);
		$body = substr($response, $header_size);
		curl_close ($curl);
		return $response;
		if ($http_status == 201) {
			$results = json_decode($body, true);
			return $response;
		} else {
			return $this->error($body, $url, $json, 'POST');
		}
	}
	/**
	 * Performs a put request to the instantiated class
	 * 
	 * Accepts the url to perform the request on, and fields to be sent
	 * 
	 * @param string $url a string to perform get on
	 * @param array $fields an array to be sent in the request
	 * @return results or var_dump error
	 */
	public function put($url, $fields) {
		$headers = array(
            'X-Auth-Client: '.$this->_clientid.'',
            'X-Auth-Token: '.$this->_accessToken.'',
            'Accept: application/json',
            'Content-Type: application/json'
        );
		$json = json_encode($fields);
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_VERBOSE, 1);
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
		curl_setopt($curl, CURLOPT_POSTFIELDS, $json); 
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		$response = curl_exec($curl);
		$http_status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
		$header_size = curl_getinfo($curl, CURLINFO_HEADER_SIZE);
		$body = substr($response, $header_size);
		curl_close($curl);
		if ($http_status == 200) {
			$results = json_decode($body, true);
			return $results;
		} else {
			return $this->error($body, $url, $json, 'PUT');
		}

	}
	/**
	 * Performs a delete request to the instantiated class
	 * 
	 * Accepts the url to perform the request on
	 * 
	 * @param string $url a string to perform get on
	 * @return proper response or var_dump error
	 */
	public function delete($url) {
		$headers = array(
            'X-Auth-Client: '.$this->_clientid.'',
            'X-Auth-Token: '.$this->_accessToken.'',
            'Accept: application/json',
            'Content-Type: application/json'
        );
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_VERBOSE, 1);
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "DELETE");
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		$response = curl_exec($curl);
		$http_status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
		$header_size = curl_getinfo($curl, CURLINFO_HEADER_SIZE);
		$headers = substr($response, 0, $header_size);
		$body = substr($response, $header_size);        
		curl_close ($curl);
		if ($http_status == 204) {
	     	return $http_status . ' DELETED';
		 } else {
		 	return $this->error($body, $url, null, 'DELETE');
		 }
	}
	public function error($body, $url, $json, $type) {
    	global $error;
    	if (isset($json)) {
	    	$results = json_decode($body, true);
			$results = $results[0];
			$results['type'] = $type;
			$results['url'] = $url;
			$results['payload'] = $json;
			return $error = $results;
		} else {
			$results = json_decode($body, true);
			$results = $results[0];
			$results['type'] = $type;
			$results['url'] = $url;
			return $error = $results;
		}
    }
}
