<?php
/* Check Un-authorize Access */
if(!defined('accessUser')) die("Error");	 
class OrdersStore extends UTIL {
	public function __construct(){
		parent::__construct();
		$this->storeApi = new storeApi();
	}
	public function orderLists($lastOrderId,  $range, $start)
	{
		$result = $this->orderDetailsFromDesignerAdminApp($lastOrderId, $range, $start);
		$orderLists = array();
		$k = 0;
		foreach ($result as $key => $value) {
			$orderDate = date('Y-m-d H:i:s', strtotime($value['date_created'])); 
			$orderLists[$k]['order_id'] = $value['id'];
			$orderLists[$k]['order_incremental_id'] = $value['order_id'];
			$orderLists[$k]['order_status'] = $value['status'];
			$orderLists[$k]['order_date'] = $orderDate;
			$orderLists[$k]['customer_name'] = $value['customer_name'];
			$orderLists[$k]['id'] = $value['order_id'];
			$k++;
		}
		$res['order_list'] = $orderLists;
		return $res;
	}

	public function orderDetailsFromDesignerAdminApp($lastOrderId, $range, $start)
    {
        try{
            $select_sql = 'SELECT * FROM '.TABLE_PREFIX.'webhook_data ';
            $select_sql .= ($lastOrderId)?'WHERE order_id>'.$lastOrderId.' ' : '';
            $select_sql .= ($range)?'GROUP BY(order_id) ORDER BY id desc limit ' .$start .', '. $range . '' : 'GROUP BY(order_id) ORDER BY id desc';
            $rows = $this->executeGenericDQLQuery($select_sql);
            return $rows;
        }catch(Exception $e){
            $e->getMessage();
        }
    }
	
	/**
	 * Get all orders  by order id from store
	 *
	 * @param (Int)lastOrderId
	 * @param (Int)range
	 * @param (Int)start
	 * @param (Date)fromDate
	 * @param (Date)toDate
	 * @return json array 
	*/
	public function orderList(){
		$error = false;
		if ($this->checkSendQuote()) {
			$lastOrderId = (isset($this->_request['start']) && trim($this->_request['start']) != '') ? trim($this->_request['start']) : 0;
			$range = (isset($this->_request['range']) && trim($this->_request['range']) != '') ? trim($this->_request['range']) : 0;
			try {
				$result = $this->getOrderlistByRange($lastOrderId,$range);
			} catch (Exception $e) {
				$result = json_encode(array('isFault' => 1, 'faultMessage' => $e->getMessage()));
				$error = true;
			}
		} else {
			$lastOrderId = (isset($this->_request['lastOrderId']) && trim($this->_request['lastOrderId'])!='')?trim($this->_request['lastOrderId']):0;
			$range = (isset($this->_request['range']) && trim($this->_request['range'])!='')?trim($this->_request['range']):0;
			$start = (isset($this->_request['start']) && trim($this->_request['start'])!='')?trim($this->_request['start']):0;
			$fromDate = (isset($this->_request['fromDate']) && trim($this->_request['fromDate'])!='')?trim($this->_request['fromDate']):'';
			$toDate = (isset($this->_request['toDate']) && trim($this->_request['toDate'])!='')?trim($this->_request['toDate']):'';

			try {
				$orderLists = $this->orderLists($lastOrderId, $range, $start);
			}catch(Exception $e) {
				$result = json_encode(array('isFault' => 1, 'faultMessage'=>$e->getMessage()));
				$error = true;
			}
		}
			// get print status
		$result = $orderLists;
		foreach ($result['order_list'] as $k => $V) {
			$order['id'] = $order['order_id'];
			$result[$k] = $order;
		}
		$orderArr = $result['order_list'];
		//$orderArr = array_reverse($orderArr);
		if(!$error){
			$result = array('orderArr' => $orderArr, 'result' => $orderArr, 'store_res' => $result);
		}else{  
			$result = array('isFault' => 1, 'error' => json_decode($result));                 
		}
		return $result;
	}
	
	/**
	 * get all orders date  from store
	 *
	 * @param (Int)from
	 * @param (Int)to
	 * @return json array 
	*/
	public function getOrdersGraph() {
		$error = false;
		$from = (isset($this->_request['from']) && trim($this->_request['from'])!='')?trim($this->_request['from']):'';
		$to = (isset($this->_request['to']) && trim($this->_request['to'])!='')?trim($this->_request['to']):'';
		try {
			$orderLists = $this->orderDetailsFromDesignerAdminApp($lastOrderId = false, $range = false, $start = false);
			$result = array();
			$tempId = 0;
			$count = -1;
			$j = 0;
			foreach ($orderLists as $order) {
				$orderData[$j] = array(
					'created_date' => date('Y-m-d', strtotime($order['date_created'])),
					'order_id' => $order['order_id']
				);
				$j++;
			}

			foreach ($orderData as $k => $v) {
				$orderDate = $v['created_date'];
				if ($tempId != strtotime($orderDate)) {
					$i = 0;
					$count++;
					$tempId = strtotime($orderDate);
					$result[$count]['date'] = $orderDate;
					$result[$count]['sales'] = $i+1;
				} else {
					$i++;
					$result[$count]['date'] = $orderDate;
					$result[$count]['sales'] = $i+1;
				}
			}
		}catch(Exception $e) {
			$result = json_encode(array('isFault' => 1, 'faultMessage'=>$e->getMessage()));
			$error = true;
		}
		if(!$error){
			$result = json_encode($result);
			print_r($result);exit;
		} else {
			print_r(json_decode($result));exit;                 
		}
	}
	
	/**
	 * Get order details by order id
	 *
	 * @param (Int)orderIncrementId
	 * @return json array 
	*/
	public function getOrderDetails(){
		$error = false;
		$result = $this->storeApiLogin();
		if($this->storeApiLogin==true){
			$orderId = 0;
			if(isset($this->_request['orderIncrementId']) && trim($this->_request['orderIncrementId'])!='') {
				$orderId = trim($this->_request['orderIncrementId']);
			} 
			try {
				$result = $this->storeApi->getOrderDetails($orderId);
			} catch(Exception $e) {
				$result = json_encode(array('isFault' => 1, 'faultMessage'=>$e->getMessage()));
				$error = true;
			}
			if(!$error){
				print_r($result);exit;
			} else {
				print_r(json_decode($result));exit;                 
			}
		}else{
			$msg=array('status'=>'apiLoginFailed','error'=>json_decode($result));
			$this->response($this->json($msg), 200);
		}
	}
	
	/**
	*
	*date 15th_Apr-2016
	*download zip containing order files for Order App
	*
	* @param (int)order_id 
	* @param (int)range  
	* @param (int)fd
	* @param (int)download	
	* @return zip file link or zip download
	* 
	*/
	public function downloadOrdersZipApp(){	
		ini_set('memory_limit','18000M');
		$downloadUrl = ''; $msg = "Success";
		$last_order_id = (isset($this->_request['last_order_id']))?$this->_request['last_order_id']:0;
		$range = (isset($this->_request['range']))?$this->_request['range']:20;
		$fd = (isset($this->_request['fd']))?$this->_request['fd']:0; // force Download flag//
		$download = (isset($this->_request['download']))?$this->_request['download']:'';
		$is_create = (isset($this->_request['is_create']))?$this->_request['is_create']:0;
		// get Orders id from Store (returns an array with all selected orders) //
		$error = false;$orders =array();
		$result = $this->storeApiLogin();
		$lastOrderId = $last_order_id;
		$start = $last_order_id;
		try {
			$orders = $this->orderLists($last_order_id, $range, $start);
		}catch(Exception $e) {
			$result = json_encode(array('isFault' => 1, 'faultMessage'=>$e->getMessage()));
			$msg=array('status'=>'apiLoginFailed','error'=>json_decode($result));
			$this->response($this->json($msg), 200);
		}
		$orders = isset($orders['order_list']) ? $orders['order_list'] : array();
		$orders2 = $orders;
		$orderPath = $this->getOrdersPath();
		if (is_array($orders2) && count($orders2) > 0) {
			if ($fd == 1 || $is_create == 1) {
                // if $is_create is 1 or $fd (force download) is 1 //
				foreach ($orders2 as $order_id_arry) {
					$order_id = $order_id_arry['order_id'];
					$increment_id = $order_id_arry['order_incremental_id'];
					
					// fetch order_id or incremental_id //
					if (file_exists($orderPath . "/" . $increment_id) && is_dir($orderPath . "/" . $increment_id)) { 
						$orderFolderPath = $orderPath . "/" . $increment_id; // increment_id //
						$orderTypeFlag = 1;
						$order_id = $increment_id;
					}else{
						$orderFolderPath = $orderPath . "/" . $order_id;
						$orderTypeFlag = 0;
					}

					if (file_exists($orderFolderPath) && is_dir($orderFolderPath)) {
                        $scanProductDir = scandir($orderFolderPath); // scan directory to fetch all items folder //
                        if (file_exists($orderFolderPath . '/order.json')) {
                        	$order_json = file_get_contents($orderFolderPath . '/order.json');
                        	$json_content = $this->formatJSONToArray($order_json);
                        	foreach ($json_content['order_details']['order_items'] as $item_details) {
                        		$item_id = $item_details['item_id'];
                        		$sizeArr[$item_id] = $item_details['xe_size'];
                        	}
                        }

                        if (is_array($scanProductDir)) {
                        	foreach ($scanProductDir as $itemDir) {
                                if ($itemDir != '.' && $itemDir != '..' && is_dir($orderFolderPath . "/" . $itemDir)){ // check the item folders under the product folder //
									//Fetch the design state json details //
                                	$designState = file_get_contents(XEPATH . "xetool" . ORDER_PATH_DIR . "/" . $order_id . "/" . $itemDir . "/designState.json");
                                	$resultDesign = $this->formatJSONToArray($designState);

									// check if side_index folder exists or not //
                                	$sidePath = $orderFolderPath . "/" . $itemDir;
                                	$scanSidePath = scandir($sidePath);
                                	$scanSideDir = $scanSidePath;
                                	$orderTypeFlag = 0;
                                	if(is_array($scanSideDir)){
                                		foreach($scanSideDir as $sidecheckPath){
                                			if(strpos($sidecheckPath, "side_") !== false){
                                				$orderTypeFlag = 1;
                                				continue;
                                			}
                                		}
                                	}

									// for new file structure //
                                	if($orderTypeFlag == 1){
								        //check and find the sides of each item //
                                		$sidePath = $orderFolderPath . "/" . $itemDir; 
                                		if (file_exists($sidePath) && is_dir($sidePath)){
									        $scanSideDir = scandir($sidePath); // scan item directory to fetch all side folders //
									        $scanSideDirSide = $scanSideDir;
									        if(is_array($scanSideDir)){
									        	foreach ($scanSideDir as $sideDir){
									        		if($sideDir != '.' && $sideDir != '..' && is_dir($orderFolderPath . "/" . $itemDir. "/". $sideDir)){
															$i = str_replace("side_","",$sideDir);  //to fetch only side folders//
															if (file_exists($orderFolderPath."/".$itemDir."/".$sideDir."/preview_0".$i.".svg")){
																// with product svg file exists or not//
																if ($fd == 1){
																	$reqSvgFile = XEPATH."xetool".ORDER_PATH_DIR."/".$order_id."/".$itemDir."/".$sideDir."/preview_0".$i.".svg";
																	$item_id = $itemDir;
																	//check name and number exists or not

																	if (!empty($resultDesign['nameNumberData'])) {
																		$this->creteNameAndNumberSeparatedSvg($reqSvgFile,$resultDesign, $order_id, $item_id, $sizeArr[$item_id],1);
																	}else{
																		$this->createWithoutProductSvg($reqSvgFile, $order_id, $item_id, $resultDesign, 1);
																	}
																	$msg = 'Success';
																}else{
																	if (!file_exists($orderFolderPath . "/" . $itemDir . "/" . $sideDir. "/". $sideDir."_".$itemDir."_".$order_id . ".svg") && $is_create == 1){
																		/* check if without product svg file exists or not.if not exist, then create the file */
																		$reqSvgFile = XEPATH."xetool".ORDER_PATH_DIR."/".$order_id."/".$itemDir."/".$sideDir."/preview_0".$i.".svg";
																		$item_id = $itemDir;
																	//check name and number exit or not

																		if (!empty($resultDesign['nameNumberData'])) {
																			$this->creteNameAndNumberSeparatedSvg($reqSvgFile,$resultDesign, $order_id, $item_id, $sizeArr[$item_id],1);
																		}else{
																			$this->createWithoutProductSvg($reqSvgFile, $order_id, $item_id, $resultDesign, 1);
																		}

																		$msg = 'Success';
																	}
																}
															}
														}
													}
												}
											}
										}
										// for old file structure //
										else if($orderTypeFlag == 0){
											//to fetch only item id folders //
											$kounter = 1;
											for($i=1;$i<=15;$i++){
												if(file_exists($orderFolderPath."/".$itemDir."/preview_0".$i.".svg")){// with product svg file exists or not//	
													if($fd == 1){
														$reqSvgFile = XEPATH."xetool".ORDER_PATH_DIR."/".$order_id."/".$itemDir."/preview_0".$i.".svg";
														$item_id = $itemDir;
														//check name and number exit or not
														$designState = file_get_contents(XEPATH."xetool".ORDER_PATH_DIR."/".$order_id."/".$item_id."/designState.json");
														$resultDesign = $this->formatJSONToArray($designState);
														if(!empty($resultDesign['nameNumberData'])){
															$this->creteNameAndNumberSeparatedSvg($reqSvgFile,$resultDesign,$item_id,$order_id,$sizeArr[$item_id],0);
														}else{
															$this->createWithoutProductSvg($reqSvgFile, $order_id, $item_id, $resultDesign, 0);
														}
														$msg = 'Success';
													}
													else{
														if(!file_exists($orderFolderPath."/".$itemDir."/".$i.".svg") && $is_create == 1){ 
															/* check if without product svg file exists or not. if not exist, then create the file */
															$reqSvgFile = XEPATH."xetool".ORDER_PATH_DIR."/".$order_id."/".$itemDir."/preview_0".$i.".svg";
															$item_id = $itemDir;
															//check name and number exit or not
															$designState = file_get_contents(XEPATH."xetool".ORDER_PATH_DIR."/".$order_id."/".$item_id."/designState.json");
															$resultDesign = $this->formatJSONToArray($designState);
															if(!empty($resultDesign['nameNumberData'])){
																$this->creteNameAndNumberSeparatedSvg($reqSvgFile,$resultDesign,$item_id,$order_id,$sizeArr[$item_id],0);
															}else{
																$this->createWithoutProductSvg($reqSvgFile, $order_id, $item_id, $resultDesign, 0);
															}
															$msg = 'Success';
														}
													}
												}
											}	
										}
									}
								}
							}
						}
					}
				}
				$orderFolderPath = $this->getOrdersPath();
				$zipName = 'orders_' . time() . '.zip';
				$zip = new \ZipArchive;
				$res = $zip->open($orderFolderPath . '/' . $zipName, ZipArchive::CREATE);
            // check if zip file created //
				if ($res === true) {
					$zipCheckKounter = 0;
					$tarray = array(" ", "\n", "\r");
					if (file_exists($this->getBasePath() . "localsettings.js")) {
						$contents = file_get_contents($this->getBasePath() . "/localsettings.js");
						$contents = trim(str_replace($tarray, "", $contents));
						$contents = substr($contents, 0, -1);
						$contents = explode("localSettings=", $contents);
						$contents = json_decode($contents['1']);
						$temp_array = array();
						$store_url = $settings_arry['base_url'];
						$temp_array['base_url'] = $contents->base_url . $contents->service_api_url;
						$temp_array['api_key'] = $contents->api_key;
						$new_json = json_encode($temp_array);
						$zip->addFromString('settings.json', $new_json);
					}
					$order_list_arr = array();
					$temp_arry212 = array();
                    // loop all order ids //
					foreach ($orders as $order_id_arry2) {
						$orderNo = $order_id_arry2['order_id'];
						$order_incremental_id = $order_id_arry2['order_incremental_id'];

						$orderFolderPath = $this->getOrdersPath();
					    // fetch order_id or incremental_id //
						if (file_exists($orderFolderPath . "/" . $order_incremental_id) && is_dir($orderPath . "/" . $order_incremental_id)) { 
							$orderTypeFlag = 1;
							$orderNo = $order_incremental_id;
						}else{
							$orderTypeFlag = 0;
						}

						if (file_exists($orderFolderPath . "/" . $orderNo)) {
							if (file_exists($orderFolderPath . '/' . $orderNo . '/order.json')) {
								$order_json = file_get_contents($orderFolderPath . '/' . $orderNo . '/order.json');
								$json_content = json_decode($order_json, true);
								$noOfRefIds = count($json_content['order_details']['order_items']);
								if ($noOfRefIds > 0) {
									$zip->addEmptyDir($orderNo);
									$zip->addFile($orderFolderPath . '/' . $orderNo . '/order.json', $orderNo . '/order.json');
									$zip->addFile($orderFolderPath . '/' . $orderNo . '/info.html', $orderNo . '/info.html');
									$item_kounter = 1;
									foreach ($json_content['order_details']['order_items'] as $item_details) {
										$item_id = $item_details['item_id'];
										$ref_id = $item_details['ref_id'];
									$scanDirArr = scandir($orderFolderPath . "/" . $orderNo. "/". $item_id); //for name and number folder scan
									
									// check if side_index folder exists or not //
									$sidePath = $orderFolderPath . "/" . $orderNo. "/" . $item_id;
									$scanSidePath = scandir($sidePath);
									$scanSideDir = $scanSidePath;
									$orderTypeFlag = 0;
									if(is_array($scanSideDir)) {
										foreach($scanSideDir as $sidecheckPath){
											if(strpos($sidecheckPath, "side_") !== false){
												$orderTypeFlag = 1;
												continue;
											}
										}
									}
									if ($item_id != null && $item_id > 0 && $ref_id != null && $ref_id > 0) {
										$zip->addEmptyDir($orderNo . "/" . $item_id);
										if($orderTypeFlag == 1){
											// add side folders inside item directory //
											//Fetch the design state json details //
											$designState = file_get_contents(XEPATH . "xetool" . ORDER_PATH_DIR . "/" . $orderNo . "/" . $item_id . "/designState.json");
											$resultDesign = $this->formatJSONToArray($designState);
											//echo "<pre>"; print_r($resultDesign['sides']);
											$sidesCount = count($resultDesign['sides']);
											for($flag=1;$flag<=$sidesCount;$flag++){
												if(is_dir($orderFolderPath . "/" . $orderNo. "/". $item_id."/side_".$flag)){
													$zip->addEmptyDir($orderNo . "/" . $item_id."/side_".$flag);
												}
												
												$scanDirArr = scandir($orderFolderPath . "/" . $orderNo. "/". $item_id."/side_".$flag);
												if (count($scanDirArr) > 2) {
													//for name and number folder scan
													foreach ($scanDirArr as $nameAndNumberDir) {
														if ($nameAndNumberDir != '.' && $nameAndNumberDir != '..' && is_dir($orderFolderPath . "/" . $orderNo. "/". $item_id . "/side_".$flag.'/' . $nameAndNumberDir)) {
															$zip->addEmptyDir($orderNo . "/" . $item_id."/side_".$flag);
															$from_url = $orderFolderPath . "/" . $orderNo. "/". $item_id."/side_".$flag.'/' . $nameAndNumberDir;
															$options = array('add_path' => $orderNo . "/" . $item_id ."/side_".$flag.'/' . $nameAndNumberDir . "/", 'remove_path' => $from_url);
															$zip->addGlob($from_url . '/*{svg}', GLOB_BRACE, $options);
														}
													}
												}
												
												//copy to side folder //
												$fromUrlSide = $orderFolderPath . "/" .$orderNo. "/". $item_id."/side_".$flag;
												$optionsSide = array('add_path' => $orderNo . "/" . $item_id . "/side_".$flag."/", 'remove_path' => $fromUrlSide);
												$zip->addGlob($fromUrlSide . '/*{svg,json,html,pdf,png,jpg}', GLOB_BRACE, $optionsSide);
												
												//copy to asset folder //
												if(is_dir($orderFolderPath . "/" . $orderNo. "/". $item_id."/side_".$flag."/assets")){
													$zip->addEmptyDir($orderNo . "/" . $item_id."/side_".$flag."/assets");
													
													$fromUrlAsset = $orderFolderPath . "/" .$orderNo. "/". $item_id."/side_".$flag."/assets";
													$optionsAsset = array('add_path' => $orderNo . "/" . $item_id . "/side_".$flag."/assets/", 'remove_path' => $fromUrlAsset);
													$zip->addGlob($fromUrlAsset . '/*{svg,json,html,pdf,png,jpg,jpeg,PNG,bmp,BMP}', GLOB_BRACE, $optionsAsset);
												}
												
												//copy to preview folder //
												if(is_dir($orderFolderPath . "/" . $orderNo. "/". $item_id."/side_".$flag."/preview")){
													$zip->addEmptyDir($orderNo . "/" . $item_id."/side_".$flag."/preview");
												}
												
												$fromUrlPreview = $orderFolderPath . "/" . $orderNo. "/". $item_id."/side_".$flag."/preview";
												$optionsPreview = array('add_path' => $orderNo . "/" . $item_id . "/side_".$flag."/preview/", 'remove_path' => $fromUrlPreview);
												$zip->addGlob($fromUrlPreview . '/*{png,PNG}', GLOB_BRACE, $optionsPreview);
												
												//delete preview svg from zip //
												$zip->deleteName($orderNo . "/" . $item_id."/side_".$flag."/preview_0".$flag.".svg");
											}
											
											$from_url = $orderFolderPath . "/" . $orderNo. "/". $item_id;
											$options = array('add_path' => $orderNo . "/" . $item_id . "/", 'remove_path' => $from_url);
											$zip->addGlob($from_url . '/*{svg,json,html,pdf,png,jpg}', GLOB_BRACE, $options);
											$zipCheckKounter++;
											
										}else if($orderTypeFlag == 0){
										    $scanDirArr = scandir($orderFolderPath."/".$orderNo. "/".$item_id);//for name and number folder scan
											if(count($scanDirArr) >2){//for name and number folder scan
												foreach($scanDirArr as $nameAndNumberDir){
													if($nameAndNumberDir != '.' && $nameAndNumberDir != '..' && is_dir($orderFolderPath."/".$orderNo. "/".$item_id."/".$nameAndNumberDir)){
														$zip->addEmptyDir($orderNo."/".$item_id."/".$nameAndNumberDir);
														$from_url = $orderFolderPath."/".$orderNo. "/".$item_id."/".$nameAndNumberDir;
														$options = array('add_path' => $orderNo."/".$item_id."/".$nameAndNumberDir."/",'remove_path' => $from_url);
														$zip->addGlob($from_url.'/*{svg}', GLOB_BRACE, $options);
													}
												}
											}//end for name and number zip download
											$from_url = $orderFolderPath."/".$orderNo."/".$item_id;
											$options = array('add_path' => $orderNo."/".$item_id."/",'remove_path' => $from_url);
											$zip->addGlob($from_url.'/*{svg,json,html,pdf,png,jpg}', GLOB_BRACE, $options);
											$zipCheckKounter++;
										}
									}
									$item_kounter++;
								}
							}
						}
						array_push($order_list_arr, array("order_id" => $order_id_arry2['order_id'], "order_incremental_id" => $order_incremental_id, "order_type" => $orderTypeFlag));
					}
				}
				$order_ary = array();
				$order_ary['orderList'] = $order_list_arr;
                // create Orderlist json //
				$zip->addFromString('orderList.json', json_encode($order_ary));
				$zip->close();
                //Check if download option is enabled //
				if ($zipCheckKounter > 0) {
					if ($download == 1) {
						$this->zipDownload($orderFolderPath . '/' . $zipName, $zipCheckKounter);
					} else {
						$store_url = XEPATH . "xetool/custom-assets/orders/";
						$msg = $store_url . $zipName;
					}
				} else {
					$msg = 'No Order files Found on server to download';
					if (file_exists($orderFolderPath . '/' . $zipName)) {
						unlink($orderFolderPath . '/' . $zipName);
					}
				}
			} else {
				$msg = 'Zip Creation Failed';
			}
		} else {
			$msg = 'No Orders Found to download';
		}
		$msg2 = array("Response" => $msg);
		$this->response($this->json($msg2), 200);
	}
	/**
	*Fetch customize order by order id
	*
	* @param (int)order_id
	* @return integer 
	*
	*/
	public function customizeOrder(){
		echo $ref_id = $this->storeApi->customizeOrder($this->_request['order_id']);exit;
	}
	/**
     * Get refid by order_details_id
     *
     * @param   Int($orderDetailId)
     * @return  Int
     *
     */
	public function getRefIdByOrderDetailId(){
		echo $ref_id = $this->storeApi->getRefIdByOrderDetailId($this->_request['orderDetailId']);exit;
	}
	/**
     * get pending order count by last order id
     *
     * @param   Int($last_id)
     * @return  Array($result)
     *
     */
	public function getPendingOrdersCount(){
		$result = $this->storeApi->getPendingOrdersCount($this->_request['last_id']);
		$this->response($this->json($result), 200);
	}
	/**
     *
     *date  17th_Nov-2017
     *Create details of order placed through webhook call
     *
     * @param (int)order_id
     * @param (int)fd
     * @param (int)download
     * @return zip file link or zip download
     *
     */
	public function orderGenerate()
	{
		try{
			$orderId = $this->_request['order_id'];
            // Get new order details
			$response = $this->storeApi->getOrder($orderId);
			$orderItems = $this->storeApi->getOrderItems($orderId);
			$isCustomize = $this->storeApi->checkIsCustomize($orderItems[0]['product_id']);
			if(!empty($response)){
				$cartId = $response['cart_id'];
				$customerName = $response['billing_address']['first_name'] . " " . $response['billing_address']['last_name'];
            	$statusId = $response['status_id'];
            	$status = $response['status'];
				$refDetails = $this->getRefIdByCartId($cartId);
				$refId = $refDetails[0]['ref_id'];
				if(empty($refId) && $refId==''){
					exit;
				}
				$insSql = "INSERT INTO " . TABLE_PREFIX . "webhook_data SET order_id = '" . $orderId . "', order_number = '" . $orderId . "', customer_name = '" . $customerName . "', status_id = '" . $statusId . "', status = '" . $status . "', date_created = '" . date("Y:m:d h:i:s") . "'";
				$id = $this->executeGenericInsertQuery($insSql);
            	// new order-code
				$itemArray = array();
				$carts = Flight::carts();
				
				if (!empty($orderItems)) {
					$l = 0;
					foreach ($orderItems as $item) {
						$refid = $carts->getRefidByOrderIdAndSku($cartId,$item['sku']);
						if (!empty($refid[0]['ref_id']) && $refid[0]['ref_id']!='') {
							$isOrder = 1;
							array_push($itemArray, array('item_id' => $item['id'], 'ref_id' => $refid[0]['ref_id']));
							$orderItems[$l]['ref_id'] = $refid[0]['ref_id'];
							$orderItems[$l]['name'] = $refid[0]['product_name'];
							$orderItems[$l]['sku'] = $refid[0]['sku'];
							$l++;
						}
					}
				}
				$toolDir = DOC_ROOT.'/'. APPNAME . '/xetool/';
				$folder = $toolDir . 'custom-assets/orders';
				if (!file_exists($folder)) {
					$a = mkdir($folder, 0777, true);
				}

				/*Added Restriction*/
				$htmlIndex = '<html><body><h1>Access Restricted</h1></body></html>';
				$htAccessContents = "DirectoryIndex index.html";
				$orderassetspath = $toolDir . "custom-assets";
				$pathIndexCustom = $orderassetspath;
				$myFileIndexCustom = $pathIndexCustom . "/index.html";
				$fhIndexCustom = fopen($myFileIndexCustom, 'w');
				fwrite($fhIndexCustom, $htmlIndex);
				fclose($myFileIndexCustom);
                //Add htaccess file on custom-assets directory
				$pathHtCustom = $orderassetspath;
				$myFileHtCustom = $pathIndexCustom . "/.htaccess";
				$fhHtCustom = fopen($myFileHtCustom, 'w');
				fwrite($fhHtCustom, $htAccessContents);
				fclose($myFileHtCustom);

               	//add restricated index.html file
				$pathIndex = $orderassetspath . "/orders";
				$myFileIndex = $pathIndex . "/index.html";
				$fhIndex = fopen($myFileIndex, 'w');
				fwrite($fhIndex, $htmlIndex);
				fclose($myFileIndex);
                //Add htaccess file on orders directory
				$pathHt = $orderassetspath . "/orders";
				$myFileHt = $pathIndex . "/.htaccess";
				$fhHt = fopen($myFileHt, 'w');
				fwrite($fhHt, $htAccessContents);
				fclose($myFileHt);
				/*END*/
	            // create a folder for the order
	            //$order_num = $response['order_number'];
				$orderDir = $folder . '/' . $orderId;
				if (!file_exists($orderDir) && $isOrder) {
	                //$this->manageInventory($order_id);
					mkdir($orderDir, 0755);
				}
	            //create order.json file
				$orderDetails = array();
				$orderDetails['billing_address']['first_name'] = $response['billing_address']['first_name'];
				$orderDetails['billing_address']['telephone'] = $response['billing_address']['phone'];
				$orderDetails['billing_address']['email'] = $response['billing_address']['email'];
				$orderDetails['billing_address']['street'] = $response['billing_address']['street_1'];
				$orderDetails['billing_address']['city'] = $response['billing_address']['city'];
				$orderDetails['billing_address']['postcode'] = $response['billing_address']['zip'];

				$shippingAddressesUrl = $response['shipping_addresses']['url'];
				$shippingAddresses = $this->storeApi->getShippingAddress($shippingAddressesUrl);

				$orderDetails['shipping_address']['first_name'] = $shippingAddresses['0']['first_name'];
				$orderDetails['shipping_address']['telephone'] = $shippingAddresses['0']['phone'];
				$orderDetails['shipping_address']['email'] = $shippingAddresses['0']['email'];
				$orderDetails['shipping_address']['street'] = $shippingAddresses['0']['street_1'];
				$orderDetails['shipping_address']['city'] = $shippingAddresses['0']['city'];
				$orderDetails['shipping_address']['postcode'] = $shippingAddresses['0']['zip'];

				$orderDetails['order_id'] = $response['id'];
				$orderDetails['order_incremental_id'] = $response['id'];
				$orderDetails['order_status'] = $response['status'];
				$orderDetails['order_date'] = date('Y-m-d H:i:s', strtotime($response['date_created']));
				$orderDetails['customer_name'] = $response['billing_address']['first_name'] . " " . $response['billing_address']['last_name'];
				$orderDetails['customer_email'] = $response['billing_address']['email'];
				$orderDetails['shipping_method'] = $shippingAddresses[0]['shipping_method'];

				$shipAddrCustomerName = $shippingAddresses['0']['first_name'] .' '. $shippingAddresses['0']['last_name'];
				$companyName = $shippingAddresses['0']['company'];
				$street_1 = $shippingAddresses['0']['street_1'];
				$street_2 = $shippingAddresses['0']['street_2'];
		        $zip = $shippingAddresses['0']['zip'];
				$city = $shippingAddresses['0']['city'];
		        $state = $shippingAddresses['0']['state'];
		        $country = $shippingAddresses['0']['country'];
		        $phone = $shippingAddresses['0']['phone'];

				$orderDetails['order_items'] = array();
				foreach ($orderItems as $item) {
					$itemDetails = array();
					$itemDetails['itemStatus'] = "Ordered";
					$itemDetails['ref_id'] = $item['ref_id'];
					$itemDetails['item_id'] = $item['id'];
					$itemDetails['print_status'] = null;
					$itemDetails['product_price'] = $item['base_price'];
					$itemDetails['config_product_id'] = $item['product_id'];
					$itemDetails['product_id'] = $item['product_id'];
					$itemDetails['product_sku'] = $item['sku'];
					$itemDetails['product_name'] = $item['name'];
					$itemDetails['quantity'] = $item['quantity'];
					$productOptions = $item['product_options'];
					$xeColor = '';
					$xeSize = '';
					foreach ($productOptions as $po) {
						if(strtolower($po['display_name'])=='size'){
							$xeSize = $po['display_value'];
						}
						if(strtolower($po['display_name'])=='color'){
							$xeColor = $po['display_value'];
						}
					}
	                $variant = explode(" / ", $item['variant_title']);//
	                $itemDetails['xe_color'] = $xeColor;
	                $itemDetails['xe_size'] = $xeSize;
	                $orderDetails['order_items'][] = $itemDetails;
	            }
	            $orderContent = json_encode(array('order_details' => $orderDetails));
	            if (file_exists($orderDir)) {
	            	file_put_contents($orderDir . '/' . 'order.json', $orderContent);
	            }

	            // make folder for each item in the order
	            if (count($itemArray) > 0) {
	            	foreach ($itemArray as $item) {
	            		$item_dir = $orderDir . '/' . $item['item_id'] . '/';
	            		$refIDs[] = $item['ref_id'];
	            		$svg_root = $toolDir . ASSET_PATH . '/previewimg/' . $item['ref_id'] . '/svg/';
	            		$this->recurse_copy($svg_root, $item_dir);
	            	}
	            }

	            $refIds = '';
	            if (!empty($refIDs)) {
	            	$refIds = implode(",", $refIDs);
	            }
	            $imgApiUrl = XEPATH . 'xetool/api/index.php?reqmethod=getCustomPreviewImages&refids=' . $refIds;
	            $imgResult = file_get_contents($imgApiUrl);
	            if ($imgResult) {
	            	$capturedImages = json_decode($imgResult, true);
	            }
	            if (count($itemArray) > 0) {
	                // info.html creation
	                // get lang details
	            	$langsFile = file_get_contents($toolDir . "localsettings.js");
	            	$pos = strpos($langsFile, '"');
	            	$langsFile = substr($langsFile, $pos);
	            	$langsFile = '{' . str_replace(';', '', $langsFile);
	            	$langsFile = json_decode($langsFile, true);
	            	$lang = $langsFile['language'];
	            	if ($lang) {
	            		$langFile = 'locale-' . $lang . '.json';
	            	} else {
	            		$langFile = 'locale-en.json';
	            	}

	            	$langPath = $toolDir . "languages/" . $langFile;
	            	if (!file_exists($langPath)) {
	            		$this->debug_to_console('File not found :' . $langPath);
	            	}

	            	$languageJson = file_get_contents($langPath);
	            	$languageJson1 = json_decode($languageJson, true);
	            	$langOrderId = (!empty($languageJson1['ORDER_ID'])) ? $languageJson1['ORDER_ID'] : 'Order Id';
	            	$langOrderDate = (!empty($languageJson1['ORDER_DATE'])) ? $languageJson1['ORDER_DATE'] : 'Order Date';
	            	$langOrderTime = (!empty($languageJson1['ORDER_TIME'])) ? $languageJson1['ORDER_TIME'] : 'Order Time';
	            	$langQuantity = (!empty($languageJson1['QUANTITY'])) ? $languageJson1['QUANTITY'] : 'Quantity';
	            	$langCategory = (!empty($languageJson1['CATEGORY'])) ? $languageJson1['CATEGORY'] : 'Category';
	            	$langPrintMethod = (!empty($languageJson1['PRINT_METHOD'])) ? $languageJson1['PRINT_METHOD'] : 'Print Method';
	            	$langSize = (!empty($languageJson1['SIZE'])) ? $languageJson1['SIZE'] : 'Size';
	            	$langColor = (!empty($languageJson1['COLOR'])) ? $languageJson1['COLOR'] : 'Color';
	            	$langPrintSize = (!empty($languageJson1['PRINT_SIZE'])) ? $languageJson1['PRINT_SIZE'] : 'Print Size';
	            	$langColorName = (!empty($languageJson1['COLOR_NAME'])) ? $languageJson1['COLOR_NAME'] : 'Color Name';
	            	$langCmyk = (!empty($languageJson1['CMYK'])) ? $languageJson1['CMYK'] : 'CMYK';
	            	$langHex = (!empty($languageJson1['HEX'])) ? $languageJson1['HEX'] : 'Hex';
	            	$langBRIP = (!empty($languageJson1['BROWSER_IP'])) ? $languageJson1['BROWSER_IP'] : 'Browser IP';
	            	$langBRLan = (!empty($languageJson1['BROWSER_LANGUAGE'])) ? $languageJson1['BROWSER_LANGUAGE'] : 'Accept Language';
	            	$langBRName = (!empty($languageJson1['BROWSER_NAME'])) ? $languageJson1['BROWSER_NAME'] : 'Browser Name';
	            	$langBRAgent = (!empty($languageJson1['BROWSER_AGENT'])) ? $languageJson1['BROWSER_AGENT'] : 'Browser Agent';
	            	$langBRHt = (!empty($languageJson1['BROWSER_HEIGHT'])) ? $languageJson1['BROWSER_HEIGHT'] : 'Browser Height';
	            	$langBRWd = (!empty($languageJson1['BROWSER_WIDTH'])) ? $languageJson1['BROWSER_WIDTH'] : 'Browser Width';
	            	$langHeight = (!empty($languageJson1['HEIGHT'])) ? $languageJson1['HEIGHT'] : 'Height';
	            	$langWidth = (!empty($languageJson1['WIDTH'])) ? $languageJson1['WIDTH'] : 'Width';

	            	$html = '<html><style>table, th, td {border: 1px solid #ccc; border-collapse: collapse;} th, td {padding: 8px;text-align: left; color:#333;} table.t01 tr:nth-child(even) {background-color: #efefef;}
	            	table.t01 tr:nth-child(odd) {background-color:#efefef;} table.t01 th {background-color: #666;color: white;} .barcode{margin-right:50px; margin-top:20px;} .m-b-7{margin-bottom:7px;}</style><title>Order Info</title><body style="font-family:arial;"><div style="width: 1200px; margin:auto;"><div style="clear: both;min-height: 100px;border-bottom: 1px solid #ccc;margin-bottom: 20px;font-size: 18px;line-height: 25px;padding-bottom: 15px;">';
	            	$generatorPNG = new Picqer\Barcode\BarcodeGeneratorPNG();

	            	$date = date("d-m-Y, H:i:s", strtotime($response['date_created']));
	            	$order_date = date("Y-m-d", strtotime($response['date_created']));
	            	$timestamp = gmdate($date);
	            	$splitTimeStamp = explode(" ", $timestamp);
	            	$time = $splitTimeStamp[1];

	                // ordr details(header part)
	            	$custName = $response['billing_address']['first_name'] . ' ' . $response['billing_address']['last_name'];

	            	$html .= '<div style="padding: 0px 0px 20px 0px;display: inline-block;width: 65%;vertical-align: top;"><h2 style="margin:0px; padding:0px; font-weight:normal;font-size: 17px;">' . $langOrderId . ': <span style="color:#333;">' .
	            	$response['id'] . '</span></h2> <h3 style="margin:0px; padding:0px; font-weight:normal;font-size: 17px;">' . $langOrderDate . ':&nbsp;' . $order_date . '&nbsp;<span style="color:#ababab;">(YYYY-MM-DD)</span></h3><h3 style="margin:0px; padding:0px; font-weight:normal;font-size: 17px;">' . $langOrderTime . ':&nbsp;' . $time . '</h3><br><img src="data:image/png;base64,' . base64_encode($generatorPNG->getBarcode($response['id'], $generatorPNG::TYPE_CODE_128)) . '" alt="" height="35px" /></div><address style="display: inline-block;font-style: normal;width: 30%;"> <strong style="font-size: 20px; display: block; margin-bottom: 5px;">Shipping Address:</strong><span>'.$shipAddrCustomerName.'</span><br>';

	            	if($companyName != ''){
			            $html .= '<span>'.$companyName.'</span><br>';
					}

					if($street_1 != ''){
			            $html .= '<span>'.$street_1.'</span><br>';
					}

					if($street_2 != ''){
			            $html .= '<span>'.$street_2.'</span><br>';
					}

					if($zip != ''){
			            $html .= '<span>'.$zip.'</span><br>';
					}

					if($city != ''){
			            $html .= '<span>'.$city.'</span><br>';
					}

					if($state != ''){
			            $html .= '<span>'.$state.'</span><br>';
					}

					if($country != ''){
			            $html .= '<span>'.$country.'</span><br>';
					}

					if($phone != ''){
			            $html .= '<span title="Phone">'.$phone.'</span><br>';
					}

	            	$html .= '</address></div>';

	                // product name
	            	$toMail = $response['billing_address']['email'];
	            	$nameCount = 0;
	            	$temp = array();
	            	foreach ($orderItems as $lt) {
	            		$sizePos = $colorPos = $extraAtr = "";
	            		$extraAtt = array();
	            		$options = $lt['product_options'];
	            		foreach ($options as $option) {
	            			if(strtolower($option['display_name'])=='size'){
	            				$sizePos = $option['display_name'];
	            				$sizeValue = $option['display_value'];
	            			}
	            			if(strtolower($option['display_name'])=='color'){
	            				$colorPos = $option['display_name'];
	            				$colorValue = $option['display_value'];
	            			}
	            			if (strtolower($option['display_name']) !== 'color' && strtolower($option['display_name']) !== 'size' && strtolower($option['display_name']) !== 'quantity' && strtolower($option['display_name']) !== 'title') {
	            				$extraAtt[$option['display_name']] = $option['display_value'];
	            			}
	            		}
	            		$designPath = XEPATH . "xetool/custom-assets/orders/" . $orderId . "/" . $lt['id'] . "/";
	            		$ref_id = $lt['ref_id'];
	            		$quantity = (int) $lt['quantity'];
	            		$productname = $lt['name'];
	            		$sku = $lt['sku'];
	            		$skuwithname = $productname;
	            		$product_barcode = base64_encode($generatorPNG->getBarcode($skuwithname, $generatorPNG::TYPE_CODE_128));
	            		$designState_json = file_get_contents($toolDir . ASSET_PATH . "/previewimg/" . $ref_id . "/svg/designState.json");
	            		$json_content = json_decode($designState_json, true);
	            		$noOfsides = count($json_content['sides']);

	                    // folder create
	            		foreach ($json_content['sides'] as $key => $sides) {
	            			$sideNum = $key + 1;
	            			if ($sides['svg'] != '') {
	            				$newSideDir = $orderDir . '/' . $lt['id'] . '/side_' . $sideNum;
	            				if (!file_exists($newSideDir)) {
	            					mkdir($newSideDir, 0755);
	            				}
	            				$prevDir = $newSideDir . '/preview';
	            				if (!file_exists($prevDir)) {
	            					mkdir($prevDir, 0755);
	            				}

	            				$imgContent = file_get_contents($sides['customizeImage']);
	                            // put captured image in preview folder.
	            				$previewImg = fopen($newSideDir . "/preview/side_" . $sideNum . "_" . $lt['id'] . "_" . $order_id . ".png", "w");
	            				fwrite($previewImg, $imgContent);
	            				fclose($previewImg);
	                            // Move  preview svg to side folder
	            				$prevSVGDir = $orderDir . '/' . $lt['id'] . '/preview_0' . $sideNum . '.svg';
	            				rename($prevSVGDir, $newSideDir . '/preview_0' . $sideNum . '.svg');
	                            // Getting Asset files for each side.
	            				$assetDir = $toolDir . ASSET_PATH . "/previewimg/" . $ref_id . "/assets/" . $sideNum . "/";
	            				if (file_exists($assetDir)) {
	            					mkdir($newSideDir . "/assets/", 0755);
	            					$this->recurse_copy($assetDir, $newSideDir . "/assets/");
	            				}
	            			} else {
	            				$unDesignedPrevSVG = $orderDir . '/' . $lt['id'] . '/preview_0' . $sideNum . '.svg';
	            				unlink($unDesignedPrevSVG);
	            			}
	            		}
	                    // folder create
	                    //Check for repeated name and number
	            		if (in_array($ref_id, $temp)) {
	            			if (!empty($json_content['nameNumberData']) && $nameCount != 0) {
	            				continue;
	            			}
	            		}
	            		$temp[] = $ref_id;
	            		$printColorNames = $printColors = $cmykValue = $printColorCategories = "";
	            		$k = 0;

	            		$odd = 1;
	            		$color = ($colorincrment % 2 == 0) ? 'red' : 'blue';

	            		$printType = (isset($json_content['printType']) && $json_content['printType'] != '') ? $json_content['printType'] : "No Printtype";
	            		$notes = (isset($json_content['notes']) && $json_content['notes'] != '') ? $json_content['notes'] : "";
	                    $browserIp = (isset($json_content['envInfo']) && $json_content['envInfo']['browserIp'] != '') ? $json_content['envInfo']['browserIp'] : "-";//$response['order']['client_details']['browser_ip'];
	                    $browserHeight = (isset($json_content['envInfo']) && $json_content['envInfo']['browserHeight'] != '') ? $json_content['envInfo']['browserHeight'] : "-";
	                    $browserWidth = (isset($json_content['envInfo']) && $json_content['envInfo']['browserWidth'] != '') ? $json_content['envInfo']['browserWidth'] : "-";
	                    $browserLang = (isset($json_content['envInfo']) && $json_content['envInfo']['browserLang'] != '') ? $json_content['envInfo']['browserLang'] : "-";
	                    $userAgent = (isset($json_content['envInfo']) && $json_content['envInfo']['userAgent'] != '') ? $json_content['envInfo']['userAgent'] : "-";
	                    $browserName = (isset($json_content['envInfo']) && $json_content['envInfo']['browserName'] != '') ? $json_content['envInfo']['browserName'] : "-";
	                    // Multiple Boundary
	                    $multipleBoundary = (isset($json_content['multipleBoundary']) && $json_content['multipleBoundary'] != '') ? $json_content['multipleBoundary'] : "false";
	                    if ($ref_id) {
	                    	$html .= '<div style="border: 2px solid #4CAF50; margin-bottom:20px; border-radius:5px; float:left; width:100%;"><div style="background-color:#4CAF50; padding:15px;border-radius:2px 2px 0px 0px; font-size:20px; color:#fff;">' . $productname . '</div>
	                    	<div style="padding:20px;"><table style="border: 1px solid #ccc; border-collapse: collapse; width: 100%;"><tbody><tr>';
	                        //Hide heading of size attribute and quantity for name and number case
	                    	if (empty($json_content['nameNumberData'])) {
	                    		$html .= '<th style="border: 1px solid #ccc; border-collapse: collapse;padding: 8px;text-align: left; color:#333;">' . ucwords($langQuantity) . '</th>';
	                    		if ($sizePos != '') {
	                    			$html .= '<th style="border: 1px solid #ccc; border-collapse: collapse;padding: 8px;text-align: left; color:#333;">' . ucwords($langSize) . '</th>';
	                    		}
	                    	}
	                    	if ($colorPos != '') {
	                    		$html .= '<th style="border: 1px solid #ccc; border-collapse: collapse;padding: 8px;text-align: left; color:#333;">' . ucwords($langColor) . '</th>';
	                    	}
	                    	if (!empty($extraAtt)) {
	                    		foreach ($extraAtt as $key1 => $extraAtr) {
	                    			$html .= '<th style="border: 1px solid #ccc; border-collapse: collapse;padding: 8px;text-align: left; color:#333;">' . $key1 . '</th>';
	                    		}
	                    	}
	                        // Hide product wise print method Multiple Boundary
	                    	if ($multipleBoundary != 'true') {
	                    		$html .= '<th style="border: 1px solid #ccc; border-collapse: collapse;padding: 8px;text-align: left; color:#333;">' . $langPrintMethod . '</th>';
	                    	}
	                    	$html .= '</tr>';
	                        //Hide value of size attribute and quantity for name and number case
	                    	if(empty($json_content['nameNumberData'])) {
	                    		$html .= '<td style = "border: 1px solid #ccc; border-collapse: collapse; padding: 8px; text-align: left; color: #333;">' . $quantity . '</td>';
	                    		if ($sizePos != '') {
	                    			$html .= '<td style = "border: 1px solid #ccc; border-collapse: collapse; padding: 8px; text-align: left; color: #333;">' . $sizeValue . '</td>';
	                    		}
	                    	}
	                    	if ($colorPos != '') {
	                    		$html .= '<td style = "border: 1px solid #ccc; border-collapse: collapse; padding: 8px; text-align: left; color: #333;">' . $colorValue . '</td>';
	                    	}
	                    	if (!empty($extraAtt)) {
	                    		foreach ($extraAtt as $key2 => $extraAtr) {
	                    			$html .= '<td style = "border: 1px solid #ccc; border-collapse: collapse; padding: 8px; text-align: left; color: #333;">' . $extraAtr . '</td>';
	                    		}
	                    	}
	                        // Hide product wise print method Multiple Boundary
	                    	if ($multipleBoundary != 'true') {
	                    		$html .= '<td style = "border: 1px solid #ccc; border-collapse: collapse; padding: 8px; text-align: left; color: #333;">' . $printType . '</td>';
	                    	}
	                    	$html .= '</tr></tbody></table></div>';
	                    }
	                    //Name and number table start
	                    if (!empty($json_content['nameNumberData'])) {
	                    	$nameCount++;
	                    	$langNameFrontText = "-";
	                    	$langNameBackText = "-";
	                    	if ($json_content['nameNumberData']['front']) {
	                    		if ($json_content['nameNumberData']['frontView'] == "name_num") {
	                    			$frontView = "Name & Number";
	                    		} elseif ($json_content['nameNumberData']['frontView'] == "name") {
	                    			$frontView = "Name Only";
	                    		} else {
	                    			$frontView = "Number Only";
	                    		}
	                    		$langNameFrontText = $frontView;
	                    	}
	                    	if ($json_content['nameNumberData']['back']) {
	                    		if ($json_content['nameNumberData']['backView'] == "name_num") {
	                    			$backView = "Name & Number";
	                    		} elseif ($json_content['nameNumberData']['backView'] == "name") {
	                    			$backView = "Name Only";
	                    		} else {
	                    			$backView = "Number Only";
	                    		}
	                    		$langNameBackText = $backView;
	                    	}
	                    	$html .= '<div style="padding: 0px 20px 15px 20px;"><b>Name & Number Details:</b><br/><div style="height: 140px;overflow: auto;"><table style="border: 1px solid #ccc; border-collapse: collapse; width: 100%;"><tbody><tr><th style="border: 1px solid #ccc; border-collapse: collapse;padding: 8px;text-align: left; color:#333;"> Name' . $langName . '</th><th style="border: 1px solid #ccc; border-collapse: collapse;padding: 8px;text-align: left; color:#333;">Number' . $langNumber . '</th><th style="border: 1px solid #ccc; border-collapse: collapse;padding: 8px;text-align: left; color:#333;">' . $langSize . '</th><th style="border: 1px solid #ccc; border-collapse: collapse;padding: 8px;text-align: left; color:#333;">Front' . $langNameFront . '</th><th style="border: 1px solid #ccc; border-collapse: collapse;padding: 8px;text-align: left; color:#333;">Back' . $langNameBack . '</th></tr>';
	                    	foreach ($json_content['nameNumberData']['list'] as $singleName) {
	                    		$html .= '<tr><td style = "border: 1px solid #ccc; border-collapse: collapse; padding: 8px; text-align: left; color: #333;">' . $singleName['name'] . '</td><td style = "border: 1px solid #ccc; border-collapse: collapse; padding: 8px; text-align: left; color: #333;">' . $singleName['number'] . '</td><td style = "border: 1px solid #ccc; border-collapse: collapse; padding: 8px; text-align: left; color: #333;">' . $singleName['size'] . '</td><td style = "border: 1px solid #ccc; border-collapse: collapse; padding: 8px; text-align: left; color: #333;">' . $langNameFrontText . '</td><td style = "border: 1px solid #ccc; border-collapse: collapse; padding: 8px; text-align: left; color: #333;">' . $langNameBackText . '</td>';
	                    	}
	                    	$html .= '</tr></tbody></table></div></div>';
	                    }
	                    if (!empty($notes)) {
	                    	$html .= '<div style="padding: 0px 20px 0px 20px;"><b>Notes:</b><br/><textarea rows="4" class="notes" style="border: 0px;width: 100%;resize: vertical;padding-left: 15px;font-family: inherit;" readonly>' . $notes . '</textarea></div>';
	                    }
	                    // all faces details...one by one
	                    foreach ($json_content['sides'] as $key => $sides) {
	                    	$capturedImageurl = $sides['customizeImage'];
	                    	//Previously in place of "$capturedImageurl", "$capturedImages[$ref_id][$k]['customImageUrl']()" was there
	                    	$onesidewidth = ($noOfsides <= 1) ? 'width:93%;' : '';
	                    	$barcodewidth = ($noOfsides <= 1) ? 'width:40%' : 'width:100%';
	                    	$printUnit = (isset($sides['printUnit']) && $sides['printUnit'] != '') ? $sides['printUnit'] : "No Unit";
	                    	$dimension = $sides['PrintDimension']['boundwidth'] . 'x' . $sides['PrintDimension']['boundheight'];
	                    	$addClearDiv = ($odd % 2 == 0) ? '<div style="clear:both;"/> &nbsp; </div>' : '';
	                    	if (isset($sides['printSize']) && $sides['printSize'] != '') {
	                    		$printSize = $sides['printSize'];
	                    		$printSize .= ': ' . $dimension . ' (' . $printUnit . ')';
	                    	} else {
	                    		$printSize = "No PrintSize";
	                    	}

	                    	$printColorNames = (isset($sides['printColorNames'])) ? count($sides['printColorNames']) : 0;
	                    	$height = $printColorNames * 30;

	                    	$html .= '<div style="padding: 0px 20px 20px 20px; width: 45%; text-align: center; float: left; background-color: #efefef; margin: 20px 0px 20px 13px;' . $onesidewidth . '"><div class="m-b-7 barcode"><img src="data:image/png;base64,' . $product_barcode . '" height="50px" alt="" style = "' . $barcodewidth . '" /></div><div style="margin-bottom:10px;"><h3 style="margin:0px; padding:0px; font-weight:normal;"><h3 style="font-weight:normal;">' . $skuwithname . '</h3></div><div style="margin-bottom:20px; min-height:500px; min-width:500px;"><img src="' .$capturedImageurl. '" alt="" style="width: 500px;" /> </div>';

	                    	if (isset($sides['printSize']) && $multipleBoundary != 'true') {
	                    		$printValue = $sides['printSize'];
	                    		if ($printValue[0] == '') {
	                    			$printSize = $dimension . ' (' . $printUnit . ')';
	                    		} else {
	                    			$printSize = $sides['printSize'];
	                    			$printSize .= ': ' . $dimension . ' (' . $printUnit . ')';
	                    		}
	                    		$html .= '<div><h2 style="margin-bottom:10px; min-height:30px; font-weight:normal;">' . $langPrintSize . ':   ' . $printSize . '</h2></div>';
	                    	}
	                    	$html .= '<div style="height: 141px;overflow: auto;">';
	                    	if ($printColorNames > 0 && $multipleBoundary != 'true') {
	                    		$html .= '<table style="width: 100%; height:' . $height . 'px" class="repeattd t01"><tbody> <tr>
	                    		<th style="border: 1px solid #ccc; border-collapse: collapse; padding: 8px; text-align: left; background-color: #666; color: white;">' . $langColorName . '</th> <th style="border: 1px solid #ccc; border-collapse: collapse; padding: 8px; text-align: left; background-color: #666; color: white;">' . $langCategory . '</th>
	                    		<th style="border: 1px solid #ccc; border-collapse: collapse; padding: 8px; text-align: left; background-color: #666; color: white;">' . $langCmyk . '</th> <th style="border: 1px solid #ccc; border-collapse: collapse; padding: 8px; text-align: left; background-color: #666; color: white;">' . $langHex . '</th> </tr>';
	                    		foreach ($sides['printColorNames'] as $y => $printcolornames) {
	                    			$printcolornames = (!empty($printcolornames)) ? $printcolornames : '-';
	                    			$printColors[$y] = (!empty($sides['printColors'])) ? $sides['printColors'][$y] : '-';
	                    			$printColors[$y] = ($printColors[$y][0] == "#") ? $printColors[$y] : '<img src="' . $printColors[$y] . '" width="20" height="20" />';
	                    			if (!empty($sides['cmykValue']) && $sides['cmykValue'][$y] != "") {
	                    				$content_svg = json_encode(array_change_key_case($sides['cmykValue'][$y], CASE_UPPER));
	                    				$cmykValue[$y] = substr($content_svg, 0, -1);
	                    				$cmykValue[$y] = ltrim($cmykValue[$y], '{');
	                    				$cmykValue[$y] = str_replace('"', '', $cmykValue[$y]);
	                    			} else {
	                    				$cmykValue[$y] = '-';
	                    			}
	                    			$printColorCategories[$y] = (!empty($sides['printColorCategories'])) ? $sides['printColorCategories'][$y] : '-';
	                    			$html .= '<tr> <td style="border: 1px solid #ccc; border-collapse: collapse;padding: 8px;text-align: left; color:#333;">' . $printcolornames . '</td> <td style="border: 1px solid #ccc; border-collapse: collapse;padding: 8px;text-align: left; color:#333;">' . $printColorCategories[$y] . '</td>
	                    			<td style="border: 1px solid #ccc; border-collapse: collapse;padding: 8px;text-align: left; color:#333;">' . $cmykValue[$y] . '</td>
	                    			<td style="border: 1px solid #ccc; border-collapse: collapse;padding: 8px;text-align: left; color:#333;">' . $printColors[$y] . '</td> </tr>';
	                    		}
	                    		$html .= '</tbody></table>';
	                    	} elseif ($multipleBoundary == 'true') {
	                    		foreach ($sides['multiBound'] as $section => $sectionDetails) {
	                    			$html .= '<table style="width: 100%; margin-bottom: 25px; height:' . $height . 'px" class="repeattd t01"><tbody> <tr><th colspan="4" style="border: 1px solid #ccc; border-collapse: collapse; padding: 8px; background-color: #2f2f2f; color: white;"><span style="float:left;">' . $sectionDetails['name'];
	                    			if ($sectionDetails['printProfile'] != "") {
	                    				$html .= ': ' . $sectionDetails['printProfile'] . '</span>';
	                    				$html .= ' <span style="display: block; clear: both; font-size: 14px; color: #ddd;">' . $langWidth . ': ' . $sectionDetails['actulWidth'] . '&nbsp;' . $sides['printUnit'] . '&nbsp;&nbsp;' . $langHeight . ': ' . $sectionDetails['actulHeight'] . '&nbsp;' . $sides['printUnit'] . '</span>';
	                    			}
	                    			$html .= '</th></tr>';
	                    			if (!empty($sectionDetails['printColors'])) {
	                    				$html .= '<tr><th style="border: 1px solid #ccc; border-collapse: collapse; padding: 8px; text-align: left; background-color: #666; color: white;">' . $langColorName . '</th> <th style="border: 1px solid #ccc; border-collapse: collapse; padding: 8px; text-align: left; background-color: #666; color: white;">' . $langCategory . '</th><th style="border: 1px solid #ccc; border-collapse: collapse; padding: 8px; text-align: left; background-color: #666; color: white;">' . $langCmyk . '</th> <th style="border: 1px solid #ccc; border-collapse: collapse; padding: 8px; text-align: left; background-color: #666; color: white;">' . $langHex . '</th> </tr>';
	                    			} else {
	                    				$html .= '<tr> <td style="border: 1px solid #ccc; border-collapse: collapse;padding: 8px;text-align: left; color:#333;"> -- </td> <td style="border: 1px solid #ccc; border-collapse: collapse;padding: 8px;text-align: left; color:#333;"> -- </td><td style="border: 1px solid #ccc; border-collapse: collapse;padding: 8px;text-align: left; color:#333;"> -- </td><td style="border: 1px solid #ccc; border-collapse: collapse;padding: 8px;text-align: left; color:#333;"> -- </td> </tr>';
	                    			}
	                    			foreach ($sectionDetails['printColorNames'] as $section => $sectionColorDetail) {
	                    				$sectionColorDetail = (!empty($sectionColorDetail)) ? $sectionColorDetail : '-';
	                    				$printColors[$section] = (!empty($sectionDetails['printColors'])) ? $sectionDetails['printColors'][$section] : '-';
	                    				$printColors[$section] = ($printColors[$section][0] == "#") ? $printColors[$section] : '<img src="' . $printColors[$section] . '" width="20" height="20" />';
	                    				if (!empty($sectionDetails['cmykValue'][$section])) {
	                    					$content_svg = json_encode(array_change_key_case($sectionDetails['cmykValue'][$section], CASE_UPPER));
	                    					$cmykValue[$section] = substr($content_svg, 1, -1);
	                    					$cmykValue[$section] = str_replace('"', '', $cmykValue[$section]);
	                    				} else {
	                    					$cmykValue[$section] = '-';
	                    				}
	                    				$printColorCategories[$section] = (!empty($sectionDetails['printColorCategories'])) ? $sectionDetails['printColorCategories'][$section] : '-';
	                    				$html .= '<tr> <td style="border: 1px solid #ccc; border-collapse: collapse;padding: 8px;text-align: left; color:#333;">' . str_replace('No Name', '-', $sectionColorDetail) . '</td> <td style="border: 1px solid #ccc; border-collapse: collapse;padding: 8px;text-align: left; color:#333;">' . str_replace('No Category', '-', $printColorCategories[$section]) . '</td><td style="border: 1px solid #ccc; border-collapse: collapse;padding: 8px;text-align: left; color:#333;">' . $cmykValue[$section] . '</td><td style="border: 1px solid #ccc; border-collapse: collapse;padding: 8px;text-align: left; color:#333;">' . $printColors[$section] . '</td> </tr>';
	                    			}
	                                $html .= '</tbody></table>'; //end tablecell div
	                            }
	                        } else {
	                        	if (isset($sides['printSize']) && $sides['printSize'] != '') {
	                        		$html .= '<div style="height: 141px;overflow: auto;"><table style="width: 100%; height:' . $height . 'px" class="repeattd t01"><tbody><div style=" text-align: center; padding-top: 55px;"> &nbsp; </div></tbody></tbody></table></div>';
	                        	} else {
	                        		$html .= '<div style="height: 141px;overflow: auto;"><table style="width: 100%; height:' . $height . 'px" class="repeattd t01"><tbody><div style=" text-align: center; padding-top: 55px;"> &nbsp; <br> </div></tbody></tbody></table></div>';
	                        	}
	                        }
	                        $html .= '</div></div>' . $addClearDiv;
	                        $odd++;
	                        $k++;
	                    }
	                    if ($ref_id) {
	                    	$html .= '</div>';
	                    }
	                }
	                $html .= '</div>';
	                $simpindex++;
	                $colorincrment++;
	                $html .= '<div style="width: 1200px; margin:auto;"><h1 style="margin:0px; padding:0px; font-weight:normal;font-size: 20px;padding-bottom: 8px;">Environment Information :</h1><div style="clear:both;height:100px; background-color:; border-bottom:1px solid #ccc; margin-bottom: 20px;border-bottom: 0px;"><table style="width:100%;border: 1px solid #ccc; border-collapse: collapse;"><tbody><tr><th style="border: 1px solid #ccc; border-collapse: collapse;padding: 8px;text-align: left; color:#333;">' . $langBRIP . '</th><th style="border: 1px solid #ccc; border-collapse: collapse;padding: 6px;text-align: left; color:#333;">' . $langBRLan . '</th><th style="border: 1px solid #ccc; border-collapse: collapse;padding: 6px;text-align: left; color:#333;">' . $langBRAgent . '</th><th style="border: 1px solid #ccc; border-collapse: collapse;padding: 6px;text-align: left; color:#333;">' . $langBRName . '</th><th style="border: 1px solid #ccc; border-collapse: collapse;padding: 6px;text-align: left; color:#333;">' . $langBRWd . '</th><th style="border: 1px solid #ccc; border-collapse: collapse;padding: 6px;text-align: left; color:#333;">' . $langBRHt . '</th></tr><tr><td style="border: 1px solid #ccc; border-collapse: collapse;padding: 6px;text-align: left; color:#333;">' . $browserIp . '</td><td style="border: 1px solid #ccc; border-collapse: collapse;padding: 8px;text-align: left; color:#333;">' . $browserLang . '</td><td style="border: 1px solid #ccc; border-collapse: collapse;padding: 8px;text-align: left; color:#333;">' . $userAgent . '</td><td style="border: 1px solid #ccc; border-collapse: collapse;padding: 6px;text-align: left; color:#333;">' . $browserName . '</td><td style="border: 1px solid #ccc; border-collapse: collapse;padding: 6px;text-align: left; color:#333;">' . $browserWidth . 'px' . '</td><td style="border: 1px solid #ccc; border-collapse: collapse;padding: 6px;text-align: left; color:#333;">' . $browserHeight . 'px' . '</td></tr></tbody></table></div></div>';
	                $html .= '</div></body></html>';
	                $htmlPath = $toolDir . "custom-assets/orders/";
	                $myFile = $htmlPath . $orderId . "/info.html";
	                $fh = fopen($myFile, 'w');
	                fwrite($fh, $html);
	                $htmlUpdated = str_ireplace('class="m-b-7 barcode"', 'style = "display:none;"', $html);
	                $htmlUpdated = str_ireplace('Environment Information :', '', $htmlUpdated);
	                $this->sendCustomerMail($toMail, $htmlUpdated);
	            }

	            if (count($itemArray) > 0) {
	            	foreach ($orderItems as $item) {
	            		$custProdID = $item['product_id'];
	            		$apiUrl = XEPATH . 'xetool/api/index.php?reqmethod=editCustomProduct&pid=' . $custProdID;
	            		$res = file_get_contents($apiUrl);
	            	}
	            }
	        }
	    }catch(Exception $e){
	    	return $e->getMessage();
	    }
	}

	private function recurse_copy($src, $dst)
	{
		$dir = opendir($src);
		@mkdir($dst);
		while (false !== ($file = readdir($dir))) {
			if (($file != '.') && ($file != '..')) {
				if (is_dir($src . '/' . $file)) {
					@recurse_copy($src . '/' . $file, $dst . '/' . $file);
				} else {
					@copy($src . '/' . $file, $dst . '/' . $file);
				}
			}
		}
		closedir($dir);
	}



	public function getRefIdByCartId($cartId)
	{
		$select_sql = 'SELECT DISTINCT ref_id FROM '.TABLE_PREFIX.'cart_data WHERE cart_id="'.$cartId.'"';
		$rows = $this->executeGenericDQLQuery($select_sql);
		return $rows;
	}

    /**
     * get all orders from designer admin
     *
     * @param (Int)lastOrderId
     * @param (Int)start
     * @param (Int)range
     * @return json array 
    */
    public function orderDetailsFromDesignerAdmin($lastOrderId, $range)
    {
    	try{
    		$select_sql = 'SELECT order_id, id FROM '.TABLE_PREFIX.'webhook_data ';
    		$select_sql .= ($lastOrderId)?'WHERE order_id<'.$lastOrderId.' ' : '';
    		$select_sql .= ($range)?'GROUP BY(order_id) ORDER BY id desc limit ' . $range . '' : 'GROUP BY(order_id) ORDER BY id desc';
    		$rows = $this->executeGenericDQLQuery($select_sql);
    		return $rows;
    	}catch(Exception $e){
    		$e->getMessage();
    	}
    }

    /**
     *
     * created_date 11-10-2017
     * Returns Orders with print status
     *
     * 
     * @param ()
     * @return Array od Data
     *
     */
    public function orderArrayWithPrintStatus($orderArr, $printStatus, $storeRes) {
        foreach ($orderArr as $k => $v) {
            foreach ($printStatus as $k1 => $v1) {
                if ($v['order_incremental_id'] == $v1['order_id']) {
                    unset($orderArr[$k]['id']);
                    $orderArr[$k]['print_status'] = $v1['order_status'];
                }
            }
        }
        $response = array('is_Fault' => 0, 'order_list' => $orderArr);
        return $response;
    }
    /**
     * update order status
     *
     * @param (Int)lastOrderId
     * @param (Int)start
     * @param (Int)range
     * @return json array 
    */
    public function updateOrderStatus()
    {
        try{
            $orderId = $this->_request['order_id'];
            $response = $this->storeApi->getOrder($orderId);
            if(!empty($response)){
            	$statusId = $response['status_id'];
            	$status = $response['status'];
            	$insSql = "UPDATE " . TABLE_PREFIX . "webhook_data SET status_id = '" . $statusId . "', status = '" . $status . "' WHERE order_id = ".$orderId;
            	$id = $this->executeGenericInsertQuery($insSql);
            }
        }catch(Exception $e){
            $e->getMessage();
        }
    } 
}