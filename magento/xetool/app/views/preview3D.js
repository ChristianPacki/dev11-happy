download = '';
define('preview3D/controllers/controller', [], function () {
    require(['lib/download.js'], function (down) {
        download = down;
    });
    preview3DModule.register.controller("preview3dController", ['$scope', '$rootScope', '$timeout', 'MainEvent', 'AppModelService', function($scope, $rootScope, $timeout, MainEvent, AppModelService) {
        THREE.ImageUtils.crossOrigin = "";
        var scene, camera, renderer, object, group, material, img,
            bitmap, g, boxmaterial, rotateAngle = 0,
            isPaused = false,
            ismousedown = false,
            lastmouseposition, encoder,
            WIDTH = 1024,
            HEIGHT = 768,
            stageContext,
            captureCanvas, captureContext, captureImg, captureCount = 0,
            modelFile, modelScale, textureImg, reflectivity = 0.5,
            materialColor = 0xffffff,
            shininess = 10,
            direction = 1,
            rotateSpeed = 1,
            gifSource, uploadImgSrc, data,
            watermark, imageblob;
        $scope.imagepath = '';
        $scope.rotateAngle = 0;
        $scope.objectColors = [];
        $scope.isLoading = false;
        $scope.loadingMessage = 'Loading..';
        $scope.backgrounds = [];
        $scope.isPreloading = true;
        $scope.showGifPopup = false;
        $scope.is3dDesignshare = false;
        $scope.isShareClicked = true;
        var objectfor3Dpreview = {};
        var object3DData = {};
        var setimagerectangle = [];
        $scope.show3dPreview = false;
        $scope.loadSettings = function() {
            THREE.ImageUtils.crossOrigin = "";
            $scope.isLoading = true;
            objectfor3Dpreview = AppModelService.getobjectfor3Dpreview();
            object3DData = objectfor3Dpreview.json;
            setimagerectangle = object3DData.imageRectangle;
            var canvas = document.getElementById('productsvgcanvas');
            document.getElementById("productsvgcanvas").style.visibility = "hidden";
            var context = canvas.getContext('2d');
            var img = new Image();
            img.onload = function() {
                if (object3DData.imageRectangle !== null) {
                    context.drawImage(this, setimagerectangle[0], setimagerectangle[1], setimagerectangle[2], setimagerectangle[3]);
                } else {
                    context.drawImage(this, 505, 60, 500, 230);
                }
            }
            img.src = AppModelService.getbase64for3Dpreview();
            modelFile = objectfor3Dpreview.objFilePath;
            textureImg = "assets/images/wrapup.png";
            init();
        }
        init = function() {
            $scope.isShareClicked = true;
            $scope.is3dDesignshare = false;
            //console.log = function(){};
            // Create the scene and set the scene size.
            $scope.isPreloading = true;
            scene = new THREE.Scene();
            //scene.fog = new THREE.FogExp2( 0x999999, 0.6, 600 );
            // Create a renderer and add it to the DOM.
            renderer = webglAvailable() ? new THREE.WebGLRenderer({
                antialias: true,
                preserveDrawingBuffer: true
            }) : new THREE.CanvasRenderer({
                antialias: true
            });
            //renderer = new THREE.CanvasRenderer({antialias:true});
            renderer.setClearColor(0x808080, 1);
            renderer.setSize(WIDTH, HEIGHT);
            $('#stageContent').append(renderer.domElement);
            $('#angleSlider').attr('max', 360 / rotateSpeed);
            //renderer.shadowMap.enabled = true;
            //renderer.shadowMap.type = THREE.PCFShadowMap;
            // Create a camera, zoom it out from the model a bit, and add it to the scene.
            camera = new THREE.PerspectiveCamera(40, WIDTH / HEIGHT, 0.1, 100);
            camera.position.y = 0.5;
            scene.add(camera);
            group = new THREE.Object3D();
            scene.add(group);
            /*watermark = new Image();
            watermark.src = 'images/watermark.png';*/
            var controls = new THREE.OrbitControls(camera, renderer.domElement);
            controls.minDistance = 20;
            controls.maxDistance = 50;
            controls.enableZoom = true;
            controls.maxPolarAngle = Math.PI / 3;
            camera.position.x = Math.cos(180 * Math.PI / 180) * 1;
            camera.position.y = -1;
            camera.lookAt(new THREE.Vector3(0.5, 0.25, 0));
            controls.update();
            addLights();
            addBackground();
            addModel();
        }

        function addLights() {
            // Create some lights and add them to the scene.
            //var spotLight = new THREE.SpotLight(0xffffff, 0.7, 0, Math.PI / 2);
            //spotLight.position.set(-500, 200, -580);
            //spotLight.target.position.set(0, 0, 0);
            ////spotLight.shadowCameraVisible = true;
            //spotLight.castShadow = false;
            //spotLight.shadow.camera.near = 200; // keep near and far planes as tight as possible
            //spotLight.shadow.camera.far = 5000; // shadows not cast past the far plane
            //spotLight.shadow.camera.fov = 20;
            //spotLight.shadow.bias = 0.0001; // a parameter you can tweak if there are artifacts
            ////spotLight.shadow.darkness = 0.2;
            //spotLight.shadow.mapSize.width = 2048;
            //spotLight.shadow.mapSize.height = 2048;
            //scene.add(spotLight);
            //var pointLight2 = new THREE.PointLight(0x84C46E);
            //pointLight2.position.x = 100;
            //pointLight2.position.y = 500;
            //pointLight2.position.z = 150;
            //pointLight2.distance = 1000;
            //pointLight2.intensity = 2;
            ////pointLight2.castShadow = true;
            ////pointLight2.shadowCameraVisible = true;
            //scene.add(pointLight2);
            //var directionalLight = new THREE.DirectionalLight(0xffffff);
            //directionalLight.position.set(-600, 200, 800).normalize();
            //directionalLight.intensity = 0.3;
            ////directionalLight.castShadow = true;
            //scene.add(directionalLight);
            //var directionalLight2 = new THREE.DirectionalLight(0xffffff);
            //directionalLight2.position.set(-300, 400, -300).normalize();
            //directionalLight2.intensity = 0.1;
            //directionalLight2.castShadow = true;
            ////directionalLight2.shadow.camera.visible = true;
            //directionalLight2.shadow.camera.left = -1; // or whatever value works for the scale of your scene
            //directionalLight2.shadow.camera.right = 1;
            //directionalLight2.shadow.camera.top = 1;
            //directionalLight2.shadow.camera.bottom = -1;
            ////directionalLight2.shadowDarkness = 0.2;
            //directionalLight2.shadow.bias = 0.0001;
            //directionalLight2.shadow.radius = 1;
            //directionalLight2.shadow.mapSize.width = 2048;
            //directionalLight2.shadow.mapSize.height = 2048;
            //scene.add(directionalLight2);
            //var ambientLight = new THREE.AmbientLight(0xffffff);
            //ambientLight.intensity = 1;
            ////ambientLight.shadowCameraVisible = true;
            //scene.add(ambientLight);
            var ambientLight = new THREE.AmbientLight(0xffffff);
            ambientLight.intensity = 1.2;
            scene.add(ambientLight);
            var hemiLight = new THREE.HemisphereLight(0xffffbb, 0x080820, 1);
            hemiLight.intensity = 1;
            scene.add(hemiLight);
        }

        function addBackground() {
            backgroundTexture = new THREE.TextureLoader().load('assets/images/trans.png', function() {
                animate();
            });
            backgroundTexture.needsUpdate = true;
            var backgroundGeometry = new THREE.CubeGeometry(0.01, 1.2, 1.55);
            var backgroundMaterial = new THREE.MeshPhongMaterial({
                color: 0xffffff,
                map: backgroundTexture,
            });
            background = new THREE.Mesh(backgroundGeometry, backgroundMaterial);
            background.position.x = 0.5;
            background.position.y = 0.27;
            background.position.z = 0;
            background.rotation.z = -12 * Math.PI / 180;
            scene.add(background);
            scene.remove(background);
        }

        function addModel() {
            modelScale = object3DData.scale;
            //modelScale = 1;
            reflectivity = object3DData.reflectivity;
            shininess = object3DData.shininess;
            materialColor = object3DData.color;
            rotateSpeed = object3DData.rotateSpeed;
            direction = object3DData.direction;
            bitmap = document.getElementById('productsvgcanvas');
            g = bitmap.getContext('2d');
            bitmap.width = 1024;
            bitmap.height = 1024;
            g.fillStyle = "#eeeeee";
            g.fillRect(0, 0, bitmap.width, bitmap.height);
            //g.fillStyle="#F7B359";
            //g.fillRect(100,40,874,330);
            g.fillStyle = "#eeeeee";
            g.fillRect(1024 - 719, 1024 - 306, 719, 306);
            // canvas contents will be used for a texture
            var texture = new THREE.Texture(bitmap);
            texture.needsUpdate = true;
            var path = "assets/images/textures/";
            var format = '.png';
            var urls = [
                path + 'px' + format, path + 'nx' + format,
                path + 'py' + format, path + 'ny' + format,
                path + 'pz' + format, path + 'nz' + format
            ];
            //var textureCube = new THREE.CubeTextureLoader().load(urls);
            //material = new THREE.MeshPhongMaterial({
            //    color: materialColor,
            //    map: texture,
            //    side: THREE.DoubleSide,
            //    //specular: 0xffffff,
            //    //specularMap: backgroundTexture,
            //    shininess: shininess,
            //    envMap: textureCube,
            //    reflectivity: reflectivity,
            //});
            var textureCube = new THREE.CubeTextureLoader().load(urls);
            material = new THREE.MeshStandardMaterial({
                color: materialColor,
                map: texture,
                side: THREE.DoubleSide,
                roughness: 1.0,
                color: 0xffffff,
                metalness: 0.5
                //specular: 0xffffff,
                //specularMap: backgroundTexture,
                //shininess: shininess,
                //envMap: textureCube,
                //reflectivity: reflectivity,
            });
            //var loader = new THREE.OBJLoader();
            //loader.load(modelFile + '/model.obj', function(mesh) {
            //    mesh.scale.set(modelScale, modelScale, modelScale);
            //    mesh.position.x = 0.1;
            //    mesh.position.y = -3;
            //    group.add(mesh);
            //    object = mesh;
            //    //group.rotation.z = Math.PI * 12/180;
            //    //if(textureImg) onFileSelect(textureImg);
            //    //mesh = new THREE.Mesh(geometry, material);
            //    var childFound = false;
            //    mesh.traverse(function(child) {
            //        if (child instanceof THREE.Mesh) {
            //            child.material = material;
            //            if (!childFound) child.visible = false;
            //            child.material.map.needsUpdate = true;
            //            childFound = true;
            //        }
            //    });
            //    /*camera.position.x = Math.cos( 180*Math.PI/180 ) * 1;
            //    camera.position.y = 2*Math.cos( 90*Math.PI/180 );
            //    camera.lookAt( new THREE.Vector3( 0, 0, 0 ) );*/
            //    //camera.position.x = Math.cos(180 * Math.PI / 180) * 1;
            //    //camera.position.y = 0;
            //    //camera.lookAt(new THREE.Vector3(0.5, 0.25, 0));
            //    //renderer.render(scene, camera);
            //    $timeout(function() {
            //        $scope.isLoading = false;
            //        $scope.$emit(MainEvent.MAIN_PRELOADER_SHOW_HIDE, false);
            //    });
            //});
            var loader = new THREE.JSONLoader();
            loader.load(modelFile + '/model.json', function(geometry, materials) {
                var mesh = new THREE.Mesh(geometry, new THREE.MeshStandardMaterial(materials));
                mesh.scale.set(modelScale, modelScale, modelScale);
                mesh.position.x = 0.3;
                mesh.roughness = 1.0;
                mesh.metalness = 0;
                //mesh.color = 0xffffff;
                mesh.position.y = -4;
                group.add(mesh);
                object = mesh;
                //group.rotation.z = Math.PI * 12/180;
                //if(textureImg) onFileSelect(textureImg);
                //mesh = new THREE.Mesh(geometry, material);
                var childFound = false;
                mesh.traverse(function(child) {
                    if (child instanceof THREE.Mesh) {
                        child.material = material;
                        //if (childFound) child.visible = false;
                        //child.material.map.needsUpdate = false;
                        childFound = true;
                    }
                });
                /*camera.position.x = Math.cos( 180*Math.PI/180 ) * 1;
                camera.position.y = 2*Math.cos( 90*Math.PI/180 );
                camera.lookAt( new THREE.Vector3( 0, 0, 0 ) );*/
                //camera.position.x = Math.cos(180 * Math.PI / 180) * 1;
                //camera.position.y = 0.55;
                //camera.lookAt(new THREE.Vector3(0.5, 0.25, 0));
                //renderer.render(scene, camera);
                $timeout(function() {
                    //content.addEventListener('mousedown', onDocumentMouseDown, false);
                    $scope.isLoading = false;
                    $scope.show3dPreview = true;
                    $scope.$emit(MainEvent.MAIN_PRELOADER_SHOW_HIDE, false);
                });
            });
        }

        function webglAvailable() {
            try {
                var canvas = document.createElement("canvas");
                return !!window.WebGLRenderingContext && (canvas.getContext("webgl") || canvas.getContext("experimental-webgl"));
            } catch (e) {
                return false;
            }
        }
        $scope.changeObjectColor = function(value) {
            g.fillStyle = value.color1;
            g.fillRect(0, 0, bitmap.width, bitmap.height);
            g.fillStyle = value.color2;
            g.fillRect(450, 400, 530, 350);
            g.fillRect(1024 - 719, 1024 - 306, 719, 306);
            if (img) g.drawImage(img, 100, 30, 874, 358);
            update();
        }
        onSliderAngleChange = function(value) {
            $scope.playPause(true);
            rotateAngle = direction * value;
            rotateObject(direction * value);
        }
        onsliderhover = function(value) {
            $scope.playPause(isPaused);
        }
        $scope.playPause = function(value) {
            isPaused = !isPaused;
            if (value) isPaused = value;
            $('#playPauseBtn').html(isPaused ? 'Play' : 'Pause');
            animate();
        }

        function animate() {
            if (isPaused) return;
            else requestAnimationFrame(animate);
            if (object) {
                rotateObject(0);
                rotateAngle += 1;
                if (rotateAngle > 360 / rotateSpeed) rotateAngle = 0;
                //object.rotation.z = Math.PI  *timer * 10/180;
            }
        }
        //var axis = new THREE.Vector3(0,0.5,0.1);
        function rotateObject(angle) {
            //tilted a bit on x and y - feel free to plug your different axis here
            //in your update/draw function
            //object.rotateOnAxis(axis, direction * Math.PI *rotateSpeed/180);
            object.rotation.y = (direction * Math.PI * angle * rotateSpeed / 180);
            $('#angleSlider').val(direction * angle);
            renderer.render(scene, camera);
        }
        $scope.captureGif = function() {
            $scope.captureClicked = false;
            $scope.playPause(true);
            rotateAngle = 0;
            renderer.setSize(WIDTH / 2, HEIGHT / 2);
            $scope.isPreloading = true;
            $scope.showGifPopup = true;
            gif = new GIF({
                workers: 5,
                quality: 10,
                workerScript: '/xetool/gif.worker.js'
            });
            gif.on('finished', function(blob) {
                imageblob = blob;
                $('#gifImg')[0].src = URL.createObjectURL(blob);
                $timeout(function() {
                    $scope.isPreloading = false;
                });
            });
            /*gif.on('progress', function(value){
                console.log(value*100);
            });*/
            generateGIF();
        }
        $scope.captureJpg = function() {
            renderer.render(scene, camera);
            download(renderer.domElement.toDataURL('image/jpeg'), 'download.jpg', 'image/jpeg');
            $scope.captureClicked = false;
            //fbShare();
        }
        $scope.downloadGifFile = function() {
            download(imageblob, 'download.gif', 'image/gif');
        }
        $scope.shareGif = function() {
            $scope.isShareClicked = true;
            $scope.is3dDesignshare = false;
            $scope.loadingMessage = '';
            $scope.isLoading = true;
            //$('#gifContainer').css('pointer-events', 'none');
            var reader = new window.FileReader();
            reader.readAsDataURL(imageblob);
            reader.onloadend = function() {
                base64data = reader.result;
                console.log(base64data);
                var data = new FormData();
                //data.append('fname', 'gif_to_upload.gif');
                data.append('imgData', base64data);
                $.ajax({
                    type: 'POST',
                    processData: false, // important
                    contentType: false, // important
                    data: data,
                    //url: 'http://3dmockuper.com/gifupload.php',
                    url: '/xetool/api/index.php?reqmethod=uploadGIFFile',
                    success: function(path) {
                        $timeout(function() {
                            $scope.isLoading = false;
                            $scope.imagepath = path;
                            addthis3D($scope.imagepath);
                        }, 100);
                        //fbShare(imgPath);
                        //$('#gifContainer').css('pointer-events', 'all');
                        //imgPath = path;
                        //console.log(imgPath);
                        //window.open('https://www.facebook.com/sharer/sharer.php?u='+imgPath, 'Share Facebook', config='height=384, width=512');
                    }
                });
            }
        }
        addthis3D = function(gif3Dlink) {
            $scope.linkFor3dGIF = gif3Dlink;
            $scope.is3dDesignshare = true;
        }
        generateGIF = function() {
            imgPath = null;
            if (gif) {
                if (captureCount >= 60 / rotateSpeed) {
                    gif.render();
                    $scope.stopGif();
                } else {
                    rotateObject(rotateAngle);
                    renderer.render(scene, camera);
                    $('#gifImg')[0].src = renderer.domElement.toDataURL();
                    gif.addFrame(renderer.domElement, {
                        delay: 1,
                        copy: true
                    });
                    rotateAngle += 6;
                    var percentage = Math.round(100 * captureCount / (60 / rotateSpeed));
                    $('#preloader').css('width', percentage + '%');
                    $('#preloader').html(percentage + '%');
                    //gif.render();
                    captureCount++;
                    setTimeout(function() {
                        generateGIF();
                    }, 10);
                }
            }
        }
        $scope.stopGif = function() {
            $('#preloader').css('width', '100%');
            $('#preloader').html('Finalizing..');
            $scope.playPause(true);
            captureCount = 0;
            renderer.setSize(WIDTH, HEIGHT);
            renderer.render(scene, camera);
        }
        $scope.captureClick = function() {
            $scope.captureClicked = !$scope.captureClicked;
        }
        $scope.closePopup = function() {
            $('#gifImg')[0].src = '';
            $scope.showGifPopup = false;
        }
        /*onrotateZChange = function(value) {
            group.rotation.z = value * Math.PI / 180;
            renderer.render(scene, camera);
        }*/
        $rootScope.$on(MainEvent.CLOSE_3D_PREVIEW, function() {
            renderer.dispose();
            renderer.forceContextLoss();
            renderer.context = undefined;
            renderer.domElement = undefined;
            $scope.closepreview3dmodal();
        });
    }]);
});
require(['preview3D/controllers/controller'])