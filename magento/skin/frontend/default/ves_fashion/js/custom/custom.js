$j(document).ready(function() {
    /**
     * Fix Navbar on top of screen when scrolling past a certain point for mobile view
     */

    $j(window).scroll(function() {
        if ($j(window).scrollTop() > 225) {
            $j('#ves-mainnav').addClass('header-fixed-mobile');
            $j('.hf-brand').addClass('hf-brand-fixed-mobile');
        } else {
            $j('#ves-mainnav').removeClass('header-fixed-mobile');
            $j('.hf-brand').removeClass('hf-brand-fixed-mobile');
        }
    });

    /**
     * Fix Navbar on top of screen when scrolling past a certain point for desktop view
     */

    $j(window).scroll(function() {
        if ($j(window).scrollTop() > 149) {
            $j('#ves-mainnav').addClass('header-fixed');
            $j('.hf-brand').addClass('hf-brand-fixed');
        } else {
            $j('#ves-mainnav').removeClass('header-fixed');
            $j('.hf-brand').removeClass('hf-brand-fixed');
        }
    });

    /**
     *    Insert Text in front of topics
     *    @type {[type]}
     */

    $j('<span>Themen: </span>').insertBefore('.amfaq-page .topics');

    /**
     *    Add class 'button' to question link
     */

    $j('.amfaq-page .ask').addClass('button');

    /**
     *    Insert Image before each Topic
     */

    let topics = [];

    $j('.column .amfaq-questions-group h2 a').each(function(index, value) {
        topics[index] = $j(this)
            .text()
            .replace(/\W+/g, '-');
    });

    $j('.column .amfaq-questions-group').each(function(index, value) {
        $j(this).before(
            `<img id="${
                topics[index]
            }" class="img img-responsive" src="media/amfaq/${
                topics[index]
            }.jpg" alt="${topics[index]}" />`
        );
    });

    /**
     *    Replace img with default img, if no image could be loaded
     *    @return {[type]} [description]
     */

    $j('.column img').error(function() {
        $j(this).attr('src', 'media/amfaq/Image-not-Found.jpg');
    });

    /**
     *    Create ID string
     *    @type {Array}
     */

    let idTopics = [];
    const topicsId = '#';

    $j('.column img').each(function(index, value) {
        idTopics[index] = topicsId.concat($j(this).attr('id'));
    });

    /**
     *    Wrap image and topic in same link as topic name
     *    @param {[type]} index position in array
     *    @param {[type]} value href of image
     *    @return {[type]} [description]
     */

    let href = [];

    $j('.amfaq-questions-group h2 a').each(function(index, value) {
        href[index] = $j(this).attr('href');
    });

    $j.each(idTopics, function(index, value) {
        $j(value)
            .next('div')
            .addBack()
            .wrapAll(
                `<a class="col-lg-4 col-md-4 col-sm-6 col-xs-12 amfaq-card" href="${
                    href[index]
                }"/>`
            );
    });

    /*
    Display message that customer is untracked
     */

    $j('a[href^="javascript:gaOptout"]').click(function() {
        $j(this).replaceWith(
            "<p class='google-analytics-untracked'>Die Erfassung durch Google Analytics auf dieser Website wird zukünftig verhindert.</p>"
        );
    });

    var $checkbox_html =
        '<input type="checkbox" name="privacy_policy" class="checkbox required-entry">';
    var $checkbox_label =
        '<label style="margin-top: -14px; margin-left: 20px;" class="required" for="privacy_policy">Ja, ich habe die <a target="_blank" href="/datenschutz">Datenschutzerklärung</a> zur Kenntnis genommen und bin damit einverstanden, dass die von mir angegeben Daten elektronisch erhoben und gespeichert werden. Meine Daten werden ausschließlich zu diesem Zweck genutzt</label>';

    $j('.customer-account-create .form-list #remember-me-box')
        .parent($(this))
        .append(
            "<div><div class='input-box'>" +
                $checkbox_html +
                '</div>' +
                $checkbox_label +
                '</div>'
        );

    /*  Insert message below button     */

    $j(
        '<p class="amfaq-question-note">Bitte stelle uns Fragen zu konkreten Bestellungen per E-Mail.</p>'
    ).insertAfter('.amfaq-page .amfaq-buttons .ask.button');

    /*  Add new attribute to a slider link */

    $j('a[href="https://www.facebook.com/groups/251921962228567/"]').attr('target', '_blank');

    /** Hide overflow on mailchimp popup */

    setTimeout(function() {
        $j('.mc-layout__modalContent iframe').contents().find('body').css('overflow','hidden');
    }, 1000);

});

/* Light YouTube Embeds by @labnol */
/* Web: http://labnol.org/?p=27941 */
let div,
    n,
    v = document.getElementsByClassName('youtube-player');

document.addEventListener('DOMContentLoaded', function() {
    for (n = 0; n < v.length; n++) {
        div = document.createElement('div');
        div.setAttribute('data-id', v[n].dataset.id);
        div.innerHTML = labnolThumb(v[n].dataset.id);
        div.onclick = labnolIframe;
        v[n].appendChild(div);
    }
});

/**
 *    Replace Youtube videos with Thumbnails. Autoplay on click.
 *    @type {String}
 */

let thumb = '<img src="https://i.ytimg.com/vi/ID/hqdefault.jpg">',
    play = '<div class="play"></div>';

function labnolThumb(id) {
    return thumb.replace('ID', id) + play;
}

let iframe = document.createElement('iframe');
let embed = 'https://www.youtube.com/embed/ID?autoplay=1';

function labnolIframe() {
    iframe.setAttribute('src', embed.replace('ID', this.dataset.id));
    iframe.setAttribute('frameborder', '0');
    iframe.setAttribute('allowfullscreen', '1');
    this.parentNode.replaceChild(iframe, this);
}

/**
 *    Replace small load image with actual image after window load.
 *    @return {[type]} [description]
 */

function init() {
    let imgDefer = document.getElementsByTagName('img');
    for (let i = 0; i < imgDefer.length; i++) {
        if (imgDefer[i].getAttribute('data-src')) {
            imgDefer[i].setAttribute(
                'src',
                imgDefer[i].getAttribute('data-src')
            );
        }
    }
}
window.onload = init;
