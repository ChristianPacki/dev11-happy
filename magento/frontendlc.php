<?php
require_once "app/Mage.php";
class Login
{
    private $email;
    private $password;
    public $sessionId = 0;
    public $data;
    public function setSignin($email, $password)
    {
        $this->email = $email;
        $this->password = $password;
    }
    public function setSignup($fName, $lName)
    {
        $this->firstName = $fName;
        $this->lastName = $lName;
    }
    //Return magento customer session
    public static function magentoSession()
    {
        Mage::app();
        umask(0);
        Mage::app()->loadArea('frontend');
        Mage::getSingleton('core/session', array('name' => 'frontend'));
        return Mage::getSingleton('customer/session', array('name' => 'frontend'));
    }
    //user logout
    public function userLogout()
    {
        $session = self::magentoSession();
        $session->logout();
        $this->sessionId = 0;
    }
    //authenticate user login
    public function userLogin()
    {
        if (!empty($this->email) && !empty($this->password)) {
            $session = self::magentoSession();
            if (!$session->isLoggedIn) {
                try {
                    $session->login($this->email, $this->password);
                    if ($session->getId()) {
                        $sessionId = $session->getId();
                        $session->setCustomerAsLoggedIn($session->getCustomer());
                        $this->data = array('status' => '0', 'customerId' => $sessionId);
                    } else {
                        $this->data = array('status' => 'NA', 'customerId' => '0');
                    }
                    return $this->data;
                } catch (Mage_Core_Exception $e) {
                    return $this->data = array('status' => 'NA', 'customerId' => '0');
                }
            }
        } else {
            $session->addError('Login and password are required.');
        }
    }
    //user sign up
    public function userSignUp()
    {
        $current_store_id = Mage::app()->getStore()->getId();
        $websiteId = Mage::app()->getStore($current_store_id)->getWebsiteId();
        $customer = Mage::getModel('customer/customer');
        $customer->setWebsiteId($websiteId);
        $customer->loadByEmail($this->email);

        if (!$customer->getId()) {
            $session = self::magentoSession();
            $customer->setEmail($this->email);
            $customer->setFirstname($this->firstName);
            $customer->setLastname($this->lastName);
            $customer->setPassword($this->password);
            $customer->save();
            $customer->setConfirmation(null);
            $customer->save();
            $session->login($this->email, $this->password);
            $session->setCustomerAsLoggedIn($session->getCustomer());
            $sessionId = $session->getId();
            $this->data = array('status' => '0', 'customerId' => $sessionId);
        } else {
            $this->data = array('status' => 'AE', 'customerId' => '0');
        }
        return $this->data;
    }
}
$login = new Login();
if (isset($_POST['email']) && isset($_POST['password'])) {
    $login->setSignin($_POST['email'], $_POST['password']);
    if (isset($_POST['firstName']) && isset($_POST['lastName'])) {
        $login->setSignup($_POST['firstName'], $_POST['lastName']);
        $data = $login->userSignUp();
    } else {
        $data = $login->userLogin();
    }
} else {
    $session = Login::magentoSession();
    $login->sessionId = $session->getId();
    $sessionId = ($login->sessionId) ? $login->sessionId : 0;
    $data = array('status' => '0', 'customerId' => $sessionId);
}
header('Content-Type: application/json');
echo json_encode($data);
