	jQuery(document).ready(function() {
		if(typeof BASE_URL == 'undefined'){
			return;
		}
	    //jQuery("#shopping-cart-table tbody tr td:first-child div").hide(10000);
	    jQuery("#shopping-cart-table tbody tr td dt").each(function(index) {
	        var labeltxt = jQuery(this).html();
	        jQuery(this).next('dd').addClass("variation-"+labeltxt);
	        if (labeltxt.toLowerCase().indexOf("XE_Size")) {
	            var replaced = jQuery(this).html().replace('XE_Size', 'Size');
	            jQuery(this).html(replaced);
	        }
	        if (labeltxt.toLowerCase().indexOf("XE_Color")) {
	            var replaced = jQuery(this).html().replace('XE_Color', 'Color');
	            jQuery(this).html(replaced);
	        }
	    });
	    var ul;
	    /* var pathArray = location.href.split( '/' ),
	    protocol = pathArray[0],
	    host = pathArray[2],
	    host2nd = pathArray[3];
	    ul = protocol + '//' + host +'/'; */
	    ul = BASE_URL;
	    var progressSvg = ul + 'xetool/assets/images/progress.svg';
	    jQuery(".loading").html('<img src="' + progressSvg + '" alt="Loading" style="text-align: center; vertical-align: middle;" /> ');
	    var url = ul + 'xetool/api/index.php';
	    var refids = "";
	    jQuery("#shopping-cart-table tbody tr td:first-child div").each(function(index) {
	        //console.log('index=> '+index);                 
	        var refid = jQuery(this).find("img").attr('rel');
	        if (refid != undefined) {
	            if (refids == "") refids = refid;
	            else refids += "," + refid;
	        }
	    });
	    refids = refids.replace(/^0,+/, '');
	    if (refids[0] > 0) {
	    var getPreviewUrl = url + '?reqmethod=getCustomPreviewImages&refids=' + refids;
	    jQuery.get(getPreviewUrl, function(data, status) {
	        if (status == 'success') {
	            var jsonObj = data;
	            var size = new Array();		
		    var elem1 = jQuery("#shopping-cart-table tbody tr td dd.variation-XE_Size");
		    var i = 0;
		    elem1.each(function(ind) {							
		        size[i] = jQuery(this).text();
		        i++;						
		    });
	            jQuery("#shopping-cart-table tbody tr td:first-child div").each(function(index) {
	                jQuery(this).hide();
	                var sid = jQuery(this).find("img").attr('rel');
	                //jQuery(this).find("img").hide();
	                var obj = jQuery(this);
	                if (sid != undefined) {
	                    jQuery.each(jsonObj, function(key, val) {
	                        if (key == sid && val.length > 0) {
	                            var count = 0;
	                            var print_id = val[0].printid;
	                            var sizeId = size[index];
	                            if (val[0].display_edit == 0) {
	                                jQuery("a.edit" + sid).css('display', 'none');
	                            }
	                            if (val[0].nameAndNumber == 0){
									jQuery("a.info" + sid).css('display', 'none');
								}
	                            jQuery(obj).find('.printid').val(print_id)
	                            jQuery(obj).find('.sizeid').val(sizeId)
	                            jQuery.each(val, function(imgKey, imgObj) {
	                                count++;
	                                if (count == 1) {
	                                    jQuery(obj).find("a").remove();
	                                }
	                                /* var newElement = '<a  class="product-image product-thumb customize-image" title="" href="javascript:void(0)">'+'<object  class="previewimg" style="display:block; border: 1px solid #000; padding:2px; -webkit-transform: scale3d(0.15, 0.15, 0.15); -moz-transform: scale3d(0.15, 0.15, 0.15); -o-transform: scale3d(0.15, 0.15, 0.15); -s-transform: scale3d(0.15, 0.15, 0.15); -moz-transform-origin: 0 0;-o-transform-origin: 0 0;-webkit-transform-origin: 0 0;transform-origin: 0 0;" rel="'+sid+'" alt="" data="'+imgObj['svg']+'" type="image/svg+xml"></object>'+'</a>'; */
	                                var newElement = '<a href="javascript:void(0)" title="" class="product-image product-thumb customize-image" style="float:left;width:80px"><img src="' + imgObj['customImageUrl'] + '" width="75" height="75" alt="" rel="' + sid + '" style="display:block;" class="previewimg"></a>';
	                                jQuery(obj).append(newElement);
	                            });
	                        }
	                    });
	                }
	                jQuery(this).parent().find('.loading').hide();
	                jQuery(this).show();
	            });
	        }
	    });
		}
	});