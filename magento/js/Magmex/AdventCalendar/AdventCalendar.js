/**
 * Magmex JavaScript class for Advent Calendar
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.magmex.com/MAGMEX_SOFTWARE_LICENSE.txt
 *
 * @copyright Copyright (c) 2015 magmex, Michael Stork, (http://www.magmex.com)
 * @license http://www.magmex.com/MAGMEX_SOFTWARE_LICENSE.txt
 * @category Magmex
 * @package Magmex_AdventCalendar
 * @author Michael Stork <m.stork@magmex.com>
 */
//We define a magmex namespace for our jQuery version to avoid conflicts with other already exisiting/loaded jQuery versions
var jQueryMagmex = jQuery.noConflict();

//Define Magmex namespace
if (typeof(Magmex) === 'undefined') {
    var Magmex = {};
}

(function ($)
{
    $(document).ready(function()
    {
        Magmex.AdventCalendar.init();
    });

    Magmex.AdventCalendar = {
        /**
         * The resize factor for resizing responsive design elements
         * Initial value = 1 -> nothing gets resized
         *
         * @var integer
         */
        _resizeFactor: 1,

        /**
         * Defines all CSS properties, that should be resized
         *
         * @var Array
         */
        _resizeElementArray: new Array(
            'width',
            'height',
            'top',
            'bottom',
            'left',
            'right',
            'margin-top',
            'margin-bottom',
            'margin-left',
            'margin-right',
            'padding-top',
            'padding-bottom',
            'padding-left',
            'padding-right',
            'border-top-width',
            'border-bottom-width',
            'border-left-width',
            'border-right-width',
            'font-size',
            'background-position'
        ),

        /**
         * Initializes the Advent Calendar with responsive design
         *
         * @return void
         */
        init: function()
        {
            this._initResponsiveDesign();
            this._initEventListeners();
        },

        /**
         * Initializes all necessary responsive design features
         *
         * @return void
         */
        _initResponsiveDesign: function()
        {
            var _self = this;
            var adventCalendarElement = $('#magmexAdventCalendar');
            var adventCalendarParentElement = adventCalendarElement.parent();
            var adventCalendarParentElementWidth = adventCalendarParentElement.width();
            //adventCalendarParentElementWidth = 1110;
            var adventCalendarElementWidth = adventCalendarElement.width()

            //if the parent div is wider than the main advent calendar div, then no resizing is necessary
            if (adventCalendarParentElementWidth >= adventCalendarElementWidth) {
                this._setBackgroundImageUrls();
                adventCalendarElement.show();
                return;
            }

            this._resizeFactor = adventCalendarParentElementWidth / adventCalendarElementWidth;

            this._resizeAllAvailableSizes(adventCalendarElement);

            $('#magmexAdventCalendar *').each(function() {
                _self._resizeAllAvailableSizes($(this));
            });

            this._setBackgroundImageUrls();

            adventCalendarElement.show();
        },

        /**
         * Resizes the width and the height of a given dom element
         *
         * @param Object object DOM object
         * @return void
         */
        _resizeAllAvailableSizes: function(object)
        {
            if (this._resizeFactor === 1) {
                return;
            }

            for (var counter = 0; counter < this._resizeElementArray.length; counter++) {
                this._resizeDomElement(object, this._resizeElementArray[counter], this._resizeFactor);
            }
        },

        /**
         * Resizes the given DOM element
         *
         * @param Object object the dom element to resize
         * @param String cssProperty the css property to resize
         * @param Integer resizeFactor the factor to resize the element
         * @return void
         */
        _resizeDomElement: function(object, cssProperty, resizeFactor)
        {
            var styleValue = object.css(cssProperty);

            //Sometimes in Internet Explorer the styleValue is undefined - so return and do nothing
            if (typeof(styleValue) === 'undefined') {
                return;
            }

            var oldSize = styleValue.substring(0, styleValue.indexOf('px'));

            //only set new size, if oldValue is bigger than 0 or smaller than 0 (e.g. background-position) - otherwise every size css
            //element would be set with the size 0 - even if this css property didn't exist befor - and
            //would cause wrong css properties set to 0
            if (oldSize > 0 || oldSize < 0) {
                var newSize = resizeFactor * oldSize;
                object.css(cssProperty, newSize);
            }
        },

        /**
         * Sets the background images for all elements that should have backgrounds
         *
         * @return void
         */
        _setBackgroundImageUrls: function()
        {
            var _self = this;
            var mainBackgroundImageWidth = parseInt(this._resizeFactor * this.config.mainBackgroundImage.width);
            var mainBackgroundImageUrl = this.config.mainBackgroundImage.url + mainBackgroundImageWidth;
            var urlString = 'url(\'' + mainBackgroundImageUrl + '\')';
            $('#magmexAdventCalendar').css('background-image', 'url(\'' + mainBackgroundImageUrl + '\')');

            if (this.config.showDoorBackgroundImages == '1') {
                //we have to calculate the new background position and the background image size for every door in case
                //of the randomSizeDoors layout - the biggest door has 135 x 135 px - so this door size is the base for
                //the additional crop factor
                if (this.config.layoutType === 'randomSizeDoors') {
                    var biggestDoorSize = this._resizeFactor * 135;
                    $('#magmexAdventCalendar #adventCalendarDoorContainer *.adventCalendarDoor').each(function() {
                        var localResizeFactorDependantOnDoorSize = $(this).width() / biggestDoorSize;
                        _self._resizeDomElement($(this), 'background-position', localResizeFactorDependantOnDoorSize);
                        var doorRandomBackgroundImageSpriteWidth = parseInt(localResizeFactorDependantOnDoorSize * _self._resizeFactor * _self.config.doorBackgroundImageSprite.width);
                        var doorRandomBackgroundImageSpriteUrl = _self.config.doorBackgroundImageSprite.url + doorRandomBackgroundImageSpriteWidth;
                        $(this).css('background-image', 'url(\'' + doorRandomBackgroundImageSpriteUrl + '\')');
                    });
                } else {
                    var doorBackgroundImageSpriteWidth = parseInt(this._resizeFactor * this.config.doorBackgroundImageSprite.width);
                    var doorBackgroundImageSpriteUrl = this.config.doorBackgroundImageSprite.url + doorBackgroundImageSpriteWidth;
                    $('#magmexAdventCalendar #adventCalendarDoorContainer *.adventCalendarDoor').each(function() {
                        $(this).css('background-image', 'url(\'' + doorBackgroundImageSpriteUrl + '\')');
                    });
                }
            }
        },

        /**
         * Initializes all event listeners of the Advent Calendar
         *
         * @return void
         */
        _initEventListeners: function()
        {
            this._initAdventCalendarDoorListener();
        },

        /**
         * Initializes the advent calendar door event listeners for opening the day actions
         *
         * @return void
         */
        _initAdventCalendarDoorListener: function()
        {
            var _self = this;
            $('#magmexAdventCalendar #adventCalendarDoorContainer .adventCalendarDoor.doorActive').click(function() {
                $.blockUI({
                    message: _self.config.dayActionContainerHtml,
                    css: {
                        padding: 0,
                        margin: 0,
                        //the width has also to be added in the center hack below
                        width: '837px',
                        top: '15%',
                        left: '23%',
                        textAlign: 'left',
                        border: 'none',
                        cursor: 'auto'
                    },
                    overlayCSS: {
                        cursor: 'auto'
                    },
                    baseZ: 100000
                });

                //blockUI layer gets fixed position and does not scroll with the screen
                var blockUiContainer = $(".blockUI.blockMsg.blockPage");
                var offsetBlockUiContainer = blockUiContainer.offset();
                var topValue = parseInt(offsetBlockUiContainer.top);
                blockUiContainer.css('position', 'absolute');
                blockUiContainer.css('top', topValue);

                $('.blockUI.blockMsg').css("left", ($(window).width() - 837) / 2 + $(window).scrollLeft() + "px");

                var dayIdAndNumber = $(this).attr('rel');
                var dayNumber = dayIdAndNumber.substring(dayIdAndNumber.indexOf('_') + 1, dayIdAndNumber.length);
                var dayId = dayIdAndNumber.substring(0, dayIdAndNumber.indexOf('_'));

                $("#magmexAdventCalendarDayActionLayer #dayNumberContainer #dayNumber").append(dayNumber);

                $.ajax({
                    type: "GET",
                    dataType: "json",
                    url: _self.config.dayActionAjaxUrl + dayId,
                    success: function(resultJson) {
                        $("#magmexAdventCalendarDayActionLayer #contentContainer .loadingImage").replaceWith(resultJson['html']);
                    }
                });
            });

            //close button live event handler
            $(document).on('click', '#magmexAdventCalendarDayActionLayer #contentContainer .closeButton, .blockUI.blockOverlay', function() {
                $.unblockUI();
            });
        }
    };
})(jQueryMagmex);

if (typeof jQueryOriginal != 'undefined' && jQueryOriginal !== false) {
    jQuery = jQueryOriginal;
}