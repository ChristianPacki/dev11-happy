/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (http://www.amasty.com)
 */

document.observe("dom:loaded", function () {

    // expand
    $$('.amfaq-questions.amcollapse').each(function (container) {
        container.on(
            'click',
            '.title',
            function (event) {
                event.preventDefault();
                var title = event.findElement('.title');
                var element = title.next();

                title.toggleClassName('expanded');

                if (element.visible())
                    Effect.SlideUp(element, {duration: 0.5});
                else
                    Effect.SlideDown(element, {duration: 0.5});
            }
        );
    });

    // ask form
    $$('.amfaq-button.ask').each(function (element) {
        element.observe('click', function (event) {
            event.preventDefault();

            this.toggleClassName('expanded');
            var wrap = $('amfaq-ask-form-wrap');


            if (wrap.visible())
                Effect.SlideUp(wrap, {duration: 0.5});
            else
                Effect.SlideDown(wrap, {duration: 0.5});
        });
    });


    // product view
    $$('.amfaq-ask-link').each(function (element) {
        element.observe('click', function (event) {
            if ($('product-faq'))
                scrollTo(0, $('product-faq').offsetTop);
            amfaqAskForm.show();
            event.stop();
        });
    });

    // popup
    if ('amfaqAsk' in window) {
        var amfaqAskForm = new amfaqAsk();
    }

    $$('.amfaq-ratings').each(function (element) {
        var rate = element.readAttribute('data-rate');

        var myrate = element.readAttribute('data-myrate');
        var questionId = element.readAttribute('data-id');

        var canVote = element.readAttribute('data-canvote');
        var url = element.readAttribute('data-url');

        var ratingType = element.readAttribute('data-type');

        var args = {
        // multiple:               true,
            value: rate,
            rated: myrate,
            updateUrl: url,
            updateParameterName: 'mark',
            canVote: canVote,
            ratingType: ratingType,
            updateOptions: {
                method: 'post',
                onSuccess: function (transport) {
                    var resp = transport.responseText;
                    var result = resp.split(' ');
                    var rating = result[0];

                    control.setValue(rating);

                    var container = element.up();
                    container.down('.info').show();
                    container.down('.total').update(rating);
                    if (typeof result[1] !== 'undefined') {
                        var all = result[1];
                        container.down('.all').update(all);
                    }
                    container.down('.my').show();
                    container.down('.my .rate').update(control.rated);
                },
                onFailure: function () {
                    alert('Something went wrong...');
                },
                parameters: {question: questionId/*, product: productId*/}
            }
        };

        var control = new AmRating(element, args);
        /*        if (!canVote)
         control.disable();*/
    });

    $$('.amfaq-social a').each(function (element) {
        element.observe('click', function (e) {
            e.preventDefault();

            var w = 800;
            var h = 300;

            var docWidth = (window.innerWidth - w) / 2;
            var docHeight = (window.innerHeight - h) / 2;

            var windowURL = this.readAttribute('href');

            window.open(windowURL, "share", "toolbar=0,location=0,directories=0,status=1,menubar=0,scrollbars=yes,resizable=1,width=" + w + ",height=" + h + ",left=" + docWidth + ",top=" + docHeight);
        });
    });
});
