/**
 * @author Amasty Team
 * @copyright Copyright (c) 2010-2013 Amasty (http://www.amasty.com)
 */

document.observe("dom:loaded", function() {
    var button = $$('.amfaq-search-results .show-more')[0];
    if (button)
    {
        button.observe('click', function(e){
            e.preventDefault();

            if (!this.searchPage)
                this.searchPage = 1;

            button.addClassName('preloader');

            new Ajax.Request(window.location.href,
                {
                    parameters: {
                        page : ++this.searchPage
                    },
                    onSuccess: function(response) {
                        var result = response.responseText.evalJSON();

                        if (result.count > 0)
                        {
                            var container = $$('.amfaq-search-results .amfaq-questions')[0]
                            container.insert(result.questionsHtml);
                        }

                        if (result.stop)
                        {
                            button.hide();
                        }
                        else
                            button.removeClassName('preloader');
                    }
                });
        });
    }
});
