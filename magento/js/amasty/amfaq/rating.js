/**
 * @author Amasty Team
 * @copyright Copyright (c) 2010-2013 Amasty (http://www.amasty.com)
 */


AmRating = Class.create({
    initialize: function(container,options){
        this.value = 0;
        this.rated = 0;
        this.container = $(container);
        this.maxWidth = this.container.down('.rating-box').getStyle('width').match(/\d+/)[0]; // hack
        this.rating = this.container.down('.rating');
        this.canVote = true;
        this.ratingType = '0';
        this.options = {
            rated: 0,
            updateUrl: false,
            updateParameterName: 'value',
            updateOptions : {},
        };
        Object.extend(this.options,options || {});
        if (this.options.value) {
            this.value = this.options.value;
            delete this.options.value;
        }
        if (this.options.rated) {
            this.rated = this.options.rated;
            delete this.options.rated;
        }
        if (this.options.ratingType) {
            this.ratingType = this.options.ratingType;
            delete this.options.ratingType;
        }
        if(this.options.canVote)
            this.bindEvents();
        this.render();
    },
    bindEvents: function() {
        switch (this.ratingType) {
            case '0': //stars
                var rating = this.container.down('.rating-box');
                rating.observe('mousemove', this.mouseMove.bind(this));
                rating.observe('mouseover', this.mouseMove.bind(this));
                rating.observe('mouseout', this.mouseOut.bind(this));
                rating.observe('click', this.click.bind(this));
                break;
            case '1': //yesno
                var ratingYes = this.container.down('.useful-yes');
                var ratingNo = this.container.down('.useful-no');
                ratingYes.observe('click', this.click.bind(this));
                ratingNo.observe('click', this.click.bind(this));
                break;
        }
    },
    setValue: function(value){
        this.value = value;
        this.render();
    },
    rate: function(star)
    {
        this.rated = star;
        if(this.options.updateUrl){
            var params = {}, a;
            params[this.options.updateParameterName] = star;

            a = new Ajax.Request(this.options.updateUrl, Object.extend(
                this.options.updateOptions, { parameters : Object.extend(this.options.updateOptions.parameters, params) }
            ));
        }
        this.render();
    },
    render: function(star) {
        if (this.ratingType) {
            var star = star || this.rated || 0;
        } else {
            var star = star || this.rated || this.value || 0;
        }
        switch (this.ratingType) {
            case '0': //stars
                this.rating.setStyle({'width' : this.maxWidth / 5 * star + 'px'});
                break;
            case '1': //yesno
                var ratingYes = this.container.down('.useful-yes');
                var ratingNo = this.container.down('.useful-no');
                if ('2' == star) {
                    ratingYes.className += ' disabled';
                    ratingNo.classList.remove("disabled");
                } else if ('1' == star) {
                    ratingYes.classList.remove("disabled");
                    ratingNo.className += ' disabled';
                } else {
                    ratingYes.classList.remove("disabled");
                    ratingNo.classList.remove("disabled");
                }
                break;
        }
    },
    mouseMove: function(event){
        var star = this.getStarIndex(event);
        this.render(star);
    },
    mouseOut: function(){
        this.render();
    },
    click: function(event) {
        switch (this.ratingType) {
            case '0': //stars
                var star = this.getStarIndex(event);
                break;
            case '1': //yesno
                var star = event.currentTarget.readAttribute('data-value');
                break;
        }
        this.rate(star);
    },
    getStarIndex: function (event) {
        var x = event.clientX  - event.target.getBoundingClientRect().left;
        return Math.floor(x / this.maxWidth * 5) + 1;
    }
});