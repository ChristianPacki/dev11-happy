

OptionBundle.Main = Class.create(OptionBundle.Images, {

	initialize : function($super){

		Object.extend(this, OptionBundle.Data);	
		Object.extend(this, OptionBundle.Config);
						
    this.oldV = {b:[],o:[]};
    this.oldO = {b:[],o:[]};
    this.indByValue = {b:[],o:[]}; 
    this.dependecyIsSet = false;

    this.reorderElements();
    this.setRequired();   
    this.setVariables(); 
    
		$super();
		     		
		this.hideOptions();												
		this.selectDefault();
	},
 
  
	reorderElements : function(){
    var t,id,oT,e,dd,dt,pdd;
    	
    var l = this.sortedOIds.length;
    for (var i=0;i<l;i++){   
      t = this.sortedOIds[i]['type'];          
      id   = this.sortedOIds[i]['id'];
      fvId = this.sortedOIds[i]['fvId'];
      oT = this.sortedOIds[i]['optionType'];
                 
      e = this.getElementByOptionType(t, id, fvId, oT); 
      
      dd = this.getElementDdTag(e);
      dt = this.getElementDtTag(e);      
      
      if (pdd){
        Element.insert(pdd, {'after':dt});
        Element.insert(dt, {'after':dd});
      }
      
      dd.className = i < l - 1 ? '' : 'last';            
      pdd = dd;           
    }	
  },
  

	setRequired : function(){
    var t,id,oT,e,dd,dt;
    	
    var l = this.sortedOIds.length;
    for (var i=0;i<l;i++){   
      t = this.sortedOIds[i]['type'];          
      id   = this.sortedOIds[i]['id'];
      fvId = this.sortedOIds[i]['fvId'];
      oT = this.sortedOIds[i]['optionType'];
                 
      e = this.getElementByOptionType(t, id, fvId, oT);
      
      dd = this.getElementDdTag(e);
      dt = this.getElementDtTag(e);	

      if (t == 'b'){      
        if (this.isSingle[id] && this.isRequired[t][id])
            this.makeBoptionRequired(e, id, dd, dt, oT);                  
      } else {      
        if (!this.isRequired[t][id] && this.isEditOrderPage && (oT == 'date' || oT ==  'date_time' || oT == 'time')){ 
          this.addDateCompleteValidation(id);      
        }
        if (this.isRequired[t][id]){
          this.moveOptionRequiredSign(dt);
        }                             
      }
    }         
  },

  
	makeBoptionRequired : function(e, id, dd, dt, oT){
	  var vId,input;	
	  var t = 'b';	   
    if (oT == 'radio'|| oT == 'checkbox'){         	       
      var l = this.vIdsByOId[t][id].length;
      for (var i=0;i<l;i++){
        vId = this.vIdsByOId[t][id][i];         
        input = this.getRadioInput(t, id, vId, i);
        input.addClassName('validate-one-required-by-name');
        input.advaiceContainer = 'bundle-option-'+id+'-container';     
        input.callbackFunction = window.validateOptionsCallback ? 'validateOptionsCallback' : 'bundle.validationCallback';            
      }                     
      Element.insert(dd.down('ul'),{'after': new Element('span', {'id' : 'bundle-option-'+id+'-container'})});
      
      if ($('bundle-option-'+id)){
         $('bundle-option-'+id).up('li').remove();
      } 
    } else if (oT == 'multiple'){      
      e.options[0].remove();//remove the None option
      e.addClassName('required-entry');                     				           
    } else {         
      e.addClassName('required-entry');
    }    
    
    var label = dt.down('label');
    label.addClassName('required');         
    Element.insert(label, {'bottom': new Element('em').update('*')});
  },
  


	moveOptionRequiredSign : function(dt){
    var label = dt.down('label');      
    Element.insert(label, {'bottom': label.down('em')});
  },   
    
    
	setAdvaiceContainer : function(id){
	  var vId,input;
	  
	  var t = 'o';     	       	       
    var l = this.vIdsByOId[t][id].length;
    for (var i=0;i<l;i++){
      vId = this.vIdsByOId[t][id][i];         
      input = this.getRadioInput(t, id, vId, i);
      input.advaiceContainer = 'options-'+id+'-container';
      input.callbackFunction = 'validateOptionsCallback';            
    }     
	},
	
	
	getRadioInput : function(t, id, vId, i){	
   if (t == 'b')
     return $('bundle-option-'+id+'-'+ vId);    
   else 
     return $('options_'+id+'_'+ (i + 2));       	
	},	
	   
	    
	addDateValidation : function(id){
    Validation.add('validate-datetime-'+id, this.requiredOptionText, function(v,e) {
       var optionId = 0;
       e.name.sub(/[0-9]+/, function(match){
          optionId = parseInt(match[0]);
       });
       var cssRule = '.datetime-picker[id^="options_'+optionId+'"]';
       var dateTimeParts = this.isEditOrderPage ? $('product_composite_configure_form_fields').select(cssRule) : $$(cssRule);
       for (var i=0; i < dateTimeParts.length; i++) {
           if (dateTimeParts[i].value == "") return false;
       }
       return true;
    }.bind(this));
  },


	addDateCompleteValidation : function(id){  
    Validation.add('validate-datetime-'+id, this.fieldIsNotCompleteText, function(v,e) {
      var optionId = 0;
      e.name.sub(/[0-9]+/, function(match){
        optionId = parseInt(match[0]);
      });  

      var minuteToZero = false;
      var hasWithValue = false, hasWithNoValue = false;
      var dayId    = 'options_'+optionId+'_day_part';
      var minuteId = 'options_'+optionId+'_minute';
      
      var cssRule = '.datetime-picker[id^="options_'+optionId+'"]';
      var dateTimeParts = $('product_composite_configure_form_fields').select(cssRule);                 
      for (var i=0; i < dateTimeParts.length; i++) {
         if (dateTimeParts[i].id != dayId) {
             if (dateTimeParts[i].value != "") {
             
                if (dateTimeParts[i].id == minuteId && dateTimeParts[i].value == 0)
                  minuteToZero = true;
                  
                hasWithValue = true;
             } else {
                hasWithNoValue = true;
             }
         }
      }
      
      // to fix Magento 1.7.0.0 bug. minutes are set to 00 by default in:
      // magento/app/design/adminhtml/default/default/template/catalog/product/composite/fieldset/options/type/date.phtml
      var complete = hasWithValue ^ hasWithNoValue;      
      if (minuteToZero && !complete)
        complete = true;
        
      return complete;
    });
  }, 
  
  
	setVariables : function(){
    var type,id,fVId,oT,e;
    	
    var l = this.sortedOIds.length;
    for (i=0;i<l;i++){        
      t = this.sortedOIds[i]['type'];     
      id   = this.sortedOIds[i]['id'];
      fvId = this.sortedOIds[i]['fvId'];
      oT = this.sortedOIds[i]['optionType'];  

      e = this.getElementByOptionType(t, id, fvId, oT);             
     
      this.setOptionVariables(e, t, id, oT);
      this.setOptionElement(e, t, id, oT);
      this.observeOptionElement(e, t, id, oT);          
    }	
  },	
	
	
	setOptionVariables : function(e, t, id, oT){
            
      this.oldO[t][id] = {};								
      this.oldO[t][id].oT = oT; 
      this.oldO[t][id].visible = true;
                                         
      switch (oT){            
        case 'drop_down' :
        case 'multiple' :                   
        case 'radio' :
        case 'checkbox' :                       
        	this.setOptionValueVariables(e, t, id, oT);         	                          
      }
      
      switch (oT){  
        case 'drop_down' :
        case 'multiple' :        
        	this.setIndexByValue(e, t, oT);                        	                          
      }      

  },


	setOptionValueVariables : function(e, t ,id, oT){  
      var vId, ind;	
      			         								 			
      var l = this.vIdsByOId[t][id].length;
      for (var i=0;i<l;i++){
        vId = this.vIdsByOId[t][id][i];
        this.oldV[t][vId] = {};					
        this.oldV[t][vId].visible = true;
        switch (oT){ 
          case 'multiple' :
            ind = t == 'b' && !this.isRequired[t][id] ? i+1 : i;   //check bundle multiple not required None value   
            this.oldV[t][vId].name = e.options[ind].text;
            break;           
          case 'drop_down' :         
            this.oldV[t][vId].name = e.options[i+1].text;	          				                                    
        }                          
      }       
  },  
 
  
   
	setIndexByValue : function(e, t, oT){  
      var vId;	      			         								 			
      var l = e.options.length;
      for (var i=0;i<l;i++){
        if (e.options[i].value || oT == 'multiple'){
          vId = parseInt(e.options[i].value);
          this.indByValue[t][vId] = i;
        } 						                                                                     
      }       
  },
  
    
	setOptionElement : function(e, t, id, oT){			 
    var vId,vE;
    
    if (oT == 'radio' || oT == 'checkbox') {	       
      var l = this.vIdsByOId[t][id].length;
      for (var i=0;i<l;i++){
        vId = this.vIdsByOId[t][id][i];
        vE = this.getRadioInput(t, id, vId, i);
        this.oldV[t][vId].e  = vE;
        this.oldV[t][vId].li = vE.up('li');
      }        				
    }
            
    this.oldO[t][id].e = e;
    this.oldO[t][id].dd = this.getElementDdTag(e);
    this.oldO[t][id].dt = this.getElementDtTag(e);						

	},		


	observeOptionElement : function(e, t, id, oT){
    var vId,el;
    
    if (oT == 'radio' || oT == 'checkbox') {		
      var l = this.vIdsByOId[t][id].length;								
      if (oT == 'radio'){

        if (!this.isRequired[t][id] && oT == 'radio'){ //observe None option
          el = t == 'b' ? $('bundle-option-'+id) : $('options_'+id);
          if (el)           
            el.observe('click', this.observeRadio.bind(this, el, t, id, null));       
        }        
      
        for (var i=0;i<l;i++){
          vId = this.vIdsByOId[t][id][i];      
          if (t == 'b')    
            el = $('bundle-option-'+id+'-'+ vId);
          else
            el = $('options_'+id+'_'+ (i+2));          
          el.observe('click', this.observeRadio.bind(this, el, t, id, vId, true));          
        }							
      } else {
        for (var i=0;i<l;i++){
          vId = this.vIdsByOId[t][id][i];      
          if (t == 'b')    
            el = $('bundle-option-'+id+'-'+ vId);
          else
            el = $('options_'+id+'_'+ (i+2));          
          el.observe('click', this.observeCheckbox.bind(this, el, t, id, vId, true));
        }						
      }						
    } else if (oT == 'drop_down'){
      e.observe('change', this.observeSelectOne.bind(this, e, t, id, true));
    } else if (oT == 'multiple'){
      e.observe('change', this.observeSelectMultiple.bind(this, e, t, id, true));					
    }	
	},
	
	
	reloadElements : function(){	
	
    var type,id,fVId,oT,e;
    	
    var l = this.sortedOIds.length;
    for (i=0;i<l;i++){        
      t = this.sortedOIds[i]['type'];     
      id   = this.sortedOIds[i]['id'];
      fvId = this.sortedOIds[i]['fvId'];
      oT = this.sortedOIds[i]['optionType'];  
        
      e = this.getElementByOptionType(t, id, fvId, oT);             

      this.setOptionElement(e, t, id, oT);
      this.observeOptionElement(e, t, id, oT);          
    }	
	},

	
	hideOptions : function(){
		var t,l,ii,id;
		
		var i = 2;
		while(i--){
      t = i== 0 ? 'b' : 'o';
       
      l = this.optionIds[t].length;			
      for (ii=0;ii<l;ii++){	
        id = this.optionIds[t][ii];
        if (this.cOIdsByOId[t][id]){
          this.reloadOptions(t, id, 'b', [], []);
          this.reloadOptions(t, id, 'o', [], []);          
        }
        
      }   	
		}
		this.dependecyIsSet = true;		
	},	


	reloadOptions : function(t, id, tt, optionIds, valueIds){
    var oId,option,vIds,ndVIds;

    var vIdsByO = this.groupByOption(tt, valueIds);

    var c = this.cOIdsByOId[t][id];
    if (c && c[tt]){
      var l = c[tt].length;
      while (l--){
        oId = c[tt][l];
        option = this.oldO[tt][oId];
        vIds = vIdsByO[oId] ? vIdsByO[oId] : [];
        
        if (this.optionIds[tt].indexOf(oId) == -1) //out of stock option
          continue;
        
        ndVIds = this.notDepVIdsByOId[tt][oId];	        
        if (ndVIds)
          vIds = vIds.concat(ndVIds);
                    
        if (optionIds.indexOf(oId) != -1){ // display entire option	

           switch (option.oT){
            case 'drop_down' :
            case 'multiple' :           
            case 'radio' :
            case 'checkbox' :   
                vIds = this.vIdsByOId[tt][oId];                             
                this.reloadValues(tt, oId, vIds);
              break;      
            default:
              this.showOption(tt, oId);                          
          }
          
        } else if (vIds.length > 0){ // display children values
        
          this.reloadValues(tt, oId, vIds);		  

        } else { // hide option	
        
           switch (option.oT){         
            case 'drop_down' :
            case 'multiple' :
            case 'radio' :
            case 'checkbox' :                   			
              this.reloadValues(tt, oId, []);	                         
          }        
	
          this.hideOption(tt, oId);					
        }	
      }
    }  
      	
	},

	
	showOption : function(t, id){
	
    var option = this.oldO[t][id];
    
		if (!option.visible){		
		
		  option.dd.show();
		  option.dt.show();

      if (this.isEditOrderPage && this.isRequired[t][id]){
        if (option.oT == 'radio'|| option.oT == 'checkbox'){
          var vId;         	       
          var l = this.vIdsByOId[t][id].length;
          for (var i=0;i<l;i++){
            vId = this.vIdsByOId[t][id][i];         
            this.getRadioInput(t, id, vId, i).addClassName('validate-one-required-by-name');
          }              
        } else if (option.oT == 'date' || option.oT ==  'date_time' || option.oT == 'time'){
          var el = option.dd.select('input[name="validate_datetime_'+ id +'"]')[0];
          el.addClassName('validate-datetime-'+ id);              
        } else {	      
          option.e.addClassName('required-entry');
        }
      }
        		  
			if (option.oT == 'file'){
				var disabled = false;
				
				if (this.inPreconfigured){
	        var inputBox = option.e.up('.input-box');
	        if (!inputBox.visible()){
						var inputFileAction = inputBox.select('input[name="options_'+ id +'_file_action"]')[0];
						inputFileAction.value = 'save_old';
						disabled = true;
					}	
				}
						
				option.e.disabled = disabled;				
			}	  
		  
		  option.visible = true;           		  
		} 			 
	},	
	
	
	hideOption : function(t, id){

    var option = this.oldO[t][id];
	
		if (option.visible){

			if (this.dependecyIsSet){
        switch (option.oT){
          case 'date' :
          case 'date_time':        
          case 'time' :
            this.resetDate(id, option.oT);   
            break;
          case 'field' :
          case 'area' :         
            option.e.value = ''; 
            break;
          case 'file' :        
            if (this.inPreconfigured) {
              var inputBox = option.e.up('.input-box');
              if (!inputBox.visible()){
                var inputFileAction = inputBox.select('input[name="options_'+ id +'_file_action"]')[0];
                inputFileAction.value = '';															
              }	                
            }	        
            option.e.disabled = true;               					                        
        }
			}			

      if (this.isEditOrderPage && this.isRequired[t][id]){
        if (option.oT == 'radio'|| option.oT == 'checkbox'){         	       
          var vId;         	       
          var l = this.vIdsByOId[t][id].length;
          for (var i=0;i<l;i++){
            vId = this.vIdsByOId[t][id][i];         
            this.getRadioInput(t, id, vId, i).removeClassName('validate-one-required-by-name');
          }                         
        } else if (option.oT == 'date' || option.oT ==  'date_time' || option.oT == 'time'){
          var el = option.dd.select('input[name="validate_datetime_'+ id +'"]')[0];
          el.removeClassName('validate-datetime-'+ id);    
        } else {	      
          option.e.removeClassName('required-entry');
        }      
			}
			
		  option.dd.hide();
		  option.dt.hide();
		  option.visible = false;
		}  	
		
	},


	resetDate : function(id, oT){
    if (oT ==  'date' || oT == 'date_time') {
      if (this.useCalendar){
	      $('options_'+id+'_date').value = '';      
      } else {     	
        $('options_'+id+'_month').selectedIndex = 0;   
        $('options_'+id+'_day').selectedIndex = 0;
        $('options_'+id+'_year').selectedIndex = 0;
      }
    }          
    if (oT ==  'date_time' || oT == 'time') { 
      $('options_'+id+'_hour').selectedIndex = 0;
      $('options_'+id+'_minute').selectedIndex = 0;
      $('options_'+id+'_day_part').selectedIndex = 0;
    }	
	},		
  
  
	reloadValues : function(t, id, ids){
	  var vId,value;
	  
    var l = this.vIdsByOId[t][id].length;   
    if (l == 0)
    	return; 
    	   
	  var option = this.oldO[t][id];
    switch (option.oT){     
      case 'drop_down' :
      case 'multiple' :
        this.clearSelect(t, id);        
        for (var i=0;i<l;i++){
          vId = this.vIdsByOId[t][id][i]; 	
          if (ids.indexOf(vId) != -1)			
              this.showValue(t, id, vId);
        }      
        break;
      case 'radio' :
      case 'checkbox' :               
        for (var i=0;i<l;i++){
          vId = this.vIdsByOId[t][id][i];
          value = this.oldV[t][vId];             			
          if (ids.indexOf(vId) != -1){		
            if (!value.visible)
              this.showValue(t, id, vId);
            else 
              this.resetRadioValue(t, id, vId);
          } else if (value.visible){	    
            this.hideRadioValue(t, id, vId);
          }
        }                          
    }
     
	},
	

	showValue : function(t, id, vId){
	
	  var value = this.oldV[t][vId];
	  var option = this.oldO[t][id];

    switch (option.oT){    
      case 'drop_down' :
				this.showPickerImage(t, id, vId);          
      case 'multiple' :
        var ind = option.e.options.length;        
        option.e.options[ind] = new Option(value.name, vId);			
        this.indByValue[t][vId] = ind;
        this.showOption(t, id);    
        break;
      case 'radio' :
      case 'checkbox' :      
        if (this.isEditOrderPage && this.isRequired[t][id])       
          value.e.addClassName('validate-one-required-by-name');            
        value.li.show();	
        this.showOption(t, id);	                         
    }
		
		value.visible = true;
	},
	
	
	resetRadioValue : function(t, id, vId){
	
	  var value = this.oldV[t][vId];
	  var option = this.oldO[t][id];

    if (value.e.checked){  
      this.resetImage(t, id, vId, option.e.type);	             
      value.e.checked = false;
      if (t == 'b')
        this.reloadPrice(t, {id:value.e.id, cheked:false, value:''});
    }                          
	},
	
	
	hideRadioValue : function(t, id, vId){
		
	  this.resetRadioValue(t, id, vId);
	  
	  var value = this.oldV[t][vId];
         
    if (this.isEditOrderPage && this.isRequired[t][id])       
      value.e.removeClassName('validate-one-required-by-name'); 
                   
    value.li.hide();	                         
		value.visible = false;
	},	
		
	
  clearSelect : function(t, id){
	  var vId,value;
    var option = this.oldO[t][id];
    	    
    var l = this.vIdsByOId[t][id].length;
    while (l--){
      vId = this.vIdsByOId[t][id][l];
      value = this.oldV[t][vId];
      if (option.e.value)
	      this.resetImage(t, id, vId, option.e.type);
      if (option.e.type == 'select-one') 
        this.hidePickerImage(t, id, vId);	                        
      this.indByValue[t][vId] = null;
      value.visible = false;					  
    } 
 			
    option.e.options.length = option.e.type == 'select-one' || (t == 'b' && !this.isRequired[t][id]) ? 1 : 0; //check bundle multiple required for None value
    
    if (t == 'b'){
			this.reloadPrice(t, option.e);
		}		               			        					       	   
  },	

	
	observeRadio : function($super, e, t, id, vId, event){
    var tt,oIds,vIds,c;
    	
		if (this.cOIdsByOId[t][id]){
      var i = 2;
      while(i--){
        tt = i== 0 ? 'b' : 'o';		
        c =	this.cOIdsByVId[t][vId];	
        oIds = c && c[tt] ? c[tt] : [];
        
        c =	this.cVIdsByVId[t][vId];        			
        vIds = c && c[tt] ? c[tt] : [];
        	        
        this.reloadOptions(t, id, tt, oIds, vIds);
      }  								
		}
		
    if (event){
      this.checkedIds[t][id] = [];		
      if (vId)		
        this.checkedIds[t][id].push(parseInt(vId));
      this.selectDefault(t, id);            
    } 		
		
		$super(t, id, vId);
		this.reloadPrice(t, e);						
	},
	
	
	observeCheckbox : function($super, e, t, id, valueId, event){
		var tt,ii,vId,ids,vIds,c;	
	  var selectedIds = [];	
		
    var l = this.vIdsByOId[t][id].length;
    var i = 2;
    while(i--){
      tt = i== 0 ? 'b' : 'o';	
      ids = [];
      vIds  = [];        			
      for(ii = 0;ii<l;ii++){	
        vId = this.vIdsByOId[t][id][ii];	          			  
        if (this.oldV[t][vId].e.checked){
        
          c =	this.cOIdsByVId[t][vId];    
          if (c && c[tt])													
            ids = ids.concat(c[tt]);
            
          c =	this.cVIdsByVId[t][vId];               
          if (c && c[tt])	            	
            vIds  = vIds.concat(c[tt]);
                           
          if (selectedIds.indexOf(vId) == -1)
            selectedIds.push(vId);           
        }												
      }
      if (this.cOIdsByOId[t][id])
        this.reloadOptions(t, id, tt, this.uniq(ids), this.uniq(vIds));				
    }				
		
    if (event){
      this.checkedIds[t][id] = selectedIds;		
      this.selectDefault(t, id);         
    }		
    		
		$super(e, t, id, valueId);
		this.reloadPrice(t, e);					
	},
	
	
	observeSelectOne : function($super, e, t, id, event){
    var tt,oIds,vIds,c;
        
		var vId = e.value;  		
		if (this.cOIdsByOId[t][id]){		
      var i = 2;
      while(i--){
        tt = i== 0 ? 'b' : 'o';
        c =	this.cOIdsByVId[t][vId];	
        oIds = c && c[tt] ? c[tt] : [];
        
        c =	this.cVIdsByVId[t][vId];        			
        vIds = c && c[tt] ? c[tt] : [];	

        this.reloadOptions(t, id, tt, oIds, vIds);
      } 
		}
		
    if (event){
      this.checkedIds[t][id] = [];		
      if (vId)		
        this.checkedIds[t][id].push(parseInt(vId));
      this.selectDefault(t, id);            
    }		
		
		$super(e, t, id);		
		this.reloadPrice(t, e);				
	},
	
	
	observeSelectMultiple : function($super, e, t, id, event){
		var tt,ii,vId,ids,vIds,c;			
	  var selectedIds = [];			
		
    var options = $A(e.options);		
    var l = options.length;
    var i = 2;
    while(i--){
      tt = i== 0 ? 'b' : 'o';
      ids = [];
      vIds  = [];	        				
      for(ii = 0;ii<l;ii++){	
        vId = options[ii].value;			  
        if (vId && options[ii].selected){
          c = this.cOIdsByVId[t][vId];    
          if (c && c[tt])													
            ids = ids.concat(c[tt]);
            
          c = this.cVIdsByVId[t][vId]  
          if (c && c[tt])	            	
            vIds  = vIds.concat(c[tt]);
            
          vId = parseInt(vId);            
          if (selectedIds.indexOf(vId) == -1)
            selectedIds.push(vId);          
        }												
      }
      if (this.cOIdsByOId[t][id])
        this.reloadOptions(t, id, tt, this.uniq(ids), this.uniq(vIds));				
    }					
		
    if (event){
      this.checkedIds[t][id] = selectedIds;
      this.selectDefault(t, id);		   
    }		
		
		$super(e, t, id);
		this.reloadPrice(t, e);				
	},

	
	selectDefault : function(t, fromOptionId){
  	var tt,id;
  		
    var l = this.sortedOIds.length;
    for (var i=0;i<l;i++){        
      tt  = this.sortedOIds[i]['type'];
      id = this.sortedOIds[i]['id'];
      
      if (fromOptionId){
        if (tt == t && id == fromOptionId)
          fromOptionId = null;
        continue;
      }
      
      this.selectOptionDefault(tt, id);			   
    }
          
  },
     
     
	selectOptionDefault : function(t, id){
  	var vId,group,ids,ll,ind;
     
    var checkedIds = this.checkedIds[t][id];
    if (checkedIds == undefined)
      return;
  	      
    var option = this.oldO[t][id];        
    if (option.visible){
      
      ids = this.vIdsByOId[t][id];
      ll = ids.length;		
      while (ll--){
        vId = ids[ll];
        value = this.oldV[t][vId];
        if (value.visible && checkedIds.indexOf(vId) != -1){
          if (option.oT == 'drop_down' || option.oT == 'multiple'){
            ind = this.indByValue[t][vId];
            if (option.e.type == 'select-one')
              option.e.selectedIndex = ind;   
            else 
              option.e.options[ind].selected = true;                                   
          } else if (option.oT == 'radio' || option.oT == 'checkbox'){
            value.e.checked = true;
            if (option.oT == 'radio')
             this.observeRadio(value.e, t, id, value.e.value);
            else 
             this.observeCheckbox(value.e, t, id, value.e.value);                                
          }
        }		
      }	

      if (option.oT == 'drop_down'){
        this.observeSelectOne(option.e, t, id);		
      } else if (option.oT == 'multiple'){  		
        this.observeSelectMultiple(option.e, t, id);        
      }
    }  
				
	},

	
	groupByOption : function(t, valueIds){
    var oId,vId;
    		
	  var vIdsByO = [];
    if (valueIds){
      var l = valueIds.length;
      while (l--){
        vId = valueIds[l];
        oId = this.oIdByVId[t][vId];

        if (vIdsByO[oId] == undefined)
          vIdsByO[oId] = [];
          				
        vIdsByO[oId].push(vId);
      }      
    }
    
    return vIdsByO;
  },
    
    
	uniq : function(a){
		var l=a.length,b=[],c=[];
		while (l--)
			if (c[a[l]] == undefined) b[b.length] = c[a[l]] = a[l];
		return b;
	},


	getElementByOptionType : function(t, id, fvId, oT){
	  var e;
	
    switch (oT){       
      case 'drop_down' :
      case 'multiple' :
        if (t == 'b')   
          e = $('bundle-option-'+id); 
        else
          e = $('select_'+id);                  
        break;
      case 'radio' :
      case 'checkbox' :        
        if (t == 'b')   
          e = $('bundle-option-'+id+'-'+fvId); 
        else
          e = $('options_'+id+'_2');                     
        break;
      case 'field' :
      case 'area' :
        e = $('options_'+id+'_text');            
        break;                                    
      case 'date' :
      case 'date_time' :
        e = this.useCalendar ? $('options_'+id+'_date') : $('options_'+id+'_month');                  
        break;
      case 'time' :
        e = $('options_'+id+'_hour');            
        break;
      case 'file' :
        e = this.getOptionsWrapperDiv().select('input[name="options_'+id+'_file"]')[0];                           
    }
   
    return e;  
	},
	
	
	getOptionsWrapperDiv : function(){	  
	  return this.isEditOrderPage ? $('product_composite_configure_form_fields') : $('product-options-wrapper');
	},
	  
	  
	getElementDdTag : function(e){
    return e.up('dd');
  }, 
  
  
	getElementDtTag : function(e){
    return e.up('dd').previous('dt');
  },      
  
  
	reloadPrice : function(t, e){
    if (this.isEditOrderPage)
      return;	
        
	  if (t == 'b'){
      bundle.changeSelection(e);
    } else {
	    opConfig.reloadPrice();    
    }  	  
	}	
			
});














