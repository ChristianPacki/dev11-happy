/**
 * Little wrapper for magento zoom tool initialization
 */
// Copy old initialize method to the new _init private method
Product.Zoom.prototype._init = Product.Zoom.prototype.initialize;
// Replace the old initialize method with a wrapper and call for private _init method if necessary 
Product.Zoom.prototype.initialize = function (imageEl, trackEl, handleEl, zoomInEl, zoomOutEl, hintEl) {
    if ($(imageEl)) {
        this._init(imageEl, trackEl, handleEl, zoomInEl, zoomOutEl, hintEl);
    }
};