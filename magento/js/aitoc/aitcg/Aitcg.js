/**
 * Set of different usable functions
 */
Aitcg =
{
    /**
     * From and to radians multipliers
     * @see http://en.wikipedia.org/wiki/Radian
     */
    toRadMult  : Math.PI / 180,
    fromRadMult: 180 / Math.PI,

    tooltipObj: null,

    optionArray: [ ],
    /**
     * Create and return tooltip singleton object
     *
     * @return Aitcg_Tooltip
     */
    tooltip: function()
    {
        if (!this.tooltipObj) {
            var style = 'dark';
            if (typeof aitcgTooltipsStyle != 'undefined') {
                style = aitcgTooltipsStyle;
            }
            this.tooltipObj = new Aitcg_Tooltip( style );
        }
        return this.tooltipObj;
    },

    /**
     * Returns an evaluation of a parameter which was passed false or returns -1 if all params are positive.
     *
     * @param x Cathetus
     * @param y Cathetus
     * @param z Hypotenuse
     * @return {number} // -1 if params are incorrect
     *
     * @see http://en.wikipedia.org/wiki/Pythagorean_theorem
     */
    pythagoras: function( x, y, z )
    {
        if (x && y && !z) {
            return Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2));
        } else if ( x && z && !y ) {
            return Math.sqrt(Math.pow(z, 2) - Math.pow(x, 2));
        } else if ( y && z && !x ) {
            return Math.sqrt(Math.pow(z, 2) - Math.pow(y, 2));
        }
        return -1;
    },

    cachedImages : {},
    
    getImageData: function( imgId )
    {
        return {
            origW: this.cachedImages[imgId].width,
            origH: this.cachedImages[imgId].height
        }        
    }, 
    
    getDefault: function()
    {
        return {
            dim : document.viewport.getDimensions(),
            off : document.viewport.getScrollOffsets(),
            curr: {}
        };        
    },
    
    clone: function( el )
    {
        if (typeof(JSON) != 'undefined') {
            return eval('(' + JSON.stringify(el) + ')');
        } else {
            return this.deepCloneJSON(el);
        }
    },
    
    deepCloneJSON: function( obj )
    {
        var outputArr = new Array();
        for (var i in obj) {
            outputArr[i] = typeof (obj[i]) == 'object' ? this.deepCloneJSON(obj[i]) : obj[i];
        }
        return outputArr;
    },

    /**
     * Load and cache an image then invoke a callback
     *
     * @param imgId
     * @param path
     * @param callback
     */
    cacheImage: function( imgId, path, callback )
    {
        if (typeof(this.cachedImages[imgId]) == 'undefined') {
            this.cachedImages[imgId] = document.createElement('img');
            var img = this.cachedImages[imgId];
    
            Event.observe(img, 'load', callback.cached.bind(callback));
            img.src = path;
        }else{
            callback.cached();
        }
    },     
    
    countMult: function( imgId, origW, origH, mult )
    {
        // multiplier should not be higher then 1.0
        mult = (mult > 1) ? 1 : mult;

        var scr = this.getDefault(),
            offsetX = 30,
            offsetY = 75;

        if (imgId != 0 && typeof(this.cachedImages[imgId]) != 'undefined') {
            scr.orig = this.getImageData(imgId);
        } else {
            scr.orig = {
                origW: origW,
                origH: origH
            };
        }

        if (!mult) {
            if (scr.orig.origW > (scr.dim.width - offsetX)) {
                mult = (scr.dim.width - offsetX) / scr.orig.origW;
            }

            if (scr.orig.origH > (scr.dim.height - offsetY)) {
                var mY = (scr.dim.height - offsetY) / scr.orig.origH;
                if (mY < mult) {
                    mult = mY;
                }
            }
        }

        scr.mult = mult;
        scr.curr.width  = Math.round(scr.orig.origW * mult);
        scr.curr.height = Math.round(scr.orig.origH * mult);
        return scr;

    },
    
    /**
     * if you've made changes in this method be sure to
     * make same changes in Aitoc_Aitcg_Model_Image
     */        
    checkSizes: function( imgX, imgY, maxX, maxY )
    {
        imgX = imgX * 2;
        imgY = imgY * 2;
        var id = 'id' + imgX + '_' + imgY + '_' + maxX + '_' + maxY;
        if (typeof(this.checkSizes[id]) != 'undefined') {
            return this.checkSizes[id];
        }
        var x = imgX / maxX,
            y = imgY / maxY;
        var ret = {};
        if ( x > y && x > 1) {
            ret =  this.calcSizes(imgX, imgY, maxX, maxY);
            ret.axis = 'x';
        } else if( y > 1 ) {
            var t = this.calcSizes(imgY, imgX, maxY, maxX);
            ret = {x: t.y, y: t.x};
            ret.axis = 'y';
        } else {
            ret = {x: imgX, y: imgY};
            ret.axis = 'b';
        }
        if (ret.x > maxX || ret.y > maxY) {
            var scale_x = maxX / ret.x,
                scale_y = maxY / ret.y;
            if ( scale_x < scale_y ) {
                ret.y = ret.y * ret.x / maxX;
                ret.x = maxX;
                ret.axis = 'y';
            } else {
                ret.x = ret.x * ret.y / maxY;
                ret.y = maxY;
                ret.axis = 'x';
            }
        }
        ret["posX"] = Math.round((maxX - ret.x) / 2);
        ret["posY"] = Math.round((maxY - ret.y) / 2);
        ret.mult = ret.x / imgX;
        return ret;
    },

    calcSizes: function( a, b, maxa, maxb ) {
        maxb = b * maxa / a;
        return {x: maxa, y: maxb};
    },
    
    showLoader: function()
    {
        try {
            if ($('loading-mask') == null) {
                if (typeof(AitPopupHtml) != 'undefined') {
                    $$('body')[0].insert({ bottom: AitPopupHtml });
                } else {
                    document.body.insert({ bottom: '<div id="loading-mask">Please wait...</div>' });
                }
            }
            $('loading-mask').show();
        }
        catch (e){}
    },

    hideLoader: function()
    {
        try {
            $('loading-mask').hide();
        }
        catch (e){}
    },

    findPosX: function( obj )
    {
        var curleft = 0;
        if (obj.offsetParent) {
            while (1) {
                curleft += obj.offsetLeft;
                if (!obj.offsetParent) {
                    break;
                }
                obj = obj.offsetParent;
            }
        } else if (obj.x) {
            curleft += obj.x;
        }
        return curleft;
    },

    findPosY: function( obj )
    {
        var curtop = 0;
        if (obj.offsetParent) {
            while (1) {
                curtop += obj.offsetTop;
                if (!obj.offsetParent) {
                    break;
                }
                obj = obj.offsetParent;
            }
        } else if (obj.y) {
            curtop += obj.y;
        }
        return curtop;
    },

    /**
     * Add 'px' to all given array values
     *
     * @param arrays {}
     * @return {Object}
     */
    addPxToValue: function( arrays )
    {
        var array_new = {};
        for (var item in arrays) {
            array_new[item] = arrays[item] + 'px';

        }
        return array_new;
    },

    getEventTarget: function(e)
    {
        if(e)return e.target;
        return window.event.srcElement;
    }
};