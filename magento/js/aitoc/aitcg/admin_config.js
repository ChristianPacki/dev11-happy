Validation.add('aitcg-image-size', 'Incorrect size pattern. Please use the one like 123x123.', function(v){
    return /^\d+x\d+$/.test(v);
});
Event.observe(window, 'load', function(){
    jscolor.init();
});