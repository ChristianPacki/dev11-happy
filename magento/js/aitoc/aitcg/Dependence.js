var Dependence = Class.create({
    initialize: function() {
        var element = $('font_family_id');
        var input = $('new_font_family');

        if (element) {
            element.observe('change', function(event) {
                if (this.value != 0) {
                    input.parentElement.parentElement.hide();
                } else {
                    input.parentElement.parentElement.show();
                }
            });

            if (element.value != 0) {
                input.parentElement.parentElement.hide();
            } else {
                input.parentElement.parentElement.show();
            }
        }
    },
});
