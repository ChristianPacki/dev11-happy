/**
 * Gallery editor view
 */

Aitcg_View_Gallery.prototype._initEditorPlaceholder = function()
{
    // check the flag in the shared static object
    if (!this.gallery.inited) {
        this.gallery.inited = true;

        // remove zoom tool
        $$('.product-view .zoom, .product-view .zoom-notice').each(Element.remove);

        // replace the product image block with an editor's container
        $$('.product-view .product-image')[0]
            .insert({ before: '<div id="aitcg-gallery-editor"></div><div id="aitcg-gallery-tooltip" class="zoom-notice">' + this.config.text.galleryTooltip + '</div>' })
            .hide();

        this.gallery.editor = $('aitcg-gallery-editor');
    }
    else if(!$('aitcg-gallery-editor').visible())
    {
        this.gallery.editor.show();
        $('aitcg-gallery-tooltip').show();
        $$('.product-view .product-image')[0].hide();
    }
}
document.observe("dom:loaded", function() {
    if(typeof ProductMediaManager == 'undefined')
    {
        return true;
    }
    ProductMediaManager.swapImageOld = ProductMediaManager.swapImage;
    ProductMediaManager.swapImage = function(targetImage) {
        this.swapImageOld(targetImage);

        if($('aitcg-gallery-editor') && $('aitcg-gallery-editor').visible() )
        {
            var disable = false;
            var activeOpt = null;
            Aitcg.optionArray.each(function(opt){
                if(opt.config.viewType == 'Gallery'
                    && opt.view.gallery.active
                    && opt.view.gallery.active.id == opt.id )
                {
                    disable = true;
                    activeOpt = opt.view;
                }
            });

            if (disable) {
                if (!window.confirm(activeOpt.config.text.areYouSure)) {
                    return;
                }
                activeOpt.switchFromEditor();
            }

            $('aitcg-gallery-editor').hide();
            $('aitcg-gallery-tooltip').hide();
            $$('.product-view .product-image')[0].show();
        }
    }
});