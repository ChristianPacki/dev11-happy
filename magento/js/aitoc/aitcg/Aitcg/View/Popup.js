/**
 * Popup editor view
 *
 * TODO: Escape button should close the editor
 */
var Aitcg_View_Popup = Class.create( Aitcg_View_Abstract,
{
    window: null,
    template: '',
    previewScale: null,

    startEditor: function()
    {
        Aitcg.showLoader();

        this.scr = Aitcg.countMult( this.id, this.config.productImage.sizeX, this.config.productImage.sizeY, 1);
        this._setTemplateSetting();

        var options = this.getTemplateSetting();

        // create popup window object by template and its options
        this.window = new Aitcg_Popup(this._getPopupTemplate(), options);
        this.window.showWindow(this.id, this.scr);

        // init additional tools data and observers
        this.option.tools.initToolsData().initTools();

        var el = $(Aitcg_Editor.CONTAINER_ID + this.id);
        this.editor.init(el, Aitcg_Editor.MODE_EDIT, false, 1);
        this.editor.load( $('options_' + this.id).getValue() );

        Aitcg.tooltip().initObservers(".tooltip-help");

        this.initObservers();

        Aitcg.hideLoader();
    },

    /**
     * Render entire popup template
     *
     * @return string
     */
    _getPopupTemplate: function()
    {
        if (this.template == '') {
            if (mobileMode) {
                this.template =
                    '<div id="message-popup-window-mask" ></div>' +
                    '<div id="message-popup-window" class="aitcg-popup print-area-editor">' +
                    '<div class="aitclear"></div>' +
                    '<div class="message-popup-ait" style="text-align: left;">' +
                    '<div class="title">' +
                    this._getHeadButtons() +
                    '</div>' +
                    this._getToolsHtml() +
                    this._getControlPanelHtml() +
                    this._getImageContainerHtml() +
                    '</div>'+
                    '</div>';
            } else {
                this.template =
                    '<div id="message-popup-window-mask" ></div>' +
                    '<div id="message-popup-window" class="aitcg-popup print-area-editor">' +
                    this._getImageContainerHtml() +
                    '<div class="aitclear"></div>' +
                    '<div class="message-popup-ait" style="text-align: left;">' +
                    '<div class="title">' +
                    this._getHeadButtons() +
                    '</div>' +
                    this._getToolsHtml() +
                    this._getControlPanelHtml() +
                    '</div>'+
                    '</div>';
            }
        }
        return this.template;
    },

    _getHeadButtons: function()
    {
        var template = '<div onclick="opCimage{{rand}}.closeEditor();" title="{{close_text}}" class="close-but"></div>';
        if (this.config.editorEnabled) {
            template += '<div title="' + this.config.text.editorHelp + '" class="help-but tooltip-help"></div>';
        }
        return template;
    },

    /**
     * Return HTML code for main editor container
     * and all its inner layers (except masks layer)
     *
     * @return string
     */
    _getImageContainerHtml: function()
    {
        return '<div id="' + Aitcg_Editor.CONTAINER_ID + '{{option_id}}" class="aitcg_image_container" style="width:{{img_width}}px;height:{{img_height}}px;position:relative;overflow:hidden;">' +
            '<div id="' + Aitcg_Editor_Canvas.RAPH_BOT_ID_SUFFIX + '{{option_id}}" class="aitraph aitraph-bot" style="left:{{left}};top:{{top}};width:{{width}};height:{{height}};z-index:1000;"></div>' +
            '<img src="{{full_image}}" id="' + Aitcg_Editor.TEMPLATE_ID + '" width="{{img_width}}" height="{{img_height}}" alt="" style="position:absolute;left:0px;top:0px; z-index:3000;"/>' +
            '<div id="' + Aitcg_Editor_Canvas.RAPH_TOP_ID_SUFFIX + '{{option_id}}" class="aitraph aitraph-top" style="left:{{left}};top:{{top}};width:{{width}};height:{{height}};z-index:5000;"></div>' +
            '</div> ';
    },

    closeEditor: function()
    {
        this.editor.canvas.shapes.each(function(item) {
            if(item._isEditAvailable()){
                item.creator.tool.editStop();
            }
        });

        this.window.closeEditor();
        this.window = null;
    },

    _showPreviewBlock: function( container )
    {
        container.show();
    },

    onPreviewClick: function()
    {
        if (this.window != null) {
            this.closeEditor();
        }
        this.startEditor();
    },

    /**
     * Set VYA extension image as CPP option image
     *
     */
    setVYAProductImage: function()
    {
        this._setVYAProductImage();

        if (this.window == null) {
            this.option.applyVYA();
        }
    }
});