var Aitcg_View_Abstract = Class.create(
{
    id     : '',
    option : null,
    editor : null,
    preview: null,

    templateSyntax: /(^|.|\r|\n)({{(\w+)}})/,
    templateSettings: null,
    scr: null,

    initialize: function( option ) {
        this.option = option;
        this.config = option.config;
        this.id     = this.config.optionId;
        this.editor = option.editor;
        if(typeof(AitPopupHtml)!= 'undefined') {
            Event.observe(document, 'dom:loaded', function(){
                $$('body')[0].insert( {bottom:AitPopupHtml} );
            });
        }
    },

    /**
     * @abstract
     */
    closeEditor: function(){},

    /**
     * Return an array of data necessary to render an editor
     *
     * @return Object
     */
    getTemplateSetting: function() {
        return this.templateSettings;
    },

    /**
     * Init an array of data necessary to render an editor
     */
    _setTemplateSetting: function()
    {
        var c   = this.config,
            t   = this.config.text,
            scr = this.scr,
            options = {
                full_image: c.productImage.fullUrl,
                rand      : c.rand,
                option_id : this.id,
                scale_image  : c.productImage.scaleImage,

                close_text           : t.close,
                apply_text           : t.apply,
                reset_text           : t.reset,
                cancel_text          : t.cancel,
                save_text            : t.save,
                edit_text            : t.edit,
                required_text        : t.required,
                texttoadd_text       : t.texttoadd,
                addtext_text         : t.addtext,
                pickcolor_text       : t.pickcolor,
                pickcoloroutline_text: t.pickcoloroutline,
                pickcolorshadow_text : t.pickcolorshadow,
                widthoutline_text    : t.widthoutline,
                outline_text         : t.outline,
                shadow_text          : t.shadow,
                shadowalpha_text     : t.shadowalpha,
                shadowoffsetx_text   : t.shadowoffsetx,
                shadowoffsety_text   : t.shadowoffsety,
                addimage_text        : t.addimage,
                addmasks_text        : t.addmasks,
                delmasks_text        : t.delmasks,
                instagram_text       : t.instagram,
                pinterest_text       : t.pinterest,
                svg_text             : t.svg,
                pdf_text             : t.pdf,
                png_text             : t.png,
                font_text            : t.font,
                font_family_text     : t.font_family,
                fontpreview_text     : t.fontpreview,
                scale_text           : t.scale,
                print_text           : t.print_text,
                print_type_top       : t.print_type_top,
                print_type_bottom    : t.print_type_bottom,
                print_type_bg        : t.print_type_bg,
                print_type_mask      : t.print_type_mask,
                print_type_error     : t.print_type_error,

                masks_title         : t.masks_title,
                instagram_title     : t.instagram_title,
                pinterest_title     : t.pinterest_title,
                predefined_title    : t.predefined_title,
                user_title          : t.user_title,
                text_title          : t.text_title,
                under_template_text : t.under_template_text,
                save_title          : t.save_title,
                save_about          : t.save_about,
                buttonHelp          : t.buttonHelp,

                areaSizeX    : c.area.sizeX,
                areaSizeY    : c.area.sizeY,
                areaOffsetX  : c.area.offsetX,
                areaOffsetY  : c.area.offsetY,

                fontOptions       : c.tools.Text.fontOptions,
                fontFamilyOptions : c.tools.Text.fontFamilyOptions,
                empty_img_url     : c.emptyImgUrl,

                instagram_client_id: t.instagram_client_id,
                instagram_redirect_uri: t.instagram_redirect_uri,

                pinterest_client_id: t.pinterest_client_id,
                pinterest_redirect_uri: t.pinterest_redirect_uri
            };

        if (scr) {
            // some window rendering variables for template
            options['img_width']  = scr.curr.width;
            options['img_height'] = scr.curr.height;
            options['width']      = Math.floor(c.area.sizeX * scr.mult) + 'px';
            options['height']     = Math.floor(c.area.sizeY * scr.mult) + 'px';
            options['left']       = Math.max(0, Math.round(c.area.offsetX * scr.mult - 1)) + 'px';
            options['top']        = Math.max(0, Math.round(c.area.offsetY * scr.mult - 1)) + 'px';
        }

        Aitoc_Common_Events.dispatch('aitcg_view_set_template_settings', {view: this, options: options});

        this.templateSettings = options;
    },

    /**
     * Render a bottom control panel of the editor
     *
     * @return string
     */
    _getControlPanelHtml: function()
    {
        if (this.config.editorEnabled) {
            if (mobileMode) {
                return '<div id="aitcg-control-panel" style="width: 100%; text-align: left;">' +
                    '<button class="aitcg-button apply-but" id="submit-editorApply-{{rand}}" title="{{apply_text}}" style="float: initial; max-width: 100%;">{{apply_text}}</button>' +
                    '<button class="aitcg-button reset-but" id="submit-editorReset-{{rand}}" title="{{reset_text}}">{{reset_text}}</button>' +
                    '</div>';
            } else {
                return '<div id="aitcg-control-panel">' +
                    '<button class="aitcg-button apply-but" id="submit-editorApply-{{rand}}" title="{{apply_text}}">{{apply_text}}</button>' +
                    '<button class="aitcg-button reset-but" id="submit-editorReset-{{rand}}" title="{{reset_text}}">{{reset_text}}</button>' +
                    '</div>';
            }
        }
        return '';
    },

    /**
     * Init Apply and Reset buttons events
     */
    initObservers: function()
    {
        if (this.config.editorEnabled) {
            $('submit-editorApply-' + this.config.rand).observe('click', this.submitApply.bindAsEventListener(this));
            $('submit-editorReset-' + this.config.rand).observe('click', this.submitReset.bindAsEventListener(this));
        }
    },

    submitApply: function(event)
    {
        Event.stop(event);
        this.option.apply();
    },

    submitReset: function(event)
    {
        Event.stop(event);
        this.option.reset();
    },

    /**
     * Render editor popup header and
     * toolbox if editor in enabled
     *
     * @return string
     */
    _getToolsHtml: function()
    {
        return this.option.tools.render();
    },

    initPreview: function()
    {
        var scale = this.option.calcScale(),
            elementId = this.config.previewContainer,
            container = $(elementId),
            c      = this.config,
            area   = c.area,
            thumb  = c.productImage.thumb;
        this.previewScale = scale;

        var areaParams = {
            width : Math.round(area.sizeX   * scale),
            height: Math.round(area.sizeY   * scale),
            left  : Math.round(area.offsetX * scale),
            top   : Math.round(area.offsetY * scale)
        };

        var html =
                '<div class="aitraph aitraph-bot"></div>' +
                '<img class="aitcg_preview_bg" src="' + thumb.fullUrl + '" />'+
                '<div class="aitraph aitraph-top"></div>' +
                '<div class="aitcg-overlay" style="width:'+ thumb.sizeX +'px;height:'+thumb.sizeY+'px;"></div>';
        container.update(html);

        this._showPreviewBlock( container );

        container.observe('click', this.onPreviewClick.bind(this));

        var title         = c.editorEnabled ? c.text.thumbTooltipEdit : c.text.thumbTooltipPreview,
            viewIconClass = c.editorEnabled ? '' : 'view-icon';
        container.select('.aitcg-overlay')[0]
            .observe('mouseover', function(){Aitcg.tooltip().update(title).show()})
            .observe('mouseout', function(){Aitcg.tooltip().hide()})
            .addClassName(viewIconClass);


        var styleParams = Aitcg.addPxToValue(areaParams);
        $$('#' + elementId + ' .aitraph').each( function(element) {
            element.setStyle(styleParams);
        });

        Aitoc_Common_Events.dispatch('aitcg_option_preview_create_after_' + this.id, {element: container});

        this.preview = new Aitcg_Editor(this.option);
        this.preview.init(container, Aitcg_Editor.MODE_PREVIEW, true, this.previewScale);
        this.preview.load( $('options_' + this.id).getValue() );
    },

    previewReset: function()
    {
        this.preview.reset();
        this.preview.load( $('options_' + this.id).getValue() );
    },

    /**
     * @abstract
     *
     * @param container
     * @private
     */
    _showPreviewBlock: function( container ){},

    /**
     * @abstract
     */
    onPreviewClick: function(){},

    /**
     * Render some template using current view options
     *
     * @param template String
     * @return string.
     */
    renderTemplate: function( template )
    {
        var tempObj = new Template(template, this.templateSyntax),
            options = this.getTemplateSetting();
        return tempObj.evaluate(options)
    },

    _setVYAProductImage: function()
    {
        this._setTemplateSetting();
        this.initPreview();
    }
});