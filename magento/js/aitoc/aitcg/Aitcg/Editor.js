var Aitcg_Editor = Class.create();
Object.extend(Aitcg_Editor, {
    // constants
    MODE_EDIT   : 0,
    MODE_PREVIEW: 1,
    MODE_SAVE   : 2,

    CONTAINER_ID: 'aitcg_image_container',
    TEMPLATE_ID : 'printable-area-image'
});
Aitcg_Editor.prototype = {
    option     : null,
    config     : null,
    canvas     : null, //editor
    attr       : "height,opacity,r,rotation,src,width,x,y,preserveAspectRatio,matrix,transform".split(","),
    mode       : this.MODE_EDIT,
    noListeners: false,
    scale      : 1,

    /**
     * @param option Object
     */
    initialize: function (option) {
        this.option = option;
        this.config = option.config;
    },

    init: function (element, mode, noListeners, scale) {
        this.mode = mode;
        this.sizeX = Math.round(this.config.area.sizeX * scale);
        this.sizeY = Math.round(this.config.area.sizeY * scale);
        this.scale = scale;
        this.canvas = new Aitcg_Editor_Canvas(this.config, element, this.sizeX, this.sizeY, this.mode == Aitcg_Editor.MODE_EDIT);
        this.noListeners = noListeners;


    },

    /**
     * Create new Raphael Element object by some image data
     *
     * @param element Object
     * @param scale Float
     * @returns Element
     */
    createImageElement: function (element, scale) {
        switch (Raphael.type) {
            case 'SVG':
            case 'VML':
                element.type = 'image';
                element.id = '';
                return this.canvas.createShapeByShapeInfo(element, scale);
            default :
                document.write("Error: undefined image type");
        }
    },

    /**
     * @param shape Object
     * @return {Aitcg_Editor_Canvas_Shape}
     */
    addShape: function (shape) {
        return this.canvas.addShape(shape, true);
    },

    /**
     * Load editor form saved jason data
     *
     * @param data String
     */
    load: function (data, scaleEnable) {
        this.makeActive();

        if (data != '') {
            Aitoc_Common_Events.dispatch('aitcg_editor_load_before_' + this.config.optionId);

            var json = eval("(" + data + ")");
            var pictureData = json.data;
            $(pictureData).each(function (item) {
                this.loadShape(item, this.noListeners, this.scale, scaleEnable);
            }.bind(this));

            Aitoc_Common_Events.dispatch('aitcg_editor_load_after_' + this.config.optionId);
        }
    },

    /**
     * Load certain shape by its saved data
     *
     * @param shape Object
     * @param noListeners Boolean
     * @param scale Float
     */
    loadShape: function (shape, noListeners, scale, scaleEnable) {
        if (!shape || !shape.type || !shape.id) return;

        Aitoc_Common_Events.dispatch('aitcg_editor_load_shape_before_' + this.config.optionId, {
            shape      : shape,
            scale      : scale,
            scaleEnable: scaleEnable
        });

        var creator = null;
        if (shape.creator) {
            creator = shape.creator;
            creator.tool = this.option.tools.getTool(creator.type);
        }

        var proportionScreen = this.option.tools.config.proportionScreen;

        if (proportionScreen != 'undefined' && proportionScreen < 1 && scaleEnable != false) {
            scale *= this.option.tools.config.proportionScreen;
        }

        var newShape = this.canvas.createShapeByShapeInfo(shape, scale, noListeners);

        this.addShape(newShape);
    },

    /**
     * Dump editor data and encode it to JSON format
     *
     * @return string
     */
    save: function (proportionScreen) {
        var canvasShapes = this.canvas.shapes;
        if (proportionScreen < 1 && proportionScreen != 'undefined') {
            canvasShapes.forEach(function (item, i, arr) {
                item.element.matrix.a /= proportionScreen;
                item.element.matrix.b /= proportionScreen;
                item.element.matrix.c /= proportionScreen;
                item.element.matrix.d /= proportionScreen;
                item.element.matrix.e /= proportionScreen;
                item.element.matrix.f /= proportionScreen;
            });
        }

        var canvas = $(this.canvas.shapes).collect(this.dumpShape.bind(this));
        var mirror = [];
        if (this.config.allowUploadUnderTemplate) {
            mirror = $(this.canvas.mirror.shapes).collect(this.dumpMirror.bind(this));
        }
        canvas = canvas.concat(mirror);
        var result = {
            data        : [],
            dataOriginal: [], //for custom popup size
            config      : this.config
        };

        if (canvas.length == 0) {
            return [];// do not save value if no modifications were performed
        }
        for (var i = 0; i < canvas.length; i++) {
            if (typeof canvas[i].type == 'undefined') {
                continue;
            }
            result.data.push(canvas[i]);
        }

        return Object.toJSON(result);
    },

    /**
     * Convert shape info into simpler format before saving
     *
     * @param shape Aitcg_Editor_Canvas_Shape
     * @returns Object
     */
    dumpShape: function (shape) {
        var info = {};
        // reflected shape will be parsed separately
        if (!shape.hasReflection()) {
            info = {
                type   : shape.element.type,
                id     : shape.element.id,
                subtype: shape.element.subtype
            };

            if (shape.creator) {
                info.creator = Object.clone(shape.creator);
                delete info.creator.tool;
                info.creator.isNew = false;
            }

            Aitoc_Common_Events.dispatch('aitcg_editor_dumpshape_before_' + this.config.optionId, {
                info : info,
                shape: shape
            });

            //fix for ie
            info.id++;
            for (var i = 0; i < this.attr.length; i++) {
                var tmp = shape.element.attr(this.attr[i]);
                if (typeof tmp != 'undefined' && tmp !== null) {
                    switch (this.attr[i]) {
                        case 'path':
                            tmp = tmp.toString();
                            break;
                        case 'transform':
                            var matrix = shape.element.matrix.clone(),
                                invert = matrix.invert();
                            matrix.scale(1 / this.scale, 1 / this.scale, invert.x(0, 0), invert.y(0, 0));
                            tmp = matrix.toTransformString();
                            break;
                    }
                    info[this.attr[i]] = tmp;
                }
            }
        }
        return info;
    },

    /**
     * Convert reflected shape info into simpler format before saving
     *
     * @param shape Aitcg_Editor_Canvas_Shape
     * @returns Object
     */
    dumpMirror: function (shape) {
        var info = this.dumpShape(shape);
        info.reflection = 1;
        return info;
    },

    /**
     * Remove all shapes from the related canvas
     *
     * @return {Aitcg_Editor}
     */
    reset: function () {
        this.makeActive();
        this.canvas.clear();
        Aitoc_Common_Events.dispatch('aitcg_editor_reset_after_' + this.config.optionId, {editor: this});
        return this;
    },

    /**
     * TODO: Do we still in need of this method, hm? Commented for now.
     */
    /*
     select: function( element )
     {
     this.canvas.select(element);
     },
     */

    /**
     * Remove selection from current selected shape
     *
     * @return {Aitcg_Editor}
     */
    unselect: function () {
        this.canvas.unselect();
        return this;
    },

    /**
     * Select current editor instance as an active one in the option
     *
     * @return {Aitcg_Editor}
     */
    makeActive: function () {
        this.option.currentEditor = this;
        return this;
    }
};