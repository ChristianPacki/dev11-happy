/**
 * required jscolor
 * required script.aculo.us Builder 
 */
var Aitcg_ColorSet = Class.create
({
    initialize: function(params)
    {
        Object.extend(this, params);
        this._colorContainer = null;
        this.lastColorId = null;
    },

    renderSet: function()
    {
        var colors = this.getColorsArray();
        this._colorContainer = null;
        var table = this.getColorContainer();
        
        var obj = this;
        if(colors !== null )
        {
            colors.each(function(colorCode){
                var colorElement = obj._getNewColorElement({
                    colorCode: colorCode 
                });
                table.appendChild(colorElement);
            });    
        }
        $(this.containerId).insert({top:table});
        this._initColorInput();
    },
    
    _initColorInput: function()
    {
        //add first color from set
        $(this.colorInputId).color.fromString(this.getColorsArray().first());
    },

    _generateColorIdNumber: function()
    {
        if(this.lastColorId == null)
        {
            this.lastColorId = 0;
        }
        else
        {
            this.lastColorId = this.lastColorId +1;
        }
        return this.id +'_'+ this.lastColorId;
    },

    _getNewColorElement: function(params)
    {
        var newNumber = this._generateColorIdNumber();
        var colorElement = Builder.node('div', 
        { 
            id:'aitcg_color_'+newNumber,
            style: 'background-color:'+ params.colorCode + ';cursor:pointer;',
            className: 'aitcg_colorset_color'
        });
        var colorsetObj = this;
        //change color in ColorElement
        colorElement.observe('click',function(){
            $(colorsetObj.colorInputId).color.fromString(params.colorCode);
            var event = new Event("change");
            $(colorsetObj.colorInputId).dispatchEvent(event);
        });
        return colorElement;
    },

    getColorContainer: function()
    {
        if (this._colorContainer == null) {
            this._colorContainer = $(Builder.node('div', {
                id: 'aitcg_colorset_'+this.id,
                className: 'aitcg_colorset'
            }));
        }
        return this._colorContainer;
    },

    getColorsArray: function()
    {
        return this.source.match(/(#[A-F,0-9]{6})/ig);
    }

});