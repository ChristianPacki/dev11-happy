/**
 * Tooltip handler
 */
var Aitcg_Tooltip = Class.create(
{
    tooltip: null,
    id     : 'aitcg-tooltip',
    style  : 'dark',
    canHide: true,
    text   : '',

    initialize: function( style )
    {
        this.style = style || this.style;
        this._build();
    },

    initObservers: function( selector )
    {
        $$(selector).each(function(el) {
            var title = el.title;
            el.title = '';
            el.observe('mouseover', function(){
                Aitcg.tooltip().update(title).show();
            }).observe('mouseout', function(){
                Aitcg.tooltip().update(title).hide();
            });
        });
    },

    /**
     * Build tooltip box and text
     *
     */
    _build: function()
    {
        if (!$(this.id)) {
            this.tooltip = new Element('div', {
                'id'    : this.id,
                'style' : 'display:none;',
                'class' : this.id + '-' + this.style
            });
            $$('body')[0].insert({ bottom: this.tooltip });
        } else {
            this.tooltip = $(this.id);
        }
        this.tooltip.observe('mouseout',  this.mouseOut.bind(this))
                    .observe('mouseover', this.mouseOver.bind(this));
    },

    /**
     * Update tooltip box contents
     *
     * @param text String
     * @return {*}
     */
    update: function( text )
    {
        this.text = text;
        return this;
    },

    _updateText: function()
    {
        this.tooltip.update(this.text);
    },

    /**
     * Hide the tooltip
     *
     * @return {*}
     */
    hide: function()
    {
        setTimeout(function(){
            if (this.canHide) {
                this.tooltip.fade({duration: 0.2, queue: 'end'});
            }
        }.bind(this), 1);
        return this;
    },

    /**
     * Show the tooltip
     *
     * @return {*}
     */
    show: function()
    {
        setTimeout(function(){
            this.tooltip.appear({duration: 0.2, queue: 'end', afterSetup: this._updateText.bind(this)});
        }.bind(this), 1);
        return this;
    },

    mouseOver: function()
    {
        this.canHide = false;
    },

    mouseOut: function()
    {
        this.canHide = true;
        this.hide();
    }
});