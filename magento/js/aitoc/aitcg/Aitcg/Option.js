var Aitcg_Option = Class.create(
{
    id     : null,
    view   : null,
    editor : null,
    config : null,
    tools  : null,
    currentEditor: null,
    origFullUrl: null,// compatibility with VYA extension
    origThumbFullUrl: null,// compatibility with VYA extension
    mult   : 1,
    origFullUrl : null,// compatibility with VYA extension
    origThumbUrl : null,// compatibility with VYA extension

    initialize: function( config )
    {
        this.config = config;
        this.id     = this.config.optionId;

        Aitoc_Common_Events.dispatch('aitcg_option_init', {option: this});

        this.editor = new Aitcg_Editor(this);
        this.tools  = new Aitcg_Editor_Tools(this);
        this.view   = new window['Aitcg_View_' + this.config.viewType]( this );
        Aitcg.optionArray.push(this);
    },

    /**
     * Set VYA extension image as CPP option image
     *
     * @param url String
     * @param thumbUrl String
     */
    setVYAProductImage: function(url, thumbUrl, useDefault, leftOffset, topOffset)
    {
        this._storeDefaultImage();

        if(useDefault == true){
            url = this.origFullUrl;
            thumbUrl = this.origThumbFullUrl
        }

        this.config.productImage.fullUrl = url;
        this.config.productImage.thumb.fullUrl = thumbUrl;

        this.view.setVYAProductImage();

        this._applyVYAOffsets(leftOffset, topOffset);
    },

    _applyVYAOffsets: function(leftOffset, topOffset){
        if(leftOffset > 0){
            $$('#preview_container'+this.id+' .aitcg_preview_bg').each(function(el){
                el.style.marginLeft = "-"+leftOffset+"px";                
            });
        }
        if(topOffset > 0){
            $$('#preview_container'+this.id+' .aitcg_preview_bg').each(function(el){
                el.style.marginTop = "-"+topOffset+"px";
            });
                        $$('#preview_container'+this.id+' .aitraph-top').each(function(el){
                el.style.marginTop = topOffset+"px";
            });
            $$('#preview_container'+this.id+' .aitraph-bot').each(function(el){
                el.style.marginTop = topOffset+"px";
            });
            $$('#preview_container'+this.id+' .aitcg-overlay').each(function(el){
                el.style.marginTop = topOffset+"px";
            });
            $$('.aitcg_margin').each(function(el){
                el.style.marginTop = "-"+topOffset+"px";
                el.style.zIndex = "5";
                el.style.position = "relative";
            });
        }
    },

    _storeDefaultImage: function()
    {
        if(this.origFullUrl == null){
            this.origFullUrl = this.config.productImage.fullUrl;
        }
        if(this.origThumbFullUrl == null){
            this.origThumbFullUrl = this.config.productImage.thumb.fullUrl;
        }
    },

    /**
     * Apply changes made to editor
     */
    applyVYA: function()
    {
        // if editor was not loaded yet
        if(this.editor.canvas == null){
            return;
        }

        var value = this.editor.save(),
        optionInput = $('options_' + this.id);
        optionInput.setValue((value == '[]') ? '' : value);
    },

    /**
     * Return scale multiplier of the thumbnail comparing to the full image
     *
     * @return {float}
     */
    calcScale: function()
    {
        return 1 / Math.max(this.config.productImage.sizeX / this.config.productImage.thumb.sizeX, this.config.productImage.sizeY / this.config.productImage.thumb.sizeY);
    },

    /**
     * Render terms agreement popup
     *
     * @return string
     */
    _getConfirmBoxTemplate: function()
    {
        return  '<div id="message-popup-window-mask" onclick="opCimage{{rand}}.agree(false);"></div>' +
                '<div id="message-popup-window" class="aitcg-popup">' +
                    '<div class="title">' +
                        '<div class="close-but" onclick="opCimage{{rand}}.agree(false);" ></div>' +
                        '{{title}}' +
                    '</div>' +
                    '<div class="text">{{text}}</div>' +
                    '<div class="buttons">' +
                        '<button class="aitcg-button agree-but" onclick="opCimage{{rand}}.agree(true);">{{agree_text}}</button>' +
                        '<button class="aitcg-button disagree-but" onclick="opCimage{{rand}}.agree(false);">{{disagree_text}}</button>' +
                    '</div>' +
                '</div>';
    },

    checkConfirmBox: function()
    {
        if (typeof(this.config.text.confirm) == 'undefined' || this.config.text.confirm == '') {
            return false;
        }
        this.window = new Aitcg_Popup(this._getConfirmBoxTemplate(), {
            full_image   : this.config.productImage.fullUrl,
            rand         : this.config.rand,
            option_id    : this.id,
            close_text   : this.config.text.close,
            agree_text   : this.config.text.agree,
            disagree_text: this.config.text.disagree,
            text         : this.config.text.confirm,
            title        : this.config.text.confirm_title
        });
        this.window.showTextWindow();
        return false;
    },

    /**
     * Set confirmation status and close confirmation window
     *
     * @param value Boolean
     */
    agree: function( value ) {
        $('options_' + this.id + '_checkbox').checked = value;
        this.window.closeEditor();
    },

    /**
     * Reset all changes made to the editor AFTER the last `apply` operation
     */
    reset: function()
    {
        if (!window.confirm(this.config.text.areYouSure)) {
            return;
        }
        this.editor.reset();
        this.editor.load( $('options_' + this.id).getValue() );
    },

    /**
     * Apply changes made to editor and switch to preview mode
     */
    apply: function()
    {
        Aitcg.showLoader();

        this.editor.unselect();

        var value,
            valueOriginal;

        value = this.editor.save();

        optionInput = $('options_' + this.id);

        if (this.config.proportionScreen < 1 && this.config.proportionScreen != 'undefined') {
            valueOriginal = this.editor.save(this.config.proportionScreen);
            optionInput.setValue((value == '[]') ? '' : valueOriginal);
        } else {
            optionInput.setValue((value == '[]') ? '' : value);
        }

        Aitoc_Common_Events.dispatch('aitcg_option_apply_save_after_' + this.id, {value: value, optionInput: optionInput});

        if (!this.config.optionIsRequired && this.config.checkboxEnabled) {
            if (optionInput.getValue()) {
                $('options_' + this.id + '_checkbox').addClassName('required-entry');
            } else {
                $('options_' + this.id + '_checkbox').removeClassName('required-entry');
            }
        }

        this.view.previewReset();
        opConfig.reloadPrice();
        this.closeEditor();
        Aitcg.hideLoader();
    },

    /**
     * Return an instance of current editor.
     * It could be the main editor, the preview, the hidden printable version and so on.
     *
     * @returns {Aitcg_Editor}
     */
    getCurrentEditor: function()
    {
        return this.currentEditor;
    },

    /**
     * Close the editor using an appropriate view method
     */
    closeEditor: function()
    {
        this.view.closeEditor();
    }
});