/**
 * Main canvas class and wrapper for Raphael.
 */
var Aitcg_Editor_Canvas = Class.create( Aitcg_Editor_Canvas_Abstract, {
    type  : 'main',
    mirror: null, // Aitcg_Editor_Canvas_Mirror

    initialize: function( $super, config, elem, width, height, isEditable )
    {
        $super(config, elem.down('.aitraph-top'), width, height, isEditable);
        if (this.config.allowUploadUnderTemplate) {
            this.mirror = new Aitcg_Editor_Canvas_Mirror( this, elem, width, height);
        }
    }
});
Object.extend(Aitcg_Editor_Canvas, {
    // constants
    RAPH_TOP_ID_SUFFIX: 'raphTop_',
    RAPH_BOT_ID_SUFFIX: 'raphBot_'
});