/**
 * Container and builder for all tools 
 */
var Aitcg_Editor_Tools = Class.create();

Object.extend(Aitcg_Editor_Tools, {
    STYLE_ICONS    : 'icons',
    STYLE_ACCORDION: 'accordion'
});

Aitcg_Editor_Tools.prototype = {
    template      : '',
    templateSyntax: /(^|.|\r|\n)({{(\w+)}})/,
    tools         : null,
    config        : null,
    option        : null,
    firstTool     : null,

    /**
     * @param option Aitcg_Option
     */
    initialize: function(option )
    {
        this.tools = {};
        this.option = option;
        this.config = option.config;

        // Check which tools are enabled and create instantiate appropriate objects
        for (var toolName in this.config.tools.use) {
            if (this.config.tools.use[toolName]) {
                this.tools[toolName] = new window['Aitcg_Editor_Tool_' + toolName]({
                    tools  : this,
                    config : this.config.tools[toolName],
                    toolKey: toolName
                });
                if (!this.firstTool && this.templateAllowed(toolName) && this.tools[toolName].renderable) {
                    this.firstTool = this.tools[toolName];
                }
            }
        }
    },
    
    /**
     * Build template to be rendered
     *
     * @return string
     */
    _buildTemplate: function()
    {
        var template;
        if (mobileMode) {
            template = '<div id="aitcg-toolbox-{{rand}}" class="aitcg-toolbox aitcg-toolbox-' + this.config.toolboxStyle + '" style="float: left">';
        } else {
            template = '<div id="aitcg-toolbox-{{rand}}" class="aitcg-toolbox aitcg-toolbox-' + this.config.toolboxStyle + '">';
        }

        switch (this.config.toolboxStyle) {
            case Aitcg_Editor_Tools.STYLE_ACCORDION:
                for (var toolName in this.tools) {
                    template += this.tools[toolName].render().join('');
                }
            break;
            case Aitcg_Editor_Tools.STYLE_ICONS:
                var titles = '', bodies = '', data;
                for (var toolName in this.tools) {
                    data = this.tools[toolName].render();
                    titles += data[0];
                    bodies += data[1];
                }
                template += '<div class="tools-icons aitcg-'+ this.config.toolsIconsStyle +'">' + titles + '</div>' +
                            '<div class="tools-bodies">' + bodies + '</div>';
            break;
        }
        template += '</div>';
        return template;
    },

    /**
     * Check if certain tool can be rendered
     *
     * @param toolName String
     * @returns boolean
     */
    templateAllowed: function(toolName)
    {
        return (this.config.editorEnabled || this.config.tools.global.indexOf(toolName) != -1);
    },
    
    /**
     * Render toolbox including all tools
     *
     * @return string
     */
    render: function()
    {
        return this._buildTemplate();
    },

    /**
     * Is certain tool enabled
     *
     * @param toolName Sting
     * @return {boolean}
     */
    isToolEnabled: function( toolName )
    {
        return typeof(this.tools[toolName]) != 'undefined';
    },

    /**
     * Get a certain tool by its name if a tool is available
     *
     * @param toolName String
     * @return Aitcg_Editor_Tool_Abstract|null
     */
    getTool: function( toolName )
    {
        if (this.isToolEnabled(toolName)) {
            return this.tools[toolName];
        }
        return null;
    },
    
    /**
     * Init all enabled tools' observers
     *
     * @return {Aitcg_Editor_Tools}
     */
    initTools: function()
    {
        for (var tool in this.tools) {
            this.tools[tool].initObservers();
        }

        if (this.config.toolboxStyle == Aitcg_Editor_Tools.STYLE_ICONS) {
            this.firstTool.toggleTool();
        }
        return this;
    },

    /**
     * Init all enabled tools data
     *
     * @return {Aitcg_Editor_Tools}
     */
    initToolsData: function()
    {
        for (var tool in this.tools) {
            this.tools[tool].initData();
        }
        return this;
    }
};