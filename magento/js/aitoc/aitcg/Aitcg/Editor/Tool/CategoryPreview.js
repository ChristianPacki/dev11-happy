/**
 * Displays categories with previews for predefined images and image masks
 */
var Aitcg_Editor_Tool_CategoryPreview = Class.create( Aitcg_Editor_Tool_Abstract,
{
    requestsCache: null,
    currentCategoryId: 0,

    initialize: function( $super, params )
    {
        $super(params);
        this.requestsCache = [];
    },

    _request: function ( $super,  params )
    {
        this.currentCategoryId = params.category_id;
        // make request to get images list only if response haven't been cached previously
        if (typeof this.requestsCache[params.category_id] == 'undefined') {
            $super(params);
        } else {
            this._requestSuccess( this.requestsCache[this.currentCategoryId] );
        }
    },

    _requestSuccess: function( transport )
    {
        // cache response
        this.requestsCache[this.currentCategoryId] = transport;

        var response = eval("("+transport.responseText+")");
        $(this.config.galleryIdPrefix + response.rand).update(response.images);
        this.initObservers();
        Aitcg.hideLoader();
    },
    
    /**
     * Init event observers on gallery images
     */
    initObservers: function()
    {
        var thumbs = $$('#' + this.config.galleryIdPrefix + this.tools.config.rand + ' img');
        thumbs.each(function( thumb ){
            thumb.observe('click', function(e){
                if(!$(this).hasClassName('selected')) {
                    $(this).previous().click();
                    $(this).up(1).descendants().invoke('removeClassName', 'selected');
                    $(this).addClassName('selected');
                    new Effect.Pulsate(this, {pulses: 1, duration: 0.25});
                }
            }).observe('dblclick', function(e){
                this.parent.submit();
            }.bind(this));
        }.bind(this));
    },
    
    /**
     * Reload gallery with given id
     */
    refresh: function()
    {
        var category_id = $(this.config.selectorIdPrefix + this.tools.config.rand).getValue();
        if (category_id > 0) {
            this._request({
                category_id: category_id,
                rand: this.tools.config.rand
            })
        } else {
            $(this.config.galleryIdPrefix + this.tools.config.rand).update();
        }
    }
});