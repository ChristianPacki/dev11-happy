/**
 * Social widgets selection class
 */
var Aitcg_Editor_Tool_Social = Class.create( Aitcg_Editor_Tool_Abstract,
{
    optionId        : null,
    _observersInited: false, //we should init observers only once for social widget
    renderable      : false,
    
    initialize: function( $super, params )
    {
        $super(params);

        this.optionId = this.tools.config.optionId;

        Aitoc_Common_Events.addObserver('aitcg_option_apply_save_after_' + this.optionId, this.onOptionApplySaveAfter.bind(this));
        Aitoc_Common_Events.addObserver('aitcg_editor_dumpshape_before_' + this.optionId, this.onEditorDumpshapeBefore.bind(this));
        Aitoc_Common_Events.addObserver('aitcg_option_preview_create_after_' + this.optionId, this.initWidget.bind(this));
    },
    
    /**
     * override parent, do not create tool box tab
     *
     * @return Array
     */
    render: function()
    {
        return ['',''];
    },

    /**
     * Should be empty to prevent default observers from binding
     */
    initObservers: function(){},

    onOptionApplySaveAfter : function( params )
    {
        var value = params.value;
        if(value != '[]') {
            this.hideSocialWidgets();
            //this.editor.socialWidgetsReservedImgId = this.socialWidgetsReservedImgId;//send reservedImgId to editor object
            this.tools.option.editor.socialWidgetsReservedImgId = this.config.reservedImgId;//send reservedImgId to editor object
            $('socialWidgetsTip'+this.optionId).setStyle({display:'block'});
        }
        else {
            $('socialWidgetsTip'+this.optionId).setStyle({display:'none'});
        }
        if($('imgSizeError'+this.optionId)) {
            $('imgSizeError'+this.optionId).setStyle({
                   display:'none'
                });
        }

        value = this.tools.option.editor.save();
        params.optionInput.setValue((value == '[]') ? '' : value);
    },
    
    onEditorDumpshapeBefore: function( params )
    {
        params.info.social_widgets_reserved_img_id = this.config.reservedImgId;        
    },
    
    createImage : function()
    {   
        this._showAjaxLoader(true);

        var saveTool = this.tools.getTool('Save');
        if (saveTool) {
            saveTool.processPng( function() {
                    var dataUrl = saveTool.getCanvas().toDataURL();
                    this.reservedImgId = this.config.reservedImgId;
                    this.createImgUrl = this.config.imgCreatePath;
                    this._ajaxCreateImg(dataUrl, 0);
                }.bind(this),
                {scale: 1, printTypes: {'all': 1}},
                true
            );
        }
        return false;
    },
    
    _processAjaxResponse: function( response )
    {
        if (200 == response.status) {
            if (response.responseText == 'success') {
                $('socialWidgetsTip' + this.optionId).setStyle({ display: 'none' });
                this.showSocialWidgets();
            }  else if (response.responseText == 'imgSizeError') {
                $('imgSizeError'     + this.optionId).setStyle({ display: 'block' });
                $('socialWidgetsTip' + this.optionId).setStyle({ display: 'block' });
                this.hideSocialWidgets();
            } else {
                $('socialWidgetsTip' + this.optionId).setStyle({ display: 'block' });
                this.hideSocialWidgets();
            }
        } else {
            $('socialWidgetsTip' + this.optionId).setStyle({ display: 'block' });
            this.hideSocialWidgets();
        }

        this._showAjaxLoader(false);
    },

    initWidget: function()
    {
        if ($('socialWidgetsTip' + this.optionId)) {
            if (!this._observersInited) {
                $$('#socialWidgetsTip' + this.optionId + ' a')[0].observe('click', this.createImage.bindAsEventListener(this));
                this._observersInited = true;
            }
            if (this.config.allowChecking) {
                new Ajax.Request(this.config.wasCreatedUrl,
                    {
                        method: 'post',
                        parameters: {sharedImgId : this.config.reservedImgId},
                        onComplete: function(response) {
                            this._processAjaxResponse(response);
                        }.bind(this)
                    });
            }
        }
    },

    hideSocialWidgets : function()
    {
        $ ('fbaitcg' + this.optionId).setStyle({
            visibility: 'hidden'
        });
        
        if ($('emailToFriend' + this.optionId)){
            $('emailToFriend' + this.optionId).setStyle({
                display: 'none'
            });
        }
    },

    showSocialWidgets : function()
    {
        this._showAjaxLoader(false);

        if ($('imgSizeError'+this.optionId)) {
            $('imgSizeError'+this.optionId).setStyle({ display: 'none' });
        }

        $('fbaitcg' + this.optionId).setStyle({
                visibility: 'visible'
        });

        // to not create more than 1 google share button on multiple click 'submit' button
        // we add checking if google share button was already created
        if (document.getElementById('gaitcgWrapper' + this.optionId).innerHTML.indexOf('plusone.google.com') == -1) {
            document.getElementById('gaitcgWrapper' + this.optionId).innerHTML = '<div style="display:inline;" class="g-plus" id="gaitcg' +
                this.optionId + '" data-annotation="none" data-action="share" data-href="' + this.config.imgViewUrl + '" ></div>';

            var po = document.createElement('script');
                po.type  = 'text/javascript';
                po.async = true;
                po.src   = 'https://apis.google.com/js/plusone.js';
            var s = document.getElementsByTagName('script')[0];
            s.parentNode.insertBefore(po, s);
        }

        if ($('emailToFriend'+this.optionId)) {
            $('emailToFriend'+this.optionId).setStyle({
                display: 'inline'
            });
        }
    },

    _showAjaxLoader: function( show )
    {
        var loader = $('socialButtonsLoader' + this.optionId),
            loaderStyle = show ? 'inline-block' : 'none';

        if (loader) {
            loader.setStyle({
                display: loaderStyle
            });
        }
    }

});