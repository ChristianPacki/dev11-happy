/**
 * Predefined images selection class
 */
var Aitcg_Editor_Tool_PredefinedImage = Class.create( Aitcg_Editor_Tool_Abstract,
{
    titleTemplate  : 'predefined_title',
    categoryPreview: null,

    initialize: function( $super, params )
    {
        $super(params);
        this.template =
            '<div id="aitcg-tool-' + this.toolKey + '" class="tool-body">' +
                '<div style="position: relative; height: 40px;">'+
                    '<select id="category-selector-{{rand}}">' +
                        this.config.options +
                    '</select>' +
                    '<span id="custom_select2"><span id="select-title">Select category</span><div class="button"></div><div class="options"></div></span>' +
                    '<br />' +
                '</div>'+
                '<div class="category-previews"><div class="container" id="predefined-images{{rand}}"></div></div>' +
                '<div style="display:none;" id="add_predefined_{{rand}}_error" class="validation-advice">{{required_text}}</div>' +
                this._getUnderTemplateSelectHtml() +
                '<div class="buttons">' +
                    '<button class="aitcg-button" type="button" id="submit-predefined-image-{{rand}}">{{addimage_text}}</button>' +
                '</div>' +
            '</div>';
        this.categoryPreview = new Aitcg_Editor_Tool_CategoryPreview({
            parent: this,
            tools : this.tools,
            config: {
                requestUrl      : this.config.categoryPreviewUrl,
                galleryIdPrefix : 'predefined-images',
                selectorIdPrefix: 'category-selector-'
            }
        });
    },

    _requestSuccess: function( transport )
    {
        var response = eval("("+transport.responseText+")");
        this.loadUploadedImage(response.url);
        Aitcg.hideLoader();
    },
    
    initObservers: function( $super )
    {
        if ( this.tools.config.editorEnabled ) {
            $super();
            $('category-selector-' + this.tools.config.rand).observe('change', this.categoryPreview.refresh.bindAsEventListener(this.categoryPreview));
            $('submit-predefined-image-' + this.tools.config.rand).observe('click', this.submit.bindAsEventListener(this));
            $('custom_select2').observe('click', function(element) {
            	var select = this.parentElement.children[0];
            	var event  = document.createEvent('MouseEvents');
                event.initMouseEvent('mousedown', true, true, window);
                select.dispatchEvent(event);
            });

            this.renderSelect();

            $('category-selector-' + this.tools.config.rand).parentElement.children[1].observe('click', function(element) {
                var display = this.children[2].getStyle('display');
                if (display == 'none') {
                    this.children[2].setStyle({
                        display: 'block'
                    });
                } else {
                    this.children[2].setStyle({
                        display: 'none'
                    });
                }
            });
        }  
    },

    renderSelect: function()
    {
        var self = this;
        var select = $$('#category-selector-' + this.tools.config.rand + ' option');

        var customSelect = $('category-selector-' + this.tools.config.rand).parentElement.children[1].children[2];

        select.forEach(function(option) {
            if (option.getAttribute('value') != '') {
                var temp = new Element('div', {value: option.getAttribute('value'), class: 'option'});
                temp.insert(option.innerHTML);

                customSelect.insert({
                    bottom: temp
                });

                temp.observe('click', function(element) {
                    this.parentElement.parentElement.parentElement.children[0].value = this.readAttribute('value'); //original select
                    this.parentElement.parentElement.children[0].innerHTML  = this.innerHTML; //title
                    self.categoryPreview.refresh();
                });
            }
        });

    },
    
    /**
     * Submit an image chosen by user to the server
     */
    submit: function()
    {
        $('add_predefined_' + this.tools.config.rand + '_error').hide();

        var selection = document.getElementsByName('predefined-image' + this.tools.config.rand);
        for (var i = 0; i < selection.length; i++) {
            if (selection[i].checked == true) {
                var radios = selection[i];
            }
        }
        
        if (typeof(radios) == 'undefined') {
            $('add_predefined_' + this.tools.config.rand + '_error').show();
            return;
        }        
    
        this._request({
            img_id: radios.getValue()
        })
    }
});