/**
 * Customer image upload tool
 */
var Aitcg_Editor_Tool_UserImage = Class.create(Aitcg_Editor_Tool_Abstract,
    {
        titleTemplate: 'user_title',
        _saveAllowed : true,

        initialize                : function ($super, params) {
            $super(params);
            this.template =
                '<div id="aitcg-tool-' + this.toolKey + '" class="tool-body">' +
                '<input type="file" id="add_image_{{rand}}" name="new_image" />' +
                '<br />' +
                '<div style="display:none;" id="add_image_{{rand}}_error" class="validation-advice">{{required_text}}</div>' +
                this._getUnderTemplateSelectHtml() +
                '<div class="buttons">' +
                '<button class="aitcg-button" type="button" id="submit-user-image-{{rand}}">{{addimage_text}}</button>' +
                '</div>' +
                this.instagramAndPinterestBlock($super) +
                '</div>';
        },
        instagramAndPinterestBlock: function ($super) {
            if (this.tools.config.tools.UserImage.Instagram == undefined) {
                return '';
            }
            var useInstagram = this.tools.config.tools.UserImage.Instagram.use;
            var usePinterest = this.tools.config.tools.UserImage.Pinterest.use;
            if (!useInstagram && !usePinterest) {
                return '';
            }
            var template = '';
            if (useInstagram) {
                template +=
                    '<input type="hidden" id="instagram_token" value="" />';
            }
            if (usePinterest) {
                template +=
                    '<input type="hidden" id="pinterest_token" value="" />';
            }
            template += '<div id="socialblock">';
            template += '<div id="getphotos">';
            if (useInstagram) {
                template += '<div class="get-photos-button" id="get-instagram-photos"><div class="icon"></div>Browse your Instagram <span>(linked)</span></div>';
                template += '<div class="category-previews"><div class="container" id="instagram-images{{rand}}"></div></div>';
            }
            if (usePinterest) {
                template += '<div class="get-photos-button" id="get-pinterest-photos"><div class="icon"></div>Browse your Pinterest <span>(linked)</span></div>';
                template += '<div class="category-previews"><div class="container" id="pinterest-images{{rand}}"></div></div>';
            }
            template += '</div>';
            template += '<div id="login">Link your social accounts';
            if (useInstagram) {
                template += '<div id="instagram-login"><div class="icon"></div>Instagram</div>';
            }
            if (usePinterest) {
                template += '<div id="pinterest-login"><div class="icon"></div>Pinterest</div>';
            }
            template += '</div>';
            template += '</div>';
            return template;
        },


        initObservers: function ($super) {
            if (this.tools.config.editorEnabled) {
                $super();
                $('submit-user-image-' + this.tools.config.rand).observe('click', this.submit.bindAsEventListener(this));

                var useInstagram = this.tools.config.tools.UserImage.Instagram.use;
                var usePinterest = this.tools.config.tools.UserImage.Pinterest.use;

                if (useInstagram) {
                    $('instagram-login').hide();
                    $('get-instagram-photos').hide();
                    this.getInstagramUserToken();
                    $('instagram-login').observe('click', this.logInInstagram.bindAsEventListener(this));
                    $('get-instagram-photos').observe('click', this.getInstagramUserPhotos.bindAsEventListener(this));
                }

                if (usePinterest) {
                    $('pinterest-login').hide();
                    $('get-pinterest-photos').hide();
                    this.getPinterestUserToken();
                    $('pinterest-login').observe('click', this.logInPinterest.bindAsEventListener(this));
                    $('get-pinterest-photos').observe('click', this.getPinterestUserPhotos.bindAsEventListener(this));
                }
            }
        },

        refreshLogButtons: function () {
            var useInstagram = this.config.Instagram.use;
            var usePinterest = this.config.Pinterest.use;

            if (!useInstagram && !usePinterest) {
                $('socialblock').hide();
            }

            if (($('instagram-login').getStyle('display') === 'none') && ($('pinterest-login').getStyle('display') === 'none')) {
                $('login').hide();
            } else {
                $('login').show();
            }
        },

        getInstagramUserToken: function ($super) {
            var self = this;
            new Ajax.Request(this.config.Instagram.getUserToken, {
                onSuccess: function (transport) {
                    var response = eval("(" + transport.responseText + ")");
                    $('instagram_token').value = response.token;
                    if ($('instagram_token').value != "") {
                        $('instagram-login').hide();
                        $('get-instagram-photos').show();
                    } else {
                        $('instagram-login').show();
                        $('get-instagram-photos').hide();
                    }
                    this.refreshLogButtons();
                }
            });
        },

        getPinterestUserToken: function ($super) {
            var self = this;
            new Ajax.Request(this.config.Pinterest.getUserToken, {
                onSuccess: function (transport) {
                    var response = eval("(" + transport.responseText + ")");
                    $('pinterest_token').value = response.token;
                    if ($('pinterest_token').value != "") {
                        $('pinterest-login').hide();
                        $('get-pinterest-photos').show();
                    } else {
                        $('pinterest-login').show();
                        $('get-pinterest-photos').hide();
                    }
                }
            });
        },

        logInInstagram: function ($super) {
            var self = this;
            var instagramUrl = 'https://api.instagram.com/oauth/authorize/?client_id=' +
                this.config.Instagram.instagram_client_id +
                '&redirect_uri=' + this.config.Instagram.instagram_redirect_uri + '&response_type=code';

            var childId = window.open(instagramUrl, "Log in", "width=600,height=600");

            setTimeout(function () {
                if (childId.closed)
                    self.getInstagramUserToken();
                else
                    setTimeout(arguments.callee, 1);
            }, 1);
        },

        logInPinterest: function ($super) {
            var self = this;

            var pinterestUrl = 'https://api.pinterest.com/oauth/?response_type=code&redirect_uri=' +
                this.config.Pinterest.pinterest_redirect_uri +
                '&client_id=' +
                this.config.Pinterest.pinterest_client_id +
                '&scope=read_public,write_public&state=768uyFys';

            var childId = window.open(pinterestUrl, "Log in", "width=600,height=600");

            setTimeout(function () {
                if (childId.closed)
                    self.getPinterestUserToken();
                else
                    setTimeout(arguments.callee, 1);
            }, 1);
        },

        getInstagramUserPhotos: function ($super) {
            Aitcg.showLoader();
            var self = this;
            var userToken = $('instagram_token').value;
            var photos = [];
            new Ajax.Request(this.config.Instagram.getInstagramPhotos, {
                method    : 'post',
                parameters: {token: userToken},
                onSuccess : function (transport) {
                    var data = eval("(" + transport.responseText + ")").photos;
                    self.insertInstagramPhotos(data);
                }
            });
        },

        getPinterestUserPhotos: function ($super) {
            Aitcg.showLoader();
            var self = this;
            var userToken = $('pinterest_token').value;
            var photos = [];
            new Ajax.Request(this.config.Pinterest.getPinterestPhotos, {
                method    : 'post',
                parameters: {token: userToken},
                onSuccess : function (transport) {
                    var data = eval("(" + transport.responseText + ")").photos;
                    self.insertPinterestPhotos(data);
                }
            });
        },

        insertInstagramPhotos: function (data) {
            var self = this;
            var photos = [];
            var gallery = $('instagram-images' + self.tools.config.rand);
            gallery.innerHTML = '';

            data.forEach(function (item, i, arr) {
                photos[i] = item.images.standard_resolution.url;
            });

            photos.forEach(function (src, i, arr) {
                var element = '<div>' +
                    '<input type="radio" value="' + src + '" name="instagram-image' + self.tools.config.rand + '">' +
                    '<img src="' + src + '">' +
                    '</div>';
                gallery.insert(element);
            });

            Aitcg.hideLoader();

            var thumbs = $$('#instagram-images' + this.tools.config.rand + ' img');
            thumbs.each(function (thumb) {
                thumb.observe('click', function (e) {
                    if (!$(this).hasClassName('selected')) {
                        $(this).previous().click();
                        $(this).up(1).descendants().invoke('removeClassName', 'selected');
                        $(this).addClassName('selected');
                        new Effect.Pulsate(this, {pulses: 1, duration: 0.25});
                    }
                }).observe('dblclick', function (e) {
                    self.submitSocialNetworkImage('instagram');
                }.bind(this));
            }.bind(this));

        },

        insertPinterestPhotos: function (data) {
            var self = this;
            var photos = [];
            var gallery = $('pinterest-images' + self.tools.config.rand);
            gallery.innerHTML = '';

            data.forEach(function (item, i, arr) {
                photos[i] = item.image.original.url;
            });

            photos.forEach(function (src, i, arr) {
                var element = '<div>' +
                    '<input type="radio" value="' + src + '" name="pinterest-image' + self.tools.config.rand + '">' +
                    '<img src="' + src + '">' +
                    '</div>';
                gallery.insert(element);
            });

            Aitcg.hideLoader();

            var thumbs = $$('#pinterest-images' + this.tools.config.rand + ' img');
            thumbs.each(function (thumb) {
                thumb.observe('click', function (e) {
                    if (!$(this).hasClassName('selected')) {
                        $(this).previous().click();
                        $(this).up(1).descendants().invoke('removeClassName', 'selected');
                        $(this).addClassName('selected');
                        new Effect.Pulsate(this, {pulses: 1, duration: 0.25});
                    }
                }).observe('dblclick', function (e) {
                    self.submitSocialNetworkImage('pinterest');
                }.bind(this));
            }.bind(this));

        },

        submitSocialNetworkImage: function (network) {
            var selection = '';
            if (network == 'instagram') {
                selection = document.getElementsByName('instagram-image' + this.tools.config.rand);
            } else if (network == 'pinterest') {
                selection = document.getElementsByName('pinterest-image' + this.tools.config.rand);
            }

            for (var i = 0; i < selection.length; i++) {
                if (selection[i].checked == true) {
                    var radios = selection[i];
                }
            }

            if (typeof(radios) == 'undefined') {
                $('add_predefined_' + this.tools.config.rand + '_error').show();
                return;
            }

            this._requestSocial({
                img_url: radios.getValue()
            })
        },

        _requestSocial: function (params) {
            Aitcg.showLoader();
            this.lastRequestParams = params;
            new Ajax.Request(this.config.Instagram.requestUrl,
                {
                    method    : 'post',
                    parameters: params,
                    onSuccess : this._requestSuccess.bind(this)
                });
        },

        _requestSuccess: function (transport) {
            var response = eval("(" + transport.responseText + ")");
            this.loadUploadedImageSocial(response.url);
            Aitcg.hideLoader();
        },

        /**
         * Submit customer image to the server
         */
        submit: function () {
            var id = 'add_image_' + this.tools.config.rand;

            var blackAndWhite = this.tools.tools.Save.config.isblackwhite;

            if (!$(id).value) {
                $(id + '_error').show();
                return;
            }
            $(id + '_error').hide();

            Aitcg.showLoader();
            AIM.upload(this.config.requestUrl, id, {
                onComplete      : this.loadUploadedImage.bind(this),
                useblackAndWhite: blackAndWhite
            });
        },

        /**
         * @override
         */
        loadUploadedImage      : function (url) {
            if (!url.error) {
                var img = $$('.techimg')[0];
                $$('.techimg').each(function (elem) {
                    if (elem.parentNode.visible()) {
                        img = elem;
                    }
                });
                img.onload = function (e) {
                    this.addImage(Aitcg.getEventTarget(e));
                    Aitcg.hideLoader();
                }.bind(this);
                img.src = url.src;
            } else {
                Aitcg.hideLoader();
                alert(url.error);
            }
        },
        loadUploadedImageSocial: function (url) {
            var img = $$('.techimg')[0];
            $$('.techimg').each(function (elem) {
                if (elem.parentNode.visible()) {
                    img = elem;
                }
            });

            img.onload = function (e) {
                this.addImage(Aitcg.getEventTarget(e));
            }.bind(this);
            img.src = url;
        },

        decorateSaveLink: function (linkHtml, params) {
            var text_config = this.tools.config.text,
                htmlBefore = text_config.save_type_image + ' ';
            return htmlBefore + linkHtml;
        }
    });