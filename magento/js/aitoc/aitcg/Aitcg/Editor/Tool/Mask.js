/**
 * Masks selection class
 */
var Aitcg_Editor_Tool_Mask = Class.create(Aitcg_Editor_Tool_Abstract,
    {
        titleTemplate             : 'masks_title',
        categoryPreview           : null,
        maskCreated               : 0,
        maskDelete                : 0,
        getMaskUrl                : 0,
        delMaskUrl                : 0,
        shapeMask                 : 0,
        useragent                 : navigator.userAgent,
        initialize                : function ($super, params) {
            $super(params);
            this.template =
                '<div id="aitcg-tool-' + this.toolKey + '" class="tool-body">' +
                '<div style="position: relative; height: 40px;">' +
                '<select id="masks-category-selector-{{rand}}">' +
                this.config.options +
                '</select>' +
                '<span id="custom_select3"><span id="select-title">Select category</span><div class="button"></div><div class="options"></div></span>' +
                '</div>' +
                '<div class="category-previews"><div class="container" id="masks{{rand}}"></div></div>' +
                '<div style="display:none;" id="add_masks_{{rand}}_error" class="validation-advice">{{required_text}}</div>' +
                '<div class="buttons">' +
                '<button class="aitcg-button" type="button" id="submit-add-mask-{{rand}}">{{addmasks_text}}</button>' +
                '<button class="aitcg-button" type="button" id="submit-remove-mask-{{rand}}" style="display:none;">{{delmasks_text}}</button>' +
                '</div>' +
                '</div>';
            this.categoryPreview = new Aitcg_Editor_Tool_CategoryPreview({
                parent: this,
                tools : this.tools,
                config: {
                    requestUrl      : this.config.categoryPreviewUrl,
                    galleryIdPrefix : 'masks',
                    selectorIdPrefix: 'masks-category-selector-'
                }
            });
            var optionId = this.tools.config.optionId;
            Aitoc_Common_Events.addObserver('aitcg_editor_load_before_' + optionId, this.onEditorLoadBefore.bind(this));
            Aitoc_Common_Events.addObserver('aitcg_editor_load_after_' + optionId, this.onEditorLoadAfter.bind(this));
            Aitoc_Common_Events.addObserver('aitcg_editor_load_shape_before_' + optionId, this.onEditorLoadShapeBefore.bind(this));
            Aitoc_Common_Events.addObserver('aitcg_editor_dumpshape_before_' + optionId, this.onEditorDumpshapeBefore.bind(this));
            Aitoc_Common_Events.addObserver('aitcg_editor_reset_after_' + optionId, this.onEditorReset.bind(this));
            Aitoc_Common_Events.addObserver('aitcg_option_preview_create_after_' + optionId, this.onOptionPreviewCreateAfter.bind(this));
        },
        _requestSuccess           : function (transport) {
            var response = eval("(" + transport.responseText + ")");
            var img = $$('.techimg')[0];
            $$('.techimg').each(function (elem) {
                if (elem.parentNode.visible()) {
                    img = elem;
                }
            });
            img.onload = function (e) {
                this.addMaskImage(Aitcg.getEventTarget(e), response);
                img.src = '';
                Aitcg.hideLoader();
            }.bind(this);
            img.src = response.url;
        },
        _createMaskLayer          : function (params) {
            var maskLayer = new Element('div', {
                id     : this.tools.config.optionId + params.idSuffix,
                'class': 'aitcg_preview_inverted_mask'
            }).setStyle({
                width  : params.width,
                height : params.height,
                opacity: 0.4,
                display: 'none'
            });
            if (this.useragent.indexOf('MSIE') != -1) {
                maskLayer.setStyle({
                    zoom: '1'
                });
            }
            return maskLayer;
        },
        initData                  : function () {
            var main_container = $(Aitcg_Editor.CONTAINER_ID + this.tools.config.optionId);
            var maskLayer = this._createMaskLayer({
                idSuffix: '_inverted-mask-printable-area-image',
                width   : main_container.getStyle('width'),
                height  : main_container.getStyle('height')
            });
            if (this.useragent.indexOf('MSIE') != -1 || Prototype.Browser.Opera) {
                maskLayer.setStyle({
                    zIndex: '4000'
                });
            }
            $(Aitcg_Editor.TEMPLATE_ID).insert({after: maskLayer});
        },
        initObservers             : function ($super) {
            if (this.tools.config.editorEnabled) {
                $super();
                $('masks-category-selector-' + this.tools.config.rand).observe('change', this.categoryPreview.refresh.bindAsEventListener(this.categoryPreview));
                $('submit-add-mask-' + this.tools.config.rand).observe('click', this.submit.bindAsEventListener(this));
                $('submit-remove-mask-' + this.tools.config.rand).observe('click', this.remove.bindAsEventListener(this));
                $('custom_select3').observe('click', function (element) {
                    var select = this.parentElement.children[0];
                    var event = document.createEvent('MouseEvents');
                    event.initMouseEvent('mousedown', true, true, window);
                    select.dispatchEvent(event);
                });
                this.renderSelect();
                $('masks-category-selector-' + this.tools.config.rand).parentElement.children[1].observe('click', function (element) {
                    var display = this.children[2].getStyle('display');
                    if (display == 'none') {
                        this.children[2].setStyle({
                            display: 'block'
                        });
                    } else {
                        this.children[2].setStyle({
                            display: 'none'
                        });
                    }
                });
            }
        },
        renderSelect              : function () {
            var self = this;
            var select = $$('#masks-category-selector-' + this.tools.config.rand + ' option');
            var customSelect = $('masks-category-selector-' + this.tools.config.rand).parentElement.children[1].children[2];
            select.forEach(function (option) {
                if (option.getAttribute('value') != '') {
                    var temp = new Element('div', {value: option.getAttribute('value'), class: 'option'});
                    temp.insert(option.innerHTML);
                    customSelect.insert({
                        bottom: temp
                    });
                    temp.observe('click', function (element) {
                        this.parentElement.parentElement.parentElement.children[0].value = this.readAttribute('value'); //original select
                        this.parentElement.parentElement.children[0].innerHTML = this.innerHTML; //title
                        self.categoryPreview.refresh();
                    });
                }
            });
        },
        /**
         * Submit a mask chosen by customer to the server
         */
        submit                    : function () {
            var error = $('add_masks_' + this.tools.config.rand + '_error');
            error.hide();
            var selection = document.getElementsByName('mask' + this.tools.config.rand);
            for (var i = 0; i < selection.length; i++) {
                if (selection[i].checked == true) {
                    var radios = selection[i];
                }
            }
            if (typeof(radios) == 'undefined') {
                error.show();
                return;
            }
            this._request({
                mask_id: radios.getValue()
            });
        },
        /**
         * Calculate correct mask params before adding to the editor
         */
        addMaskImage              : function (img, response) {
            var x = 0,
                y = 0,
                sizeX = 0,
                sizeY = 0,
                width = 0,
                height = 0,
                scale = 1;
            if (this.config.location == 0) {
                sizeX = Math.round(this.tools.config.area.sizeX);
                sizeY = Math.round(this.tools.config.area.sizeY);
            } else {
                sizeX = Math.round(this.tools.config.productImage.sizeX);
                sizeY = Math.round(this.tools.config.productImage.sizeY);
            }
            if (response.resize == 0) {
                width = sizeX;
                height = sizeY;
            } else {
                scale = Math.min((sizeX) / img.getWidth(), (sizeY) / img.getHeight());
                width = Math.round(img.getWidth() * scale);
                height = Math.round(img.getHeight() * scale);
                x = Math.round((sizeX - width) / 2);
                y = Math.round((sizeY - height) / 2);
            }
            if (this.config.location == 0) {
                x += this.tools.config.area.offsetX;
                y += this.tools.config.area.offsetY;
            }
            var mask = {
                x            : x,
                y            : y,
                width        : width,
                height       : height,
                url          : response.url,
                url_base     : response.url_base,
                createMaskUrl: this.config.createMaskUrl,
                mask_id      : response.id
            };
            // remove previous mask
            if (typeof this.maskCreated != 'object' && this.maskCreated > 0) {
                this.maskDelete = this.maskCreated;
            }
            // add new mask
            this.addMask(mask, this.editor().scale, false);
        },
        /**
         * Add mask in editable or preview mode
         * @param mask Object
         * @param scale int
         */
        addMask                   : function (mask, scale, scaleEnabled) {
            var name_area;
            // clone the mask to prevent from saving its scaled version
            this.maskCreated = Object.clone(mask);
            if (scaleEnabled !== false) {
                scaleEnabled = true;
            }
            if (this.editor().mode == Aitcg_Editor.MODE_PREVIEW) { // preview
                name_area = this.tools.config.optionId + '_inverted-mask-printable-area-image_preview';
            } else if (this.editor().mode == Aitcg_Editor.MODE_EDIT) { // editor
                name_area = this.tools.config.optionId + '_inverted-mask-printable-area-image';
                try {
                    $('submit-remove-mask-' + this.tools.config.rand).show();
                } catch (e) {
                }
            }
            var width_image = $(name_area).getWidth(),
                height_image = $(name_area).getHeight();
            if (width_image == 0) {
                width_image = $(name_area).getStyle('width').slice(0, -2);
            }
            if (height_image == 0) {
                height_image = $(name_area).getStyle('height').slice(0, -2);
            }
            if (this.tools.config.proportionScreen < 1 && scaleEnabled !== false) {
                scale *= this.tools.config.proportionScreen;
            }
            mask.x = Math.round(mask.x * scale);
            mask.y = Math.round(mask.y * scale);
            mask.width = Math.round(mask.width * scale);
            mask.height = Math.round(mask.height * scale);
            var opacity = '';
            if (this.useragent.indexOf('MSIE') != -1) {
                opacity = '-ms-filter:\'progid:DXImageTransform.Microsoft.Alpha(Opacity=50)\';zoom:1;';
            }
            $(name_area).innerHTML = '<img src="' + mask.url + '"  style="width:' + mask.width + 'px; height:' + mask.height + 'px;position:absolute; top:' + mask.y + 'px; ' + opacity + 'left:' + mask.x + 'px;" id="mask_image">' +
                '<img src="' + mask.url_base + '1x1_black.png"  style="width:' + mask.x + 'px; height:' + height_image + 'px;position:absolute;' + opacity + ' top:0px; left:0px; background-color:#000;">' +
                '<img  src="' + mask.url_base + '1x1_black.png"  style=" width:' + (width_image - mask.x - mask.width) + 'px; height:' + height_image + 'px;position:absolute; ' + opacity + 'top:0px; left:' + (mask.width + mask.x) + 'px; background-color:#000;">' +
                '<img  src="' + mask.url_base + '1x1_black.png"  style=" width:' + mask.width + 'px; height:' + mask.y + 'px;position:absolute; top:0px; left:' + mask.x + 'px; ' + opacity + 'background-color:#000;">' +
                '<img  src="' + mask.url_base + '1x1_black.png"   style="width:' + mask.width + 'px; height:' + (height_image - mask.y - mask.height) + 'px;position:absolute; ' + opacity + 'top:' + (mask.height + mask.y) + 'px; left:' + mask.x + 'px; background-color:#000;">';
            $(name_area).show();
        },
        /**
         * Add mask in the printable mode
         * @param mask Object
         * @param scale int
         */
        addMaskPrint              : function (mask, scale, scaleEnable) {
            this.maskCreated = mask;
            this.shapeMask = [];
            var name_area_raph = 'raphTop_' + this.tools.config.optionId;
            var left_image = Math.round($(name_area_raph).getStyle('left').slice(0, -2)),
                top_image = Math.round($(name_area_raph).getStyle('top').slice(0, -2)),
                width_image_raph = Math.round($(name_area_raph).getWidth()),
                height_image_raph = Math.round($(name_area_raph).getHeight());
            if (width_image_raph == 0) {
                width_image_raph = Math.round($(name_area_raph).getStyle('width').slice(0, -2));
            }
            if (height_image_raph == 0) {
                height_image_raph = Math.round($(name_area_raph).getStyle('height').slice(0, -2));
            }
            var tempScale = 1;
            if (this.tools.config.proportionScreen < 1) {
                tempScale *= this.tools.config.proportionScreen;
            }
            mask.x = Math.round((mask.x - (left_image / tempScale)) * tempScale);
            mask.y = Math.round((mask.y - (top_image / tempScale)) * tempScale);
            mask.width = Math.round(mask.width);
            mask.height = Math.round(mask.height);
            var creator = this._getCreatorObject();
            this.shapeMask[0] = this.editor().createImageElement({
                src    : mask.url_black,
                x      : mask.x,
                y      : mask.y,
                width  : mask.width,
                height : mask.height,
                creator: creator
            }, scale);
            if (mask.x > 0) {
                this.shapeMask[1] = this.editor().createImageElement({
                    src    : mask.url_base + '1x1_black.png',
                    x      : 0,
                    y      : 0,
                    width  : mask.x,
                    height : height_image_raph,
                    creator: creator
                }, scale);
                this.shapeMask[2] = this.editor().createImageElement({
                    src    : mask.url_base + '1x1_black.png',
                    x      : mask.x + mask.width,
                    y      : 0,
                    width  : (width_image_raph - mask.x - mask.width),
                    height : height_image_raph,
                    creator: creator
                }, scale);
            }
            if (mask.y > 0) {
                this.shapeMask[2] = this.editor().createImageElement({
                    src    : mask.url_base + '1x1_black.png',
                    x      : mask.x,
                    y      : 0,
                    width  : mask.width,
                    height : mask.y,
                    creator: creator
                }, scale);
                this.shapeMask[3] = this.editor().createImageElement({
                    src    : mask.url_base + '1x1_black.png',
                    x      : mask.x,
                    y      : mask.y + mask.height,
                    width  : mask.width,
                    height : (height_image_raph - mask.y - mask.height),
                    creator: creator
                }, scale);
            }
        },
        /**
         * Remove mask
         */
        remove                    : function () {
            $(this.tools.config.optionId + '_inverted-mask-printable-area-image').innerHTML = '';
            if (typeof this.maskCreated != 'object' && this.maskCreated > 0) {
                this.maskDelete = this.maskCreated;
            }
            this.maskCreated = 0;
        },
        onEditorLoadShapeBefore   : function (params) {
            var shape = params.shape,
                scale = params.scale;
            if (params.scaleEnable === false) {
                scale = 1;
            }

            if (shape.maskCreated != 0 && this.config.getMaskUrl != 0 && typeof shape.maskCreated != 'undefined' && typeof this.config.getMaskUrl != 'undefined') {
                if (shape.maskCreated != this.maskCreated) {
                    new Ajax.Request(this.config.getMaskUrl, {
                        method      : 'post',
                        parameters  : {id: shape.maskCreated},
                        asynchronous: (this.editor().mode == Aitcg_Editor.MODE_PREVIEW),
                        onSuccess   : function (transport) {
                            var response = eval("(" + transport.responseText + ")");
                            if (this.editor().mode == Aitcg_Editor.MODE_SAVE) {
                                this.addMaskPrint(response.mask, scale, params.scaleEnable );
                            } else {
                                this.addMask(response.mask, scale);
                            }
                            this.maskCreated = response.mask.id;
                        }.bind(this)
                    });
                }
            } else {
                try {
                    var name_area = this.tools.config.optionId + '_inverted-mask-printable-area-image';
                    if (this.editor().mode == Aitcg_Editor.MODE_PREVIEW) {
                        name_area = this.tools.config.optionId + '_inverted-mask-printable-area-image_preview';
                    }
                    var div = $(name_area);
                    if (div) {
                        div.innerHTML = '';
                    }
                } catch (err) {
                    alert(err.message)
                }
            }
        },
        onEditorLoadBefore        : function () {
            this.maskCreated = 0;
        },
        onEditorLoadAfter         : function () {
            if (typeof this.shapeMask == 'object' && this.editor().mode == Aitcg_Editor.MODE_SAVE) {
                this.shapeMask.each(function (item) {
                    this.editor().addShape(item);
                    item.toFront();
                }.bind(this));
                this.shapeMask = 0;
            }
        },
        onEditorReset             : function (params) {
            var suffix = (params.editor.mode == Aitcg_Editor.MODE_PREVIEW) ? '_preview' : '';
            $(this.tools.config.optionId + '_inverted-mask-printable-area-image' + suffix).innerHTML = '';
            if (params.editor.mode == Aitcg_Editor.MODE_EDIT && typeof this.maskCreated == 'object') {
                this.maskCreated = 0;
            }
        },
        onEditorDumpshapeBefore   : function (params) {
            if (typeof this.maskCreated == 'object') {
                var tempParametrs = this.maskCreated;
                if (this.tools.config.proportionScreen < 1) {
                    tempParametrs.x = Math.round(this.maskCreated.x / this.tools.config.proportionScreen);
                    tempParametrs.y = Math.round(this.maskCreated.y / this.tools.config.proportionScreen);
                    tempParametrs.width = Math.round(this.maskCreated.width / this.tools.config.proportionScreen);
                    tempParametrs.height = Math.round(this.maskCreated.height / this.tools.config.proportionScreen);
                }
                new Ajax.Request(this.config.createMaskUrl,
                    {
                        method      : 'post',
                        parameters  : tempParametrs,
                        asynchronous: false,
                        onSuccess   : function (transport) {
                            var response = eval("(" + transport.responseText + ")");
                            this.maskCreated = response.id;
                        }.bind(this)
                    });
            }
            if (this.maskDelete > 0) {
                new Ajax.Request(this.config.delMaskUrl,
                    {
                        method      : 'post',
                        parameters  : {id: this.maskDelete},
                        asynchronous: false,
                        onSuccess   : function (transport) {
                            if (this.maskCreated == this.maskDelete) {
                                this.maskCreated = 0;
                            }
                            this.maskDelete = 0;
                        }.bind(this)
                    });
            }
            params.info.maskCreated = this.maskCreated;
        },
        onOptionPreviewCreateAfter: function (params) {
            var element = params.element.down('img', 0),
                thumb = this.tools.config.productImage.thumb;
            var maskLayer = this._createMaskLayer({
                idSuffix: '_inverted-mask-printable-area-image_preview',
                width   : thumb.sizeX + 'px',
                height  : thumb.sizeY + 'px'
            });
            $(element).insert({after: maskLayer});
        }
    });
