/**
 * Text customization tool
 */
var Aitcg_Editor_Tool_Text = Class.create(Aitcg_Editor_Tool_Abstract,
    {
        titleTemplate   : 'text_title',
        _colorSet       : null,
        _saveAllowed    : true,
        _colorSetShadow : null,
        _colorSetOutline: null,
        inputType       : 0,
        change          : false,
        curveText       : 0,
        slider          : 0,
        oldSliderValue     : 400,

        initialize: function ($super, params) {
            this.inputType = params.config.InputBoxType;
            this.curveText = params.config.curveText;
            textLength = params.config.textLength;

            $super(params);
            this.template =
                '<div id="aitcg-tool-' + this.toolKey + '" class="tool-body"><form id="add_text_form{{rand}}">' +
                '<div id="text-edit-notice-{{rand}}" class="text-edit-notice" style="display:none;"></div>';

            // begin table
            this.template +=
                '<table class="form-list">' +
                '<input type="hidden" name="useblackAndWhite" value="' + this.tools.config.tools.Save.isblackwhite + '">';

            // text
            this.template +=
                '<tr>' +
                '<td class="label" style="vertical-align: top; padding-top: 30px;"><label for="add_text_{{rand}}">{{texttoadd_text}}</label></td>' +
                '<td class="value">';
            if (textLength) {
                this.template +=
                    '<span id="text_params"><span id="text_lenght">0</span><span id="text_limit">/' + textLength + '</span></span>';
            } else {
                this.template +=
                    '<span id="text_params"><span id="text_lenght">0</span></span>';
            }


            if (this.inputType) {
                if (textLength) {
                    this.template +=
                        '<input maxlength="' + textLength + '" class="required-entry input-text" id="add_text_{{rand}}" name="text" value="" placeholder="Happy Birthday"';
                } else {
                    this.template +=
                        '<input class="required-entry input-text" id="add_text_{{rand}}" name="text" value="" placeholder="Happy Birthday"';
                }

                this.template +=
                    '" /><span style="display: none">0</span>';

            } else {
                if (textLength) {
                    this.template +=
                        '<textarea maxlength="' + textLength + '" class="required-entry input-text" id="add_text_{{rand}}" name="text" value="" placeholder="Happy Birthday"';
                } else {
                    this.template +=
                        '<textarea  class="required-entry input-text" id="add_text_{{rand}}" name="text" value="" placeholder="Happy Birthday"';
                }

                this.template +=
                    '" ></textarea><span style="display: none">0</span>';
            }

            this.template +=
                '<input type="hidden" name="curveText" value="' + this.curveText + '">' +
                '<input type="hidden" id="radiusText" name="radiusText" value="400">' +
                '</td>' +
                '</tr>';

            //font family
            this.template +=
                '<tr>' +
                '<td class="label" style="vertical-align: top; padding-top: 10px"><label for="font-preview-selector-{{rand}}">{{font_family_text}}</label></td>' +
                '<td class="value" style="position: relative; width: 370px; height: 40px">' +
                '<select id="font-family-preview-selector-{{rand}}" name="font_family" class="required-entry select" style="float: left;">{{fontFamilyOptions}}</select>' +
                '<span id="custom_family_select"><span id="select-title">Default </span><div class="button"></div><div class="options"></div></span>' +
                '</td>' +
                '</tr>';

            // font
            this.template +=
                '<tr>' +
                '<td class="label" style="vertical-align: top; padding-top: 10px"><label for="font-preview-selector-{{rand}}">{{font_text}}</label></td>' +
                '<td class="value" style="position: relative; width: 370px; height: 40px">' +
                '<select id="font-preview-selector-{{rand}}" name="font" class="required-entry select" style="float: left;">{{fontOptions}}</select>' +
                '<span id="custom_select"><span id="select-title">All Star Resort</span><div class="button"></div><div class="options"></div></span>' +
                '</td>' +
                '</tr>';

            // font preview
            this.template +=
                '<tr id="font-preview-row" style="">' +
                '<td></td>' +
                '<td class="value">' +
                '<div class="aitcg-font-preview"><img id="font-preview-{{rand}}" src="http://www.picture-me.com/js/aitoc/aitcg/img/defaultFont.png" /></div>' +
                '</td>' +
                '</tr>';

            // align
            if (!this.inputType) {
                this.template += '<tr>';
            } else {
                this.template += '<tr style="display: none">';
            }

            this.template +=
                '<td class="label"><label for="font-preview-selector-{{rand}}">Align</label></td>' +
                '<td class="value">' +
                '<span class="icon align-button button-left selected" value="left" optionid="{{rand}}"></span>' +
                '<span class="icon align-button button-center" value="center" optionid="{{rand}}"></span>' +
                '<span class="icon align-button button-right" value="right" optionid="{{rand}}" ></span>' +
                '<input type="hidden" name="alignFont" id="align-font-{{rand}}" value="left" />' +
                '</td>';

            this.template += this._getColorpickHtml();
            this.template += this._getCurveText();
            this.template += '</tr>';


            this.template += this._getOutlineHtml();
            this.template += this._getShadowHtml();
            // end table
            this.template +=
                '</table>';

            // add text button
            this.template +=
                this._getUnderTemplateSelectHtml() +
                '<div class="buttons" style="display: none">' +
                '<button type="button" class="aitcg-button" id="submit-text-image-{{rand}}">{{addtext_text}}</button>' +
                '<button type="button" style="display:none;" class="aitcg-button" id="submit-text-edit-{{rand}}">{{apply_text}}</button>' +
                '</div>';

            // end form
            this.template +=
                '</form>' +
                '</div>';
        },

        _getColorpickHtml: function () {
            var template = '';
            if (this.config.useColorpick) {
                if (this.config.onlyPredefColor) {
                    template +=
                        '<tr>' +
                        '<td class="label"><label for="font-selector{{rand}}">{{pickcolor_text}}</label></td>' +
                        '<td class="value">' +
                        '<input id="colorfield{{rand}}" class="jscolorpicker {pickerOnfocus:false}" readonly="readonly" name="color" value="#000000" style="width: 100px; background-color:#000000;" />';

                    if (this.config.colorSet.split('#').length > 2) {
                        template += '<div id="aitcg_colorset_container{{rand}}" class="aitcg_colorset_container"></div>';
                    } else {
                        template += '<div id="aitcg_colorset_container{{rand}}" class="aitcg_colorset_container" style="display: none;"></div>';
                    }
                    template +=
                        '</td>' +
                        '</tr>';
                } else {
                    template +=
                        '<tr>' +
                        '<td class="label"><label for="font-selector{{rand}}">{{pickcolor_text}}</label></td>' +
                        '<td class="value">' +
                        '<input id="colorfield{{rand}}" name="color" class="jscolorpicker" value="#000000" style="width: 100px;" />' +
                        '</td>' +
                        '</tr>';
                }
            }
            return template;
        },

        _getCurveText: function () {
            var template = '';
            if (this.curveText) {
                template +=
                    '<tr>' +
                    '<td class="label"><label for="font-selector{{rand}}">Curve</label></td>' +
                    '<td class="value">' +
                    '<div id="zoom_slider" class="slider">' +
                    '<div class="handle"></div>' +
                    '</div>';
                template += '<input type="hidden" name="downText" id="downText" value="0">';
                template += '<span  name="downText" class="downText selected" id="downTextUp" value="0">UP</span>';
                template += '<span  name="downText" class="downText" id="downTextDown" value="1">DOWN</span>';
                template +=
                    '</td>' +
                    '</tr>';
            }
            return template;
        },

        _getOutlineHtml: function () {
            var template = '';
            if (this.config.useOutline > 0) {
                template +=
                    '<tr>' +
                    '<td class="label"><label for="outline{{rand}}">{{outline_text}}</label></td>' +
                    '<td class="value">' +
                    '<input id="outline{{rand}}" name="outline" type="checkbox" class="aitcg-text-cb-{{rand}}" />' +
                    '</td>' +
                    '</tr>';
                template +=
                    '<tr style="display: none;">' +
                    '<td></td>' +
                    '<td class="label">' +
                    '<table  class="form-list">' +
                    '<tr>' +
                    '<td class="label">';
                if (this.config.onlyPredefColor) {
                    template += '' +
                        '<label for="font-selector{{rand}}">{{pickcoloroutline_text}}</label>' +
                        '</td>' +
                        '<td class="value">' +
                        '<input id="coloroutline{{rand}}" class="jscolorpicker {pickerOnfocus:false}" readonly="readonly" name="coloroutline" value="#000000" style="width: 100px; background-color:#000000;" />' +
                        '<div id="aitcg_colorset_container_outline{{rand}}" class="aitcg_colorset_container" ></div>';
                } else {
                    template +=
                        '<label for="font-selector{{rand}}">{{pickcoloroutline_text}}</label>' +
                        '</td>' +
                        '<td class="value">' +
                        '<input id="coloroutline{{rand}}" name="coloroutline" class="jscolorpicker" value="#000000" style="width: 100px;" />';
                }

                template +=
                    '</td>' +
                    '</tr>' +
                    '<tr>' +
                    '<td class="label">' +
                    '<label for="widthoutline{{rand}}">{{widthoutline_text}}</label>' +
                    '</td>' +
                    '<td class="value">' +
                    '<input id="widthoutline{{rand}}" name="widthoutline" type="text" value="1" size=5>' +
                    '</td>' +
                    '</tr>' +
                    '</table>' +
                    '</td>' +
                    '</tr>';
            }
            return template;
        },

        _getShadowHtml: function () {
            var template = '';
            if (this.config.useShadow > 0) {
                template +=
                    '<tr>' +
                    '<td class="label"><label for="shadow{{rand}}">{{shadow_text}}</label></td>' +
                    '<td class="value">' +
                    '<input id="shadow{{rand}}" name="shadow" type="checkbox" class="aitcg-text-cb-{{rand}}" />' +
                    '</td>' +
                    '</tr>' +
                    '<tr style="display: none;">' +
                    '<td></td>' +
                    '<td class="label">' +
                    '<table  class="form-list">' +
                    '<tr>' +
                    '<td class="label">';
                if (this.config.onlyPredefColor) {
                    template +=
                        '<label for="font-selector{{rand}}">{{pickcolorshadow_text}}</label>' +
                        '</td>' +
                        '<td class="value">' +
                        '<input id="colorshadow{{rand}}" class="jscolorpicker {pickerOnfocus:false}" readonly="readonly" name="colorshadow" value="#000000" style="width: 50px; background-color:#000000;" />' +
                        '<div id="aitcg_colorset_container_shadow{{rand}}" class="aitcg_colorset_container" ></div>';
                } else {
                    template +=
                        '<label for="font-selector{{rand}}">{{pickcolorshadow_text}}</label>' +
                        '</td>' +
                        '<td class="value">' +
                        '<input id="colorshadow{{rand}}" name="colorshadow" class="jscolorpicker" value="#000000" style="width: 50px;" />';
                }

                template +=
                    '</td>' +
                    '</tr>' +
                    '<tr>' +
                    '<td class="label">' +
                    '<label for="shadowalpha{{rand}}">{{shadowalpha_text}}</label>' +
                    '</td>' +
                    '<td class="value">' +
                    '<input id="shadowalpha{{rand}}"  name="shadowalpha" value="50" type="text" size="5" />' +
                    '</td>' +
                    '</tr>' +
                    '<tr>' +
                    '<td class="label">' +
                    '<label for="shadowoffsetx{{rand}}">{{shadowoffsetx_text}}</label>' +
                    '</td>' +
                    '<td class="value">' +
                    '<input id="shadowoffsetx{{rand}}"  name="shadowoffsetx" value="20" type="text" size="5" />' +
                    '</td>' +
                    '</tr>' +
                    '<tr>' +
                    '<td class="label">' +
                    '<label for="shadowoffsety{{rand}}">{{shadowoffsety_text}}</label>' +
                    '</td>' +
                    '<td class="value">' +
                    '<input id="shadowoffsety{{rand}}"  name="shadowoffsety" value="20" type="text" size="5" />' +
                    '</td>' +
                    '</tr>' +
                    '</table>' +
                    '</td>' +
                    '</tr>';
            }
            return template;
        },

        _requestSuccess: function (transport) {
            var response = eval("(" + transport.responseText + ")");

            if (this.editShape) {
                this.editSave(response);
            } else {
                this.loadUploadedImage(response);
            }
            Aitcg.hideLoader();
        },

        initObservers: function ($super) {
            var self = this;
            if (this.tools.config.editorEnabled) {
                $super();
                $('submit-text-image-' + this.tools.config.rand).observe('click', this.preSubmit.bindAsEventListener(this));
                $('submit-text-edit-' + this.tools.config.rand).observe('click', this.preSubmit.bindAsEventListener(this));

                $('font-preview-selector-' + this.tools.config.rand).observe('change', this.preSubmit.bindAsEventListener(this));
                $('add_text_' + this.tools.config.rand).observe('mouseout', this.submit.bindAsEventListener(this));
                $('add_text_' + this.tools.config.rand).observe('keyup', function (button) {
                    self.change = true;
                });

                if ($('colorfield' + this.tools.config.rand) != undefined) {
                    $('colorfield' + this.tools.config.rand).observe('change', this.preSubmit.bindAsEventListener(this));
                }

                $('font-preview-selector-' + this.tools.config.rand).observe('change', this.fontPreview.bindAsEventListener(this));

                $$('.align-button[optionid="' + this.tools.config.rand + '"]').each(function (button) {
                    button.observe('click', function () {
                        optionId = this.getAttribute('optionid');
                        $$('.align-button.selected[optionid="' + optionId + '"]').first().removeClassName('selected');
                        this.addClassName('selected');
                        $('align-font-' + optionId).value = this.getAttribute('value');
                        self.preSubmit();
                    });
                });

                $$('.aitcg-text-cb-' + this.tools.config.rand).each(function (cb) {
                    cb.observe('click', this.onCheckboxClick.bind(this));
                    cb.observe('change', this.onCheckboxClick.bind(this));
                }.bind(this));

                this.renderFontStyles();
                this.renderSelect();
                this.fontPreview();

                $('font-preview-selector-' + this.tools.config.rand).parentElement.children[1].observe('click', function (element) {
                    var display = this.children[2].getStyle('display');
                    if (display == 'none') {
                        this.children[2].setStyle({
                            display: 'block'
                        });
                    } else {
                        this.children[2].setStyle({
                            display: 'none'
                        });
                    }
                });

                $('font-family-preview-selector-' + this.tools.config.rand).value = 1;

                $('font-family-preview-selector-' + this.tools.config.rand).parentElement.children[1].observe('click', function (element) {
                    var display = this.children[2].getStyle('display');
                    if (display == 'none') {
                        this.children[2].setStyle({
                            display: 'block'
                        });
                    } else {
                        this.children[2].setStyle({
                            display: 'none'
                        });
                    }
                });

                if (this.inputType) {
                    $$('input[name=text]')[0].observe('keyup', function (element) {
                        $('text_lenght').innerHTML = this.value.length
                    });
                } else {
                    $$('textarea[name=text]')[0].observe('keyup', function (element) {
                        $('text_lenght').innerHTML = this.value.length
                    });
                }

                jscolor.default_dir = this.tools.config.jsUrl + 'aitoc/aitcg/jscolor/';
                jscolor.init();
                if (this.config.useColorpick) {
                    if (this.config.onlyPredefColor) {
                        this._getColorSet().renderSet();
                        if (this.config.useShadow) {
                            this._getColorSetShadow().renderSet();
                        }
                        if (this.config.useOutline) {
                            this._getColorSetOutline().renderSet();
                        }
                    }
                }

                if (this.curveText) {
                    var zoom_slider = $('zoom_slider');

                    //400 - no curve
                    self.slider = new Control.Slider(zoom_slider.down('.handle'), zoom_slider, {
                        range      : $R(400, 3100),
                        minimum    : 400,
                        maximum    : 3100,
                        sliderValue: 400,
                        alignX     : -100,
                        alignY     : -5,
                        values     : [400, 700, 1000, 1300, 1600, 1900, 2200, 2500, 2800, 3100],
                        onChange   : function (value) {
                            $('radiusText').value = value;
                            if (this.oldSliderValue != value){
                                this.oldSliderValue = value;
                                self.preSubmit();
                            }
                        }
                    });

                    var downTextUpButton = $('downTextUp');
                    var downTextDownButton = $('downTextDown');
                    var valueDownText = $('downText');

                    downTextUpButton.observe('click', function (element) {
                        if (!this.hasClassName('selected')) {
                            downTextUpButton.addClassName('selected');
                            downTextDownButton.removeClassName('selected');
                            valueDownText.value = 0;
                            self.change = true;
                            self.submit();
                        }
                    });

                    downTextDownButton.observe('click', function (element) {
                        if (!this.hasClassName('selected')) {
                            downTextUpButton.removeClassName('selected');
                            downTextDownButton.addClassName('selected');
                            valueDownText.value = 1;
                            self.change = true;
                            self.submit();
                        }
                    });
                }

                this.mobileView();
                this.changeSelect();
            }
        },

        mobileView: function() {
            if (mobileMode) {
                var popUpWidth = $('message-popup-window').getWidth();
                $$('.tools-bodies .options').forEach(function(item) {
                    var select = item.up();
                    var selectIcon = select.select('.button')[0];

                    var newWidth = (parseInt(select.getStyle('width').split('px')[0]) - (500 - popUpWidth));

                    select.setStyle({
                        'width': newWidth + 'px'
                    });

                    selectIcon.setStyle({
                        'left': (newWidth - 30) + 'px'
                    });
                });

                $$('.tools-bodies .input-text').forEach(function(item) {
                    var newWidth = (parseInt(item.getStyle('width').split('px')[0]) - (500 - popUpWidth));
                    item.setStyle({
                        'width': newWidth + 'px',
                        'min-width': newWidth + 'px'
                    });
                });


                $$('.aitcg-font-preview img').forEach(function(item) {

                    var newWidth = (parseInt(item.getStyle('max-width').split('px')[0]) - (500 - popUpWidth));
                    item.setStyle({
                        'width': newWidth + 'px',
                        'max-width': newWidth + 'px'
                    });
                });


                $('downTextUp').setStyle({
                    'clear': 'left',
                    'margin-top': '15px'
                });
                $('downTextDown').setStyle({
                    'margin-top': '15px'
                });
            }
        },
        renderFamilySelect: function (fontFamilyId) {
            var self = this;
            var select = $$('#font-family-preview-selector-' + this.tools.config.rand + ' option');

            var customSelect = $('font-family-preview-selector-' + this.tools.config.rand).parentElement.children[1].children[2];

            $$('#custom_family_select .options')[0].innerHTML = '';

            select.forEach(function (option) {
                if (option.getAttribute('value') != '') {
                    var temp = new Element('div', {value: option.getAttribute('value'), class: 'option'});
                    temp.insert(option.innerHTML);

                    if ($$('#font-preview-selector-' + self.tools.config.rand + ' option[data-family-id=' + option.getAttribute('value') + ']')[0] != undefined) {
                        var fontFamilyFile = $$('#font-preview-selector-' + self.tools.config.rand + ' option[data-family-id=' + option.getAttribute('value') + ']')[0].innerHTML;

                        temp.setStyle({
                            fontFamily: fontFamilyFile,

                        });

                        customSelect.insert({
                            bottom: temp
                        });

                        temp.observe('click', function (element) {
                            this.parentElement.parentElement.parentElement.children[0].value = this.readAttribute('value'); //original select
                            this.parentElement.parentElement.children[0].innerHTML = this.innerHTML; //title

                            self.renderSelect(this.readAttribute('value'));
                        });

                        temp.observe('click', self.preSubmit.bindAsEventListener(self));
                    }
                }
            });

            if (fontFamilyId != undefined) {
                var title = $('font-family-preview-selector-' + this.tools.config.rand).select('option[value=' + fontFamilyId + ']')[0].innerHTML;
                $$('#custom_family_select #select-title')[0].innerHTML = title;
            }
        },

        renderFontStyles: function () {
            var select = $$('#font-preview-selector-' + this.tools.config.rand + ' option');

            var newStyle = document.createElement('style');


            select.forEach(function (option) {
                if (option.getAttribute('value') != '') {
                    var fontName = option.innerHTML;
                    var fontFile = option.readAttribute('data-font-file');

                    newStyle.appendChild(document.createTextNode("\
                     @font-face {\
                         font-family: '" + fontName + "';\
                         src: url('/media/custom_product_preview/fonts/" + fontFile + "');\
                     }\
                 "));

                    document.head.appendChild(newStyle);
                }
            });
        },

        renderSelect: function (fontFamilyId, fontId) {
            if (fontFamilyId == undefined) {
                fontFamilyId = 1;
            }

            var self = this;
            var select = $$('#font-preview-selector-' + this.tools.config.rand + ' option');

            var customSelect = $('font-preview-selector-' + this.tools.config.rand).parentElement.children[1].children[2];

            customSelect.innerHTML = '';

            select.forEach(function (option) {
                if (option.getAttribute('value') != '' && option.readAttribute('data-family-id') == fontFamilyId) {
                    var temp = new Element('div', {value: option.getAttribute('value'), class: 'option'});
                    temp.insert(option.innerHTML);

                    temp.setStyle({
                        fontFamily: option.innerHTML,
                    });

                    customSelect.insert({
                        bottom: temp
                    });

                    temp.observe('click', function (element) {
                        this.parentElement.parentElement.parentElement.children[0].value = this.readAttribute('value'); //original select
                        this.parentElement.parentElement.children[0].innerHTML = this.innerHTML; //title
                        self.fontPreview();
                    });

                    temp.observe('click', self.preSubmit.bindAsEventListener(self));
                }
            });

            var value;
            var title;
            if (fontId !== undefined && fontId != '') {
                value = fontId;
                title = $('font-preview-selector-' + this.tools.config.rand).parentElement.children[1].children[2].select('div[value=' + fontId + ']')[0].innerHTML;
            } else {
                if ($('font-preview-selector-' + this.tools.config.rand).parentElement.children[1].children[2].children[0] != undefined) {
                    value = $('font-preview-selector-' + this.tools.config.rand).parentElement.children[1].children[2].children[0].readAttribute('value');
                    title = $('font-preview-selector-' + this.tools.config.rand).parentElement.children[1].children[2].children[0].innerHTML;
                } else {
                    title = 'No font available';
                    value = '';
                }
            }

            $('font-preview-selector-' + this.tools.config.rand).parentElement.children[1].children[0].innerHTML = title;
            $('font-preview-selector-' + this.tools.config.rand).setValue(value);

            if (value != '') {
                this.fontPreview();
            }
        },

        preSubmit: function () {
            this.change = true;
            this.submit();
        },

        /**
         * Submit customer image to the server
         */
        submit: function () {
            if (!this.change) {
                return false;
            }
            this.change = false;
            var addTextForm = new VarienForm('add_text_form' + this.tools.config.rand);
            if (addTextForm.validator.validate()) {
                this._request($('add_text_form' + this.tools.config.rand).serialize());
            }
        },

        fontPreview: function () {
            var selector = $('font-preview-selector-' + this.tools.config.rand);
            var customSelector = $('custom_select');
            var selectorValue = $('font-preview-selector-' + this.tools.config.rand).getValue();
            if (selectorValue > 0) {
                Aitcg.showLoader();
                new Ajax.Request(this.config.fontPreviewUrl, {
                    method    : 'post',
                    parameters: {font_id: selectorValue, rand: this.tools.config.rand},
                    onSuccess : function (transport) {
                        var response = eval("(" + transport.responseText + ")");
                        $('font-preview-' + response.rand).src = response.src;
                        $('font-preview-row').setStyle({
                            display: 'inline-block'
                        });

                        Aitcg.hideLoader();
                    }.bind(this)
                });
            } else {
                $('font-preview-row').setStyle({
                    display: 'none'
                });
            }

        },

        onCheckboxClick: function (e) {
            var item = e.target,
                itemNext = item.up().up().next();
            if (item.getValue() == 'on') {
                itemNext.show();
            } else {
                itemNext.hide();
            }
        },

        editSave: function (url) {
            this.editShape.updateSource(url, this._getCreatorObject());
        },

        editStop: function () {
            this.editShape = null;
            if ($('submit-text-image-' + this.tools.config.rand)) {
                $('submit-text-image-' + this.tools.config.rand).show();
            }
            if ($('submit-text-edit-' + this.tools.config.rand)) {
                $('submit-text-edit-' + this.tools.config.rand).hide();
            }
            if ($('text-edit-notice-' + this.tools.config.rand)) {
                $('text-edit-notice-' + this.tools.config.rand).hide();
            }

            var underTemplateCheckbox = $('under_template_' + this.toolKey + '_' + this.tools.config.rand);
            if (underTemplateCheckbox) {
                underTemplateCheckbox.enable();
            }
        },

        /**
         * Edit an existing text shape
         *
         * @param shape Aitcg_Editor_Canvas_Shape
         * @param params Object|String
         */
        edit: function (shape, params) {
            this.editShape = shape;
            params = params.parseQuery();

            $('add_text_form' + this.tools.config.rand).getElements().each(function (el) {
                if (typeof params[el.name] != 'undefined') {
                    if (el.getValue() == params[el.name]) {
                        return;
                    }
                    el.setValue(params[el.name]);
                } else {
                    if (el.nodeName == 'INPUT' && el.type == 'checkbox') {
                        el.setValue(false);
                    } else {
                        return;
                    }
                }
            }.bind(this));

            if (!$('aitcg-tool-title-' + this.toolKey).hasClassName('selected')) {
                this.toggleTool();
            }
            $('submit-text-image-' + this.tools.config.rand).hide();
            $('submit-text-edit-' + this.tools.config.rand).show();
            $('text-edit-notice-' + this.tools.config.rand).show();

            var underTemplateCheckbox = $('under_template_' + this.toolKey + '_' + this.tools.config.rand);
            if (underTemplateCheckbox) {
                underTemplateCheckbox.disable();
            }
            this.changeSelect();

            if (this.curveText) {
                var radiusTextValue = $('radiusText').value;
                var downTextUpButton = $('downTextUp');
                var downTextDownButton = $('downTextDown');
                var valueDownText = $('downText').value;

                this.slider.setValue(radiusTextValue);
                if (valueDownText != "0") {
                    downTextUpButton.removeClassName('selected');
                    downTextDownButton.addClassName('selected');
                } else {
                    downTextUpButton.addClassName('selected');
                    downTextDownButton.removeClassName('selected');
                }
            }
        },

        changeSelect: function () {
            var fontId = $('font-preview-selector-' + this.tools.config.rand).value;
            var fontFamilyId = $('font-preview-selector-' + this.tools.config.rand).select('option[value=' + fontId + ']')[0].readAttribute('data-family-id');
            this.renderFamilySelect(fontFamilyId);
            this.renderSelect(fontFamilyId, fontId);
        },

        _getColorSet: function () {
            if (!this._colorSet) {
                var rand = this.tools.config.rand;
                this._colorSet = new Aitcg_ColorSet({
                    source      : this.config.colorSet,
                    containerId : 'aitcg_colorset_container' + rand,
                    initVarName : 'aitcgColorset' + rand,
                    id          : rand,
                    colorInputId: 'colorfield' + rand
                });
            }
            return this._colorSet;
        },

        _getColorSetShadow: function () {
            if (!this._colorSetShadow) {
                var rand = this.tools.config.rand;
                this._colorSetShadow = new Aitcg_ColorSet({
                    source      : this.config.colorSet,
                    containerId : 'aitcg_colorset_container_shadow' + rand,
                    initVarName : 'aitcgColorsetShadow' + rand,
                    id          : rand,
                    colorInputId: 'colorshadow' + rand
                });
            }
            return this._colorSetShadow;
        },

        _getColorSetOutline: function () {
            if (!this._colorSetOutline) {
                var rand = this.tools.config.rand;
                this._colorSetOutline = new Aitcg_ColorSet({
                    source      : this.config.colorSet,
                    containerId : 'aitcg_colorset_container_outline' + rand,
                    initVarName : 'aitcgColorsetOutline' + rand,
                    id          : rand,
                    colorInputId: 'coloroutline' + rand
                });
            }
            return this._colorSetOutline;
        },

        decorateSaveLink: function (linkHtml, params) {
            params = params.parseQuery();
            var text_config = this.tools.config.text,
                htmlBefore = text_config.save_type_text + ' ',
                htmlAfter = '',
                font = this.config.fontOptions,
                fontOptions = document.createElement('select');

            fontOptions.innerHTML = font;

            if (params && params.text && params.font) {
                htmlAfter = '<ul>';

                if (params.color) {
                    htmlAfter += '<li>- ' + text_config.saved_color + ' <b>' + params.color + '</b>;</li>';
                } else {
                    htmlAfter += '<li>- ' + text_config.saved_color + ' <b>Disabled.</b></li>';
                }

                var fontId = params.font;
                var selector = '';

                for (var i = 0; i < fontOptions.length; i++) {
                    if (fontOptions[i].value == fontId) {
                        selector = fontOptions[i];
                    }
                }

                htmlAfter += '<li>- ' + text_config.saved_text + ' <b>' + params.text + '</b>.</li>';
                htmlAfter += '<li>- ' + text_config.font + ' <b>' + selector.innerHTML + '</b>.</li>';
                htmlAfter += '</ul>';
            }
            return htmlBefore + linkHtml + htmlAfter;
        }
    });