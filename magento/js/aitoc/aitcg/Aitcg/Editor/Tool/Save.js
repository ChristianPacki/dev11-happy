/**
 * Social widgets selection class
 */
var Aitcg_Editor_Tool_Save = Class.create( Aitcg_Editor_Tool_Abstract,
{
    optionId      : null,
    form          : null,
    formId        : 'aitcg_save_tool_form',
    titleTemplate : 'save_title',
    print_types   : ['bottom','bg','top','mask'],
    _preventUpdate: false,
    _canvas       : null,
    _storedLinks  : null,

    initialize: function( $super, params )
    {
        $super(params);

        this._canvas = new Element('canvas');
        this._storedLinks = {};

        this.optionId = this.tools.config.optionId;
        this.template =
            '<div id="aitcg-tool-' + this.toolKey + '" class="tool-body">' +
                '<div id="download_default_{{rand}}" class="aitcg_downloadlinks" style="display:none;"><div><b>' + this.tools.config.text.save_links_title + '</b></div><ul></ul></div>' +
                '<div id="download_default_{{rand}}_empty">{{save_about}}</div>' +
                this._getDownloadButtonsHtml()+ 
            '</div>';
            
        Aitoc_Common_Events.addObserver('aitcg_canvas_add_shape_after_' + this.optionId, this.onAddShapeAfter.bind(this));
        Aitoc_Common_Events.addObserver('aitcg_shape_delete_before_'    + this.optionId, this.onShapeDelete.bind(this));
        Aitoc_Common_Events.addObserver('aitcg_editor_load_before_'     + this.optionId, this.onEditorReset.bind(this));
        Aitoc_Common_Events.addObserver('aitcg_editor_reset_after_'     + this.optionId, this.onEditorReset.bind(this));
        Aitoc_Common_Events.addObserver('aitcg_option_apply_save_after_' + this.optionId, this.createImageDefault.bind(this));

    },
    
    _getDownloadButtonsHtml: function()
    {
        if(/MSIE ([0-8]+.\d+);/.test(navigator.userAgent) && (document.documentMode <= 8)) {
            return '';
        }
        return '<div class="buttons" id="saveas-buttons">' +
                '<div><b>{{print_text}}</b></div>'+
                (this.tools.config.allowUploadUnderTemplate ? '<div class="aitcg_print_sub"><input type="checkbox" id="print_type_bottom" value="1" /><label for="print_type_bottom">{{print_type_bottom}}</label></div>' : '')+
                '<div class="aitcg_print_sub"><input type="checkbox" id="print_type_bg"     value="2" /><label for="print_type_bg">{{print_type_bg}}</label></div>'+
                '<div class="aitcg_print_sub"><input type="checkbox" id="print_type_top"    value="4" checked="checked" /><label for="print_type_top">{{print_type_top}}</label></div>'+
                (this.tools.isToolEnabled('Mask') ? '<div class="aitcg_print_sub"><input type="checkbox" id="print_type_mask"  value="8" /><label for="print_type_mask">{{print_type_mask}}</label></div>':'')+
                '<div class="aitcg_print_sub">{{scale_text}} '+' <input type="text" id="print_scale" value="1" size="1" /></div>' +
                '<div class="validation-advice" style="display:none;" id="print_type_error">{{print_type_error}}</div>'+
                '<button title="{{buttonHelp}}" id="submit-saveAsSvg-{{rand}}" class="aitcg-button tooltip-help">{{svg_text}}</button>'+
                (this.config.savePdfUrl?'<button title="{{buttonHelp}}" class="aitcg-button tooltip-help" id="submit-saveAsPdf-{{rand}}" >{{pdf_text}}</button>':'')+
                ((Prototype.Browser.IE || navigator.userAgent.match(/Trident\/7\./)) ? '' : '<button id="submit-saveAsPng-{{rand}}" type="button" title="{{buttonHelp}}" class="aitcg-button tooltip-help">{{png_text}}</button>') +
                '<a href="" style="display:none;" target="_blank" id="canvas_link_' + this.optionId + '"></a>' +
            '</div>';
    },


    /**
     * Render title and tool switch
     *
     * @return string
     */
    _getControlsHtml: function( $super )
    {
        if(this.config.isShow == 0)
        {
            return '';
        }
        return $super();
    },

    initObservers: function( $super )
    {
        if(this.config.isShow == 1)
        {
            $super();
        }
        if(!(/MSIE ([0-8]+.\d+);/.test(navigator.userAgent) && (document.documentMode <= 8))) {
            $('submit-saveAsSvg-' + this.tools.config.rand).observe('click', this.submitSvg.bindAsEventListener(this));
            if (this.config.savePdfUrl) {
                $('submit-saveAsPdf-' + this.tools.config.rand).observe('click', this.submitPdf.bindAsEventListener(this));
            }
        }

        if (!(Prototype.Browser.IE || navigator.userAgent.match(/Trident\/7\./))) {
            $('submit-saveAsPng-' + this.tools.config.rand).observe('click', this.submitPng.bindAsEventListener(this));
        }

        this._updateDownloadLinksBlock();
    },

    onAddShapeAfter: function( params )
    {
        var creator = params.shape.creator;
        if (creator && creator.tool && creator.tool.isSaveAllowed()) {
            this._addDownloadImageLink(params.shape.element.attr('src'), creator);
        }
    },

    onShapeDelete: function( params )
    {
        var src = params.shape.element.attr('src');
        if (this._storedLinks[src]) {
            delete this._storedLinks[src];
            this._updateDownloadLinksBlock();
        }
    },
    
    _collectFormData: function(top, bottom, scale, print_type, additional)
    {
        additional = additional || {};
        if (this.config.additional)
        {
            Object.extend(additional, this.config.additional);
        }

        var parameters = {
            data        : top, 
            data_bottom : bottom, 
            type        : Raphael.type, 
            background  : this.tools.config.productImage.fullUrl, 
            areaOffsetX : this.tools.config.area.offsetX, 
            areaOffsetY : this.tools.config.area.offsetY,
            print_type  : Object.toJSON(print_type),
            print_scale : scale,
            additional  : additional
        };

        if (this.tools.config.proportionScreen < 1) {
            parameters.areaOffsetX /= this.tools.config.proportionScreen;
            parameters.areaOffsetY /= this.tools.config.proportionScreen;
        }

        Aitoc_Common_Events.dispatch('aitcg_tool_save_collect_form_data_'+this.optionId, {formData: parameters});
        if(typeof(parameters.additional)!= 'string') {
            parameters.additional = Object.toJSON(parameters.additional);
        }
        return parameters;
    },
    
    _submit: function(formUrl, scale, dataScale)
    {
        this.editor().unselect();
        var print_type = this._getSelectedPrintTypes();
        if (print_type) {
            this.preventUpdate();
            var data = this._getPrintableVersion(dataScale, false);
            var form = this._getForm();
            form.action = formUrl;

            var parameters = this._collectFormData(data.top, data.bottom, scale, print_type);
            this._setFormVars(parameters);

            form.submit();
            this._unsetFormLink();
            this.allowUpdate();
        }
    },
    
    submitSvg: function(event)
    {
        Event.stop(event);
        var scale = this._getScale();
        this._submit(this.config.saveSvgUrl, scale, scale);
    },
    
    submitPdf: function(event)
    {
        Event.stop(event);
        var scale = this._getScale(15);
        this._submit(this.config.savePdfUrl, scale, scale);
    },

    submitPng: function(event)
    {
        Event.stop(event);
        this.processPng( function() {
                var popup = window.open(this.getCanvas().toDataURL("image/png"));
                if (popup) {
                    popup.onload = function () {
                    }
                } else {
                    alert(aitcgBaseConfig.text.save_new_tab_error);
                }
            }.bind(this)
        );
    },

    createImageDefault: function()
    {
        this.processPng( function() {
            var dataUrl = this.getCanvas().toDataURL();
            this.reservedImgId = this.tools.config.rand;
            this.createImgUrl = this.config.saveImageDefaultUrl;
            this._ajaxCreateImg(dataUrl, 0);
        }.bind(this),
            {scale: 1, printTypes: {'all': 1}}
        );
    },

    getCanvas: function()
    {
        return this._canvas;
    },
    
    processPng: function( callback, options, scaleEnable )
    {
        if (scaleEnable !== true) {
            scaleEnable = false;
        }

        options = options || {};

        if (this.tools.config.editorEnabled) {
            this.editor().unselect();
        }

        var print_type = options.printTypes || this._getSelectedPrintTypes();
        if (print_type) {
            this.preventUpdate();
            var scale   = options.scale || this._getScale(),
                data    = this._getPrintableVersion(scale, scaleEnable),
                top     = this._modifySvg(data.top),
                bottom  = this._modifySvg(data.bottom),
                tConfig = this.tools.config,
                svg;
            if (typeof(this.config.normalizateSvgToPngUrl) != 'undefined') {
                var parameters = this._collectFormData(top, bottom, scale, print_type);
                new Ajax.Request(this.config.normalizateSvgToPngUrl,
                    {
                        method:'post',
                        parameters: parameters,
                        asynchronous: false,
                        onSuccess: function(transport){
                            svg = transport.responseText;
                        }.bind(this)
                    });
            }

            var canvas = this.getCanvas();
            if(this._printTypeSelected('bg')) {
                canvas.width  = tConfig.productImage.sizeX;
                canvas.height = tConfig.productImage.sizeY;
            } else {
                canvas.width  = tConfig.area.sizeX;
                canvas.height = tConfig.area.sizeY;
            }
            this.allowUpdate();
            canvg(canvas, svg, { ignoreMouse: true, ignoreAnimation: true, renderCallback: callback });
        }
    },

    _getPrintableVersion: function( scale, scaleEnable )
    {
        var option = this.tools.option,
            config = this.tools.config,
            value;

        if (config.editorEnabled) { // product view page
            value = option.editor.save();
            if (value !== '[]') {
                $('options_' + config.optionId).setValue(value);
            }
        } else { // all other pages
            value = $('options_' + config.optionId).getValue();
            if (scaleEnable == false) {
                scaleEnable = true;
            }
        }

        scale = parseFloat(scale);
        if (scale == 0) {
            scale = 1;
        }

        if (config.proportionScreen < 1) {
            scale /= config.proportionScreen;
        }

        var divForPrint = new Element('div'),
            printable   = new Aitcg_Editor(option);
        divForPrint.update('<div class="aitraph-bot"></div><div class="aitraph-top"></div>');
        printable.init(divForPrint, Aitcg_Editor.MODE_SAVE, true, scale);
        printable.load(value, scaleEnable);
        option.editor.makeActive();
        return {
            bottom: divForPrint.down('.aitraph-bot').innerHTML,
            top   : divForPrint.down('.aitraph-top').innerHTML
        };
    },
    
    _modifySvg: function(svg) 
    {
        svg = svg.replace(/>\s+/g, ">")
            .replace(/\s+</g, "<")
            .replace(/<svg/g,'<svg xmlns:xlink="http://www.w3.org/1999/xlink"');

        if (svg.match(/xlink\s*:\s*href\s*=/ig) == null && svg.match(/href\s*=/ig)) {
            svg = svg.replace(/href\s*=/g,'xlink:href=');
        }
        return svg;
    },

    /**
     * Check is certain print type is selected by user
     *
     * @param type String
     * @return Number
     * @private
     */
    _printTypeSelected: function( type )
    {
        var cb = $('print_type_' + type);
        return (cb && cb.checked) ? 1 : 0;
    },

    _getSelectedPrintTypes: function()
    {
        var selected = 0,
            result = {},
            printType = '';
        for(var i = 0; i < this.print_types.length; i++) {
            printType = this.print_types[i];
            result[printType] = this._printTypeSelected(printType);
            selected += result[printType];
        }
        if (!selected) {
            $('print_type_error').show();
            return false;
        }
        $('print_type_error').hide();
        return result;
    },

    /**
     * @param max Number [optional]. Scale limit.
     * @return Number
     * @private
     */
    _getScale: function( max )
    {
        max = max || 0;
        var scaleOption = $('print_scale'),
            value = scaleOption.getValue();
        value = parseFloat(value);
        if (max > 0 && value > max) {
            value = max;
        }
        scaleOption.setValue(value);
        return value;
    },
    
    _getForm: function()
    {
        if (!this.form) {
            if (!$(this.formId)) {
                var d = new Element('DIV').hide();
                d.innerHTML =
                '<form action="" method="POST" id="' + this.formId + '" target="_blank">' +
                    '<input type="hidden" name="data"        value=""  id="' + this.formId + '_data" />' +
                    '<input type="hidden" name="data_bottom" value=""  id="' + this.formId + '_data_bottom" />' +
                    '<input type="hidden" name="type"        value=""  id="' + this.formId + '_type" />' +
                    '<input type="hidden" name="background"  value=""  id="' + this.formId + '_background" />' +
                    '<input type="hidden" name="areaOffsetX" value=""  id="' + this.formId + '_areaOffsetX" />' +
                    '<input type="hidden" name="areaOffsetY" value=""  id="' + this.formId + '_areaOffsetY" />' +
                    '<input type="hidden" name="print_scale" value="1" id="' + this.formId + '_print_scale" />' +
                    '<input type="hidden" name="print_type"  value=""  id="' + this.formId + '_print_type" />' +
                    '<input type="hidden" name="additional"  value=""  id="' + this.formId + '_additional" />' +
                '</form>';
                document.body.appendChild(d);
            }
            this.form = $(this.formId);
        }
        return this.form;
    },
    
    _unsetFormLink: function()
    {
        this.form = null;
    },
    
    _setFormVars: function(data)
    {
        for(var i in data) {
            this._setFormData(i, data[i]);
        }
    },
    
    _setFormData: function( type, value )
    {
        var element = this._getForm().select('#' + this.formId + '_' + type);
        if (element.length == 1) {
            element[0].setValue(value);
        }

    },
    
    _getImageLinkBlock: function()
    {
        var block = $$('#download_default_' + this.tools.config.rand + ' ul')[0];
        if (!block) {
            return false;
        }
        return block;
    },
    
    preventUpdate: function()
    {
        Aitcg.showLoader();
        this._preventUpdate = true;
        return this;
    },
    
    allowUpdate: function()
    {
        Aitcg.hideLoader();
        this._preventUpdate = false;
        return this;
    },
    
    onEditorReset: function()
    {
        if (this._preventUpdate == false) {
            this._updateDownloadLinksBlock(true);
        }
    },
    
    _addDownloadImageLink: function(src, creator)
    {
        if(this._preventUpdate == false) {
            if (!this._storedLinks[src]) {
                creator = creator || {};

                var html = '<a href="' + src + '" target="_blank">' + src.substring(src.lastIndexOf('/') + 1) + '</a>';

                if (creator.tool) {
                    html = creator.tool.decorateSaveLink(html, creator.params);
                }
                html = '<li>' + html + '</li>';
                this._storedLinks[src] = html;
            }
        }
        this._updateDownloadLinksBlock();
    },

    _updateDownloadLinksBlock: function( purge )
    {
        if (this._getImageLinkBlock()) {
            var data = '';

            if (!purge) {
                for (var src in this._storedLinks) {
                    data += this._storedLinks[src];
                }
            }

            if (data) {
                this._getImageLinkBlock().update(data);
                $('download_default_' + this.tools.config.rand).show();
                $('download_default_' + this.tools.config.rand + '_empty').hide();
            } else {
                $('download_default_' + this.tools.config.rand).hide();
                $('download_default_' + this.tools.config.rand + '_empty').show();
            }
        }
    }
});