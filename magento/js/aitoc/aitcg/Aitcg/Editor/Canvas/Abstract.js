/**
 * Abstract Raphael wrapper class
 */
var Aitcg_Editor_Canvas_Abstract = Class.create(
{
    config     : null, // Object
    paper      : null, // Paper (Raphael)
    shapes     : null, // Array
    selected   : null, // Aitcg_Editor_Canvas_Shape
    type       : 'abstract', // String
    elem       : null, // dom element containing svg
    isEditMode : false,

    initialize: function( config, elem, width, height, isEditMode )
    {
        this.config = config;
        this.elem = elem;
        this.paper = Raphael(elem, width, height);
        this.paper.parent = {
            element: elem,
            offset : function(){return Element.cumulativeOffset( this.element )}.bind(this.paper.parent)
        };
        this.shapes = [];
        this.isEditMode = isEditMode && config.editorEnabled;

        this._initObservers();
    },

    _initObservers: function()
    {
        this.elem.observe('click', this.onClick.bindAsEventListener(this));
    },

    isEditable: function()
    {
        return (this.isEditMode && this.config.editorEnabled);
    },

    onClick: function(event)
    {
        if (event.target.parentNode == this.elem){
            this.unselect();
        }
    },

    /**
     * Create new Raphael element at the binded Raphael Paper object
     *
     * @param shape Object
     * @param scale float
     * @param reflectionOnly bool
     *
     * @return {Object} Raphael Element
     */
    createShapeByShapeInfo: function( shape, scale, reflectionOnly )
    {
        var transform;
        if (typeof shape.reflection != 'undefined' && shape.reflection && this.mirror) {
            delete shape.reflection;
            transform = shape.transform;
            var mirrorShapeInfo = this.mirror.createShapeByShapeInfo(shape, scale);
            var mirrorShape = this.mirror.addShape(mirrorShapeInfo, true);
            shape.transform = transform;
            shape.opacity = 0;
            if (reflectionOnly) {
                return mirrorShapeInfo;
            }
        }

        var newShape;
        if (!(newShape = this.getShapeById(shape.id))) {
            newShape = this.paper.image(shape.src, 0, 0, 0, 0);
        }
        if (typeof shape.transform != 'undefined') {
            transform = shape.transform;
            delete shape.transform;
            newShape.transform(transform);
        }
        newShape.attr(shape);
        if (this.isEditable()) {
            newShape.attr('cursor', 'pointer');
        }

        if (scale != 1) {
            newShape.transform(Raphael.format(
                's{0},{1},0,0...',
                scale,
                scale
            ));
        }

        newShape.id         = shape.id;
        newShape.creator    = shape.creator;
        newShape.reflection = mirrorShape;

        return newShape;
    },

    /**
     * Wrap new Raphael element with our
     * wrapper and store for future use.
     *
     * @param element Element (Raphael)
     * @param no_select bool
     *
     * @return {Aitcg_Editor_Canvas_Shape}
     */
    addShape: function( element, no_select)
    {
        var shape = new Aitcg_Editor_Canvas_Shape(element, this);

        this.shapes.push(shape);
        if (!no_select && this.isEditable()){
            shape.select();
        }

        Aitoc_Common_Events.dispatch('aitcg_canvas_add_shape_after_' + this.config.optionId, {shape: shape});

        return shape;
    },

    /**
     * Get certain shape by its id
     *
     * @param id
     * @return {Object} Raphael Element | null
     */
    getShapeById: function( id )
    {
        var found = null;
        if (id) {
            this.shapes.each(function(shape){
                if (shape.id == id) {
                    found =  shape.element;
                }
            });
        }
        return found;
    },

    /**
     * Remove all shapes from canvas
     *
     * @return {Aitcg_Editor_Canvas_Shape}
     */
    clear: function()
    {
        this.shapes.each(function(shape){
            shape.remove();
        });
        return this;
    },

    /**
     * Select certain shape
     *
     * TODO: Do we still in need of this method, hm? Commented for now.
     *
     * @param shape
     * @return {Aitcg_Editor_Canvas_Shape}
     */
    /*
    select: function( shape )
    {
        return this;
    },
    */

    /**
     * Remove selection from current selected shape
     *
     * @return {Aitcg_Editor_Canvas_Shape}
     */
    unselect: function()
    {
        if (this.selected) {
            this.selected.unselect();
        }
        return this;
    }
});