/**
 * Delete tracker
 */
var Aitcg_Editor_Canvas_Tracker_Delete = Class.create( Aitcg_Editor_Canvas_Tracker_Symbol_Abstract,
{
    letter: "x",
    size  : 19,

    tooltipText: '',

    update: function( $super, box )
    {
        this.element.attr({
            x : box.inner.x + box.inner.width + 6,
            y : box.inner.y - 8
        });
        $super(box);
    },

    _initObservers: function( $super )
    {
        this.tooltipText = this.shape.canvas.config.text.click_to_del;
        $super();
//commented because text deleted when we try to edit text object 
//(press backspace or delete on 'Text:' field focused in 'Add Text' tab)
//        this.keyTracker = this.onKeyPress.bindAsEventListener(this);
//        Event.observe(document, 'keypress', this.keyTracker);
    },

    remove: function( $super )
    {
        Event.stopObserving(document, 'keypress', this.keyTracker);
        $super();
    },

    onClick: function()
    {
        this.shape.remove();
    },

    onKeyPress: function( event )
    {
        switch (event.keyCode) {
            case Event.KEY_DELETE:
            case Event.KEY_BACKSPACE: // for mac users
                event.preventDefault();
                this.shape.remove();
                break;
        }
    }
});