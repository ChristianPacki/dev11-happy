/**
 * Abstract tracker
 */
var Aitcg_Editor_Canvas_Tracker_Abstract = Class.create(
{
    trackers   : null, // Aitcg_Editor_Canvas_Trackers
    shape      : null, // Aitcg_Editor_Canvas_Shape
    element    : null, // Element (Raphael)
    paper      : null, // Paper (Raphael)
    isActive   : false,
    isRotated  : false,

    tooltipText: '',
    tooltipObj : null, // Aitcg_Editor_Canvas_Tooltip

    initialize: function( trackers )
    {
        this.trackers = trackers;
        this.shape = this.trackers.shape;
        this.paper = this.shape.paper;

        this._drawTracker();
        this._initObservers();
        this.trackers.controlsSet.push(this.element);
    },

    /**
     * Draws an appropriate tracker and returns resulting Raphael Element
     *
     * @return {Object} Raphael Element
     * @abstract
     */
    _drawTracker: function(){},

    /**
     * Create all observers which are necessary for correct tracker work
     *
     * @abstract
     */
    _initObservers: function(){},

    update: function( box )
    {
        // prepend a rotation to a tracker if the shape was rotated
        if (this.shape.isRotated) {
            this.element.transform(Raphael.format(
                'R{0},{1},{2}',
                this.shape.getAngle(),
                box.cx,
                box.cy
            ));
        }
    },

    /**
     * Remove tracker from the canvas
     */
    remove: function()
    {
        this.tooltip().hide();
        this.element.remove();
    },

    /**
     * If the shape is rotated then apply the same
     * rotation to cursor movement delta
     *
     * @param dx
     * @param dy

     * @return {dx: Number, dy: Number}
     */
    _checkDeltas: function( dx, dy )
    {
        if (this.shape.isRotated) {
            var rotatedPoint = this.getRotatedPoint( dx, dy, -this.shape.getAngle() );
            dx = rotatedPoint.x;
            dy = rotatedPoint.y;
        }
        return {dx: dx, dy: dy};
    },

    /**
     * Get coords of some point after rotation for a certain
     * angle in degrees around a circle with given center coords.
     *
     * @param x Coordinate x of the given point
     * @param y Coordinate y of the given point
     * @param angle Rotation angle
     *
     * @return {x: Number, y: Number}
     *
     * TODO: Optimize
     */
    getRotatedPoint: function( x, y, angle )
    {
        var r = Aitcg.pythagoras(x, y, false);
        // initial angle in relation to center
        var iA = Math.atan2(y, x) * Aitcg.fromRadMult;

        return {
            x: r * Math.cos((angle + iA) * Aitcg.toRadMult),
            y: r * Math.sin((angle + iA) * Aitcg.toRadMult)
        };
    },

    onMouseOver: function()
    {
        this.element.attr("fill", "red");
        this.tooltip().show();
    },

    onMouseOut: function()
    {
        this.element.attr("fill", "white");
        this.tooltip().hide();
    },

    /**
     * Creates and returns a tooltip object
     *
     * @return {Aitcg_Tooltip}
     */
    tooltip: function()
    {
        return Aitcg.tooltip().update(this.tooltipText);
    }

});