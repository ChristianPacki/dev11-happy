/**
 * Position to front tracker
 */
var Aitcg_Editor_Canvas_Tracker_Symbol_Abstract = Class.create( Aitcg_Editor_Canvas_Tracker_Abstract,
{
    offsetX: 0,
    offsetY: 0,
    letter: "",
    size: 30,

    _drawTracker: function()
    {
        this.element = this.paper.text(0, 0, this.letter).attr({
            "stroke-width": 0.5,
            "stroke": "green",
            "font-size": this.size,
            "fill": "white",
            "cursor": "pointer"
        });
    },

    _initObservers: function()
    {
        this.element.hover(this.onMouseOver, this.onMouseOut, this, this)
                    .click(this.onClick.bind(this));
    }
});