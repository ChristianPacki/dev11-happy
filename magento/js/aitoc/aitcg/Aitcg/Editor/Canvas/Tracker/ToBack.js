/**
 * Position to back tracker
 */
var Aitcg_Editor_Canvas_Tracker_ToBack = Class.create( Aitcg_Editor_Canvas_Tracker_ToFront,
{
    offsetY: 6,
    letter : "V",
    size   : 19,

    tooltipText: 'Click to bring to back',

    update: function( $super, box )
    {
        this.element.attr({
            x : box.inner.x -6,
            y : box.inner.y + box.inner.height/2 + this.offsetY
        });
        $super(box);
        this.tooltipText = this.shape.canvas.config.text.click_to_back;
    },

    onClick: function()
    {
        this.shape.changeLayer(0);
    }
});