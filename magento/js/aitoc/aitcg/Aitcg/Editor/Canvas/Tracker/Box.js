/**
 * Bounding box and move tracker
 */
var Aitcg_Editor_Canvas_Tracker_Box = Class.create( Aitcg_Editor_Canvas_Tracker_Abstract,
{
    baseDX: 0,
    baseDY: 0,
    ctrlKeyShift: 10,

    _drawTracker: function()
    {
        this.element = this.paper.rect(0, 0, 0, 0).attr({
            "fill-opacity": 0.01,
            'fill':'white',
            'stroke-opacity':'0.3',
            'cursor': 'move'
        });
    },

    _initObservers: function()
    {
        this.element.drag( this.onDragMove, this.onDragStart, this.onDragEnd, this, this, this);
        this.keyTracker = this.onKeyPress.bindAsEventListener(this);
        Event.observe(document, 'keypress', this.keyTracker);
    },

    update: function( $super, box )
    {
        this.element.attr({
            width : box.inner.width  + 11,
            height: box.inner.height + 11,
            x     : box.inner.x - 6,
            y     : box.inner.y - 6
        });
        $super(box);
    },

    onDragStart: function( x, y, event )
    {
        this.baseDX = 0;
        this.baseDY = 0;
        this.isActive = true;
    },

    onDragMove: function( dx, dy, x, y, event )
    {
        var deltas = this._checkDeltas(dx, dy);

        this.shape.transform(Raphael.format(
            '...t{0},{1}',
            deltas.dx - this.baseDX,
            deltas.dy - this.baseDY
        ));

        this.baseDX = deltas.dx;
        this.baseDY = deltas.dy;

        this.trackers.updateTrackersPositions();
    },

    onDragEnd: function(event)
    {
        this.isActive = false;
    },

    /**
     * Apply scale to deltas before rotation check
     */
    _checkDeltas: function( $super, dx, dy )
    {
        var trans = this.shape.element.matrix.split();

        dx = dx / trans.scalex;
        dy = dy / trans.scaley;

        return $super(dx, dy);
    },

    onKeyPress: function( event )
    {
        switch (event.keyCode) {
            case Event.KEY_UP:
                this._keyShift(event, 0, -1);
                break;
            case Event.KEY_RIGHT:
                this._keyShift(event, 1, 0);
                break;
            case Event.KEY_DOWN:
                this._keyShift(event, 0, 1);
                break;
            case Event.KEY_LEFT:
                this._keyShift(event, -1, 0);
                break;
        }
    },

    _keyShift: function( event, x, y )
    {
        this.baseDX = 0;
        this.baseDY = 0;

        var mult = event.ctrlKey ? this.ctrlKeyShift : 1;

        event.preventDefault();
        this.onDragMove(x * mult, y * mult);
    },

    remove: function( $super )
    {
        Event.stopObserving(document, 'keypress', this.keyTracker);
        $super();
    }
});