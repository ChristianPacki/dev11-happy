/**
 * Position to front tracker
 */
var Aitcg_Editor_Canvas_Tracker_ToFront = Class.create( Aitcg_Editor_Canvas_Tracker_Symbol_Abstract,
{
    offsetY: -6,
    letter : "^",

    tooltipText: '',

    update: function( $super, box )
    {
        this.element.attr({
            x : box.inner.x -6,
            y : box.inner.y + box.inner.height/2 + this.offsetY
        });
        $super(box);
        this.tooltipText = this.shape.canvas.config.text.click_to_front;
    },

    onClick: function()
    {
        this.shape.changeLayer(1);
    }
});