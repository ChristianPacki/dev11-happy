/**
 * Resize tracker
 */
var Aitcg_Editor_Canvas_Tracker_Resize = Class.create( Aitcg_Editor_Canvas_Tracker_Abstract,
{
    baseDX : 0,
    baseDY: 0,

    letter: "↔",
    size  : 14,
    tooltipText: '',

    rorated: false,

    _drawTracker: function()
    {
        this.element = this.paper.text(0, 0, this.letter).attr({
            "stroke-width": 0.5,
            "stroke": "green",
            "font-size": this.size,
            "fill": "white",
            "cursor": "pointer"
        });

        this.tooltipText = this.shape.canvas.config.text.click_to_resize;
    },

    _initObservers: function()
    {
        this.element.hover(this.onMouseOver, this.onMouseOut, this, this)
            .drag( this.onDragMove, this.onDragStart, this.onDragEnd, this, this, this);
    },

    update: function( $super, box )
    {
        if (this.rorated) {
            this.element.rotate(-45);
        }

        this.element.attr({
            x: box.inner.x + box.inner.width  + 5,
            y: box.inner.y + box.inner.height + 3
        });

        this.element.rotate(45);
        this.rorated = true;

        $super(box);

    },


    onMouseOver: function()
    {
        if (!this.isActive) {
            this.element.attr("fill", "red");
            this.tooltip().show();
        }
    },

    onMouseOut: function()
    {
        if (!this.isActive) {
            this.element.attr("fill", "white");
            this.tooltip().hide();
        }
    },

    onDragStart: function( x, y, event )
    {
        this.baseDX = 0;
        this.baseDY = 0;
        this.element.attr("fill", "red");
        this.isActive = true;
    },

    onDragMove: function( dx, dy, x, y, event )
    {
        var distortion = this._isDistortionAllowed(event),
            box = this.shape.getBBox(),
            deltas = this._checkDeltas(dx, dy, distortion, box),
            sx  = (box.inner.width  + deltas.dx - this.baseDX) / box.inner.width,
            sy  = (box.inner.height + deltas.dy - this.baseDY) / box.inner.height;

        if(sx <= 0 || sx == NaN)
        {
            sx = 1;
        }
        if(sy <= 0 || sy == NaN)
        {
            sy = 1;
        }

        this.shape.transform(Raphael.format(
            '...s{0},{1},0,0',
            sx, sy
        ));
        this.baseDX = deltas.dx;
        this.baseDY = deltas.dy;

        this.trackers.updateTrackersPositions();
    },

    onDragEnd: function( event )
    {
        this.element.attr("fill", "white");
        this.isActive = false;
        this.tooltip().hide();
    },

    _isDistortionAllowed: function( event )
    {
        return (this.shape.canvas.config.distortion && !event.shiftKey);
    },

    _checkDeltas: function( $super, dx, dy, distortion, box )
    {
        var deltas = $super(dx, dy);

        if (!distortion) {
            var baseAspect  = box.inner.width / box.inner.height,
                deltaAspect = deltas.dx / deltas.dy;

            if (baseAspect != deltaAspect) {
                deltas.dy /= baseAspect / deltaAspect;
            }
        }

        return deltas;
    }
});