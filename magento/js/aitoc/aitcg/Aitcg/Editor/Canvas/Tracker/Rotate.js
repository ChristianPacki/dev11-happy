/**
 * Rotate tracker
 *
 * TODO: Fix rotate + resize problem
 */

var Aitcg_Editor_Canvas_Tracker_Rotate = Class.create( Aitcg_Editor_Canvas_Tracker_Abstract,
{
    baseCx: 0,
    baseCy: 0,
    tooltipText: '',

    _drawTracker: function()
    {
        var w = 5;
        this.element = this.paper.ellipse(0, 0, w, w).attr({
            "stroke-width": 1,
            "stroke": "green",
            "fill"  : "white",
            "cursor": "pointer"
        });

        this.tooltipText = this.shape.canvas.config.text.click_to_rotate;
    },

    _initObservers: function()
    {
        this.element.hover(this.onMouseOver, this.onMouseOut, this, this)
                    .drag(this.onDragMove, this.onDragStart, this.onDragEnd, this, this, this)
                    .dblclick(this.onDoubleClick.bind(this));
    },

    update: function( $super, box )
    {
        this.element.attr({
            cx: box.cx,
            cy: box.inner.y - 14
        });
        $super(box);
    },

    onMouseOver: function()
    {
        if (!this.isActive) {
            this.element.attr("fill", "red");
            this.tooltip().show();
        }
    },

    onMouseOut: function()
    {
        if (!this.isActive) {
            this.element.attr("fill", "white");
            this.tooltip().hide();
        }
    },

    onDragStart: function(x, y, event)
    {
        var offset = this.paper.parent.offset();
        this.baseCx = x - offset.left;
        this.baseCy = y - offset.top;
        this.element.attr("fill", "red");
        this.isActive = true;
        this.shape.isRotated = true;
    },

    onDragMove: function(dx, dy, x, y, event)
    {
        var box = this.shape.getBBox(),
            rad = Math.atan2(
                this.baseCy + dy - box.cy,
                this.baseCx + dx - box.cx
            ),
            deg = ((((rad * (180/Math.PI))+90) % 360)+360) % 360;
            deg = 1 * deg.toFixed(1);
        deg -= this.shape.getAngle();

        this.shape.transform(Raphael.format(
            'r{0},{1},{2}...',
            deg,
            box.cx,
            box.cy
        ));

        this.trackers.updateTrackersPositions();
    },

    onDragEnd: function(event)
    {
        this.element.attr("fill", "white");
        this.isActive = false;
        this.tooltip().hide();
    },

    onDoubleClick: function()
    {
        var box = this.shape.getBBox()
        this.shape.transform(Raphael.format(
            'r{0},{1},{2}...',
            -this.shape.getAngle(),
            box.cx,
            box.cy
        ));
        this.trackers.updateTrackersPositions();
        this.shape.isRotated = false;
    }
});