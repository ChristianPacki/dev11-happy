/**
 * Central dot point
 */
var Aitcg_Editor_Canvas_Tracker_CenterDot = Class.create( Aitcg_Editor_Canvas_Tracker_Abstract,
{
    _drawTracker: function()
    {
        var w = 3;
        this.element = this.paper.ellipse(0, 0, w, w).attr({
            "stroke-width": 0,
            "fill"  : "red"
        });
    },

    update: function( box )
    {
        this.element.attr({
            cx: box.cx,
            cy: box.cy
        });
    }
});

/**
 * Outer box bounding circle
 */
var Aitcg_Editor_Canvas_Tracker_OuterCircle = Class.create( Aitcg_Editor_Canvas_Tracker_Abstract,
{
    _drawTracker: function()
    {
        this.element = this.paper.ellipse(0, 0, 0, 0).attr({
            "stroke-width": 1,
            "stroke": "red"
        });
    },

    update: function( box )
    {
        this.element.attr({
            rx: box.r,
            ry: box.r,
            cx: box.cx,
            cy: box.cy
        });
    }
});

/**
 * Outer bounding box
 */
var Aitcg_Editor_Canvas_Tracker_OuterBox = Class.create( Aitcg_Editor_Canvas_Tracker_Abstract,
{
    _drawTracker: function()
    {
        this.element = this.paper.rect(0, 0, 0, 0).attr({
            "stroke-width": 1,
            "stroke": "red"
        });
    },

    update: function( box )
    {
        this.element.attr({
            width : box.width,
            height: box.height,
            x: box.x,
            y: box.y
        });
    }
});


/**
 * Inner box bounding circle
 */
var Aitcg_Editor_Canvas_Tracker_InnerCircle = Class.create( Aitcg_Editor_Canvas_Tracker_Abstract,
{
    _drawTracker: function()
    {
        this.element = this.paper.ellipse(0, 0, 0, 0).attr({
            "stroke-width": 1,
            "stroke": "lime"
        });
    },

    update: function( box )
    {
        this.element.attr({
            rx: box.inner.r,
            ry: box.inner.r,
            cx: box.cx,
            cy: box.cy
        });
    }
});

/**
 * Inner bounding box
 */
var Aitcg_Editor_Canvas_Tracker_InnerBox = Class.create( Aitcg_Editor_Canvas_Tracker_Abstract,
{
    _drawTracker: function()
    {
        this.element = this.paper.rect(0, 0, 0, 0).attr({
            "stroke-width": 1,
            "stroke": "lime"
        });
    },

    update: function( box )
    {
        this.element.attr({
            width : box.inner.width,
            height: box.inner.height,
            x: box.inner.x,
            y: box.inner.y
        });
    }
});