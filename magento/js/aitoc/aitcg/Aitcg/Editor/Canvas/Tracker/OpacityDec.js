/**
 * Opacity decrease tracker
 */
var Aitcg_Editor_Canvas_Tracker_OpacityDec = Class.create( Aitcg_Editor_Canvas_Tracker_Symbol_Abstract,
{
    offsetX: -7,
    offsetY: 10,
    letter : "-",
    size   : 30,

    tooltipText: '',

    update: function( $super, box )
    {
        this.element.attr({
            x : box.inner.x + box.inner.width/2 + this.offsetX,
            y : box.inner.y + box.inner.height + this.offsetY
        });
        $super(box);
        this.tooltipText = this.shape.canvas.config.text.click_to_dec_opac;
    },

    onClick: function()
    {
        this.shape.changeOpacity(-0.1);
    }
});