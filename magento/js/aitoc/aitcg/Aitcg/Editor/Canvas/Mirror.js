/**
 * Class which represents canvas layer located under the template image.
 * I've called it `Mirror` because its main goal is to catch and reflect
 * actions made to some elements in the main canvas layer.
 */
var Aitcg_Editor_Canvas_Mirror = Class.create( Aitcg_Editor_Canvas_Abstract,
{
    type: 'mirror',
    main: null, // Aitcg_Editor_Canvas

    initialize: function( $super, main, elem, width, height )
    {
        this.main = main;
        $super( main.config, elem.down('.aitraph-bot'), width, height, false );
        this._initObservers();
    },

    _initObservers: function()
    {
        //add shape
        //change shape
        //remove shape
    }

});