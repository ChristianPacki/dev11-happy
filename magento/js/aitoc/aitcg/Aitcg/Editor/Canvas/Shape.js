/**
 * Wrapper for Raphael element
 */
var Aitcg_Editor_Canvas_Shape = Class.create(
{
    canvas     : null,  // Aitcg_Editor_Canvas_Abstract
    element    : null,  // Element (Raphael)
    reflection : null,  // Aitcg_Editor_Canvas_Shape
    selected   : false, // Bool
    paper      : null,  // Paper (Raphael)
    trackers   : null,  // Aitcg_Editor_Canvas_Trackers
    isRotated  : false,
    keyTracker : null,
    ghostOpacity: 0.3,
    editAvailableFor: ['Text'],

    initialize: function( element, canvas )
    {
        this.canvas  = canvas;
        this.element = element;
        this.paper   = element.paper;
        this.creator = element.creator;

        if (this.element.matrix.split().rotate) {
            this.isRotated = true;
        }

        this._initObservers();

        if (element.reflection) {
            this.reflection = element.reflection;
        }

        if (this.creator && this.creator.isNew) {
            this.select();
        }
    },

    _initObservers: function()
    {
        this.element.click(this.select.bind(this));
    },
    
    /**
     * @param raphaelFormat
     */
    transform: function(raphaelFormat)
    {
        this.element.transform(raphaelFormat);
        if(this.reflection) {
            this.reflection.transform(raphaelFormat);
        }
    },

    select: function()
    {
        if (!this.selected && this.canvas.isEditable()) {
            this.selected = true;
            if (this.canvas.selected) {
                this.canvas.selected.unselect();
            }
            this.canvas.selected = this;

            if (!this.trackers) {
                this.trackers = new Aitcg_Editor_Canvas_Trackers( this );
            }

            if (this._isEditAvailable()) {
                this.creator.tool.edit( this, this.creator.params );
            }
            
            if(this.hasReflection()) {
                this.element.attr('opacity', this.ghostOpacity);
            }
        }
    },

    unselect: function()
    {
        if (this.selected) {
            this.selected = false;
            if (this.trackers) {
                this.trackers.remove();
                this.trackers = null;
            }

            if (this._isEditAvailable()) {
                this.creator.tool.editStop();
            }

            if(this.hasReflection()) {
                this.element.attr('opacity', 0);
            }
            this.canvas.selected = null;
        }
    },
    
    hasReflection: function()
    {
        return this.reflection ? true : false;
    },

    /**
     * Return Raphael Element's bounding box object extended
     * with information about current inner shape params.
     *
     * @return {Object}
     */
    getBBox: function()
    {
        var box = this.element.getBBox();

        box.r = Aitcg.pythagoras(box.width/2, box.height/2, false);
        box.inner = this._getInnerBBox( box, this.getAngle() );

        return box;
    },

    /**
     * Beware: Dark magic ahead!
     *
     * @param box Outer bounding box object
     * @param angle Angle id degrees
     * @return {Object}
     * @private
     *
     * TODO: Optimize
     * TODO: Description with illustration (should be placed on www.aitoc.com)!
     */
    _getInnerBBox: function( box, angle )
    {
        // multiplier is equal to either 1 or -1 depending on circle's quarter
        var mult = Math.pow(-1, Math.floor(angle / 90));
        // flip angle in the second and the fourth quarters
        angle = Math.abs((360 + angle*mult) % 90);

        var i        = {},
            alphaRad = angle * Aitcg.toRadMult, // alpha angle in radians
            omegaRad  = (90 - angle * 2) * Aitcg.toRadMult, //omega angle in radians
            cosAlpha = Math.cos(alphaRad),
            tanAlpha = Math.tan(alphaRad),
            tmp      = (box.width/2 - box.height/2 * tanAlpha) / Math.sin(omegaRad);

        i.width  = tmp * cosAlpha; // half(!) of the inner shape's width
        i.height = (box.height/2 / cosAlpha - i.width * tanAlpha); // half(!) of the inner shape's height
        i.x = box.cx - i.width;
        i.y = box.cy - i.height;
        i.r = Aitcg.pythagoras(i.width, i.height, false);

        i.width  *= 2;
        i.height *= 2;

        return i;
    },

    remove: function()
    {
        Aitoc_Common_Events.dispatch('aitcg_shape_delete_before_' + this.canvas.config.optionId, {shape: this});

        this.unselect();
        this.element.remove();
        if (this.hasReflection()) {
            this.reflection.remove();
        }        
        this.canvas.shapes = this.canvas.shapes.without(this);
    },

    /**
     * Change shape's opacity or if the shape has a reflection
     * then change reflection's opacity instead.
     *
     * @param delta Float
     */
    changeOpacity: function( delta )
    {
        if (this.hasReflection()) {
            return this.reflection.changeOpacity(delta);
        }

        var opacity = 1.0, // default
            currentOpacity = this.element.attr("opacity");

        if (typeof currentOpacity != 'undefined' && !isNaN(currentOpacity)) {
            opacity = parseFloat(currentOpacity);
        }

        var newOpacity = opacity + delta;
        newOpacity = Math.max(Math.min(1, newOpacity), 0);
        this.element.attr('opacity', newOpacity);
    },
    
    changeLayer: function( direction )
    {
        if (this.hasReflection()) {
            this.reflection.changeLayer(direction);
        }
        var i = this.canvas.shapes.indexOf(this);
        this.canvas.shapes.splice(i,1);
        switch (direction) {
            case 1:
                this.element.toFront();
                this.trackers.controlsSet.toFront();
                this.canvas.shapes.push(this);
            break;
            default:
                this.element.toBack();
                this.canvas.shapes.unshift(this);
            break;
        }        
    },

    /**
     * Return current rotation angle
     *
     * @return {float}
     */
    getAngle: function()
    {
        return this.element._.deg % 360;
    },

    /**
     * Update the shape with the new source image and assign new creator params
     *
     * @param newSrc String
     * @param creator Object
     */
    updateSource: function( newSrc, creator )
    {
        if (this.hasReflection()) {
            this.reflection.updateSource(newSrc, creator);
        }        
        this.element.attr('src', newSrc);
        this.creator = creator;
    },

    /**
     * Check if edit action is available for this shape
     *
     * @return {boolean}
     * @private
     */
    _isEditAvailable: function()
    {
        if ( typeof this.creator != 'undefined' ) {
            // is available for a certain creator tool
            return this.editAvailableFor.indexOf(this.creator.type) != -1;
        }
        // creator is unknown - edit is forbidden
        return false;
    }
});