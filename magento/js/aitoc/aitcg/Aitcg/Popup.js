var Aitcg_Popup = Class.create(
{
    template : null,
    options : null,
    templateSyntax : /(^|.|\r|\n)({{(\w+)}})/,
    
    initialize: function(template, options) {
        this.template = template;
        this.options  = options;
    },
    
    render: function() {
        var popup = new Template(this.template, this.templateSyntax);
        return popup.evaluate(this.options);
    },
    
    renderWindow: function ( imgId, fullUrl, callback ) {
        Aitcg.showLoader();
        Aitcg.cacheImage(imgId, fullUrl, callback);
    },

    showTextWindow: function() {
        // window rendering
        var scr = Aitcg.getDefault(),
            body = $$('body')[0];
        Element.insert(body, {bottom: this.render()});

        var popup = $('message-popup-window'),
            mask  = $('message-popup-window-mask');

        mask.setStyle({'height': body.getHeight() + 'px'});

        popup.setStyle({
            'top'        : ''  + parseInt(50 + scr.off.top) + 'px'
        });

        mask.show();
        popup.addClassName('show');
    }, 


    //custom popup
    showWindow: function ( imgId, scr )
    {
        var body = $$('body')[0];

        Element.insert(body, {bottom: this.render()});

        var popup = $('message-popup-window'),
            mask = $('message-popup-window-mask');

        mask.setStyle({'height': body.getHeight()+'px'});

        var windowWidth = scr.curr.width + 500, // +32 is because of 15px padding and 1px border
            topTmp = scr.off.top;
        var windowWidth;

        if (mobileMode) {
            windowWidth = scr.curr.width + 32;// +32 is because of 15px padding and 1px border
        } else {
            windowWidth = scr.curr.width + 500;
        }

        var topTmp = scr.off.top;

        if (scr.dim.height > popup.getHeight()) {
            topTmp += parseInt( (scr.dim.height - popup.getHeight()) / 2);
        }

        popup.setStyle({
            'width'      : windowWidth + 'px',
            'top'        : topTmp + 'px',
            'marginLeft' : '-' + (parseInt(windowWidth/2)) + 'px' // 16 = padding 15 + border 1
        });

        mask.show();
        popup.addClassName('show');

        return scr;
    },

    closeEditor : function() {
        $('message-popup-window').remove();
        $('message-popup-window-mask').remove();
    }
    
});